package com.xz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 启动程序
 *
 * @author xz
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableTransactionManagement
@EnableAspectJAutoProxy(proxyTargetClass = true,exposeProxy = true)
public class XzApplication {
    public static void main(String[] args) {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(XzApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  ERP系统启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                "\n" +
                "       _                     _      _ \n" +
                "      (_)                   | |    (_)\n" +
                "__  __ _  _ __    __ _  ____| |__   _ \n" +
                "\\ \\/ /| || '_ \\  / _` ||_  /| '_ \\ | |\n" +
                " >  < | || | | || (_| | / / | | | || |\n" +
                "/_/\\_\\|_||_| |_| \\__, |/___||_| |_||_|\n" +
                "                  __/ |               \n" +
                "                 |___/                \n");

//        OrderAutoCancel.consumerDelayMessage();
    }
}
