package com.xz.web.controller.report;

import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.utils.SecurityUtils;
import com.xz.common.utils.StringUtils;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.purchase.domain.Purchase;
import com.xz.purchase.vo.PurchaseStatisticsVo;
import com.xz.report.param.SalesReportParam;
import com.xz.report.service.ISalesStatisticsService;
import com.xz.report.vo.SalesFlowReportVo;
import com.xz.report.vo.SalesReportVo;
import com.xz.report.vo.SalesReturnReportVo;
import com.xz.system.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 销售单换货Controller
 *
 * @author xz
 * @date 2024-02-05
 */
@RestController
@RequestMapping("/sales/statistics")
public class SalesStatisticsController extends BaseController {
    @Autowired
    private ISalesStatisticsService salesStatisticsService;
    @Autowired
    private ISysRoleService sysRoleService;

    /**
     * 查询销售报表数据
     */
    //@PreAuthorize("@ss.hasPermi('sales:order:list')")
    @GetMapping("/salesReport")
    public TableDataInfo salesReport(SalesReportParam salesReportParam)
    {
        setQueryDept(salesReportParam);
        startPage();
        List<SalesReportVo> salesReportVos = salesStatisticsService.salesReport(salesReportParam);
        return getDataTable(salesReportVos);
    }

    /**
     * 销售报表导出
     */
    @GetMapping("/exportSalesReport")
    public AjaxResult exportSalesReport(SalesReportParam salesReportParam) {

        setQueryDept(salesReportParam);
        List<SalesReportVo> salesReportVos = salesStatisticsService.salesReport(salesReportParam);
        ExcelUtil<SalesReportVo> util = new ExcelUtil<SalesReportVo>(SalesReportVo.class);
        return util.exportExcel(salesReportVos, "销售报表");
    }



    /**
     * 查询销售报表数据
     */
    //@PreAuthorize("@ss.hasPermi('sales:order:list')")
    @GetMapping("/salesReturnReport")
    public TableDataInfo salesReturnReport(SalesReportParam salesReportParam)
    {
        setQueryDept(salesReportParam);
        startPage();
        List<SalesReturnReportVo> list = salesStatisticsService.salesReturnReport(salesReportParam);
        return getDataTable(list);
    }

    /**
     *  销售退货报表导出
     */
    @GetMapping("/exportSalesReturnReport")
    public AjaxResult exportSalesReturnReport(SalesReportParam salesReportParam) {

        setQueryDept(salesReportParam);
        List<SalesReturnReportVo> list = salesStatisticsService.salesReturnReport(salesReportParam);
        ExcelUtil<SalesReturnReportVo> util = new ExcelUtil<SalesReturnReportVo>(SalesReturnReportVo.class);
        return util.exportExcel(list, "销售退货");
    }

    /**
     * 查询销售报表数据
     */
    //@PreAuthorize("@ss.hasPermi('sales:order:list')")
    @GetMapping("/salesFlowReport")
    public TableDataInfo salesFlowReport(SalesReportParam salesReportParam)
    {
        setQueryDept(salesReportParam);
        startPage();
        List<SalesFlowReportVo> list = salesStatisticsService.salesFlowReport(salesReportParam);
        return getDataTable(list);
    }

    /**
     *  销售流水报表导出
     */
    @GetMapping("/exportSalesFlowReport")
    public AjaxResult exportSalesFlowReport(SalesReportParam salesReportParam) {

        setQueryDept(salesReportParam);
        List<SalesFlowReportVo> list = salesStatisticsService.salesFlowReport(salesReportParam);
        ExcelUtil<SalesFlowReportVo> util = new ExcelUtil<SalesFlowReportVo>(SalesFlowReportVo.class);
        return util.exportExcel(list, "销售流水");
    }

    private void setQueryDept(SalesReportParam salesReportParam){
        // 获取当前登录人可看到部门
        List<Long> list1 = sysRoleService.myQueryDeptId();
        if(list1 != null && list1.size()==0){
            // 0 看自己部门
            salesReportParam.setQueryDeptIdListStr(SecurityUtils.getLoginUser().getDeptId().toString());
        }else if(list1 != null && list1.size()>0){
            // 大于0  自定义
            salesReportParam.setQueryDeptIdListStr(StringUtils.join(list1,","));
        }
    }
}
