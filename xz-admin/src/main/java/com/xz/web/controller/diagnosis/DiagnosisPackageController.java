package com.xz.web.controller.diagnosis;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.diagnosis.domain.DiagnosisPackage;
import com.xz.diagnosis.dto.DiagnosisPackageDto;
import com.xz.diagnosis.service.IDiagnosisPackageItemService;
import com.xz.diagnosis.service.IDiagnosisPackageService;
import com.xz.diagnosis.vo.DiagnosisPackageItemVo;
import com.xz.diagnosis.vo.DiagnosisPackageVo;
import com.xz.diagnosis.vo.PackageUnionItemChildrenVo;
import com.xz.diagnosis.vo.PackageUnionItemVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 诊疗套餐Controller
 *
 * @author xz
 * @date 2024-03-14
 */
@RestController
@RequestMapping("/diagnosis/package")
public class DiagnosisPackageController extends BaseController {
  @Autowired
  private IDiagnosisPackageService diagnosisPackageService;
  @Autowired
  private IDiagnosisPackageItemService diagnosisPackageItemService;

  /**
   * 查询诊疗套餐与项目合并列表
   */
  @GetMapping("/union/list")
  public AjaxResult selectPackageUnionItemList(DiagnosisPackage diagnosisPackage) {
    List<PackageUnionItemVo> list = diagnosisPackageService.selectPackageUnionItemList(diagnosisPackage);
    list.forEach(data->{
      if(data.getType().equals(1)){
        List<DiagnosisPackageItemVo> items = diagnosisPackageItemService.selectByPackageId(data.getId());
        if(!CollectionUtils.isEmpty(items)){
          data.setChildren(items.stream().map(a -> new PackageUnionItemChildrenVo(a)).collect(Collectors.toList()));
        }
      }
    });
    return AjaxResult.success(list);
  }
  /**
   * 查询诊疗套餐列表
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:package:list')")
  @GetMapping("/list")
  public TableDataInfo list(DiagnosisPackage diagnosisPackage) {
    startPage();
    List<DiagnosisPackageVo> list = diagnosisPackageService.selectDiagnosisPackageVoList(diagnosisPackage);
    return getDataTable(list);
  }

  /**
   * 导出诊疗套餐列表
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:package:export')")
  @Log(title = "诊疗套餐", businessType = BusinessType.EXPORT)
  @GetMapping("/export")
  public AjaxResult export(DiagnosisPackage diagnosisPackage) {
    List<DiagnosisPackage> list = diagnosisPackageService.selectDiagnosisPackageList(diagnosisPackage);
    ExcelUtil<DiagnosisPackage> util = new ExcelUtil<DiagnosisPackage>(DiagnosisPackage.class);
    return util.exportExcel(list, "诊疗套餐数据");
  }

  /**
   * 获取诊疗套餐详细信息
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:package:query')")
  @GetMapping(value = "/{id}")
  public AjaxResult getInfo(@PathVariable("id") Long id) {
    DiagnosisPackage entity = diagnosisPackageService.selectDiagnosisPackageById(id);
    DiagnosisPackageVo vo = new DiagnosisPackageVo();
    if(null != entity){
      BeanUtils.copyProperties(entity,vo);
      // 查询项目
      List<DiagnosisPackageItemVo> items = diagnosisPackageItemService.selectByPackageId(id);
      vo.setItems(items);
      if(!CollectionUtils.isEmpty(items)){
        StringBuilder names = new StringBuilder();
        BigDecimal price = new BigDecimal(0);
        for (DiagnosisPackageItemVo project : items) {
          price = price.add(project.getTotalPrice());
          names.append("," + project.getItemName());
        }
        vo.setPrice(price);
        vo.setItemNames(names.substring(1));
      }
    }
    return AjaxResult.success(vo);
  }

  /**
   * 新增诊疗套餐
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:package:add')")
  @Log(title = "诊疗套餐", businessType = BusinessType.INSERT)
  @PostMapping
  public AjaxResult add(@Valid @RequestBody DiagnosisPackageDto dto) {

    return toAjax(diagnosisPackageService.insertDiagnosisPackage(dto));
  }

  /**
   * 修改诊疗套餐
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:package:edit')")
  @Log(title = "诊疗套餐", businessType = BusinessType.UPDATE)
  @PutMapping
  public AjaxResult edit(@Valid @RequestBody DiagnosisPackageDto dto) {
    return toAjax(diagnosisPackageService.updateDiagnosisPackage(dto));
  }

  /**
   * 删除诊疗套餐
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:package:remove')")
  @Log(title = "诊疗套餐", businessType = BusinessType.DELETE)
  @DeleteMapping("/{ids}")
  public AjaxResult remove(@PathVariable Long[] ids) {
    return toAjax(diagnosisPackageService.deleteDiagnosisPackageByIds(ids));
  }
}
