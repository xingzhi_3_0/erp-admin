package com.xz.web.controller.message;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.SecurityUtils;
import com.xz.message.domain.SysTenantNotifyItem;
import com.xz.message.service.ISysTenantNotifyItemService;
import com.xz.message.vo.TempLateVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 消息模板配置Controller
 *
 * @author xz
 * @date 2023-08-04
 */
@RestController
@RequestMapping("/notify/notify")
public class SysTenantNotifyItemController extends BaseController {
    @Autowired
    private ISysTenantNotifyItemService sysTenantNotifyItemService;

    /**
     * 获取消息模板配置详细信息
     */
    @GetMapping(value = "/getInfo")
    public AjaxResult getInfo() {
        return AjaxResult.success(sysTenantNotifyItemService.selectSysTenantNotifyItemByTenantId(SecurityUtils.getLoginUser().getTenantId()));
    }

    /**
     * 获取消息模板配置详细信息
     */
    @GetMapping(value = "/findTypeList")
    public AjaxResult findTypeList() {
        return AjaxResult.success(sysTenantNotifyItemService.selectListByType());
    }

    /**
     * 修改消息模板配置
     */
    @Log(title = "消息模板配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody TempLateVO tempLateVO) {
        return toAjax(sysTenantNotifyItemService.updateSysTenantNotifyItem(tempLateVO));
    }
}
