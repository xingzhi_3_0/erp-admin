package com.xz.web.controller.product;

import java.util.ArrayList;
import java.util.List;

import com.xz.product.dto.ProductDto;
import com.xz.product.dto.ProductExportDto;
import com.xz.product.vo.ProductImportVo;
import com.xz.supplierContract.domain.SupplierContract;
import com.xz.supplierContract.service.ISupplierContractService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.product.domain.Product;
import com.xz.product.service.IProductService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 商品Controller
 *
 * @author xz
 * @date 2024-01-02
 */
@RestController
@RequestMapping("/product/product")
public class ProductController extends BaseController {
    @Autowired
    private IProductService productService;
    @Autowired
    private ISupplierContractService supplierContractService;
    /**
     * 查询商品列表
     */
    //@PreAuthorize("@ss.hasPermi('product:product:list')")
    @GetMapping("/list")
    public TableDataInfo list(Product product) {
        if(CollectionUtils.isEmpty(product.getStatusList())){
            product.setStatusList(new ArrayList<>());
        }

        // 兼容贸易中间商的商品选择的业务
        if(null != product.getEntSupplierId()){
          SupplierContract contract = supplierContractService.selectCurrentBySupplierId(product.getEntSupplierId());
          if(null == contract){
            return getDataTable(new ArrayList<>());
          }
          product.setEntContractId(contract.getId());
        }
        //开始查询商品列表
        startPage();
        List<ProductDto> list = productService.selectProductList(product);
        return getDataTable(list);
    }

  /**
   * 查询商品列表
   */
  //@PreAuthorize("@ss.hasPermi('product:product:list')")
  @GetMapping("/productList")
  public AjaxResult productList(Product product) {
    if(CollectionUtils.isEmpty(product.getStatusList())){
      product.setStatusList(new ArrayList<>());
    }
    //开始查询商品列表
    return AjaxResult.success(productService.selectProductList(product));
  }

    /**
     * 导出商品列表
     */
    //@PreAuthorize("@ss.hasPermi('product:product:export')")
    @Log(title = "商品", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Product product) {
        List<ProductExportDto> list = productService.selectProductExport(product);
        ExcelUtil<ProductExportDto> util = new ExcelUtil<ProductExportDto>(ProductExportDto.class);
        return util.exportExcel(list, "商品数据");
    }
    /**
     * 导入商品
     */
    @Log(title = "导入商品", businessType = BusinessType.EXPORT)
    @PostMapping("/import")
    public AjaxResult importExcel(@RequestParam("file") MultipartFile file) throws Exception {
        ExcelUtil<ProductImportVo> util = new ExcelUtil<ProductImportVo>(ProductImportVo.class);
        List<ProductImportVo> list = util.importExcel(file.getInputStream());
        AjaxResult message = productService.importExcel(list);
        return message;
    }
    /**
     * 获取商品详细信息
     */
    //@PreAuthorize("@ss.hasPermi('product:product:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(productService.selectProductById(id));
    }
    /**
     * 获取商品属性参数
     */
    //@PreAuthorize("@ss.hasPermi('product:product:query')")
    @GetMapping(value = "/getProductAttributeParamList/{id}")
    public AjaxResult getProductAttributeParamList(@PathVariable("id") Long id) {
        return AjaxResult.success(productService.getProductAttributeParamList(id));
    }

    /**
     * 新增商品
     */
    //@PreAuthorize("@ss.hasPermi('product:product:add')")
    @Log(title = "商品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody Product product) {
        return productService.insertProduct(product);
    }

    /**
     * 修改商品
     */
    //@PreAuthorize("@ss.hasPermi('product:product:edit')")
    @Log(title = "商品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody Product product) {
        return productService.updateProduct(product);
    }
    /**
     * 修改商品价格
     */
    //@PreAuthorize("@ss.hasPermi('product:product:edit')")
    @Log(title = "修改商品价格", businessType = BusinessType.UPDATE)
    @PutMapping("/updatePrice")
    public AjaxResult editPrice(@RequestBody Product product) {
        return productService.updatePrice(product);
    }

    /**
     * 删除商品
     */
    //@PreAuthorize("@ss.hasPermi('product:product:remove')")
    @Log(title = "商品", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(productService.deleteProductByIds(ids));
    }
    /**
     * 删除商品
     */
    //@PreAuthorize("@ss.hasPermi('product:product:remove')")
    @Log(title = "商品", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{id}")
    public AjaxResult delete(@PathVariable Long id) {
        return productService.deleteProductById(id);
    }
}
