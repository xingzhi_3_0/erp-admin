package com.xz.web.controller.warehouse;

import java.util.List;

import com.xz.warehouse.dto.WarehouseDto;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.warehouse.domain.Warehouse;
import com.xz.warehouse.service.IWarehouseService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 仓库Controller
 *
 * @author xz
 * @date 2024-01-04
 */
@RestController
@RequestMapping("/warehouse/warehouse")
public class WarehouseController extends BaseController {
    @Autowired
    private IWarehouseService warehouseService;

    /**
     * 查询仓库列表
     */
    @GetMapping("/list")
    public TableDataInfo list(Warehouse warehouse) {
        startPage();
        List<WarehouseDto> list = warehouseService.selectWarehouseList(warehouse);
        return getDataTable(list);
    }
    /**
     * 查询仓库列表
     */
    @GetMapping("/warehouseList")
    public AjaxResult warehouseList() {
        Warehouse warehouse =new Warehouse();
        warehouse.setStatus("0");
        return AjaxResult.success(warehouseService.selectWarehouseList(warehouse));
    }
    /**
     * 查询仓库列表--全部
     */
    @GetMapping("/allWarehouseList")
    public AjaxResult getAllWarehouseList() {
        Warehouse warehouse =new Warehouse();
        return AjaxResult.success(warehouseService.getAllWarehouseList(warehouse));
    }
    /**
     * 查询仓库列表--去重+权限
     */
    @GetMapping("/getWarehouseList")
    public AjaxResult getWarehouseList() {
        Warehouse warehouse =new Warehouse();
        warehouse.setStatus("0");
        return AjaxResult.success(warehouseService.getWarehouseList(warehouse));
    }

    /**
     * 导出仓库列表
     */
    @Log(title = "仓库", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Warehouse warehouse) {
        List<WarehouseDto> list = warehouseService.selectWarehouseList(warehouse);
        ExcelUtil<WarehouseDto> util = new ExcelUtil<WarehouseDto>(WarehouseDto.class);
        return util.exportExcel(list, "仓库数据");
    }

    /**
     * 获取仓库详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(warehouseService.selectWarehouseById(id));
    }

    /**
     * 新增仓库
     */
    @Log(title = "仓库", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody Warehouse warehouse) {
        return warehouseService.insertWarehouse(warehouse);
    }

    /**
     * 修改仓库
     */
    //@PreAuthorize("@ss.hasPermi('warehouse:warehouse:edit')")
    @Log(title = "仓库", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody Warehouse warehouse) {
        return warehouseService.updateWarehouse(warehouse);
    }

    /**
     * 删除仓库
     */
    @Log(title = "仓库", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(warehouseService.deleteWarehouseByIds(ids));
    }
    /**
     * 删除仓库
     */
    @Log(title = "仓库", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{id}")
    public AjaxResult delete(@PathVariable Long id) {
        return warehouseService.deleteWarehouseById(id);
    }
}
