package com.xz.web.controller.staging;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.SecurityUtils;
import com.xz.common.utils.StringUtils;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.product.domain.Product;
import com.xz.product.dto.ProductExportDto;
import com.xz.report.param.SalesReportParam;
import com.xz.report.service.ISalesStatisticsService;
import com.xz.report.vo.SalesFlowReportVo;
import com.xz.report.vo.SalesReportVo;
import com.xz.report.vo.SalesReturnReportVo;
import com.xz.staging.service.ISalesStagingService;
import com.xz.staging.vo.FoldLineStagingVo;
import com.xz.staging.vo.FrameLensesRangeVo;
import com.xz.staging.vo.FrameLensesStagingVo;
import com.xz.staging.vo.WorkbenchesVo;
import com.xz.system.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 销售单换货Controller
 * 
 * @author xz
 * @date 2024-02-05
 */
@RestController
@RequestMapping("/sales/staging")
public class SalesStagingController extends BaseController {
    @Autowired
    private ISalesStatisticsService salesStatisticsService;
    @Autowired
    private ISysRoleService sysRoleService;
    @Autowired
    private ISalesStagingService stagingService;

    /**
     * 查询销售报表数据
     */
    //@PreAuthorize("@ss.hasPermi('sales:order:list')")
    @GetMapping("/workbenches")
    public AjaxResult workbenches(SalesReportParam salesReportParam)
    {
        setQueryDept(salesReportParam);
        List<WorkbenchesVo> workbenchesVos = stagingService.queryData(salesReportParam);
        return AjaxResult.success(workbenchesVos);
    }



    /**
     * 查询销售报表数据
     */
    //@PreAuthorize("@ss.hasPermi('sales:order:list')")
    @GetMapping("/hotSalesProduct")
    public AjaxResult hotSalesProduct(SalesReportParam salesReportParam)
    {
        setQueryDept(salesReportParam);
        List<WorkbenchesVo> list = stagingService.hotSalesProduct(salesReportParam);
        return AjaxResult.success(list);
    }

    @GetMapping("/foldLineStatistics")
    public AjaxResult foldLineStatistics(SalesReportParam salesReportParam)
    {
        setQueryDept(salesReportParam);
        List<FoldLineStagingVo> list = stagingService.foldLineStatistics(salesReportParam);
        return AjaxResult.success(list);
    }

    @GetMapping("/foldLineMonthList")
    public TableDataInfo foldLineMonthList(SalesReportParam salesReportParam)
    {
        setQueryDept(salesReportParam);
        startPage();
        List<FoldLineStagingVo> list = stagingService.foldLineMonthList(salesReportParam);
        return getDataTable(list);
    }

    @GetMapping("/frameLensesList")
    public TableDataInfo frameLensesList(SalesReportParam salesReportParam)
    {
        setQueryDept(salesReportParam);
        startPage();
        List<FrameLensesStagingVo> frameLensesStagingVos = stagingService.frameLensesList(salesReportParam);
        return getDataTable(frameLensesStagingVos);
    }

    @GetMapping("/frameLensesRangeList")
    public TableDataInfo frameLensesRangeList(SalesReportParam salesReportParam)
    {
        setQueryDept(salesReportParam);
        List<FrameLensesStagingVo> frameLensesList = stagingService.findFrameLensesList(salesReportParam);
        startPage();
        salesReportParam.setFrameLensesList(frameLensesList);
        List<FrameLensesRangeVo> list = stagingService.frameLensesRangList(salesReportParam);
        return getDataTable(list);
    }

    /**
     * 导出商品列表
     */
    //@PreAuthorize("@ss.hasPermi('product:product:export')")
    @Log(title = "商品", businessType = BusinessType.EXPORT)
    @GetMapping("/exportFrameLensesRangeList")
    public AjaxResult exportFrameLensesRangeList(SalesReportParam salesReportParam) {
        setQueryDept(salesReportParam);
        List<FrameLensesStagingVo> frameLensesList = stagingService.findFrameLensesList(salesReportParam);
        salesReportParam.setFrameLensesList(frameLensesList);
        List<FrameLensesRangeVo> list = stagingService.frameLensesRangList(salesReportParam);
        ExcelUtil<FrameLensesRangeVo> util = new ExcelUtil<FrameLensesRangeVo>(FrameLensesRangeVo.class);
        String sheetName = "";
        if(salesReportParam.getRangeType().equals(1)){
            sheetName = "1000元以下售价订单数据";
        }else if(salesReportParam.getRangeType().equals(2)){
            sheetName = "1000元~2000元售价订单数据";
        }else if(salesReportParam.getRangeType().equals(3)){
            sheetName = "2000元~3000元售价订单数据";
        }else if(salesReportParam.getRangeType().equals(4)){
            sheetName = "3000元~4000元售价订单数据";
        }else if(salesReportParam.getRangeType().equals(5)){
            sheetName = "4000元~5000元售价订单数据";
        }else if(salesReportParam.getRangeType().equals(6)){
            sheetName = "5000元以上售价订单数据";
        }
        return util.exportExcel(list, sheetName);
    }


    private void setQueryDept(SalesReportParam salesReportParam){
        // 获取当前登录人可看到部门
        List<Long> list1 = sysRoleService.myQueryDeptId();
        if(list1 != null && list1.size()==0){
            // 0 看自己部门
            salesReportParam.setQueryDeptIdListStr(SecurityUtils.getLoginUser().getDeptId().toString());
        }else if(list1 != null && list1.size()>0){
            // 大于0  自定义
            salesReportParam.setQueryDeptIdListStr(StringUtils.join(list1,","));
        }
    }
}
