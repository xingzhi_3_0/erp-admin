package com.xz.web.controller.log;

import java.util.List;

import com.xz.log.dto.OperationLogDto;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.log.domain.OperationLog;
import com.xz.log.service.IOperationLogService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 操作记录Controller
 *
 * @author xz
 * @date 2024-01-08
 */
@RestController
@RequestMapping("/log/log")
public class OperationLogController extends BaseController {
    @Autowired
    private IOperationLogService operationLogService;

    /**
     * 查询操作记录列表
     */
    //@PreAuthorize("@ss.hasPermi('log:log:list')")
    @GetMapping("/list")
    public TableDataInfo list(OperationLog operationLog) {
        startPage();
        List<OperationLogDto> list = operationLogService.selectOperationLogList(operationLog);
        return getDataTable(list);
    }

    /**
     * 导出操作记录列表
     */
    //@PreAuthorize("@ss.hasPermi('log:log:export')")
    @Log(title = "操作记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OperationLog operationLog) {
        List<OperationLogDto> list = operationLogService.selectOperationLogList(operationLog);
        ExcelUtil<OperationLogDto> util = new ExcelUtil<OperationLogDto>(OperationLogDto.class);
        return util.exportExcel(list, "操作记录数据");
    }

    /**
     * 获取操作记录详细信息
     */
    //@PreAuthorize("@ss.hasPermi('log:log:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(operationLogService.selectOperationLogById(id));
    }

    /**
     * 新增操作记录
     */
    //@PreAuthorize("@ss.hasPermi('log:log:add')")
    @Log(title = "操作记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody OperationLog operationLog) {
        return toAjax(operationLogService.insertOperationLog(operationLog));
    }

    /**
     * 修改操作记录
     */
    //@PreAuthorize("@ss.hasPermi('log:log:edit')")
    @Log(title = "操作记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody OperationLog operationLog) {
        return toAjax(operationLogService.updateOperationLog(operationLog));
    }

    /**
     * 删除操作记录
     */
    //@PreAuthorize("@ss.hasPermi('log:log:remove')")
    @Log(title = "操作记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(operationLogService.deleteOperationLogByIds(ids));
    }
}
