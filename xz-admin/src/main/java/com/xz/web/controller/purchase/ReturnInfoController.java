package com.xz.web.controller.purchase;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.purchase.domain.Purchase;
import com.xz.purchase.domain.ReturnInfo;
import com.xz.purchase.dto.PurchaseDto;
import com.xz.purchase.param.ReturnInfoParam;
import com.xz.purchase.service.IPurchaseService;
import com.xz.purchase.service.IReturnInfoService;
import com.xz.purchase.vo.AdjustInfoNoFlowVo;
import com.xz.purchase.vo.BlushVo;
import com.xz.purchase.vo.ReturnInfoNoFlowVo;
import com.xz.purchase.vo.ReturnInfoRepeatFlowVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

/**
 * 采购退货Controller
 *
 * @author xz
 * @date 2024-01-06
 */
@RestController
@RequestMapping("/purchase/returnInfo")
public class ReturnInfoController extends BaseController {
    @Autowired
    private IPurchaseService purchaseService;
    @Autowired
    private IReturnInfoService returnInfoService;

    /**
     * 查询采购退货列表
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:list')")
    @GetMapping("/list")
    public TableDataInfo list(ReturnInfo returnInfo) {
        startPage();
        List<ReturnInfo> list = returnInfoService.queryList(returnInfo);
         return getDataTable(list);
    }

    /**
     * 导出采购列表
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:export')")
    @Log(title = "采购退货导出", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Purchase purchase) {
        List<PurchaseDto> list = purchaseService.selectPurchaseList(purchase);
        ExcelUtil<PurchaseDto> util = new ExcelUtil<PurchaseDto>(PurchaseDto.class);
        return util.exportExcel(list, "采购数据");
    }

    /**
     * 获取采购退货详细信息
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(returnInfoService.returnDetailById(id));
    }

    /**
     * 新增采购退货
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:add')")
    @Log(title = "采购退货", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody ReturnInfoParam param) {
        return returnInfoService.addReturnInfo(param);
    }

    /**
     * 修改采购退货
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:edit')")
    @Log(title = "采购退货", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody ReturnInfoParam param) {
        return returnInfoService.updateReturnInfo(param);
    }

    /**
     * 修改采购退货
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:edit')")
    @Log(title = "采购退货状态修改", businessType = BusinessType.UPDATE)
    @PostMapping("/editStatus")
    public AjaxResult editStatus(@Valid @RequestBody ReturnInfo returnInfo) {
        return returnInfoService.editStatus(returnInfo);
    }

    /**
     * 冲红
     */
    @PostMapping("/redInkEntry")
    public AjaxResult redInkEntry(@RequestBody ReturnInfo returnInfo) {
        return returnInfoService.redInkEntry(returnInfo);
    }

    /**
     * 导入会员信息
     */
    @Log(title = "导入患者信息", businessType = BusinessType.EXPORT)
    @PostMapping("/importNoFlowExcel")
    public AjaxResult importNoFlowExcel(@RequestParam("file") MultipartFile file) throws Exception {
        ExcelUtil<ReturnInfoNoFlowVo> util = new ExcelUtil<ReturnInfoNoFlowVo>(ReturnInfoNoFlowVo.class);
        List<ReturnInfoNoFlowVo> list = util.importExcel("Sheet1",file.getInputStream(),0);
        AjaxResult message = returnInfoService.importNoFlowExcel(list);
        return message;
    }

    /**
     * 导入会员信息
     */
    @Log(title = "导入患者信息", businessType = BusinessType.EXPORT)
    @PostMapping("/importRepeatFlowExcel")
    public AjaxResult importRepeatFlowExcel(@RequestParam("file") MultipartFile file) throws Exception {
        ExcelUtil<ReturnInfoRepeatFlowVo> util = new ExcelUtil<ReturnInfoRepeatFlowVo>(ReturnInfoRepeatFlowVo.class);
        List<ReturnInfoRepeatFlowVo> list = util.importExcel("Sheet1",file.getInputStream(),0);
        AjaxResult message = returnInfoService.importRepeatFlowExcel(list);
        return message;
    }

    /**
     * 调整库存数量
     */
    @Log(title = "调整库存数量", businessType = BusinessType.EXPORT)
    @PostMapping("/importKcExcel")
    public AjaxResult importKcExcel(@RequestParam("file") MultipartFile file) throws Exception {
        ExcelUtil<ReturnInfoNoFlowVo> util = new ExcelUtil<ReturnInfoNoFlowVo>(ReturnInfoNoFlowVo.class);
        List<ReturnInfoNoFlowVo> list = util.importExcel("Sheet1",file.getInputStream(),0);
        AjaxResult message = returnInfoService.importKcExcel(list);
        return message;
    }

}
