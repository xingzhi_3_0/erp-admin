package com.xz.web.controller.member;

import java.util.List;

import com.xz.member.vo.MemberPatientVo;
import com.xz.patient.service.IPatientInfoService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;
import javax.validation.Valid;
import com.xz.member.domain.MemberPatient;
import com.xz.member.service.IMemberPatientService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 会员患者关联信息Controller
 * 
 * @author xz
 * @date 2024-01-31
 */
@RestController
@RequestMapping("/member/patient")
public class MemberPatientController extends BaseController {
    @Autowired
    private IMemberPatientService memberPatientService;


    /**
     * 查询会员患者关联信息列表
     */
    //@PreAuthorize("@ss.hasPermi('patient:patient:list')")
    @GetMapping("/bindList")
    public AjaxResult list(MemberPatient memberPatient)
    {
        List<MemberPatientVo> list = memberPatientService.queryList(memberPatient);
        return AjaxResult.success(list);
    }

    /**
     * 绑定患者
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:add')")
    @Log(title = "会员患者关联信息", businessType = BusinessType.INSERT)
    @PostMapping("/binding")
    public AjaxResult binding(@Valid @RequestBody MemberPatient memberPatient) {
        return memberPatientService.insertMemberPatient(memberPatient);
    }

    /**
     * 绑定患者
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:add')")
    @Log(title = "会员患者关联信息", businessType = BusinessType.INSERT)
    @PostMapping("/unbind")
    public AjaxResult unbind(@Valid @RequestBody MemberPatient memberPatient) {
        return memberPatientService.unbind(memberPatient);
    }

    /**
     * 导出会员患者关联信息列表
     */
    //@PreAuthorize("@ss.hasPermi('patient:patient:export')")
    @Log(title = "会员患者关联信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(MemberPatient memberPatient)
    {
        List<MemberPatient> list = memberPatientService.selectMemberPatientList(memberPatient);
        ExcelUtil<MemberPatient> util = new ExcelUtil<MemberPatient>(MemberPatient.class);
        return util.exportExcel(list, "会员患者关联信息数据");
    }

    /**
     * 获取会员患者关联信息详细信息
     */
    //@PreAuthorize("@ss.hasPermi('patient:patient:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(memberPatientService.selectMemberPatientById(id));
    }

//    /**
//     * 新增会员患者关联信息
//     */
//    //@PreAuthorize("@ss.hasPermi('patient:patient:add')")
//    @Log(title = "会员患者关联信息", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@Valid @RequestBody MemberPatient memberPatient)
//    {
//        return toAjax(memberPatientService.insertMemberPatient(memberPatient));
//    }

    /**
     * 修改会员患者关联信息
     */
    //@PreAuthorize("@ss.hasPermi('patient:patient:edit')")
    @Log(title = "会员患者关联信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody MemberPatient memberPatient)
    {
        return toAjax(memberPatientService.updateMemberPatient(memberPatient));
    }

    /**
     * 删除会员患者关联信息
     */
    //@PreAuthorize("@ss.hasPermi('patient:patient:remove')")
    @Log(title = "会员患者关联信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(memberPatientService.deleteMemberPatientByIds(ids));
    }
}
