package com.xz.web.controller.member;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.member.domain.MemberInfo;
import com.xz.member.domain.WalletRecord;
import com.xz.member.service.IMemberInfoService;
import com.xz.member.service.IWalletRecordService;
import com.xz.member.vo.WalletRecordListVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

/**
 * 钱包记录Controller
 *
 * @author xz
 * @date 2024-1-24 17:09:37
 */
@RestController
@RequestMapping("/member/walletRecord")
public class WalletRecordController extends BaseController {

	@Autowired
	private IMemberInfoService memberInfoService;
	@Autowired
	private IWalletRecordService walletRecordService;

	/**
	 * 获取会员钱包记录
	 */
	//@PreAuthorize("@ss.hasPermi('purchase:purchase:query')")
	@GetMapping("/listByMemberId")
	public TableDataInfo listByMemberId(WalletRecord walletRecord) {
		MemberInfo memberInfo = memberInfoService.getById(walletRecord.getMemberId());
		walletRecord.setMemberCardNo(memberInfo.getMemberCardNo());
		startPage();
		List<WalletRecord> walletRecords = walletRecordService.listByMemberId(walletRecord);
		return getDataTable(walletRecords);
	}

	/**
	 * 获取充值记录
	 */
	//@PreAuthorize("@ss.hasPermi('purchase:purchase:query')")
	@GetMapping("/recharge/list")
	public TableDataInfo list(WalletRecord walletRecord) {
		// 1-充值. 2.钱包退款
		//walletRecord.setChangeTypeList(Arrays.asList(1, 2));
		startPage();
		List<WalletRecordListVo> walletRecords = walletRecordService.queryVoList(walletRecord);
		return getDataTable(walletRecords);
	}

	/**
	 * 获取充值记录导出
	 */
	//@PreAuthorize("@ss.hasPermi('purchase:purchase:query')")
	@GetMapping("/recharge/export")
	public AjaxResult listExport(WalletRecord walletRecord) {
		// 1-充值. 2.钱包退款
        //walletRecord.setChangeTypeList(Arrays.asList(1, 2));
		List<WalletRecordListVo> list = walletRecordService.queryVoList(walletRecord);
		ExcelUtil<WalletRecordListVo> excelUtil = new ExcelUtil<WalletRecordListVo>(WalletRecordListVo.class);
		return excelUtil.exportExcel(list, "充值记录");
	}

	/**
	 * 获取会员钱包余额
	 */
	//@PreAuthorize("@ss.hasPermi('purchase:purchase:query')")
	@GetMapping(value = "/memberWallet")
	public AjaxResult memberWallet(WalletRecord walletRecord) {
		return walletRecordService.memberWallet(walletRecord);
	}

	/**
	 * 钱包充值/退款
	 */
	//@PreAuthorize("@ss.hasPermi('purchase:purchase:add')")
	@Log(title = "会员钱包", businessType = BusinessType.INSERT)
	@PostMapping("/addWalletRecord")
	public AjaxResult addWalletRecord(@Valid @RequestBody WalletRecord walletRecord) {
		return walletRecordService.addRecord(walletRecord);
	}

	/**
	 * 作废钱包记录
	 */
	//@PreAuthorize("@ss.hasPermi('purchase:purchase:add')")
	@Log(title = "会员钱包", businessType = BusinessType.UPDATE)
	@PostMapping("/cancelWalletRecord")
	public AjaxResult cancelWalletRecord(@Valid @RequestBody WalletRecord walletRecord) {
		return walletRecordService.cancelWalletRecord(walletRecord);
	}

}
