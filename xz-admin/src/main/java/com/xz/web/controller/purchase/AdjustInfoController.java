package com.xz.web.controller.purchase;

import com.xz.audit.domain.AuditProcess;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.purchase.domain.AdjustInfo;
import com.xz.purchase.domain.Purchase;
import com.xz.purchase.param.AdjustInfoParam;
import com.xz.purchase.service.IAdjustInfoService;
import com.xz.purchase.service.IReturnInfoService;
import com.xz.purchase.vo.AdjustInfoNoFlowVo;
import com.xz.purchase.vo.AdjustOutVo;
import com.xz.sales.util.SalesExcelUtils;
import com.xz.sales.vo.SalesNoFlowVo;
import com.xz.sales.vo.SalesOrderExportBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * 采购调整Controller
 *
 * @author xz
 * @date 2024-1-16
 */
@RestController
@RequestMapping("/purchase/adjustInfo")
public class AdjustInfoController extends BaseController {
    @Autowired
    private IAdjustInfoService adjustInfoService;

    /**
     * 查询采购调整列表
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:list')")
    @GetMapping("/list")
    public TableDataInfo list(AdjustInfo adjustInfo) {
        startPage();
        List<AdjustInfo> list = adjustInfoService.queryList(adjustInfo);
        return getDataTable(list);
    }

    /**
     * 获取采购库存调整详细信息
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(adjustInfoService.adjustDetailById(id));
    }

    /**
     * 新增采购库存调整
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:add')")
    @Log(title = "采购库存调整", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody AdjustInfoParam param) {
        return adjustInfoService.addAdjustInfo(param);
    }

    /**
     * 修改采购退货
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:edit')")
    @Log(title = "采购库存调整", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody AdjustInfoParam param) {
        return adjustInfoService.updateAdjustInfo(param);
    }

    /**
     * 修改采购退货
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:edit')")
    @Log(title = "采购退货状态修改", businessType = BusinessType.UPDATE)
    @PostMapping("/editStatus")
    public AjaxResult editStatus(@Valid @RequestBody AdjustInfo adjustInfo) {
        return adjustInfoService.editStatus(adjustInfo);
    }

//    /**
//     * 冲红
//     */
//    @PostMapping("/redInkEntry")
//    public AjaxResult redInkEntry(@RequestBody ReturnInfo returnInfo) {
//        return returnInfoService.redInkEntry(returnInfo);
//    }


    /**
     * 导入会员信息
     */
    @Log(title = "导入患者信息", businessType = BusinessType.EXPORT)
    @PostMapping("/importNoFlowExcel")
    public AjaxResult importNoFlowExcel(@RequestParam("file") MultipartFile file) throws Exception {
        ExcelUtil<AdjustInfoNoFlowVo> util = new ExcelUtil<AdjustInfoNoFlowVo>(AdjustInfoNoFlowVo.class);
        List<AdjustInfoNoFlowVo> list = util.importExcel("调整出库无流水",file.getInputStream(),0);
        AjaxResult message = adjustInfoService.importNoFlowExcel(list);
        return message;
    }

    /**
     * 导入新增其他出库
     */
    @Log(title = "导入患者信息", businessType = BusinessType.EXPORT)
    @PostMapping("/importAddOutAdjustExcel")
    public AjaxResult importAddOutAdjustExcel(@RequestParam("file") MultipartFile file) throws Exception {
        ExcelUtil<AdjustOutVo> util = new ExcelUtil<AdjustOutVo>(AdjustOutVo.class);
        List<AdjustOutVo> list = util.importExcel("Sheet1",file.getInputStream(),0);
        AjaxResult message = adjustInfoService.importAddOutAdjustExcel(list);
        return message;
    }

    /**
     * 导入新增其他出库
     */
    @Log(title = "导入患者信息", businessType = BusinessType.EXPORT)
    @GetMapping("/importAddOutAdjustExport")
    public AjaxResult importAddOutAdjustExport(@RequestParam("file") MultipartFile file, HttpServletResponse response) throws Exception {
        ExcelUtil<AdjustOutVo> util = new ExcelUtil<AdjustOutVo>(AdjustOutVo.class);
        List<AdjustOutVo> list = util.importExcel("Sheet1",file.getInputStream(),0);
        AjaxResult message = adjustInfoService.importAddOutAdjustExport(list);
        List<AdjustOutVo> errorList = (List<AdjustOutVo>) message.get("data");
        String fileName = "其他错误数据_" + System.currentTimeMillis() ;
        return util.exportExcel(errorList, fileName);
    }


    /**
     * 导入会员信息
     */
    @Log(title = "导入患者信息", businessType = BusinessType.EXPORT)
    @GetMapping("/importAddOutMatchExport")
    public AjaxResult importAddOutMatchExport(@RequestParam("file") MultipartFile file, HttpServletResponse response) throws Exception {
        ExcelUtil<AdjustOutVo> util = new ExcelUtil<AdjustOutVo>(AdjustOutVo.class);
        List<AdjustOutVo> list = util.importExcel("Sheet1",file.getInputStream(),0);
        AjaxResult message = adjustInfoService.importAddOutMatchExport(list);
        List<AdjustOutVo> errorList = (List<AdjustOutVo>) message.get("data");
        String fileName = "镜片错误数据_" + System.currentTimeMillis() ;
        return util.exportExcel(errorList, fileName);

    }
}
