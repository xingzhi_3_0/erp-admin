package com.xz.web.controller.supplier;

import java.util.List;

import com.xz.supplier.dto.SupplierListDto;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.supplier.domain.Supplier;
import com.xz.supplier.service.ISupplierService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 供应商、企业客户Controller
 *
 * @author xz
 * @date 2024-01-04
 */
@RestController
@RequestMapping("/supplier/supplier")
public class SupplierController extends BaseController {
    @Autowired
    private ISupplierService supplierService;

    /**
     * 查询供应商、企业客户列表
     */
    //@PreAuthorize("@ss.hasPermi('supplier:supplier:list')")
    @GetMapping("/list")
    public TableDataInfo list(Supplier supplier) {
        startPage();
        List<SupplierListDto> list = supplierService.selectSupplierList(supplier);
        return getDataTable(list);
    }
    /**
     * 查询供应商、企业客户列表
     */
    //@PreAuthorize("@ss.hasPermi('supplier:supplier:list')")
    @GetMapping("/supplierList")
    public AjaxResult supplierList(Supplier supplier) {
       if(null == supplier.getIsEnterprise()){
          supplier.setUnitProperty(1); // 如果没传isEnterprise，说明只查供应商数据
        }
        supplier.setStatus("0");
        return AjaxResult.success(supplierService.selectSupplierList(supplier));
    }
    /**
     * 导出供应商、企业客户列表
     */
    //@PreAuthorize("@ss.hasPermi('supplier:supplier:export')")
    @Log(title = "供应商、企业客户", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Supplier supplier) {
        List<SupplierListDto> list = supplierService.selectSupplierList(supplier);
        ExcelUtil<SupplierListDto> util = new ExcelUtil<SupplierListDto>(SupplierListDto.class);
        return util.exportExcel(list, "供应商、企业客户数据");
    }

    /**
     * 获取供应商、企业客户详细信息
     */
    //@PreAuthorize("@ss.hasPermi('supplier:supplier:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(supplierService.selectSupplierById(id));
    }

    /**
     * 新增供应商、企业客户
     */
    //@PreAuthorize("@ss.hasPermi('supplier:supplier:add')")
    @Log(title = "供应商、企业客户", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody Supplier supplier) {
        return supplierService.insertSupplier(supplier);
    }

    /**
     * 修改供应商、企业客户
     */
    //@PreAuthorize("@ss.hasPermi('supplier:supplier:edit')")
    @Log(title = "供应商、企业客户", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody Supplier supplier) {
        return supplierService.updateSupplier(supplier);
    }

    /**
     * 删除供应商、企业客户
     */
    //@PreAuthorize("@ss.hasPermi('supplier:supplier:remove')")
    @Log(title = "供应商、企业客户", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(supplierService.deleteSupplierByIds(ids));
    }
    /**
     * 删除供应商、企业客户
     */
    //@PreAuthorize("@ss.hasPermi('supplier:supplier:remove')")
    @Log(title = "供应商、企业客户", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{id}")
    public AjaxResult delete(@PathVariable Long id) {
        return toAjax(supplierService.deleteSupplierById(id));
    }
}
