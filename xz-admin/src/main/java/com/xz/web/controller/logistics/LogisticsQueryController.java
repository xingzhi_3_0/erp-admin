package com.xz.web.controller.logistics;

import com.kuaidi100.sdk.request.QueryTrackParam;
import com.kuaidi100.sdk.response.QueryTrackMapResp;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.StringUtils;
import com.xz.logistics.domain.Logistics;
import com.xz.logistics.dto.LogisticsDto;
import com.xz.logistics.service.ILogisticsQueryService;
import com.xz.logistics.service.ILogisticsService;
import com.xz.sales.domain.SalesOrder;
import com.xz.sales.service.ISalesOrderService;
import com.xz.supplierOrder.domain.SupplierOrder;
import com.xz.supplierOrder.enums.SupplierOrderStatusEnum;
import com.xz.supplierOrder.service.ISupplierOrderService;
import org.eclipse.jetty.util.StringUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 物流Controller
 *
 * @author xz
 * @date 2024-1-16
 */
@RestController
@RequestMapping("/logisticsQuery")
public class LogisticsQueryController extends BaseController {

    @Autowired
    private ILogisticsService logisticsService;
    @Autowired
    private ISupplierOrderService supplierOrderService;
    @Autowired
    private ISalesOrderService salesOrderService;
    @Autowired
    private ILogisticsQueryService logisticsQueryService;

    /**
     * 查询贸易中间商快递信息
     * @param id
     */
    @GetMapping(value = "/getById")
    public AjaxResult queryLogistics(Long id) {
        if(null == id){
            return AjaxResult.error("ID参数异常");
        }
        //查询订单
        Logistics logistics = logisticsService.getById(id);
        if(null == logistics){
            return AjaxResult.error("物流不存在或已删除");
        }
        return getResult(logistics);
    }

    /**
     * 查询贸易中间商快递信息
     * @param businessNo
     */
    @GetMapping(value = "/getByNo")
    public AjaxResult queryOrderLogisticsByNo(String businessNo) {
        if(StringUtils.isEmpty(businessNo)){
            return AjaxResult.error("编号参数异常");
        }
        //查询物流
        List<Logistics> logistics = logisticsService.selectByBusinessNo(businessNo);
        if(CollectionUtils.isEmpty(logistics)){
            return AjaxResult.error("物流不存在或已删除");
        }
        //目前只有一个物流，所以拿第一个数据即可
        return getResult(logistics.get(0));
    }

    private AjaxResult getResult(Logistics logistics) {
        //查询物流
        QueryTrackParam param = new QueryTrackParam();
        param.setCom(logistics.getExpressDeliveryCompanies());//快递公司的编码
        param.setNum(logistics.getExpressTrackingNumber());//快递单号
        param.setPhone(logistics.getExpressSenderPhone());//寄件人的电话号码,顺丰的手机号码不能为null；
        param.setTo(logistics.getAddress());
        // 查询快递信息
        return logisticsQueryService.queryLogisticsByCaptcha(param);
    }

    /**
     * 查询贸易中间商订单快递信息
     * @param id
     */
    @GetMapping(value = "/supplier/getById")
    public AjaxResult querySupplierOrderLogistics(Long id) {
        if(null == id){
            return AjaxResult.error("ID参数异常");
        }
        //查询订单
        SupplierOrder order = supplierOrderService.selectSupplierOrderById(id);
        if(null == order){
            return AjaxResult.error("订单不存在或已删除");
        }
        //待提交,已废除的订单无法查快递
        if (null == order.getOrderStatus()
                || SupplierOrderStatusEnum.TO_BE_DELIVERY.getStatus() == order.getOrderStatus()
                || SupplierOrderStatusEnum.NULLIFIED.getStatus() == order.getOrderStatus()
        ) {
            return AjaxResult.success(new QueryTrackMapResp());
        }

        //查询物流
        QueryTrackParam param = new QueryTrackParam();
        param.setCom(order.getExpressDeliveryCompanies());//快递公司的编码
        param.setNum(order.getExpressTrackingNumber());//快递单号
        param.setPhone(order.getExpressSenderPhone());//寄件人的电话号码,顺丰的手机号码不能为null；
        param.setTo(order.getAddress());
        // 查询快递信息
        return logisticsQueryService.queryLogisticsByCaptcha(param);
    }

    /**
     * 查询销售订单快递信息
     * @param id
     */
    @GetMapping(value = "/order/getById")
    public AjaxResult queryOrderLogistics(Long id) {
        if(null == id){
            return AjaxResult.error("ID参数异常");
        }
        //查询订单
        SalesOrder order = salesOrderService.getById(id);
        if(null == order){
            return AjaxResult.error("订单不存在或已删除");
        }
        //查询物流
        List<Logistics> logistics = logisticsService.selectByBusinessNo(order.getSalesNo());
        if(CollectionUtils.isEmpty(logistics)){
            return AjaxResult.error("物流不存在或已删除");
        }
        //目前只有一个物流，所以传0，即可
        return getResult(logistics.get(0));
    }
}
