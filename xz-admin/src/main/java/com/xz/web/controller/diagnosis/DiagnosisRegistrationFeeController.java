package com.xz.web.controller.diagnosis;

import java.util.List;

import com.xz.diagnosis.domain.DiagnosisTime;
import com.xz.diagnosis.dto.DiagnosisRegistrationFeeDto;
import com.xz.diagnosis.vo.DiagnosisRegistrationFeeTreeVo;
import com.xz.diagnosis.vo.DiagnosisRegistrationFeeVo;
import com.xz.diagnosis.vo.DiagnosisTimeTreeVo;
import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.diagnosis.domain.DiagnosisRegistrationFee;
import com.xz.diagnosis.service.IDiagnosisRegistrationFeeService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 诊疗套餐Controller
 *
 * @author xz
 * @date 2024-03-17
 */
@RestController
@RequestMapping("/diagnosis/registration/fee")
public class DiagnosisRegistrationFeeController extends BaseController {
  @Autowired
  private IDiagnosisRegistrationFeeService diagnosisRegistrationFeeService;

  /**
   * 查询诊疗套餐列表
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:fee:list')")
  @GetMapping("/list")
  public TableDataInfo list(DiagnosisRegistrationFee diagnosisRegistrationFee) {
    startPage();
    List<DiagnosisRegistrationFeeVo> list = diagnosisRegistrationFeeService.selectDiagnosisRegistrationFeeVoList(diagnosisRegistrationFee);
    return getDataTable(list);
  }

  /**
   * 导出诊疗套餐列表
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:fee:export')")
  @Log(title = "诊疗套餐", businessType = BusinessType.EXPORT)
  @GetMapping("/export")
  public AjaxResult export(DiagnosisRegistrationFee diagnosisRegistrationFee) {
    List<DiagnosisRegistrationFee> list = diagnosisRegistrationFeeService.selectDiagnosisRegistrationFeeList(diagnosisRegistrationFee);
    ExcelUtil<DiagnosisRegistrationFee> util = new ExcelUtil<DiagnosisRegistrationFee>(DiagnosisRegistrationFee.class);
    return util.exportExcel(list, "诊疗套餐数据");
  }

  /**
   * 获取诊疗套餐详细信息
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:fee:query')")
  @GetMapping(value = "/{id}")
  public AjaxResult getInfo(@PathVariable("id") Long id) {
    DiagnosisRegistrationFee entity = diagnosisRegistrationFeeService.selectDiagnosisRegistrationFeeById(id);
    DiagnosisRegistrationFeeVo vo = new DiagnosisRegistrationFeeVo();
    BeanUtils.copyProperties(entity,vo);
    return AjaxResult.success(vo);
  }

  /**
   * 新增诊疗套餐
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:fee:add')")
  @Log(title = "诊疗套餐", businessType = BusinessType.INSERT)
  @PostMapping
  public AjaxResult add(@Valid @RequestBody DiagnosisRegistrationFeeDto diagnosisRegistrationFee) {
    return toAjax(diagnosisRegistrationFeeService.insertDiagnosisRegistrationFee(diagnosisRegistrationFee));
  }

  /**
   * 修改诊疗套餐
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:fee:edit')")
  @Log(title = "诊疗套餐", businessType = BusinessType.UPDATE)
  @PutMapping
  public AjaxResult edit(@Valid @RequestBody DiagnosisRegistrationFeeDto diagnosisRegistrationFee) {
    return toAjax(diagnosisRegistrationFeeService.updateDiagnosisRegistrationFee(diagnosisRegistrationFee));
  }

  /**
   * 删除诊疗套餐
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:fee:remove')")
  @Log(title = "诊疗套餐", businessType = BusinessType.DELETE)
  @DeleteMapping("/{ids}")
  public AjaxResult remove(@PathVariable Long[] ids) {
    return toAjax(diagnosisRegistrationFeeService.deleteDiagnosisRegistrationFeeByIds(ids));
  }

  /**
   * 查询诊疗时间项列表
   */
  @GetMapping("tree/list")
  public AjaxResult treeList(DiagnosisRegistrationFee fee) {
    List<DiagnosisRegistrationFeeTreeVo> vo = diagnosisRegistrationFeeService.treeList(fee);
    return AjaxResult.success(vo);
  }
}
