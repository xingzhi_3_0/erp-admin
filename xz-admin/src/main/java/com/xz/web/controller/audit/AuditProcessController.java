package com.xz.web.controller.audit;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.audit.domain.AuditProcess;
import com.xz.audit.service.IAuditProcessService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 流程定义Controller
 *
 * @author xz
 * @date 2024-01-09
 */
@RestController
@RequestMapping("/audit/process")
public class AuditProcessController extends BaseController {
    @Autowired
    private IAuditProcessService auditProcessService;

    /**
     * 查询流程定义列表
     */
    //@PreAuthorize("@ss.hasPermi('audit:process:list')")
    @GetMapping("/list")
    public TableDataInfo list(AuditProcess auditProcess) {
        startPage();
        auditProcess.setBizType(2);
        List<AuditProcess> list = auditProcessService.selectAuditProcessList(auditProcess);
        return getDataTable(list);
    }

    /**
     * 导出流程定义列表
     */
    //@PreAuthorize("@ss.hasPermi('audit:process:export')")
    @Log(title = "流程定义", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AuditProcess auditProcess) {
        List<AuditProcess> list = auditProcessService.selectAuditProcessList(auditProcess);
        ExcelUtil<AuditProcess> util = new ExcelUtil<AuditProcess>(AuditProcess.class);
        return util.exportExcel(list, "流程定义数据");
    }

    /**
     * 获取流程定义详细信息
     */
    //@PreAuthorize("@ss.hasPermi('audit:process:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(auditProcessService.selectAuditProcessById(id));
    }

    /**
     * 新增流程定义
     */
    //@PreAuthorize("@ss.hasPermi('audit:process:add')")
    @Log(title = "流程定义", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody AuditProcess auditProcess) {
        return toAjax(auditProcessService.insertAuditProcess(auditProcess));
    }

    /**
     * 修改流程定义
     */
    //@PreAuthorize("@ss.hasPermi('audit:process:edit')")
    @Log(title = "流程定义", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody AuditProcess auditProcess) {
        return toAjax(auditProcessService.updateAuditProcess(auditProcess));
    }

    /**
     * 删除流程定义
     */
    //@PreAuthorize("@ss.hasPermi('audit:process:remove')")
    @Log(title = "流程定义", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(auditProcessService.deleteAuditProcessByIds(ids));
    }
}
