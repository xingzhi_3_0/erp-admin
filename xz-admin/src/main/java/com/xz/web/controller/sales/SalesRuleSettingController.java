package com.xz.web.controller.sales;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.sales.domain.SalesExchange;
import com.xz.sales.domain.SalesRuleSetting;
import com.xz.sales.dto.SalesRuleSettingDTO;
import com.xz.sales.service.ISalesExchangeService;
import com.xz.sales.service.ISalesRuleSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 销售单换货Controller
 * 
 * @author xz
 * @date 2024-02-05
 */
@RestController
@RequestMapping("/sales/ruleSetting")
public class SalesRuleSettingController extends BaseController {
    @Autowired
    private ISalesRuleSettingService salesRuleSettingService;

    /**
     * 查询销售单设置信息
     */
    //@PreAuthorize("@ss.hasPermi('sales:exchange:list')")
    @GetMapping("/queryInfo")
    public AjaxResult queryInfo()
    {
        return salesRuleSettingService.queryInfo();
    }


    /**
     * 保存售单设置信息
     */
    //@PreAuthorize("@ss.hasPermi('sales:exchange:add')")
    @Log(title = "销售单换货", businessType = BusinessType.INSERT)
    @PostMapping("/saveInfo")
    public AjaxResult saveInfo(@Valid @RequestBody SalesRuleSettingDTO salesRuleSettingDTO)
    {
        return salesRuleSettingService.saveInfo(salesRuleSettingDTO);
    }

}
