package com.xz.web.controller.product;

import java.util.List;

import com.xz.product.vo.ProductAttributeParamVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.product.domain.ProductAttributeParam;
import com.xz.product.service.IProductAttributeParamService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 商品属性参数Controller
 *
 * @author xz
 * @date 2024-01-02
 */
@RestController
@RequestMapping("/product/param")
public class ProductAttributeParamController extends BaseController {
    @Autowired
    private IProductAttributeParamService productAttributeParamService;

    /**
     * 查询商品属性参数列表
     */
    //@PreAuthorize("@ss.hasPermi('product:param:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProductAttributeParam productAttributeParam) {
        startPage();
        List<ProductAttributeParam> list = productAttributeParamService.selectProductAttributeParamList(productAttributeParam);
        return getDataTable(list);
    }

    /**
     * 导出商品属性参数列表
     */
    //@PreAuthorize("@ss.hasPermi('product:param:export')")
    @Log(title = "商品属性参数", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ProductAttributeParam productAttributeParam) {
        List<ProductAttributeParam> list = productAttributeParamService.selectProductAttributeParamList(productAttributeParam);
        ExcelUtil<ProductAttributeParam> util = new ExcelUtil<ProductAttributeParam>(ProductAttributeParam.class);
        return util.exportExcel(list, "商品属性参数数据");
    }

    /**
     * 获取商品属性参数详细信息
     */
    //@PreAuthorize("@ss.hasPermi('product:param:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(productAttributeParamService.selectProductAttributeParamById(id));
    }

    /**
     * 新增商品属性参数
     */
    //@PreAuthorize("@ss.hasPermi('product:param:add')")
    @Log(title = "商品属性参数", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody ProductAttributeParamVo productAttributeParam) {
        return productAttributeParamService.insertProductAttributeParam(productAttributeParam);
    }

    /**
     * 修改商品属性参数
     */
    //@PreAuthorize("@ss.hasPermi('product:param:edit')")
    @Log(title = "商品属性参数", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody ProductAttributeParam productAttributeParam) {
        return toAjax(productAttributeParamService.updateProductAttributeParam(productAttributeParam));
    }

    /**
     * 删除商品属性参数
     */
    //@PreAuthorize("@ss.hasPermi('product:param:remove')")
    @Log(title = "商品属性参数", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(productAttributeParamService.deleteProductAttributeParamByIds(ids));
    }
}
