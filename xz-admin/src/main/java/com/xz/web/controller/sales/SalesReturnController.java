package com.xz.web.controller.sales;

import java.util.List;

import com.xz.sales.param.SalesReturnParam;
import com.xz.sales.vo.SalesNoFlowVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;
import javax.validation.Valid;
import com.xz.sales.domain.SalesReturn;
import com.xz.sales.service.ISalesReturnService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 销售单退货Controller
 * 
 * @author xz
 * @date 2024-02-05
 */
@RestController
@RequestMapping("/sales/return")
public class SalesReturnController extends BaseController {
    @Autowired
    private ISalesReturnService salesReturnService;

    /**
     * 查询销售单退货列表
     */
    //@PreAuthorize("@ss.hasPermi('sales:return:list')")
    @GetMapping("/list")
    public TableDataInfo list(SalesReturn salesReturn)
    {
        startPage();
        List<SalesReturn> list = salesReturnService.selectSalesReturnList(salesReturn);
        return getDataTable(list);
    }

    /**
     * 导出销售单退货列表
     */
    //@PreAuthorize("@ss.hasPermi('sales:return:export')")
    @Log(title = "销售单退货", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SalesReturn salesReturn)
    {
        List<SalesReturn> list = salesReturnService.selectSalesReturnList(salesReturn);
        ExcelUtil<SalesReturn> util = new ExcelUtil<SalesReturn>(SalesReturn.class);
        return util.exportExcel(list, "销售单退货数据");
    }

    /**
     * 获取销售单退货详细信息
     */
    //@PreAuthorize("@ss.hasPermi('sales:return:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(salesReturnService.selectSalesReturnById(id));
    }

    /**
     * 新增销售单退货
     */
    //@PreAuthorize("@ss.hasPermi('sales:return:add')")
    @Log(title = "销售单退货", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody SalesReturnParam returnParam)
    {
        return salesReturnService.insertSalesReturn(returnParam);
    }


    /**
     * 取消销售单退货
     */
    //@PreAuthorize("@ss.hasPermi('sales:return:add')")
    @Log(title = "销售单退货", businessType = BusinessType.UPDATE)
    @PostMapping("/cancelSalesReturn")
    public AjaxResult cancelSalesReturn(@Valid @RequestBody SalesReturnParam returnParam)
    {
        return salesReturnService.cancelSalesReturn(returnParam);
    }


    /**
     * 取消销售单退货
     */
    //@PreAuthorize("@ss.hasPermi('sales:return:add')")
    @Log(title = "销售单退货", businessType = BusinessType.UPDATE)
    @PostMapping("/confirmReturn")
    public AjaxResult confirmReturn(@Valid @RequestBody SalesReturnParam returnParam)
    {
        return salesReturnService.confirmReturn(returnParam);
    }

    /**
     * 修改销售单退货
     */
    //@PreAuthorize("@ss.hasPermi('sales:return:edit')")
    @Log(title = "销售单退货", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody SalesReturn salesReturn)
    {
        return toAjax(salesReturnService.updateSalesReturn(salesReturn));
    }

    /**
     * 删除销售单退货
     */
    //@PreAuthorize("@ss.hasPermi('sales:return:remove')")
    @Log(title = "销售单退货", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(salesReturnService.deleteSalesReturnByIds(ids));
    }

    /**
     * 导入会员信息
     */
    @Log(title = "导入患者信息", businessType = BusinessType.EXPORT)
    @PostMapping("/importNoFlowExcel")
    public AjaxResult importNoFlowExcel(@RequestParam("file") MultipartFile file) throws Exception {
        ExcelUtil<SalesNoFlowVo> util = new ExcelUtil<SalesNoFlowVo>(SalesNoFlowVo.class);
        List<SalesNoFlowVo> list = util.importExcel("退货无流水",file.getInputStream(),0);
        AjaxResult message = salesReturnService.importNoFlowExcel(list);
        return message;
    }
}
