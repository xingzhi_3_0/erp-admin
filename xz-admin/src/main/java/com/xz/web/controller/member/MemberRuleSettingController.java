package com.xz.web.controller.member;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.member.domain.MemberInfo;
import com.xz.member.domain.RechargeActivity;
import com.xz.member.dto.MemberRuleSettingDTO;
import com.xz.member.service.IMemberRuleSettingService;
import com.xz.member.service.IRechargeActivityService;
import com.xz.member.vo.RechargeActivityVo;
import com.xz.purchase.service.IWarningSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 采购调整Controller
 *
 * @author xz
 * @date 2024-1-16
 */
@RestController
@RequestMapping("/member/ruleSetting")
public class MemberRuleSettingController extends BaseController {

    @Autowired
    private IRechargeActivityService rechargeActivityService;
    @Autowired
    private IMemberRuleSettingService memberRuleSettingService;

    /**
     * 获取设置信息
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:query')")
    @GetMapping(value = "/{setType}")
    public AjaxResult getISetTypeInfo(@PathVariable("setType") Integer setType) {
        return AjaxResult.success(memberRuleSettingService.getSettingInfo(setType));
    }

    /**
     * 保持配置设置
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:add')")
    @Log(title = "充值活动", businessType = BusinessType.INSERT)
    @PostMapping("/saveSetInfo")
    public AjaxResult saveSetInfo(@Valid @RequestBody MemberRuleSettingDTO dto) {
        return memberRuleSettingService.saveSettingInfo(dto);
    }

    /**
     * 获取设置信息
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:query')")
    @GetMapping(value = "/getSetInfo/{setType}")
    public AjaxResult getInfoBySetType(@PathVariable("setType") Integer setType) {
        return AjaxResult.success(memberRuleSettingService.getInfoBySetType(setType));
    }
}
