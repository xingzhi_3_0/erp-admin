package com.xz.web.controller.member;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.member.domain.MemberLevel;
import com.xz.member.dto.MemberRuleSettingDTO;
import com.xz.member.param.MemberQueryParam;
import com.xz.member.service.IMemberLevelService;
import com.xz.member.service.IMemberRuleSettingService;
import com.xz.member.service.IRechargeActivityService;
import com.xz.member.vo.MemberInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 会员等级Controller
 *
 * @author xz
 * @date 2024-1-24 09:42:44
 */
@RestController
@RequestMapping("/member/level")
public class MemberLevelController extends BaseController {

    @Autowired
    private IMemberRuleSettingService memberRuleSettingService;

    @Autowired
    private IMemberLevelService memberLevelService;

    /**
     * 查询会员列表
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:list')")
    @GetMapping("/list")
    public AjaxResult list(MemberLevel memberLevel) {
        List<MemberLevel> list = memberLevelService.getList(memberLevel);
        return AjaxResult.success(list);
    }

    /**
     * 获取会员等级信息
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(memberLevelService.getById(id));
    }

    /**
     * 会员等级
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:add')")
    @Log(title = "会员等级", businessType = BusinessType.UPDATE)
    @PostMapping("/editLevel")
    public AjaxResult editLevel(@Valid @RequestBody MemberLevel memberLevel) {
        return memberLevelService.editLevelInfo(memberLevel);
    }


}
