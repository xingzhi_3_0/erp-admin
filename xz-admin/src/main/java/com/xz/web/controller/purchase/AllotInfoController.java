package com.xz.web.controller.purchase;

import java.util.List;

import com.xz.common.core.domain.entity.SysTenant;
import com.xz.product.domain.Product;
import com.xz.product.dto.ProductExportDto;
import com.xz.purchase.vo.AllotInfoDataVo;
import com.xz.purchase.vo.AllotInfoVo;
import com.xz.audit.dto.AuditDto;
import com.xz.tenant.TenantContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.purchase.domain.AllotInfo;
import com.xz.purchase.service.IAllotInfoService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 库存调拨Controller
 *
 * @author xz
 * @date 2024-01-10
 */
@RestController
@RequestMapping("/allot/info")
public class AllotInfoController extends BaseController {
    @Autowired
    private IAllotInfoService allotInfoService;

    /**
     * 查询库存调拨列表
     */
    //@PreAuthorize("@ss.hasPermi('allot:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(AllotInfo allotInfo) {
        startPage();
        List<AllotInfoVo> list = allotInfoService.selectAllotInfoList(allotInfo);
        return getDataTable(list);
    }

    /**
     * 导出库存调拨列表
     */
    //@PreAuthorize("@ss.hasPermi('allot:info:export')")
    @Log(title = "库存调拨", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AllotInfo allotInfo) {
        List<AllotInfoVo> list = allotInfoService.selectAllotInfoList(allotInfo);
        ExcelUtil<AllotInfoVo> util = new ExcelUtil<AllotInfoVo>(AllotInfoVo.class);
        return util.exportExcel(list, "库存调拨数据");
    }

    /**
     * 获取库存调拨详细信息
     */
    //@PreAuthorize("@ss.hasPermi('allot:info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(allotInfoService.selectAllotInfoById(id));
    }

    /**
     * 新增库存调拨
     */
    //@PreAuthorize("@ss.hasPermi('allot:info:add')")
    @Log(title = "库存调拨", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody AllotInfo allotInfo) {
        return allotInfoService.insertAllotInfo(allotInfo);
    }

    /**
     * 修改库存调拨
     */
    //@PreAuthorize("@ss.hasPermi('allot:info:edit')")
    @Log(title = "库存调拨", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody AllotInfo allotInfo) {
        return allotInfoService.updateAllotInfo(allotInfo);
    }

    /**
     * 作废
     */
    //@PreAuthorize("@ss.hasPermi('allot:info:remove')")
    @Log(title = "库存调拨", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{id}")
    public AjaxResult delete(@PathVariable Long id) {
        return toAjax(allotInfoService.deleteAllotInfoById(id));
    }
    /**
     * 流程审核
     */
    @PostMapping("/allotInfoAudit")
    public AjaxResult allotInfoAudit(@Valid @RequestBody AuditDto auditDto){
        return allotInfoService.allotInfoAudit(auditDto);
    }
    /**
     * 调拨导出
     */
    @GetMapping("/allotInfoDataExport")
    public AjaxResult allotInfoDataExport() {
        List<AllotInfoDataVo> allotInfoData = allotInfoService.getAllotInfoData();
        ExcelUtil<AllotInfoDataVo> util = new ExcelUtil<AllotInfoDataVo>(AllotInfoDataVo.class);
        return util.exportExcel(allotInfoData, "调拨数据");
    }
}
