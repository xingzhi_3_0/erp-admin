package com.xz.web.controller.optometry;

import java.util.List;

import com.xz.common.utils.StringUtils;
import com.xz.diagnosisOrder.utils.DiagnosisOrderUtils;
import com.xz.optometry.vo.OptometryExportVo;
import com.xz.optometry.vo.OptometryInfoVo;
import com.xz.optometry.vo.OptometryVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.optometry.domain.Optometry;
import com.xz.optometry.service.IOptometryService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 验光配镜Controller
 *
 * @author xz
 * @date 2024-01-31
 */
@RestController
@RequestMapping("/optometry/optometry")
public class OptometryController extends BaseController {
    @Autowired
    private IOptometryService optometryService;

    /**
     * 查询验光配镜列表
     */
    //@PreAuthorize("@ss.hasPermi('optometry:optometry:list')")
    @GetMapping("/list")
    public TableDataInfo list(Optometry optometry) {
        startPage();
        List<OptometryVo> list = optometryService.selectOptometryList(optometry);
        return getDataTable(list);
    }
    /**
     * 查询验光配镜列表--无分页
     */
    //@PreAuthorize("@ss.hasPermi('optometry:optometry:list')")
    @GetMapping("/optometryList")
    public AjaxResult optometryList(Optometry optometry) {
        List<OptometryVo> list = optometryService.selectOptometryList(optometry);
        return AjaxResult.success(list);
    }
    /**
     * 导出验光配镜列表
     */
    //@PreAuthorize("@ss.hasPermi('optometry:optometry:export')")
    @Log(title = "验光配镜", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Optometry optometry) {
        List<OptometryExportVo> list = optometryService.selectOptometryListForExport(optometry);
        list.forEach(data-> data.setPrescriptionInfo(data.initPrescriptionInfo()));
        ExcelUtil<OptometryExportVo> util = new ExcelUtil<OptometryExportVo>(OptometryExportVo.class);
        return util.exportExcel(list, "验光配镜数据");
    }

    /**
     * 获取验光配镜详细信息
     */
    //@PreAuthorize("@ss.hasPermi('optometry:optometry:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(optometryService.selectOptometryById(id));
    }

    /**
     * 新增验光配镜
     */
    //@PreAuthorize("@ss.hasPermi('optometry:optometry:add')")
    @Log(title = "验光配镜", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody Optometry optometry) {
        return optometryService.insertOptometry(optometry);
    }

    /**
     * 修改验光配镜
     */
    //@PreAuthorize("@ss.hasPermi('optometry:optometry:edit')")
    @Log(title = "验光配镜", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody Optometry optometry) {
        return optometryService.updateOptometry(optometry);
    }

    /**
     * 作废验光配镜
     */
    //@PreAuthorize("@ss.hasPermi('optometry:optometry:remove')")
    @Log(title = "验光配镜", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{id}")
    public AjaxResult delete(@PathVariable Long id) {
        return toAjax(optometryService.updateOptometryById(3,id));
    }
}
