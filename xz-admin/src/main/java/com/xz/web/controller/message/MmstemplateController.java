package com.xz.web.controller.message;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.domain.entity.SysUser;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.message.domain.Mmstemplate;
import com.xz.message.service.IMmstemplateService;
import com.xz.store.domain.StoreBasic;
import com.xz.store.service.IStoreBasicService;
import com.xz.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 消息模板Controller
 *
 * @author xz
 * @date 2022-03-04
 */
@RestController
@RequestMapping("/message/mmstemplate")
public class MmstemplateController extends BaseController {
    @Autowired
    private IMmstemplateService mmstemplateService;
    @Autowired
    private ISysUserService userService;

    @Autowired
    private IStoreBasicService storeBasicService;
    /**
     * 查询消息模板列表--新订单通知
     */
    @GetMapping("/newOrderList")
    public TableDataInfo newOrderList(Mmstemplate mmstemplate) {
        startPage();
        mmstemplate.setPushNode(1);
        List<Mmstemplate> list = mmstemplateService.selectMmstemplateList(mmstemplate);
        return getDataTable(list);
    }

    /**
     * 查询消息模板列表--业务员通知
     */
    @GetMapping("/salespersonNoticeList")
    public TableDataInfo salespersonNoticeList(Mmstemplate mmstemplate) {
        startPage();
        mmstemplate.setPushNode(2);
        List<Mmstemplate> list = mmstemplateService.selectMmstemplateList(mmstemplate);
        return getDataTable(list);
    }

    /**
     * 查询消息模板列表--用户取车通知
     */
    @GetMapping("/userPickupNotificationList")
    public TableDataInfo userPickupNotificationList(Mmstemplate mmstemplate) {
        startPage();
        mmstemplate.setPushNode(3);
        List<Mmstemplate> list = mmstemplateService.selectMmstemplateList(mmstemplate);
        return getDataTable(list);
    }

    /**
     * 查询消息模板列表--gps告警通知
     */
    @GetMapping("/gpsAlarmNotification")
    public TableDataInfo gpsAlarmNotification(Mmstemplate mmstemplate) {
        startPage();
        mmstemplate.setPushNode(4);
        List<Mmstemplate> list = mmstemplateService.selectMmstemplateList(mmstemplate);
        return getDataTable(list);
    }
    /**
     * 查询门店下员工
     */
    @GetMapping("/getUserList")
    public TableDataInfo getUserList(SysUser user) {
        StoreBasic storeBasic = storeBasicService.selectStoreBasicByStoreId(user.getStoreId());
        user.setDeptIdTemplate(storeBasic.getDeptId());
        startPage();
        List<SysUser> list = userService.selectUserList(user);
        return getDataTable(list);
    }
    /**
     * 导出消息模板列表
     */
    //@PreAuthorize("@ss.hasPermi('message:mmstemplate:export')")
    @Log(title = "消息模板", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Mmstemplate mmstemplate) {
        List<Mmstemplate> list = mmstemplateService.selectMmstemplateList(mmstemplate);
        ExcelUtil<Mmstemplate> util = new ExcelUtil<Mmstemplate>(Mmstemplate.class);
        return util.exportExcel(list, "消息模板数据");
    }

    /**
     * 获取消息模板详细信息
     */
    //@PreAuthorize("@ss.hasPermi('message:mmstemplate:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(mmstemplateService.selectMmstemplateById(id));
    }

    /**
     * 新增消息模板
     */
    //@PreAuthorize("@ss.hasPermi('message:mmstemplate:add')")
    @Log(title = "消息模板", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody Mmstemplate mmstemplate) {
        return toAjax(mmstemplateService.insertMmstemplate(mmstemplate));
    }

    /**
     * 修改消息模板
     */
    //@PreAuthorize("@ss.hasPermi('message:mmstemplate:edit')")
    @Log(title = "消息模板", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody Mmstemplate mmstemplate) {
        return toAjax(mmstemplateService.updateMmstemplate(mmstemplate));
    }


    /**
     * 启停消息模板
     */
    //@PreAuthorize("@ss.hasPermi('message:mmstemplate:edit')")
    @Log(title = "消息模板", businessType = BusinessType.UPDATE)
    @PutMapping("/editStatus/{id}/{status}")
    public AjaxResult editStatus(@PathVariable("id") Long id, @PathVariable("status") Long status) {
        Mmstemplate mmstemplate = new Mmstemplate();
        mmstemplate.setId(id);
        mmstemplate.setStatus(status);
        return toAjax(mmstemplateService.updateMmstemplate(mmstemplate));
    }

    /**
     * 删除消息模板
     */
    //@PreAuthorize("@ss.hasPermi('message:mmstemplate:remove')")
    @Log(title = "消息模板", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(mmstemplateService.deleteMmstemplateByIds(ids));
    }
}
