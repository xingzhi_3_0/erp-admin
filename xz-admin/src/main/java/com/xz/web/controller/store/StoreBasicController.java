package com.xz.web.controller.store;

import com.xz.common.annotation.Log;
import com.xz.common.constant.UserConstants;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.exception.ServiceException;
import com.xz.common.utils.SecurityUtils;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.store.Vo.AlsoFeeStoreVo;
import com.xz.store.Vo.ApplicableUnitListVo;
import com.xz.store.Vo.AssociationEnterpriseCertificationVO;
import com.xz.store.Vo.StoreBasicListVo;
import com.xz.store.domain.StoreBasic;
import com.xz.store.service.IStoreBasicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * 门店基本信息Controller
 *
 * @author xz
 * @date 2022-03-01
 */
@RestController
@RequestMapping("/store/store")
public class StoreBasicController extends BaseController {
    @Autowired
    private IStoreBasicService storeBasicService;

    /**
     * 查询门店基本信息列表
     */
    @GetMapping("/allStoreHaveScope")
    public TableDataInfo allStoreHaveScope(StoreBasic storeBasic) {
        startPage();
        List<StoreBasic> list = storeBasicService.selectStoreBasicList(storeBasic);
        return getDataTable(list);
    }
    @GetMapping("/allStoreNoScope")
    public TableDataInfo allStoreNoScope(StoreBasic storeBasic) {
        startPage();
        List<StoreBasic> list = storeBasicService.selectAllListStore(storeBasic);
        return getDataTable(list);
    }
    /**
     * 查询门店基本信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(StoreBasic storeBasic) {
        startPage();
        List<StoreBasic> list = storeBasicService.selectStoreBasicList(storeBasic);
        return getDataTable(list);
    }
    /**
     * 查询所有门店
     */
    @GetMapping("/getAllStore")
    public AjaxResult getAllStore(StoreBasic storeBasic) {
        List<StoreBasic> list = storeBasicService.selectStoreBasicList(storeBasic);
        return AjaxResult.success(list);
    }
    /**
     * 门店列表(其他模块调用)
     */
    @GetMapping("/storeBasicList")
    public TableDataInfo storeBasicList(@RequestBody StoreBasic storeBasic) {
        startPage();
        List<StoreBasicListVo> listVoList = storeBasicService.storeBasicList(storeBasic);
        return getDataTable(listVoList);
    }
    /**
     * 导出门店基本信息列表
     */
    @Log(title = "门店基本信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StoreBasic storeBasic) {
        List<StoreBasic> list = storeBasicService.selectStoreBasicList(storeBasic);
        ExcelUtil<StoreBasic> util = new ExcelUtil<StoreBasic>(StoreBasic.class);
        return util.exportExcel(list, "门店基本信息数据");
    }

    /**
     * 获取门店基本信息详细信息
     */
    @GetMapping(value = "/{storeId}")
    public AjaxResult getInfo(@PathVariable("storeId") String storeId) {
        return AjaxResult.success(storeBasicService.selectStoreBasicByStoreId(storeId));
    }

    /**
     * 新增门店基本信息
     */
    @Log(title = "门店基本信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StoreBasic storeBasic) {
        if (UserConstants.NOT_UNIQUE.equals(storeBasicService.checkNameUnique(storeBasic))) {
            return AjaxResult.error("新增门店'" + storeBasic.getStoreName() + "'失败，门店名称已存在");
        }
        storeBasic.setCompanyId(SecurityUtils.getDeptId()+"");
        storeBasic.setStoreType(1);
        return toAjax(storeBasicService.insertStoreBasic(storeBasic));
    }

    /**
     * 修改门店基本信息
     */
    @Log(title = "门店基本信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody StoreBasic storeBasic) {
        if (UserConstants.NOT_UNIQUE.equals(storeBasicService.checkNameUnique(storeBasic))) {
            return AjaxResult.error("修改门店'" + storeBasic.getStoreName() + "'失败，门店名称已存在");
        }
        return toAjax(storeBasicService.updateStoreBasic(storeBasic));
    }

    /**
     * 删除门店基本信息
     */
    @Log(title = "门店基本信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{storeId}")
    public AjaxResult remove(@PathVariable String storeId) {
        return storeBasicService.deleteStoreBasicByStoreId(storeId);
    }

    /**
     * 启用/停用
     */
    @Log(title = "启用/停用门店", businessType = BusinessType.UPDATE)
    @PutMapping("/editAvailable")
    public AjaxResult editAvailable(@RequestBody StoreBasic storeBasic) {
        return toAjax(storeBasicService.editAvailable(storeBasic));
    }


    /**
     * 异店还车门店列表
     */
    @GetMapping("/alsoFeeStoreList")
    public AjaxResult alsoFeeStoreList() {
        List<AlsoFeeStoreVo> storeVoList = storeBasicService.alsoFeeStoreList();
        return AjaxResult.success(storeVoList);
    }

    /**
     * 适用单位
     */
    @GetMapping("/applicableUnitList")
    public AjaxResult applicableUnitList() {
        List<ApplicableUnitListVo> applicableUnitListVos = storeBasicService.applicableUnitList(SecurityUtils.getLoginUser().getTenantId());
        return AjaxResult.success(applicableUnitListVos);
    }
}
