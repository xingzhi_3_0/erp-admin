package com.xz.web.controller.system;

import java.util.ArrayList;
import java.util.List;

import com.xz.common.utils.SecurityUtils;
import com.xz.member.domain.MemberInfo;
import com.xz.member.service.IMemberInfoService;
import com.xz.message.service.ISysTenantNotifyItemService;
import com.xz.message.vo.MemberMsgInfo;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.system.domain.SysNotice;
import com.xz.system.service.ISysNoticeService;

/**
 * 公告 信息操作处理
 *
 * @author xz
 */
@RestController
@RequestMapping("/system/notice")
public class SysNoticeController extends BaseController {
    @Autowired
    private ISysNoticeService noticeService;
    @Autowired
    private ISysTenantNotifyItemService iSysTenantNotifyItemService;
    @Autowired
    private IMemberInfoService iMemberInfoService;

    /**
     * 获取通知公告列表
     */
    //@PreAuthorize("@ss.hasPermi('system:notice:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysNotice notice) {
        startPage();
        notice.setUserId(SecurityUtils.getLoginUser().getUserId());
        if ("1".equals(notice.getNoticeType())) {
            notice.setTenantId(SecurityUtils.getLoginUser().getTenantId());
        }else{
            if(!SecurityUtils.getLoginUser().getUser().isAdmin()){
                notice.setStatus("0");
            }
        }
        List<SysNotice> list = noticeService.selectNoticeList(notice);
        return getDataTable(list);
    }

    /**
     * 根据通知公告编号获取详细信息
     */
    //@PreAuthorize("@ss.hasPermi('system:notice:query')")
    @GetMapping(value = "/{noticeId}")
    public AjaxResult getInfo(@PathVariable Long noticeId) {
        return AjaxResult.success(noticeService.selectNoticeById(noticeId, SecurityUtils.getLoginUser().getUserId()));
    }

    /**
     * 根据通知公告编号获取详细信息
     */
    //@PreAuthorize("@ss.hasPermi('system:notice:query')")
    @GetMapping(value = "/getNotReadNotice")
    public AjaxResult getNotReadNotice() {
        return AjaxResult.success(noticeService.getNotReadNotice(SecurityUtils.getLoginUser().getUserId()));
    }

    /**
     * 新增通知公告
     */
    //@PreAuthorize("@ss.hasPermi('system:notice:add')")
    @Log(title = "通知公告", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody SysNotice notice) {
        notice.setCreateBy(SecurityUtils.getLoginUser().getUserId());
        notice.setTenantId(SecurityUtils.getLoginUser().getTenantId());
        return toAjax(noticeService.insertNotice(notice));
    }

    /**
     * 修改通知公告
     */
    //@PreAuthorize("@ss.hasPermi('system:notice:edit')")
    @Log(title = "通知公告", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody SysNotice notice) {
        notice.setUpdateBy(SecurityUtils.getLoginUser().getUserId());
        return toAjax(noticeService.updateNotice(notice));
    }

    /**
     * 删除通知公告
     */
    //@PreAuthorize("@ss.hasPermi('system:notice:remove')")
    @Log(title = "通知公告", businessType = BusinessType.DELETE)
    @DeleteMapping("/{noticeIds}")
    public AjaxResult remove(@PathVariable Long[] noticeIds) {
        return toAjax(noticeService.deleteNoticeByIds(noticeIds));
    }


    /**
     * 通知公告已读
     */
    @Log(title = "通知公告", businessType = BusinessType.DELETE)
    @GetMapping("/markRead/{noticeIds}")
    public AjaxResult markRead(@PathVariable Long[] noticeIds) {
        return toAjax(noticeService.markRead(noticeIds, SecurityUtils.getLoginUser().getUserId()));
    }
    /**
     * 新增工单通知公告
     */
    @PostMapping("/insertWorkNotice")
    public AjaxResult insertWorkNotice(@RequestBody SysNotice notice) {
        notice.setCreateBy(SecurityUtils.getLoginUser().getUserId());
        notice.setTenantId(SecurityUtils.getLoginUser().getTenantId());
        return toAjax(noticeService.insertWorkNotice(notice));
    }


    @PostMapping("/sendMemberInfoMsg")
    public AjaxResult sendMemberInfoMsg(MemberMsgInfo memberMsgInfo){
        iSysTenantNotifyItemService.sendMemberInfoMsg(memberMsgInfo,1,SecurityUtils.getLoginUser().getUser());
        return AjaxResult.success();
    }

}
