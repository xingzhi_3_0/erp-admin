package com.xz.web.controller.product;

import java.util.List;

import com.xz.product.dto.ProductCategoryListDto;
import com.xz.product.service.IProductService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.product.domain.ProductCategory;
import com.xz.product.service.IProductCategoryService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 商品类别Controller
 *
 * @author xz
 * @date 2024-01-02
 */
@RestController
@RequestMapping("/product/category")
public class ProductCategoryController extends BaseController {
    @Autowired
    private IProductCategoryService productCategoryService;

    /**
     * 查询商品类别列表
     */
    //@PreAuthorize("@ss.hasPermi('product:category:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProductCategory productCategory) {
        startPage();
        List<ProductCategoryListDto> list = productCategoryService.selectProductCategoryList(productCategory);
        return getDataTable(list);
    }


    /**
     * 查询商品类别列表
     */
    //@PreAuthorize("@ss.hasPermi('product:category:list')")
    @GetMapping("/getProductCategoryList")
    public AjaxResult getProductCategoryList() {
        ProductCategory productCategory =new ProductCategory();
        productCategory.setStatus("0");
        return AjaxResult.success(productCategoryService.selectProductCategoryList(productCategory));
    }

    /**
     * 查询商品类别列表
     */
    //@PreAuthorize("@ss.hasPermi('product:category:list')")
    @GetMapping("/queryProductCategoryList")
    public TableDataInfo queryProductCategoryList(ProductCategory productCategory) {
        productCategory.setStatus("0");
        startPage();
        List<ProductCategoryListDto> list = productCategoryService.selectProductCategoryList(productCategory);
        return getDataTable(list);
    }

    /**
     * 导出商品类别列表
     */
    //@PreAuthorize("@ss.hasPermi('product:category:export')")
    @Log(title = "商品类别", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ProductCategory productCategory) {
        List<ProductCategoryListDto> list = productCategoryService.selectProductCategoryList(productCategory);
        ExcelUtil<ProductCategoryListDto> util = new ExcelUtil<ProductCategoryListDto>(ProductCategoryListDto.class);
        return util.exportExcel(list, "商品类别数据");
    }

    /**
     * 获取商品类别详细信息
     */
    //@PreAuthorize("@ss.hasPermi('product:category:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(productCategoryService.selectProductCategoryById(id));
    }

    /**
     * 新增商品类别
     */
    //@PreAuthorize("@ss.hasPermi('product:category:add')")
    @Log(title = "商品类别", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody ProductCategory productCategory) {
        return productCategoryService.insertProductCategory(productCategory);
    }

    /**
     * 修改商品类别
     */
    //@PreAuthorize("@ss.hasPermi('product:category:edit')")
    @Log(title = "商品类别", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody ProductCategory productCategory) {
        return productCategoryService.updateProductCategory(productCategory);
    }


    /**
     * 设置商品类别预警天数
     */
    //@PreAuthorize("@ss.hasPermi('product:category:add')")
    @Log(title = "商品类别", businessType = BusinessType.UPDATE)
    @PostMapping("setWarnDays")
    public AjaxResult setWarnDays(@Valid @RequestBody ProductCategory productCategory) {
        return productCategoryService.setWarnDays(productCategory);
    }

    /**
     * 删除商品类别
     */
    //@PreAuthorize("@ss.hasPermi('product:category:remove')")
    @Log(title = "商品类别", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(productCategoryService.deleteProductCategoryByIds(ids));
    }
    /**
     * 删除商品类别
     */
    //@PreAuthorize("@ss.hasPermi('product:category:remove')")
    @Log(title = "商品类别", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{id}")
    public AjaxResult delete(@PathVariable Long id) {
        return productCategoryService.deleteProductCategoryById(id);
    }
}
