package com.xz.web.controller.system;

import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.utils.DateUtils;
import com.xz.common.utils.SecurityUtils;
import com.xz.system.domain.SysTenantConfig;
import com.xz.system.domain.vo.DictDataVo;
import com.xz.system.mapper.SysTenantMapper;
import common.ECDateUtils;
import common.Safes;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import com.xz.common.config.XzConfig;
import com.xz.common.utils.StringUtils;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * 首页
 *
 * @author xz
 */
@RestController
public class SysIndexController extends BaseController {
    /**
     * 系统基础配置
     */
    @Autowired
    private XzConfig xzConfig;

    @Autowired
    private SysTenantMapper sysTenantMapper;
    /**
     * 访问首页，提示语
     */
    @RequestMapping("/")
    public String index() {
        return StringUtils.format("欢迎使用{}后台管理框架，当前版本：v{}，请通过前端地址访问。", xzConfig.getName(), xzConfig.getVersion());
    }
}
