package com.xz.web.controller.workbench;

import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.system.service.ISysRoleService;
import com.xz.workbench.dto.WorkbenchTopDto;
import com.xz.workbench.service.IWorkbenchTopStatService;
import com.xz.workbench.vo.WorkbenchesTopStaticVo;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @program: xz
 * @description:
 * @author: pengyuyan
 * @create: 2024-06-27 16:53
 **/
@RestController
@RequestMapping("/workbench")
public class WorkbenchController extends BaseController {

	@Autowired
	private ISysRoleService sysRoleService;
	@Autowired
	private IWorkbenchTopStatService workbenchService;

	/**
	 * 查询工作台顶部统计数据
	 */
	@GetMapping("/top/statistics")
	public AjaxResult workbenches(WorkbenchTopDto dto) {
		dto.resetEndTime();
		setLoginUserDept(dto);
		List<WorkbenchesTopStaticVo> workbenchesVos = workbenchService.selectStatistics(dto);
		return AjaxResult.success(workbenchesVos);
	}

	/**
	 * 设置当前登录人看到的部门
	 *
	 * @param dto
	 */
	private void setLoginUserDept(WorkbenchTopDto dto) {
		if (CollectionUtils.isNotEmpty(dto.getDeptIds())) {
			return;
		}
		List<Long> deptIds = sysRoleService.myQueryDeptId();
		if (CollectionUtils.isEmpty(deptIds)) {
			dto.setDeptIds(deptIds);
		}
	}
}
