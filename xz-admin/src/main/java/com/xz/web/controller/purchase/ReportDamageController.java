package com.xz.web.controller.purchase;

import java.util.List;

import com.xz.audit.dto.AuditDto;
import com.xz.purchase.vo.ReportDamageVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.purchase.domain.ReportDamage;
import com.xz.purchase.service.IReportDamageService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 库存报损Controller
 *
 * @author xz
 * @date 2024-01-16
 */
@RestController
@RequestMapping("/purchase/damage")
public class ReportDamageController extends BaseController {
    @Autowired
    private IReportDamageService reportDamageService;

    /**
     * 查询库存报损列表
     */
    //@PreAuthorize("@ss.hasPermi('purchase:damage:list')")
    @GetMapping("/list")
    public TableDataInfo list(ReportDamage reportDamage) {
        startPage();
        List<ReportDamageVo> list = reportDamageService.selectReportDamageList(reportDamage);
        return getDataTable(list);
    }

    /**
     * 导出库存报损列表
     */
    //@PreAuthorize("@ss.hasPermi('purchase:damage:export')")
    @Log(title = "库存报损", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ReportDamage reportDamage) {
        List<ReportDamageVo> list = reportDamageService.selectReportDamageList(reportDamage);
        ExcelUtil<ReportDamageVo> util = new ExcelUtil<ReportDamageVo>(ReportDamageVo.class);
        return util.exportExcel(list, "库存报损数据");
    }

    /**
     * 获取库存报损详细信息
     */
    //@PreAuthorize("@ss.hasPermi('purchase:damage:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(reportDamageService.selectReportDamageById(id));
    }

    /**
     * 新增库存报损
     */
    //@PreAuthorize("@ss.hasPermi('purchase:damage:add')")
    @Log(title = "库存报损", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody ReportDamage reportDamage) {
        return reportDamageService.insertReportDamage(reportDamage);
    }

    /**
     * 修改库存报损
     */
    //@PreAuthorize("@ss.hasPermi('purchase:damage:edit')")
    @Log(title = "库存报损", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody ReportDamage reportDamage) {
        return reportDamageService.updateReportDamage(reportDamage);
    }

    /**
     * 作废报损
     */
    //@PreAuthorize("@ss.hasPermi('purchase:damage:remove')")
    @Log(title = "库存报损", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{id}")
    public AjaxResult delete(@PathVariable Long id) {
        return toAjax(reportDamageService.deleteReportDamageById(id));
    }
    /**
     * 流程审核
     */
    @PostMapping("/reportDamageAudit")
    public AjaxResult reportDamageAudit(@Valid @RequestBody AuditDto auditDto){
        return reportDamageService.reportDamageAudit(auditDto);
    }
    /**
     * 加工单报损
     */
    @GetMapping("/getProcessingReportDamageInfo/{id}")
    public AjaxResult getProcessingReportDamageInfo(@PathVariable("id") Long id){
        return AjaxResult.success(reportDamageService.getProcessingReportDamageInfo(id));
    }
}
