package com.xz.web.controller.system;

import java.net.URLDecoder;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.system.domain.Graphic;
import com.xz.system.service.IGraphicService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * H5图文Controller
 *
 * @author xz
 * @date 2022-04-12
 */
@RestController
@RequestMapping("/system/graphic")
public class GraphicController extends BaseController {
    @Autowired
    private IGraphicService graphicService;

    /**
     * 查询H5图文列表
     */
    //@PreAuthorize("@ss.hasPermi('system:graphic:list')")
    @GetMapping("/list")
    public TableDataInfo list(Graphic graphic) {
        startPage();
        List<Graphic> list = graphicService.selectGraphicList(graphic);
        return getDataTable(list);
    }

    /**
     * 获取H5图文详细信息
     */
    //@PreAuthorize("@ss.hasPermi('system:graphic:query')")
    @GetMapping(value = "/{graphicId}")
    public AjaxResult getInfo(@PathVariable("graphicId") Long graphicId) {
        return AjaxResult.success(graphicService.selectGraphicByGraphicId(graphicId));
    }

    /**
     * 新增H5图文
     */
    //@PreAuthorize("@ss.hasPermi('system:graphic:add')")
    @Log(title = "H5图文", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody Graphic graphic) {
        graphic.setGraphicIntroduction(URLDecoder.decode(graphic.getRemark()));
        graphic.setRemark(URLDecoder.decode(graphic.getRemark()));
        return graphicService.insertGraphic(graphic);
    }

    /**
     * 修改H5图文
     */
    //@PreAuthorize("@ss.hasPermi('system:graphic:edit')")
    @Log(title = "H5图文", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody Graphic graphic) {
        graphic.setGraphicIntroduction(URLDecoder.decode(graphic.getRemark()));
        graphic.setRemark(URLDecoder.decode(graphic.getRemark()));
        return graphicService.updateGraphic(graphic);
    }
    /**
     * 删除活动模板
     */
    @DeleteMapping("/{graphicId}")
    public AjaxResult remove(@PathVariable Long graphicId) {
        return toAjax(graphicService.deleteId(graphicId));
    }
}
