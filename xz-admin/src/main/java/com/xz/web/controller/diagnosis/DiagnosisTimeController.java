package com.xz.web.controller.diagnosis;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.diagnosis.domain.DiagnosisTime;
import com.xz.diagnosis.service.IDiagnosisTimeService;
import com.xz.diagnosis.vo.DiagnosisTimeTreeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 诊疗时间项Controller
 *
 * @author xz
 * @date 2024-03-21
 */
@RestController
@RequestMapping("/diagnosis/time")
public class DiagnosisTimeController extends BaseController {
  @Autowired
  private IDiagnosisTimeService diagnosisTimeService;

  /**
   * 查询诊疗时间项列表
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:time:list')")
  @GetMapping("tree/list")
  public AjaxResult treeList(DiagnosisTime diagnosisTime) {
    List<DiagnosisTimeTreeVo> vo = diagnosisTimeService.treeList(diagnosisTime);
    return AjaxResult.success(vo);
  }
}
