package com.xz.web.controller.logistics;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.logistics.domain.Logistics;
import com.xz.logistics.dto.LogisticsDto;
import com.xz.logistics.service.ILogisticsService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/logistics")
public class LogisticsController extends BaseController {

    @Autowired
    private ILogisticsService logisticsService;

    /**
     * 查询业务订单列表
     */
    @GetMapping("/selectByBusinessNo")
    public TableDataInfo selectByBusinessNo(String businessNo) {
        List<Logistics> list = logisticsService.selectByBusinessNo(businessNo);
        return getDataTable(list);
    }

    /**
     * 获取物流详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfoById(@PathVariable("id") Long id) {
        return AjaxResult.success(logisticsService.getById(id));
    }

    /**
     * 获取物流详细信息（根据业务编码）
     */
    @GetMapping(value = "/business/{businessNo}")
    public AjaxResult getInfoByBusinessNo(@PathVariable("businessNo") String businessNo) {
        List<Logistics> list = logisticsService.selectByBusinessNo(businessNo);
        Logistics logistics = CollectionUtils.isEmpty(list) ? null : list.get(0);
        return AjaxResult.success(logistics);
    }

    @Log(title = "新增物流信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody LogisticsDto dto) {
        Logistics logistics = new Logistics();
        BeanUtils.copyProperties(dto,logistics);
        return logisticsService.saveLogistics(logistics);
    }

    @Log(title = "修改物流信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody LogisticsDto dto) {
        if (null == dto.getId()) {
            return AjaxResult.error("ID参数异常");
        }
        Logistics logistics = logisticsService.getById(dto.getId());
        BeanUtils.copyProperties(dto,logistics);
        return logisticsService.updateLogistics(logistics);
    }

}
