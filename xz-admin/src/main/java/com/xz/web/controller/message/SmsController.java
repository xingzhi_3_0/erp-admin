package com.xz.web.controller.message;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.message.domain.Sms;
import com.xz.message.service.ISmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 短信Controller
 * 
 * @author xz
 * @date 2022-03-07
 */
@RestController
@RequestMapping("/message/sms")
public class SmsController extends BaseController {
    @Autowired
    private ISmsService smsService;

    /**
     * 查询短信列表
     */
    //@PreAuthorize("@ss.hasPermi('message:sms:list')")
    @GetMapping("/list")
    public TableDataInfo list(Sms sms)
    {
        startPage();
        List<Sms> list = smsService.selectSmsList(sms);
        return getDataTable(list);
    }

    /**
     * 导出短信列表
     */
    //@PreAuthorize("@ss.hasPermi('message:sms:export')")
    @Log(title = "短信", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Sms sms)
    {
        List<Sms> list = smsService.selectSmsList(sms);
        ExcelUtil<Sms> util = new ExcelUtil<Sms>(Sms.class);
        return util.exportExcel(list, "短信数据");
    }

    /**
     * 获取短信详细信息
     */
    //@PreAuthorize("@ss.hasPermi('message:sms:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(smsService.selectSmsById(id));
    }

    /**
     * 新增短信
     */
    //@PreAuthorize("@ss.hasPermi('message:sms:add')")
    @Log(title = "短信", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody Sms sms)
    {
        return toAjax(smsService.insertSms(sms));
    }

    /**
     * 修改短信
     */
    //@PreAuthorize("@ss.hasPermi('message:sms:edit')")
    @Log(title = "短信", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody Sms sms)
    {
        return toAjax(smsService.updateSms(sms));
    }

    /**
     * 删除短信
     */
    //@PreAuthorize("@ss.hasPermi('message:sms:remove')")
    @Log(title = "短信", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(smsService.deleteSmsByIds(ids));
    }

    /**
     * 发送短信
     */
    //@PreAuthorize("@ss.hasPermi('message:sms:edit')")
    @Log(title = "发送短信", businessType = BusinessType.UPDATE)
    @PostMapping("/sendSms")
    public AjaxResult sendSms(@RequestBody Sms sms)
    {

        return toAjax(smsService.sendSms(sms));
    }

}
