package com.xz.web.controller.repertory;

import java.util.List;

import com.xz.repertory.dto.RepertoryFlowBillDto;
import com.xz.repertory.dto.RepertoryFlowDto;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.repertory.domain.RepertoryFlow;
import com.xz.repertory.service.IRepertoryFlowService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 库存流水Controller
 *
 * @author xz
 * @date 2024-01-06
 */
@RestController
@RequestMapping("/repertory/flow")
public class RepertoryFlowController extends BaseController {
    @Autowired
    private IRepertoryFlowService repertoryFlowService;

    /**
     * 查询库存流水列表--库存
     */
    //@PreAuthorize("@ss.hasPermi('repertory:flow:list')")
    @GetMapping("/list")
    public TableDataInfo list(RepertoryFlow repertoryFlow) {
        startPage();
        List<RepertoryFlowDto> list = repertoryFlowService.selectRepertoryFlowList(repertoryFlow);
        return getDataTable(list);
    }

    /**
     * 导出库存流水列表--库存
     */
    //@PreAuthorize("@ss.hasPermi('repertory:flow:export')")
    @Log(title = "库存流水", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(RepertoryFlow repertoryFlow) {
        List<RepertoryFlowDto> list = repertoryFlowService.selectRepertoryFlowList(repertoryFlow);
        ExcelUtil<RepertoryFlowDto> util = new ExcelUtil<RepertoryFlowDto>(RepertoryFlowDto.class);
        return util.exportExcel(list, "库存流水数据");
    }
    /**
     * 查询库存流水列表--财务
     */
    //@PreAuthorize("@ss.hasPermi('repertory:flow:list')")
    @GetMapping("/billList")
    public TableDataInfo billList(RepertoryFlow repertoryFlow) {
        startPage();
        List<RepertoryFlowBillDto> list = repertoryFlowService.selectRepertoryFlowBillList(repertoryFlow);
        return getDataTable(list);
    }

    /**
     * 导出库存流水列表--财务
     */
    //@PreAuthorize("@ss.hasPermi('repertory:flow:export')")
    @Log(title = "库存流水", businessType = BusinessType.EXPORT)
    @GetMapping("/billExport")
    public AjaxResult billExport(RepertoryFlow repertoryFlow) {
        List<RepertoryFlowBillDto> list = repertoryFlowService.selectRepertoryFlowBillList(repertoryFlow);
        ExcelUtil<RepertoryFlowBillDto> util = new ExcelUtil<RepertoryFlowBillDto>(RepertoryFlowBillDto.class);
        return util.exportExcel(list, "库存流水数据");
    }

    /**
     * 获取库存流水详细信息
     */
    //@PreAuthorize("@ss.hasPermi('repertory:flow:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(repertoryFlowService.selectRepertoryFlowById(id));
    }

    /**
     * 新增库存流水
     */
    //@PreAuthorize("@ss.hasPermi('repertory:flow:add')")
    @Log(title = "库存流水", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody RepertoryFlow repertoryFlow) {
        return toAjax(repertoryFlowService.insertRepertoryFlow(repertoryFlow));
    }

    /**
     * 修改库存流水
     */
    //@PreAuthorize("@ss.hasPermi('repertory:flow:edit')")
    @Log(title = "库存流水", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody RepertoryFlow repertoryFlow) {
        return repertoryFlowService.updateRepertoryFlow(repertoryFlow);
    }

    /**
     * 删除库存流水
     */
    //@PreAuthorize("@ss.hasPermi('repertory:flow:remove')")
    @Log(title = "库存流水", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(repertoryFlowService.deleteRepertoryFlowByIds(ids));
    }
}
