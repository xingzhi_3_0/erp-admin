package com.xz.web.controller.sales;

import java.util.List;

import com.xz.sales.param.SalesDetailParam;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;
import javax.validation.Valid;
import com.xz.sales.domain.SalesOrderDetail;
import com.xz.sales.service.ISalesOrderDetailService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 销售单商品明细Controller
 * 
 * @author xz
 * @date 2024-01-31
 */
@RestController
@RequestMapping("/sales/orderDetail")
public class SalesOrderDetailController extends BaseController {
    @Autowired
    private ISalesOrderDetailService salesOrderDetailService;

    /**
     * 查询销售单商品明细列表
     */
    //@PreAuthorize("@ss.hasPermi('sales:detail:list')")
    @GetMapping("/list")
    public TableDataInfo list(SalesOrderDetail salesOrderDetail)
    {
        startPage();
        List<SalesOrderDetail> list = salesOrderDetailService.selectSalesOrderDetailList(salesOrderDetail);
        return getDataTable(list);
    }

    /**
     * 查询销售单商品明细列表
     */
    //@PreAuthorize("@ss.hasPermi('sales:detail:list')")
    @GetMapping("/listBySaleId")
    public AjaxResult listBySaleId(SalesOrderDetail salesOrderDetail)
    {
        List<SalesOrderDetail> list = salesOrderDetailService.selectSalesOrderDetailList(salesOrderDetail);
        return AjaxResult.success(list);
    }

    /**
     * 查询销售单商品明细列表
     */
    //@PreAuthorize("@ss.hasPermi('sales:detail:list')")
    @GetMapping("/chooseDetailList")
    public AjaxResult chooseDetailList(SalesDetailParam salesDetailParam)
    {
        List<SalesOrderDetail> list = salesOrderDetailService.chooseDetailList(salesDetailParam);
        return AjaxResult.success(list);
    }

    /**
     * 导出销售单商品明细列表
     */
    //@PreAuthorize("@ss.hasPermi('sales:detail:export')")
    @Log(title = "销售单商品明细", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SalesOrderDetail salesOrderDetail)
    {
        List<SalesOrderDetail> list = salesOrderDetailService.selectSalesOrderDetailList(salesOrderDetail);
        ExcelUtil<SalesOrderDetail> util = new ExcelUtil<SalesOrderDetail>(SalesOrderDetail.class);
        return util.exportExcel(list, "销售单商品明细数据");
    }

    /**
     * 获取销售单商品明细详细信息
     */
    //@PreAuthorize("@ss.hasPermi('sales:detail:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(salesOrderDetailService.selectSalesOrderDetailById(id));
    }

    /**
     * 新增销售单商品明细
     */
    //@PreAuthorize("@ss.hasPermi('sales:detail:add')")
    @Log(title = "销售单商品明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody SalesOrderDetail salesOrderDetail)
    {
        return toAjax(salesOrderDetailService.insertSalesOrderDetail(salesOrderDetail));
    }

    /**
     * 新增销售单商品明细
     */
    //@PreAuthorize("@ss.hasPermi('sales:detail:add')")
    @Log(title = "销售单商品明细", businessType = BusinessType.INSERT)
    @PostMapping("/updateStatus")
    public AjaxResult updateStatus(@Valid @RequestBody SalesOrderDetail salesOrderDetail)
    {
        return salesOrderDetailService.updateStatus(salesOrderDetail);
    }

    /**
     * 修改销售单商品明细
     */
    //@PreAuthorize("@ss.hasPermi('sales:detail:edit')")
    @Log(title = "销售单商品明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody SalesOrderDetail salesOrderDetail)
    {
        return toAjax(salesOrderDetailService.updateSalesOrderDetail(salesOrderDetail));
    }

    /**
     * 删除销售单商品明细
     */
    //@PreAuthorize("@ss.hasPermi('sales:detail:remove')")
    @Log(title = "销售单商品明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(salesOrderDetailService.deleteSalesOrderDetailByIds(ids));
    }
}
