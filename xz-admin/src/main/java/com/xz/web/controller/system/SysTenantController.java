package com.xz.web.controller.system;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xz.common.annotation.Log;
import com.xz.common.constant.UserConstants;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.domain.entity.SysTenant;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.SecurityUtils;
import com.xz.common.utils.StringUtils;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.system.domain.SysTenantConfig;
import com.xz.system.service.ISysDeptService;
import com.xz.system.service.ISysTenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 参数配置 信息操作处理
 *
 * @author xz
 */
@RestController
@RequestMapping("/system/tenant")
public class SysTenantController extends BaseController {
    @Autowired
    private ISysTenantService sysTenantService;
    @Autowired
    private ISysDeptService deptService;
    /**
     * 获取租户列表
     */
    //@PreAuthorize("@ss.hasPermi('system:tenant:list')")
    @GetMapping("/list")
    public TableDataInfo list(HttpServletRequest request) {
        Page<SysTenant> page = sysTenantService.page(getPage(), createQueryWrapper(request));
        return getDataTable2(page.getRecords(),new Long(page.getTotal()).intValue());
    }

    @GetMapping("/findMyTenantList/{phone}")
    public AjaxResult findMyTenantList(@PathVariable("phone") String phone){

        List<SysTenant> sysTenants = sysTenantService.listByIds(null);
        return AjaxResult.success(sysTenants);
    }


    /**
     * 导出租户管理列表
     */
    //@PreAuthorize("@ss.hasPermi('system:tenant:export')")
    @Log(title = "租户管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HttpServletRequest request) {
        List<SysTenant> list = sysTenantService.list(createQueryWrapper(request));
        ExcelUtil<SysTenant> util = new ExcelUtil<>(SysTenant.class);
        return util.exportExcel(list, "租户管理数据");
    }

    /**
     * 获取租户管理详细信息
     */
    //@PreAuthorize("@ss.hasPermi('system:tenant:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(sysTenantService.getSysTenantById(id));
    }

    /**
     * 新增租户管理
     */
    //@PreAuthorize("@ss.hasPermi('system:tenant:add')")
    @Log(title = "租户管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysTenant sysTenant) {
        if (UserConstants.NOT_UNIQUE.equals(sysTenantService.checkNameOrCodeUnique(sysTenant)))
        {
            return AjaxResult.error("新增租户'" + sysTenant.getTenantName() + "'失败，租户名称或租户编号已存在");
        }
        sysTenant.setCreateBy(SecurityUtils.getLoginUser().getUserId());
        return toAjax(sysTenantService.save(sysTenant));
    }

    /**
     * 修改租户管理
     */
    //@PreAuthorize("@ss.hasPermi('system:tenant:edit')")
    @Log(title = "租户管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysTenant sysTenant) {
        if (UserConstants.NOT_UNIQUE.equals(sysTenantService.checkNameOrCodeUnique(sysTenant)))
        {
            return AjaxResult.error("修改租户'" + sysTenant.getTenantName() + "'失败，租户名称或租户编号已存在");
        }
        return toAjax(sysTenantService.updateTenant(sysTenant));
    }
    /**
     * 获取租户配置
     */
    //@PreAuthorize("@ss.hasPermi('system:tenant:query')")
    @GetMapping(value = "/tenantConfig/{id}")
    public AjaxResult getTenantConfig(@PathVariable("id") Long id) {
        return AjaxResult.success(sysTenantService.getTenantConfig(id));
    }
    /**
     * 修改租户配置
     */
    //@PreAuthorize("@ss.hasPermi('system:tenant:edit')")
    @Log(title = "修改租户配置", businessType = BusinessType.UPDATE)
    @PutMapping(value = "/editConfig")
    public AjaxResult editConfig(@RequestBody SysTenantConfig sysTenantConfig) {
        return toAjax(sysTenantService.updateTenantConfig(sysTenantConfig));
    }
    /**
     * 删除租户管理
     */
    /*//@PreAuthorize("@ss.hasPermi('system:tenant:remove')")
    @Log(title = "租户管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{infoIds}")
    public AjaxResult remove(@PathVariable Long[] infoIds) {
        return toAjax(sysTenantService.removeByIds(Arrays.asList(infoIds)));
    }*/
    /**
     * 删除租户管理
     */
    //@PreAuthorize("@ss.hasPermi('system:tenant:remove')")
    @Log(title = "租户管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{infoIds}")
    public AjaxResult remove(@PathVariable Long infoIds) {
        return toAjax(sysTenantService.deleteById(infoIds,getUsername()));
    }

    private QueryWrapper createQueryWrapper(HttpServletRequest request){
        QueryWrapper<SysTenant> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("a.tenant_id");
        String tenantCode = request.getParameter("tenantCode");
        if (!StringUtils.isEmpty(tenantCode)) {
            queryWrapper.eq("tenant_code", tenantCode);
        }
        String tenantConcat = request.getParameter("tenantConcat");
        if (!StringUtils.isEmpty(tenantConcat)) {
            queryWrapper.like("tenant_concat", tenantConcat);
        }
        String tenantPhone = request.getParameter("tenantPhone");
        if (!StringUtils.isEmpty(tenantPhone)) {
            queryWrapper.eq("tenant_phone", tenantPhone);
        }
        String tenantName = request.getParameter("tenantName");
        if (!StringUtils.isEmpty(tenantName)) {
            queryWrapper.like("tenant_name", tenantName);
        }
        return queryWrapper;
    }
    /**
     *  ota获取车牌信息
     */
    @GetMapping("/getOtaSynvehicle")
    public AjaxResult getOtaSynvehicle(){
        return sysTenantService.getOtaSynvehicle();
    }
    /**
     *  ota修改同步车牌状态
     */
    @PostMapping("/updateOtaSynvehicle")
    public AjaxResult updateOtaSynvehicle(@RequestBody SysTenantConfig sysTenantConfig){
        return sysTenantService.updateOtaSynvehicle(sysTenantConfig);
    }
}
