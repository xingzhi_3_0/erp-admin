package com.xz.web.controller.process;

import java.util.List;

import com.xz.process.domain.ProcessOrderItem;
import com.xz.process.dto.ProcessOrderFinishedDto;
import com.xz.process.vo.MaterialRequisitionProcessingVo;
import com.xz.process.vo.ProcessOrderExportVo;
import com.xz.process.vo.ProcessOrderVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.process.domain.ProcessOrder;
import com.xz.process.service.IProcessOrderService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 加工单Controller
 *
 * @author xz
 * @date 2024-02-04
 */
@RestController
@RequestMapping("/process/order")
public class ProcessOrderController extends BaseController {
    @Autowired
    private IProcessOrderService processOrderService;

    /**
     * 查询加工单列表
     */
    //@PreAuthorize("@ss.hasPermi('process:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProcessOrder processOrder) {
        startPage();
        List<ProcessOrderVo> list = processOrderService.selectProcessOrderList(processOrder);
        return getDataTable(list);
    }

    /**
     * 导出加工单列表
     */
    //@PreAuthorize("@ss.hasPermi('process:order:export')")
    @Log(title = "加工单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ProcessOrder processOrder) {
        List<ProcessOrderExportVo> list = processOrderService.selectProcessOrderExportList(processOrder);
        ExcelUtil<ProcessOrderExportVo> util = new ExcelUtil<ProcessOrderExportVo>(ProcessOrderExportVo.class);
        return util.exportExcel(list, "加工管理导出");
    }

    /**
     * 获取加工单详细信息
     */
    //@PreAuthorize("@ss.hasPermi('process:order:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(processOrderService.selectProcessOrderById(id));
    }

    /**
     * 新增加工单
     */
    //@PreAuthorize("@ss.hasPermi('process:order:add')")
    @Log(title = "加工单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody ProcessOrder processOrder) {
        boolean insertProcessOrder = processOrderService.insertProcessOrder(processOrder);

        return insertProcessOrder? AjaxResult.success():AjaxResult.error();
    }

    /**
     * 修改加工单
     */
    //@PreAuthorize("@ss.hasPermi('process:order:edit')")
    @Log(title = "加工单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody ProcessOrder processOrder) {
        return processOrderService.updateProcessOrder(processOrder);
    }
    /**
     * 变更加工师
     */
    @PostMapping("/changeMachinist")
    public AjaxResult changeMachinist( @RequestBody ProcessOrder processOrder) {
        return processOrderService.changeMachinist(processOrder);
    }

    /**
     * 删除加工单
     */
    //@PreAuthorize("@ss.hasPermi('process:order:remove')")
    @Log(title = "加工单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(processOrderService.deleteProcessOrderByIds(ids));
    }

    /**
     * 物料签收
     */
    @PostMapping("/materielSigner")
    public AjaxResult materielSigner(@RequestBody ProcessOrderItem processOrderItem){
        return processOrderService.materielSigner(processOrderItem);
    }
    /**
     * 客户自备镜框签收
     */
    @PostMapping("/eyeglassFramesSigner")
    public AjaxResult eyeglassFramesSigner(@RequestBody ProcessOrder processOrder){
        return processOrderService.eyeglassFramesSigner(processOrder);
    }
    /**
     * 客户自备镜框签--短信通知
     */
    @PostMapping("/eyeglassFramesSmsNotification")
    public AjaxResult eyeglassFramesSmsNotification(@RequestBody ProcessOrder processOrder){
        return processOrderService.eyeglassFramesSmsNotification(processOrder);
    }
    /**
     * 客户自备镜框签--短信通知--发前模板查看
     */
    @GetMapping("/getSmsNotificationContext")
    public AjaxResult getSmsNotificationContext(Long orderId, Integer msgType){
        return processOrderService.getSmsNotificationContext(orderId,msgType);
    }
    /**
     * 领料加工
     */
    @PostMapping("/materialRequisitionProcessing")
    public AjaxResult materialRequisitionProcessing(@RequestBody MaterialRequisitionProcessingVo processingVo){
        return processOrderService.materialRequisitionProcessing(processingVo);
    }
    /**
     * 加工报损
     */
    @PostMapping("/reportDamage")
    public AjaxResult reportDamage(@RequestBody ProcessOrderItem orderItem){
        return processOrderService.reportDamage(orderItem);
    }
    /**
     * 成品配送
     */
    @PostMapping("/finishedProductDistribution")
    public AjaxResult finishedProductDistribution(@RequestBody ProcessOrderFinishedDto processOrder){
        return processOrderService.finishedProductDistribution(processOrder);
    }
    /**
     * 定制采购
     */
    @PostMapping("/customPurchasing")
    public AjaxResult customPurchasing(@RequestBody ProcessOrderItem orderItem) {
        return processOrderService.customPurchasing(orderItem);
    }

}
