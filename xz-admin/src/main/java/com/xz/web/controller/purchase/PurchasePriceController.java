package com.xz.web.controller.purchase;

import java.util.List;

import com.xz.product.vo.ProductImportVo;
import com.xz.purchase.dto.PurchasePriceDto;
import com.xz.purchase.vo.PurchasePriceImportVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.purchase.domain.PurchasePrice;
import com.xz.purchase.service.IPurchasePriceService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 进货价Controller
 *
 * @author xz
 * @date 2024-01-08
 */
@RestController
@RequestMapping("/purchase/price")
public class PurchasePriceController extends BaseController {
    @Autowired
    private IPurchasePriceService purchasePriceService;

    /**
     * 查询进货价列表
     */
    //@PreAuthorize("@ss.hasPermi('purchase:price:list')")
    @GetMapping("/list")
    public TableDataInfo list(PurchasePrice purchasePrice) {
        startPage();
        List<PurchasePriceDto> list = purchasePriceService.selectPurchasePriceList(purchasePrice);
        return getDataTable(list);
    }

    /**
     * 导出进货价列表
     */
    //@PreAuthorize("@ss.hasPermi('purchase:price:export')")
    @Log(title = "进货价", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PurchasePrice purchasePrice) {
        List<PurchasePriceDto> list = purchasePriceService.selectPurchasePriceList(purchasePrice);
        ExcelUtil<PurchasePriceDto> util = new ExcelUtil<PurchasePriceDto>(PurchasePriceDto.class);
        return util.exportExcel(list, "进货价数据");
    }

    /**
     * 获取进货价详细信息
     */
    //@PreAuthorize("@ss.hasPermi('purchase:price:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(purchasePriceService.selectPurchasePriceById(id));
    }

    /**
     * 修改进货价
     */
    //@PreAuthorize("@ss.hasPermi('purchase:price:edit')")
    @Log(title = "进货价", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody PurchasePrice purchasePrice) {
        return purchasePriceService.updatePurchasePrice(purchasePrice);
    }
    /**
     * 导入进货价
     */
    @Log(title = "导入进货价", businessType = BusinessType.EXPORT)
    @PostMapping("/importExcel")
    public AjaxResult importExcel(@RequestParam("file") MultipartFile file) throws Exception {
        ExcelUtil<PurchasePriceImportVo> util = new ExcelUtil<PurchasePriceImportVo>(PurchasePriceImportVo.class);
        List<PurchasePriceImportVo> list = util.importExcel(file.getInputStream());
        AjaxResult message = purchasePriceService.importExcel(list);
        return message;
    }
    /**
     * 删除进货价
     */
    //@PreAuthorize("@ss.hasPermi('purchase:price:remove')")
    @Log(title = "进货价", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(purchasePriceService.deletePurchasePriceByIds(ids));
    }
}
