package com.xz.web.controller.member;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.member.domain.MemberInfo;
import com.xz.member.domain.RechargeActivity;
import com.xz.member.param.MemberQueryParam;
import com.xz.member.service.IMemberInfoService;
import com.xz.member.service.IRechargeActivityService;
import com.xz.member.vo.MemberInfoVo;
import com.xz.member.vo.RechargeActivityVo;
import com.xz.purchase.domain.Purchase;
import com.xz.purchase.service.IWarningSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 采购调整Controller
 *
 * @author xz
 * @date 2024-1-16
 */
@RestController
@RequestMapping("/member/rechargeActivity")
public class RechargeActivityController extends BaseController {
    @Autowired
    private IWarningSettingsService warningSettingsService;

    @Autowired
    private IRechargeActivityService rechargeActivityService;

    /**
     * 查询充值活动列表
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:list')")
    @GetMapping("/list")
    public TableDataInfo list(RechargeActivity activity) {
        startPage();
        List<RechargeActivity> rechargeActivities = rechargeActivityService.queryList(activity);
        return getDataTable(rechargeActivities);
    }

    /**
     * 获取充值活动
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(rechargeActivityService.activityDetailById(id));
    }

    /**
     * 新增充值活动
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:add')")
    @Log(title = "充值活动", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody RechargeActivityVo rechargeActivityVo) {
        return rechargeActivityService.addActivity(rechargeActivityVo);
    }


    /**
     * 编辑充值活动
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:edit')")
    @Log(title = "充值活动", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody RechargeActivityVo rechargeActivityVo) {
        return rechargeActivityService.updateActivity(rechargeActivityVo);
    }

    /**
     * 作废充值活动
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:add')")
    @Log(title = "充值活动", businessType = BusinessType.UPDATE)
    @PostMapping("/invalidInfo")
    public AjaxResult invalidInfo(@Valid @RequestBody RechargeActivity rechargeActivity) {
        return rechargeActivityService.invalid(rechargeActivity);
    }
}
