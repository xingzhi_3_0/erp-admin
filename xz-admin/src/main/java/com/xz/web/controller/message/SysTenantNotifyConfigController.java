package com.xz.web.controller.message;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.SecurityUtils;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.message.domain.SysTenantNotifyConfig;
import com.xz.message.service.ISysTenantNotifyConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 租户消息通知配置Controller
 *
 * @author xz
 * @date 2023-08-04
 */
@RestController
@RequestMapping("/notify/config")
public class SysTenantNotifyConfigController extends BaseController {
    @Autowired
    private ISysTenantNotifyConfigService sysTenantNotifyConfigService;

    /**
     * 查询租户消息通知配置列表
     */
    //@PreAuthorize("@ss.hasPermi('notify:notify:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysTenantNotifyConfig sysTenantNotifyConfig)
    {
        startPage();
        List<SysTenantNotifyConfig> list = sysTenantNotifyConfigService.selectSysTenantNotifyConfigList(sysTenantNotifyConfig);
        return getDataTable(list);
    }

    /**
     * 导出租户消息通知配置列表
     */
    //@PreAuthorize("@ss.hasPermi('notify:notify:export')")
    @Log(title = "租户消息通知配置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysTenantNotifyConfig sysTenantNotifyConfig)
    {
        List<SysTenantNotifyConfig> list = sysTenantNotifyConfigService.selectSysTenantNotifyConfigList(sysTenantNotifyConfig);
        ExcelUtil<SysTenantNotifyConfig> util = new ExcelUtil<SysTenantNotifyConfig>(SysTenantNotifyConfig.class);
        return util.exportExcel(list, "租户消息通知配置数据");
    }

    /**
     * 获取租户消息通知配置详细信息
     */
    //@PreAuthorize("@ss.hasPermi('notify:notify:query')")
    @GetMapping(value = "/getInfo")
    public AjaxResult getInfo()
    {
        return AjaxResult.success(sysTenantNotifyConfigService.selectNotifyConfigByTenantId(SecurityUtils.getLoginUser().getTenantId()));
    }

    /**
     * 新增租户消息通知配置
     */
    //@PreAuthorize("@ss.hasPermi('notify:notify:add')")
    @Log(title = "租户消息通知配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody SysTenantNotifyConfig sysTenantNotifyConfig)
    {
        return toAjax(sysTenantNotifyConfigService.insertSysTenantNotifyConfig(sysTenantNotifyConfig));
    }

    /**
     * 修改租户消息通知配置
     */
    //@PreAuthorize("@ss.hasPermi('notify:notify:edit')")
    @Log(title = "租户消息通知配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody SysTenantNotifyConfig sysTenantNotifyConfig)
    {
        return toAjax(sysTenantNotifyConfigService.updateSysTenantNotifyConfig(sysTenantNotifyConfig));
    }

    /**
     * 删除租户消息通知配置
     */
    //@PreAuthorize("@ss.hasPermi('notify:notify:remove')")
    @Log(title = "租户消息通知配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sysTenantNotifyConfigService.deleteSysTenantNotifyConfigByIds(ids));
    }



}
