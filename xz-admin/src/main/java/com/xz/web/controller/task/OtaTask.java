package com.xz.web.controller.task;

import com.xz.system.service.ISysTenantService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2023/6/18 15:04
 * @description：ota 预警
 */
@Component
@Configuration      // 1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
public class OtaTask {

    private static  final Logger log = LoggerFactory.getLogger(OtaTask.class);
    @Resource
    private RedisTemplate redisTemplate;
    @Autowired
    private ISysTenantService sysTenantService;

    /**
     * ota预警
     */
    //@Scheduled(cron = "0 0/3 * * * ? ")
    public void ota(){
    }

}
