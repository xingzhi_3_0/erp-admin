package com.xz.web.controller.purchaseOrder;

import com.xz.common.annotation.Log;
import com.xz.common.config.XzConfig;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.core.redis.RedisCache;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.SecurityUtils;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.purchaseOrder.domain.PurchaseOrder;
import com.xz.purchaseOrder.dto.ApplyReorderDto;
import com.xz.purchaseOrder.dto.TerminationPurchaseDto;
import com.xz.purchaseOrder.service.IPurchaseOrderService;
import com.xz.purchaseOrder.vo.PurchaseOrderVo;
import com.xz.purchaseOrder.vo.WaitingListVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * 采购订单Controller
 *
 * @author xz
 * @date 2024-01-22
 */
@RestController
@RequestMapping("/purchaseOrder/order")
public class PurchaseOrderController extends BaseController {
    @Autowired
    private IPurchaseOrderService purchaseOrderService;
    @Autowired
    private RedisCache redisCache;
    /**
     * 查询采购订单列表
     */
    //@PreAuthorize("@ss.hasPermi('purchaseOrder:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(PurchaseOrder purchaseOrder) {
        startPage();
        List<PurchaseOrderVo> list = purchaseOrderService.selectPurchaseOrderList(purchaseOrder);
        return getDataTable(list);
    }

    /**
     * 导出采购订单列表
     */
    //@PreAuthorize("@ss.hasPermi('purchaseOrder:order:export')")
    @Log(title = "采购订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PurchaseOrder purchaseOrder) {
        List<PurchaseOrderVo> list = purchaseOrderService.selectPurchaseOrderList(purchaseOrder);
        ExcelUtil<PurchaseOrderVo> util = new ExcelUtil<PurchaseOrderVo>(PurchaseOrderVo.class);
        return util.exportExcel(list, "采购订单数据");
    }

    /**
     * 获取采购订单详细信息
     */
    //@PreAuthorize("@ss.hasPermi('purchaseOrder:order:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(purchaseOrderService.selectPurchaseOrderById(id));
    }

    /**
     * 新增采购订单
     */
    //@PreAuthorize("@ss.hasPermi('purchaseOrder:order:add')")
    @Log(title = "采购订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody PurchaseOrder purchaseOrder) {
        return purchaseOrderService.insertPurchaseOrder(purchaseOrder);
    }

    /**
     * 修改
     */
    //@PreAuthorize("@ss.hasPermi('purchaseOrder:order:edit')")
    @Log(title = "采购订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody PurchaseOrder purchaseOrder) {
        return purchaseOrderService.updatePurchaseOrder(purchaseOrder);
    }

    /**
     * 采购终止
     */
    //@PreAuthorize("@ss.hasPermi('purchaseOrder:order:remove')")
    @Log(title = "采购订单", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{id}")
    public AjaxResult delete(@PathVariable Long id) {
        return purchaseOrderService.deletePurchaseOrderById(id);
    }

    /**
     * 获取最后提交的供应商
     */
    @GetMapping("/applyReorder/lastSupplier")
    public AjaxResult getLastSupplierForApplyReorder(){
      String key = XzConfig.getRedisPrefix() + "applyreorder:lastsupplier:" + SecurityUtils.getUserId();
      Map<String,String> map = redisCache.getCacheMap(key);
      return AjaxResult.success(map);
    }
    /**
     * 门店采购申请转订单
     */
    @PostMapping("/applyReorder")
    public AjaxResult applyReorder(@Valid @RequestBody ApplyReorderDto applyReorderDto){
        return purchaseOrderService.applyReorder(applyReorderDto);
    }
    /**
     * 删除
     */
    @PostMapping("/terminationPurchase")
    public AjaxResult terminationPurchase(@Valid @RequestBody TerminationPurchaseDto purchaseDto) {
        return purchaseOrderService.terminationPurchase(purchaseDto);
    }
    /**
     * 待入库列表
     */
    //@PreAuthorize("@ss.hasPermi('purchaseOrder:order:list')")
    @GetMapping("/getWaitingList")
    public TableDataInfo getWaitingList(PurchaseOrder purchaseOrder) {
        startPage();
        List<WaitingListVo> waitingList = purchaseOrderService.getWaitingList(purchaseOrder);
        return getDataTable(waitingList);
    }
}
