package com.xz.web.controller.supplierContract;

import java.util.ArrayList;
import java.util.List;

import com.xz.product.domain.Product;
import com.xz.supplierContract.dto.SupplierContractDto;
import com.xz.supplierContract.vo.SupplierContractDetailVo;
import com.xz.supplierContract.vo.SupplierContractVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.supplierContract.domain.SupplierContract;
import com.xz.supplierContract.service.ISupplierContractService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 合同Controller
 *
 * @author xz
 * @date 2024-02-26
 */
@RestController
@RequestMapping("/supplier/contract")
public class SupplierContractController extends BaseController {
  @Autowired
  private ISupplierContractService supplierContractService;


  /**
   * 查询合同列表
   */
  //@PreAuthorize("@ss.hasPermi('supplier:contract:list')")
  @GetMapping("/list")
  public TableDataInfo list(SupplierContract supplierContract) {
    startPage();
    List<SupplierContractVo> list = supplierContractService.selectSupplierContractVoList(supplierContract);
    return getDataTable(list);
  }


  /**
   * 查询贸易供应商当下合同
   */
  //@PreAuthorize("@ss.hasPermi('supplier:contract:query')")
  @GetMapping(value = "/supplier/{id}")
  public AjaxResult selectCurrentBySupplierId(@PathVariable("id") Long id) {
    return AjaxResult.success(supplierContractService.selectCurrentBySupplierId(id));
  }

  /**
   * 获取合同详细信息
   */
  //@PreAuthorize("@ss.hasPermi('supplier:contract:query')")
  @GetMapping(value = "/{id}")
  public AjaxResult getInfo(@PathVariable("id") Long id) {
    SupplierContractDetailVo data = supplierContractService.selectSupplierContractVoById(id);
    return AjaxResult.success(data);
  }

  /**
   * 新增合同
   */
  //@PreAuthorize("@ss.hasPermi('supplier:contract:add')")
  @Log(title = "合同", businessType = BusinessType.INSERT)
  @PostMapping
  public AjaxResult add(@Valid @RequestBody SupplierContractDto supplierContract) {
    return toAjax(supplierContractService.insertSupplierContract(supplierContract));
  }

  /**
   * 修改合同
   */
  //@PreAuthorize("@ss.hasPermi('supplier:contract:edit')")
  @Log(title = "合同", businessType = BusinessType.UPDATE)
  @PutMapping
  public AjaxResult edit(@Valid @RequestBody SupplierContractDto supplierContract) {
    return toAjax(supplierContractService.updateSupplierContract(supplierContract));
  }

  /**
   * 修改状态
   */
  //@PreAuthorize("@ss.hasPermi('supplier:contract:edit')")
  @Log(title = "合同", businessType = BusinessType.UPDATE)
  @PutMapping("status")
  public AjaxResult editStatus(@RequestBody SupplierContractDto supplierContract) {
    return toAjax(supplierContractService.editStatus(supplierContract.getId(),supplierContract.getContractStatus()));
  }

  /**
   * 删除合同
   */
  //@PreAuthorize("@ss.hasPermi('supplier:contract:remove')")
  @Log(title = "合同", businessType = BusinessType.DELETE)
  @DeleteMapping("/{ids}")
  public AjaxResult remove(@PathVariable Long[] ids) {
    return toAjax(supplierContractService.deleteSupplierContractByIds(ids));
  }
}
