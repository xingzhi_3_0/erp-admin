package com.xz.web.controller.member;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.StringUtils;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.member.domain.MemberInfo;
import com.xz.member.domain.MemberPatient;
import com.xz.member.param.MemberQueryParam;
import com.xz.member.service.IMemberInfoService;
import com.xz.member.vo.MemberImportVo;
import com.xz.member.vo.MemberInfoVo;
import com.xz.member.vo.RechargeActivityVo;
import com.xz.patient.vo.PatientImportVo;
import com.xz.purchase.domain.WarningSettings;
import com.xz.purchase.param.ReturnInfoParam;
import com.xz.purchase.param.WarningSettingParam;
import com.xz.purchase.service.IWarningSettingsService;
import com.xz.purchase.vo.WarningSettingsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 采购调整Controller
 *
 * @author xz
 * @date 2024-1-16
 */
@RestController
@RequestMapping("/member/memberInfo")
public class MemberInfoController extends BaseController {
    @Autowired
    private IWarningSettingsService warningSettingsService;

    @Autowired
    private IMemberInfoService memberInfoService;

    /**
     * 查询会员列表
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:list')")
    @GetMapping("/list")
    public TableDataInfo list(MemberQueryParam queryParam) {
        startPage();
        List<MemberInfoVo> memberInfoVos = memberInfoService.queryList(queryParam);
        return getDataTable(memberInfoVos);
    }

    /**
     * 导出会员信息列表
     */
    //@PreAuthorize("@ss.hasPermi('patient:patient:export')")
    @Log(title = "会员信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(MemberQueryParam queryParam)
    {
        List<MemberInfoVo> memberInfoVos = memberInfoService.queryList(queryParam);
        ExcelUtil<MemberInfoVo> util = new ExcelUtil<MemberInfoVo>(MemberInfoVo.class);
        return util.exportExcel(memberInfoVos, "会员信息数据");
    }

    /**
     * 获取会员信息
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(memberInfoService.memberDetailById(id));
    }

  /**
   * 获取会员账户信息
   */
  //@PreAuthorize("@ss.hasPermi('purchase:purchase:query')")
  @GetMapping(value = "/balance")
  public AjaxResult getBalance(String memberCardNo) {
    MemberInfo member = null;
    if (!StringUtils.isEmpty(memberCardNo)) {
      member = memberInfoService.selectByMemberCarNo(memberCardNo);
    }
    Map<String, Object> map = new HashMap<String, Object>();
    if (null == member) {
      map.put("cardNo", memberCardNo);
      map.put("walletBalance", 0.00D);
      map.put("integralBalance", 0.00D);
      map.put("accumulatedRecharge", 0.00D);
    } else {
      map.put("cardNo", memberCardNo);
      map.put("walletBalance", member.getWalletBalance() == null ? BigDecimal.ZERO : member.getWalletBalance());
      map.put("integralBalance", member.getIntegralBalance() == null ? BigDecimal.ZERO : member.getIntegralBalance());
      map.put("accumulatedRecharge", member.getAccumulatedRecharge() == null ? BigDecimal.ZERO : member.getAccumulatedRecharge());
    }
    return AjaxResult.success(map);
  }


  /**
     * 编辑会员信息
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:edit')")
    @Log(title = "会员信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody MemberInfo MemberInfo) {
        return memberInfoService.edit(MemberInfo);
    }

    /**
     * 导入会员信息
     */
    @Log(title = "导入患者信息", businessType = BusinessType.EXPORT)
    @PostMapping("/importExcel")
    public AjaxResult importExcel(@RequestParam("file") MultipartFile file) throws Exception {
        ExcelUtil<MemberImportVo> util = new ExcelUtil<MemberImportVo>(MemberImportVo.class);
        List<MemberImportVo> list = util.importExcel("会员信息",file.getInputStream(),3);
        AjaxResult message = memberInfoService.importExcel(list);
        return message;
    }
}
