package com.xz.web.controller.audit;

import com.xz.audit.domain.AuditInfo;
import com.xz.audit.domain.AuditNode;
import com.xz.audit.dto.AuditNodeDto;
import com.xz.audit.service.IAuditNodeService;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 审核Controller
 *
 * @author xz
 * @date 2023-09-27
 */
@RestController
@RequestMapping("/audit/node")
public class AuditNodeController extends BaseController {
    @Autowired
    private IAuditNodeService auditNodeService;

    /**
     * 查询审核列表
     */
    //@PreAuthorize("@ss.hasPermi('audit:node:list')")
    @GetMapping("/list")
    public TableDataInfo list(AuditNode auditNode) {
        startPage();
        List<AuditNodeDto> list = auditNodeService.selectAuditNodeList(auditNode);
        return getDataTable(list);
    }

    /**
     * 导出审核列表
     */
    //@PreAuthorize("@ss.hasPermi('audit:node:export')")
    @Log(title = "审核", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AuditNode auditNode) {
        List<AuditNodeDto> list = auditNodeService.selectAuditNodeList(auditNode);
        ExcelUtil<AuditNodeDto> util = new ExcelUtil<AuditNodeDto>(AuditNodeDto.class);
        return util.exportExcel(list, "审核数据");
    }

    /**
     * 获取审核详细信息
     */
    //@PreAuthorize("@ss.hasPermi('audit:node:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(auditNodeService.selectAuditNodeById(id));
    }

    /**
     * 节点明细
     */
    //@PreAuthorize("@ss.hasPermi('audit:node:query')")
    @PostMapping(value = "getSortList")
    public AjaxResult getSortList(@RequestBody AuditNode auditNode) {
        return AjaxResult.success(auditNodeService.getSortList(auditNode));
    }

    /**
     * 新增审核
     */
    //@PreAuthorize("@ss.hasPermi('audit:node:add')")
    @Log(title = "审核", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody AuditNode auditNode) {
        return auditNodeService.insertAuditNode(auditNode);
    }

    /**
     * 修改审核
     */
    //@PreAuthorize("@ss.hasPermi('audit:node:edit')")
    @Log(title = "审核", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody AuditNode auditNode) {
        return auditNodeService.updateAuditNode(auditNode);
    }

    /**
     * 删除审核
     */
    //@PreAuthorize("@ss.hasPermi('audit:node:remove')")
    @Log(title = "审核", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{id}")
    public AjaxResult delete(@PathVariable Long id) {
        return auditNodeService.deleteAuditNodeById(id);
    }
}
