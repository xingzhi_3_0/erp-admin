package com.xz.web.controller.sales;

import java.util.List;

import com.xz.common.utils.SecurityUtils;
import com.xz.common.utils.StringUtils;
import com.xz.diagnosisOrder.utils.DiagnosisOrderUtils;
import com.xz.member.vo.MemberImportVo;
import com.xz.product.domain.Product;
import com.xz.product.dto.ProductExportDto;
import com.xz.sales.param.SalesOrderParam;
import com.xz.sales.vo.SalesNoFlowVo;
import com.xz.sales.vo.SalesOrderVo;
import com.xz.sales.vo.SalesPurchaseNoFlowVo;
import com.xz.sales.vo.SalesServiceVo;
import com.xz.system.service.ISysRoleService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import com.xz.sales.domain.SalesOrder;
import com.xz.sales.service.ISalesOrderService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 销售单信息Controller
 * 
 * @author xz
 * @date 2024-01-31
 */
@RestController
@RequestMapping("/sales/order")
public class SalesOrderController extends BaseController {
    @Autowired
    private ISalesOrderService salesOrderService;
    @Autowired
    private ISysRoleService sysRoleService;

    /**
     * 查询销售单信息列表
     */
    //@PreAuthorize("@ss.hasPermi('sales:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(SalesOrder salesOrder)
    {
//        // 获取当前登录人可看到部门
//        List<Long> list1 = sysRoleService.myQueryDeptId();
//        if(list1 != null && list1.size()==0){
//            // 0 看自己部门
//            salesOrder.setQueryDeptIdListStr(SecurityUtils.getLoginUser().getDeptId().toString());
//        }else if(list1 != null && list1.size()>0){
//            // 大于0  自定义
//            salesOrder.setQueryDeptIdListStr(StringUtils.join(list1,","));
//        }
        startPage();
        List<SalesOrder> list = salesOrderService.selectSalesOrderList(salesOrder);
        return getDataTable(list);
    }

    @GetMapping("/serviceList")
    public TableDataInfo serviceList(SalesOrder salesOrder)
    {
        startPage();
        List<SalesServiceVo> salesServiceVos = salesOrderService.serviceList(salesOrder);
        return getDataTable(salesServiceVos);
    }


    /**
     * 导出销售单信息列表
     */
    //@PreAuthorize("@ss.hasPermi('sales:order:export')")
    @Log(title = "销售单信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public void export(SalesOrder salesOrder, HttpServletResponse response)
    {
        // 获取当前登录人可看到部门
        List<Long> list1 = sysRoleService.myQueryDeptId();
        if(list1 != null && list1.size()==0){
            // 0 看自己部门
            salesOrder.setQueryDeptIdListStr(SecurityUtils.getLoginUser().getDeptId().toString());
        }else if(list1 != null && list1.size()>0){
            // 大于0  自定义
            salesOrder.setQueryDeptIdListStr(StringUtils.join(list1,","));
        }
        salesOrderService.export222(salesOrder,response);

    }

//    /**
//     * 导出商品列表
//     */
//    //@PreAuthorize("@ss.hasPermi('product:product:export')")
//    @Log(title = "商品", businessType = BusinessType.EXPORT)
//    @GetMapping("/export")
//    public AjaxResult export(Product product) {
//        List<ProductExportDto> list = productService.selectProductExport(product);
//        ExcelUtil<ProductExportDto> util = new ExcelUtil<ProductExportDto>(ProductExportDto.class);
//        return util.exportExcel(list, "商品数据");
//    }


    /**
     * 获取销售单信息详细信息
     */
    //@PreAuthorize("@ss.hasPermi('sales:order:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        SalesOrderVo data = salesOrderService.selectSalesOrderById(id);
        if(null != data){
            data.setPatientInfo(DiagnosisOrderUtils.getPatientInfo(data.getPatientName(), data.getSex(), data.getBirthday()));
        }
        return AjaxResult.success(data);
    }

    /**
     * 新增销售单信息
     */
    //@PreAuthorize("@ss.hasPermi('sales:order:add')")
    @Log(title = "销售单信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody SalesOrderParam salesOrderParam)
    {
        return salesOrderService.insertSalesOrder(salesOrderParam);
    }

    /**
     * 确认收款
     */
    //@PreAuthorize("@ss.hasPermi('sales:order:add')")
    @Log(title = "销售单信息", businessType = BusinessType.INSERT)
    @PostMapping("/confirmCollection")
    public AjaxResult confirmCollection(@Valid @RequestBody SalesOrderParam salesOrder)
    {
        return salesOrderService.confirmCollectionTwo(salesOrder);
    }

    /**
     * 确认收款
     */
    //@PreAuthorize("@ss.hasPermi('sales:order:add')")
    @Log(title = "销售单信息", businessType = BusinessType.INSERT)
    @PostMapping("/confirmCollectionTwo")
    public AjaxResult confirmCollectionTwo(@Valid @RequestBody SalesOrderParam salesOrder)
    {
        return salesOrderService.confirmCollectionTwo(salesOrder);
    }

    /**
     * 确认收款
     */
    //@PreAuthorize("@ss.hasPermi('sales:order:add')")
    @Log(title = "销售单信息", businessType = BusinessType.UPDATE)
    @PostMapping("/cancel")
    public AjaxResult cancel(@Valid @RequestBody SalesOrder salesOrder)
    {
        return salesOrderService.cancel(salesOrder);
    }



    /**
     * 修改销售单信息
     */
    //@PreAuthorize("@ss.hasPermi('sales:order:edit')")
    @Log(title = "销售单信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody SalesOrder salesOrder)
    {
        return toAjax(salesOrderService.updateSalesOrder(salesOrder));
    }

    /**
     * 删除销售单信息
     */
    //@PreAuthorize("@ss.hasPermi('sales:order:remove')")
    @Log(title = "销售单信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(salesOrderService.deleteSalesOrderByIds(ids));
    }


    /**
     * 销售已交付无流水
     */
    @Log(title = "销售已交付无流水", businessType = BusinessType.EXPORT)
    @PostMapping("/importNoFlowExcel")
    public AjaxResult importNoFlowExcel(@RequestParam("file") MultipartFile file) throws Exception {
        ExcelUtil<SalesNoFlowVo> util = new ExcelUtil<SalesNoFlowVo>(SalesNoFlowVo.class);
        List<SalesNoFlowVo> list = util.importExcel("销售单无流水",file.getInputStream(),0);
        AjaxResult message = salesOrderService.importNoFlowExcel(list);
        return message;
    }

    /**
     * 销售已预占无流水
     */
    @Log(title = "销售已预占无流水", businessType = BusinessType.EXPORT)
    @PostMapping("/importSalesPurchaseNoFlowExcel")
    public AjaxResult importSalesPurchaseNoFlowExcel(@RequestParam("file") MultipartFile file) throws Exception {
        ExcelUtil<SalesPurchaseNoFlowVo> util = new ExcelUtil<SalesPurchaseNoFlowVo>(SalesPurchaseNoFlowVo.class);
        List<SalesPurchaseNoFlowVo> list = util.importExcel("Sheet1",file.getInputStream(),0);
        AjaxResult message = salesOrderService.importSalesPurchaseNoFlowExcel(list);
        return message;
    }


    /**
     * 销售未交付预占
     */
    @Log(title = "销售未交付预占", businessType = BusinessType.EXPORT)
    @PostMapping("/importUnUseExcel")
    public AjaxResult importUnUseExcel(@RequestParam("file") MultipartFile file) throws Exception {
        ExcelUtil<SalesNoFlowVo> util = new ExcelUtil<SalesNoFlowVo>(SalesNoFlowVo.class);
        List<SalesNoFlowVo> list = util.importExcel("未预占",file.getInputStream(),0);
        AjaxResult message = salesOrderService.importUnUseExcel(list);
        return message;
    }
}
