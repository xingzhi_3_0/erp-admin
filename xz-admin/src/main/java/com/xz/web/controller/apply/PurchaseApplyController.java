package com.xz.web.controller.apply;

import java.util.List;

import com.xz.apply.vo.ConfirmPurchaseApplyVo;
import com.xz.apply.vo.PurchaseApplyVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.apply.domain.PurchaseApply;
import com.xz.apply.service.IPurchaseApplyService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 采购申请Controller
 *
 * @author xz
 * @date 2024-01-19
 */
@RestController
@RequestMapping("/apply/apply")
public class PurchaseApplyController extends BaseController {
    @Autowired
    private IPurchaseApplyService purchaseApplyService;

    /**
     * 查询采购申请列表
     */
    //@PreAuthorize("@ss.hasPermi('apply:apply:list')")
    @GetMapping("/list")
    public TableDataInfo list(PurchaseApply purchaseApply) {
        startPage();
        List<PurchaseApplyVo> list = purchaseApplyService.selectPurchaseApplyList(purchaseApply);
        return getDataTable(list);
    }
    /**
     * 门店确定采购申请列表
     */
    //@PreAuthorize("@ss.hasPermi('apply:apply:list')")
    @GetMapping("/confirmPurchaseApplyList")
    public TableDataInfo confirmPurchaseApplyList(PurchaseApply purchaseApply) {
        startPage();
        purchaseApply.setProductApplyType(1);
        List<ConfirmPurchaseApplyVo> list = purchaseApplyService.confirmPurchaseApplyList(purchaseApply);
        return getDataTable(list);
    }
    /**
     * 确定定制申请列表
     */
    //@PreAuthorize("@ss.hasPermi('apply:apply:list')")
    @GetMapping("/confirmCustomPurchaseApplyList")
    public TableDataInfo confirmCustomPurchaseApplyList(PurchaseApply purchaseApply) {
        startPage();
        purchaseApply.setProductApplyType(2);
        List<ConfirmPurchaseApplyVo> list = purchaseApplyService.confirmPurchaseApplyList(purchaseApply);
        return getDataTable(list);
    }

    /**
     * 导出采购申请列表
     */
    //@PreAuthorize("@ss.hasPermi('apply:apply:export')")
    @Log(title = "采购申请", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PurchaseApply purchaseApply) {
        List<PurchaseApplyVo> list = purchaseApplyService.selectPurchaseApplyList(purchaseApply);
        ExcelUtil<PurchaseApplyVo> util = new ExcelUtil<PurchaseApplyVo>(PurchaseApplyVo.class);
        return util.exportExcel(list, "采购申请数据");
    }

    /**
     * 获取采购申请详细信息
     */
    //@PreAuthorize("@ss.hasPermi('apply:apply:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(purchaseApplyService.selectPurchaseApplyById(id));
    }

    /**
     * 新增采购申请
     */
    //@PreAuthorize("@ss.hasPermi('apply:apply:add')")
    @Log(title = "采购申请", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody PurchaseApply purchaseApply) {
        purchaseApply.setProductApplyType(1);
        return purchaseApplyService.insertPurchaseApply(purchaseApply);
    }

    /**
     * 修改采购申请
     */
    //@PreAuthorize("@ss.hasPermi('apply:apply:edit')")
    @Log(title = "采购申请", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody PurchaseApply purchaseApply) {
        return purchaseApplyService.updatePurchaseApply(purchaseApply);
    }

    /**
     * 作废采购申请
     */
    //@PreAuthorize("@ss.hasPermi('apply:apply:remove')")
    @Log(title = "作废采购申请", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{id}")
    public AjaxResult delete(@PathVariable("id")  Long id) {
        return toAjax(purchaseApplyService.deletePurchaseApplyById(id));
    }
}
