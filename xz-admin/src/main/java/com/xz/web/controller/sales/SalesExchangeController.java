package com.xz.web.controller.sales;

import java.util.List;

import com.xz.sales.param.SalesExchangeParam;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;
import javax.validation.Valid;
import com.xz.sales.domain.SalesExchange;
import com.xz.sales.service.ISalesExchangeService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 销售单换货Controller
 * 
 * @author xz
 * @date 2024-02-05
 */
@RestController
@RequestMapping("/sales/exchange")
public class SalesExchangeController extends BaseController {
    @Autowired
    private ISalesExchangeService salesExchangeService;

    /**
     * 查询销售单换货列表
     */
    //@PreAuthorize("@ss.hasPermi('sales:exchange:list')")
    @GetMapping("/list")
    public TableDataInfo list(SalesExchange salesExchange)
    {
        startPage();
        List<SalesExchange> list = salesExchangeService.selectSalesExchangeList(salesExchange);
        return getDataTable(list);
    }

    /**
     * 导出销售单换货列表
     */
    //@PreAuthorize("@ss.hasPermi('sales:exchange:export')")
    @Log(title = "销售单换货", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SalesExchange salesExchange)
    {
        List<SalesExchange> list = salesExchangeService.selectSalesExchangeList(salesExchange);
        ExcelUtil<SalesExchange> util = new ExcelUtil<SalesExchange>(SalesExchange.class);
        return util.exportExcel(list, "销售单换货数据");
    }

    /**
     * 获取销售单换货详细信息
     */
    //@PreAuthorize("@ss.hasPermi('sales:exchange:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(salesExchangeService.selectSalesExchangeById(id));
    }

    /**
     * 新增销售单换货
     */
    //@PreAuthorize("@ss.hasPermi('sales:exchange:add')")
    @Log(title = "销售单换货", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody SalesExchangeParam param)
    {
        return salesExchangeService.insertSalesExchange(param);
    }

    /**
     * 修改销售单换货
     */
    //@PreAuthorize("@ss.hasPermi('sales:exchange:edit')")
    @Log(title = "销售单换货", businessType = BusinessType.UPDATE)
    @PostMapping("/cancelSalesExchange")
    public AjaxResult cancelSalesExchange(@Valid @RequestBody SalesExchangeParam exchangeParam)
    {
        return salesExchangeService.cancelSalesExchange(exchangeParam);
    }

    /**
     * 修改销售单换货
     */
    //@PreAuthorize("@ss.hasPermi('sales:exchange:edit')")
    @Log(title = "销售单换货", businessType = BusinessType.UPDATE)
    @PostMapping("/confirmSalesExchange")
    public AjaxResult confirmSalesExchange(@Valid @RequestBody SalesExchangeParam exchangeParam)
    {
        return salesExchangeService.confirmSalesExchange(exchangeParam);
    }


    /**
     * 删除销售单换货
     */
    //@PreAuthorize("@ss.hasPermi('sales:exchange:remove')")
    @Log(title = "销售单换货", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(salesExchangeService.deleteSalesExchangeByIds(ids));
    }
}
