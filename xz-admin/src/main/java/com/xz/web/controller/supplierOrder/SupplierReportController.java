package com.xz.web.controller.supplierOrder;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.sales.domain.SalesReturn;
import com.xz.sales.vo.SalesNoFlowVo;
import com.xz.supplierOrder.domain.SupplierOrderDetail;
import com.xz.supplierOrder.service.ISupplierOrderDetailService;
import com.xz.supplierOrder.vo.SupplierOrderDetailFlowReportVo;
import com.xz.supplierOrder.vo.SupplierOrderDetailFlowSummaryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 贸易中间商订单报表
 *
 * @author xz
 * @date 2024-01-04
 */
@RestController
@RequestMapping("/supplier/salesflow")
public class SupplierReportController extends BaseController {
  @Autowired
  private ISupplierOrderDetailService supplierOrderDetailService;

  /**
   * 查询贸易中间商流水报表
   */
  @GetMapping("/list")
  public TableDataInfo selectReport(SupplierOrderDetail supplier) {
    startPage();
    List<SupplierOrderDetailFlowReportVo> list = supplierOrderDetailService.selectReport(supplier);
    return getDataTable(list);
  }

  /**
   * 贸易中间商销售流水
   */
  @Log(title = "贸易销售流水", businessType = BusinessType.EXPORT)
  @GetMapping("/export")
  public AjaxResult exportReport(SupplierOrderDetail supplier) throws Exception {

    List<SupplierOrderDetailFlowReportVo> list = supplierOrderDetailService.selectReport(supplier);
    ExcelUtil<SupplierOrderDetailFlowReportVo> util = new ExcelUtil<>(SupplierOrderDetailFlowReportVo.class);
    return util.exportExcel(list, "贸易销售流水");
  }

  /**
   * 查询贸易中间商汇总报表（贸易销售）
   */
  @GetMapping("/summary/list")
  public TableDataInfo selectSummary(SupplierOrderDetail supplier) {
    startPage();
    List<SupplierOrderDetailFlowSummaryVo> list = supplierOrderDetailService.selectSummary(supplier);
    return getDataTable(list);
  }

  /**
   * 贸易销售
   */
  @Log(title = "贸易销售", businessType = BusinessType.EXPORT)
  @GetMapping("/summary/export")
  public AjaxResult exportSummary(SupplierOrderDetail supplier) throws Exception {

    List<SupplierOrderDetailFlowSummaryVo> list = supplierOrderDetailService.selectSummary(supplier);
    ExcelUtil<SupplierOrderDetailFlowSummaryVo> util = new ExcelUtil<>(SupplierOrderDetailFlowSummaryVo.class);
    return util.exportExcel(list, "贸易销售");
  }
}
