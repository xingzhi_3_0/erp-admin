package com.xz.web.controller.member;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.member.domain.GiftReceive;
import com.xz.member.domain.IntegralRecord;
import com.xz.member.domain.MemberInfo;
import com.xz.member.domain.WalletRecord;
import com.xz.member.service.IGiftReceiveService;
import com.xz.member.service.IIntegralRecordService;
import com.xz.member.service.IMemberInfoService;
import com.xz.member.service.IWalletRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 钱包记录Controller
 *
 * @author xz
 * @date 2024-1-24 17:09:37
 */
@RestController
@RequestMapping("/member/giftReceive")
public class GiftReceiveController extends BaseController {

    @Autowired
    private IGiftReceiveService giftReceiveService;
    @Autowired
    private IMemberInfoService memberInfoService;

    /**
     * 获取会员赠品卡记录
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:query')")
    @GetMapping("/listByMemberId")
    public TableDataInfo listByMemberId(GiftReceive giftReceive) {
        MemberInfo memberInfo = memberInfoService.getById(giftReceive.getMemberId());
        giftReceive.setMemberCardNo(memberInfo.getMemberCardNo());
        startPage();
        List<GiftReceive> giftReceives = giftReceiveService.listByMemberId(giftReceive);
        return getDataTable(giftReceives);
    }

    @GetMapping("/listByMemberCardNo")
    public List<GiftReceive> listByMemberCardNo(GiftReceive giftReceive) {
        List<GiftReceive> giftReceives = giftReceiveService.listByMemberCardNo(giftReceive);
        return giftReceives;
    }
}
