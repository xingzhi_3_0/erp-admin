package com.xz.web.controller.audit;

import java.util.List;

import com.xz.audit.ProcessApproveVo;
import com.xz.audit.domain.AuditProcess;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.audit.domain.ProcessApprove;
import com.xz.audit.service.IProcessApproveService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 流程审批Controller
 *
 * @author xz
 * @date 2024-01-15
 */
@RestController
@RequestMapping("/audit/approve")
public class ProcessApproveController extends BaseController {
    @Autowired
    private IProcessApproveService processApproveService;

    /**
     * 查询流程审批列表
     */
    //@PreAuthorize("@ss.hasPermi('audit:approve:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProcessApprove processApprove) {
        startPage();
        List<ProcessApproveVo> list = processApproveService.selectProcessApproveList(processApprove);
        return getDataTable(list);
    }
    /**
     * 查询流程定义列表--无分页
     */
    //@PreAuthorize("@ss.hasPermi('audit:process:list')")
    @GetMapping("/processApproveList")
    public AjaxResult processApproveList(ProcessApprove processApprove) {
        List<ProcessApproveVo> list = processApproveService.selectProcessApproveList(processApprove);
        return AjaxResult.success(list);
    }
    /**
     * 导出流程审批列表
     */
    //@PreAuthorize("@ss.hasPermi('audit:approve:export')")
    @Log(title = "流程审批", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ProcessApprove processApprove) {
        List<ProcessApproveVo> list = processApproveService.selectProcessApproveList(processApprove);
        ExcelUtil<ProcessApproveVo> util = new ExcelUtil<ProcessApproveVo>(ProcessApproveVo.class);
        return util.exportExcel(list, "流程审批数据");
    }

    /**
     * 获取流程审批详细信息
     */
    //@PreAuthorize("@ss.hasPermi('audit:approve:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(processApproveService.selectProcessApproveById(id));
    }

    /**
     * 新增流程审批
     */
    //@PreAuthorize("@ss.hasPermi('audit:approve:add')")
    @Log(title = "流程审批", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody ProcessApprove processApprove) {
        return toAjax(processApproveService.insertProcessApprove(processApprove));
    }

    /**
     * 修改流程审批
     */
    //@PreAuthorize("@ss.hasPermi('audit:approve:edit')")
    @Log(title = "流程审批", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody ProcessApprove processApprove) {
        return toAjax(processApproveService.updateProcessApprove(processApprove));
    }

    /**
     * 删除流程审批
     */
    //@PreAuthorize("@ss.hasPermi('audit:approve:remove')")
    @Log(title = "流程审批", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(processApproveService.deleteProcessApproveByIds(ids));
    }
}
