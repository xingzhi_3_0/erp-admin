package com.xz.web.controller.task;

import com.xz.common.core.domain.entity.SysTenant;
import com.xz.member.service.IRechargeActivityService;
import com.xz.system.service.ISysTenantService;
import com.xz.tenant.TenantContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author ：wwj
 * @date ：Created in 2024-3-7 16:05:39
 * @description：ota
 */
@Component
@Configuration      // 1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
public class RechargeActivityTask {

    private static  final Logger log = LoggerFactory.getLogger(RechargeActivityTask.class);

    @Autowired
    private IRechargeActivityService rechargeActivityService;

    /**
     * 活动结束和活动开始
     */
    @Scheduled(cron = "0 5 0 * * ? ")
    public void stopActivity(){
        // 停止活动 和 开始活动
        SysTenant tenant=new SysTenant();
        tenant.setTenantType(999);
        TenantContext.setSysTenantThreadLocal(tenant);
        rechargeActivityService.stopActivity();
    }

}
