package com.xz.web.controller.diagnosis;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.xz.diagnosis.dto.DiagnosisCauseDto;
import com.xz.diagnosis.vo.DiagnosisCauseVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.diagnosis.domain.DiagnosisCause;
import com.xz.diagnosis.service.IDiagnosisCauseService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 诊疗事项Controller
 *
 * @author xz
 * @date 2024-03-21
 */
@RestController
@RequestMapping("/diagnosis/cause")
public class DiagnosisCauseController extends BaseController {
  @Autowired
  private IDiagnosisCauseService diagnosisCauseService;

  /**
   * 查询诊疗事项列表
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:cause:list')")
  @GetMapping("/list")
  public TableDataInfo list(DiagnosisCause diagnosisCause) {
    startPage();
    List<DiagnosisCauseVo> list = diagnosisCauseService.selectDiagnosisCauseVoList(diagnosisCause);
    return getDataTable(list);
  }

  /**
   * 查询诊疗事项列表(下拉选项)
   */
  @GetMapping("/select/list")
  public AjaxResult selectList(DiagnosisCause diagnosisCause) {
    List<DiagnosisCauseVo> list = diagnosisCauseService.selectDiagnosisCauseVoList(diagnosisCause);
    list.sort((a,b) -> a.getSortNo() > b.getSortNo() ? 1 : 0);
    return AjaxResult.success(list);
  }

  /**
   * 导出诊疗事项列表
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:cause:export')")
  @Log(title = "诊疗事项", businessType = BusinessType.EXPORT)
  @GetMapping("/export")
  public AjaxResult export(DiagnosisCause diagnosisCause) {
    List<DiagnosisCause> list = diagnosisCauseService.selectDiagnosisCauseList(diagnosisCause);
    ExcelUtil<DiagnosisCause> util = new ExcelUtil<DiagnosisCause>(DiagnosisCause.class);
    return util.exportExcel(list, "诊疗事项数据");
  }

  /**
   * 获取诊疗事项详细信息
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:cause:query')")
  @GetMapping(value = "/{id}")
  public AjaxResult getInfo(@PathVariable("id") Long id) {
    return AjaxResult.success(diagnosisCauseService.selectDiagnosisCauseById(id));
  }

  /**
   * 新增诊疗事项
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:cause:add')")
  @Log(title = "诊疗事项", businessType = BusinessType.INSERT)
  @PostMapping
  public AjaxResult add(@Valid @RequestBody DiagnosisCauseDto diagnosisCause) {
    return toAjax(diagnosisCauseService.insertDiagnosisCause(diagnosisCause));
  }

  /**
   * 修改诊疗事项
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:cause:edit')")
  @Log(title = "诊疗事项", businessType = BusinessType.UPDATE)
  @PutMapping
  public AjaxResult edit(@Valid @RequestBody DiagnosisCauseDto diagnosisCause) {
    return toAjax(diagnosisCauseService.updateDiagnosisCause(diagnosisCause));
  }

  /**
   * 删除诊疗事项
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:cause:remove')")
  @Log(title = "诊疗事项", businessType = BusinessType.DELETE)
  @DeleteMapping("/{ids}")
  public AjaxResult remove(@PathVariable Long[] ids) {
    return toAjax(diagnosisCauseService.deleteDiagnosisCauseByIds(ids));
  }
}
