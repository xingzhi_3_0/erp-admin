package com.xz.web.controller.member;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.member.domain.IntegralRecord;
import com.xz.member.domain.MemberInfo;
import com.xz.member.dto.MemberRuleSettingDTO;
import com.xz.member.param.MemberQueryParam;
import com.xz.member.service.IIntegralRecordService;
import com.xz.member.service.IMemberInfoService;
import com.xz.member.service.IMemberRuleSettingService;
import com.xz.member.service.IRechargeActivityService;
import com.xz.member.vo.MemberInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 采购调整Controller
 *
 * @author xz
 * @date 2024-1-16
 */
@RestController
@RequestMapping("/member/integralRecord")
public class IntegralRecordController extends BaseController {

    @Autowired
    private IIntegralRecordService integralRecordService;
    @Autowired
    private IMemberInfoService memberInfoService;

    /**
     * 获取会员积分记录
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:query')")
    @GetMapping("/listByMemberId")
    public TableDataInfo listByMemberId(IntegralRecord integralRecord) {
        MemberInfo memberInfo = memberInfoService.getById(integralRecord.getMemberId());
        integralRecord.setMemberCardNo(memberInfo.getMemberCardNo());
        startPage();
        List<IntegralRecord> integralRecords = integralRecordService.listByMemberId(integralRecord);
        return getDataTable(integralRecords);
    }

    /**
     * 获取会员积分余额
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:query')")
    @GetMapping(value = "/memberIntegral")
    public AjaxResult memberIntegral(IntegralRecord integralRecord) {
        return integralRecordService.memberIntegral(integralRecord);
    }

    /**
     * 更改积分
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:add')")
    @Log(title = "会员积分", businessType = BusinessType.INSERT)
    @PostMapping("/changeIntegralRecord")
    public AjaxResult changeIntegralRecord(@Valid @RequestBody IntegralRecord integralRecord) {
        return integralRecordService.addRecord(integralRecord);
    }


}
