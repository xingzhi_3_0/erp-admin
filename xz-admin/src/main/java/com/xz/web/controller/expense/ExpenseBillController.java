package com.xz.web.controller.expense;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;
import javax.validation.Valid;
import com.xz.expense.domain.ExpenseBill;
import com.xz.expense.service.IExpenseBillService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 费用账单信息Controller
 * 
 * @author xz
 * @date 2024-02-25
 */
@RestController
@RequestMapping("/expense/bill")
public class ExpenseBillController extends BaseController {
    @Autowired
    private IExpenseBillService expenseBillService;

    /**
     * 查询费用账单信息列表
     */
    //@PreAuthorize("@ss.hasPermi('car:bill:list')")
    @GetMapping("/list")
    public TableDataInfo list(ExpenseBill expenseBill)
    {
        startPage();
        List<ExpenseBill> list = expenseBillService.selectExpenseBillList(expenseBill);
        return getDataTable(list);
    }

    /**
     * 导出费用账单信息列表
     */
    //@PreAuthorize("@ss.hasPermi('car:bill:export')")
    @Log(title = "费用账单信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ExpenseBill expenseBill)
    {
        List<ExpenseBill> list = expenseBillService.selectExpenseBillList(expenseBill);
        ExcelUtil<ExpenseBill> util = new ExcelUtil<ExpenseBill>(ExpenseBill.class);
        return util.exportExcel(list, "费用账单信息数据");
    }

    /**
     * 获取费用账单信息详细信息
     */
    //@PreAuthorize("@ss.hasPermi('car:bill:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(expenseBillService.selectExpenseBillById(id));
    }

    /**
     * 新增费用账单信息
     */
    //@PreAuthorize("@ss.hasPermi('car:bill:add')")
    @Log(title = "费用账单信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody ExpenseBill expenseBill)
    {
        return toAjax(expenseBillService.insertExpenseBill(expenseBill));
    }

    /**
     * 修改费用账单信息
     */
    //@PreAuthorize("@ss.hasPermi('car:bill:edit')")
    @Log(title = "费用账单信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody ExpenseBill expenseBill)
    {
        return toAjax(expenseBillService.updateExpenseBill(expenseBill));
    }

    /**
     * 删除费用账单信息
     */
    //@PreAuthorize("@ss.hasPermi('car:bill:remove')")
    @Log(title = "费用账单信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(expenseBillService.deleteExpenseBillByIds(ids));
    }
}
