package com.xz.web.controller.product;

import java.util.List;

import com.xz.product.dto.ProductAttributeDto;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.product.domain.ProductAttribute;
import com.xz.product.service.IProductAttributeService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 商品属性Controller
 *
 * @author xz
 * @date 2024-01-02
 */
@RestController
@RequestMapping("/product/attribute")
public class ProductAttributeController extends BaseController {
    @Autowired
    private IProductAttributeService productAttributeService;

    /**
     * 查询商品属性列表
     */
    //@PreAuthorize("@ss.hasPermi('product:attribute:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProductAttribute productAttribute) {
        startPage();
        List<ProductAttributeDto> list = productAttributeService.selectProductAttributeList(productAttribute);
        return getDataTable(list);
    }
    /**
     * 获取属性列表
     */
    @GetMapping("/getProductAttributeList/{attributeCategory}")
    public AjaxResult getProductAttributeList(@PathVariable("attributeCategory") Integer attributeCategory) {
        return AjaxResult.success(productAttributeService.getProductAttributeList(attributeCategory));
    }
    /**
     * 导出商品属性列表
     */
    //@PreAuthorize("@ss.hasPermi('product:attribute:export')")
    @Log(title = "商品属性", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ProductAttribute productAttribute) {
        List<ProductAttributeDto> list = productAttributeService.selectProductAttributeList(productAttribute);
        ExcelUtil<ProductAttributeDto> util = new ExcelUtil<ProductAttributeDto>(ProductAttributeDto.class);
        return util.exportExcel(list, "商品属性数据");
    }

    /**
     * 获取商品属性详细信息
     */
    //@PreAuthorize("@ss.hasPermi('product:attribute:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(productAttributeService.selectProductAttributeById(id));
    }

    /**
     * 新增商品属性
     */
    //@PreAuthorize("@ss.hasPermi('product:attribute:add')")
    @Log(title = "商品属性", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody ProductAttribute productAttribute) {
        return productAttributeService.insertProductAttribute(productAttribute);
    }

    /**
     * 修改商品属性
     */
    //@PreAuthorize("@ss.hasPermi('product:attribute:edit')")
    @Log(title = "商品属性", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody ProductAttribute productAttribute) {
        return productAttributeService.updateProductAttribute(productAttribute);
    }
    /**
     * 更新是否自由输入状态
     */
    //@PreAuthorize("@ss.hasPermi('product:attribute:edit')")
    @Log(title = "更新是否自由输入状态", businessType = BusinessType.UPDATE)
    @PutMapping("/isInputEdit")
    public AjaxResult isInputEdit(@RequestBody ProductAttribute productAttribute) {
        return productAttributeService.updateProductAttribute(productAttribute);
    }

    /**
     * 删除商品属性
     */
    //@PreAuthorize("@ss.hasPermi('product:attribute:remove')")
    @Log(title = "商品属性", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(productAttributeService.deleteProductAttributeByIds(ids));
    }
    /**
     * 删除商品属性
     */
    //@PreAuthorize("@ss.hasPermi('product:attribute:remove')")
    @Log(title = "商品属性", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{id}")
    public AjaxResult delete(@PathVariable Long id) {
        return productAttributeService.deleteProductAttributeById(id);
    }
}
