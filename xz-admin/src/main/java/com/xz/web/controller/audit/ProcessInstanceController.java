package com.xz.web.controller.audit;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.audit.domain.ProcessInstance;
import com.xz.audit.service.IProcessInstanceService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 流程实例Controller
 *
 * @author xz
 * @date 2024-01-15
 */
@RestController
@RequestMapping("/audit/instance")
public class ProcessInstanceController extends BaseController {
    @Autowired
    private IProcessInstanceService processInstanceService;

    /**
     * 查询流程实例列表
     */
    //@PreAuthorize("@ss.hasPermi('audit:instance:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProcessInstance processInstance) {
        startPage();
        List<ProcessInstance> list = processInstanceService.selectProcessInstanceList(processInstance);
        return getDataTable(list);
    }

    /**
     * 导出流程实例列表
     */
    //@PreAuthorize("@ss.hasPermi('audit:instance:export')")
    @Log(title = "流程实例", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ProcessInstance processInstance) {
        List<ProcessInstance> list = processInstanceService.selectProcessInstanceList(processInstance);
        ExcelUtil<ProcessInstance> util = new ExcelUtil<ProcessInstance>(ProcessInstance.class);
        return util.exportExcel(list, "流程实例数据");
    }

    /**
     * 获取流程实例详细信息
     */
    //@PreAuthorize("@ss.hasPermi('audit:instance:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(processInstanceService.selectProcessInstanceById(id));
    }


    /**
     * 删除流程实例
     */
    //@PreAuthorize("@ss.hasPermi('audit:instance:remove')")
    @Log(title = "流程实例", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(processInstanceService.deleteProcessInstanceByIds(ids));
    }
}
