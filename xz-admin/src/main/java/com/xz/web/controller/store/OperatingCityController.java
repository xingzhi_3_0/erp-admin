package com.xz.web.controller.store;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.store.domain.OperatingCity;
import com.xz.store.service.IOperatingCityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

/**
 * 运营城市Controller
 *
 * @author xz
 * @date 2022-03-08
 */
@RestController
@RequestMapping("/operatingCity/operatingCity")
public class OperatingCityController extends BaseController {
    @Autowired
    private IOperatingCityService operatingCityService;

    /**
     * 查询运营城市列表
     */
    //@PreAuthorize("@ss.hasPermi('operatingCity:operatingCity:list')")
    @GetMapping("/list")
    public TableDataInfo list(OperatingCity operatingCity) {
        startPage();
        List<OperatingCity> list = operatingCityService.selectOperatingCityList(operatingCity);
        return getDataTable(list);
    }

    /**
     * 查询运营城市code
     */
    //@PreAuthorize("@ss.hasPermi('operatingCity:operatingCity:list')")
    @GetMapping("/getCode")
    public AjaxResult getCode(OperatingCity operatingCity) {
        HashMap<String, Object> map = operatingCityService.getCode(operatingCity);
        return AjaxResult.success(map);
    }


    /**
     * 导出运营城市列表
     */
    //@PreAuthorize("@ss.hasPermi('operatingCity:operatingCity:export')")
    @Log(title = "运营城市", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OperatingCity operatingCity) {
        List<OperatingCity> list = operatingCityService.selectOperatingCityList(operatingCity);
        ExcelUtil<OperatingCity> util = new ExcelUtil<OperatingCity>(OperatingCity.class);
        return util.exportExcel(list, "运营城市数据");
    }

    /**
     * 获取运营城市详细信息
     */
    //@PreAuthorize("@ss.hasPermi('operatingCity:operatingCity:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(operatingCityService.selectOperatingCityById(id));
    }

    /**
     * 新增运营城市
     */
    //@PreAuthorize("@ss.hasPermi('operatingCity:operatingCity:add')")
    @Log(title = "运营城市", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody OperatingCity operatingCity) {
        return operatingCityService.insertOperatingCity(operatingCity);
    }

    /**
     * 修改运营城市
     */
    //@PreAuthorize("@ss.hasPermi('operatingCity:operatingCity:edit')")
    @Log(title = "运营城市", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody OperatingCity operatingCity) {
        return operatingCityService.updateOperatingCity(operatingCity);
    }

    /**
     * 删除运营城市
     */
    //@PreAuthorize("@ss.hasPermi('operatingCity:operatingCity:remove')")
    @Log(title = "运营城市", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(operatingCityService.deleteOperatingCityByIds(ids));
    }

    /**
     * 启用/停用
     */
    //@PreAuthorize("@ss.hasPermi('operatingCity:operatingCity:edit')")
    @Log(title = "启用/停用", businessType = BusinessType.UPDATE)
    @PutMapping("/editAvailable")
    public AjaxResult editAvailable(@RequestBody OperatingCity operatingCity) {
        return toAjax(operatingCityService.editAvailable(operatingCity));
    }
}
