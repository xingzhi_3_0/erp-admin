//package com.xz.web.controller.tool;
//
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONObject;
//import com.xz.common.utils.StringUtils;
//import org.apache.http.client.methods.CloseableHttpResponse;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.client.methods.HttpPut;
//import org.apache.http.entity.ByteArrayEntity;
//import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClients;
//import org.apache.http.util.EntityUtils;
//
///**
// * @class name:Test
// * @description: TODO
// * @author: LiWangJun
// * @create Time: 2022/11/17 12:39
// **/
//public class Test {
//    public static JSONObject upLoadVoid(String voidAddress, CookieCommonDto dto) {
//        String cookie = dto.getAppCookie();
//        String sec_ch_ua = dto.getSecChUa();
//        String user_agent = dto.getUserAgent();
//        String browser_platform = dto.getBrowserPlatform();
//        String browser_name = dto.getBrowserName();
//        String browser_version = dto.getBrowserVersion();
//        //////////////////////首先获取密钥////////////////////////////////
//        CloseableHttpClient httpClient = HttpClients.createDefault();
//        if (cookie == null) {
//            cookie = cookieAdd;
//        }
//        if (StringUtils.isEmpty(voidAddress)) {
//            throw new RenException("请传入文件地址");
//        }
//        //TODO 这里的值从外包那里获取
//        if (sec_ch_ua == null) {
//            sec_ch_ua = sec_ch_uaV;
//        }
//        if (user_agent == null) {
//            user_agent = user_agentV;
//        }
//        if (browser_platform == null) {
//            browser_platform = browser_platformV;
//        }
//        if (browser_name == null) {
//            browser_name = browser_nameV;
//        }
//        if (browser_version == null) {
//            browser_version = browser_versionV;
//        }
//        String aid = "1128";
//        String _signature = "_02B4Z6wo00101qRn83AAAIDCJGUJMjhYWNKkY.fAAMofXFl1JrFUPevb.FTvu2hhwdgo3BI0YFOTwYiTJc4mtdRs-RhDw5ZhzVHaAcdBXB6g6OFK6nIQx107FawdKyCUuVW2cA39NUKoFt..39";
//        //TODO 结束
//        try {
//            HttpGet httpGet = new HttpGet("https://creator.douyin.com/web/api/media/upload/auth/v5/" +
//                    "?cookie_enabled=true" +
//                    "&screen_width=1920&screen_height=1080&browser_language=zh-CN" +
//                    "&browser_platform=" + browser_platform + "" +
//                    "&browser_name=" + browser_name +
//                    "&browser_version=" + browser_version +
//                    "&browser_online=true" +
//                    "&timezone_name=Asia%2FShanghai" +
//                    "&aid=" + aid +
//                    "&_signature=" + _signature);
//            httpGet.setHeader("cookie", cookie);
//            httpGet.setHeader("Content-Type", "application/json; charset=utf-8");
//            httpGet.setHeader("browser_language", "zh-CN");
//            httpGet.setHeader("browser_platform", browser_platform);
//            httpGet.setHeader("browser_name", browser_name);
//            httpGet.setHeader("sec-ch-ua", sec_ch_ua);
//            httpGet.setHeader("browser_version", browser_version);
//            httpGet.setHeader("sec-fetch-dest", "empty");
//            httpGet.setHeader("sec-fetch-mode", "cors");
//            httpGet.setHeader("sec-fetch-site", "cross-site");
//            httpGet.setHeader("user-agent", user_agent);
//            CloseableHttpResponse response = httpClient.execute(httpGet);
//            //转为字符串
//            String result = EntityUtils.toString(response.getEntity(), "UTF-8");
//            JSONObject json = JSONObject.parseObject(result);
//            if (!json.getInteger("status_code").equals(SUCCESS_CODE)) {
//                log.error("抖音上传视频返回参数:{}", result);
//                return json;
//            }
//            String nonceStr = RandomStringUtils.randomAlphanumeric(11);
//            String accessKey = json.getJSONObject("auth").getString("AccessKeyID");
//            String secretAccessKey = json.getJSONObject("auth").getString("SecretAccessKey");
//            String SessionToken = json.getJSONObject("auth").getString("SessionToken");
//            String date = AWSV4Auth.getTimeStamp(json.getJSONObject("auth").getString("CurrentTime"));
//            //获取加密密钥的值
//            HttpPost httpPost = new HttpPost("http://39.106.73.65:8989/api/douyin/signature");
//            String json1 = "{" +
//                    "    \"mothod\": \"GET\"," +
//                    "    \"url_params\": \"Action=ApplyUploadInner&FileType=video&IsInner=1&SpaceName=aweme&Version=2020-11-19&s=" + nonceStr + "\"," +
//                    "    \"type\": \"vod\"," +
//                    "    \"formData\": \"\"," +
//                    "    \"date\": \"" + date + "\"," +
//                    "    \"accessKeyId\": \"" + accessKey + "\"," +
//                    "    \"secretAccessKey\": \"" + secretAccessKey + "\"," +
//                    "    \"sessionToken\": \"" + SessionToken + "\"," +
//                    "    \"has_content\": true" +
//                    "}";
//            httpPost.setHeader("Content-Type", "application/json");
//            httpPost.setEntity(new StringEntity(json1, "UTF-8"));
//            response = httpClient.execute(httpPost);
//            //转为字符串
//            String signResult = EntityUtils.toString(response.getEntity(), "UTF-8");
//////////////////////////////////////这里进行拼接获取tos的值/////////////////////////////////////
//            String authorization =
//                    "AWS4-HMAC-SHA256 Credential=" + accessKey +
//                            "/" + date.substring(0, 8) + "/cn-north-1/vod/aws4_request, SignedHeaders=x-amz-date;x-amz-security-token, Signature=" + signResult;
//            HttpGet httpUp = new HttpGet("https://vod.bytedanceapi.com/?Action=ApplyUploadInner&FileType=video&IsInner=1&SpaceName=aweme&Version=2020-11-19&s=" + nonceStr);
//            httpUp.setHeader("x-amz-date", date);
//            httpUp.setHeader("authorization", authorization);
//            httpUp.setHeader("x-amz-security-token", json.getJSONObject("auth").getString("SessionToken"));
//            httpUp.setHeader("browser_language", "zh-CN");
//            httpUp.setHeader("browser_platform", browser_platform);
//            httpUp.setHeader("browser_name", browser_name);
//            httpUp.setHeader("sec-ch-ua", sec_ch_ua);
//            httpUp.setHeader("browser_version", browser_version);
//            httpUp.setHeader("sec-fetch-dest", "empty");
//            httpUp.setHeader("sec-fetch-mode", "cors");
//            httpUp.setHeader("accept-language", "zh-CN,zh;q=0.9");
//            httpUp.setHeader("accept", "*/*");
//            httpUp.setHeader("sec-fetch-site", "same-origin");
//            httpUp.setHeader("user-agent", user_agent);
//            //body参数
//            response = httpClient.execute(httpUp);
//            //转为字符串
//            result = EntityUtils.toString(response.getEntity(), "UTF-8");
//            ///////////////////////这里得到了返回集合信息，用于后面的上传/////////////
//            json = JSONObject.parseObject(result);
//            JSONArray UploadNodesarray = json.getJSONObject("Result").getJSONObject("InnerUploadAddress").getJSONArray("UploadNodes");
//            JSONObject UploadNodes = null;
//            for (Object obj : UploadNodesarray) {
//                JSONObject jobj = (JSONObject) obj;
//                if (jobj.getString("Type").equals("Edge")) {
//                    UploadNodes = jobj;
//                    break;
//                }
//            }
//            String Type = UploadNodes.getString("Type");
//            String SessionKey = UploadNodes.getString("SessionKey");
//            String UploadHost = UploadNodes.getString("UploadHost");
//            JSONObject StoreInfos = (JSONObject) UploadNodes.getJSONArray("StoreInfos").get(0);
//            String StoreUri = StoreInfos.getString("StoreUri");
//            String Auth = StoreInfos.getString("Auth");
//            String UploadID = StoreInfos.getString("UploadID");
//
//            // 第三步向该地址post空获取uploadID
//            httpPost = new HttpPost("https://" + UploadHost + "/" + StoreUri + "?uploads");
//            httpPost.setHeader("Host", UploadHost);
//            httpPost.setHeader("Authorization", Auth);
//            httpPost.setHeader("Content-Type", "multipart/form-data");
//            response = httpClient.execute(httpPost);
//            String thrResult = EntityUtils.toString(response.getEntity(), "UTF-8");
//            JSONObject thrJson = JSONObject.parseObject(thrResult);
//            String uploadID = thrJson.getJSONObject("payload").getString("uploadID");
//
//
//            // 第四步向该地址拼接上uploadID并上传图片二进制文件，需要在请求头放上crc校验值
//            String forUrl = "https://" + UploadHost + "/" + StoreUri + "?partNumber=1&uploadID=" + uploadID;
//
//            String crc = getCRC32(voidAddress);
////            System.out.println("视频上传中。。。。。。。。。。");
//            HttpPut httpPut = new HttpPut(forUrl);
//            httpPut.setHeader(":authority", UploadHost);
//            httpPut.setHeader(":method", "PUT");
//            httpPut.setHeader(":path", StoreUri);
//            httpPut.setHeader(":scheme", "https");
//            httpPut.setHeader("accept", "*/*");
//            httpPut.setHeader("accept-language", "zh-CN,zh;q=0.9");
//            httpPut.setHeader("content-crc32", crc);
//            httpPut.setHeader("authorization", Auth);
//            httpPut.setHeader("content-type", "application/octet-stream");
//            httpPut.setHeader("content-disposition", "attachment; filename=\"undefined\"");
//            httpPut.setHeader("user-agent", user_agent);
//            httpPut.setHeader("sec-ch-ua", "\"Chromium\";v=\"104\", \" Not A;Brand\";v=\"99\", \"Google Chrome\";v=\"104\"");
//            httpPut.setHeader("browser_version", "5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36");
//            httpPut.setHeader("sec-fetch-dest", "empty");
//            httpPut.setHeader("sec-fetch-mode", "cors");
//            httpPut.setHeader("sec-fetch-site", "cross-site");
//            httpPut.setHeader("x-storage-u", "");
//            httpPut.setHeader("origin", "https://creator.douyin.com");
//            httpPut.setHeader("referer", "https://creator.douyin.com/");
//
//            File file = new File(voidAddress);
//            byte[] b = getBytesByFile(file);
//            httpPut.setEntity(new ByteArrayEntity(b));
//            CloseableHttpResponse forResponse = httpClient.execute(httpPut);
//            EntityUtils.toString(forResponse.getEntity(), "UTF-8");
//            ////////////////////////////////获取视频最终地址
//
////////////////////////////////////////这里进行拼接获取tos的值/////////////////////////////////////
//
//            httpPost = new HttpPost("https://" + UploadHost + "/" + StoreUri + "?uploadID=" + uploadID);
//            httpPost.setEntity(new StringEntity("1:" + crc, "UTF-8"));
//            httpPost.setHeader(":authority", UploadHost);
//            httpPost.setHeader(":method", "PUT");
//            httpPost.setHeader(":path", StoreUri);
//            httpPost.setHeader(":scheme", "https");
//            httpPost.setHeader("accept", "*/*");
//            httpPost.setHeader("accept-language", "zh-CN,zh;q=0.9");
//            httpPost.setHeader("content-crc32", crc);
//            httpPost.setHeader("authorization", Auth);
//            httpPost.setHeader("content-type", "application/octet-stream");
//            httpPost.setHeader("content-disposition", "attachment; filename=\"undefined\"");
//            httpPost.setHeader("user-agent", user_agent);
//            httpPost.setHeader("sec-ch-ua", "\"Chromium\";v=\"104\", \" Not A;Brand\";v=\"99\", \"Google Chrome\";v=\"104\"");
//            httpPost.setHeader("browser_version", "5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36");
//            httpPost.setHeader("sec-fetch-dest", "empty");
//            httpPost.setHeader("sec-fetch-mode", "cors");
//            httpPost.setHeader("sec-fetch-site", "cross-site");
//            httpPost.setHeader("x-storage-u", "");
//            httpPost.setHeader("origin", "https://creator.douyin.com");
//            httpPost.setHeader("referer", "https://creator.douyin.com/");
//
//            CloseableHttpResponse aaaResponse = httpClient.execute(httpPost);
//            String signResulxxt = EntityUtils.toString(aaaResponse.getEntity(), "UTF-8");
//            //获取加密密钥的值
//            httpPost = new HttpPost("http://39.106.73.65:8989/api/douyin/signatureVoid");
//            String forData = "{\"SessionKey\":\"" + SessionKey + "\",\"Functions\":[{\"name\":\"GetMeta\"},{\"name\":\"Snapshot\",\"input\":{\"SnapshotTime\":0}}]}";
//            String formDataSha256 = AwsvUtil.getSHA256StrJava(forData);
//            json1 = "{" +
//                    "    \"mothod\": \"POST\"," +
//                    "    \"url_params\": \"Action=CommitUploadInner&SpaceName=aweme&Version=2020-11-19\"," +
//                    "    \"type\": \"vod\"," +
//                    "    \"formData\": \"" + formDataSha256 + "\"," +
//                    "    \"date\": \"" + date + "\"," +
//                    "    \"accessKeyId\": \"" + accessKey + "\"," +
//                    "    \"secretAccessKey\": \"" + secretAccessKey + "\"," +
//                    "    \"sessionToken\": \"" + SessionToken + "\"," +
//                    "    \"has_content\": true" +
//                    "}";
//            httpPost.setHeader("Content-Type", "application/json");
//            httpPost.setEntity(new StringEntity(json1, "UTF-8"));
//
//            response = httpClient.execute(httpPost);
//            //转为字符串
//            signResult = EntityUtils.toString(response.getEntity(), "UTF-8");
//
//
//            //视频再次进行加密
//
//
//            //转为字符串
//
//            authorization =
//                    "AWS4-HMAC-SHA256 Credential=" + accessKey +
//                            "/" + date.substring(0, 8) + "/cn-north-1/vod/aws4_request, SignedHeaders=x-amz-content-sha256;x-amz-date;x-amz-security-token, Signature=" + signResult;
//
//            HttpPost httppostImgComit = new HttpPost("https://vod.bytedanceapi.com/?Action=CommitUploadInner&SpaceName=aweme&Version=2020-11-19");
//            httppostImgComit.setHeader("cache-control", "no-cache");
//            httppostImgComit.setHeader("authorization", authorization);
//            httppostImgComit.setHeader("content-type", "text/plain;charset=UTF-8");
//            httppostImgComit.setHeader("origin", "https://creator.douyin.com");
//            httppostImgComit.setHeader("referer", "https://creator.douyin.com/");
//            httppostImgComit.setHeader("x-amz-content-sha256", formDataSha256);
//            httppostImgComit.setHeader("x-amz-date", date);
//            httppostImgComit.setHeader("x-amz-security-token", SessionToken);
//            httppostImgComit.setEntity(new StringEntity(forData, "UTF-8"));
//            //body参数
//            response = httpClient.execute(httppostImgComit);
//            //转为字符串
//            String resultOver = EntityUtils.toString(response.getEntity(), "UTF-8");
//            log.info("上传抖音视频返回参数:{}", resultOver);
//            if (StringUtils.isEmpty(resultOver)) {
//                return null;
//            }
//            JSONObject fiveResult = JSONObject.parseObject(resultOver);
//            String Vid = ((JSONObject) fiveResult.getJSONObject("Result").getJSONArray("Results").get(0)).getString("Vid");
//
//            //最后一步获取图片地址
//
//            httpGet = new HttpGet("https://creator.douyin.com/web/api/media/video/enable/?video_id=" + Vid + "&cookie_enabled=true&screen_width=1920&screen_height=1080&browser_language=zh-CN&browser_platform=Win32&browser_name=Mozilla&browser_version=5.0+(Windows+NT+10.0%3B+Win64%3B+x64)+AppleWebKit%2F537.36+(KHTML,+like+Gecko)+Chrome%2F104.0.0.0+Safari%2F537.36&browser_online=true&timezone_name=Asia%2FShanghai&aid=1128&_signature=_02B4Z6wo00101KSGU0AAAIDAJISpAtQYghCkglfAAEoz39191MRwZ5cEmPlN-BhOhfCvQFsHWiTG1BgDO4u6B3hjvEvxxqRLt-IJ6MBL3zoB1CAHOtwWs4ojm.757J9RSieVuT.iFoqMclrkbd");
//            httpGet.setHeader("cookie", cookie);
//            response = httpClient.execute(httpGet);
//            result = EntityUtils.toString(response.getEntity(), "UTF-8");
//
//            httpGet = new HttpGet("https://creator.douyin.com/aweme/v1/creator/get/url/?uri=" + Vid + "&cookie_enabled=true&screen_width=1920&screen_height=1080&browser_language=zh-CN&browser_platform=Win32&browser_name=Mozilla&browser_version=5.0+(Windows+NT+10.0%3B+Win64%3B+x64)+AppleWebKit%2F537.36+(KHTML,+like+Gecko)+Chrome%2F104.0.0.0+Safari%2F537.36&browser_online=true&timezone_name=Asia%2FShanghai&aid=1128&_signature=_02B4Z6wo00101KSGU0AAAIDAJISpAtQbxgykglfAAEoz39191MRwZ5cEmPlN-BhOhfCvQFsHWiTG1BgDO4u6B3hjvEvxxqRLt-IJ6MBL3zoB1CAHOtwWs4ojm.757J9RSieVuT.iFoqMclrk3e");
//            httpGet.setHeader("cookie", cookie);
//            response = httpClient.execute(httpGet);
//            result = EntityUtils.toString(response.getEntity(), "UTF-8");
//
//            return fiveResult;
//        } catch (Exception e) {
//            log.error("上传视频失败,失败原因:{}", e.getMessage());
//        }
//        return null;
//    }
//
//}
