package com.xz.web.controller.diagnosis;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.diagnosis.domain.DiagnosisItem;
import com.xz.diagnosis.dto.DiagnosisItemDto;
import com.xz.diagnosis.service.IDiagnosisItemService;
import com.xz.diagnosis.vo.DiagnosisItemVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 诊疗项目Controller
 *
 * @author xz
 * @date 2024-03-14
 */
@RestController
@RequestMapping("/diagnosis/item")
public class DiagnosisItemController extends BaseController {
  @Autowired
  private IDiagnosisItemService diagnosisItemService;

  /**
   * 查询诊疗项目列表
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:item:list')")
  @GetMapping("/list")
  public TableDataInfo list(DiagnosisItem diagnosisItem) {
    startPage();
    List<DiagnosisItemVo> list = diagnosisItemService.selectDiagnosisItemVoList(diagnosisItem);
    return getDataTable(list);
  }

  /**
   * 查询诊疗项目列表
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:item:list')")
  @GetMapping("/select/list")
  public AjaxResult selectList(DiagnosisItem diagnosisItem) {
    List<DiagnosisItemVo> list = diagnosisItemService.selectDiagnosisItemVoList(diagnosisItem);
    return AjaxResult.success(list);
  }

  /**
   * 导出诊疗项目列表
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:item:export')")
  @Log(title = "诊疗项目", businessType = BusinessType.EXPORT)
  @GetMapping("/export")
  public AjaxResult export(DiagnosisItem diagnosisItem) {
    List<DiagnosisItem> list = diagnosisItemService.selectDiagnosisItemList(diagnosisItem);
    ExcelUtil<DiagnosisItem> util = new ExcelUtil<DiagnosisItem>(DiagnosisItem.class);
    return util.exportExcel(list, "诊疗项目数据");
  }

  /**
   * 获取诊疗项目详细信息
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:item:query')")
  @GetMapping(value = "/{id}")
  public AjaxResult getInfo(@PathVariable("id") Long id) {
    DiagnosisItem entity = diagnosisItemService.selectDiagnosisItemById(id);
    DiagnosisItemVo vo = new DiagnosisItemVo();
    BeanUtils.copyProperties(entity, vo);
    return AjaxResult.success(vo);
  }

  /**
   * 新增诊疗项目
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:item:add')")
  @Log(title = "诊疗项目", businessType = BusinessType.INSERT)
  @PostMapping
  public AjaxResult add(@Valid @RequestBody DiagnosisItemDto dto) {

    return toAjax(diagnosisItemService.insertDiagnosisItem(dto));
  }

  /**
   * 修改诊疗项目
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:item:edit')")
  @Log(title = "诊疗项目", businessType = BusinessType.UPDATE)
  @PutMapping
  public AjaxResult edit(@Valid @RequestBody DiagnosisItemDto dto) {
    return toAjax(diagnosisItemService.updateDiagnosisItem(dto));
  }

  /**
   * 删除诊疗项目
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:item:remove')")
  @Log(title = "诊疗项目", businessType = BusinessType.DELETE)
  @DeleteMapping("/{ids}")
  public AjaxResult remove(@PathVariable Long[] ids) {
    return toAjax(diagnosisItemService.deleteDiagnosisItemByIds(ids));
  }
}
