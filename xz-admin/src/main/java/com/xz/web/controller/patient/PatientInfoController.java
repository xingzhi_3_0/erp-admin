package com.xz.web.controller.patient;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.diagnosisOrder.utils.DiagnosisOrderUtils;
import com.xz.expense.domain.ExpenseBill;
import com.xz.expense.service.IExpenseBillService;
import com.xz.member.domain.MemberLevel;
import com.xz.patient.domain.PatientInfo;
import com.xz.patient.service.IPatientInfoService;
import com.xz.patient.vo.PatientImportVo;
import com.xz.patient.vo.PatientInfoExportVo;
import com.xz.patient.vo.SendPatientVo;
import com.xz.purchase.dto.PurchaseImportVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * 患者信息Controller
 *
 * @author xz
 * @date 2024-01-30
 */
@RestController
@RequestMapping("/patient/patientInfo")
public class PatientInfoController extends BaseController {
    @Autowired
    private IPatientInfoService patientInfoService;
    @Autowired
    private IExpenseBillService expenseBillService;

    /**
     * 查询患者信息列表
     */
    //@PreAuthorize("@ss.hasPermi('patient:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(PatientInfo patientInfo)
    {
        startPage();
        List<PatientInfo> list = patientInfoService.selectPatientInfoList(patientInfo);
        return getDataTable(list);
    }

    /**
     * 查询患者列表
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:list')")
    @GetMapping("/findList")
    public AjaxResult findList(PatientInfo patientInfo) {
        startPage();
        List<PatientInfo> list = patientInfoService.findList(patientInfo);
        return AjaxResult.success(list);
    }

    /**
     * 导出患者信息列表
     */
    //@PreAuthorize("@ss.hasPermi('patient:info:export')")
    @Log(title = "患者信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PatientInfo patientInfo)
    {
        List<PatientInfoExportVo> list = patientInfoService.queryListForExport(patientInfo);
        list.forEach(data->{data.setPatientInfo(DiagnosisOrderUtils.getPatientInfo(data.getPatientName(), data.getSex(), data.getBirthday()));});
        ExcelUtil<PatientInfoExportVo> util = new ExcelUtil<PatientInfoExportVo>(PatientInfoExportVo.class);
        return util.exportExcel(list, "患者信息数据");
    }

    /**
     * 获取患者信息详细信息
     */
    //@PreAuthorize("@ss.hasPermi('patient:info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id){
        PatientInfo data = patientInfoService.selectPatientInfoById(id);
        if(null != data){
            data.setPatientInfo(DiagnosisOrderUtils.getPatientInfo(data.getPatientName(), data.getSex(), data.getBirthday()));
        }
        return AjaxResult.success(data);
    }


    /**
     * 查询患者费用账单信息列表
     */
    @GetMapping("/expense/bill/list")
    public TableDataInfo list(ExpenseBill expenseBill){
      if(null == expenseBill.getPatientId()){
        return getDataTable2(new ArrayList<>(),0);
      }
      startPage();
      List<ExpenseBill> list = expenseBillService.selectExpenseBillList(expenseBill);
      return getDataTable(list);
    }


    /**
     * 新增患者信息
     */
    //@PreAuthorize("@ss.hasPermi('patient:info:add')")
    @Log(title = "患者信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody PatientInfo patientInfo)
    {
        return patientInfoService.insertPatientInfo(patientInfo);
    }


    /**
     * 修改患者信息
     */
    //@PreAuthorize("@ss.hasPermi('patient:info:edit')")
    @Log(title = "患者信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody PatientInfo patientInfo)
    {
        return patientInfoService.updatePatientInfo(patientInfo);
    }

    /**
     * 修改患者建档门店
     */
    //@PreAuthorize("@ss.hasPermi('patient:info:edit')")
    @Log(title = "患者信息", businessType = BusinessType.UPDATE)
    @PostMapping("/updateDept")
    public AjaxResult updateDept(@Valid @RequestBody PatientInfo patientInfo)
    {
        return toAjax(patientInfoService.updateDept(patientInfo));
    }

    /**
     * 修改患者建档门店
     */
    //@PreAuthorize("@ss.hasPermi('patient:info:edit')")
    @Log(title = "患者信息", businessType = BusinessType.UPDATE)
    @PostMapping("/registerMember")
    public AjaxResult registerMember(@Valid @RequestBody PatientInfo patientInfo)
    {
        return toAjax(patientInfoService.registerMember(patientInfo));
    }

    /**
     * 修改患者建档门店
     */
    //@PreAuthorize("@ss.hasPermi('patient:info:edit')")
    @Log(title = "患者信息", businessType = BusinessType.UPDATE)
    @PostMapping("/sendPatientSms")
    public AjaxResult sendPatientSms(@Valid @RequestBody SendPatientVo sendPatientVo)
    {
        return patientInfoService.sendPatientSms(sendPatientVo);
    }

    /**
     * 删除患者信息
     */
    //@PreAuthorize("@ss.hasPermi('patient:info:remove')")
    @Log(title = "患者信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(patientInfoService.deletePatientInfoByIds(ids));
    }

    /**
     * 导入患者信息
     */
    @Log(title = "导入患者信息", businessType = BusinessType.EXPORT)
    @PostMapping("/importExcel")
    public AjaxResult importExcel(@RequestParam("file") MultipartFile file) throws Exception {
        ExcelUtil<PatientImportVo> util = new ExcelUtil<PatientImportVo>(PatientImportVo.class);
        List<PatientImportVo> list = util.importExcel("患者信息",file.getInputStream(),2);
        AjaxResult message = patientInfoService.importExcel(list);
        return message;
    }

}
