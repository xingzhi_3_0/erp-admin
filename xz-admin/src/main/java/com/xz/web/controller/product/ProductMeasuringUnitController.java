package com.xz.web.controller.product;

import java.util.List;

import com.xz.product.dto.ProductMeasuringUnitDto;
import io.swagger.models.auth.In;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.product.domain.ProductMeasuringUnit;
import com.xz.product.service.IProductMeasuringUnitService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 商品基础配置Controller
 *
 * @author xz
 * @date 2024-01-02
 */
@RestController
@RequestMapping("/product/unit")
public class ProductMeasuringUnitController extends BaseController {
    @Autowired
    private IProductMeasuringUnitService productMeasuringUnitService;

    /**
     * 查询商品基础配置列表
     */
    //@PreAuthorize("@ss.hasPermi('product:unit:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProductMeasuringUnit productMeasuringUnit) {
        startPage();
        List<ProductMeasuringUnitDto> list = productMeasuringUnitService.selectProductMeasuringUnitList(productMeasuringUnit);
        return getDataTable(list);
    }
    /**
     * 查询商品基础配置列表
     */
    //@PreAuthorize("@ss.hasPermi('product:unit:list')")
    @GetMapping("/getProductMeasuringUnitList/{category}")
    public AjaxResult getProductMeasuringUnitList(@PathVariable("category") Integer category) {
        ProductMeasuringUnit productMeasuringUnit =new ProductMeasuringUnit();
        productMeasuringUnit.setCategory(category);
        productMeasuringUnit.setStatus("0");
        return AjaxResult.success(productMeasuringUnitService.selectProductMeasuringUnitList(productMeasuringUnit));
    }

    /**
     * 导出商品基础配置列表
     */
    //@PreAuthorize("@ss.hasPermi('product:unit:export')")
    @Log(title = "商品基础配置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ProductMeasuringUnit productMeasuringUnit) {
        List<ProductMeasuringUnitDto> list = productMeasuringUnitService.selectProductMeasuringUnitList(productMeasuringUnit);
        ExcelUtil<ProductMeasuringUnitDto> util = new ExcelUtil<ProductMeasuringUnitDto>(ProductMeasuringUnitDto.class);
        return util.exportExcel(list, "商品基础配置数据");
    }

    /**
     * 获取商品基础配置详细信息
     */
    //@PreAuthorize("@ss.hasPermi('product:unit:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(productMeasuringUnitService.selectProductMeasuringUnitById(id));
    }

    /**
     * 新增商品基础配置
     */
    //@PreAuthorize("@ss.hasPermi('product:unit:add')")
    @Log(title = "商品基础配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody ProductMeasuringUnit productMeasuringUnit) {
        return productMeasuringUnitService.insertProductMeasuringUnit(productMeasuringUnit);
    }

    /**
     * 修改商品基础配置
     */
    //@PreAuthorize("@ss.hasPermi('product:unit:edit')")
    @Log(title = "商品基础配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody ProductMeasuringUnit productMeasuringUnit) {
        return productMeasuringUnitService.updateProductMeasuringUnit(productMeasuringUnit);
    }

    /**
     * 删除商品基础配置
     */
    //@PreAuthorize("@ss.hasPermi('product:unit:remove')")
    @Log(title = "商品基础配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(productMeasuringUnitService.deleteProductMeasuringUnitByIds(ids));
    }
    /**
     * 删除商品基础配置
     */
    //@PreAuthorize("@ss.hasPermi('product:unit:remove')")
    @Log(title = "商品基础配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{id}")
    public AjaxResult delete(@PathVariable Long id) {
        return productMeasuringUnitService.deleteProductMeasuringUnitById(id);
    }
}
