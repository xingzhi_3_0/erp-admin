package com.xz.web.controller.purchase;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.purchase.domain.AdjustInfo;
import com.xz.purchase.domain.PurchaseProduct;
import com.xz.purchase.domain.WarningSettings;
import com.xz.purchase.param.AdjustInfoParam;
import com.xz.purchase.param.WarningSettingParam;
import com.xz.purchase.service.IAdjustInfoService;
import com.xz.purchase.service.IWarningSettingsService;
import com.xz.purchase.vo.CommodityDimensionStatisticsVo;
import com.xz.purchase.vo.WarningSettingsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 采购调整Controller
 *
 * @author xz
 * @date 2024-1-16
 */
@RestController
@RequestMapping("/purchase/warningSettings")
public class WarningSettingsController extends BaseController {
    @Autowired
    private IWarningSettingsService warningSettingsService;

    /**
     * 查询采购调整列表
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:list')")
    @GetMapping("/list")
    public TableDataInfo list(WarningSettings warningSettings) {
        startPage();
        List<WarningSettingsVo> warningSettingsVos = warningSettingsService.queryList(warningSettings);
        return getDataTable(warningSettingsVos);
    }

    /**
     * 导出即使库存--商品纬度
     */
    @GetMapping("/exportList")
    public AjaxResult exportList(WarningSettings warningSettings) {
        List<WarningSettingsVo> warningSettingsVos = warningSettingsService.queryList(warningSettings);
        ExcelUtil<WarningSettingsVo> util = new ExcelUtil<WarningSettingsVo>(WarningSettingsVo.class);
        return util.exportExcel(warningSettingsVos, "库存预警数据");
    }

    /**
     * 查询采购调整列表
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:list')")
    @GetMapping("/setWarningList")
    public TableDataInfo setWarningList(WarningSettings warningSettings) {
        startPage();
        List<WarningSettings> setWarningList = warningSettingsService.setWarningList(warningSettings);
        return getDataTable(setWarningList);
    }

    /**
     * 新增采购库存调整
     */
    //@PreAuthorize("@ss.hasPermi('purchase:purchase:add')")
    @Log(title = "采购库存调整", businessType = BusinessType.INSERT)
    @PostMapping("/setUp")
    public AjaxResult setUp(@Valid @RequestBody WarningSettingParam warningSettingParam) {
        return warningSettingsService.setUp(warningSettingParam);
    }

}
