package com.xz.web.controller.diagnosisOrder;

import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.page.TableDataInfo;
import com.xz.common.enums.BusinessType;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrder;
import com.xz.diagnosisOrder.dto.DiagnosisTreatmentOrderSaveDto;
import com.xz.diagnosisOrder.service.IDiagnosisTreatmentOrderService;
import com.xz.diagnosisOrder.utils.DiagnosisOrderUtils;
import com.xz.diagnosisOrder.vo.DiagnosisTreatmentOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 就诊单Controller
 *
 * @author xz
 * @date 2024-03-25
 */
@RestController
@RequestMapping("/treatmentorder/order")
public class DiagnosisTreatmentOrderController extends BaseController {
  @Autowired
  private IDiagnosisTreatmentOrderService diagnosisTreatmentOrderService;

  /**
   * 查询就诊单列表
   */
  //@PreAuthorize("@ss.hasPermi('treatmentorder:order:list')")
  @GetMapping("/list")
  public TableDataInfo list(DiagnosisTreatmentOrder diagnosisTreatmentOrder) {
    startPage();
    List<DiagnosisTreatmentOrderVo> list = diagnosisTreatmentOrderService.selectDiagnosisTreatmentOrderVoList(diagnosisTreatmentOrder);
    return getDataTable(list);
  }

  /**
   * 导出就诊单列表
   */
  //@PreAuthorize("@ss.hasPermi('treatmentorder:order:export')")
  @Log(title = "就诊单", businessType = BusinessType.EXPORT)
  @GetMapping("/export")
  public AjaxResult export(DiagnosisTreatmentOrder diagnosisTreatmentOrder) {

    List<DiagnosisTreatmentOrderVo> list = diagnosisTreatmentOrderService.selectDiagnosisTreatmentOrderVoList(diagnosisTreatmentOrder);
    list.forEach(data->{data.setPatientInfo(DiagnosisOrderUtils.getPatientInfo(data.getPatientName(), data.getSex(), data.getBirthday()));});
    ExcelUtil<DiagnosisTreatmentOrderVo> util = new ExcelUtil<DiagnosisTreatmentOrderVo>(DiagnosisTreatmentOrderVo.class);
    return util.exportExcel(list, "就诊记录数据");
  }

  /**
   * 获取就诊单详细信息
   */
  //@PreAuthorize("@ss.hasPermi('treatmentorder:order:query')")
  @GetMapping(value = "/{id}")
  public AjaxResult getInfo(@PathVariable("id") Long id) {
    return AjaxResult.success(diagnosisTreatmentOrderService.selectDiagnosisTreatmentOrderById(id));
  }

  /**
   * 获取就诊单详细信息
   */
  //@PreAuthorize("@ss.hasPermi('treatmentorder:order:query')")
  @GetMapping(value = "/getByNo")
  public AjaxResult getByNo(String orderNo) {
    return AjaxResult.success(diagnosisTreatmentOrderService.selectDiagnosisTreatmentOrderByNo(orderNo));
  }


  /**
   * 新增就诊单
   */
  //@PreAuthorize("@ss.hasPermi('treatmentorder:order:add')")
  @Log(title = "就诊单", businessType = BusinessType.INSERT)
  @PostMapping
  public AjaxResult add(@Valid @RequestBody DiagnosisTreatmentOrderSaveDto diagnosisTreatmentOrder) {
    return toAjax(diagnosisTreatmentOrderService.insertDiagnosisTreatmentOrder(diagnosisTreatmentOrder));
  }

  /**
   * 退费
   */
  @Log(title = "诊疗（预约/挂号）单", businessType = BusinessType.UPDATE)
  @PutMapping("/refund/{id}")
  public AjaxResult refundFeeById(@PathVariable("id") Long[] ids) {
    return toAjax(diagnosisTreatmentOrderService.refundFeeById(ids));
  }

}
