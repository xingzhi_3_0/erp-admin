package com.xz.web.controller.diagnosisOrder;

import java.util.List;

import com.xz.diagnosisOrder.dto.DiagnosisOrderSaveDto;
import com.xz.diagnosisOrder.enums.ArrivalStatusEnum;
import com.xz.diagnosisOrder.utils.DiagnosisOrderUtils;
import com.xz.diagnosisOrder.vo.DiagnosisOrderListVo;
import com.xz.diagnosisOrder.vo.OrderServiceDeskListVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.diagnosisOrder.domain.DiagnosisOrder;
import com.xz.diagnosisOrder.service.IDiagnosisOrderService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 诊疗（预约/挂号）单Controller
 *
 * @author xz
 * @date 2024-03-17
 */
@RestController
@RequestMapping("/diagnosis/order")
public class DiagnosisOrderController extends BaseController {
  @Autowired
  private IDiagnosisOrderService diagnosisOrderService;

  /**
   * 查询挂号记录列表
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:order:list')")
  @GetMapping("/list")
  public TableDataInfo selectList(DiagnosisOrder diagnosisOrder) {

    startPage();
    //只查已到店的数据
    diagnosisOrder.setArrivalStatus(ArrivalStatusEnum.ARRIVED.getStatus());
    List<DiagnosisOrderListVo> list = diagnosisOrderService.selectDiagnosisOrderVoList(diagnosisOrder);
    return getDataTable(list);
  }

  /**
   * 导出挂号记录列表
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:order:export')")
  @Log(title = "诊疗（预约/挂号）单", businessType = BusinessType.EXPORT)
  @GetMapping("/export")
  public AjaxResult export(DiagnosisOrder diagnosisOrder) {
    //只查已到店的数据
    diagnosisOrder.setArrivalStatus(ArrivalStatusEnum.ARRIVED.getStatus());
    List<DiagnosisOrderListVo> list = diagnosisOrderService.selectDiagnosisOrderVoList(diagnosisOrder);
    list.forEach(data->{data.setPatientInfo(DiagnosisOrderUtils.getPatientInfo(data.getPatientName(), data.getSex(), data.getBirthday()));});
    ExcelUtil<DiagnosisOrderListVo> util = new ExcelUtil<DiagnosisOrderListVo>(DiagnosisOrderListVo.class);
    return util.exportExcel(list, "挂号记录列表数据`");
  }

  /**
   * 查询服务台列表
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:order:list')")
  @GetMapping("service/list")
  public TableDataInfo selectOrderServiceDeskList(DiagnosisOrder diagnosisOrder) {
    startPage();
    List<OrderServiceDeskListVo> list = diagnosisOrderService.selectOrderServiceDeskList(diagnosisOrder);
    return getDataTable(list);
  }

  /**
   * 导出诊疗（预约/挂号）单列表
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:order:export')")
  @Log(title = "诊疗（预约/挂号）单", businessType = BusinessType.EXPORT)
  @GetMapping("/service/export")
  public AjaxResult serviceExport(DiagnosisOrder diagnosisOrder) {
    List<OrderServiceDeskListVo> list = diagnosisOrderService.selectOrderServiceDeskList(diagnosisOrder);
    ExcelUtil<OrderServiceDeskListVo> util = new ExcelUtil<OrderServiceDeskListVo>(OrderServiceDeskListVo.class);
    return util.exportExcel(list, "导诊服务台列表数据`");
  }

  /**
   * 获取诊疗（预约/挂号）单详细信息
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:order:query')")
  @GetMapping(value = "/{id}")
  public AjaxResult getInfo(@PathVariable("id") Long id) {
    return AjaxResult.success(diagnosisOrderService.selectDiagnosisOrderDetailVoById(id));
  }

  /**
   * 新增诊疗（预约/挂号）单
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:order:add')")
  @Log(title = "诊疗（预约/挂号）单", businessType = BusinessType.INSERT)
  @PostMapping
  public AjaxResult add(@Valid @RequestBody DiagnosisOrderSaveDto diagnosisOrder) {
    return toAjax(diagnosisOrderService.insertDiagnosisOrder(diagnosisOrder));
  }

  /**
   * 修改诊疗（预约/挂号）单
   */
  //@PreAuthorize("@ss.hasPermi('diagnosis:order:edit')")
  @Log(title = "诊疗（预约/挂号）单", businessType = BusinessType.UPDATE)
  @PutMapping
  public AjaxResult edit(@Valid @RequestBody DiagnosisOrderSaveDto diagnosisOrder) {
    return toAjax(diagnosisOrderService.updateDiagnosisOrder(diagnosisOrder));
  }

  /**
   * 取消预约
   */
  @Log(title = "诊疗（预约/挂号）单", businessType = BusinessType.UPDATE)
  @PutMapping("/cancel/{id}")
  public AjaxResult cancel(@PathVariable("id") Long id) {
    return toAjax(diagnosisOrderService.cancelById(id));
  }

  /**
   * 退号
   */
  @Log(title = "诊疗（预约/挂号）单", businessType = BusinessType.UPDATE)
  @PutMapping("/refund/{id}")
  public AjaxResult refundFeeById(@PathVariable("id") Long id) {
    return toAjax(diagnosisOrderService.refundFeeById(id));
  }
}
