package com.xz.web.controller.product;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xz.common.annotation.Log;
import com.xz.common.core.controller.BaseController;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.enums.BusinessType;

import javax.validation.Valid;

import com.xz.product.domain.ProductCategoryItem;
import com.xz.product.service.IProductCategoryItemService;
import com.xz.common.utils.poi.ExcelUtil;
import com.xz.common.core.page.TableDataInfo;

/**
 * 商品类别--关联属性Controller
 *
 * @author xz
 * @date 2024-01-02
 */
@RestController
@RequestMapping("/product/item")
public class ProductCategoryItemController extends BaseController {
    @Autowired
    private IProductCategoryItemService productCategoryItemService;

    /**
     * 查询商品类别--关联属性列表
     */
    //@PreAuthorize("@ss.hasPermi('product:item:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProductCategoryItem productCategoryItem) {
        startPage();
        List<ProductCategoryItem> list = productCategoryItemService.selectProductCategoryItemList(productCategoryItem);
        return getDataTable(list);
    }

    /**
     * 导出商品类别--关联属性列表
     */
    //@PreAuthorize("@ss.hasPermi('product:item:export')")
    @Log(title = "商品类别--关联属性", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ProductCategoryItem productCategoryItem) {
        List<ProductCategoryItem> list = productCategoryItemService.selectProductCategoryItemList(productCategoryItem);
        ExcelUtil<ProductCategoryItem> util = new ExcelUtil<ProductCategoryItem>(ProductCategoryItem.class);
        return util.exportExcel(list, "商品类别--关联属性数据");
    }

    /**
     * 获取商品类别--关联属性详细信息
     */
    //@PreAuthorize("@ss.hasPermi('product:item:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(productCategoryItemService.selectProductCategoryItemById(id));
    }

    /**
     * 新增商品类别--关联属性
     */
    //@PreAuthorize("@ss.hasPermi('product:item:add')")
    @Log(title = "商品类别--关联属性", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody ProductCategoryItem productCategoryItem) {
        return toAjax(productCategoryItemService.insertProductCategoryItem(productCategoryItem));
    }

    /**
     * 修改商品类别--关联属性
     */
    //@PreAuthorize("@ss.hasPermi('product:item:edit')")
    @Log(title = "商品类别--关联属性", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody ProductCategoryItem productCategoryItem) {
        return toAjax(productCategoryItemService.updateProductCategoryItem(productCategoryItem));
    }

    /**
     * 删除商品类别--关联属性
     */
    //@PreAuthorize("@ss.hasPermi('product:item:remove')")
    @Log(title = "商品类别--关联属性", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(productCategoryItemService.deleteProductCategoryItemByIds(ids));
    }
}
