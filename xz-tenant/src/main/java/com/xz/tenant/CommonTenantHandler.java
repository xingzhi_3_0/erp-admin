package com.xz.tenant;

import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import org.springframework.beans.factory.annotation.Autowired;


//@AllArgsConstructor
public class CommonTenantHandler implements TenantLineHandler {

    @Autowired
    private TenantPropertiesConfig tenantPropertiesConfig;

    /**
     * 获取租户ID
     *
     * @return 租户ID
     */
    @Override
    public Expression getTenantId() {
        return new LongValue(TenantContext.getSysTenantThreadLocal().getTenantId());
    }

    /**
     * 获取租户字段名称
     *
     * @return 租户字段名称
     */
    @Override
    public String getTenantIdColumn() {
        return com.xz.tenant.TenantProperties.getInstance().getColumn();
    }

    /**
     * 过滤租户表
     *
     * @param tableName 表名
     * @return 是否进行过滤 返回true 表示不进行多租户处理
     */
    @Override
    public boolean ignoreTable(String tableName) {
        /*if (!com.xz.tenant.TenantProperties.getInstance().getEnable()){
            return true;
        }*/
        if("1".equals(TenantContext.getOutFlag())){
            return true;
        }
        if(TenantProperties.getInstance().getIgnoreTables().contains(tableName)){
            return true;
        }
        if (tenantPropertiesConfig.getSaasConfig() == 1){
            return true;
        }
        if(TenantContext.getSysTenantThreadLocal().getTenantType().equals(TenantConstant.TENANT_TYPE_P)){
            return true;
        }
        if(TenantContext.getSysTenantThreadLocal().getTenantType().equals(TenantConstant.TENANT_TYPE_PPP)){
            return true;
        }
        return false;
    }


}
