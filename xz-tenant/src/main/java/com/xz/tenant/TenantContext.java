package com.xz.tenant;

import com.xz.common.core.domain.entity.SysTenant;


public class TenantContext {

    private TenantContext (){};

    private static ThreadLocal<SysTenant> SYS_TENANT_THREAD_LOCAL=new ThreadLocal<SysTenant>();

    private static ThreadLocal<String> outFlag=new ThreadLocal<>();

    private static ThreadLocal<String> source =new ThreadLocal<>();




    public  static void  setSysTenantThreadLocal(SysTenant sysTenantThreadLocal){
        SYS_TENANT_THREAD_LOCAL.set(sysTenantThreadLocal);
    }


    public static SysTenant  getSysTenantThreadLocal(){
       return   SYS_TENANT_THREAD_LOCAL.get();
    }

    public static void  removeSysTenantThreadLocal(){
        SYS_TENANT_THREAD_LOCAL.remove();
    }

    public  static void  setOutFlag(String flag){
        outFlag.set(flag);
    }

    public static String  getOutFlag(){
        return   outFlag.get();
    }

    public  static void  removeOutFlag(){
        outFlag.remove();
    }

    public  static void  setSource(String flag){
        source.set(flag);
    }
    public static String  getSource(){
        return   source.get();
    }
    public  static void  removeSource(){
        source.remove();
    }



}
