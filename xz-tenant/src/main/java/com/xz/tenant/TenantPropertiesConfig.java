package com.xz.tenant;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "tenant")
@Data
public class TenantPropertiesConfig {
    /**
     * 1.单体架构 2.SaaS架构
     */
    private Integer saasConfig  = 1;
}
