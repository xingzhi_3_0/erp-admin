package com.xz.tenant;

import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Data
public class TenantProperties {

    private static TenantProperties instance;

    //private Boolean enable = true;

    //private String defaultTenantId = "00000000";

    /**
     * 多租户字段名称
     */
    private String column = "tenant_id";

    /**
     * 多租户系统数据表
     */
    private List<String> ignoreTables = new ArrayList<>();


    private TenantProperties() {
    }

    public static TenantProperties getInstance() {
        if (instance == null) {
            instance = new TenantProperties();
            instance.getIgnoreTables().addAll(Arrays.asList(
                    "dual",
                    "gen_table",
                    "gen_table_column",
                    "t_product_attribute_param",
                    "t_allot_purchase",
                    "t_allot_product",
                    "t_optometry_item",
                    "t_product_category_item",
                    "sys_config",
                    "sys_dict_data",
                    "sys_dict_type",
                    "sys_job",
                    "sys_job_log",
                    "sys_menu",
                    "sys_role_dept",
                    "sys_role_menu",
                    "sys_tenant",
                    "sys_user_post",
                    "sys_user_role",
                    "t_report_damage_product",
                    "t_purchase_product_param",
                    "t_return_product",
                    "t_adjust_product",
                    "t_sales_return_detail",
                    "t_sales_exchange_detail",
                    "t_sales_purchase",
                    "t_allot_product",
                    "t_member_patient",
                    "t_operation_log",
                    "t_recharge_activity_gift",
                    "t_process_order_item",
                    "sys_seq_num"
                ));
        }
        return instance;
    }
}
