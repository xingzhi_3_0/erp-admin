package com.xz.tenant;

public class TenantConstant {

    public static final Integer ABLE_STATUS= 0;
    public static final Integer DISABLE_STATUS= 1;

    public static final Integer TENANT_TYPE_S= 1;
    public static final Integer TENANT_TYPE_P= 0;
    public static final Integer TENANT_TYPE_PPP= 999;


    public static final Integer NORMAL_STATUS= 0;
    public static final Integer DELETE_STATUS= 2;


    public static final Integer NORMAL_SYS_USER_STATUS= 1;
    public static final Integer ADMIN_SYS_USER_STATUS= 0;


    public static final String TENANT_DEFAULT_PASSWORD= "123456";




}
