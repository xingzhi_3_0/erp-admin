package com.xz.system.domain.Enum;

import com.xz.system.domain.SysTenantCommonConfig;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * 商户公共配置
 */
public enum TenantConfigEnum {

    otaOfflineAutoLimitTime("otaOfflineAutoLimitTime","2","OTA掉线自动同步时间范围（小时）"),
    otaOfflineGenerateOrderLimitTime("otaOfflineGenerateOrderLimitTime","24","OTA掉线生成工单时间范围（小时）");

    @Getter
    @Setter
    private final String configKey;
    @Getter
    @Setter
    private final String configValue;
    @Getter
    @Setter
    private final String configName;

    TenantConfigEnum(String configKey, String configValue,String configName) {
        this.configKey = configKey;
        this.configValue = configValue;
        this.configName = configName;
    }

   public static List<SysTenantCommonConfig> getTenantCommonConfigList(Long tenantId) {
        List<SysTenantCommonConfig> tenantCommonConfigList=new ArrayList<>();
        for (TenantConfigEnum e : values()){
            SysTenantCommonConfig sysTenantCommonConfig=new SysTenantCommonConfig();
            sysTenantCommonConfig.setTenantConfigKey(e.getConfigKey());
            sysTenantCommonConfig.setTenantConfigValue(e.getConfigValue());
            sysTenantCommonConfig.setTenantConfigName(e.getConfigName());
            sysTenantCommonConfig.setTenantId(tenantId);
            tenantCommonConfigList.add(sysTenantCommonConfig);
        }
       return tenantCommonConfigList;
    }
}
