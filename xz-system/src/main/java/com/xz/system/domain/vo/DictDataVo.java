package com.xz.system.domain.vo;

import lombok.Data;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2022/6/10 14:26
 * @description：门店端菜单
 */
@Data
public class DictDataVo {

    private String id;
    private String name;
    //logo
    private String  logo;
}
