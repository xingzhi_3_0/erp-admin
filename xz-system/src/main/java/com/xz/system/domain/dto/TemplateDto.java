package com.xz.system.domain.dto;

import lombok.Data;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2022/7/22 15:11
 * @description：模板
 */
@Data
public class TemplateDto {
    private String key;
    private String value;
}
