package com.xz.system.domain.vo;


import lombok.Data;

@Data
public class SysRoleMenuVo {

    private String roleKey;
    private String groupByMenus;
}
