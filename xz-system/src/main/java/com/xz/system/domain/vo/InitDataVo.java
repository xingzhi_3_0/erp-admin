package com.xz.system.domain.vo;

import lombok.Data;

import java.util.List;
import java.util.Set;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2022/6/10 14:24
 * @description：初始话数据
 */
@Data
public class InitDataVo {
    //常用工具
    private List<DictDataVo> commonToolsList;
    //首页模块
    private List<DictDataVo> financialManagementList;
    //运营报表
    private List<DictDataVo> operatingReportsList;
    //底部导航
    private List<DictDataVo> navigationList;
    //租户对应业务
    private List<DictDataVo> bizList;
    //车辆状态
    private List<DictDataVo> carStatusList;
    //租户下门店
    private List<DictDataVo> storeList;
    //订单来源
    private List<DictDataVo> orderSource;
    //支付类型
    private List<DictDataVo> sysPayWay;
    //支付类型
    private List<DictDataVo> chargeType;

    private Set<String> permissions;
    /**
     * 电子合同配置开关
     */
    private Integer electronicContractEnable = 0;
}
