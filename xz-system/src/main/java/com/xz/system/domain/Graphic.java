package com.xz.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * H5图文对象 t_graphic
 *
 * @author xz
 * @date 2022-04-12
 */
@Data
@TableName("t_graphic")
public class Graphic extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId(type = IdType.AUTO)
    private Long graphicId;

    /** 图文名称 */
    @Excel(name = "图文名称")
    private String graphicName;

    /** 图文url */
    @Excel(name = "图文url")
    private String graphicUrl;
    //图文类型(1租车指南2放心租3平台保障4商务合作5微信分介绍6用车服务协议7预定须知8开票帮助9违章处理说明10关于我们)
    private Integer graphicType;

    /** 图文介绍 */
    @Excel(name = "图文介绍")
    private String graphicIntroduction;

    private Integer jumpType;
    private String appid;
    private String path;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;
    @TableField(exist = false)
    private String tenantName;

    /** 删除状态(0-未删除，1-已删除) */
    private Integer delFlag;

}
