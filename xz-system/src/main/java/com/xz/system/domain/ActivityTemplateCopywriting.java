package com.xz.system.domain;

import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 活动模板文案对象 sys_activity_template_copywriting
 * 
 * @author xz
 * @date 2022-07-22
 */
@Data
@TableName("sys_activity_template_copywriting")
public class ActivityTemplateCopywriting {
    private static final long serialVersionUID = 1L;

    /** 模板id */
    @Excel(name = "模板id")
    private Long templateId;

    /** 类型（1.模板名称 2.模板内容） */
    @Excel(name = "类型", readConverterExp = "1=.模板名称,2=.模板内容")
    private Integer copywritingType;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

}
