package com.xz.system.domain.dto;

import lombok.Data;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2022/7/23 16:24
 * @description：新增租户
 */
@Data
public class InsertTenantDto {
    //租户名称
    private String tenantName;
    //租户code
    private String tenantCode;
    //门店名称
    private String storeName;
    private String abbreviation;  //商户简称
}
