package com.xz.system.domain.vo;

import lombok.Data;

/**
 * @class name:OrderStatisticsVo
 * @description: TODO
 * @author: LiWangJun
 * @create Time: 2023/4/7 14:34
 **/
@Data
public class OrderStatisticsVo {

    private Integer unscheduledQuantity;
    private Integer inventoryShortage;

}
