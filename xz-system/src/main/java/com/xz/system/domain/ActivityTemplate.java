package com.xz.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;

import com.xz.system.domain.dto.TemplateDto;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 活动模板对象 sys_activity_template
 * 
 * @author xz
 * @date 2022-07-22
 */
@Data
@TableName("sys_activity_template")
public class ActivityTemplate extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId(type = IdType.AUTO)
    private Long templateId;

    /** 模板code */
    @Excel(name = "模板code")
    private String code;

    /** 模板分类（1.优惠券模板 2.车型模板 3.节日模板） */
    @Excel(name = "模板分类", readConverterExp = "1=.优惠券模板,2=.车型模板,3=.节日模板")
    private Integer templateClassification;

    /** 模板类型（1.优惠券图文活动 2.车型图文活动） */
    @Excel(name = "模板类型", readConverterExp = "1=.优惠券图文活动,2=.车型图文活动")
    private Integer templateType;

    /** 模板名称（数据字段维护） */
    @Excel(name = "模板名称", readConverterExp = "数=据字段维护")
    private Integer templateName;
    //模板名称
    @TableField(exist = false)
    private List<TemplateDto> headlineList;
    //模板内容
    @TableField(exist = false)
    private List<TemplateDto> contentList;
    //推广文案
    @TableField(exist = false)
    private List<TemplateDto>  copywritingList;
}
