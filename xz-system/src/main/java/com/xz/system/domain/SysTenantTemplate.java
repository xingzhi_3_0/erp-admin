package com.xz.system.domain;

import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 租户与活动模板关联对象 sys_tenant_template
 * 
 * @author xz
 * @date 2022-07-22
 */
@Data
@TableName("sys_tenant_template")
public class SysTenantTemplate {
    private static final long serialVersionUID = 1L;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 模板id */
    @Excel(name = "模板id")
    private Long templateId;

}
