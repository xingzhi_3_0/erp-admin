package com.xz.system.domain.vo;

import lombok.Data;

/**
 * 功能介绍 状态
 */
@Data
public class FunctionIntroduceVo {
    /**
     * 字典编号
     */
    public String dictValue;
    /**
     * 字典名称
     */
    public String dictLabel;
    /**
     * 状态 字典 sys_show_hide  0.显示 1.隐藏
     */
    public Integer switchStatus;
    public Long tenantId;
    public Long id;
}
