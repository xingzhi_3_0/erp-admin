package com.xz.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.time.temporal.ChronoUnit;

import static com.xz.common.utils.StringUtils.NULLSTR;

/**
 * @author renyong
 * @date 2023/7/24 13:49
 * @description 业务编号
 */
@Data
public class SysSeqNum extends BaseEntity {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 编码
     */
    @Length(max = 32,message = "编码key最多{max}个字符")
    @NotBlank(message = "编码key不能为空")
    private String code;
    /**
     * 前缀
     */
    @Length(max = 32,message = "编码前缀最多{max}个字符")
    private String prefix;

    /**
     * 当前值
     */
    private Integer currentNum;

    /**
     * 重置周期 YEARS年,MONTHS月,DAYS日,FOREVER永久,默认永久
     * 参考java枚举 ChronoUnit
     */
    private String resetType;
    /**
     * 时间格式化字符串,默认拼接到编号前
     */
    @Length(max = 32,message = "时间格式化字符串最多{max}个字符")
    private String dateFormatStr;

    /**
     * 租户id
     */
    private Long tenantId;

    public SysSeqNum() {
        this.prefix = NULLSTR;
        this.resetType = ChronoUnit.FOREVER.name();
        this.currentNum = 1;
        this.dateFormatStr = NULLSTR;
    }

    public String getDateStr() {
        if (StringUtils.isBlank(dateFormatStr)) {
            return NULLSTR;
        }
        return DateFormatUtils.format(getUpdateTime(), dateFormatStr);
    }
}
