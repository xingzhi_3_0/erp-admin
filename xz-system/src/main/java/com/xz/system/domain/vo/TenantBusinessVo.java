package com.xz.system.domain.vo;

import lombok.Data;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2022/3/9 9:58
 * @description：租户已开通业务
 */
@Data
public class TenantBusinessVo {
    //业务id
    private Integer busId;
    //业务名称
    private String busName;
}
