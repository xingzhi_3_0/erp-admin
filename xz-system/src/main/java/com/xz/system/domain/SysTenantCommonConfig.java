package com.xz.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 租户配置对象 sys_tenant_common_config
 * 
 * @author xz
 * @date 2023-09-07
 */
@Data
@TableName("sys_tenant_common_config")
public class SysTenantCommonConfig {
    /** id */
    private Long id;

    /** 租户id */
    private Long tenantId;

    /** 租户配置key */
    private String  tenantConfigKey;

    /** 租户配置value */
    private String  tenantConfigValue;

    /** 租户配置名称 */
    private String  tenantConfigName;

}
