package com.xz.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 租户参数配置对象 sys_tenant_config
 *
 * @author xz
 * @date 2022-03-02
 */
@Data
public class SysTenantConfig extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId
    private Long id;

    /** 租户id */
    private Long tenantId;

    /** 小程序app_id */
    private String appId;

    /** 小程序app_secret */
    private String appSecret;

    /** 证书*/
    private String certificate;

    /** 短信账号 */
    private String smsId;

    /** 短信密码 */
    private String smsPsw;

    /** 是否高级认证（1.是 2.否） */
    private Integer advancedMode;
    //下单延迟时间
    private Integer delayTime;
    //支付配置（1.测试金额0.01  2.实际支付）
    private Integer payTest;
    //是否认证下单（1.是 2.否）
    private Integer certificationOrder;
    //免费时长
    private Integer freeHour;

    /** 高级认证code(身份证二要素认证) */
    private String alipayAppCode;

    /** 百度地图ak */
    private String baiduAk;

    /** orc识别id */
    private String clientId;

    /** orc识别secret */
    private String clientSecret;
    //商户id
    private String mchId;
    //支付回调地址
    private String notifyUrl;
    //商户key
    private String appKey;
    //退款限制时间
    private Integer timeLimit;
    //退款扣除手续费
    private Double serviceCharge;
    //保险失效预警时间
    private Integer insuranceDay;
    //年检失效预警时间
    private Integer inspectionDay;
    //商业险失效预警时间
    private Integer maintenanceDay;
    //违章secretId
    private String secretId;
    //违章secretKey
    private String secretKey;
    /**
     * 车辆监控登录名
     */
    private String cljkLoginName;

    /**
     * 车辆监控登录密码
     */
    private String cljkPassword;

    //用户有进行中的订单,是否可以继续下单（1.是 2.否）
    private Integer onlyRunOrder;

   /**汇聚相关*/
    //汇聚报备商户号
    private String tradeMerchantNo;
    //汇聚分账方编号
    private String altMchNo;
    //汇聚分账比例
    private Double splitProportion;
    //违章押金
    private BigDecimal illegalAmount;
    //活动模板
    private List<Long> template;
    //悦保OCR_key
    private String yueBaoOcrKey;
    //悦保OCR_key
    private String yueBaoOcrSecret;
    //押金预授权有效期
    private Integer depositPreAuthorization;
    //违章押金预授权有效期
    private Integer illegalDepositAuthorization;
    //财务确认收款（1.是 2.否）
    private Integer financialConfirmCollection;
    //财务确认退款（1.是 2.否）
    private Integer financeConfirmsRefund;
    //几米子账户用户名
    private String subAccountUser;
    //几米子账户密码
    private String subAccountPwd;
    //是否开启整备（1.是 2.否）
    private Integer isVehicleServicing;
    //车辆整备时长（单位：时）
    private Integer vehicleServicingDuration;
    //GPS设备告警声音是否开启（1.是 2.否）
    private Integer gpsAlarmSound;
    //库存限制（1.限制 2.不限制）
    private Integer stockLimit;

    //是否开启ota订单平台车辆同步（0：不开启，1：开启）
    private Integer otaAutoCarSync;
    //是否开启夜间服务费（1.是 2.否）
    private Integer nightServiceFee;
    //夜间服务费开始时间
    private String nightServiceFeeStart;
    //夜间服务费结束时间
    private String nightServiceFeeEnd;
    //夜间服务费价格
    private BigDecimal nightServiceFeePrice;
    //夜间服务费说明
    private String nightServiceFeeRemark;
    //OTA掉线预警联系电话
    private String otaPhone;
    //OTA最后通讯时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date otaLastTime;

    //是否开启库存车辆（1.是 2.否）
    private Integer enableSyncTime;
    //支付宝小程序公钥路径
    private String alipayKeyUrl;
    //门店服务范围外下单(1允许2禁止)
    private Integer outOfRange;
    //门店服务时间外下单(1允许2禁止)
    private Integer outOfTime;
    //门店服务范围外代客下单(1开启2关闭)
    private Integer outOfRangeAdmin;
    //门店服务时间外代客下单(1开启2关闭)
    private Integer outOfTimeAdmin;
    //租户名称
    @TableField(exist = false)
    private String tenantName;
    //是否开启掉线库存同步（1.开启 2.关闭）
    private Integer isEnableOfflineSyn;
    //托管结算方式（1.线下结算 2线上自动结算 默认1）
    private Integer escrowSettlementType;
    //交车前必须查询反欺诈和签署电子合同 1.开启 2.关闭）
    private Integer preDeliveryInspection;
}
