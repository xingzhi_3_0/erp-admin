package com.xz.system.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.system.domain.Enum.TenantConfigEnum;
import com.xz.system.domain.SysTenantCommonConfig;
import com.xz.system.mapper.SysTenantCommonConfigMapper;
import com.xz.system.service.ISysTenantCommonConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 租户配置Service业务层处理
 *
 * @author xz
 * @date 2023-09-07
 */
@Service
public class SysTenantCommonConfigServiceImpl extends ServiceImpl<SysTenantCommonConfigMapper, SysTenantCommonConfig> implements ISysTenantCommonConfigService {
    @Autowired
    private SysTenantCommonConfigMapper sysTenantCommonConfigMapper;

    /**
     * 查询租户配置
     *
     * @param id 租户配置主键
     * @return 租户配置
     */
    @Override
    public SysTenantCommonConfig selectSysTenantCommonConfigById(Long id) {
        return sysTenantCommonConfigMapper.selectSysTenantCommonConfigById(id);
    }

    /**
     * 查询租户配置列表
     *
     * @param tenantId 租户id
     * @return 租户配置
     */
    @Override
    public List<SysTenantCommonConfig> selectSysTenantCommonConfigList(Long tenantId) {
        return sysTenantCommonConfigMapper.selectSysTenantCommonConfigList(tenantId);
    }

    /**
     * 新增租户配置
     *
     * @return 结果
     */
    @Override
    public Boolean insertSysTenantCommonConfig(Long tenantId) {
        List<SysTenantCommonConfig> tenantCommonConfigList = TenantConfigEnum.getTenantCommonConfigList(tenantId);
        tenantCommonConfigList.stream().forEach(item->{
            sysTenantCommonConfigMapper.insertSysTenantCommonConfig(item);
        });
        return true;
    }

    /**
     * 修改租户配置
     *
     * @param sysTenantCommonConfig 租户配置
     * @return 结果
     */
    @Override
    public int updateSysTenantCommonConfig(SysTenantCommonConfig sysTenantCommonConfig) {
        return sysTenantCommonConfigMapper.updateSysTenantCommonConfig(sysTenantCommonConfig);
    }

    /**
     * 批量删除租户配置
     *
     * @param ids 需要删除的租户配置主键
     * @return 结果
     */
    @Override
    public int deleteSysTenantCommonConfigByIds(Long[] ids) {
        return sysTenantCommonConfigMapper.deleteSysTenantCommonConfigByIds(ids);
    }

    /**
     * 删除租户配置信息
     *
     * @param id 租户配置主键
     * @return 结果
     */
    @Override
    public int deleteSysTenantCommonConfigById(Long id) {
        return sysTenantCommonConfigMapper.deleteSysTenantCommonConfigById(id);
    }
}
