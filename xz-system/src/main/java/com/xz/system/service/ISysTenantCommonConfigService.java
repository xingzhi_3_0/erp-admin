package com.xz.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.system.domain.SysTenantCommonConfig;

/**
 * 租户配置Service接口
 * 
 * @author xz
 * @date 2023-09-07
 */
public interface ISysTenantCommonConfigService extends IService<SysTenantCommonConfig> {
    /**
     * 查询租户配置
     * 
     * @param id 租户配置主键
     * @return 租户配置
     */
    public SysTenantCommonConfig selectSysTenantCommonConfigById(Long id);

    /**
     * 查询租户配置列表
     * 
     * @param tenantId 租户id
     * @return 租户配置集合
     */
    public List<SysTenantCommonConfig> selectSysTenantCommonConfigList(Long tenantId);

    /**
     * 新增租户配置
     * 
     * @return 结果
     */
    public Boolean insertSysTenantCommonConfig(Long tenantId);

    /**
     * 修改租户配置
     * 
     * @param sysTenantCommonConfig 租户配置
     * @return 结果
     */
    public int updateSysTenantCommonConfig(SysTenantCommonConfig sysTenantCommonConfig);

    /**
     * 批量删除租户配置
     * 
     * @param ids 需要删除的租户配置主键集合
     * @return 结果
     */
    public int deleteSysTenantCommonConfigByIds(Long[] ids);

    /**
     * 删除租户配置信息
     * 
     * @param id 租户配置主键
     * @return 结果
     */
    public int deleteSysTenantCommonConfigById(Long id);
}
