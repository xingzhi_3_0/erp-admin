package com.xz.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.domain.entity.SysTenant;
import com.xz.system.domain.SysTenantConfig;
import com.xz.system.domain.dto.InsertTenantDto;
import com.xz.system.domain.vo.InitDataVo;

import java.util.List;
import java.util.Map;

/**
 * All rights Reserved, Designed By www.sunseagear.com
 *
 * @version V1.0
 * @package sys
 * @title: 租户管理控制器
 * @description: 租户管理控制器
 * @author: 未知
 * @date: 2019-11-28 06:24:52
 * @copyright: www.sunseagear.com Inc. All rights reserved.
 */
public interface ISysTenantService extends IService<SysTenant> {

    SysTenant getSysTenantById(Long id);

    int deleteById(Long asList,String username);

    int updateTenant(SysTenant sysTenant);

    int updateTenantConfig(SysTenantConfig sysTenantConfig);

    int updateSysTenantConfig(SysTenantConfig sysTenantConfig);

    SysTenantConfig getTenantConfig(Long id);
    /**
     * 租户对应业务
     * @param id
     * @return
     */
    AjaxResult tenantBusiness(Long id);

    String checkNameOrCodeUnique(SysTenant sysTenant);
    /**
     * 门店端初始话数据
     */
    InitDataVo initData();

    /**
     * 数据字典
     */
    AjaxResult dictDataList(String dictType);

    //插入租户--快捷
    boolean insertTenant(SysTenant sysTenant);
    //商户信息
    List<Map<String,Object>> tenantList();
    //商户信息--租户id
    SysTenantConfig sysTenantConfigInfo(Long tenantId);

    /**
     * ota获取车牌信息
     * @return
     */
    AjaxResult getOtaSynvehicle();
    /**
     *  ota修改同步车牌状态
     */
    AjaxResult updateOtaSynvehicle(SysTenantConfig sysTenantConfig);

    //获取配置项
    String getTenantConfigValue(String key,Long tenantId);
}
