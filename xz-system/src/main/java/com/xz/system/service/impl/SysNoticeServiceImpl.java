package com.xz.system.service.impl;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;

import com.xz.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.system.domain.SysNotice;
import com.xz.system.mapper.SysNoticeMapper;
import com.xz.system.service.ISysNoticeService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 公告 服务层实现
 * 
 * @author xz
 */
@Service
public class SysNoticeServiceImpl implements ISysNoticeService
{
    @Resource
    private SysNoticeMapper noticeMapper;

    /**
     * 查询公告信息
     * 
     * @param noticeId 公告ID
     * @return 公告信息
     */
    @Override
    public SysNotice selectNoticeById(Long noticeId,Long userId)
    {
        return noticeMapper.selectNoticeById(noticeId,userId);
    }

    /**
     * 查询公告列表
     * 
     * @param notice 公告信息
     * @return 公告集合
     */
    @Override
    public List<SysNotice> selectNoticeList(SysNotice notice)
    {
        return noticeMapper.selectNoticeList(notice);
    }

    /**
     * 新增公告
     * 
     * @param notice 公告信息
     * @return 结果
     */
    @Override
    public int insertNotice(SysNotice notice)
    {
        return noticeMapper.insertNotice(notice);
    }

    /**
     * 修改公告
     * 
     * @param notice 公告信息
     * @return 结果
     */
    @Override
    public int updateNotice(SysNotice notice)
    {
        return noticeMapper.updateNotice(notice);
    }

    /**
     * 删除公告对象
     * 
     * @param noticeId 公告ID
     * @return 结果
     */
    @Override
    public int deleteNoticeById(Long noticeId)
    {
        return noticeMapper.deleteNoticeById(noticeId);
    }

    /**
     * 批量删除公告信息
     * 
     * @param noticeIds 需要删除的公告ID
     * @return 结果
     */
    @Override
    public int deleteNoticeByIds(Long[] noticeIds)
    {
        return noticeMapper.deleteNoticeByIds(noticeIds);
    }

    @Override
    @Transactional
    public int markRead(Long[] noticeIds, Long userId) {
        try{
            for(Long noticeId:noticeIds){
                noticeMapper.markRead(noticeId,userId, SecurityUtils.getLoginUser().getTenantId());
            }
        }catch (Exception e){

        }
        return 1;
    }

    @Override
    public int getNotReadNotice(Long userId) {
        return noticeMapper.getNotReadNotice(userId);
    }

    /**
     * 新增通知
     * @param notice
     * @return
     */
    @Override
    public boolean insertWorkNotice(SysNotice notice) {
        notice.setNoticeType("2");
        notice.setStatus("0");
        notice.setCreateTime(new Date());
        int insertNotice = noticeMapper.insertNotice(notice);
        return insertNotice>0?true:false;
    }
}
