package com.xz.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.system.domain.Graphic;

/**
 * H5图文Service接口
 * 
 * @author xz
 * @date 2022-04-12
 */
public interface IGraphicService extends IService<Graphic> {
    /**
     * 查询H5图文
     * 
     * @param graphicId H5图文主键
     * @return H5图文
     */
    public Graphic selectGraphicByGraphicId(Long graphicId);

    /**
     * 查询H5图文列表
     * 
     * @param graphic H5图文
     * @return H5图文集合
     */
    public List<Graphic> selectGraphicList(Graphic graphic);

    /**
     * 新增H5图文
     * 
     * @param graphic H5图文
     * @return 结果
     */
    public AjaxResult insertGraphic(Graphic graphic);

    /**
     * 修改H5图文
     * 
     * @param graphic H5图文
     * @return 结果
     */
    public AjaxResult updateGraphic(Graphic graphic);
    /**
     * 删除H5图文
     *
     * @param templateId 活动模板主键
     * @return 结果
     */
    int deleteId(Long templateId);

}
