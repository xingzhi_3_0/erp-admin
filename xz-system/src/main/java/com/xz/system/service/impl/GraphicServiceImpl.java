package com.xz.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.annotation.DataScope;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.utils.DateUtils;
import com.xz.common.utils.SecurityUtils;
import com.xz.system.domain.Graphic;
import com.xz.system.mapper.GraphicMapper;
import com.xz.system.service.IGraphicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * H5图文Service业务层处理
 *
 * @author xz
 * @date 2022-04-12
 */
@Service
public class GraphicServiceImpl extends ServiceImpl<GraphicMapper, Graphic> implements IGraphicService {
    @Autowired
    private GraphicMapper graphicMapper;
    /**
     * 查询H5图文
     *
     * @param graphicId H5图文主键
     * @return H5图文
     */
    @Override
    public Graphic selectGraphicByGraphicId(Long graphicId) {
        return graphicMapper.selectGraphicByGraphicId(graphicId);
    }

    /**
     * 查询H5图文列表
     *
     * @param graphic H5图文
     * @return H5图文
     */
    public List<Graphic> selectGraphicList(Graphic graphic) {
        return graphicMapper.selectGraphicList(graphic);
    }

    /**
     * 新增H5图文
     *
     * @param graphic H5图文
     * @return 结果
     */
    @Override
    public AjaxResult insertGraphic(Graphic graphic) {
        graphic.setCreateBy(SecurityUtils.getLoginUser().getUserId());
        graphic.setCreateTime(DateUtils.getNowDate());
        graphic.setUpdateBy(SecurityUtils.getLoginUser().getUserId());
        graphic.setUpdateTime(DateUtils.getNowDate());
        graphic.setTenantId(SecurityUtils.getLoginUser().getTenantId());
        int i = graphicMapper.insertGraphic(graphic);
        return i>0?AjaxResult.success("新增成功"):AjaxResult.error("新增失败");
    }

    /**
     * 修改H5图文
     *
     * @param graphic H5图文
     * @return 结果
     */
    @Override
    public AjaxResult updateGraphic(Graphic graphic) {
        if(graphic.getGraphicId()==null){
            return AjaxResult.error("H5图文编号不能为空");
        }
        if(graphic.getGraphicType()!=14&&SecurityUtils.getLoginUser().getUserId().intValue()!=1){
            //校验H5图文是否已存在
            Integer count = baseMapper.selectCount(new QueryWrapper<Graphic>().ne("graphic_id",graphic.getGraphicId()).eq("del_flag",0).eq("graphic_name",graphic.getGraphicName()));
            if(count>0){
                return AjaxResult.error("H5图文已存在");
            }
        }

        graphic.setUpdateBy(SecurityUtils.getLoginUser().getUserId());
        graphic.setUpdateTime(DateUtils.getNowDate());
        int i = graphicMapper.updateGraphic(graphic);
       return i>0?AjaxResult.success("修改成功"):AjaxResult.error("修改失败");
    }

    //删除H5图文
    public int deleteId(Long templateId) {
        return baseMapper.deleteById(templateId);
    }
}
