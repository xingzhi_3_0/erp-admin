package com.xz.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.system.domain.SysTenantCommonConfig;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 租户配置Mapper接口
 * 
 * @author xz
 * @date 2023-09-07
 */
@Mapper
public interface SysTenantCommonConfigMapper  extends BaseMapper<SysTenantCommonConfig> {
    /**
     * 查询租户配置
     * 
     * @param id 租户配置主键
     * @return 租户配置
     */
    public SysTenantCommonConfig selectSysTenantCommonConfigById(Long id);

    /**
     * 查询租户配置列表
     * 
     * @param tenantId 租户id
     * @return 租户配置集合
     */
    public List<SysTenantCommonConfig> selectSysTenantCommonConfigList(Long tenantId);

    /**
     * 新增租户配置
     * 
     * @param sysTenantCommonConfig 租户配置
     * @return 结果
     */
    public int insertSysTenantCommonConfig(SysTenantCommonConfig sysTenantCommonConfig);

    /**
     * 修改租户配置
     * 
     * @param sysTenantCommonConfig 租户配置
     * @return 结果
     */
    public int updateSysTenantCommonConfig(SysTenantCommonConfig sysTenantCommonConfig);

    /**
     * 删除租户配置
     * 
     * @param id 租户配置主键
     * @return 结果
     */
    public int deleteSysTenantCommonConfigById(Long id);

    /**
     * 批量删除租户配置
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysTenantCommonConfigByIds(Long[] ids);
}
