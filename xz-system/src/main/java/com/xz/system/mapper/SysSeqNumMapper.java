package com.xz.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.system.domain.SysSeqNum;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author renyong
 * @date 2023/7/24 13:51
 * @description 业务编号
 */
@Mapper
public interface SysSeqNumMapper extends BaseMapper<SysSeqNum> {
}
