package com.xz.system.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.system.domain.Graphic;
import org.apache.ibatis.annotations.Param;

/**
 * H5图文Mapper接口
 * 
 * @author xz
 * @date 2022-04-12
 */
@Mapper
public interface GraphicMapper  extends BaseMapper<Graphic> {
    /**
     * 查询H5图文
     * 
     * @param graphicId H5图文主键
     * @return H5图文
     */
    public Graphic selectGraphicByGraphicId(Long graphicId);

    /**
     * 查询H5图文列表
     * 
     * @param graphic H5图文
     * @return H5图文集合
     */
    public List<Graphic> selectGraphicList(Graphic graphic);
    //api 图文
    public List<Map<String,Object>> apiGraphicList(@Param("tenantId") Long tenantId,@Param("type") Integer type);
    String apiGraphicInfo(Long graphicId);

    /**
     * 新增H5图文
     * 
     * @param graphic H5图文
     * @return 结果
     */
    public int insertGraphic(Graphic graphic);

    /**
     * 修改H5图文
     * 
     * @param graphic H5图文
     * @return 结果
     */
    public int updateGraphic(Graphic graphic);
}
