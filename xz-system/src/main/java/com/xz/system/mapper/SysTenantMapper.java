package com.xz.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.common.core.domain.entity.SysTenant;
import com.xz.system.domain.ActivityTemplateCopywriting;
import com.xz.system.domain.SysTenantConfig;
import com.xz.system.domain.SysTenantTemplate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * All rights Reserved, Designed By www.sunseagear.com
 *
 * @version V1.0
 * @package sys
 * @title: 租户管理控制器
 * @description: 租户管理控制器
 * @author: 未知
 * @date: 2019-11-28 06:24:52
 * @copyright: www.sunseagear.com Inc. All rights reserved.
 */
@Mapper
public interface SysTenantMapper extends BaseMapper<SysTenant> {
    /**
     * 查询租户参数配置
     *
     * @param id 租户参数配置主键
     * @return 租户参数配置
     */
    public SysTenantConfig selectSysTenantConfigById(Long id);

  /**
   * 批量查询租户参数配置
   *
   * @param ids 租户参数配置主键集合
   * @return 租户参数配置
   */
  public List<SysTenantConfig> selectSysTenantConfigByBatchId( @Param("ids") List<Long> ids);

    /**
     * 新增租户参数配置
     *
     * @param sysTenantConfig 租户参数配置
     * @return 结果
     */
    public int insertSysTenantConfig(SysTenantConfig sysTenantConfig);

    /**
     * 修改租户参数配置
     *
     * @param sysTenantConfig 租户参数配置
     * @return 结果
     */
    public int updateSysTenantConfig(SysTenantConfig sysTenantConfig);

    SysTenant checkUnique(@Param("tenantName") String tenantName,@Param("tenantCode") String tenantCode);

    //新增租户活动模板
    Integer insertTemplate(@Param("templateList") List<SysTenantTemplate> templateList);
    //查询模板
    List<Long> selectTenantTemplate(@Param("tenantId") Long tenantId);
    //删除模板
    Integer deleteTemplate(@Param("tenantId") Long tenantId);
    //商户信息
    List<Map<String,Object>> tenantList();
    //商户信息--租户id
    SysTenantConfig sysTenantConfigInfo(Long tenantId);

}
