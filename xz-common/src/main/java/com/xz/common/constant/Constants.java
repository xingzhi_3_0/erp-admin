package com.xz.common.constant;

import com.xz.common.config.XzConfig;
import io.jsonwebtoken.Claims;

import java.util.regex.Pattern;

/**
 * 通用常量信息
 *
 * @author xz
 */
public class Constants
{
    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * GBK 字符集
     */
    public static final String GBK = "GBK";

    /**
     * http请求
     */
    public static final String HTTP = "http://";

    /**
     * https请求
     */
    public static final String HTTPS = "https://";

    /**
     * 通用成功标识
     */
    public static final String SUCCESS = "0";

    /**
     * 通用失败标识
     */
    public static final String FAIL = "1";

    /**
     * 登录成功
     */
    public static final String LOGIN_SUCCESS = "Success";

    /**
     * 注销
     */
    public static final String LOGOUT = "Logout";

    /**
     * 注册
     */
    public static final String REGISTER = "Register";

    /**
     * 登录失败
     */
    public static final String LOGIN_FAIL = "Error";

    /**
     * 验证码 redis key
     */
    public static final String CAPTCHA_CODE_KEY = XzConfig.getRedisPrefix()+"captcha_codes:";

    /**
     * 登录用户 redis key
     */
    public static final String LOGIN_TOKEN_KEY = XzConfig.getRedisPrefix()+"login_tokens:";

    /**
     * 防重提交 redis key
     */
    public static final String REPEAT_SUBMIT_KEY = XzConfig.getRedisPrefix()+"repeat_submit:";

    /**
     * 限流 redis key
     */
    public static final String RATE_LIMIT_KEY = "rate_limit:";

    /**
     * 验证码有效期（分钟）
     */
    public static final Integer CAPTCHA_EXPIRATION = 2;

    /**
     * 令牌
     */
    public static final String TOKEN = "token";

    /**
     * 令牌前缀
     */
    public static final String TOKEN_PREFIX = "Bearer ";

    /**
     * 令牌前缀
     */
    public static final String LOGIN_USER_KEY = "login_user_key";

    /**
     * 用户ID
     */
    public static final String JWT_USERID = "userid";

    /**
     * 用户名称
     */
    public static final String JWT_USERNAME = Claims.SUBJECT;

    /**
     * 用户头像
     */
    public static final String JWT_AVATAR = "avatar";

    /**
     * 创建时间
     */
    public static final String JWT_CREATED = "created";

    /**
     * 用户权限
     */
    public static final String JWT_AUTHORITIES = "authorities";

    /**
     * 参数管理 cache key
     */
    public static final String SYS_CONFIG_KEY = XzConfig.getRedisPrefix()+"sys_config:";

    /**
     * 字典管理 cache key
     */
    public static final String SYS_DICT_KEY = XzConfig.getRedisPrefix()+"sys_dict:";

    /**
     * 企业微信 cache key
     */
    public static final String SYS_QYAPI_KEY = XzConfig.getRedisPrefix()+"qyapi:";
    /**
     * 企业微信 cache key
     */
    public static final String SYS_DDAPI_KEY = XzConfig.getRedisPrefix()+"ddapi:";

    /**
     * 资源映射路径 前缀
     */
    public static final String RESOURCE_PREFIX = "/";

    /**
     * RMI 远程方法调用
     */
    public static final String LOOKUP_RMI = "rmi://";

    /**
     * LDAP 远程方法调用
     */
    public static final String LOOKUP_LDAP = "ldap://";
    /**
     * 只取结果集第一个
     */
    public static final String SQL_LAST_LIMIT1 = "LIMIT 1";
    /**
     * 字典类型：电子合同配置项
     */
    public static final String DICT_TYPE_ELECTRONIC_CONTRACT_CONFIG = "electronic_contract_config";
    /**
     * 所有的正向定义，如：正常，启用，成功
     */
    public static final String YES = "1";
    /**
     * 所有的反向定义，如：异常，禁用，失败
     */
    public static final String NO = "0";
    /**
     * 所有的正向定义，如：正常，启用，成功
     */
    public static final int YES_NUM = 1;
    /**
     * 所有的反向定义，如：异常，禁用，失败
     */
    public static final int NO_NUM = 0;
    /**
     * 订单车辆某一项配置
     */
    public static final Pattern ORDER_CAR_PATTERN = Pattern.compile("(^\\d+,)|(,\\d+$)|(\\d+,)");
}
