package com.xz.common.core.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * All rights Reserved, Designed By www.sunseagear.com
 *
 * @version V1.0
 * @package sys
 * @title: 租户管理控制器
 * @description: 租户管理控制器
 * @author: 未知
 * @date: 2019-11-28 06:24:52
 * @copyright: www.sunseagear.com Inc. All rights reserved.
 */

@Data
@TableName("sys_tenant")
public class SysTenant extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "tenant_id", type = IdType.AUTO)
    private Long tenantId; //id
    @TableField(value = "tenant_name")
    private String tenantName;  //商户名称
    @TableField(value = "abbreviation")
    private String abbreviation;  //商户简称
    @TableField(value = "tenant_code")
    private String tenantCode;  //商户号
    @TableField(value = "tenant_concat")
    private String tenantConcat;  //商户联系人
    @TableField(value = "tenant_phone")
    private String tenantPhone;  //联系人电话"
    /** 商户状态：0 正常  1.停用 */
    @TableField(value = "tenant_flag")
    private Integer tenantFlag;
    /** 商户类型：0: 平台  1.商户 */
    @TableField(value = "tenant_type")
    private Integer tenantType;
    /** 租户开通的业务类型 */
    @TableField(value = "tenant_business")
    private String tenantBusiness;
    /** 删除标识 */
    @TableField(value = "del_flag")
    private String delFlag;
    @TableField(exist = false)
    private String userName;

    @TableField(exist = false)
    private List<String> roleKeys;

    @TableField(exist = false)
    private String illegalQueryPrice;
    @TableField(exist = false)
    private String creditQueryRules;





}
