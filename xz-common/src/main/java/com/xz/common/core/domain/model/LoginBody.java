package com.xz.common.core.domain.model;

import javax.validation.constraints.NotBlank;

/**
 * 用户登录对象
 * 
 * @author xz
 */
public class LoginBody
{
    /**
     * 用户名
     */
    //@NotBlank(message = "用户名不能为空")
    private String username;

    /**
     * 用户密码
     */
    @NotBlank(message = "用户密码不能为空")
    private String password;

    /**
     * 验证码
     */
   // @NotBlank(message = "验证码不能为空")
    private String code;

    /**
     * 唯一标识
     */
    private String uuid = "";

    /**
     * 商户号
     */
    @NotBlank(message = "商户号不能为空")
    private String tenantCode;



    //pc,mobile ,app
    private String source;

    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getUuid()
    {
        return uuid;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }
}
