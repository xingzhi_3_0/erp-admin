package com.xz.common.core.domain.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 用户切换门店对象
 * 
 * @author xz
 */
@Data
public class RefreshDeptBody
{
    /**
     * 切换门店id
     */
    private Long changeDeptId;

    private String changeDeptName;

}
