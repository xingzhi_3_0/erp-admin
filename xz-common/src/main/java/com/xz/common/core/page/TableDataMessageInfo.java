package com.xz.common.core.page;


import lombok.Data;

/**
 * 表格分页数据对象
 * 
 * @author xz
 */
@Data
public class TableDataMessageInfo extends TableDataInfo

{

    private Integer unreadCount;


    private Integer readCount;


}
