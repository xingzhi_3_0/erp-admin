package com.xz.common.core.domain.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 用户登录对象
 * 
 * @author xz
 */
@Data
public class WxJsCodeLoginBody
{
    /**
     * 手机号
     */
    @NotBlank(message = "手机号不能为空")
    private String phone;

    /**
     * 验证码
     */
    @NotBlank(message = "验证码不能为空")
    private String verifyCode;

    /**
     * 商户id
     */
    private Long tenantId;
}
