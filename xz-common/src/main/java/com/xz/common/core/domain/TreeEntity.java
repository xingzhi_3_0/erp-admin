package com.xz.common.core.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xz.common.utils.StringUtils;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Tree基类
 *
 * @author xz
 */
@Data
public abstract class TreeEntity extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    /** 父部门ID */
    protected Long parentId = 0L;

    /** 祖级列表 */
    protected String ancestors = "0,";

    public abstract Long getId();

    public String makeSelfAsNewParentIds() {
        if (StringUtils.isEmpty(getAncestors())) {
            return getId() + ",";
        }
        return getAncestors() + getId() + ",";
    }
}
