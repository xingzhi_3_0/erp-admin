package com.xz.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author renyong
 * @date 2023/7/18 19:21
 * @description
 */
@RequiredArgsConstructor
@Getter
public enum TypeOfCertificateEnum {
    身份证("0", "身份证"),
    护照("1", "护照"),
    军人身份证("2", "军人身份证"),
    工商登记证("3", "工商登记证"),
    税务登记证("4", "税务登记证"),
    股东代码证("5", "股东代码证"),
    社会保障卡("6", "社会保障卡"),
    组织机构代码证("7", "组织机构代码证"),
    企业营业执照("8", "企业营业执照"),
    法人代码证("9", "法人代码证"),
    武装警察身份证件("A", "武装警察身份证件"),
    港澳居民往来内地通行证("B", "港澳居民往来内地通行证"),
    台湾居民来往大陆通行证("C", "台湾居民来往大陆通行证"),
    户口簿("E", "户口簿"),
    临时居民身份证("F", "临时居民身份证"),
    警察证("G", "警察(警官)证"),
    事业单位法人证书("H", "事业单位法人证书"),
    社会团体登记证书("J", "社会团体登记证书"),
    民办非企业登记证书("K", "民办非企业登记证书"),
    外企常驻代表机构登记证("L", "外国(地区)企业常驻代表机构登记证"),
    GOVERNMENT_APPROVAL("M", "政府批文"),
    FOREIGNER_PERMANENT_RESIDENCE_PERMIT("P", "外国人永久居留证"),
    UNIFIED_SOCIAL_CREDIT_IDENTIFIER("Q", "统一社会信用代码(Jlht 新增)"),
    OTHER("Z", "其他");

    private final String code;
    private final String name;
}
