package com.xz.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author renyong
 * @date 2023/7/18 15:11
 * @description 企业认证 签章管理员类型
 */
@RequiredArgsConstructor
@Getter
public enum EnterpriseContactsTypeEnum {
    LEGAL_PERSON("1","法人"),
    AGENT_PERSON("2","代理人");
    /**
     * 类型
     */
    private final String type;
    /**
     * 描述
     */
    private final String description;

    public static Map<String,EnterpriseContactsTypeEnum> enterpriseContactsTypeEnumMap;

    static {
        enterpriseContactsTypeEnumMap = Arrays.stream(EnterpriseContactsTypeEnum.values())
                .collect(Collectors.toMap(EnterpriseContactsTypeEnum::getType, Function.identity()));
    }
}
