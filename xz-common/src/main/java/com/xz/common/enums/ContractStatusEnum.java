package com.xz.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author renyong
 * @date 2023-7-21 15:06:21
 * @description 合同实例状态 参考字典contract_status
 */
@RequiredArgsConstructor
@Getter
public enum ContractStatusEnum {
    /**
     * 0 这个主要是前端传参用，后台根据数据内容判断废弃类型
     */
    OBSOLETE(0,"废弃"),
    UNSIGNED(-1,"未签署"),

    WAIT_LESSOR_SIGN(1,"待出租方签署"),
    WAIT_LESSEE_SIGN(2,"待承租方签署"),
    COMPLETED(3,"已完成"),
    OBSOLETE_SIGNED(4,"已废弃(已签署)"),
    OBSOLETE_UNSIGNED(5,"已废弃(未签署)");

    /**
     * 类型
     */
    private final int type;
    /**
     * 描述
     */
    private final String description;

    public static Map<Integer, ContractStatusEnum> enterpriseContactsTypeEnumMap;

    static {
        enterpriseContactsTypeEnumMap = Arrays.stream(ContractStatusEnum.values())
                .collect(Collectors.toMap(ContractStatusEnum::getType, Function.identity()));
    }
}
