package com.xz.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author renyong
 * @date 2023-7-24 10:56:48
 * @description 合同模板状态
 */
@RequiredArgsConstructor
@Getter
public enum ContractTempStatusEnum {
    ENABLE(1,"启用"),
    DISABLE(2,"禁用");

    /**
     * 类型
     */
    private final int type;
    /**
     * 描述
     */
    private final String description;

    public static Map<Integer, ContractTempStatusEnum> enterpriseContactsTypeEnumMap;

    static {
        enterpriseContactsTypeEnumMap = Arrays.stream(ContractTempStatusEnum.values())
                .collect(Collectors.toMap(ContractTempStatusEnum::getType, Function.identity()));
    }
}
