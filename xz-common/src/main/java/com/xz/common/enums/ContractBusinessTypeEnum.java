package com.xz.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author renyong
 * @date 2023-7-31 09:48:59
 * @description 合同业务类型
 */
@RequiredArgsConstructor
@Getter
public enum ContractBusinessTypeEnum {
    CAR_RENTAL(1,"租车合同"),
    RENEWAL(2,"续租合同");

    /**
     * 类型
     */
    private final int type;
    /**
     * 描述
     */
    private final String description;

    public static Map<Integer, ContractBusinessTypeEnum> contractBusinessTypeEnumMap;

    static {
        contractBusinessTypeEnumMap = Arrays.stream(ContractBusinessTypeEnum.values())
                .collect(Collectors.toMap(ContractBusinessTypeEnum::getType, Function.identity()));
    }
}
