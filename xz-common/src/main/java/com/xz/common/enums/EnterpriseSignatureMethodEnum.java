package com.xz.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author renyong
 * @date 2023-7-18 15:16:39
 * @description 企业认证 签章方式
 */
@RequiredArgsConstructor
@Getter
public enum EnterpriseSignatureMethodEnum {
    AUTO(1,"自动签章"),
    MANUAL(2,"手动签章");
    /**
     * 类型
     */
    private final int type;
    /**
     * 描述
     */
    private final String description;

    public static Map<Integer, EnterpriseSignatureMethodEnum> enterpriseSignatureMethodEnumMap;

    static {
        enterpriseSignatureMethodEnumMap = Arrays.stream(EnterpriseSignatureMethodEnum.values())
                .collect(Collectors.toMap(EnterpriseSignatureMethodEnum::getType, Function.identity()));
    }
}
