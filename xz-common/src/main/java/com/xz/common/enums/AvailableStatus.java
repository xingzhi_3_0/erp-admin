package com.xz.common.enums;

import java.util.Arrays;

/**
 * 通用的删除状态
 * 
 * @author xz
 */
public enum AvailableStatus
{
    YES(1, "开启"), NO(0, "关闭");

    private final int code;
    private final String info;

    AvailableStatus(int code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public int getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }

    public static boolean isExistence(Integer code){
        if(null == code){
            return false;
        }

        AvailableStatus[] values = values();
        boolean isFlag = false;
        for (AvailableStatus value : values) {
            if(value.getCode() == code){
                isFlag = true;
                break;
            }
        }
        return isFlag;
    }
}
