package com.xz.common.enums;

/**
 * 通用的删除状态
 * 
 * @author xz
 */
public enum DelFlagStatus
{
    YES(1, "已删除"), NO(0, "未删除");

    private final int code;
    private final String info;

    DelFlagStatus(int code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public int getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }
}
