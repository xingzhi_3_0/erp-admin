package com.xz.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author renyong
 * @date 2023-7-26 14:51:01
 * @description 客户身份证件类型 参考字典document_type
 */
@RequiredArgsConstructor
@Getter
public enum DocumentTypeEnum {
    ID_CARD(1,"身份证","0"),
    INTERNATIONAL_PASSPORT(2,"国际护照","1"),
    TAIWANESE_SYNDROME(3,"台胞证","B"),
    /**
     * 军人证，没有则默认按身份证算
     */
    MILITARY_CARD(4,"军人证","0"),
    PASSPORT(5,"港澳通行证","C");

    /**
     * 类型
     */
    private final int type;
    /**
     * 描述
     */
    private final String description;
    /**
     * 法大大证件类型
     */
    private final String fadadaCustomerIdentType;

    public static Map<Integer, DocumentTypeEnum> documentTypeEnumMap;

    static {
        documentTypeEnumMap = Arrays.stream(DocumentTypeEnum.values())
                .collect(Collectors.toMap(DocumentTypeEnum::getType, Function.identity()));
    }
}
