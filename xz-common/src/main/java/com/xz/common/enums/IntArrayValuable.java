package com.xz.common.enums;

/**
 * @author lizhihan
 * @date 2022-11-23
 */
public interface IntArrayValuable {

	/**
	 * @return int 数组
	 */
	int[] array();
}
