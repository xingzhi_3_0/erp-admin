package com.xz.common.validation;

import javax.validation.groups.Default;

/**
 * @author lizhihan
 * @date 2023-03-21
 */
public interface SituationGroup extends Default {

    interface A {}
    interface B {}
    interface C {}
    interface D {}
}
