package com.xz.common.validation;

/**
 * @author lizhihan
 * @date 2023-03-21
 */
public interface ValidGroup {

    interface Create {}
    interface Update {}
    interface UpdateEnable {}
    interface Delete {}
}
