package com.xz.common.utils.file;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ResourceFileUtils {


    /**
     * description:file转base64
     * create Time:2020-08-26 16:12:20
     *
     * @param file
     * @return
     */
    public static String file2Base64(File file) {
        if (file == null) {
            return null;
        }
        String base64 = null;

        try (FileInputStream fin = new FileInputStream(file);) {
            byte[] buff = new byte[fin.available()];
            fin.read(buff);
            base64 = Base64.encode(buff);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return base64;
    }

    /**
     * description:base64转file
     * create Time:2020-08-26 16:22:31
     *
     * @param base64
     * @return
     */
    public static File base64ToFile(String base64) {
        if (StringUtils.isBlank(base64)) {
            return null;
        }
        byte[] buff = Base64.decode(base64);
        File file = null;
        FileOutputStream fout = null;
        try {
            file = File.createTempFile("tmp", null);
            fout = new FileOutputStream(file);
            fout.write(buff);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fout != null) {
                try {
                    fout.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return file;
    }

    /**
     * description:base64转MultipartFile
     * create Time:2020-08-26 16:22:31
     *
     * @param base64
     * @return
     */
    public static MultipartFile base64ToMultipart(String base64) {
        try {
            String[] baseStrs = base64.split(",");

            BASE64Decoder decoder = new BASE64Decoder();
            byte[] b = new byte[0];
            b = decoder.decodeBuffer(baseStrs[1]);

            for (int i = 0; i < b.length; ++i) {
                if (b[i] < 0) {
                    b[i] += 256;
                }
            }
            return new BASE64DecodedMultipartFile(b, baseStrs[0]);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * description:MultipartFile转文件
     * create Time:2020-08-26 16:22:31
     *
     * @param
     * @return
     */
    public static File multipartFile2File(MultipartFile multipartFile, File file) {
        try {
            FileUtils.copyInputStreamToFile(multipartFile.getInputStream(), file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    /**
     * description:MultipartFile转文件
     * create Time:2020-08-26 16:22:31
     *
     * @param
     * @return
     */
    public static File multipartFile2File(MultipartFile multipartFile, String path) {
        File file = new File(path);
        try {
            FileUtils.copyInputStreamToFile(multipartFile.getInputStream(), file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

}



