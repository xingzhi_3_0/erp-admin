package com.xz.common.utils.uuid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import java.util.Calendar;
import java.util.Date;

/**
 * @author xf
 * @version 1.0.0
 * @ClassName PrimaryKeyUtil
 * @Description TODO 利用redis生成数据库全局唯一性id
 * @createTime 2020.10.22 10:42
 */
@Service
public class PrimaryKeyUtil {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 获取年的后两位加上一年多少天+当前小时数作为前缀
     * @param date
     * @return
     */
    public String getOrderIdPrefix(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        //String.format("%1$02d", var)
        // %后的1指第一个参数，当前只有var一个可变参数，所以就是指var。
        // $后的0表示，位数不够用0补齐，如果没有这个0（如%1$nd）就以空格补齐，0后面的n表示总长度，总长度可以可以是大于9例如（%1$010d），d表示将var按十进制转字符串，长度不够的话用0或空格补齐。
        String monthFormat = String.format("%1$02d", month+1);
        String dayFormat = String.format("%1$02d", day);
        String hourFormat = String.format("%1$02d", hour);

        return year + monthFormat + dayFormat+hourFormat;
    }

    /**
     * 生成订单号
     * @param prefix
     * @return
     */
    public Long orderId(String prefix) {
        String key = "ORDER_ID_" + prefix;
        String orderId = null;
        try {
            Long increment = redisTemplate.opsForValue().increment(key,1);
            //往前补6位
            orderId=prefix+String.format("%1$06d",increment);
        } catch (Exception e) {
            System.out.println("生成单号失败");
            e.printStackTrace();
        }
        return Long.valueOf(orderId);
    }

    public static void main(String[] args) {
        long startMillis = System.currentTimeMillis();
        PrimaryKeyUtil primaryKeyUtil = new PrimaryKeyUtil();
        String orderIdPrefix = primaryKeyUtil.getOrderIdPrefix(new Date());
        //生成单个id
        //Long aLong = primaryKeyUtil.orderId(orderIdPrefix);
        for (int i = 0; i < 1000; i++) {
            Long aLong = primaryKeyUtil.orderId(orderIdPrefix);
            System.out.println(aLong);
        }
        long endMillis = System.currentTimeMillis();
        System.out.println("测试生成1000个使用时间："+(endMillis-startMillis)+",单位毫秒");
        //测试生成1000个使用时间：514,单位毫秒
    }


}
