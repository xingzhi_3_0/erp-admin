package com.xz.common.utils;

/**
 * 开发框架基础架构的公共常量设置类（行知网络使用此基础框架的项目均可使用此常量类）
 *
 * @author xieyong
 *
 */
public class Constant {


	/** 整个应用0表示失败 */
	public static final String FAIL = "0";
	public final static String RES_ERROR_MSG="失败";
	/** 整个应用1表示成功 */
	public static final String SECCUESS = "1";
	public final static String RES_SUCCESS_MSG="成功";
	/** 未登录 */
	public static final String NO_LOGIN = "-1";
	/** 未授权 */
	public static final String NO_AUTHORITY = "-2";
	/** 无数据 */
	public static final String NO_DATA = "3";

	public static final String NO_RECORD = "暂无数据";

	/** 整个应用0表示否 */
	public static final int NO = 0;
	/** 整个应用1表示是 */
	public static final int YES = 1;

	/** 禁用 */
	public static final int DISABLED = 0;
	/** 启用 */
	public static final int ENABLED = 1;

	/** 不删除 */
	public static final int DEL_STATE_NO = 0;
	/** 已删除 */
	public static final int DEL_STATE_YES = 1;

	/** 默认的分页每页记录数 */
	public static final int DEFAULT_PAGE_SIZE = 10;

	// web端登录方式
	public static final String PHONE_CODE = "1";// 手机+验证码

	public static final String PHONE_PASSWORD = "2";// 手机+密码

	/** 各个验证码的有效时长，以秒为单位 */
	public static final int regCodeExpire = 120; // 注册验证码的过期时间为120秒；

	public static final int codeExpire = 2;// 验证码过期时间2分钟；

	/**
	 * 登录token失效时间
	 */
	public static final int webloginTokenExpire = 1800;// 失效时间30分钟=1800秒

	public static final int apploginTokenExpire = 259200;// 失效时间30分钟=1800秒

	// 上传目录
	public static final String UPLOADFILE = "ldy_images";


	// 消息通知业务类型 1、保险 2、保养、3、违章 4、租金逾期缴费 6、年检 7、车辆租赁到期 8、租金生成账单缴费
	public static final int MSG_INSURANCE = 1;

	public static final int MSG_MAINTAIN = 2;

	public static final int MSG_REGULATIONS = 3;

	public static final int MSG_RENT = 4;

	public static final int MSG_INSPECTION = 6;

	public static final int MSG_CAR_RENTAL = 7;

	public static final int MSG_RENT_BILL = 8;


	// 线索类型
	public static final String CLUES_RENT_CAR = "0"; //租车

	public static final String CLUES_HY_BM = "1"; //货源报名

	public static final String CLUES_YY_SJ = "2"; //预约试驾

	public static final String CLUES_HD_ZX = "3"; //活动咨询

	public static final String CLUES_HT_JM = "4"; //合同加盟



	//==================小程序========================
	//失败
	public static final String REQ_FAIL_STATUS="0";
	//成功
	public static final String REQ_SUCCESS_STATUS="1";
	//参数异常
	public static final String REQ_PARAM_ERROR_STATUS="400";
	//需要有token,标志用户拒绝授权,需要授权操作
	public static final String REQ_NOT_AUTH_STATUS="403";
	//需要绑定手机号
	public static final String REQ_COMPARE_PARAM_STATUS="407";
	//服务器异常
	public static final String REQ_SERVER_ERROR_STATUS="500";
	/**
	 * 验证码为空
	 */
	public final static String MSG_NULL_CODE="1001";
	/**
	 * 验证码已过期
	 */
	public final static String MSG_EXPIRE_CODE="1002";
	/**
	 * 验证码不正确
	 */
	public final static String MSG_CORRECT_CODE="1003";
	/**
	 * sessionKey失效
	 */
	public final static String SESSION_KEY_INVALID_CODE="4001";




	/***************************************验证码相关***********************************/
	/**客户端加密参数KEY*/
	public static final String MINIPROGRAM_SEND_CODE_MSGKEY="TJFS_SP_2020_826";
	public static final String MINIPROGRAM_DEFAULT_CODE_MSG="123456";
	public static final String MINIPROGRAM_DEFAULT_PAY_AMOUNT="1";
	/**redis存储验证码key*/
	public static final String MINIPROGRAM_MSG_REDIS_SAVE_KEY="MINIPROGRAM_MSG_REDIS_SAVE_KEY";
	/**各个验证码的有效时长，以秒为单位*/
	public static final int MSG_CODE_EXPIRE = 120;	//验证码的过期时间为120秒；
	/**调用接口失效时间*/
	public static final int MSG_REQUEST_EEXPIRE_TIME = 20;	//请求超时
	/***************************************验证码相关***********************************/

	//==================小程序========================
	/** MQ推送参数配置 **/
	public static final String MQ_URL = "http://10.7.17.106:8641/xz-mq-provider/provider/";

	public static final String MQ_TOKEN = "ZSXTWDFVXCSGG455DWW2123@@@%%%!!!%*&";





	/***************************************小程序端预约发送短信***********************************/
	/**
	 * 选车
	 */
	public static final String XUANCHE_MSG_CONTENT="客户CUSTOMER通过小程序提交了选车申请,请您及时联系!手机号为:PHONE";
	/**
	 * 货源合作
	 */
	public static final String HUOYUAN_MSG_CONTENT="客户CUSTOMER通过小程序申请了货源合作,请您及时联系!手机号为:PHONE";
	/**
	 * 预约试驾
	 */
	public static final String YUYUESHIJIA_MSG_CONTENT="客户CUSTOMER通过小程序预约了试驾,请您及时联系!手机号为:PHONE";
	/**活动咨询*/
	public static final String HUODONGZIXUN_MSG_CONTENT="客户CUSTOMER通过小程序咨询了活动ACTIVITY,请您及时联系!手机号为:PHONE";
	/**合同加盟*/
	public static final String HETONGJIAMENG_MSG_CONTENT = "客户CUSTOMER通过小程序申请了合作加盟,请您及时联系!手机号为:PHONE";
	/***************************************验证码相关***********************************/


}
