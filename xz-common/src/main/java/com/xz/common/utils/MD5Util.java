package com.xz.common.utils;

import java.security.MessageDigest;
import java.util.UUID;

public class MD5Util {

	private static final String hexDigits[] = { "0", "1", "2", "3", "4", "5",
			"6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };

	public static String byteArrayToHexString(byte b[]) {
		StringBuffer resultSb = new StringBuffer();
		for (int i = 0; i < b.length; i++)
			resultSb.append(byteToHexString(b[i]));

		return resultSb.toString();
	}

	private static String byteToHexString(byte b) {
		int n = b;
		if (n < 0)
			n += 256;
		int d1 = n / 16;
		int d2 = n % 16;
		return hexDigits[d1] + hexDigits[d2];
	}

	public static String MD5Encode(String origin, String charsetname) {
		String resultString = null;
		try {
			resultString = new String(origin);
			MessageDigest md = MessageDigest.getInstance("MD5");
			if (charsetname == null || "".equals(charsetname))
				resultString = byteArrayToHexString(md.digest(resultString
						.getBytes()));
			else
				resultString = byteArrayToHexString(md.digest(resultString
						.getBytes(charsetname)));
		} catch (Exception exception) {
		}
		return resultString;
	}

	/**
	 * 生成合同唯一编号
	 * @param tenantId 租户id
	 * @return
	 * @author renyong on 2023/7/31 9:36
	 */
	public static String getContractDocId(Long tenantId){
		StringBuffer sb = new StringBuffer(UUID.randomUUID().toString());
		if (tenantId!=null){
			sb.append(tenantId);
		}
		sb.append(System.currentTimeMillis());
		return MD5Encode(sb.toString(), "UTF-8");
	}
}
