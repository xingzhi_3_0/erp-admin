package com.xz.common.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

/**
 * @ClassName RedisCode * @Description TODO
 * @Author Administrator
 * @Date 18:24 2023/12/21
 * @Version 1.0
 **/
@Component
public class RedisCode {
    @Autowired
    private RedisTemplate redisTemplate;

    //2.添加静态的变量
    public static RedisTemplate redis;

    @PostConstruct
    public void getRedisTemplate(){
        redis=this.redisTemplate;
    }


    /**
     * 依赖Redis的原子性，设置数据中的自动递增，有效期到12点---加时间
     *
     * @param key
     */
    public static String getCode(String key) {
        StringBuilder orderNo = new StringBuilder();
        String format = new SimpleDateFormat("yyyyMMdd").format(new Date());
        orderNo.append(key).append(format);
        Object object = redis.opsForValue().get(key);
        long increment=1;
        if(Objects.isNull(object)){
            redis.opsForValue().set(key,increment);
        }else{
            increment = new Long(object.toString()) + 1;
            redis.opsForValue().set(key,increment);

        }
        NumberFormat formatter = NumberFormat.getNumberInstance();
        formatter.setMinimumIntegerDigits(6);
        formatter.setGroupingUsed(false);
        //当日返回的id最低六位
        orderNo.append(formatter.format(increment + 1));
        return orderNo.toString();
    }
    /**
     * 依赖Redis的原子性，设置数据中的自动递增，有效期到12点--不加时间
     *
     * @param key
     */
    public static String getCodeNoTime(String key) {
        StringBuilder orderNo = new StringBuilder();
        orderNo.append(key);
        Object object = redis.opsForValue().get(key);
        long increment=1;
        if(Objects.isNull(object)){
            redis.opsForValue().set(key,increment);
        }else{
            increment = new Long(object.toString()) + 1;
            redis.opsForValue().set(key,increment);

        }
        NumberFormat formatter = NumberFormat.getNumberInstance();
        formatter.setMinimumIntegerDigits(6);
        formatter.setGroupingUsed(false);
        //当日返回的id最低六位
        orderNo.append(formatter.format(increment + 1));
        return orderNo.toString();
    }

    /**
     * 获取次日凌晨的时间戳
     *
     * @return
     */
    public static long getNextDay() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        //今天凌晨
        return c.getTimeInMillis() / 1000 + (24 * 60 * 60);
    }

    public static void main(String[] args) {
        System.out.println(getNextDay());
    }
}
