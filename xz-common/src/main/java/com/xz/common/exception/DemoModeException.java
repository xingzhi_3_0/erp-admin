package com.xz.common.exception;

/**
 * 演示模式异常
 * 
 * @author xz
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
