package com.xz.framework.config;

import com.xz.framework.interceptor.TenantSetInterceptor;
import com.xz.tenant.TenantPropertiesConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import com.xz.common.config.XzConfig;
import com.xz.common.constant.Constants;
import com.xz.framework.interceptor.RepeatSubmitInterceptor;

/**
 * 通用配置
 *
 * @author xz
 */
@Configuration
public class ResourcesConfig implements WebMvcConfigurer
{
    @Autowired
    private RepeatSubmitInterceptor repeatSubmitInterceptor;
    @Autowired
    private TenantSetInterceptor tenantSetInterceptor;
    @Autowired
    private TenantPropertiesConfig configPropertiesConfig;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry)
    {
        /** 本地文件上传路径 */
        registry.addResourceHandler(Constants.RESOURCE_PREFIX + "/**").addResourceLocations("file:" + XzConfig.getProfile() + "/");

        /** swagger配置 */
        registry.addResourceHandler("/swagger-ui/**").addResourceLocations("classpath:/META-INF/resources/webjars/springfox-swagger-ui/");
    }

    /**
     * 自定义拦截规则
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry)
    {
        registry.addInterceptor(repeatSubmitInterceptor).addPathPatterns("/**");
        registry.addInterceptor(tenantSetInterceptor).addPathPatterns("/**")
                //todo 测试支付接口，调试完毕后删除
                .excludePathPatterns("/login","/wxJsCode/login","/wxJsCode/getVerifyCode","/captchaImage","/joinpay/**","/error","/car/altImageApproveNotify","/car/altAuthNotify", "/merchant/tenant/page", "/car/illegalWorkOrder/workOrderCarPage", "/car/illegalWorkOrder/list", "/car/illegalWorkOrder/sync", "/car/workOrderAccount/list")
        ;
    }

    /**
     * 跨域配置
     */
    @Bean
    public CorsFilter corsFilter()
    {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        // 设置访问源地址
        config.addAllowedOriginPattern("*");
        // 设置访问源请求头
        config.addAllowedHeader("*");
        // 设置访问源请求方法
        config.addAllowedMethod("*");
        // 对接口配置跨域设置
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}
