package com.xz.optometry.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.optometry.domain.OptometryItem;

/**
 * 验光配镜明细Service接口
 * 
 * @author xz
 * @date 2024-01-31
 */
public interface IOptometryItemService extends IService<OptometryItem> {
    /**
     * 查询验光配镜明细
     * 
     * @param id 验光配镜明细主键
     * @return 验光配镜明细
     */
    public OptometryItem selectOptometryItemById(Long id);

    /**
     * 查询验光配镜明细列表
     * 
     * @param optometryItem 验光配镜明细
     * @return 验光配镜明细集合
     */
    public List<OptometryItem> selectOptometryItemList(OptometryItem optometryItem);

    /**
     * 新增验光配镜明细
     * 
     * @param optometryItem 验光配镜明细
     * @return 结果
     */
    public int insertOptometryItem(OptometryItem optometryItem);

    /**
     * 修改验光配镜明细
     * 
     * @param optometryItem 验光配镜明细
     * @return 结果
     */
    public int updateOptometryItem(OptometryItem optometryItem);

    /**
     * 批量删除验光配镜明细
     * 
     * @param ids 需要删除的验光配镜明细主键集合
     * @return 结果
     */
    public int deleteOptometryItemByIds(Long[] ids);

    /**
     * 删除验光配镜明细信息
     * 
     * @param id 验光配镜明细主键
     * @return 结果
     */
    public int deleteOptometryItemById(Long id);
}
