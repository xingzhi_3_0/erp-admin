package com.xz.optometry.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.optometry.mapper.OptometryItemMapper;
import com.xz.optometry.domain.OptometryItem;
import com.xz.optometry.service.IOptometryItemService;

/**
 * 验光配镜明细Service业务层处理
 *
 * @author xz
 * @date 2024-01-31
 */
@Service
public class OptometryItemServiceImpl extends ServiceImpl<OptometryItemMapper, OptometryItem> implements IOptometryItemService {
    @Autowired
    private OptometryItemMapper optometryItemMapper;

    /**
     * 查询验光配镜明细
     *
     * @param id 验光配镜明细主键
     * @return 验光配镜明细
     */
    @Override
    public OptometryItem selectOptometryItemById(Long id) {
        return optometryItemMapper.selectOptometryItemById(id);
    }

    /**
     * 查询验光配镜明细列表
     *
     * @param optometryItem 验光配镜明细
     * @return 验光配镜明细
     */
    @Override
    public List<OptometryItem> selectOptometryItemList(OptometryItem optometryItem) {
        return optometryItemMapper.selectOptometryItemList(optometryItem);
    }

    /**
     * 新增验光配镜明细
     *
     * @param optometryItem 验光配镜明细
     * @return 结果
     */
    @Override
    public int insertOptometryItem(OptometryItem optometryItem) {
        return optometryItemMapper.insertOptometryItem(optometryItem);
    }

    /**
     * 修改验光配镜明细
     *
     * @param optometryItem 验光配镜明细
     * @return 结果
     */
    @Override
    public int updateOptometryItem(OptometryItem optometryItem) {
        return optometryItemMapper.updateOptometryItem(optometryItem);
    }

    /**
     * 批量删除验光配镜明细
     *
     * @param ids 需要删除的验光配镜明细主键
     * @return 结果
     */
    @Override
    public int deleteOptometryItemByIds(Long[] ids) {
        return optometryItemMapper.deleteOptometryItemByIds(ids);
    }

    /**
     * 删除验光配镜明细信息
     *
     * @param id 验光配镜明细主键
     * @return 结果
     */
    @Override
    public int deleteOptometryItemById(Long id) {
        return optometryItemMapper.deleteOptometryItemById(id);
    }
}
