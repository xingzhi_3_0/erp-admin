package com.xz.optometry.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.optometry.domain.Optometry;
import com.xz.optometry.vo.OptometryExportVo;
import com.xz.optometry.vo.OptometryInfoVo;
import com.xz.optometry.vo.OptometryVo;

/**
 * 验光配镜Service接口
 *
 * @author xz
 * @date 2024-01-31
 */
public interface IOptometryService extends IService<Optometry> {
    /**
     * 查询验光配镜
     *
     * @param id 验光配镜主键
     * @return 验光配镜
     */
    public OptometryInfoVo selectOptometryById(Long id);

    /**
     * 查询验光配镜列表
     *
     * @param optometry 验光配镜
     * @return 验光配镜集合
     */
    public List<OptometryVo> selectOptometryList(Optometry optometry);
    /**
     * 查询验光配镜列表(导出)
     *
     * @param optometry 验光配镜
     * @return 验光配镜集合
     */
    public List<OptometryExportVo> selectOptometryListForExport(Optometry optometry);

    /**
     * 查询验光配镜列表
     *
     * @param optometry 验光配镜
     * @return 验光配镜集合
     */
    public List<OptometryVo> selectByPatientId(Optometry optometry);

    /**
     * 新增验光配镜
     *
     * @param optometry 验光配镜
     * @return 结果
     */
    public AjaxResult insertOptometry(Optometry optometry);

    /**
     * 修改验光配镜
     *
     * @param optometry 验光配镜
     * @return 结果
     */
    public AjaxResult updateOptometry(Optometry optometry);

    /**
     * 批量删除验光配镜
     *
     * @param ids 需要删除的验光配镜主键集合
     * @return 结果
     */
    public int deleteOptometryByIds(Long[] ids);

    /**
     * 更新状态
     *
     * @param id 验光配镜主键
     * @return 结果
     */
    public int updateOptometryById(Integer status,Long id);
    /**
     * 配镜处方
     * @param id
     * @return
     */
    public OptometryInfoVo selectProcessOptometryById(Long id);
}
