package com.xz.optometry.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

import java.util.Date;
import java.util.List;

/**
 * 验光配镜对象 t_optometry
 * 
 * @author xz
 * @date 2024-01-31
 */
@Data
@TableName("t_optometry")
public class Optometry extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 客户id */
    @NotNull(message = "客户不能为空")
    @Excel(name = "客户id")
    private Long patientId;
    /** 状态（1.未开单 2.已开单 3.已作废） */
    private Integer status;

    /** 客户信息 */
    @NotBlank(message = "客户不能为空")
    @Excel(name = "客户信息")
    private String patientName;

    /** 验光类型（字典维护） */
    @NotNull(message = "验光类型不能为空")
    @Excel(name = "验光类型", readConverterExp = "字典维护")
    private Integer optometryType;

    /** 验光师id */
    @NotNull(message = "验光师不能为空")
    @Excel(name = "验光师id")
    private Long optometristId;

    /** 验光师 */
    @NotBlank(message = "验光师不能为空")
    @Excel(name = "验光师")
    private String optometrist;

    /** 医生id */
    @NotNull(message = "医生不能为空")
    @Excel(name = "医生id")
    private Long doctorId;

    /** 医生 */
    @NotBlank(message = "医生不能为空")
    @Excel(name = "医生")
    private String doctor;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 部门id */
    @NotNull(message = "验光门店不能为空")
    @Excel(name = "部门id")
    private Long deptId;

    /** 视远 */
    @Excel(name = "视远")
    private String visualDistance;

    /** 阅读 */
    @Excel(name = "阅读")
    private String optometryRead;

    /** 瞳距左 */
    @Excel(name = "瞳距左")
    private String pupilDistanceLeft;

    /** 瞳距有 */
    @Excel(name = "瞳距有")
    private String pupilDistanceRight;

    /** 瞳高左 */
    @Excel(name = "瞳高左")
    private String pupillaryHeightLeft;

    /** 瞳高右 */
    @Excel(name = "瞳高右")
    private String pupillaryHeightRight;

    /** ADD */
    @Excel(name = "ADD")
    private String optometryAdd;
    /**验光单号*/
    private String optometryNo;
    /**处方类型 1.框架处方*/
    private Integer prescriptionType;

    /** 配镜处方备注 */
    @Excel(name = "配镜处方备注")
    private String prescriptionRemark;
    /*处方信息-视远*/
    private String prescriptionDistance;
    /*处方信息-阅读*/
    private String prescriptionRead;
    /*配镜明细*/
    @TableField(exist = false)
    private List<OptometryItem> optometryItemList;
    /*附件*/
    @TableField(exist = false)
    private List<String> urlList;
    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
    /**
     * 状态
     */
    @TableField(exist = false)
    public List<Integer> statusList;
    /** 手机号 */
    @Excel(name = "手机号")
    private String phoneNumber;

    /** 出生日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;
}
