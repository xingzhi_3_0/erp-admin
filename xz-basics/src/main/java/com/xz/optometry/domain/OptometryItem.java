package com.xz.optometry.domain;

import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 验光配镜明细对象 t_optometry_item
 * 
 * @author xz
 * @date 2024-01-31
 */
@Data
@TableName("t_optometry_item")
public class OptometryItem{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 验光配镜id */
    @Excel(name = "验光配镜id")
    private Long optometryId;

    /** 类型（1.验光结果 2.配镜处方） */
    @Excel(name = "类型", readConverterExp = "1=.验光结果,2=.配镜处方")
    private Integer optometryType;

    /** 用途（1.视远 2.阅读） */
    @Excel(name = "用途", readConverterExp = "1=.视远,2=.阅读")
    private Integer optometryUse;

    /** 眼别（1.右OD 2.左OS） */
    @Excel(name = "眼别", readConverterExp = "1=.右OD,2=.左OS")
    private Integer eyeCategory;

    /** 球镜 */
    @Excel(name = "球镜")
    private String sphericalLens;

    /** 柱镜 */
    @Excel(name = "柱镜")
    private String nylindricalMirror;

    /** 轴向 */
    @Excel(name = "轴向")
    private String nxialDirection;

    /** 三棱镜 */
    @Excel(name = "三棱镜")
    private String triplePrism;

    /** 底向 */
    @Excel(name = "底向")
    private String basad;

    /** 裸眼--视力 */
    @Excel(name = "裸眼--视力")
    private String nakedEye;

    /** 散瞳--视力 */
    @Excel(name = "散瞳--视力")
    private String mydriasis;

    /** 矫正--视力 */
    @Excel(name = "矫正--视力")
    private String correct;

}
