package com.xz.optometry.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @ClassName OptometryInfoVo * @Description TODO
 * @Author Administrator
 * @Date 9:56 2024/1/31
 * @Version 1.0 验光详情
 **/
@Data
public class OptometryInfoVo extends OptometryVo{
    /** 备注 */
    private String remark;
    /** 手机号 */
    private String phoneNumber;

    /** 出生日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;
    /*附件*/
    private List<String> urlList;

    /** 验光师id */
    @Excel(name = "验光师id")
    private Long optometristId;
    /** 医生id */
    @Excel(name = "医生id")
    private Long doctorId;
    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;
    /** 配镜处方备注 */
    @Excel(name = "配镜处方备注")
    private String prescriptionRemark;

    /*配镜明细*/
    private List<OptometryItemVo> optometryItemList;

}
