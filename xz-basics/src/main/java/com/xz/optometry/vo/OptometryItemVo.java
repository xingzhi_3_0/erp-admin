package com.xz.optometry.vo;

import lombok.Data;

/**
 * @ClassName OptometryItemVo * @Description TODO
 * @Author Administrator
 * @Date 10:01 2024/1/31
 * @Version 1.0  配镜明细
 **/
@Data
public class OptometryItemVo {
    /** 类型（1.验光结果 2.配镜处方） */
    private Integer optometryType;

    /** 用途（1.视远 2.阅读） */
    private Integer optometryUse;

    /** 眼别（1.右OD 2.左OS） */
    private Integer eyeCategory;

    /** 球镜 */
    private String sphericalLens;

    /** 柱镜 */
    private String nylindricalMirror;

    /** 轴向 */
    private String nxialDirection;

    /** 三棱镜 */
    private String triplePrism;

    /** 底向 */
    private String basad;

    /** 裸眼--视力 */
    private String nakedEye;

    /** 散瞳--视力 */
    private String mydriasis;

    /** 矫正--视力 */
    private String correct;
}
