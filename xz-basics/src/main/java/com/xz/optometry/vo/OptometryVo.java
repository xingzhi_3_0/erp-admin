package com.xz.optometry.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName OptometryListVo * @Description TODO
 * @Author Administrator
 * @Date 9:32 2024/1/31
 * @Version 1.0  验光信息
 **/
@Data
public class OptometryVo {
    /** id */
    private Long id;
    /** 客户id */
    @Excel(name = "客户id")
    private Long patientId;
    /** 客户信息 */
    @Excel(name = "客户信息")
    private String patientName;
    /**验光单号*/
    private String optometryNo;
    /**处方类型 1.框架处方*/
    private Integer prescriptionType;
    private Integer optometryType;
    /*状态（1.未开单 2.已开单 3.已作废）*/
    private Integer status;

    /** 制单人 */
    private String userName;
    /** 制单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 验光门店 */
    private String deptName;
    /** 验光师 */
    @Excel(name = "验光师")
    private String optometrist;

    /** 医生 */
    @Excel(name = "医生")
    private String doctor;
    /** 视远 */
    @Excel(name = "视远")
    private String visualDistance;

    /** 阅读 */
    @Excel(name = "阅读")
    private String optometryRead;

    /** 瞳距左 */
    @Excel(name = "瞳距左")
    private String pupilDistanceLeft;

    /** 瞳距右 */
    @Excel(name = "瞳距右")
    private String pupilDistanceRight;

    /** 瞳高左 */
    @Excel(name = "瞳高左")
    private String pupillaryHeightLeft;

    /** 瞳高右 */
    @Excel(name = "瞳高右")
    private String pupillaryHeightRight;

    /** ADD */
    @Excel(name = "ADD")
    private String optometryAdd;

    /*处方信息-视远*/
    private String prescriptionDistance;
    /*处方信息-阅读*/
    private String prescriptionRead;

}
