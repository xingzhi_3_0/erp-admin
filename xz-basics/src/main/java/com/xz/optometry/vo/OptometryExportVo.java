package com.xz.optometry.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.utils.StringUtils;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName OptometryListVo * @Description TODO
 * @Author Administrator
 * @Date 9:32 2024/1/31
 * @Version 1.0  验光信息
 **/
@Data
public class OptometryExportVo {

    @Excel(name = "验光单号")
    private String optometryNo;
    @Excel(name = "客户信息")
    private String patientName;
    @Excel(name = "处方类型", readConverterExp = "1=眼阿托品散瞳检影验光,2=眼短效散瞳检影验光,3=眼小瞳验光 远视 近视,4=复光 远视 近视")
    private Integer prescriptionType;
    @Excel(name = "处方信息")
    private String prescriptionInfo;
    /*处方信息-视远*/
    private String prescriptionDistance;
    /*处方信息-阅读*/
    private String prescriptionRead;
    @Excel(name = "制单时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createTime;
    @Excel(name = "制单人")
    private String userName;
    @Excel(name = "验光师")
    private String optometrist;
    @Excel(name = "验光门店")
    private String deptName;
    @Excel(name = "医生")
    private String doctor;
    @Excel(name = "状态", readConverterExp = "1=未开单,2=已开单,3=已作废")
    private Integer status;


  public String initPrescriptionInfo() {
    StringBuilder sb = new StringBuilder();
    if(!StringUtils.isEmpty(prescriptionDistance)){
      sb.append("视远");
      sb.append(prescriptionDistance);
    }
    if(!StringUtils.isEmpty(prescriptionRead)){
      if(sb.length() > 0){
        sb.append("\t\n");
      }
      sb.append("阅读");
      sb.append(prescriptionRead);
    }
    return sb.toString();
  }
}
