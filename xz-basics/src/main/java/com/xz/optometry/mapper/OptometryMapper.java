package com.xz.optometry.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.optometry.vo.OptometryExportVo;
import com.xz.optometry.vo.OptometryInfoVo;
import com.xz.optometry.vo.OptometryVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.optometry.domain.Optometry;
import org.apache.ibatis.annotations.Param;

/**
 * 验光配镜Mapper接口
 *
 * @author xz
 * @date 2024-01-31
 */
@Mapper
public interface OptometryMapper  extends BaseMapper<Optometry> {
    /**
     * 查询验光配镜
     *
     * @param id 验光配镜主键
     * @return 验光配镜
     */
    public OptometryInfoVo selectOptometryById(Long id);

    /**
     * 配镜处方
     * @param id
     * @return
     */
    public OptometryInfoVo selectProcessOptometryById(Long id);

    /**
     * 查询验光配镜列表
     *
     * @param optometry 验光配镜
     * @return 验光配镜集合
     */
    public List<OptometryVo> selectOptometryList(Optometry optometry);
    /**
     * 查询验光配镜列表(导出)
     *
     * @param optometry 验光配镜
     * @return 验光配镜集合
     */
    public List<OptometryExportVo> selectOptometryListForExport(Optometry optometry);

    public List<OptometryVo> selectByPatientId(Optometry optometry);

    /**
     * 新增验光配镜
     *
     * @param optometry 验光配镜
     * @return 结果
     */
    public int insertOptometry(Optometry optometry);

    /**
     * 修改验光配镜
     *
     * @param optometry 验光配镜
     * @return 结果
     */
    public int updateOptometry(Optometry optometry);

    /**
     * 删除验光配镜
     *
     * @param id 验光配镜主键
     * @return 结果
     */
    public int updateOptometryById(@Param("status") Integer status,@Param("id") Long id);

    /**
     * 批量删除验光配镜
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOptometryByIds(Long[] ids);
}
