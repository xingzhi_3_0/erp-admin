package com.xz.supplier.service.impl;

import java.util.List;
import java.util.Objects;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.domain.SysAccessory;
import com.xz.common.service.ISysAccessoryService;
import com.xz.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.utils.SecurityUtils;
import com.xz.product.domain.ProductAttribute;
import com.xz.supplier.dto.SupplierAccessoryDto;
import com.xz.supplier.dto.SupplierDto;
import com.xz.supplier.dto.SupplierListDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.supplier.mapper.SupplierMapper;
import com.xz.supplier.domain.Supplier;
import com.xz.supplier.service.ISupplierService;
import org.springframework.util.CollectionUtils;

/**
 * 供应商、企业客户Service业务层处理
 *
 * @author xz
 * @date 2024-01-04
 */
@Service
public class SupplierServiceImpl extends ServiceImpl<SupplierMapper, Supplier> implements ISupplierService {
    @Autowired
    private SupplierMapper supplierMapper;
    @Autowired
    private ISysAccessoryService iSysAccessoryService;

    /**
     * 查询供应商、企业客户
     *
     * @param id 供应商、企业客户主键
     * @return 供应商、企业客户
     */
    @Override
    public SupplierDto selectSupplierById(Long id) {
        SupplierDto supplierDto = supplierMapper.selectSupplierById(id);
        if(Objects.nonNull(supplierDto)){
            List<SupplierAccessoryDto> accessoryDtoList = iSysAccessoryService.selectSupplierAccessoryList(id.toString(), 1);
            supplierDto.setUrlList(accessoryDtoList);
        }
        return supplierDto;
    }

    /**
     * 查询供应商、企业客户列表
     *
     * @param supplier 供应商、企业客户
     * @return 供应商、企业客户
     */
    @Override
    public List<SupplierListDto> selectSupplierList(Supplier supplier) {
        return supplierMapper.selectSupplierList(supplier);
    }

    /**
     * 新增供应商、企业客户
     *
     * @param supplier 供应商、企业客户
     * @return 结果
     */
    @Override
    public AjaxResult insertSupplier(Supplier supplier) {
        //校验是否已经添加
        int count = this.count(new QueryWrapper<Supplier>().eq("unit_name", supplier.getUnitName()).eq("del_flag", 0));
        if(count>0){
            return AjaxResult.error("数据【"+supplier.getUnitName()+"】已存在");
        }
        supplier.setCreateTime(DateUtils.getNowDate());
        supplier.setCreateBy(SecurityUtils.getUserId());
        supplier.setTenantId(SecurityUtils.getLoginUser().getTenantId());
        int insertsupplier = supplierMapper.insertSupplier(supplier);
        if(insertsupplier>0){
            if(!CollectionUtils.isEmpty(supplier.getUrlList())){
                List<Object> resultList = JSON.parseArray(JSON.toJSONString(supplier.getUrlList()), Object.class);
                iSysAccessoryService.insertUrlList(resultList,supplier.getId().toString(),1,1);
            }
        }
        return insertsupplier>0? AjaxResult.success("新增成功"):AjaxResult.error("新增失败");
    }

    /**
     * 修改供应商、企业客户
     *
     * @param supplier 供应商、企业客户
     * @return 结果
     */
    @Override
    public AjaxResult updateSupplier(Supplier supplier) {
        if(supplier.getId()==null){
            return AjaxResult.error("参数id不能为空");
        }
        //校验是否已经添加
        int count = this.count(new QueryWrapper<Supplier>().eq("unit_name", supplier.getUnitName()).eq("del_flag", 0).ne("id",supplier.getId()));
        if(count>0){
            return AjaxResult.error("数据【"+supplier.getUnitName()+"】已存在");
        }
        supplier.setUpdateTime(DateUtils.getNowDate());
        supplier.setUpdateBy(SecurityUtils.getUserId());
        int updateSupplier = supplierMapper.updateSupplier(supplier);
        if(updateSupplier>0){
            //删除历史附件
            iSysAccessoryService.deleteSysAccessoryByBizId(supplier.getId().toString(),1);
            if(!CollectionUtils.isEmpty(supplier.getUrlList())){
                List<Object> resultList = JSON.parseArray(JSON.toJSONString(supplier.getUrlList()), Object.class);
                iSysAccessoryService.insertUrlList(resultList,supplier.getId().toString(),1,1);
            }
        }
        return updateSupplier>0? AjaxResult.success("修改成功"):AjaxResult.error("修改失败");
    }


    /**
     * 批量删除供应商、企业客户
     *
     * @param ids 需要删除的供应商、企业客户主键
     * @return 结果
     */
    @Override
    public int deleteSupplierByIds(Long[] ids) {
        return supplierMapper.deleteSupplierByIds(ids);
    }

    /**
     * 删除供应商、企业客户信息
     *
     * @param id 供应商、企业客户主键
     * @return 结果
     */
    @Override
    public int deleteSupplierById(Long id) {
        return supplierMapper.deleteSupplierById(id);
    }

    @Override
    public Supplier getSupplierInfo(String unitName) {
        return baseMapper.getSupplierInfo(unitName);
    }
}
