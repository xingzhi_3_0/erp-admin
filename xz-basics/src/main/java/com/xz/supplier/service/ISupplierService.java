package com.xz.supplier.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.supplier.domain.Supplier;
import com.xz.supplier.dto.SupplierDto;
import com.xz.supplier.dto.SupplierListDto;

/**
 * 供应商、企业客户Service接口
 * 
 * @author xz
 * @date 2024-01-04
 */
public interface ISupplierService extends IService<Supplier> {
    /**
     * 查询供应商、企业客户
     * 
     * @param id 供应商、企业客户主键
     * @return 供应商、企业客户
     */
    public SupplierDto selectSupplierById(Long id);

    /**
     * 查询供应商、企业客户列表
     * 
     * @param supplier 供应商、企业客户
     * @return 供应商、企业客户集合
     */
    public List<SupplierListDto> selectSupplierList(Supplier supplier);

    /**
     * 新增供应商、企业客户
     * 
     * @param supplier 供应商、企业客户
     * @return 结果
     */
    public AjaxResult insertSupplier(Supplier supplier);

    /**
     * 修改供应商、企业客户
     * 
     * @param supplier 供应商、企业客户
     * @return 结果
     */
    public AjaxResult updateSupplier(Supplier supplier);

    /**
     * 批量删除供应商、企业客户
     * 
     * @param ids 需要删除的供应商、企业客户主键集合
     * @return 结果
     */
    public int deleteSupplierByIds(Long[] ids);

    /**
     * 删除供应商、企业客户信息
     * 
     * @param id 供应商、企业客户主键
     * @return 结果
     */
    public int deleteSupplierById(Long id);
    /**
     * 通过供应商名称获取信息
     * @param unitName
     * @return
     */
    Supplier getSupplierInfo(String unitName);
}
