package com.xz.supplier.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.supplier.dto.SupplierDto;
import com.xz.supplier.dto.SupplierListDto;
import org.apache.ibatis.annotations.Mapper;
import com.xz.supplier.domain.Supplier;

/**
 * 供应商、企业客户Mapper接口
 * 
 * @author xz
 * @date 2024-01-04
 */
@Mapper
public interface SupplierMapper  extends BaseMapper<Supplier> {
    /**
     * 查询供应商、企业客户
     * 
     * @param id 供应商、企业客户主键
     * @return 供应商、企业客户
     */
    public SupplierDto selectSupplierById(Long id);

    /**
     * 查询供应商、企业客户列表
     * 
     * @param supplier 供应商、企业客户
     * @return 供应商、企业客户集合
     */
    public List<SupplierListDto> selectSupplierList(Supplier supplier);

    /**
     * 新增供应商、企业客户
     * 
     * @param supplier 供应商、企业客户
     * @return 结果
     */
    public int insertSupplier(Supplier supplier);

    /**
     * 修改供应商、企业客户
     * 
     * @param supplier 供应商、企业客户
     * @return 结果
     */
    public int updateSupplier(Supplier supplier);

    /**
     * 删除供应商、企业客户
     * 
     * @param id 供应商、企业客户主键
     * @return 结果
     */
    public int deleteSupplierById(Long id);

    /**
     * 批量删除供应商、企业客户
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSupplierByIds(Long[] ids);

    /**
     * 通过供应商名称获取信息
     * @param unitName
     * @return
     */
    Supplier getSupplierInfo(String unitName);
}
