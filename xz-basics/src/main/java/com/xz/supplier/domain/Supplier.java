package com.xz.supplier.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.xz.supplier.dto.SupplierAccessoryDto;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 供应商、企业客户对象 t_supplier
 * 
 * @author xz
 * @date 2024-01-04
 */
@Data
@TableName("t_supplier")
public class Supplier extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 单位名称 */
    @NotBlank(message = "单位名称不能为空")
    @Excel(name = "单位名称")
    private String unitName;

    /** 统一社会代码 */
    @Excel(name = "统一社会代码")
    private String unifiedCode;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 联系人 */
    @Excel(name = "联系人")
    private String contactPerson;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String contactInformation;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 单位性质（1.供应商 2.企业客户） */
    //@NotNull(message = "单位性质不能为空")
    //@Excel(name = "单位性质", readConverterExp = "1=供应商;2=企业客户")
    //private Integer unitProperty;

    @Excel(name = "是否供应商", readConverterExp = "1=是;2=否")
    private Integer unitProperty;

    @Excel(name = "是否企业客户", readConverterExp = "1=是;2=否")
    private Integer isEnterprise;

    /** 银行账户名 */
    @Excel(name = "银行账户名")
    private String bankName;

    /** 银行账号 */
    @Excel(name = "银行账号")
    private String bankAccount;

    /** 开户银行 */
    @Excel(name = "开户银行")
    private String bankDeposit;

    /** 状态（0正常 1停用） */
    @NotNull(message = "状态不能为空")
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;
    /**附件*/
    @TableField(exist = false)
    private List<SupplierAccessoryDto> urlList;

    @TableField(exist = false)
    private List<Integer> statusList;

}
