package com.xz.supplier.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.List;

/**
 * @ClassName SupplierDto * @Description TODO
 * @Author Administrator
 * @Date 16:03 2024/1/4
 * @Version 1.0
 **/
@Data
public class SupplierDto {

    /** id */
    private Long id;

    /** 单位名称 */
    @Excel(name = "单位名称")
    private String unitName;

    /** 统一社会代码 */
    @Excel(name = "统一社会代码")
    private String unifiedCode;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 联系人 */
    @Excel(name = "联系人")
    private String contactPerson;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String contactInformation;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 单位性质（1.供应商 2.企业客户） */
    //@Excel(name = "单位性质", readConverterExp = "1=.供应商,2=.企业客户")
    //private Integer unitProperty;

    @Excel(name = "是否供应商", readConverterExp = "1=是;2=否")
    private Integer unitProperty;

    @Excel(name = "是否企业客户", readConverterExp = "1=是;2=否")
    private Integer isEnterprise;

    /** 银行账户名 */
    @Excel(name = "银行账户名")
    private String bankName;

    /** 银行账号 */
    @Excel(name = "银行账号")
    private String bankAccount;

    /** 开户银行 */
    @Excel(name = "开户银行")
    private String bankDeposit;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /**附件*/
    @TableField(exist = false)
    private List<SupplierAccessoryDto> urlList;
}
