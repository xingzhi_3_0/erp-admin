package com.xz.supplier.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName SupplierAccessoryDtoDto * @Description TODO
 * @Author Administrator
 * @Date 16:06 2024/1/4
 * @Version 1.0
 **/
@Data
public class SupplierAccessoryDto {
    /** 附件名称 */
    @Excel(name = "附件名称")
    private String fileName;

    /** 文件url */
    @Excel(name = "文件url")
    private String url;
    /**起始时间*/
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /**有效期*/
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date validity;
}
