package com.xz.supplier.dto;

import com.xz.common.annotation.Excel;
import lombok.Data;

/**
 * @ClassName SupplierListDto * @Description TODO
 * @Author Administrator
 * @Date 16:22 2024/1/4
 * @Version 1.0
 **/
@Data
public class SupplierListDto {
    /** id */
    private Long id;

    /** 单位名称 */
    @Excel(name = "单位名称")
    private String unitName;
    /** 单位性质（1.供应商 2.企业客户） */
    //@Excel(name = "单位性质", readConverterExp = "1=.供应商,2=.企业客户")
    //private Integer unitProperty;

    @Excel(name = "是否供应商", readConverterExp = "0=否;1=是")
    private Integer unitProperty;

    @Excel(name = "是否企业客户", readConverterExp = "0=否;1=是")
    private Integer isEnterprise;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 统一社会代码 */
    @Excel(name = "统一社会代码")
    private String unifiedCode;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 联系人 */
    @Excel(name = "联系人")
    private String contactPerson;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String contactInformation;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;
}
