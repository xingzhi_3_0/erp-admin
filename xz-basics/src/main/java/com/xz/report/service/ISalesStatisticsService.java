package com.xz.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.report.param.SalesReportParam;
import com.xz.report.vo.SalesFlowReportVo;
import com.xz.report.vo.SalesReportVo;
import com.xz.report.vo.SalesReturnReportVo;

import java.util.List;

/**
 * 销售单换货Service接口
 * 
 * @author xz
 * @date 2024-2-19 11:13:40
 */
public interface ISalesStatisticsService extends IService {

    List<SalesReportVo> salesReport(SalesReportParam salesReportParam);

    List<SalesReturnReportVo> salesReturnReport(SalesReportParam salesReportParam);

    List<SalesFlowReportVo> salesFlowReport(SalesReportParam salesReportParam);

}
