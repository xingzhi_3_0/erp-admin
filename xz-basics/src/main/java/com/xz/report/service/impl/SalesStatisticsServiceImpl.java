package com.xz.report.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.patient.domain.PatientInfo;
import com.xz.patient.mapper.PatientInfoMapper;
import com.xz.product.domain.ProductMeasuringUnit;
import com.xz.product.dto.ProductMeasuringUnitDto;
import com.xz.product.service.IProductMeasuringUnitService;
import com.xz.report.param.SalesReportParam;
import com.xz.sales.mapper.SalesOrderMapper;
import com.xz.report.service.ISalesStatisticsService;
import com.xz.report.vo.SalesFlowReportVo;
import com.xz.report.vo.SalesReportVo;
import com.xz.report.vo.SalesReturnReportVo;
import com.xz.sales.mapper.SalesReturnMapper;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;

/**
 * 销售单换货Service业务层处理
 *
 * @author xz
 * @date 2024-2-19 11:15:53
 */
@Service
public class SalesStatisticsServiceImpl implements ISalesStatisticsService
{

    @Resource
    private SalesOrderMapper salesOrderMapper;
    @Resource
    private SalesReturnMapper salesReturnMapper;
    @Resource
    private PatientInfoMapper patientInfoMapper;
    @Resource
    private IProductMeasuringUnitService productMeasuringUnitService;

    @Override
    public List<SalesReportVo> salesReport(SalesReportParam salesReportParam){
        List<SalesReportVo> salesReportVos = salesOrderMapper.salesReport(salesReportParam);
        if(!CollectionUtils.isEmpty(salesReportVos)){
            SalesReportVo salesReportVo = salesOrderMapper.sumSalesReportData(salesReportParam);
            BigDecimal salesNumTotal = salesReportVo.getSalesNumTotal();
            BigDecimal netReceiptsTotal = salesReportVo.getNetReceiptsTotal();
            salesReportVos.stream().forEach(o->{
                BigDecimal sellingNum = o.getSellingNum();
                BigDecimal salesNumRatio = sellingNum.divide(salesNumTotal, 4, BigDecimal.ROUND_HALF_UP).multiply(BigDecimal.valueOf(100));
                BigDecimal netReceipts = o.getNetReceipts();
                BigDecimal netReceiptsRatio = netReceipts.divide(netReceiptsTotal, 4, BigDecimal.ROUND_HALF_UP).multiply(BigDecimal.valueOf(100));
                o.setSalesNumRatio(salesNumRatio);
                o.setNetReceiptsRatio(netReceiptsRatio);
            });
        }

        return salesReportVos;
    }

    @Override
    public List<SalesReturnReportVo> salesReturnReport(SalesReportParam salesReportParam){
        List<SalesReturnReportVo> salesReturnReportVos = salesReturnMapper.salesReturnReport(salesReportParam);
        if(!CollectionUtils.isEmpty(salesReturnReportVos)){
            SalesReturnReportVo salesReturnReportVo = salesReturnMapper.sumSalesReturnReportData(salesReportParam);
            BigDecimal returnAmountTotal = salesReturnReportVo.getReturnAmountTotal();
            BigDecimal returnNumTotal = salesReturnReportVo.getReturnNumTotal();
            salesReturnReportVos.stream().forEach(o->{
                BigDecimal returnNum = o.getReturnNum();
                BigDecimal returnNumRatio = returnNum.divide(returnNumTotal, 4, BigDecimal.ROUND_HALF_UP).multiply(BigDecimal.valueOf(100));
                BigDecimal returnAmount = o.getReturnAmount();
                BigDecimal returnAmountRatio = returnAmount.divide(returnAmountTotal, 4, BigDecimal.ROUND_HALF_UP).multiply(BigDecimal.valueOf(100));
                o.setReturnNumRatio(returnNumRatio);
                o.setReturnAmountRatio(returnAmountRatio);
            });
        }

        return salesReturnReportVos;
    }

    @Override
    public List<SalesFlowReportVo> salesFlowReport(SalesReportParam salesReportParam){
        List<SalesFlowReportVo> salesFlowReportVos = salesOrderMapper.salesFlowReport(salesReportParam);
        if(!CollectionUtils.isEmpty(salesFlowReportVos)){
            // 查询所有患者
            PatientInfo queryPatientInfo = new PatientInfo();
            List<PatientInfo> patientInfoList = patientInfoMapper.selectPatientInfoList(queryPatientInfo);
            Map<Long,String> patientMap = new HashMap<>();
            for (PatientInfo patientInfo:patientInfoList) {
                patientMap.put(patientInfo.getId(),patientInfo.getPatientName()+"/"+patientInfo.getSexStr()+"/"+patientInfo.getAge()+"岁");
            }
            // 给流水数据写这些统计的值
            salesFlowReportVos.stream().forEach(o->{
                o.setPatientInfo(patientMap.get(o.getPatientId()));
            });


            // 获取计量单位，然后填充计量单位字段
            ProductMeasuringUnit productMeasuringUnit = new ProductMeasuringUnit();
            productMeasuringUnit.setCategory(1);
            List<ProductMeasuringUnitDto>  units = productMeasuringUnitService.selectProductMeasuringUnitList(productMeasuringUnit);
            salesFlowReportVos.forEach(report->{
                Long unitId = report.getUnitId();
                if(null != unitId){
                   units.stream().filter(unit -> unitId.equals(unit.getId())).findFirst().ifPresent(unit->{
                       report.setUnitName(unit.getName());
                   });
                }
            });


            return salesFlowReportVos;
        }

        return new ArrayList<>();
    }

    @Override
    public boolean saveBatch(Collection entityList, int batchSize) {
        return false;
    }

    @Override
    public boolean saveOrUpdateBatch(Collection entityList, int batchSize) {
        return false;
    }

    @Override
    public boolean updateBatchById(Collection entityList, int batchSize) {
        return false;
    }

    @Override
    public boolean saveOrUpdate(Object entity) {
        return false;
    }

    @Override
    public Object getOne(Wrapper queryWrapper, boolean throwEx) {
        return null;
    }

    @Override
    public Map<String, Object> getMap(Wrapper queryWrapper) {
        return null;
    }

    @Override
    public BaseMapper getBaseMapper() {
        return null;
    }

    @Override
    public Class getEntityClass() {
        return null;
    }

    @Override
    public Object getObj(Wrapper queryWrapper, Function mapper) {
        return null;
    }
}
