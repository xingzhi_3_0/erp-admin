package com.xz.report.param;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import com.xz.staging.vo.FrameLensesStagingVo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 销售报表统计类
 */
@Data
public class SalesReportParam extends BaseEntity {

    private String productName;

    /** 1-本周 2-本月 3-近12月 */
    private Integer queryType;

    /** 1-销售 2-诊疗 3-充值 */
    private Integer bizType;

    /** 1-1000以下 2-1000~2000 3-2000~3000 4-3000~4000 5-4000~5000 6-5000以上 */
    private Integer rangeType;

    private String queryMonth;

    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;
    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

    private Long categoryId;
    private Long patientId;
    private String patientName;

    private Long deptId;

    private String queryDeptIdListStr;

    private String categoryIdListStr;

    List<FrameLensesStagingVo> frameLensesList;

    List<Long> idList;

    /** 仓库id */
    private Long warehouseId;

    /** 仓库 */
    private String warehouseName;
}
