package com.xz.report.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 销售报表统计类
 */
@Data
public class SalesReportVo extends BaseEntity {

    @Excel(name = "分类")
    private String productCategory;
    @Excel(name = "商品")
    private String productName;
    @Excel(name = "售价")
    private BigDecimal sellingPrice;
    @Excel(name = "数量")
    private BigDecimal sellingNum;

    private BigDecimal salesNumTotal;
    @Excel(name = "销售数量占比")
    private BigDecimal salesNumRatio;
    @Excel(name = "应收")
    private BigDecimal receivable;
    @Excel(name = "优惠")
    private BigDecimal preferential;
    @Excel(name = "实收")
    private BigDecimal netReceipts;
    private BigDecimal netReceiptsTotal;
    @Excel(name = "实收占比")
    private BigDecimal netReceiptsRatio;

    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;
    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

    private Long categoryId;

    private Long deptId;

    private String queryDeptIdListStr;
}