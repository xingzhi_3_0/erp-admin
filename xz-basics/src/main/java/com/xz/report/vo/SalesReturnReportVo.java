package com.xz.report.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 销售报表统计类
 */
@Data
public class SalesReturnReportVo extends BaseEntity {

    @Excel(name="分类")
    private String productCategory;
    @Excel(name="商品")
    private String productName;
    @Excel(name="售价")
    private BigDecimal sellingPrice;
    @Excel(name="数量")
    private BigDecimal returnNum;

    private BigDecimal returnNumTotal;
    @Excel(name="退回数量占比")
    private BigDecimal returnNumRatio;
    @Excel(name="退货金额")
    private BigDecimal returnAmount;

    private BigDecimal returnAmountTotal;
    @Excel(name="退货金额占比")
    private BigDecimal returnAmountRatio;


    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;
    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

    private Long categoryId;

    private Long deptId;

    private String queryDeptIdListStr;
}