package com.xz.report.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 销售流水统计类
 */
@Data
public class SalesFlowReportVo extends BaseEntity {

    @Excel(name = "销售日期", dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date salesDate;

    @Excel(name = "销售门店")
    private String salesDeptName;

    @Excel(name = "单据类型", readConverterExp = "1=销售单,2=销售退款,3=销售退货退款,4=销售换货(入),5=销售换货(出),-1=挂号单,-2=挂号退费,-3=检查单,-4=检查退费")
    private Integer sourceType;
    @Excel(name = "患者")
    private String patientInfo;

    @Excel(name = "商品类别")
    private String productCategory;
    /** 编码 */
    @Excel(name = "商品编码")
    private String encoded;
    @Excel(name = "商品名称")
    private String productName;
    /** 计量单位ID */
    private Long unitId;
    /** 计量单位ID */
    @Excel(name = "计量单位")
    private String unitName;
    @Excel(name = "参数")
    private String productParam;
    @Excel(name = "仓库")
    private String warehouseName;
    @Excel(name = "数量")
    private Integer sellingNum;
    @Excel(name = "售价")
    private BigDecimal sellingPrice;
    @Excel(name = "备注")
    private String remark;
    @Excel(name = "应收(元)")
    private BigDecimal receivable;
    @Excel(name = "优惠(元)")
    private BigDecimal discountAmount;
    @Excel(name = "实收(元)")
    private BigDecimal subtotal;
    @Excel(name = "交付日期", dateFormat = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date deliveryTime;
    @Excel(name = "销售员")
    private String salesUser;
    @Excel(name = "关联销售单号")
    private String associatedNo;
    @Excel(name = "业务单号")
    private String bizNo;

    private Long patientId;

    @Excel(name = "单据备注")
    private String documentRemark;

    @Excel(name = "钱包")
    private BigDecimal detailWalletPay;
    @Excel(name = "现金")
    private BigDecimal detailDishAmount;
    @Excel(name = "银行卡")
    private BigDecimal detailBankAmount;
    @Excel(name = "E支付")
    private BigDecimal detailEPayAmount;

    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;
    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

    private Long categoryId;

    private Long deptId;

    private String queryDeptIdListStr;
}
