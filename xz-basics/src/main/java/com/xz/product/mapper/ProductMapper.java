package com.xz.product.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.product.domain.ProductAttributeParam;
import com.xz.product.dto.ProductDto;
import com.xz.product.dto.ProductExportDto;
import com.xz.product.dto.ProductInfoDto;
import com.xz.product.vo.ProductCategoryVo;
import com.xz.product.vo.ProductRelatedInfoVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.product.domain.Product;

/**
 * 商品Mapper接口
 *
 * @author xz
 * @date 2024-01-02
 */
@Mapper
public interface ProductMapper  extends BaseMapper<Product> {
    /**
     * 查询商品
     *
     * @param id 商品主键
     * @return 商品
     */
    public ProductInfoDto selectProductById(Long id);

    /**
     * 查询商品列表
     *
     * @param product 商品
     * @return 商品集合
     */
    public List<ProductDto> selectProductList(Product product);
    public List<ProductExportDto> selectProductExport(Product product);

    /**
     * 新增商品
     *
     * @param product 商品
     * @return 结果
     */
    public int insertProduct(Product product);

    /**
     * 修改商品
     *
     * @param product 商品
     * @return 结果
     */
    public int updateProduct(Product product);

    /**
     * 删除商品
     *
     * @param id 商品主键
     * @return 结果
     */
    public int deleteProductById(Long id);

    /**
     * 批量删除商品
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductByIds(Long[] ids);

    /**
     * 商品属性
     * @param id
     * @return
     */
    List<ProductAttributeParam> getProductAttributeParamList(Long id);

    /**
     * 通过编码获取商品信息
     * @return
     */
    Product getProductInfo(String encoded);

    /**
     * 获取商品属性
     * @param id
     * @return
     */
    ProductCategoryVo getProductCategory(Long id);

    /**
     * 获取商品关联信息
     * @param id
     * @return
     */
    ProductRelatedInfoVo getProductRelatedInfo(Long id);
}
