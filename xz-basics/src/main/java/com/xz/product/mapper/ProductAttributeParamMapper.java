package com.xz.product.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.product.domain.ProductAttributeParam;

/**
 * 商品属性参数Mapper接口
 * 
 * @author xz
 * @date 2024-01-02
 */
@Mapper
public interface ProductAttributeParamMapper  extends BaseMapper<ProductAttributeParam> {
    /**
     * 查询商品属性参数
     * 
     * @param id 商品属性参数主键
     * @return 商品属性参数
     */
    public ProductAttributeParam selectProductAttributeParamById(Long id);

    /**
     * 查询商品属性参数列表
     * 
     * @param productAttributeParam 商品属性参数
     * @return 商品属性参数集合
     */
    public List<ProductAttributeParam> selectProductAttributeParamList(ProductAttributeParam productAttributeParam);

    /**
     * 新增商品属性参数
     * 
     * @param productAttributeParam 商品属性参数
     * @return 结果
     */
    public int insertProductAttributeParam(ProductAttributeParam productAttributeParam);

    /**
     * 修改商品属性参数
     * 
     * @param productAttributeParam 商品属性参数
     * @return 结果
     */
    public int updateProductAttributeParam(ProductAttributeParam productAttributeParam);

    /**
     * 删除商品属性参数
     * 
     * @param id 商品属性参数主键
     * @return 结果
     */
    public int deleteProductAttributeParamById(Long id);
    /**
     * 根据属性id删除属性参数
     *
     * @param id 商品属性参数主键
     * @return 结果
     */
    public int deleteProductAttributeById(Long id);

    /**
     * 批量删除商品属性参数
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductAttributeParamByIds(Long[] ids);
}
