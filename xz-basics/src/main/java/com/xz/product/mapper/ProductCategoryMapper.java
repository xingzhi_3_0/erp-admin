package com.xz.product.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.product.dto.ProductCategoryDto;
import com.xz.product.dto.ProductCategoryItemDto;
import com.xz.product.dto.ProductCategoryListDto;
import org.apache.ibatis.annotations.Mapper;
import com.xz.product.domain.ProductCategory;
import org.apache.ibatis.annotations.Param;

/**
 * 商品类别Mapper接口
 * 
 * @author xz
 * @date 2024-01-02
 */
@Mapper
public interface ProductCategoryMapper  extends BaseMapper<ProductCategory> {
    /**
     * 查询商品类别
     * 
     * @param id 商品类别主键
     * @return 商品类别
     */
    public ProductCategoryDto selectProductCategoryById(Long id);

    /**
     * 查询商品类别列表
     * 
     * @param productCategory 商品类别
     * @return 商品类别集合
     */
    public List<ProductCategoryListDto> selectProductCategoryList(ProductCategory productCategory);

    /**
     * 新增商品类别
     * 
     * @param productCategory 商品类别
     * @return 结果
     */
    public int insertProductCategory(ProductCategory productCategory);

    /**
     * 修改商品类别
     * 
     * @param productCategory 商品类别
     * @return 结果
     */
    public int updateProductCategory(ProductCategory productCategory);

    /**
     * 删除商品类别
     * 
     * @param id 商品类别主键
     * @return 结果
     */
    public int deleteProductCategoryById(Long id);

    /**
     * 批量删除商品类别
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductCategoryByIds(Long[] ids);

    /**
     * 分类属性列表
     * @return
     */
    List<ProductCategoryItemDto> getProductCategoryItem(@Param("productCategoryId") Long productCategoryId,@Param("attributeCategory") Integer attributeCategory);

    /**
     * 通过名称获取id
     * @param productCategory
     * @return
     */
    Long getId(String productCategory);

    int setWarningDays(ProductCategory productCategory);

    List<ProductCategory> findIds(@Param("nameList") List<String> nameList);
}
