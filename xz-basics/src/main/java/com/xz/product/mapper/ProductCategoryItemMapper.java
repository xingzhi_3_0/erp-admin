package com.xz.product.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.product.domain.ProductCategoryItem;

/**
 * 商品类别--关联属性Mapper接口
 * 
 * @author xz
 * @date 2024-01-02
 */
@Mapper
public interface ProductCategoryItemMapper  extends BaseMapper<ProductCategoryItem> {
    /**
     * 查询商品类别--关联属性
     * 
     * @param id 商品类别--关联属性主键
     * @return 商品类别--关联属性
     */
    public ProductCategoryItem selectProductCategoryItemById(Long id);

    /**
     * 查询商品类别--关联属性列表
     * 
     * @param productCategoryItem 商品类别--关联属性
     * @return 商品类别--关联属性集合
     */
    public List<ProductCategoryItem> selectProductCategoryItemList(ProductCategoryItem productCategoryItem);

    /**
     * 新增商品类别--关联属性
     * 
     * @param productCategoryItem 商品类别--关联属性
     * @return 结果
     */
    public int insertProductCategoryItem(ProductCategoryItem productCategoryItem);

    /**
     * 修改商品类别--关联属性
     * 
     * @param productCategoryItem 商品类别--关联属性
     * @return 结果
     */
    public int updateProductCategoryItem(ProductCategoryItem productCategoryItem);

    /**
     * 删除商品类别--关联属性
     * 
     * @param id 商品类别--关联属性主键
     * @return 结果
     */
    public int deleteProductCategoryItemById(Long id);

    /**
     * 批量删除商品类别--关联属性
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductCategoryItemByIds(Long[] ids);
}
