package com.xz.product.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.product.dto.ProductMeasuringUnitDto;
import org.apache.ibatis.annotations.Mapper;
import com.xz.product.domain.ProductMeasuringUnit;
import org.apache.ibatis.annotations.Param;

/**
 * 商品基础配置Mapper接口
 * 
 * @author xz
 * @date 2024-01-02
 */
@Mapper
public interface ProductMeasuringUnitMapper  extends BaseMapper<ProductMeasuringUnit> {
    /**
     * 查询商品基础配置
     * 
     * @param id 商品基础配置主键
     * @return 商品基础配置
     */
    public ProductMeasuringUnitDto selectProductMeasuringUnitById(Long id);

    /**
     * 通过名称获取配置id
     * @param name
     * @return
     */
    Long getId(@Param("name") String name, @Param("category") Integer category);

    /**
     * 查询商品基础配置列表
     * 
     * @param productMeasuringUnit 商品基础配置
     * @return 商品基础配置集合
     */
    public List<ProductMeasuringUnitDto> selectProductMeasuringUnitList(ProductMeasuringUnit productMeasuringUnit);

    /**
     * 新增商品基础配置
     * 
     * @param productMeasuringUnit 商品基础配置
     * @return 结果
     */
    public int insertProductMeasuringUnit(ProductMeasuringUnit productMeasuringUnit);

    /**
     * 修改商品基础配置
     * 
     * @param productMeasuringUnit 商品基础配置
     * @return 结果
     */
    public int updateProductMeasuringUnit(ProductMeasuringUnit productMeasuringUnit);

    /**
     * 删除商品基础配置
     * 
     * @param id 商品基础配置主键
     * @return 结果
     */
    public int deleteProductMeasuringUnitById(Long id);

    /**
     * 批量删除商品基础配置
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductMeasuringUnitByIds(Long[] ids);
}
