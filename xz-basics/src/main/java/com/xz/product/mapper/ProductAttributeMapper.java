package com.xz.product.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.product.dto.ProductAttributeDto;
import com.xz.product.dto.ProductAttributeListDto;
import com.xz.product.dto.ProductAttributeParamDto;
import org.apache.ibatis.annotations.Mapper;
import com.xz.product.domain.ProductAttribute;

/**
 * 商品属性Mapper接口
 * 
 * @author xz
 * @date 2024-01-02
 */
@Mapper
public interface ProductAttributeMapper  extends BaseMapper<ProductAttribute> {
    /**
     * 查询商品属性
     * 
     * @param id 商品属性主键
     * @return 商品属性
     */
    public ProductAttributeParamDto selectProductAttributeById(Long id);

    /**
     * 查询商品属性列表
     * 
     * @param productAttribute 商品属性
     * @return 商品属性集合
     */
    public List<ProductAttributeDto> selectProductAttributeList(ProductAttribute productAttribute);

    /**
     * 新增商品属性
     * 
     * @param productAttribute 商品属性
     * @return 结果
     */
    public int insertProductAttribute(ProductAttribute productAttribute);

    /**
     * 修改商品属性
     * 
     * @param productAttribute 商品属性
     * @return 结果
     */
    public int updateProductAttribute(ProductAttribute productAttribute);

    /**
     * 删除商品属性
     * 
     * @param id 商品属性主键
     * @return 结果
     */
    public int deleteProductAttributeById(Long id);

    /**
     * 批量删除商品属性
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductAttributeByIds(Long[] ids);

    /**
     * 获取属性列表
     * @param attributeCategory
     * @return
     */
    List<ProductAttributeListDto> getProductAttributeList(Integer attributeCategory);
}
