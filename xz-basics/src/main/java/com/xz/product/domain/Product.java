package com.xz.product.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 商品对象 t_product
 *
 * @author xz
 * @date 2024-01-02
 */
@Data
@TableName("t_product")
public class Product extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** 商品ID */
    private Long id;
    @Excel(name = "商品名称")
    private String productName;

    /** 类别ID */
    @Excel(name = "类别ID")
    @NotNull(message = "类别ID不能为空")
    private Long categoryId;

    /** 品牌ID */
    @Excel(name = "品牌ID")
    @NotNull(message = "品牌ID不能为空")
    private Long brandId;

    /** 产品名称ID */
    @Excel(name = "产品名称ID")
    @NotNull(message = "产品名称ID不能为空")
    private Long prodNameId;

    /** 规格 */
    @Excel(name = "规格")
    @NotBlank(message = "规格不能为空")
    private String specification;

    /** 系列 */
    @Excel(name = "系列")
    private String series;

    /** 计量单位ID */
    @Excel(name = "计量单位ID")
    @NotNull(message = "计量单位ID不能为空")
    private Long unitId;

    /** 价格 */
    @Excel(name = "价格")
    @NotNull(message = "价格不能为空")
    private BigDecimal price;

    /** 状态（0.在售，1.停售，2.停用） */
    @NotBlank(message = "状态不能为空")
    @Excel(name = "状态", readConverterExp = "0=.在售，1.停售，2.停用")
    private String status;

    /** 状态 */
    @Excel(name = "状态")
    private String statusName;

    /** 生产厂家 */
    @Excel(name = "生产厂家")
    private String manufacturer;

    /** 医疗器械/非医疗器械（1.医疗器械，2.非医疗器械） */
    @NotNull(message = "医疗器械/非医疗器械不能为空")
    @Excel(name = "医疗器械/非医疗器械", readConverterExp = "1=.医疗器械，2.非医疗器械")
    private Integer isDevices;

    /** 医疗器械/非医疗器械 */
    @Excel(name = "医疗器械/非医疗器械")
    private String isDevicesName;

    /** 注册证号 */
    @Excel(name = "注册证号")
    private String registrationNo;

    /** 注册证有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "注册证有效期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date registrationValiddityDate;

    /** 注册证图片 */
    @Excel(name = "注册证图片")
    private String ossRegistrationLicense;

    /** 商品图片 */
    @Excel(name = "商品图片")
    private String ossCommodityCover;

    /** 是否诊疗物品（1.否，2.是） */
    @Excel(name = "是否诊疗物品", readConverterExp = "1=否，2=是")
    private String isDiagnosisConsumables;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;
    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;
    /** 编码 */
    @Excel(name = "编码")
    private String encoded;

    /**
     * 价格下限
     */
    @TableField(exist = false)
    private BigDecimal priceCeiling;
    /**
     * 价格上限
     */
    @TableField(exist = false)
    private BigDecimal priceFloor;
    /**
     * 状态
     */
    @TableField(exist = false)
    public List<Integer> statusList;
    /** 是否加工*/
    @TableField(exist = false)
    public Integer isMake;
    /** 是否定制*/
    @TableField(exist = false)
    private Integer isCustom;

    //贸易中间商，就是供应商模块（"单位性质"字段值为"企业客户"的）数据。
    /** 贸易中间商ID */
    @TableField(exist = false)
    private Long entSupplierId;
    /** 贸易中间商 合同ID */
    @TableField(exist = false)
    private Long entContractId;

}
