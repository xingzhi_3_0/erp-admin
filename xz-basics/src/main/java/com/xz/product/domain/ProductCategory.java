package com.xz.product.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 商品类别对象 t_product_category
 * 
 * @author xz
 * @date 2024-01-02
 */
@Data
@TableName("t_product_category")
public class ProductCategory extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 商品类别 */
    @Excel(name = "商品类别")
    private String productCategory;

    /** 配置类别id */
    private Long measuringUnitId;
    /** 配置类别名称 */
    @Excel(name = "配置类别名称")
    private String measuringUnitName;

    /** 属性类别（1.镜片 2.镜框 3.常规） */
    @Excel(name = "属性类别", readConverterExp = "1=.镜片,2=.镜框,3=.常规")
    private Integer attributeCategory;

    /** 是否加工 */
    @Excel(name = "是否加工")
    private Integer isMake;

    /** 是否定制 */
    @Excel(name = "是否定制")
    private Integer isCustom;

    /** 有无有效期管理 */
    @Excel(name = "有无有效期管理")
    private Integer isValidityPeriod;

    /** 预警天数 */
    private Integer warningDays;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 编码 */
    @Excel(name = "编码")
    private String encoded;
    /** 属性id */
    @Excel(name = "属性id")
    private String attributeId;
    /**绑定的属性参数*/
    @TableField(exist = false)
    private List<ProductCategoryItem> categoryItemList;

    @TableField(exist = false)
    private List<Long> idList;

}
