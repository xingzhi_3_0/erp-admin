package com.xz.product.domain;

import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 商品类别--关联属性对象 t_product_category_item
 * 
 * @author xz
 * @date 2024-01-02
 */
@Data
@TableName("t_product_category_item")
public class ProductCategoryItem {
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 商品类别id */
    @Excel(name = "商品类别id")
    private Long productCategoryId;

    /** 属性id */
    @Excel(name = "属性参数id")
    private Long attributeId;

    /** 参数组名称 */
    @Excel(name = "参数组名称")
    private String parameterName;

    /** 属性名称 */
    @Excel(name = "属性名称")
    private String productAttributeName;

    /** 下限 */
    @Excel(name = "下限")
    private String lowerLimit;

    /** 上限 */
    @Excel(name = "上限")
    private String upperLimit;

    /** 步长 */
    @Excel(name = "步长")
    private String step;

    /** 文本参数 */
    @Excel(name = "文本参数")
    private String textParam;

    /** 类别（1.镜片 2.镜框 3.常规） */
    @Excel(name = "类别", readConverterExp = "1=.镜片,2=.镜框,3=.常规")
    private Integer category;

}
