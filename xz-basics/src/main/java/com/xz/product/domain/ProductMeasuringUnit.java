package com.xz.product.domain;

import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 商品基础配置对象 t_product_measuring_unit
 * 
 * @author xz
 * @date 2024-01-02
 */
@Data
@TableName("t_product_measuring_unit")
public class ProductMeasuringUnit extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;
    /** 类别（1.计量单位 2.产品 3.品牌） */
    @NotNull(message = "类别不能为空")
    @Excel(name = "类别", readConverterExp = "1=计量单位,2=产品,3=品牌,4.商品类别")
    private Integer category;
    /** 名称 */
    @NotBlank(message = "名称不能为空")
    @Excel(name = "名称")
    private String name;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 编码 */
    @Excel(name = "编码")
    private String encoded;


}
