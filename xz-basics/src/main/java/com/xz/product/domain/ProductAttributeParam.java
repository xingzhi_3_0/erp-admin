package com.xz.product.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 商品属性参数对象 t_product_attribute_param
 * 
 * @author xz
 * @date 2024-01-02
 */
@Data
@TableName("t_product_attribute_param")
public class ProductAttributeParam {
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 商品属性id */
    @Excel(name = "商品属性id")
    private Long productAttributeId;

    /** 参数组名称 */
    @Excel(name = "参数组名称")
    private String parameterName;

    /** 输入方式（1.数值 2.文本） */
    @Excel(name = "输入方式", readConverterExp = "1=.数值,2=.文本")
    private Integer inputMode;

    /** 属性名称 */
    @Excel(name = "属性名称")
    private String productAttributeName;

    /** 下限 */
    @Excel(name = "下限")
    private String lowerLimit;

    /** 上限 */
    @Excel(name = "上限")
    private String upperLimit;

    /** 步长 */
    @Excel(name = "步长")
    private String step;

    /** 文本参数 */
    @Excel(name = "文本参数")
    private String textParam;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;
    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;
    /**
     * 属性类比
     */
    @TableField(exist = false)
    private String attributeCategory;
    /** 是否自由输入1=是,2=否 */
    @TableField(exist = false)
    private Integer isInput;
}
