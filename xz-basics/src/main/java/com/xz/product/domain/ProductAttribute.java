package com.xz.product.domain;

import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 商品属性对象 t_product_attribute
 * 
 * @author xz
 * @date 2024-01-02
 */
@Data
@TableName("t_product_attribute")
public class ProductAttribute extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 属性名称 */
    @NotBlank(message = "属性名称不能为空")
    @Excel(name = "属性名称")
    private String attributeName;

    /** 是否自由输入 */
    @NotNull(message = "是否自由输入不能为空")
    @Excel(name = "是否自由输入", readConverterExp = "1=是,2=否")
    private Integer isInput;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 属性类别（1.镜片 2.镜框 3.常规） */
    @NotNull(message = "属性类别不能为空")
    @Excel(name = "属性类别", readConverterExp = "1=镜片,2=镜框,3=常规")
    private Integer attributeCategory;

}
