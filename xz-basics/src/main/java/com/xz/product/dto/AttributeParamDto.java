package com.xz.product.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.List;

/**
 * @ClassName attributeParamDto * @Description TODO
 * @Author Administrator
 * @Date 11:57 2024/1/6
 * @Version 1.0  商品类别属性配置
 **/
@Data
public class AttributeParamDto {
    /**
     * 属性名称
     */
    private String attributeName;
    /**
     * 属性类比
     */
    private String attributeCategory;
    /** 是否自由输入1=是,2=否 */
    private Integer isInput;
    /**
     * 属性参数输入方式（1.数值 2.文本）
     */
    private Integer inputMode;
    /**
     * 属性参数
     */
    private List<String> attributeParameter;
}
