package com.xz.product.dto;

import com.xz.common.annotation.Excel;
import lombok.Data;

/**
 * @ClassName ProductAttributeDto * @Description TODO
 * @Author Administrator
 * @Date 11:25 2024/1/3
 * @Version 1.0  属性
 **/
@Data
public class ProductAttributeDto {
    /** id */
    private Long id;

    /** 属性名称 */
    @Excel(name = "属性名称")
    private String attributeName;

    /** 是否自由输入 */
    @Excel(name = "是否自由输入", readConverterExp = "1=是,2=否")
    private Integer isInput;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;
    /** 属性类别（1.镜片 2.镜框 3.常规） */
    @Excel(name = "属性类别", readConverterExp = "1=镜片,2=镜框,3=常规")
    private Integer attributeCategory;
}
