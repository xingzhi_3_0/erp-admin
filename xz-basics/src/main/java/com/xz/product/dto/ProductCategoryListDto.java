package com.xz.product.dto;

import com.xz.common.annotation.Excel;
import lombok.Data;

/**
 * @ClassName ProductCategoryListDto * @Description TODO
 * @Author Administrator
 * @Date 19:51 2024/1/3
 * @Version 1.0 商品类别列表
 **/
@Data
public class ProductCategoryListDto {
    /** id */
    private Long id;

    /** 商品类别 */
    @Excel(name = "商品类别")
    private String productCategory;
    /** 配置类别名称 */
    @Excel(name = "配置类别名称")
    private String measuringUnitName;

    /** 属性类别（1.镜片 2.镜框 3.常规） */
    @Excel(name = "属性类别", readConverterExp = "1=.镜片,2=.镜框,3=.常规")
    private Integer attributeCategory;
    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;
    /** 编码 */
    @Excel(name = "编码")
    private String encoded;
    /**
     * 商品属性
     */
    private String productAttribute;
    /**
     * 定制属性
     */
    private String customAttribute;

    /**
     * 预警天数
     */
    private Integer warningDays;
    /** 属性id */
    @Excel(name = "属性id")
    private String attributeId;
}
