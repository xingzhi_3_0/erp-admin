package com.xz.product.dto;

import com.xz.product.domain.ProductAttributeParam;
import lombok.Data;

import java.util.List;

/**
 * @ClassName ProductAttributeParamDto * @Description TODO
 * @Author Administrator
 * @Date 11:31 2024/1/3
 * @Version 1.0  查询商品属性
 **/
@Data
public class ProductAttributeParamDto extends ProductAttributeDto{
    //商品属性参数
    private List<ProductAttributeParam> productAttributeParamList;
}
