package com.xz.product.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName ProductDto * @Description TODO
 * @Author Administrator
 * @Date 10:12 2024/1/4
 * @Version 1.0
 **/
@Data
public class ProductDto {
    /** 商品ID */
    private Long id;
    @Excel(name = "商品名称")
    private String productName;
    /** 编码 */
    @Excel(name = "编码")
    private String encoded;
    /** 价格 */
    @Excel(name = "价格")
    @NotNull(message = "价格不能为空")
    private BigDecimal price;
    /** 状态（0.在售，1.停售，2.停用） */
    @NotBlank(message = "状态不能为空")
    @Excel(name = "状态", readConverterExp = "0=.在售，1.停售，2.停用")
    private String status;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /**
     * 计量单位
     */
    private String unitName;
    /** 生产厂家 */
    @Excel(name = "生产厂家")
    private String manufacturer;
    /** 注册证号 */
    @Excel(name = "注册证号")
    private String registrationNo;
    /** 医疗器械/非医疗器械（1.医疗器械，2.非医疗器械） */
    private Integer isDevices;
    @Excel(name = "商品品牌")
    private String brandName;
    @Excel(name = "产品名称")
    private String prodName;
    /*商品类别*/
    private String productCategory;
    /** 贸易中间商合同商品价格 **/
    private BigDecimal supllierContractPrice;
    /** 是否加工  字典biz_yes_no*/
    private Integer isMake;
    /** 是否定制*/
    private Integer isCustom;

}
