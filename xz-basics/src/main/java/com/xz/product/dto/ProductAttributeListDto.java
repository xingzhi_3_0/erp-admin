package com.xz.product.dto;

import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.List;

/**
 * @ClassName ProductAttributeListDto * @Description TODO
 * @Author Administrator
 * @Date 15:40 2024/1/3
 * @Version 1.0  获取属性列表
 **/
@Data
public class ProductAttributeListDto {
    /** id */
    private Long id;

    /** 属性名称 */
    private String attributeName;
    /**属性参数*/
    private List<ProductAttributeParamListDto> attributeParamListDtoList;
}

