package com.xz.product.dto;

import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.List;

/**
 * @ClassName ProductCategoryVo * @Description TODO
 * @Author Administrator
 * @Date 16:42 2024/1/3
 * @Version 1.0  分类dto
 **/
@Data
public class ProductCategoryDto {

    /** id */
    private Long id;

    /** 商品类别 */
    @Excel(name = "商品类别")
    private String productCategory;

    /** 配置类别id */
    private Long measuringUnitId;
    /** 配置类别名称 */
    @Excel(name = "配置类别名称")
    private String measuringUnitName;

    /** 属性类别（1.镜片 2.镜框 3.常规） */
    @Excel(name = "属性类别", readConverterExp = "1=.镜片,2=.镜框,3=.常规")
    private Integer attributeCategory;

    /** 是否加工 */
    @Excel(name = "是否加工")
    private Integer isMake;

    /** 是否定制 */
    @Excel(name = "是否定制")
    private Integer isCustom;

    /** 有无有效期管理 */
    @Excel(name = "有无有效期管理")
    private Integer isValidityPeriod;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 编码 */
    @Excel(name = "编码")
    private String encoded;
    /** 属性id */
    @Excel(name = "属性id")
    private String attributeId;
    /**非定制类*/
    private List<ProductCategoryItemDto> noCustomList;
    /**定制类*/
    private List<ProductCategoryItemDto> customList;
}
