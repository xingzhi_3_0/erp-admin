package com.xz.product.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName ProductExportDto * @Description TODO
 * @Author Administrator
 * @Date 10:54 2024/1/4
 * @Version 1.0
 **/
@Data
public class ProductExportDto {
    /** 编码 */
    @Excel(name = "编码")
    private String encoded;
    @Excel(name = "商品名称")
    private String productName;
    @Excel(name = "商品类别")
    private String productCategory;
    @Excel(name = "商品品牌")
    private String brandName;
    @Excel(name = "产品名称")
    private String prodName;
    @Excel(name = "计量单位")
    private String unitName;
    /** 规格 */
    @Excel(name = "规格")
    private String specification;
    /** 系列 */
    @Excel(name = "系列")
    private String series;
    /** 价格 */
    @Excel(name = "售价")
    private BigDecimal price;
    @Excel(name = "状态", readConverterExp = "0=在售,1=停售,2=停用")
    private String status;
    /** 生产厂家 */
    @Excel(name = "生产厂家")
    private String manufacturer;

    /** 医疗器械/非医疗器械（1.医疗器械，2.非医疗器械） */
    @Excel(name = "医疗器械/非医疗器械", readConverterExp = "1=医疗器械,2=非医疗器械")
    private Integer isDevices;
    /** 注册证号 */
    @Excel(name = "注册证号")
    private String registrationNo;

    /** 注册证有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "注册证有效期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date registrationValiddityDate;

}
