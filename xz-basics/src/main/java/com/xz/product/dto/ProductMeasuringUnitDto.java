package com.xz.product.dto;

import com.xz.common.annotation.Excel;
import lombok.Data;

/**
 * @ClassName ProductMeasuringUnitDto * @Description TODO
 * @Author Administrator
 * @Date 11:20 2024/1/3
 * @Version 1.0
 **/
@Data
public class ProductMeasuringUnitDto {
    /** id */
    private Long id;
    /** 类别（1.计量单位 2.产品 3.品牌） */
    @Excel(name = "类别", readConverterExp = "1=计量单位,2=产品,3=品牌")
    private Integer category;
    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;
    /** 编码 */
    @Excel(name = "编码")
    private String encoded;

}
