package com.xz.product.dto;

import com.xz.common.annotation.Excel;
import lombok.Data;

/**
 * @ClassName ProductCategoryItemDto * @Description TODO
 * @Author Administrator
 * @Date 16:49 2024/1/3
 * @Version 1.0  商品类别--关联属性
 **/
@Data
public class ProductCategoryItemDto {
    /** id */
    private Long id;

    /** 参数组名称 */
    @Excel(name = "参数组名称")
    private String parameterName;

    /** 输入方式（1.数值 2.文本） */
    @Excel(name = "输入方式", readConverterExp = "1=.数值,2=.文本")
    private Integer inputMode;

    /** 属性名称 */
    @Excel(name = "属性名称")
    private String attributeName;

    /** 下限 */
    @Excel(name = "下限")
    private String lowerLimit;

    /** 上限 */
    @Excel(name = "上限")
    private String upperLimit;

    /** 步长 */
    @Excel(name = "步长")
    private String step;

    /** 文本参数 */
    @Excel(name = "文本参数")
    private String textParam;
}
