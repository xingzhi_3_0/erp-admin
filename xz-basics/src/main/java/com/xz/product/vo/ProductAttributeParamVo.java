package com.xz.product.vo;

import com.xz.product.domain.ProductAttributeParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @ClassName productAttributeParamVo * @Description TODO
 * @Author Administrator
 * @Date 15:01 2024/1/3
 * @Version 1.0
 **/
@Data
public class ProductAttributeParamVo {
    //属性id
    @NotNull(message = "属性id不能为空")
    private Long id ;
    //商品属性参数
    @NotNull(message = "属性参数不能为空")
    private List<ProductAttributeParam> productAttributeParamList;
}
