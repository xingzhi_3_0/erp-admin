package com.xz.product.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName ProductDto * @Description TODO
 * @Author Administrator
 * @Date 10:12 2024/1/4
 * @Version 1.0
 **/
@Data
public class ProductRelatedInfoVo {
    private Long id;
    /** 商品名称 **/
    private String productName;
    /** 编码 */
    private String encoded;
    /** 价格 */
    private BigDecimal price;
    /** 状态（0.在售，1.停售，2.停用） */
    private String status;
    /** 创建日期**/
    private Date createTime;
    /** 生产厂家 */
    private String manufacturer;
    /** 注册证号 */
    private String registrationNo;
    /** 医疗器械/非医疗器械（1.医疗器械，2.非医疗器械） */
    private Integer isDevices;
    /** 规格 */
    private String specification;
    /** 系列 */
    private String series;
    /** 商品品牌 */
    private Long brandId;
    /** 产品名称 */
    private Long prodNameId;
    /** 计量单位 */
    private Long unitId;
    /** 商品类别 */
    private Long categoryId;
    /** 商品品牌 */
    private String brandName;
    /** 产品名称 */
    private String prodName;
    /** 计量单位 **/
    private String unitName;
    /*商品类别*/
    private String productCategory;
}
