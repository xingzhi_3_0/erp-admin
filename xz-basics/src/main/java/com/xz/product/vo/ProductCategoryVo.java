package com.xz.product.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @ClassName ProductCategoryVo * @Description TODO
 * @Author Administrator
 * @Date 15:19 2024/1/18
 * @Version 1.0
 **/
@Data
public class ProductCategoryVo {
    /** 商品ID */
    private Long id;
    @Excel(name = "商品名称")
    private String productName;

    /** 类别ID */
    @Excel(name = "类别ID")
    @NotNull(message = "类别ID不能为空")
    private Long categoryId;

    /** 商品类别 */
    @Excel(name = "商品类别")
    private String productCategory;
    /** 生产厂商 */
    private String manufacturer;
    /** 注册证号 */
    private String registrationNo;
    /**
     * 品牌
     */
    private String brandName;
    /**
     * 产品名称
     */
    private String prodName;
    /**
     * 产品编码
     */
    private String encoded;
    /**
     * 单位
     */
    private String unit;
}
