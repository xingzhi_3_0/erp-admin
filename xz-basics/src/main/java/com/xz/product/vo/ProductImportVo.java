package com.xz.product.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName ProductImportVo * @Description TODO
 * @Author Administrator
 * @Date 11:53 2024/1/4
 * @Version 1.0
 **/
@Data
public class ProductImportVo {
    /** *商品类别 */
    @Excel(name = "*商品类别")
    private String productCategory;
    /** *商品品牌 */
    @Excel(name = "*商品品牌")
    private String brandName;
    /** *产品名称 */
    @Excel(name = "*产品名称")
    private String prodName;
    /** *规格型号 */
    @Excel(name = "*规格型号")
    private String specification;
    /** 系列 */
    @Excel(name = "系列")
    private String series;
    /** *计量单位 */
    @Excel(name = "*计量单位")
    private String unitName;
    /** *售价 */
    @Excel(name = "*售价")
    private BigDecimal price;
    /** *生产厂家 */
    @Excel(name = "*生产厂家")
    private String manufacturer;
    /** *医疗器械/非医疗器械 */
    @Excel(name = "*医疗器械/非医疗器械")
    private String isDevicesName;
    /** 注册证 */
    @Excel(name = "注册证")
    private String registrationNo;
    /** 注册证有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "注册证有效期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date registrationValiddityDate;
    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;
}
