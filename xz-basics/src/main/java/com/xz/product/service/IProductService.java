package com.xz.product.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.product.domain.Product;
import com.xz.product.dto.AttributeParamDto;
import com.xz.product.dto.ProductDto;
import com.xz.product.dto.ProductExportDto;
import com.xz.product.dto.ProductInfoDto;
import com.xz.product.vo.ProductCategoryVo;
import com.xz.product.vo.ProductImportVo;

/**
 * 商品Service接口
 * 
 * @author xz
 * @date 2024-01-02
 */
public interface IProductService extends IService<Product> {
    /**
     * 查询商品
     * 
     * @param id 商品主键
     * @return 商品
     */
    public ProductInfoDto selectProductById(Long id);

    /**
     * 查询商品列表
     * 
     * @param product 商品
     * @return 商品集合
     */
    public List<ProductDto> selectProductList(Product product);
    public List<ProductExportDto> selectProductExport(Product product);
    public AjaxResult importExcel(List<ProductImportVo> importVoList);

    /**
     * 新增商品
     * 
     * @param product 商品
     * @return 结果
     */
    public AjaxResult insertProduct(Product product);

    /**
     * 修改商品
     * 
     * @param product 商品
     * @return 结果
     */
    public AjaxResult updateProduct(Product product);

    /**
     * 修改商品价格
     * @param product
     * @return
     */
    public AjaxResult updatePrice(Product product);

    /**
     * 批量删除商品
     * 
     * @param ids 需要删除的商品主键集合
     * @return 结果
     */
    public int deleteProductByIds(Long[] ids);

    /**
     * 删除商品信息
     * 
     * @param id 商品主键
     * @return 结果
     */
    public AjaxResult deleteProductById(Long id);
    /**
     * 商品属性
     * @param id
     * @return
     */
    List<AttributeParamDto> getProductAttributeParamList(Long id);
    /**
     * 通过编码获取商品信息
     * @return
     */
    Product getProductInfo(String encoded);
    /**
     * 获取商品属性
     * @param id
     * @return
     */
    ProductCategoryVo getProductCategory(Long id);
}
