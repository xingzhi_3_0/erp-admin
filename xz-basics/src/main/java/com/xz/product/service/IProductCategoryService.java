package com.xz.product.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.product.domain.ProductCategory;
import com.xz.product.dto.ProductCategoryDto;
import com.xz.product.dto.ProductCategoryItemDto;
import com.xz.product.dto.ProductCategoryListDto;
import org.apache.ibatis.annotations.Param;

/**
 * 商品类别Service接口
 * 
 * @author xz
 * @date 2024-01-02
 */
public interface IProductCategoryService extends IService<ProductCategory> {
    /**
     * 查询商品类别
     * 
     * @param id 商品类别主键
     * @return 商品类别
     */
    public ProductCategoryDto selectProductCategoryById(Long id);

    /**
     * 查询商品类别列表
     * 
     * @param productCategory 商品类别
     * @return 商品类别集合
     */
    public List<ProductCategoryListDto> selectProductCategoryList(ProductCategory productCategory);

    /**
     * 新增商品类别
     * 
     * @param productCategory 商品类别
     * @return 结果
     */
    public AjaxResult insertProductCategory(ProductCategory productCategory);

    /**
     * 设置预警天数
     * @param productCategory
     * @return
     */
    AjaxResult setWarnDays(ProductCategory productCategory);

    /**
     * 修改商品类别
     * 
     * @param productCategory 商品类别
     * @return 结果
     */
    public AjaxResult updateProductCategory(ProductCategory productCategory);

    /**
     * 批量删除商品类别
     * 
     * @param ids 需要删除的商品类别主键集合
     * @return 结果
     */
    public int deleteProductCategoryByIds(Long[] ids);

    /**
     * 删除商品类别信息
     * 
     * @param id 商品类别主键
     * @return 结果
     */
    public AjaxResult deleteProductCategoryById(Long id);
    /**
     * 分类属性列表
     * @return
     */
    List<ProductCategoryItemDto> getProductCategoryItem(Long productCategoryId, Integer attributeCategory);
    /**
     * 通过名称获取id
     * @param productCategory
     * @return
     */
    Long getId(String productCategory);

}
