package com.xz.product.service.impl;

import java.util.List;
import java.util.Objects;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.utils.RedisCode;
import com.xz.common.utils.SecurityUtils;
import com.xz.product.domain.ProductAttribute;
import com.xz.product.domain.ProductCategory;
import com.xz.product.dto.ProductMeasuringUnitDto;
import com.xz.product.service.IProductCategoryService;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.product.mapper.ProductMeasuringUnitMapper;
import com.xz.product.domain.ProductMeasuringUnit;
import com.xz.product.service.IProductMeasuringUnitService;

/**
 * 商品基础配置Service业务层处理
 *
 * @author xz
 * @date 2024-01-02
 */
@Service
public class ProductMeasuringUnitServiceImpl extends ServiceImpl<ProductMeasuringUnitMapper, ProductMeasuringUnit> implements IProductMeasuringUnitService {
    @Autowired
    private ProductMeasuringUnitMapper productMeasuringUnitMapper;
    @Autowired
    private IProductCategoryService productCategoryService;
    /**
     * 查询商品基础配置
     *
     * @param id 商品基础配置主键
     * @return 商品基础配置
     */
    @Override
    public ProductMeasuringUnitDto selectProductMeasuringUnitById(Long id) {
        return productMeasuringUnitMapper.selectProductMeasuringUnitById(id);
    }

    /**
     * 查询商品基础配置列表
     *
     * @param productMeasuringUnit 商品基础配置
     * @return 商品基础配置
     */
    @Override
    public List<ProductMeasuringUnitDto> selectProductMeasuringUnitList(ProductMeasuringUnit productMeasuringUnit) {
        return productMeasuringUnitMapper.selectProductMeasuringUnitList(productMeasuringUnit);
    }

    /**
     * 新增商品基础配置
     *
     * @param productMeasuringUnit 商品基础配置
     * @return 结果
     */
    @Override
    public AjaxResult insertProductMeasuringUnit(ProductMeasuringUnit productMeasuringUnit) {
        //校验是否已经添加
        Integer category = productMeasuringUnit.getCategory();
        int count = this.count(new QueryWrapper<ProductMeasuringUnit>().eq("name", productMeasuringUnit.getName()).eq("category", category).eq("del_flag", 0));
        if(count>0){
            return AjaxResult.error("数据【"+productMeasuringUnit.getName()+"】已存在");
        }
        productMeasuringUnit.setEncoded(RedisCode.getCode(category==1?"UM":category==2?"PN":"PB"));
        productMeasuringUnit.setCreateTime(DateUtils.getNowDate());
        productMeasuringUnit.setCreateBy(SecurityUtils.getUserId());
        productMeasuringUnit.setTenantId(SecurityUtils.getLoginUser().getTenantId());
        int insertProductMeasuringUnit = productMeasuringUnitMapper.insertProductMeasuringUnit(productMeasuringUnit);
        return insertProductMeasuringUnit>0? AjaxResult.success("新增成功"):AjaxResult.error("新增失败");
    }

    /**
     * 修改商品基础配置
     *
     * @param productMeasuringUnit 商品基础配置
     * @return 结果
     */
    @Override
    public AjaxResult updateProductMeasuringUnit(ProductMeasuringUnit productMeasuringUnit) {
        if(productMeasuringUnit.getId()==null){
            return AjaxResult.error("参数id不能为空");
        }
        ProductMeasuringUnit measuringUnit = productMeasuringUnitMapper.selectById(productMeasuringUnit.getId());
        if(Objects.isNull(measuringUnit)){
            return AjaxResult.error("数据不存在");
        }
        //校验是否已经添加
        Integer category = productMeasuringUnit.getCategory();
        int count = this.count(new QueryWrapper<ProductMeasuringUnit>().eq("name", productMeasuringUnit.getName()).eq("category", category).eq("del_flag", 0).ne("id",productMeasuringUnit.getId()));
        if(count>0){
            return AjaxResult.error("数据【"+productMeasuringUnit.getName()+"】已存在");
        }
        productMeasuringUnit.setUpdateTime(DateUtils.getNowDate());
        productMeasuringUnit.setUpdateBy(SecurityUtils.getUserId());
        int updateProductMeasuringUnit = productMeasuringUnitMapper.updateProductMeasuringUnit(productMeasuringUnit);
        return updateProductMeasuringUnit>0? AjaxResult.success("修改成功"):AjaxResult.error("修改失败");
    }

    /**
     * 批量删除商品基础配置
     *
     * @param ids 需要删除的商品基础配置主键
     * @return 结果
     */
    @Override
    public int deleteProductMeasuringUnitByIds(Long[] ids) {
        return productMeasuringUnitMapper.deleteProductMeasuringUnitByIds(ids);
    }

    /**
     * 删除商品基础配置信息
     *
     * @param id 商品基础配置主键
     * @return 结果
     */
    @Override
    public AjaxResult deleteProductMeasuringUnitById(Long id) {
        ProductMeasuringUnit productMeasuringUnit = productMeasuringUnitMapper.selectById(id);
        if(Objects.isNull(productMeasuringUnit)){
            return AjaxResult.error("商品属性不存在");
        }
        if(productMeasuringUnit.getCategory()==4){
            int count = productCategoryService.count(new QueryWrapper<ProductCategory>().eq("measuring_unit_id", id));
            if(count>0){
                return AjaxResult.error("类别已被使用，不能进行删除");
            }
        }
        int deleteProductMeasuringUnitById = productMeasuringUnitMapper.deleteProductMeasuringUnitById(id);
        return deleteProductMeasuringUnitById>0? AjaxResult.success("删除成功"):AjaxResult.error("删除失败");
    }

    @Override
    public Long getId(String name,Integer category) {
        return baseMapper.getId(name,category);
    }
}
