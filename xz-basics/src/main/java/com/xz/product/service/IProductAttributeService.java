package com.xz.product.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.product.domain.ProductAttribute;
import com.xz.product.dto.ProductAttributeDto;
import com.xz.product.dto.ProductAttributeListDto;
import com.xz.product.dto.ProductAttributeParamDto;

/**
 * 商品属性Service接口
 * 
 * @author xz
 * @date 2024-01-02
 */
public interface IProductAttributeService extends IService<ProductAttribute> {
    /**
     * 查询商品属性
     * 
     * @param id 商品属性主键
     * @return 商品属性
     */
    public ProductAttributeParamDto selectProductAttributeById(Long id);

    /**
     * 查询商品属性列表
     * 
     * @param productAttribute 商品属性
     * @return 商品属性集合
     */
    public List<ProductAttributeDto> selectProductAttributeList(ProductAttribute productAttribute);

    /**
     * 新增商品属性
     * 
     * @param productAttribute 商品属性
     * @return 结果
     */
    public AjaxResult insertProductAttribute(ProductAttribute productAttribute);

    /**
     * 修改商品属性
     * 
     * @param productAttribute 商品属性
     * @return 结果
     */
    public AjaxResult updateProductAttribute(ProductAttribute productAttribute);

    /**
     * 批量删除商品属性
     * 
     * @param ids 需要删除的商品属性主键集合
     * @return 结果
     */
    public int deleteProductAttributeByIds(Long[] ids);

    /**
     * 删除商品属性信息
     * 
     * @param id 商品属性主键
     * @return 结果
     */
    public AjaxResult deleteProductAttributeById(Long id);
    /**
     * 获取属性列表
     * @param attributeCategory
     * @return
     */
    List<ProductAttributeListDto> getProductAttributeList(Integer attributeCategory);
}
