package com.xz.product.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.product.domain.ProductAttributeParam;
import com.xz.product.vo.ProductAttributeParamVo;

/**
 * 商品属性参数Service接口
 * 
 * @author xz
 * @date 2024-01-02
 */
public interface IProductAttributeParamService extends IService<ProductAttributeParam> {
    /**
     * 查询商品属性参数
     * 
     * @param id 商品属性参数主键
     * @return 商品属性参数
     */
    public ProductAttributeParam selectProductAttributeParamById(Long id);

    /**
     * 查询商品属性参数列表
     * 
     * @param productAttributeParam 商品属性参数
     * @return 商品属性参数集合
     */
    public List<ProductAttributeParam> selectProductAttributeParamList(ProductAttributeParam productAttributeParam);

    /**
     * 新增商品属性参数
     * 
     * @param productAttributeParam 商品属性参数
     * @return 结果
     */
    public AjaxResult insertProductAttributeParam(ProductAttributeParamVo productAttributeParam);

    /**
     * 修改商品属性参数
     * 
     * @param productAttributeParam 商品属性参数
     * @return 结果
     */
    public int updateProductAttributeParam(ProductAttributeParam productAttributeParam);

    /**
     * 批量删除商品属性参数
     * 
     * @param ids 需要删除的商品属性参数主键集合
     * @return 结果
     */
    public int deleteProductAttributeParamByIds(Long[] ids);

    /**
     * 删除商品属性参数信息
     * 
     * @param id 商品属性参数主键
     * @return 结果
     */
    public int deleteProductAttributeParamById(Long id);
    /**
     * 根据属性id删除属性参数
     *
     * @param id 商品属性参数主键
     * @return 结果
     */
    public int deleteProductAttributeById(Long id);
}
