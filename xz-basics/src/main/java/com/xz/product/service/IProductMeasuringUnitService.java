package com.xz.product.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.product.domain.ProductMeasuringUnit;
import com.xz.product.dto.ProductMeasuringUnitDto;

/**
 * 商品基础配置Service接口
 * 
 * @author xz
 * @date 2024-01-02
 */
public interface IProductMeasuringUnitService extends IService<ProductMeasuringUnit> {
    /**
     * 查询商品基础配置
     * 
     * @param id 商品基础配置主键
     * @return 商品基础配置
     */
    public ProductMeasuringUnitDto selectProductMeasuringUnitById(Long id);

    /**
     * 查询商品基础配置列表
     * 
     * @param productMeasuringUnit 商品基础配置
     * @return 商品基础配置集合
     */
    public List<ProductMeasuringUnitDto> selectProductMeasuringUnitList(ProductMeasuringUnit productMeasuringUnit);

    /**
     * 新增商品基础配置
     * 
     * @param productMeasuringUnit 商品基础配置
     * @return 结果
     */
    public AjaxResult insertProductMeasuringUnit(ProductMeasuringUnit productMeasuringUnit);

    /**
     * 修改商品基础配置
     * 
     * @param productMeasuringUnit 商品基础配置
     * @return 结果
     */
    public AjaxResult updateProductMeasuringUnit(ProductMeasuringUnit productMeasuringUnit);

    /**
     * 批量删除商品基础配置
     * 
     * @param ids 需要删除的商品基础配置主键集合
     * @return 结果
     */
    public int deleteProductMeasuringUnitByIds(Long[] ids);

    /**
     * 删除商品基础配置信息
     * 
     * @param id 商品基础配置主键
     * @return 结果
     */
    public AjaxResult deleteProductMeasuringUnitById(Long id);
    /**
     * 通过名称获取配置id
     * @param name
     * @return
     */
    Long getId(String name,Integer category);
}
