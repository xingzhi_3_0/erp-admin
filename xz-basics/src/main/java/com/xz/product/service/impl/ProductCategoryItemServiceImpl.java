package com.xz.product.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.product.mapper.ProductCategoryItemMapper;
import com.xz.product.domain.ProductCategoryItem;
import com.xz.product.service.IProductCategoryItemService;

/**
 * 商品类别--关联属性Service业务层处理
 *
 * @author xz
 * @date 2024-01-02
 */
@Service
public class ProductCategoryItemServiceImpl extends ServiceImpl<ProductCategoryItemMapper, ProductCategoryItem> implements IProductCategoryItemService {
    @Autowired
    private ProductCategoryItemMapper productCategoryItemMapper;

    /**
     * 查询商品类别--关联属性
     *
     * @param id 商品类别--关联属性主键
     * @return 商品类别--关联属性
     */
    @Override
    public ProductCategoryItem selectProductCategoryItemById(Long id) {
        return productCategoryItemMapper.selectProductCategoryItemById(id);
    }

    /**
     * 查询商品类别--关联属性列表
     *
     * @param productCategoryItem 商品类别--关联属性
     * @return 商品类别--关联属性
     */
    @Override
    public List<ProductCategoryItem> selectProductCategoryItemList(ProductCategoryItem productCategoryItem) {
        return productCategoryItemMapper.selectProductCategoryItemList(productCategoryItem);
    }

    /**
     * 新增商品类别--关联属性
     *
     * @param productCategoryItem 商品类别--关联属性
     * @return 结果
     */
    @Override
    public int insertProductCategoryItem(ProductCategoryItem productCategoryItem) {
        return productCategoryItemMapper.insertProductCategoryItem(productCategoryItem);
    }

    /**
     * 修改商品类别--关联属性
     *
     * @param productCategoryItem 商品类别--关联属性
     * @return 结果
     */
    @Override
    public int updateProductCategoryItem(ProductCategoryItem productCategoryItem) {
        return productCategoryItemMapper.updateProductCategoryItem(productCategoryItem);
    }

    /**
     * 批量删除商品类别--关联属性
     *
     * @param ids 需要删除的商品类别--关联属性主键
     * @return 结果
     */
    @Override
    public int deleteProductCategoryItemByIds(Long[] ids) {
        return productCategoryItemMapper.deleteProductCategoryItemByIds(ids);
    }

    /**
     * 删除商品类别--关联属性信息
     *
     * @param id 商品类别--关联属性主键
     * @return 结果
     */
    @Override
    public int deleteProductCategoryItemById(Long id) {
        return productCategoryItemMapper.deleteProductCategoryItemById(id);
    }
}
