package com.xz.product.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.product.domain.ProductCategoryItem;

/**
 * 商品类别--关联属性Service接口
 * 
 * @author xz
 * @date 2024-01-02
 */
public interface IProductCategoryItemService extends IService<ProductCategoryItem> {
    /**
     * 查询商品类别--关联属性
     * 
     * @param id 商品类别--关联属性主键
     * @return 商品类别--关联属性
     */
    public ProductCategoryItem selectProductCategoryItemById(Long id);

    /**
     * 查询商品类别--关联属性列表
     * 
     * @param productCategoryItem 商品类别--关联属性
     * @return 商品类别--关联属性集合
     */
    public List<ProductCategoryItem> selectProductCategoryItemList(ProductCategoryItem productCategoryItem);

    /**
     * 新增商品类别--关联属性
     * 
     * @param productCategoryItem 商品类别--关联属性
     * @return 结果
     */
    public int insertProductCategoryItem(ProductCategoryItem productCategoryItem);

    /**
     * 修改商品类别--关联属性
     * 
     * @param productCategoryItem 商品类别--关联属性
     * @return 结果
     */
    public int updateProductCategoryItem(ProductCategoryItem productCategoryItem);

    /**
     * 批量删除商品类别--关联属性
     * 
     * @param ids 需要删除的商品类别--关联属性主键集合
     * @return 结果
     */
    public int deleteProductCategoryItemByIds(Long[] ids);

    /**
     * 删除商品类别--关联属性信息
     * 
     * @param id 商品类别--关联属性主键
     * @return 结果
     */
    public int deleteProductCategoryItemById(Long id);
}
