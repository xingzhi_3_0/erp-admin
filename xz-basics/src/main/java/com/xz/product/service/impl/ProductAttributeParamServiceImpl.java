package com.xz.product.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beust.jcommander.Strings;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.exception.ServiceException;
import com.xz.product.vo.ProductAttributeParamVo;
import common.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.product.mapper.ProductAttributeParamMapper;
import com.xz.product.domain.ProductAttributeParam;
import com.xz.product.service.IProductAttributeParamService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

/**
 * 商品属性参数Service业务层处理
 *
 * @author xz
 * @date 2024-01-02
 */
@Service
public class ProductAttributeParamServiceImpl extends ServiceImpl<ProductAttributeParamMapper, ProductAttributeParam> implements IProductAttributeParamService {
    @Autowired
    private ProductAttributeParamMapper productAttributeParamMapper;

    /**
     * 查询商品属性参数
     *
     * @param id 商品属性参数主键
     * @return 商品属性参数
     */
    @Override
    public ProductAttributeParam selectProductAttributeParamById(Long id) {
        return productAttributeParamMapper.selectProductAttributeParamById(id);
    }

    /**
     * 查询商品属性参数列表
     *
     * @param productAttributeParam 商品属性参数
     * @return 商品属性参数
     */
    @Override
    public List<ProductAttributeParam> selectProductAttributeParamList(ProductAttributeParam productAttributeParam) {
        return productAttributeParamMapper.selectProductAttributeParamList(productAttributeParam);
    }

    /**
     * 新增商品属性参数
     *
     * @param productAttributeParam 商品属性参数
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public AjaxResult insertProductAttributeParam(ProductAttributeParamVo productAttributeParam) {
        List<ProductAttributeParam> productAttributeParamList = productAttributeParam.getProductAttributeParamList();

        List<Long> collected = productAttributeParamList.stream().filter(i -> i.getId() != null).map(i -> i.getId()).collect(Collectors.toList());
        //删除不存在的数据
        if(!CollectionUtils.isEmpty(collected)) {

            List<ProductAttributeParam> attributeParamList = this.list(new QueryWrapper<ProductAttributeParam>().notIn("id", collected).eq("product_attribute_id", productAttributeParam.getId()));
            if (!CollectionUtils.isEmpty(attributeParamList)) {
                attributeParamList.stream().forEach(i -> {
                    this.deleteProductAttributeById(i.getId());
                });
            }
        }
        productAttributeParamList.stream().forEach(i->{
            i.setProductAttributeId(productAttributeParam.getId());
            if(i.getId()!=null){
                this.updateProductAttributeParam(i);
            }else{
                this.save(i);
            }
        });

        return AjaxResult.success("设置成功");

    }

    /**
     * 修改商品属性参数
     *
     * @param productAttributeParam 商品属性参数
     * @return 结果
     */
    @Override
    public int updateProductAttributeParam(ProductAttributeParam productAttributeParam) {
        return productAttributeParamMapper.updateProductAttributeParam(productAttributeParam);
    }

    /**
     * 批量删除商品属性参数
     *
     * @param ids 需要删除的商品属性参数主键
     * @return 结果
     */
    @Override
    public int deleteProductAttributeParamByIds(Long[] ids) {
        return productAttributeParamMapper.deleteProductAttributeParamByIds(ids);
    }

    /**
     * 删除商品属性参数信息
     *
     * @param id 商品属性参数主键
     * @return 结果
     */
    @Override
    public int deleteProductAttributeParamById(Long id) {
        return productAttributeParamMapper.deleteProductAttributeParamById(id);
    }

    @Override
    public int deleteProductAttributeById(Long id) {
        return productAttributeParamMapper.deleteProductAttributeById(id);
    }
}
