package com.xz.product.service.impl;

import java.util.List;
import java.util.Objects;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.utils.RedisCode;
import com.xz.common.utils.SecurityUtils;
import com.xz.product.domain.ProductAttributeParam;
import com.xz.product.domain.ProductMeasuringUnit;
import com.xz.product.dto.ProductAttributeDto;
import com.xz.product.dto.ProductAttributeListDto;
import com.xz.product.dto.ProductAttributeParamDto;
import com.xz.product.service.IProductAttributeParamService;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.product.mapper.ProductAttributeMapper;
import com.xz.product.domain.ProductAttribute;
import com.xz.product.service.IProductAttributeService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

/**
 * 商品属性Service业务层处理
 *
 * @author xz
 * @date 2024-01-02
 */
@Service
public class ProductAttributeServiceImpl extends ServiceImpl<ProductAttributeMapper, ProductAttribute> implements IProductAttributeService {
    @Autowired
    private ProductAttributeMapper productAttributeMapper;
    @Autowired
    private IProductAttributeParamService iProductAttributeParamService;

    /**
     * 查询商品属性
     *
     * @param id 商品属性主键
     * @return 商品属性
     */
    @Override
    public ProductAttributeParamDto selectProductAttributeById(Long id) {
        ProductAttributeParamDto productAttributeParamDto = productAttributeMapper.selectProductAttributeById(id);
        if(Objects.nonNull(productAttributeParamDto)){
            List<ProductAttributeParam> attributeParamList = iProductAttributeParamService.list(new QueryWrapper<ProductAttributeParam>().eq("product_attribute_id", id).eq("del_flag",0));
            if(!CollectionUtils.isEmpty(attributeParamList)){
                productAttributeParamDto.setProductAttributeParamList(attributeParamList);
            }
        }
        return productAttributeParamDto;
    }

    /**
     * 查询商品属性列表
     *
     * @param productAttribute 商品属性
     * @return 商品属性
     */
    @Override
    public List<ProductAttributeDto> selectProductAttributeList(ProductAttribute productAttribute) {
        return productAttributeMapper.selectProductAttributeList(productAttribute);
    }

    /**
     * 新增商品属性
     *
     * @param productAttribute 商品属性
     * @return 结果
     */
    @Override
    public AjaxResult insertProductAttribute(ProductAttribute productAttribute) {
        //校验是否已经添加
        int count = this.count(new QueryWrapper<ProductAttribute>().eq("attribute_name", productAttribute.getAttributeName()).eq("attribute_category", productAttribute.getAttributeCategory()).eq("del_flag", 0));
        if(count>0){
            return AjaxResult.error("数据【"+productAttribute.getAttributeName()+"】已存在");
        }
        productAttribute.setCreateTime(DateUtils.getNowDate());
        productAttribute.setCreateBy(SecurityUtils.getUserId());
        productAttribute.setTenantId(SecurityUtils.getLoginUser().getTenantId());
        int insertProductAttribute = productAttributeMapper.insertProductAttribute(productAttribute);
        return insertProductAttribute>0? AjaxResult.success("新增成功"):AjaxResult.error("新增失败");
    }

    /**
     * 修改商品属性
     *
     * @param productAttribute 商品属性
     * @return 结果
     */
    @Override
    public AjaxResult updateProductAttribute(ProductAttribute productAttribute) {
        if(productAttribute.getId()==null){
            return AjaxResult.error("参数id不能为空");
        }
        //校验是否已经添加
        int count = this.count(new QueryWrapper<ProductAttribute>().eq("attribute_name", productAttribute.getAttributeName()).eq("attribute_category", productAttribute.getAttributeCategory()).eq("del_flag", 0).ne("id",productAttribute.getId()));
        if(count>0){
            return AjaxResult.error("数据【"+productAttribute.getAttributeName()+"】已存在");
        }
        productAttribute.setUpdateTime(DateUtils.getNowDate());
        productAttribute.setUpdateBy(SecurityUtils.getUserId());
        int updateProductAttribute = productAttributeMapper.updateProductAttribute(productAttribute);
        return updateProductAttribute>0? AjaxResult.success("修改成功"):AjaxResult.error("修改失败");
    }

    /**
     * 批量删除商品属性
     *
     * @param ids 需要删除的商品属性主键
     * @return 结果
     */
    @Override
    public int deleteProductAttributeByIds(Long[] ids) {
        return productAttributeMapper.deleteProductAttributeByIds(ids);
    }

    /**
     * 删除商品属性信息
     *
     * @param id 商品属性主键
     * @return 结果
     */
    @Override
    public AjaxResult deleteProductAttributeById(Long id) {
        ProductAttribute productAttribute = productAttributeMapper.selectById(id);
        if(Objects.isNull(productAttribute)){
            return AjaxResult.error("商品属性不存在");
        }
        int deleteProductAttributeById = productAttributeMapper.deleteProductAttributeById(id);
        if(deleteProductAttributeById>0){
            iProductAttributeParamService.deleteProductAttributeById(id);
        }

        return deleteProductAttributeById>0? AjaxResult.success("删除成功"):AjaxResult.error("删除失败");
    }

    /**
     * 获取属性列表
     * @param attributeCategory
     * @return
     */
    @Override
    public List<ProductAttributeListDto> getProductAttributeList(Integer attributeCategory) {
        return baseMapper.getProductAttributeList(attributeCategory);
    }
}
