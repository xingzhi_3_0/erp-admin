package com.xz.log.service.impl;

import java.util.List;

import com.xz.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.log.dto.OperationLogDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.log.mapper.OperationLogMapper;
import com.xz.log.domain.OperationLog;
import com.xz.log.service.IOperationLogService;

/**
 * 操作记录Service业务层处理
 *
 * @author xz
 * @date 2024-01-08
 */
@Service
public class OperationLogServiceImpl extends ServiceImpl<OperationLogMapper, OperationLog> implements IOperationLogService {
    @Autowired
    private OperationLogMapper operationLogMapper;

    /**
     * 查询操作记录
     *
     * @param id 操作记录主键
     * @return 操作记录
     */
    @Override
    public OperationLog selectOperationLogById(Long id) {
        return operationLogMapper.selectOperationLogById(id);
    }

    /**
     * 查询操作记录列表
     *
     * @param operationLog 操作记录
     * @return 操作记录
     */
    @Override
    public List<OperationLogDto> selectOperationLogList(OperationLog operationLog) {
        return operationLogMapper.selectOperationLogList(operationLog);
    }

    /**
     * 新增操作记录
     *
     * @param operationLog 操作记录
     * @return 结果
     */
    @Override
    public int insertOperationLog(OperationLog operationLog) {
        operationLog.setCreateTime(DateUtils.getNowDate());
        return operationLogMapper.insertOperationLog(operationLog);
    }

    /**
     * 修改操作记录
     *
     * @param operationLog 操作记录
     * @return 结果
     */
    @Override
    public int updateOperationLog(OperationLog operationLog) {
        return operationLogMapper.updateOperationLog(operationLog);
    }

    /**
     * 批量删除操作记录
     *
     * @param ids 需要删除的操作记录主键
     * @return 结果
     */
    @Override
    public int deleteOperationLogByIds(Long[] ids) {
        return operationLogMapper.deleteOperationLogByIds(ids);
    }

    /**
     * 删除操作记录信息
     *
     * @param id 操作记录主键
     * @return 结果
     */
    @Override
    public int deleteOperationLogById(Long id) {
        return operationLogMapper.deleteOperationLogById(id);
    }
}
