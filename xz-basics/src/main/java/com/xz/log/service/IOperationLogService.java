package com.xz.log.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.log.domain.OperationLog;
import com.xz.log.dto.OperationLogDto;

/**
 * 操作记录Service接口
 * 
 * @author xz
 * @date 2024-01-08
 */
public interface IOperationLogService extends IService<OperationLog> {
    /**
     * 查询操作记录
     * 
     * @param id 操作记录主键
     * @return 操作记录
     */
    public OperationLog selectOperationLogById(Long id);

    /**
     * 查询操作记录列表
     * 
     * @param operationLog 操作记录
     * @return 操作记录集合
     */
    public List<OperationLogDto> selectOperationLogList(OperationLog operationLog);

    /**
     * 新增操作记录
     * 
     * @param operationLog 操作记录
     * @return 结果
     */
    public int insertOperationLog(OperationLog operationLog);

    /**
     * 修改操作记录
     * 
     * @param operationLog 操作记录
     * @return 结果
     */
    public int updateOperationLog(OperationLog operationLog);

    /**
     * 批量删除操作记录
     * 
     * @param ids 需要删除的操作记录主键集合
     * @return 结果
     */
    public int deleteOperationLogByIds(Long[] ids);

    /**
     * 删除操作记录信息
     * 
     * @param id 操作记录主键
     * @return 结果
     */
    public int deleteOperationLogById(Long id);
}
