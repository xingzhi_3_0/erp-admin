package com.xz.log.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName OperationLogDto * @Description TODO
 * @Author Administrator
 * @Date 19:32 2024/1/8
 * @Version 1.0
 **/
@Data
public class OperationLogDto {
    /** 创建时间 */

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 操作人名称 */
    @Excel(name = "操作人名称")
    private String createName;

    /**
     * 日志记录
     */
    private String remark;
}
