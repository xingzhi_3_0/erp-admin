package com.xz.log.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.log.dto.OperationLogDto;
import org.apache.ibatis.annotations.Mapper;
import com.xz.log.domain.OperationLog;

/**
 * 操作记录Mapper接口
 * 
 * @author xz
 * @date 2024-01-08
 */
@Mapper
public interface OperationLogMapper  extends BaseMapper<OperationLog> {
    /**
     * 查询操作记录
     * 
     * @param id 操作记录主键
     * @return 操作记录
     */
    public OperationLog selectOperationLogById(Long id);

    /**
     * 查询操作记录列表
     * 
     * @param operationLog 操作记录
     * @return 操作记录集合
     */
    public List<OperationLogDto> selectOperationLogList(OperationLog operationLog);

    /**
     * 新增操作记录
     * 
     * @param operationLog 操作记录
     * @return 结果
     */
    public int insertOperationLog(OperationLog operationLog);

    /**
     * 修改操作记录
     * 
     * @param operationLog 操作记录
     * @return 结果
     */
    public int updateOperationLog(OperationLog operationLog);

    /**
     * 删除操作记录
     * 
     * @param id 操作记录主键
     * @return 结果
     */
    public int deleteOperationLogById(Long id);

    /**
     * 批量删除操作记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOperationLogByIds(Long[] ids);
}
