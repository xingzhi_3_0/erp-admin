package com.xz.log.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 操作记录对象 t_operation_log
 * 
 * @author xz
 * @date 2024-01-08
 */
@Data
@TableName("t_operation_log")
public class OperationLog extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** 记录id，主键 */
    private Long id;

    /** 业务编号 */
    @Excel(name = "业务编号")
    private String bizNo;

    /** 业务类型（1.采购单价）4-预占库存日志 */
    @Excel(name = "业务类型", readConverterExp = "1=.采购单价")
    private Integer bizType;

    /** 操作人名称 */
    @Excel(name = "操作人名称")
    private String createName;
    private String remark;

    /** 所属部门 */
    @Excel(name = "所属部门")
    private Long deptId;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;
    /** 创建者 */
//    @Excel(name = "创建人")
    private Long createBy;
    /** 创建时间 */

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    public OperationLog(){};
    /** 4-预占库存日志 */
    public OperationLog(String bizNo, Integer bizType, Long tenantId, Long createBy,String createName,String remark){
        this.bizNo=bizNo;
        this.bizType=bizType;
        this.tenantId=tenantId;
        this.createBy=createBy;
        this.createName=createName;
        this.remark=remark;
        this.createTime=new Date();
    }

}
