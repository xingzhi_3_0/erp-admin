package com.xz.patient.vo;

import com.xz.common.annotation.Excel;
import com.xz.diagnosisOrder.utils.DiagnosisOrderUtils;
import lombok.Data;

import java.util.Date;

/**
 * 患者信息对象 t_patient_info
 *
 * @author xz
 * @date 2024-01-30
 */
@Data
public class PatientInfoExportVo {

  @Excel(name = "患者信息")
  private String patientInfo;
  @Excel(name = "姓名")
  private String patientName;
  @Excel(name = "性别", readConverterExp = "0=不详,1=男,2=女")
  private Integer sex;
  @Excel(name = "出生日期", width = 30, dateFormat = "yyyy-MM-dd")
  private Date birthday;
  @Excel(name = "手机号")
  private String phoneNumber;
  @Excel(name = "证件类型", readConverterExp = "1=身份证,2=就诊卡,3=护照,4=军官证,5=台湾居民来往大陆通行证,6=港澳居民来往大陆通行证")
  private Integer documentType;
  @Excel(name = "证件号码")
  private String documentNo;
  @Excel(name = "民族")
  private String nation;
  @Excel(name = "省/市/县")
  private String provinceCityArea;
  @Excel(name = "详细地址")
  private String address;
  @Excel(name = "籍贯")
  private String nativePlace;
  @Excel(name = "紧急联系人关系", readConverterExp = "1=父母,2=子女,3=配偶,4=亲属,5=朋友,6=其他")
  private Integer contactsType;
  @Excel(name = "紧急联系人姓名")
  private String contacts;
  @Excel(name = "紧急联系人号码")
  private String contactPhone;
  @Excel(name = "标签", readConverterExp = "1=角塑镜客户,2=角塑镜客户")
  private Integer label;
  @Excel(name = "推荐人", readConverterExp = "1=机构人员,2=老客户")
  private Integer referenceType;
  @Excel(name = "推荐人姓名")
  private String referenceName;
  @Excel(name = "档案号")
  private String fileNo;
  @Excel(name = "备注")
  private String remark;
  @Excel(name = "建档日期", width = 30, dateFormat = "yyyy-MM-dd")
  private Date createTime;
  @Excel(name = "建档人")
  private String createByName;
  @Excel(name = "建档门店")
  private String deptName;
  @Excel(name = "绑定会员卡号")
  private String memberCardNo;
  @Excel(name = "最近一次就诊日期", width = 30, dateFormat = "yyyy-MM-dd")
  private Date registrationDate;
}
