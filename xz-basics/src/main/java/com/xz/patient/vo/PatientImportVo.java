package com.xz.patient.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName PatientImportVo * @Description TODO
 * @Author Administrator
 * @Date 2024-3-4 13:55:29
 * @Version 1.0 患者导入
 **/
@Data
public class PatientImportVo {
    /** 患者名称 */
    @Excel(name = "患者名称")
    private String patientName;

    /** 档案号 */
    @Excel(name = "档案号")
    private String fileNo;


    /** 0-- 1-男 2-女 */
    @Excel(name = "性别", readConverterExp = "0=-,1=男,2=女",combo={ "-","男","女"},defaultValue="-")
    private Integer sex;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phoneNumber;

    /** 绑定会员号 */
    @Excel(name = "绑定会员号")
    private String bindMemberCardNo;

    /** 绑定关系 */
    @Excel(name = "绑定关系", readConverterExp = "1=父母,2=子女,3=配偶,4=亲属,5=朋友,6=其他",combo={ "父母","子女","配偶","亲属","朋友","其他"},defaultValue="其他")
    private Integer bindRelationship;

    /** 出生日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birthday;

}
