package com.xz.patient.vo;

import lombok.Data;

import java.util.List;

@Data
public class SendPatientVo {

    private List<Long> idList;

    private Integer smsTemplateType;
}
