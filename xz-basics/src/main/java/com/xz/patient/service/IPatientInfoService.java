package com.xz.patient.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.patient.domain.PatientInfo;
import com.xz.patient.vo.PatientImportVo;
import com.xz.patient.vo.PatientInfoExportVo;
import com.xz.patient.vo.SendPatientVo;

/**
 * 患者信息Service接口
 *
 * @author xz
 * @date 2024-01-30
 */
public interface IPatientInfoService extends IService<PatientInfo> {
    /**
     * 查询患者信息
     *
     * @param id 患者信息主键
     * @return 患者信息
     */
    public PatientInfo selectPatientInfoById(Long id);

    /**
     * 查询患者信息列表
     *
     * @param patientInfo 患者信息
     * @return 患者信息集合
     */
    public List<PatientInfo> selectPatientInfoList(PatientInfo patientInfo);

    /**
     * 查询患者信息列表（导出）
     *
     * @param patientInfo 患者信息
     * @return 患者信息
     */
    List<PatientInfoExportVo> queryListForExport(PatientInfo patientInfo);

    public List<PatientInfo> findList(PatientInfo patientInfo);


    /**
     * 新增患者信息
     *
     * @param patientInfo 患者信息
     * @return 结果
     */
    public AjaxResult insertPatientInfo(PatientInfo patientInfo);

    /**
     * 修改患者信息
     *
     * @param patientInfo 患者信息
     * @return 结果
     */
    public AjaxResult updatePatientInfo(PatientInfo patientInfo);

    /**
     * 修改患者门店
     *
     * @param patientInfo 患者信息
     * @return 结果
     */
    public int updateDept(PatientInfo patientInfo);

    int registerMember(PatientInfo patientInfo);

    AjaxResult sendPatientSms(SendPatientVo sendPatientVo);

    AjaxResult importExcel(List<PatientImportVo> list);

    /**
     * 批量删除患者信息
     *
     * @param ids 需要删除的患者信息主键集合
     * @return 结果
     */
    public int deletePatientInfoByIds(Long[] ids);

    /**
     * 删除患者信息信息
     *
     * @param id 患者信息主键
     * @return 结果
     */
    public int deletePatientInfoById(Long id);
}
