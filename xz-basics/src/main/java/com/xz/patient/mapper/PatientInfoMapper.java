package com.xz.patient.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.patient.vo.PatientInfoExportVo;
import com.xz.report.param.SalesReportParam;
import com.xz.staging.vo.WorkbenchesVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.patient.domain.PatientInfo;
import org.apache.ibatis.annotations.Param;

/**
 * 患者信息Mapper接口
 *
 * @author xz
 * @date 2024-01-30
 */
@Mapper
public interface PatientInfoMapper extends BaseMapper<PatientInfo> {
  /**
   * 查询患者信息
   *
   * @param id 患者信息主键
   * @return 患者信息
   */
  public PatientInfo selectPatientInfoById(Long id);

  /**
   * 根据PatientInfo对象查询患者信息
   *
   * @param patientInfo
   * @return
   */
  public PatientInfo selectPatientInfo(PatientInfo patientInfo);

  /**
   * 查询患者信息列表
   *
   * @param patientInfo 患者信息
   * @return 患者信息集合
   */
  public List<PatientInfo> selectPatientInfoList(PatientInfo patientInfo);

  /**
   * 查询患者信息列表(导出)
   *
   * @param patientInfo 患者信息
   * @return 患者信息集合
   */
  public List<PatientInfoExportVo> queryListForExport(PatientInfo patientInfo);

  /**
   * 查询患者信息列表
   *
   * @param patientInfo 患者信息
   * @return 患者信息集合
   */
  public List<PatientInfo> queryList(PatientInfo patientInfo);

  /**
   * 批量ID查询列表
   *
   * @param idList
   * @return
   */
  public List<PatientInfo> findByIdList(@Param("idList") List<Long> idList);

  /**
   * 工作台患者统计
   *
   * @param salesReportParam
   * @return
   */
  WorkbenchesVo patientWorkbenches(SalesReportParam salesReportParam);

  /**
   * 新增患者信息
   *
   * @param patientInfo 患者信息
   * @return 结果
   */
  public int insertPatientInfo(PatientInfo patientInfo);

  /**
   * 根据手机号查询患者
   *
   * @param phoneNumber
   * @return
   */
  PatientInfo findByPhoneNumber(@Param("phoneNumber") String phoneNumber);

  /**
   * 修改患者信息
   *
   * @param patientInfo 患者信息
   * @return 结果
   */
  public int updatePatientInfo(PatientInfo patientInfo);

  /**
   * 删除患者信息
   *
   * @param id 患者信息主键
   * @return 结果
   */
  public int deletePatientInfoById(Long id);

  /**
   * 批量删除患者信息
   *
   * @param ids 需要删除的数据主键集合
   * @return 结果
   */
  public int deletePatientInfoByIds(Long[] ids);
}
