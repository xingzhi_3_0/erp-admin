package com.xz.patient.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 患者信息对象 t_patient_info
 *
 * @author xz
 * @date 2024-01-30
 */
@Data
@TableName("t_patient_info")
public class PatientInfo extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 患者名称 */
    @Excel(name = "患者名称")
    private String patientName;

    /** 档案号 */
    @Excel(name = "档案号")
    private String fileNo;


    /** 0-不详 1-男 2-女 */
    @Excel(name = "0-不详 1-男 2-女")
    private Integer sex;

    @TableField(exist = false)
    private String sexStr;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phoneNumber;

    /** 出生日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birthday;

    /** 民族 */
    @Excel(name = "民族")
    private String nation;

    /** 籍贯 */
    @Excel(name = "籍贯")
    private String nativePlace;

    /** 证件类型:1-身份证 2-就诊卡 3-护照 4-军官证 5-台湾居民来往大陆通行证 6-港澳居民来往大陆通行证 */
    @Excel(name = "证件类型:1-身份证 2-就诊卡 3-护照 4-军官证 5-台湾居民来往大陆通行证 6-港澳居民来往大陆通行证")
    private Integer documentType;

    /** 证件号 */
    @Excel(name = "证件号")
    private String documentNo;

    /** j紧急联系人类型-1-父母 2-子女 3-配偶 4-亲属 5-朋友 6-其他 */
    @Excel(name = "j紧急联系人类型-1-父母 2-子女 3-配偶 4-亲属 5-朋友 6-其他")
    private Integer contactsType;

    /** 联系人 */
    @Excel(name = "联系人")
    private String contacts;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String contactPhone;

    /** 所在省编号 */
    @Excel(name = "所在省编号")
    private String provinceId;

    /** 所在省名称 */
    @Excel(name = "所在省名称")
    private String provinceName;

    /** 所在市编号 */
    @Excel(name = "所在市编号")
    private String cityId;

    /** 所在市名称 */
    @Excel(name = "所在市名称")
    private String cityName;

    /** 所在区编号 */
    @Excel(name = "所在区编号")
    private String areaId;

    /** 所在区名称 */
    @Excel(name = "所在区名称")
    private String areaName;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 推荐类型:1-父母 2-子女 3-配偶 4-亲属 5-朋友 6-其他 */
    @Excel(name = "推荐类型:1-父母 2-子女 3-配偶 4-亲属 5-朋友 6-其他")
    private Integer referenceType;

    /** 推荐人 */
    @Excel(name = "推荐人")
    private String referenceName;

    /** 标签 */
    @Excel(name = "标签")
      private Integer label;

      /** 删除标志（0代表存在 2代表删除） */
      private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 部门id(门店id) */
    @Excel(name = "部门id(门店id)")
    private Long deptId;

    @TableField(exist = false)
    private Integer age;
    @TableField(exist = false)
    private String bindMemberCarNo;
    @TableField(exist = false)
    private Long bindMemberId;
    @TableField(exist = false)
    private String bindMemberLevel;
    @TableField(exist = false)
    private BigDecimal bindMemberDiscount;
    @TableField(exist = false)
    private BigDecimal walletBalance;
    @TableField(exist = false)
    private BigDecimal integralBalance;
    @TableField(exist = false)
    private String deptName;
    @TableField(exist = false)
    private String deptPhone;
    @TableField(exist = false)
    private String userName;
    @TableField(exist = false)
    private Date lastVisitDate;
    @TableField(exist = false)
    private String searchPatient;
    @TableField(exist = false)
    private Integer isRegisterMember;
    @TableField(exist = false)
    private Integer ifNeedMember;
    /** 累计消费 */
    private BigDecimal  accumulatedConsumption;

    @TableField(exist = false)
    private BigDecimal quota;
    @TableField(exist = false)
    private BigDecimal deductionCash;
    @TableField(exist = false)
    private BigDecimal deductionLimit;
    @TableField(exist = false)
    private String patientPhone;
    @TableField(exist = false)
    private String patientInfo;

}
