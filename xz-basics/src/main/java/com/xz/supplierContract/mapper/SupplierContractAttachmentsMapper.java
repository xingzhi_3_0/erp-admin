package com.xz.supplierContract.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.supplierContract.vo.SupplierContractAttachmentsVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.supplierContract.domain.SupplierContractAttachments;

/**
 * 合同附件Mapper接口
 *
 * @author xz
 * @date 2024-02-26
 */
@Mapper
public interface SupplierContractAttachmentsMapper extends BaseMapper<SupplierContractAttachments> {
  /**
   * 查询合同附件
   *
   * @param id 合同附件主键
   * @return 合同附件
   */
  public SupplierContractAttachments selectSupplierContractAttachmentsById(Long id);

  /**
   * 查询合同附件
   *
   * @param contractId 合同主键
   * @return 合同附件
   */
  List<SupplierContractAttachmentsVo> selectByContractId(Long contractId);

  /**
   * 查询合同附件列表
   *
   * @param supplierContractAttachments 合同附件
   * @return 合同附件集合
   */
  public List<SupplierContractAttachments> selectSupplierContractAttachmentsList(SupplierContractAttachments supplierContractAttachments);

  /**
   * 新增合同附件
   *
   * @param supplierContractAttachments 合同附件
   * @return 结果
   */
  public int insertSupplierContractAttachments(SupplierContractAttachments supplierContractAttachments);

  /**
   * 修改合同附件
   *
   * @param supplierContractAttachments 合同附件
   * @return 结果
   */
  public int updateSupplierContractAttachments(SupplierContractAttachments supplierContractAttachments);

  /**
   * 删除合同附件
   *
   * @param id 合同附件主键
   * @return 结果
   */
  public int deleteSupplierContractAttachmentsById(Long id);

  /**
   * 批量删除合同附件
   *
   * @param ids 需要删除的数据主键集合
   * @return 结果
   */
  public int deleteSupplierContractAttachmentsByIds(Long[] ids);


}
