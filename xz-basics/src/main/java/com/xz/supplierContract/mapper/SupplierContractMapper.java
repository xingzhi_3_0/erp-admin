package com.xz.supplierContract.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.supplierContract.vo.SupplierContractDetailVo;
import com.xz.supplierContract.vo.SupplierContractVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.supplierContract.domain.SupplierContract;

/**
 * 合同Mapper接口
 *
 * @author xz
 * @date 2024-02-26
 */
@Mapper
public interface SupplierContractMapper  extends BaseMapper<SupplierContract> {
    /**
     * 查询合同
     *
     * @param id 合同主键
     * @return 合同
     */
    public SupplierContract selectSupplierContractById(Long id);

    /**
     * 查询合同列表
     *
     * @param supplierContract 合同
     * @return 合同集合
     */
    public List<SupplierContract> selectSupplierContractList(SupplierContract supplierContract);
    /**
     * 查询合同
     *
     * @param id 合同主键
     * @return 合同
     */
    public SupplierContractDetailVo selectSupplierContractVoById(Long id);
    /**
     * 查询合同列表
     *
     * @param supplierContract 合同
     * @return 合同集合
     */
    public List<SupplierContractVo> selectSupplierContractVoList(SupplierContract supplierContract);

    /**
     * 新增合同
     *
     * @param supplierContract 合同
     * @return 结果
     */
    public int insertSupplierContract(SupplierContract supplierContract);

    /**
     * 修改合同
     *
     * @param supplierContract 合同
     * @return 结果
     */
    public int updateSupplierContract(SupplierContract supplierContract);

    /**
     * 删除合同
     *
     * @param id 合同主键
     * @return 结果
     */
    public int deleteSupplierContractById(Long id);

    /**
     * 批量删除合同
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSupplierContractByIds(Long[] ids);
}
