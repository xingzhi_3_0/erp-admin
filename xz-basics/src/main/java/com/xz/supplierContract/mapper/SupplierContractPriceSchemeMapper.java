package com.xz.supplierContract.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.supplierContract.vo.SupplierContractAttachmentsVo;
import com.xz.supplierContract.vo.SupplierContractPriceSchemeVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.supplierContract.domain.SupplierContractPriceScheme;

/**
 * 合同价格方案Mapper接口
 *
 * @author xz
 * @date 2024-02-26
 */
@Mapper
public interface SupplierContractPriceSchemeMapper  extends BaseMapper<SupplierContractPriceScheme> {
    /**
     * 查询合同价格方案
     *
     * @param id 合同价格方案主键
     * @return 合同价格方案
     */
    public SupplierContractPriceScheme selectSupplierContractPriceSchemeById(Long id);

    /**
     * 查询合同附件
     *
     * @param contractId 合同主键
     * @return 合同附件
     */
    List<SupplierContractPriceSchemeVo> selectByContractId(Long contractId);
    /**
     * 查询合同价格方案列表
     *
     * @param supplierContractPriceScheme 合同价格方案
     * @return 合同价格方案集合
     */
    public List<SupplierContractPriceScheme> selectSupplierContractPriceSchemeList(SupplierContractPriceScheme supplierContractPriceScheme);

    /**
     * 查询当下合同价格方案
     *
     * @param supplierContractPriceScheme
     * @return 合同价格方案
     */
    public List<SupplierContractPriceSchemeVo> selectByCurrnet(SupplierContractPriceScheme supplierContractPriceScheme);

    /**
     * 新增合同价格方案
     *
     * @param supplierContractPriceScheme 合同价格方案
     * @return 结果
     */
    public int insertSupplierContractPriceScheme(SupplierContractPriceScheme supplierContractPriceScheme);

    /**
     * 修改合同价格方案
     *
     * @param supplierContractPriceScheme 合同价格方案
     * @return 结果
     */
    public int updateSupplierContractPriceScheme(SupplierContractPriceScheme supplierContractPriceScheme);

    /**
     * 删除合同价格方案
     *
     * @param id 合同价格方案主键
     * @return 结果
     */
    public int deleteSupplierContractPriceSchemeById(Long id);

    /**
     * 批量删除合同价格方案
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSupplierContractPriceSchemeByIds(Long[] ids);
}
