package com.xz.supplierContract.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 合同附件对象 t_supplier_contract_attachments
 *
 * @author xz
 * @date 2024-02-26
 */
@Data
public class SupplierContractAttachmentsVo{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private Long id;

    /** 合同ID */
    @Excel(name = "合同ID")
    private Long contractId;

    /** 合同编号 */
    @Excel(name = "合同编号")
    private String contractNo;

    /** 名称 */
    @Excel(name = "名称")
    private String attachmentsName;

    /** 起始日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "起始日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date attachmentsStartTime;

    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "有效期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date attachmentsEndTime;

    /** 附件url */
    @Excel(name = "附件url")
    private String attachmentsUrl;

    /** 系统附件id */
    @Excel(name = "系统附件id")
    private Long attachmentsId;

    /** 附件名称 */
    @Excel(name = "附件名称")
    private String attachmentsRealName;
}
