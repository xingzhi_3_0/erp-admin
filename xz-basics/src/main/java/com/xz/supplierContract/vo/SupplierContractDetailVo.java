package com.xz.supplierContract.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import com.xz.supplierContract.domain.SupplierContractAttachments;
import com.xz.supplierContract.domain.SupplierContractPriceScheme;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * 合同对象 t_supplier_contract
 *
 * @author xz
 * @date 2024-02-26
 */
@Data
public class SupplierContractDetailVo{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private Long id;
    /** 合同编号 */
    private String contractNo;
    /** 合同状态：1待提交,2-执行中,3-已完成,4-已停用 */
    private Integer contractStatus;
    /** 开始日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date contractStartTime;
    /** 终止日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date contractEndTime;
    /** 剩余天数 */
    private Integer contractDayDiff;
    /** 备注 */
    private String remark;


    /** 贸易中间商id */
    private Long supplierId;
    /** 贸易中间商名称 */
    private String supplierName;
    /** 贸易中间商 社会统一代码 */
    private String supplierUnifiedCode;
    /** 贸易中间商 地址 */
    private String supplierAddress;
    /** 贸易中间商 联系人 */
    private String supplierContactPerson;
    /** 贸易中间商 联系方式 */
    private String supplierContactInformation;
    /** 贸易中间商 邮件 */
    private String supplierEmail;
    /** 合同附件集合 */
    private List<SupplierContractAttachmentsVo> attachmentsList;
    /** 合同价格方案集合 */
    private List<SupplierContractPriceSchemeVo>schemeList;
}
