package com.xz.supplierContract.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 合同对象 t_supplier_contract
 *
 * @author xz
 * @date 2024-02-26
 */
@Data
public class SupplierContractVo{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private Long id;

    /** 合同状态：1待提交,2-执行中,3-已完成,4-已停用 */
    @Excel(name = "合同状态：1待提交,2-执行中,3-已完成,4-已停用")
    private Integer contractStatus;

    /** 合同编号 */
    @Excel(name = "合同编号")
    private String contractNo;

    /** 剩余天数 */
    @Excel(name = "剩余天数")
    private Integer contractDayDiff;

    /** 客户名称 */
    @Excel(name = "客户名称")
    private String supplierName;

    /** 合同文件数量 */
    @Excel(name = "合同文件数量")
    private Integer contractAttachmentsCount;

    /** 合同方案数量 */
    @Excel(name = "合同方案数量")
    private Integer contractPriceSchemeCount;

    /** 生效时间 */
    @Excel(name = "生效时间")
    private String contractDate;

    public Integer getContractDayDiff() {
      if(null != contractDayDiff && 0 < contractDayDiff.intValue()){
        return contractDayDiff;
      }
      return 0;
    }
}
