package com.xz.supplierContract.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 合同价格方案对象 t_supplier_contract_price_scheme
 *
 * @author xz
 * @date 2024-02-26
 */
@Data
public class SupplierContractPriceSchemeVo{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private Long id;

    /** 合同ID */
    private Long contractId;

    /** 合同编号 */
    private String contractNo;

    /** 贸易中间商ID */
    private Long supplierId;

    /** 商品ID */
    private Long productId;

    /** 商品名称 */
    private String productName;

    /** 商品名称 */
    private String productCategory;

    /** 商品单位 */
    private String productUnit;

    /** 商品售价 */
    private BigDecimal productPrice;

    /** 合同单价 */
    private BigDecimal contractPrice;

}
