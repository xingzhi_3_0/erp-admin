package com.xz.supplierContract.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.supplierContract.domain.SupplierContract;
import com.xz.supplierContract.dto.SupplierContractDto;
import com.xz.supplierContract.vo.SupplierContractDetailVo;
import com.xz.supplierContract.vo.SupplierContractVo;

/**
 * 合同Service接口
 *
 * @author xz
 * @date 2024-02-26
 */
public interface ISupplierContractService extends IService<SupplierContract> {

  /**
   * 查询合同
   * @param id
   * @return
   */
  public SupplierContractDetailVo selectSupplierContractVoById(Long id);

  /**
   * 查询贸易供应商当下合同
   *
   * @param supplierId 贸易中间商主键
   * @return 合同
   */
  public SupplierContract selectCurrentBySupplierId(Long supplierId);

  /**
   * 查询合同列表
   *
   * @param supplierContract 合同
   * @return 合同
   */
  public List<SupplierContractVo> selectSupplierContractVoList(SupplierContract supplierContract);

  /**
   * 修改状态
   * @param contractId
   * @param contractStatus
   * @return
   */
  boolean editStatus(Long contractId, Integer contractStatus);

  /**
   * 新增合同
   *
   * @param supplierContract 合同
   * @return 结果
   */
  public int insertSupplierContract(SupplierContractDto supplierContract);

  /**
   * 修改合同
   *
   * @param supplierContract 合同
   * @return 结果
   */
  public int updateSupplierContract(SupplierContractDto supplierContract);

  /**
   * 批量删除合同
   *
   * @param ids 需要删除的合同主键集合
   * @return 结果
   */
  public boolean deleteSupplierContractByIds(Long[] ids);
}
