package com.xz.supplierContract.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.supplierContract.domain.SupplierContractAttachments;
import com.xz.supplierContract.vo.SupplierContractAttachmentsVo;

/**
 * 合同附件Service接口
 *
 * @author xz
 * @date 2024-02-26
 */
public interface ISupplierContractAttachmentsService extends IService<SupplierContractAttachments> {
  /**
   * 查询合同附件
   *
   * @param id 合同附件主键
   * @return 合同附件
   */
  public SupplierContractAttachments selectSupplierContractAttachmentsById(Long id);

  /**
   * 查询合同附件列表
   *
   * @param supplierContractAttachments 合同附件
   * @return 合同附件集合
   */
  public List<SupplierContractAttachments> selectSupplierContractAttachmentsList(SupplierContractAttachments supplierContractAttachments);

  /**
   * 估计合同ID查询合同附件列表
   * @param contractId
   * @return
   */
  List<SupplierContractAttachmentsVo> selectByContractId(Long contractId);

  /**
   * 新增合同附件
   * @param contractId 合同id
   * @param contractNo 合同编号
   * @param attachmentsList 合同附件集合
   */
  void insertForContract(Long contractId, String contractNo, List<SupplierContractAttachments> attachmentsList);

  /**
   * 更新合同附件
   * @param contractId 合同id
   * @param contractNo 合同编号
   * @param attachmentsList 合同附件集合
   */
  public void updateForContract(Long contractId, String contractNo, List<SupplierContractAttachments> attachmentsList);

  /**
   * 批量删除合同附件
   *
   * @param ids 需要删除的合同附件主键集合
   * @return 结果
   */
  public int deleteSupplierContractAttachmentsByIds(Long[] ids);

  /**
   * 删除合同附件信息
   *
   * @param id 合同附件主键
   * @return 结果
   */
  public int deleteSupplierContractAttachmentsById(Long id);
}
