package com.xz.supplierContract.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.core.domain.model.LoginUser;
import com.xz.common.utils.DateUtils;
import com.xz.common.utils.SecurityUtils;
import com.xz.supplierContract.domain.SupplierContractAttachments;
import com.xz.supplierContract.domain.SupplierContractPriceScheme;
import com.xz.supplierContract.mapper.SupplierContractAttachmentsMapper;
import com.xz.supplierContract.service.ISupplierContractAttachmentsService;
import com.xz.supplierContract.vo.SupplierContractAttachmentsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 合同附件Service业务层处理
 *
 * @author xz
 * @date 2024-02-26
 */
@Service
public class SupplierContractAttachmentsServiceImpl extends ServiceImpl<SupplierContractAttachmentsMapper, SupplierContractAttachments> implements ISupplierContractAttachmentsService {
  @Autowired
  private SupplierContractAttachmentsMapper supplierContractAttachmentsMapper;

  /**
   * 查询合同附件
   *
   * @param id 合同附件主键
   * @return 合同附件
   */
  @Override
  public SupplierContractAttachments selectSupplierContractAttachmentsById(Long id) {
    return supplierContractAttachmentsMapper.selectSupplierContractAttachmentsById(id);
  }

  /**
   * 查询合同附件列表
   *
   * @param supplierContractAttachments 合同附件
   * @return 合同附件
   */
  @Override
  public List<SupplierContractAttachments> selectSupplierContractAttachmentsList(SupplierContractAttachments supplierContractAttachments) {
    return supplierContractAttachmentsMapper.selectSupplierContractAttachmentsList(supplierContractAttachments);
  }

  /**
   * 估计合同ID查询合同附件列表
   *
   * @param contractId 合同ID
   * @return 合同附件
   */
  @Override
  public List<SupplierContractAttachmentsVo> selectByContractId(Long contractId) {
    return supplierContractAttachmentsMapper.selectByContractId(contractId);
  }

  /**
   * 新增合同附件
   * @param contractId 合同id
   * @param contractNo 合同编号
   * @param attachmentsList 合同附件集合
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public void insertForContract(Long contractId, String contractNo, List<SupplierContractAttachments> attachmentsList) {

    if (!CollectionUtils.isEmpty(attachmentsList)) {
      LoginUser user = SecurityUtils.getLoginUser();
      attachmentsList.forEach(data -> {
        data.setContractId(contractId);
        data.setContractNo(contractNo);
        data.setCreateBy(user.getUserId());
        data.setCreateTime(DateUtils.getNowDate());
        data.setTenantId(user.getTenantId());
        data.setDeptId(user.getDeptId());
        supplierContractAttachmentsMapper.insert(data);
      });
    }
  }

  /**
   * 更新合同附件
   * @param contractId 合同id
   * @param contractNo 合同编号
   * @param attachmentsList 合同附件集合
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public void updateForContract(Long contractId,String contractNo, List<SupplierContractAttachments> attachmentsList) {
    List<SupplierContractAttachments> addList = null;
    List<SupplierContractAttachments> updateList = null;
    if (!CollectionUtils.isEmpty(attachmentsList)) {
      addList = attachmentsList.stream().filter(a -> null == a.getId()).collect(Collectors.toList());
       updateList = attachmentsList.stream().filter(a -> null != a.getId()).collect(Collectors.toList());
    }

    // 更新
    List<Long> updateId = null;
    if (!CollectionUtils.isEmpty(updateList)) {
      LoginUser user = SecurityUtils.getLoginUser();
      updateList.forEach(data -> {
        data.setUpdateBy(user.getUserId());
        data.setUpdateTime(DateUtils.getNowDate());
        supplierContractAttachmentsMapper.updateSupplierContractAttachments(data);
      });
      updateId = updateList.stream().map(SupplierContractAttachments::getId).collect(Collectors.toList());
    }
    // 删除不在此次更新内的数据
    supplierContractAttachmentsMapper.delete(
      new LambdaQueryWrapper<SupplierContractAttachments>()
        .eq(SupplierContractAttachments::getContractId, contractId)
        .notIn(!CollectionUtils.isEmpty(updateId), SupplierContractAttachments::getId, updateId)
    );

    if (!CollectionUtils.isEmpty(addList)) {
      LoginUser user = SecurityUtils.getLoginUser();
      addList.forEach(data -> {
        data.setContractId(contractId);
        data.setContractNo(contractNo);
        data.setCreateBy(user.getUserId());
        data.setCreateTime(DateUtils.getNowDate());
        data.setTenantId(user.getTenantId());
        data.setDeptId(user.getDeptId());
        supplierContractAttachmentsMapper.insert(data);
      });
    }
  }


  /**
   * 批量删除合同附件
   *
   * @param ids 需要删除的合同附件主键
   * @return 结果
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public int deleteSupplierContractAttachmentsByIds(Long[] ids) {
    return supplierContractAttachmentsMapper.deleteSupplierContractAttachmentsByIds(ids);
  }

  /**
   * 删除合同附件信息
   *
   * @param id 合同附件主键
   * @return 结果
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public int deleteSupplierContractAttachmentsById(Long id) {
    return supplierContractAttachmentsMapper.deleteSupplierContractAttachmentsById(id);
  }
}
