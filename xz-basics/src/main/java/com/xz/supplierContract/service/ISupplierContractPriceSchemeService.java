package com.xz.supplierContract.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.supplierContract.domain.SupplierContract;
import com.xz.supplierContract.domain.SupplierContractPriceScheme;
import com.xz.supplierContract.vo.SupplierContractPriceSchemeVo;

/**
 * 合同价格方案Service接口
 *
 * @author xz
 * @date 2024-02-26
 */
public interface ISupplierContractPriceSchemeService extends IService<SupplierContractPriceScheme> {
  /**
   * 查询合同价格方案
   *
   * @param id 合同价格方案主键
   * @return 合同价格方案
   */
  public SupplierContractPriceScheme selectSupplierContractPriceSchemeById(Long id);

  /**
   * 估计合同ID查询合同价格方案
   *
   * @param contractId
   * @return
   */
  List<SupplierContractPriceSchemeVo> selectByContractId(Long contractId);

  /**
   * 查询当下合同价格方案
   *
   * @param supplierId
   * @return
   */
  Map<Long,SupplierContractPriceSchemeVo> selectByCurrnet(Long supplierId);

  /**
   * 查询合同价格方案列表
   *
   * @param supplierContractPriceScheme 合同价格方案
   * @return 合同价格方案集合
   */
  public List<SupplierContractPriceScheme> selectSupplierContractPriceSchemeList(SupplierContractPriceScheme supplierContractPriceScheme);


  /**
   * 新增合同价格方案
   *
   * @param supplierContract 合同
   * @param schemeList       合同价格方案集合
   */
  public void insertForContract(SupplierContract supplierContract, List<SupplierContractPriceScheme> schemeList);

  /**
   * 更新合同价格方案
   *
   * @param supplierContract 合同
   * @param schemeList       合同价格方案集合
   */
  public void updateForContract(SupplierContract supplierContract, List<SupplierContractPriceScheme> schemeList);


  /**
   * 批量删除合同价格方案
   *
   * @param ids 需要删除的合同价格方案主键集合
   * @return 结果
   */
  public int deleteSupplierContractPriceSchemeByIds(Long[] ids);

  /**
   * 删除合同价格方案信息
   *
   * @param id 合同价格方案主键
   * @return 结果
   */
  public int deleteSupplierContractPriceSchemeById(Long id);


}
