package com.xz.supplierContract.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 合同附件对象 t_supplier_contract_attachments
 *
 * @author xz
 * @date 2024-02-26
 */
@Data
@TableName("t_supplier_contract_attachments")
public class SupplierContractAttachments extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private Long id;

    /** 合同ID */
    @Excel(name = "合同ID")
    private Long contractId;

    /** 合同编号 */
    @Excel(name = "合同编号")
    private String contractNo;

    /** 名称 */
    @Excel(name = "名称")
    @NotBlank(message = "文件名称不能为空")
    private String attachmentsName;

    /** 起始日期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "起始日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date attachmentsStartTime;

    /** 有效期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "有效期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date attachmentsEndTime;

    /** 附件url */
    @Excel(name = "附件url")
    @NotBlank(message = "合同文件不能为空")
    private String attachmentsUrl;

    /** 系统附件id */
    @Excel(name = "系统附件id")
    @NotNull(message = "合同文件不能为空")
    private Long attachmentsId;

    /** 附件名称 */
    @Excel(name = "附件名称")
    @NotBlank(message = "合同文件不能为空")
    private String attachmentsRealName;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    @NotNull(message = "租户id不能为空")
    private Long tenantId;

    /** 部门id(门店id) */
    @Excel(name = "部门id(门店id)")
    @NotNull(message = "部门id(门店id)不能为空")
    private Long deptId;

}
