package com.xz.supplierContract.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 合同价格方案对象 t_supplier_contract_price_scheme
 *
 * @author xz
 * @date 2024-02-26
 */
@Data
@TableName("t_supplier_contract_price_scheme")
public class SupplierContractPriceScheme extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private Long id;

    /** 合同ID */
    @Excel(name = "合同ID")
    @NotNull(message = "合同ID不能为空")
    private Long contractId;

    /** 合同编号 */
    @Excel(name = "合同编号")
    @NotNull(message = "合同编号不能为空")
    private String contractNo;

    /** 贸易中间商ID */
    @Excel(name = "贸易中间商ID")
    @NotNull(message = "贸易中间商ID不能为空")
    private Long supplierId;

    /** 商品ID */
    @Excel(name = "商品ID")
    private Long productId;

    /** 合同单价 */
    @Excel(name = "合同单价")
    private BigDecimal contractPrice;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 部门id(门店id) */
    @Excel(name = "部门id(门店id)")
    private Long deptId;

    @TableField(exist = false)
    private Date currentContractDate;
}
