package com.xz.supplierContract.domain;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 合同对象 t_supplier_contract
 *
 * @author xz
 * @date 2024-02-26
 */
@Data
@TableName("t_supplier_contract")
public class SupplierContract extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private Long id;

    /** 合同编号 */
    @Excel(name = "合同编号")
    private String contractNo;

    /** 开始日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date contractStartTime;

    /** 终止日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "终止日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date contractEndTime;

    /** 合同状态：1待提交,2-执行中,3-已完成,4-已停用 */
    @Excel(name = "合同状态：1待提交,2-执行中,3-已完成,4-已停用")
    private Integer contractStatus;

    /** 贸易中间商id */
    @Excel(name = "贸易中间商id")
    private Long supplierId;

    /** 合同文件数量 */
    @Excel(name = "合同文件数量")
    private Integer contractAttachmentsCount;

    /** 合同方案数量 */
    @Excel(name = "合同方案数量")
    private Integer contractPriceSchemeCount;

    /** 提交时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "提交时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date submitTime;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    @NotNull(message = "租户id不能为空")
    private Long tenantId;

    /** 部门id(门店id) */
    @Excel(name = "部门id(门店id)")
    @NotNull(message = "部门id(门店id)不能为空")
    private Long deptId;


    /** 贸易中间商名称 */
    @TableField(exist = false)
    private String supplierName;

    @TableField(exist = false)
    private Date currentDate;

    @TableField(exist = false)
    private List<String> statusList;

    @TableField(exist = false)
    private boolean adoptingContractPrice;
}
