package com.xz.supplierContract.dto;

import com.xz.common.annotation.Excel;
import com.xz.supplierContract.domain.SupplierContractAttachments;
import com.xz.supplierContract.domain.SupplierContractPriceScheme;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * 合同对象 t_supplier_contract
 *
 * @author xz
 * @date 2024-02-26
 */
@Data
public class SupplierContractDto{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private Long id;

    /** 合同编号 */
    @Excel(name = "合同编号")
    @NotBlank(message = "合同编号不能为空")
    @Length(max = 64,message = "合同编号最多{max}个字符")
    private String contractNo;

    /** 开始日期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "开始日期不能为空")
    private Date contractStartTime;

    /** 终止日期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "终止日期不能为空")
    private Date contractEndTime;

    /** 合同状态：1待提交,2-执行中,3-已完成,4-已停用 */
    @NotNull(message = "状态不能为空")
    private Integer contractStatus;

    /** 贸易中间商id */
    @NotNull(message = "客户不能为空")
    private Long supplierId;

    /** 备注 */
    @Length(max = 200,message = "备注最多{max}个字符")
    private String remark;

    /** 合同附件集合 */
    private List<SupplierContractAttachments> attachmentsList;

    /** 合同价格方案集合 */
    @NotEmpty(message = "价格方案不能为空")
    private List<SupplierContractPriceScheme>  schemeList;

    public int getAttachmentsListSize() {
      if(null == attachmentsList){
        return 0;
      }
      return attachmentsList.size();
    }

    public int getSchemeListSize() {
      if(null == schemeList){
        return 0;
      }
      return schemeList.size();
    }
}
