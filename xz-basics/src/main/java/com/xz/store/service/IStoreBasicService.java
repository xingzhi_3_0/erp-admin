package com.xz.store.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.annotation.DataScope;
import com.xz.common.core.domain.AjaxResult;
import com.xz.store.Vo.*;
import com.xz.store.domain.StoreBasic;
import com.xz.store.dto.OperatingCityDto;
import com.xz.system.domain.vo.DictDataVo;

import java.util.List;

/**
 * 门店基本信息Service接口
 * 
 * @author xz
 * @date 2022-03-01
 */
public interface IStoreBasicService extends IService<StoreBasic> {
    /**
     * 查询门店基本信息
     * 
     * @param storeId 门店基本信息主键
     * @return 门店基本信息
     */
    public StoreBasic selectStoreBasicByStoreId(String storeId);

    /**
     * 查询门店基本信息列表
     * 
     * @param storeBasic 门店基本信息
     * @return 门店基本信息集合
     */
    public List<StoreBasic> selectStoreBasicList(StoreBasic storeBasic);
    /**
     * 门店列表（其他模块调用）
     * @param storeBasic
     * @return
     */
    List<StoreBasicListVo> storeBasicList(StoreBasic storeBasic);
    /**
     * 新增门店基本信息
     * 
     * @param storeBasic 门店基本信息
     * @return 结果
     */
    public int insertStoreBasic(StoreBasic storeBasic);

    /**
     * 修改门店基本信息
     * 
     * @param storeBasic 门店基本信息
     * @return 结果
     */
    public int updateStoreBasic(StoreBasic storeBasic);

    /**
     * 批量删除门店基本信息
     * 
     * @param storeIds 需要删除的门店基本信息主键集合
     * @return 结果
     */
    public int deleteStoreBasicByStoreIds(String[] storeIds);

    /**
     * 删除门店基本信息信息
     * 
     * @param storeId 门店基本信息主键
     * @return 结果
     */
    public AjaxResult deleteStoreBasicByStoreId(String storeId);

    /**
     * 启用/停用
     * @author ZhangZhiJia
     * @date 2022/3/2 14:56
     * @param storeBasic
     * @return int
     */
    int editAvailable(StoreBasic storeBasic);
    /**
     * 运营城市下门店
     * @param operatingCityDto
     * @return
     */
    List<ClientAreaListVo> clientStoreList(OperatingCityDto operatingCityDto);
    /**
     * 门店详情
     * @param operatingCityDto
     * @return
     */
    AjaxResult clientStoreInfo(OperatingCityDto operatingCityDto);
    /**
     * 运营城市下门店--搜索门店
     * @param operatingCityDto
     * @return
     */
    List<ClientStoreListVo> searchClientStoreList(OperatingCityDto operatingCityDto);
    /**
     * 门店下车型运营分类
     * @param operatingCityDto
     * @return
     */
    AjaxResult classificationList(OperatingCityDto operatingCityDto);
    /**
     * 适用门店列表
     * @param tenantId
     * @return
     */
    List<ApplicableUnitListVo> applicableUnitList(Long tenantId);

    int selectCountByCityId(Long[] ids);

    int selectAvailableByCityId(Long cityId);

    String checkNameUnique(StoreBasic storeBasic);
    /**
     * 异店还车门店列表
     * @return
     */
    @DataScope(deptAlias = "d")
    List<AlsoFeeStoreVo> alsoFeeStoreList();

    List<StoreBasic> selectAllListStore(StoreBasic storeBasic);
    //租户门店
    List<DictDataVo> tenantStoreList(StoreBasic storeBasic);

    String selectCurrUserStore(Long deptId);

    List<DictDataVo> selectSeries(String storeId);
    List<DictDataVo> selectModel(String storeId,String seriesId);
    /**
     * 关联企业认证，先删后加
     * @param associationEnterpriseCertificationVO 关联企业认证VO
     * @return
     * @author renyong on 2023/7/18 16:06
     */
    void associationEnterpriseCertification(AssociationEnterpriseCertificationVO associationEnterpriseCertificationVO);
}
