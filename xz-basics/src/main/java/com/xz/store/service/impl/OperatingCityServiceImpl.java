package com.xz.store.service.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.xz.common.core.domain.AjaxResult;
import com.xz.common.exception.ServiceException;
import com.xz.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.utils.SecurityUtils;
import com.xz.store.Vo.ClientOperatingCityVo;
import com.xz.store.dto.OperatingCityDto;
import com.xz.store.service.IStoreBasicService;
import common.PinyinUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.store.mapper.OperatingCityMapper;
import com.xz.store.domain.OperatingCity;
import com.xz.store.service.IOperatingCityService;

/**
 * 运营城市Service业务层处理
 *
 * @author xz
 * @date 2022-03-08
 */
@Service
public class OperatingCityServiceImpl extends ServiceImpl<OperatingCityMapper, OperatingCity> implements IOperatingCityService {
    @Autowired
    private OperatingCityMapper operatingCityMapper;
    @Autowired
    private IStoreBasicService storeBasicService;

    /**
     * 查询运营城市
     *
     * @param id 运营城市主键
     * @return 运营城市
     */
    @Override
    public OperatingCity selectOperatingCityById(Long id) {
        return operatingCityMapper.selectOperatingCityById(id);
    }

    /**
     * 查询运营城市列表
     *
     * @param operatingCity 运营城市
     * @return 运营城市
     */
    @Override
    public List<OperatingCity> selectOperatingCityList(OperatingCity operatingCity) {
        return operatingCityMapper.selectOperatingCityList(operatingCity);
    }

    /**
     * 新增运营城市
     *
     * @param operatingCity 运营城市
     * @return 结果
     */
    @Override
    public AjaxResult insertOperatingCity(OperatingCity operatingCity) {
        operatingCity.setCreateTime(DateUtils.getNowDate());
        int size = operatingCityMapper.selectOperatingCityByCode(operatingCity);
        if (size > 0) {
            return AjaxResult.error("运营城市已存在");
        } else {
            operatingCity.setIsAvailable(1);
            operatingCity.setProvincePinyin(PinyinUtils.getPinyinFirst(operatingCity.getProvinceName()));
            operatingCity.setCityPinyin(PinyinUtils.getPinyinFirst(operatingCity.getCityName()));
            int rows = operatingCityMapper.insertOperatingCity(operatingCity);
            return rows > 0 ? AjaxResult.success() : AjaxResult.error();
        }

    }

    /**
     * 修改运营城市
     *
     * @param operatingCity 运营城市
     * @return 结果
     */
    @Override
    public AjaxResult updateOperatingCity(OperatingCity operatingCity) {
        operatingCity.setUpdateTime(DateUtils.getNowDate());
        int size = operatingCityMapper.selectOperatingCityByCode(operatingCity);
        if (size > 0) {
            return AjaxResult.error("运营城市已存在");
        } else {
            operatingCity.setTenantId(SecurityUtils.getLoginUser().getTenantId());
            operatingCity.setProvincePinyin(PinyinUtils.getPinyinFirst(operatingCity.getProvinceName()));
            operatingCity.setCityPinyin(PinyinUtils.getPinyinFirst(operatingCity.getCityName()));
            int rows = operatingCityMapper.updateOperatingCity(operatingCity);
            ;
            return rows > 0 ? AjaxResult.success() : AjaxResult.error();
        }
    }

    /**
     * 批量删除运营城市
     *
     * @param ids 需要删除的运营城市主键
     * @return 结果
     */
    @Override
    public int deleteOperatingCityByIds(Long[] ids) {
        int count = storeBasicService.selectCountByCityId(ids);
        if (count > 0) {
            throw new ServiceException("请先删除城市下的门店!");
        }
        return operatingCityMapper.deleteOperatingCityByIds(ids);
    }

    /**
     * 删除运营城市信息
     *
     * @param id 运营城市主键
     * @return 结果
     */
    @Override
    public int deleteOperatingCityById(Long id) {
        return operatingCityMapper.deleteOperatingCityById(id);
    }

    @Override
    public int editAvailable(OperatingCity operatingCity) {
        if (operatingCity.getIsAvailable() == 0) {
            int count = storeBasicService.selectAvailableByCityId(operatingCity.getId());
            if (count > 0) {
                throw new ServiceException("请先停用城市下的门店!");
            }
        }
        return operatingCityMapper.editAvailable(operatingCity);
    }

    @Override
    public HashMap<String, Object> getCode(OperatingCity operatingCity) {
        operatingCity.setIsAvailable(1);
        List<OperatingCity> list = operatingCityMapper.selectOperatingCityList(operatingCity);
        Set<String> provinceIds = new HashSet<>();
        Set<String> cityIds = new HashSet<>();
        for (OperatingCity item : list) {
            provinceIds.add(item.getProvinceId());
            cityIds.add(item.getCityId());
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("provinceIds", provinceIds);
        map.put("cityIds", cityIds);
        return map;
    }

    //小程序运营城市
    public AjaxResult clientOperatingCity(OperatingCityDto operatingCityDto) {
        List<ClientOperatingCityVo> operatingCityVoList = operatingCityMapper.clientOperatingCity(operatingCityDto);
        //operatingCityVoList.stream().collect(Collectors.groupingBy(ClientOperatingCityVo::getCode))
        return AjaxResult.success(operatingCityVoList);
    }
}
