package com.xz.store.service;

import java.util.HashMap;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.store.Vo.ClientOperatingCityVo;
import com.xz.store.domain.OperatingCity;
import com.xz.store.dto.OperatingCityDto;

/**
 * 运营城市Service接口
 * 
 * @author xz
 * @date 2022-03-08
 */
public interface IOperatingCityService extends IService<OperatingCity> {
    /**
     * 查询运营城市
     * 
     * @param id 运营城市主键
     * @return 运营城市
     */
    public OperatingCity selectOperatingCityById(Long id);

    /**
     * 查询运营城市列表
     * 
     * @param operatingCity 运营城市
     * @return 运营城市集合
     */
    public List<OperatingCity> selectOperatingCityList(OperatingCity operatingCity);

    /**
     * 新增运营城市
     * 
     * @param operatingCity 运营城市
     * @return 结果
     */
    public AjaxResult insertOperatingCity(OperatingCity operatingCity);

    /**
     * 修改运营城市
     * 
     * @param operatingCity 运营城市
     * @return 结果
     */
    public AjaxResult updateOperatingCity(OperatingCity operatingCity);

    /**
     * 批量删除运营城市
     * 
     * @param ids 需要删除的运营城市主键集合
     * @return 结果
     */
    public int deleteOperatingCityByIds(Long[] ids);

    /**
     * 删除运营城市信息
     * 
     * @param id 运营城市主键
     * @return 结果
     */
    public int deleteOperatingCityById(Long id);

    int editAvailable(OperatingCity operatingCity);

    HashMap<String, Object> getCode(OperatingCity operatingCity);
    //小程序运营城市
    AjaxResult clientOperatingCity(OperatingCityDto operatingCityDto);
}
