package com.xz.store.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.store.Vo.*;
import com.xz.store.dto.OperatingCityDto;
import com.xz.system.domain.vo.DictDataVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.store.domain.StoreBasic;
import org.apache.ibatis.annotations.Param;

/**
 * 门店基本信息Mapper接口
 * 
 * @author xz
 * @date 2022-03-01
 */
@Mapper
public interface StoreBasicMapper  extends BaseMapper<StoreBasic> {
    /**
     * 查询门店基本信息
     * 
     * @param storeId 门店基本信息主键
     * @return 门店基本信息
     */
    public StoreBasic selectStoreBasicByStoreId(String storeId);
    StoreBasic selectStoreBasic(String storeName);

    /**
     * 查询门店基本信息列表
     * 
     * @param storeBasic 门店基本信息
     * @return 门店基本信息集合
     */
    public List<StoreBasic> selectStoreBasicList(StoreBasic storeBasic);

    /**
     * 门店列表（其他模块调用）
     * @param storeBasic
     * @return
     */
    List<StoreBasicListVo> storeBasicList(StoreBasic storeBasic);

    /**
     * 新增门店基本信息
     * 
     * @param storeBasic 门店基本信息
     * @return 结果
     */
    public int insertStoreBasic(StoreBasic storeBasic);

    /**
     * 修改门店基本信息
     * 
     * @param storeBasic 门店基本信息
     * @return 结果
     */
    public int updateStoreBasic(StoreBasic storeBasic);

    /**
     * 删除门店基本信息
     * 
     * @param storeId 门店基本信息主键
     * @return 结果
     */
    public int deleteStoreBasicByStoreId(String storeId);

    /**
     * 批量删除门店基本信息
     * 
     * @param storeIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStoreBasicByStoreIds(String[] storeIds);

    int editAvailable(StoreBasic storeBasic);


    /**
     * 运营城市下门店
     * @param operatingCityDto
     * @return
     */
    List<ClientAreaListVo> clientStoreList(OperatingCityDto operatingCityDto);

    /**
     * 门店详情
     * @param operatingCityDto
     * @return
     */
    ClientStoreInfoVo clientStoreInfo(OperatingCityDto operatingCityDto);
    /**
     * 运营城市下门店--搜索
     * @param operatingCityDto
     * @return
     */
    List<ClientStoreListVo> searchClientStoreList(OperatingCityDto operatingCityDto);

    /**
     * 门店下车型运营分类 短租
     * @param operatingCityDto
     * @return
     */
    List<ClassificationListVo> classificationList(OperatingCityDto operatingCityDto);
    /**
     * 门店下车型运营分类 长租
     * @param operatingCityDto
     * @return
     */
    List<ClassificationListVo> longClassificationList(OperatingCityDto operatingCityDto);

    /**
     * 适用门店列表
     * @param tenantId
     * @return
     */
    List<ApplicableUnitListVo> applicableUnitList(Long tenantId);

    int selectCountByCityId(Long[] ids);

    int selectAvailableByCityId(Long cityId);

    StoreBasic checkUnique(String storeName);

    /**
     * 异店还车门店列表
     * @return
     */
    List<AlsoFeeStoreVo> alsoFeeStoreList();

    //租户门店
    List<DictDataVo> tenantStoreList(StoreBasic storeBasic);

    String selectCurrUserStore(Long deptId);

    List<DictDataVo> selectSeries(String storeId);
    List<DictDataVo> selectModel(@Param("storeId") String storeId,@Param("seriesId") String seriesId);
}
