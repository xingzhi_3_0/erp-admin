package com.xz.store.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.store.Vo.ClientOperatingCityVo;
import com.xz.store.dto.OperatingCityDto;
import org.apache.ibatis.annotations.Mapper;
import com.xz.store.domain.OperatingCity;

/**
 * 运营城市Mapper接口
 * 
 * @author xz
 * @date 2022-03-08
 */
@Mapper
public interface OperatingCityMapper  extends BaseMapper<OperatingCity> {
    /**
     * 查询运营城市
     * 
     * @param id 运营城市主键
     * @return 运营城市
     */
    public OperatingCity selectOperatingCityById(Long id);

    /**
     * 查询运营城市列表
     * 
     * @param operatingCity 运营城市
     * @return 运营城市集合
     */
    public List<OperatingCity> selectOperatingCityList(OperatingCity operatingCity);

    /**
     * 新增运营城市
     * 
     * @param operatingCity 运营城市
     * @return 结果
     */
    public int insertOperatingCity(OperatingCity operatingCity);

    /**
     * 修改运营城市
     * 
     * @param operatingCity 运营城市
     * @return 结果
     */
    public int updateOperatingCity(OperatingCity operatingCity);

    /**
     * 删除运营城市
     * 
     * @param id 运营城市主键
     * @return 结果
     */
    public int deleteOperatingCityById(Long id);

    /**
     * 批量删除运营城市
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOperatingCityByIds(Long[] ids);

    int editAvailable(OperatingCity operatingCity);

    int selectOperatingCityByCode(OperatingCity operatingCity);
    //小程序运营城市
    List<ClientOperatingCityVo> clientOperatingCity(OperatingCityDto operatingCityDto);
}
