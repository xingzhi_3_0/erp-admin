package com.xz.store.Vo;

import lombok.Data;

import java.util.List;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2022/3/10 11:35
 * @description：运营城市下区域
 */
@Data
public class ClientAreaListVo {
    //区域id
    private String areaId;
    //区域名称
    private String areaName;
    //门店信息
    List<ClientStoreListVo> clientStoreListVo;
}
