package com.xz.store.Vo;

import lombok.Data;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2022/3/10 13:53
 * @description：门店下车型分类
 */
@Data
public class ClassificationListVo {
    //分类名称
    private String operatingClassification;
    //分类类型
    private String classification;
    //最低价格
    private Double minPrice;
}
