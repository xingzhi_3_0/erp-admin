package com.xz.store.Vo;

import lombok.Data;

import java.util.Date;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2023/5/11 11:33
 * @description：占用车辆
 */
@Data
public class OccupyCarNumVo {
    private Date actualEndTime;
    private Date actualStartTime;
    private String  carId;
}
