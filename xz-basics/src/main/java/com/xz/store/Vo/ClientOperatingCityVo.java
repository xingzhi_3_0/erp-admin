package com.xz.store.Vo;

import lombok.Data;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2022/3/9 18:25
 * @description：运营城市
 */
@Data
public class ClientOperatingCityVo {
    /** 所在市编号 */
    private String cityId;

    /** 所在市名称 */
    private String cityName;
    //市首字母
    private String code;
}
