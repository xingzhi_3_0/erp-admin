package com.xz.store.Vo;

import lombok.Data;

import java.util.Collections;
import java.util.List;

/**
 * @author renyong
 * @date 2023/7/18 16:28
 * @description 关联企业认证
 */
@Data
public class AssociationEnterpriseCertificationVO {
    /**
     * 企业认证id
     */
    private Long enterpriseCertificationId;
    /**
     * 门店id列表
     */
    private List<String> storeIdList;

    public AssociationEnterpriseCertificationVO() {
        this.storeIdList = Collections.EMPTY_LIST;
    }
}
