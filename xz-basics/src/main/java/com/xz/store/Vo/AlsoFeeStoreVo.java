package com.xz.store.Vo;

import lombok.Data;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2022/3/22 19:26
 * @description：异店还车门店列表
 */
@Data
public class AlsoFeeStoreVo {
    //门店id
    private String storeId;
    //门店列表
    private String storeName;
}
