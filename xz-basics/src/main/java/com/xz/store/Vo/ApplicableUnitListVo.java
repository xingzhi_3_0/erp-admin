package com.xz.store.Vo;

import lombok.Data;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2022/3/15 16:56
 * @description：适用门店列表
 */
@Data
public class ApplicableUnitListVo {
    //门店id
    private String applicableUnitNo;
    //门店名称
    private String applicableUnit;
    //联系电话
    private String unitPhone;
    //经度
    private Double unitLon;
    //纬度
    private Double unitLat;
    //门店地址
    private String unitAddress;
}
