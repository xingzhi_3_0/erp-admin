package com.xz.store.Vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2022/3/5 9:26
 * @description：门店列表
 */
@Data
public class StoreBasicListVo {
    //门店id
    private String storeId;

    /** 门店名称*/
    private String storeName;

    /** 门店地址 */
    private String addrDetail;

}
