package com.xz.store.Vo;

import lombok.Data;

import java.util.Date;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2023/5/11 11:33
 * @description：占用车辆
 */
@Data
public class CarOccupyNumVo {
    private String carId;
    private String modelId;
    private String carNo;
    private String source;
    private Integer orderNum;
    private Integer distributeNum;
    private Integer otaNum;
    private Integer secondmentNum;
    private Integer secondedNum;
    private Integer onoffNum;


    private Integer occupyNum;

    public Integer getOccupyNum() {
        return orderNum+distributeNum+otaNum+secondmentNum+secondedNum+onoffNum;
    }
}
