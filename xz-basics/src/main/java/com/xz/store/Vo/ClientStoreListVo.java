package com.xz.store.Vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2022/3/9 19:27
 * @description：小程序门店
 */
@Data
public class ClientStoreListVo {
    //门店id
    private String storeId;
    //门店名称
    private String storeName;
    //门店服务类型 1人工服务,2自助取还
    private String serviceMode;
    //联系方式
    private String contactPhone;
    //地址
    private String addrDetail;
    //开始时间
    @JsonFormat(pattern = "HH:mm")
    private Date businessStartTime;
    //结束时间
    @JsonFormat(pattern = "HH:mm")
    private Date businessEndTime;
    //经度
    private Double lon;
    //纬度
    private Double lat;

}
