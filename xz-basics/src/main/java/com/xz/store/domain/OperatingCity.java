package com.xz.store.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xz.common.annotation.Excel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 运营城市对象 t_operating_city
 * 
 * @author xz
 * @date 2022-03-08
 */
@Data
@TableName("t_operating_city")
public class OperatingCity extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 所在省编号 */
    private String provinceId;

    /** 所在省名称 */
    @Excel(name = "省名称")
    @NotBlank(message = "省名称不能为空")
    private String provinceName;

    /** 所在省拼英首字母 */
    private String provincePinyin;

    /** 所在市编号 */
    private String cityId;

    /** 所在市名称 */
    @Excel(name = "市名称")
    @NotBlank(message = "市名称不能为空")
    private String cityName;

    /** 所在省拼英首字母 */
    private String cityPinyin;

    /** 所在区编号 */
    private String areaId;

    /** 所在区名称 */
    @Excel(name = "区名称")
    private String areaName;

    /** 删除状态（0未删除，1已删除） */
    private Integer delFlag;

    /** 是否启用 0未启用，1启用 */
    @Excel(name = "是否启用",readConverterExp ="0=停用,1=启用" )
    private Integer isAvailable;

    /** 租户id */
    private Long tenantId;
    /** 租户id */
    private String tenantName;

}
