package com.xz.store.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 门店基本信息对象 t_store_basic
 *
 * @author xz
 * @date 2022-03-01
 */
@Data
@TableName("t_store_basic")
public class StoreBasic extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @TableId
    private String storeId;

    /** 门店名称（网点名称） */
    @Excel(name = "门店名称")
    private String storeName;

    /** 门店类型（1.普通门店 2.送车站点） */
    private Integer storeType;

    /** 门店分类(1自营门店,2加盟门店) */
    private Integer storeClass;

    /** 服务模式(1人工服务,2自助取还)） */
    private Integer serviceMode;

    /** 联系人 */
    @Excel(name = "联系人")
    private String contactName;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String contactPhone;

    /** 所属公司id */
    private String companyId;

    /** 所属公司名称 */
    @Excel(name = "所属公司名称")
    private String companyName;

    /** 门店标签 */
    private String label;

    /** 所在省编号 */
    private String provinceId;

    /** 所在省名称 */
    @Excel(name = "所在省名称")
    private String provinceName;

    /** 所在市编号 */
    private String cityId;

    /** 所在市名称 */
    @Excel(name = "所在市名称")
    private String cityName;

    /** 所在区编号 */
    private String areaId;

    /** 所在区名称 */
    @Excel(name = "所在区名称")
    private String areaName;

    /** 经度 */
    private Double storeLon;

    /** 纬度 */
    private Double storeLat;

    /** 门店地址 */
    @Excel(name = "门店地址")
    private String addrDetail;

    /** 营业开始时间 */
    @JsonFormat(pattern = "HH:mm")
    @Excel(name = "营业开始时间", width = 30, dateFormat = "HH:mm")
    private Date businessStartTime;

    /** 营业结束时间 */
    @JsonFormat(pattern = "HH:mm")
    @Excel(name = "营业结束时间", width = 30, dateFormat = "HH:mm")
    private Date businessEndTime;

    /** 删除状态（0未删除，1已删除） */
    private Integer delFlag;

    /** 是否启用 0未启用，1启用 */
    private Long isAvailable;

    /** 租户id */
    private Long tenantId;

    /** 部门id */
    private Long deptId;

    /**门店实景图*/
    @TableField(exist = false)
    private String storeImage;
    //服务范围/km
    @Excel(name = "服务范围/km")
    private Double serverScope;
    //范围内服务费/元
    //超范围收费标准km/元
    @Excel(name = "范围内服务费/元")
    private BigDecimal serviceChargeScope;
    @Excel(name = "超范围收费标准km/元")
    private BigDecimal standardFees;
    //城市封顶费用/元
    @Excel(name = "城市封顶费用/元")
    private BigDecimal cappingFees;
    //距离（公里）
    @TableField(exist = false)
    private Double distance;
    /**
     * 企业资格认证id
     */
    private Long enterpriseCertificationId;
    //region 查询条件
    /**
     * 查询条件
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableField(exist = false)
    private boolean enterpriseCertificationIdIsNull;

    /** 最大提前预约天数 -1 不限制 */
    private Integer advanceAppointmentDays;
}
