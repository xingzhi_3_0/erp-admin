package com.xz.store.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;


@Data
public class VerificationDto {
    //租户id
    private Long tenantId;
    //预约取车门店
    private String takeStoreId;
    //预约还车门店
    private String alsoStoreId;

    //租赁开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    //取车类型(1到店取还车,2自助取还车,3上门送取车,4上门送车,5上门取车)
    private Integer rentalType;

    //上门送车经度
    private String takeLat;
    //上门送车纬度
    private String takeLon;
    //上门取车经度
    private String alsoLat;
    //上门取车纬度
    private String alsoLon;
}
