package com.xz.store.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2022/3/9 19:07
 * @description：首页运营城市
 */
@Data
public class OperatingCityDto {

    //城市名称
    private String cityName;
    //城市id
    private String cityId;
    //门店id
    private String storeId;
    //门店名称
    private String storeName;
    //租赁开始时间
    private String startTime;
    //租赁结束时间
    private String endTime;
    //租户id
    @NotNull(message = "租户id不能为空")
    private Long tenantId;
    //1.短租 3.豪车
    private Integer bizType;
    //页码
    private Integer pageNo;
    //每页显示数据
    private Integer pageSize;
    //租户对应部门id
    private Long tenantDeptId;
}
