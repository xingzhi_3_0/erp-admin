package com.xz.store.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2022/3/31 9:54
 * @description：长租车型运营分类下车型
 */
@Data
public class LongClassificationDto {
    //运营分类 1. 经济型; 2. 舒适型; 3. 商务型; 4. 豪华型 5.SUV
    @NotNull(message = "运营分类不能为空")
    private Integer classification;
    //租户id
    @NotNull(message = "租户id不能为空")
    private Long tenantId;
    //门店id
    @NotBlank(message = "门店id不能为空")
    private String storeId;
    //车系id
    private String seriesId;
    //排序 1.由低到高 2.由高到低
    private Integer sort;
    //变速箱 1. 手动; 2. 自动
    private Integer speedChangingBox;
    //页码
    private Integer pageNo;
    //每页显示数据
    private Integer pageSize;
    //租户对应部门id
    private Long tenantDeptId;
}
