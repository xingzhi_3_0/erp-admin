package com.xz.store.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2022/3/10 14:04
 * @description：车型运营分类筛选车型
 */
@Data
public class ClassificationDto {
    //运营分类 1. 经济型; 2. 舒适型; 3. 商务型; 4. 豪华型 5.SUV
    @NotNull(message = "运营分类不能为空")
    private String classification;
    //租户id
    @NotNull(message = "租户id不能为空")
    private Long tenantId;
    //门店id
    @NotBlank(message = "门店id不能为空")
    private String storeId;
    private String alsoStoreId;
    //租赁开始时间
    @NotBlank(message = "租赁开始时间不能为空")
    private String startTime;
    //租赁结束时间
    @NotBlank(message = "租赁结束时间不能为空")
    private String endTime;
    //品牌id
    private List<String> brandId;
    //排序 1.由低到高 2.由高到低
    private Integer sort;
    //变速箱 1. 手动; 2. 自动
    private List<Integer> speedChangingBox;
    //价格区间(1 100以内 2.100-300 3.300以上)
    private Integer priceType;
    //座位
    private List<Integer> seat;
    //车型配置
    private List<Integer> modelConfig;
    //标签
    private List<Integer> label;
    //活动
    private Integer activity;
    //页码
    private Integer pageNo;
    //每页显示数据
    private Integer pageSize;
    //1.短租 3.豪车
    private Integer bizType;

    //租户对应部门id
    private Long tenantDeptId;
    //车型id
    private String modelId;
    //订单编号
    private String orderNo;

    //上门送车经度
    private String takeLat;
    //上门送车纬度
    private String takeLon;
    //上门取车经度
    private String alsoLat;
    //上门取车纬度
    private String alsoLon;
}
