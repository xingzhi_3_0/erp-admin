package com.xz.purchaseOrder.dto;

import com.xz.apply.vo.ConfirmPurchaseApplyVo;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @ClassName ApplyReorderDto * @Description TODO
 * @Author Administrator
 * @Date 11:18 2024/1/22
 * @Version 1.0  采购申请转订单
 **/
@Data
public class ApplyReorderDto {

    /**
     * 采购申请类型 1.门店申请  2.定制采购
     */
    @NotNull(message = "采购申请类型不能为空")
    private Integer applyType;
    /**
     * 采购申请商品
     */
    @NotNull(message = "采购申请商品不能为空")
    private List<ConfirmPurchaseApplyVo> reorderInfoDtoList;
}
