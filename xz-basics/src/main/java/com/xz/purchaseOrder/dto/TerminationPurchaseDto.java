package com.xz.purchaseOrder.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @ClassName TerminationPurchaseDto * @Description TODO
 * @Author Administrator
 * @Date 17:27 2024/2/1
 * @Version 1.0 采购终止
 **/
@Data
public class TerminationPurchaseDto {
    /*采购类型（1.全部终止 2.部分终止）*/
    @NotNull(message = "操作类型不能为空")
    private Integer type;
    /*采购商品id*/
    private Long purchaseProductOrderId;
    /*采购id*/
    @NotNull(message = "采购id不能为空")
    private Long purchaseOrderId;
}
