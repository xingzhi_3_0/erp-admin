package com.xz.purchaseOrder.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @ClassName ApplyReorderInfoDto * @Description TODO
 * @Author Administrator
 * @Date 11:52 2024/1/22
 * @Version 1.0 采购申请转订单 申请商品明细
 **/
@Data
public class ApplyReorderInfoDto {
    /**
     * 采购申请id
     */
    @NotNull(message = "采购申请id不能为空")
    private Long applyId;
    /**
     * 供应商
     */
    @NotNull(message = "供应商不能为空")
    private Long supplierId;
    /**
     * 仓库
     */
    @NotNull(message = "仓库不能为空")
    private Long warehouseId;
    /**
     * 采购数量
     */
    private Integer applyNum;
    /**
     * 附件
     */
    private String url;
}
