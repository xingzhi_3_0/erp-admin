package com.xz.purchaseOrder.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.purchaseOrder.domain.PurchaseProductOrder;

/**
 * 采购订单商品Mapper接口
 * 
 * @author xz
 * @date 2024-01-22
 */
@Mapper
public interface PurchaseProductOrderMapper  extends BaseMapper<PurchaseProductOrder> {
    /**
     * 查询采购订单商品
     * 
     * @param id 采购订单商品主键
     * @return 采购订单商品
     */
    public PurchaseProductOrder selectPurchaseProductOrderById(Long id);

    /**
     * 查询采购订单商品列表
     * 
     * @param purchaseProductOrder 采购订单商品
     * @return 采购订单商品集合
     */
    public List<PurchaseProductOrder> selectPurchaseProductOrderList(PurchaseProductOrder purchaseProductOrder);

    /**
     * 新增采购订单商品
     * 
     * @param purchaseProductOrder 采购订单商品
     * @return 结果
     */
    public int insertPurchaseProductOrder(PurchaseProductOrder purchaseProductOrder);

    /**
     * 修改采购订单商品
     * 
     * @param purchaseProductOrder 采购订单商品
     * @return 结果
     */
    public int updatePurchaseProductOrder(PurchaseProductOrder purchaseProductOrder);

    /**
     * 删除采购订单商品
     * 
     * @param id 采购订单商品主键
     * @return 结果
     */
    public int deletePurchaseProductOrderById(Long id);
    public int updatePurchaseProductOrderStatus(Long id);

    /**
     * 批量删除采购订单商品
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePurchaseProductOrderByIds(Long[] ids);
}
