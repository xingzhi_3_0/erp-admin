package com.xz.purchaseOrder.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.purchaseOrder.vo.PurchaseOrderInfoVo;
import com.xz.purchaseOrder.vo.PurchaseOrderVo;
import com.xz.purchaseOrder.vo.WaitingListVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.purchaseOrder.domain.PurchaseOrder;

/**
 * 采购订单Mapper接口
 * 
 * @author xz
 * @date 2024-01-22
 */
@Mapper
public interface PurchaseOrderMapper  extends BaseMapper<PurchaseOrder> {
    /**
     * 查询采购订单
     * 
     * @param id 采购订单主键
     * @return 采购订单
     */
    public PurchaseOrderInfoVo selectPurchaseOrderById(Long id);

    /**
     * 查询采购订单列表
     * 
     * @param purchaseOrder 采购订单
     * @return 采购订单集合
     */
    public List<PurchaseOrderVo> selectPurchaseOrderList(PurchaseOrder purchaseOrder);

    /**
     * 新增采购订单
     * 
     * @param purchaseOrder 采购订单
     * @return 结果
     */
    public int insertPurchaseOrder(PurchaseOrder purchaseOrder);

    /**
     * 修改采购订单
     * 
     * @param purchaseOrder 采购订单
     * @return 结果
     */
    public int updatePurchaseOrder(PurchaseOrder purchaseOrder);

    /**
     * 删除采购订单
     * 
     * @param id 采购订单主键
     * @return 结果
     */
    public int deletePurchaseOrderById(Long id);

    /**
     * 批量删除采购订单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePurchaseOrderByIds(Long[] ids);

    /**
     * 待入库列表
     * @return
     */
    List<WaitingListVo> getWaitingList(PurchaseOrder purchaseOrder);
}
