package com.xz.purchaseOrder.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xz.common.annotation.Excel;
import com.xz.purchase.domain.PurchaseProductParam;
import lombok.Data;

import java.util.List;

/**
 * @ClassName PurchaseProductOrderVo * @Description TODO
 * @Author Administrator
 * @Date 16:49 2024/1/22
 * @Version 1.0 采购商品
 **/
@Data
public class PurchaseProductOrderVo {
    /** id */
    private Long purchaseProductOrderId;
    /** 采购数量 */
    @Excel(name = "采购数量")
    private Integer purchaseNum;
    /** 等待入库数量 */
    private Integer waitPurchaseNum;
    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;

    /** 商品id */
    @Excel(name = "商品id")
    private Long productId;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private String productEncoded;
    /**
     * 客户信息
     */
    private Long patientId;
    private String patientName;
    /** 商品参数 */
    @Excel(name = "商品参数")
    private String productParam;
    /** 单位 */
    @Excel(name = "单位")
    private String unit;
    /** 状态（1.正常 2.终止采购） */
    @Excel(name = "状态", readConverterExp = "1=.正常,2=.终止采购")
    private Integer ppaStatus;
    /** 备注 */
    private String ppaRemark;
    /*采购附件*/
    private String purchaseUrl;
    /*关联单号*/
    private String applyNumber;
    /*定制采购商品id*/
    private Long purchaseDetailId;
    private String productUrlName;
    /**
     * 采购商品属性
     */
    @TableField(exist = false)
    private List<PurchaseProductParam> purchaseProductParamList;
}
