package com.xz.purchaseOrder.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName WaitingListVo * @Description TODO
 * @Author Administrator
 * @Date 14:32 2024/1/23
 * @Version 1.0  待入库商品
 **/
@Data
public class WaitingListVo {
    /** id */
    private Long id;

    /** 采购数量 */
    private Integer purchaseNum;
    /** 等待入库数量 */
    private Integer waitPurchaseNum;

    /** 生产厂商 */
    private String manufacturer;

    /** 注册证号 */
    private String registrationNumber;

    /** 商品名称 */
    private String productName;

    /** 商品id */
    private Long productId;

    /** 商品编号 */
    private String productEncoded;

    /** 供应商id */
    private Long supplierId;

    /** 仓库id */
    private Long warehouseId;

    /** 商品参数 */
    private String productParam;

    /** 供应商 */
    private String supplierName;

    /** 仓库 */
    private String warehouseName;

    /** 单位 */
    private String unit;


    /** 部门id */
    private Long deptId;

    /** 采购附件 */
    private String purchaseUrl;
    private String productUrlName;
    /** 制单人 */
    private String userName;
    /** 制单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 采购部门 */
    private String deptName;
    /** 客户信息 */
    private String memberName;
    /** 采购编号 */
    private String purchaseNo;
    /** 关联采购单号 */
    private String relevancePurchaseNo;
}
