package com.xz.purchaseOrder.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.utils.StringUtils;
import lombok.Data;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @ClassName PurchaseOrderVO * @Description TODO
 * @Author Administrator
 * @Date 16:26 2024/1/22
 * @Version 1.0  采购订单列表
 **/
@Data
public class PurchaseOrderVo {

    /** id */
    private Long id;

    /** 采购订单类型 (1.门店采购 2.定制采购 3.自主采购)*/
    @Excel(name = "采购订单类型")
    private Integer purchaseType;
    /** 采购编号 */
    @Excel(name = "采购编号")
    private String purchaseNo;

    /** 关联采购单号 */
    @Excel(name = "关联采购单号")
    private String relevancePurchaseNo;
    /** 客户信息 */
    @Excel(name = "客户信息")
    private String patientName;
    /** 供应商 */
    @Excel(name = "供应商")
    private String supplierName;

    /** 采购数量 */
    @Excel(name = "采购数量")
    private Integer purchaseNum;
    /** 仓库 */
    @Excel(name = "仓库")
    private String warehouseName;
    /** 仓库地址 */
    private String warehouseAddress;

    /** 状态（1.未提交 2.已提交 3.已作废 4.采购终止 5.已完成） */
    @Excel(name = "状态", readConverterExp = "1=.未提交,2=.已提交,3=.已作废,4=.采购终止,5=.已完成")
    private Integer status;
    /** 制单人 */
    @Excel(name = "制单人")
    private String userName;
    /** 制单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 采购部门 */
    @Excel(name = "采购部门")
    private String deptName;

  public String getPatientName() {
    if (StringUtils.isEmpty(this.patientName) || !this.patientName.contains(",")) {
      return patientName;
    }
    List<String> patientNames = Arrays.stream(patientName.split(",")).distinct().collect(Collectors.toList());
    return String.join(",", patientNames);
  }
}
