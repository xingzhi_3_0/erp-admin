package com.xz.purchaseOrder.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.List;

/**
 * @ClassName PurchaseOrderInfoVo * @Description TODO
 * @Author Administrator
 * @Date 16:43 2024/1/22
 * @Version 1.0 采购详情
 **/
@Data
public class PurchaseOrderInfoVo extends PurchaseOrderVo{
    /** 备注 */
    private String remark;
    /**
     * 部门id
     */
    private Long deptId;
    /** 仓库id */
    @Excel(name = "仓库id")
    private Long warehouseId;
    /** 供应商id */
    @Excel(name = "供应商id")
    private Long supplierId;
    /** 采购商品 */
    private List<PurchaseProductOrderVo> productOrderVoList;
}
