package com.xz.purchaseOrder.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.purchaseOrder.domain.PurchaseOrder;
import com.xz.purchaseOrder.dto.ApplyReorderDto;
import com.xz.purchaseOrder.dto.TerminationPurchaseDto;
import com.xz.purchaseOrder.vo.PurchaseOrderInfoVo;
import com.xz.purchaseOrder.vo.PurchaseOrderVo;
import com.xz.purchaseOrder.vo.WaitingListVo;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 采购订单Service接口
 * 
 * @author xz
 * @date 2024-01-22
 */
public interface IPurchaseOrderService extends IService<PurchaseOrder> {
    /**
     * 查询采购订单
     * 
     * @param id 采购订单主键
     * @return 采购订单
     */
    public PurchaseOrderInfoVo selectPurchaseOrderById(Long id);

    /**
     * 查询采购订单列表
     * 
     * @param purchaseOrder 采购订单
     * @return 采购订单集合
     */
    public List<PurchaseOrderVo> selectPurchaseOrderList(PurchaseOrder purchaseOrder);

    /**
     * 新增采购订单
     * 
     * @param purchaseOrder 采购订单
     * @return 结果
     */
    public AjaxResult insertPurchaseOrder(PurchaseOrder purchaseOrder);

    /**
     * 修改采购订单
     * 
     * @param purchaseOrder 采购订单
     * @return 结果
     */
    public AjaxResult updatePurchaseOrder(PurchaseOrder purchaseOrder);

    /**
     * 批量删除采购订单
     * 
     * @param ids 需要删除的采购订单主键集合
     * @return 结果
     */
    public int deletePurchaseOrderByIds(Long[] ids);

    /**
     * 采购终止
     * 
     * @param id 采购订单主键
     * @return 结果
     */
    public AjaxResult deletePurchaseOrderById(Long id);

    /**
     * 删除
     * @param purchaseDto
     * @return
     */
    public AjaxResult terminationPurchase(TerminationPurchaseDto purchaseDto);

    /**
     * 门店采购申请转订单
     * @param applyReorderDto
     * @return
     */
    AjaxResult applyReorder(ApplyReorderDto applyReorderDto);
    /**
     * 待入库列表
     * @return
     */
    List<WaitingListVo> getWaitingList(PurchaseOrder purchaseOrder);
}
