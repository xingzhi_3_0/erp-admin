package com.xz.purchaseOrder.service.impl;

import java.util.List;

import com.xz.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.purchaseOrder.mapper.PurchaseProductOrderMapper;
import com.xz.purchaseOrder.domain.PurchaseProductOrder;
import com.xz.purchaseOrder.service.IPurchaseProductOrderService;

/**
 * 采购订单商品Service业务层处理
 *
 * @author xz
 * @date 2024-01-22
 */
@Service
public class PurchaseProductOrderServiceImpl extends ServiceImpl<PurchaseProductOrderMapper, PurchaseProductOrder> implements IPurchaseProductOrderService {
    @Autowired
    private PurchaseProductOrderMapper purchaseProductOrderMapper;

    /**
     * 查询采购订单商品
     *
     * @param id 采购订单商品主键
     * @return 采购订单商品
     */
    @Override
    public PurchaseProductOrder selectPurchaseProductOrderById(Long id) {
        return purchaseProductOrderMapper.selectPurchaseProductOrderById(id);
    }

    /**
     * 查询采购订单商品列表
     *
     * @param purchaseProductOrder 采购订单商品
     * @return 采购订单商品
     */
    @Override
    public List<PurchaseProductOrder> selectPurchaseProductOrderList(PurchaseProductOrder purchaseProductOrder) {
        return purchaseProductOrderMapper.selectPurchaseProductOrderList(purchaseProductOrder);
    }

    /**
     * 新增采购订单商品
     *
     * @param purchaseProductOrder 采购订单商品
     * @return 结果
     */
    @Override
    public int insertPurchaseProductOrder(PurchaseProductOrder purchaseProductOrder) {
        purchaseProductOrder.setCreateTime(DateUtils.getNowDate());
        return purchaseProductOrderMapper.insertPurchaseProductOrder(purchaseProductOrder);
    }

    /**
     * 修改采购订单商品
     *
     * @param purchaseProductOrder 采购订单商品
     * @return 结果
     */
    @Override
    public int updatePurchaseProductOrder(PurchaseProductOrder purchaseProductOrder) {
        purchaseProductOrder.setUpdateTime(DateUtils.getNowDate());
        return purchaseProductOrderMapper.updatePurchaseProductOrder(purchaseProductOrder);
    }

    @Override
    public int updatePurchaseProductOrderStatus(Long id) {
        return purchaseProductOrderMapper.updatePurchaseProductOrderStatus(id);
    }

    /**
     * 批量删除采购订单商品
     *
     * @param ids 需要删除的采购订单商品主键
     * @return 结果
     */
    @Override
    public int deletePurchaseProductOrderByIds(Long[] ids) {
        return purchaseProductOrderMapper.deletePurchaseProductOrderByIds(ids);
    }

    /**
     * 删除采购订单商品信息
     *
     * @param id 采购订单商品主键
     * @return 结果
     */
    @Override
    public int deletePurchaseProductOrderById(Long id) {
        return purchaseProductOrderMapper.deletePurchaseProductOrderById(id);
    }
}
