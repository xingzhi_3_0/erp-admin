package com.xz.purchaseOrder.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;

import com.xz.purchase.domain.PurchaseProductParam;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 采购订单商品对象 t_purchase_product_order
 *
 * @author xz
 * @date 2024-01-22
 */
@Data
@TableName("t_purchase_product_order")
public class PurchaseProductOrder extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 采购订单id */
    @Excel(name = "采购订单id")
    private Long purchaseOrderId;

    /** 采购数量 */
    @Excel(name = "采购数量")
    private Integer purchaseNum;
    /** 等待入库数量 */
    private Integer waitPurchaseNum;

    /** 供货单号 */
    @Excel(name = "供货单号")
    private String deliveryNumber;

    /** 生产批号 */
    @Excel(name = "生产批号")
    private String batchNumber;

    /** 生成日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "生成日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date batchDate;

    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "有效期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date validity;

    /** 生产厂商 */
    @Excel(name = "生产厂商")
    private String manufacturer;

    /** 注册证号 */
    @Excel(name = "注册证号")
    private String registrationNumber;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;

    /** 商品id */
    @Excel(name = "商品id")
    private Long productId;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private String productEncoded;

    /** 供应商id */
    @Excel(name = "供应商id")
    private Long supplierId;

    /** 仓库id */
    @Excel(name = "仓库id")
    private Long warehouseId;

    /** 商品参数 */
    @Excel(name = "商品参数")
    private String productParam;

    /** 供应商 */
    @Excel(name = "供应商")
    private String supplierName;

    /** 仓库 */
    @Excel(name = "仓库")
    private String warehouseName;

    /** 成本单价 */
    @Excel(name = "成本单价")
    private BigDecimal costPrice;

    /** 单位 */
    @Excel(name = "单位")
    private String unit;

    /** 租户id */
    @Excel(name = "租户id")
    @NotNull(message = "租户id不能为空")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    /** 品牌 */
    @Excel(name = "品牌")
    private String brandName;

    /** 商品类别 */
    @Excel(name = "商品类别")
    private String productCategory;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String prodName;
    /** 采购附件 */
    private String purchaseUrl;

    /** 状态（1.正常 2.终止采购） */
    @Excel(name = "状态", readConverterExp = "1=.正常,2=.终止采购 3.已作废")
    private Integer status;
    /** 关联单号 */
    @Excel(name = "关联单号")
    private String applyNumber;
    /*定制采购商品id*/
    private Long purchaseDetailId;
    private String productUrlName;
    /**
     * 采购商品属性
     */
    @TableField(exist = false)
    private List<PurchaseProductParam> purchaseProductParamList;

    /** 客户id */
    @Excel(name = "客户id")
    private Long patientId;

    /** 客户信息 */
    @Excel(name = "客户信息")
    private String patientName;
}
