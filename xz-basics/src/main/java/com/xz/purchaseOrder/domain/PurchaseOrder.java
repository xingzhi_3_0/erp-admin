package com.xz.purchaseOrder.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.apply.domain.PurchaseProductApply;
import com.xz.apply.vo.ConfirmPurchaseApplyVo;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

import java.util.Date;
import java.util.List;

/**
 * 采购订单对象 t_purchase_order
 *
 * @author xz
 * @date 2024-01-22
 */
@Data
@TableName("t_purchase_order")
public class PurchaseOrder extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 采购订单类型 (1.门店采购 2.定制采购 3.自主采购)*/
    @Excel(name = "采购订单类型")
    private Integer purchaseType;

    /** 采购申请id */
    @Excel(name = "采购申请id")
    private String purchaseApplyId;

    /** 采购编号 */
    @Excel(name = "采购编号")
    private String purchaseNo;

    /** 关联采购单号 */
    @Excel(name = "关联采购单号")
    private String relevancePurchaseNo;

    /** 客户id */
    @Excel(name = "客户id")
    private String patientId;

    /** 客户信息 */
    @Excel(name = "客户信息")
    private String patientName;

    /** 供应商id */
    @Excel(name = "供应商id")
    private Long supplierId;

    /** 供应商 */
    @Excel(name = "供应商")
    private String supplierName;

    /** 采购数量 */
    @Excel(name = "采购数量")
    private Integer purchaseNum;

    /** 仓库id */
    @Excel(name = "仓库id")
    private Long warehouseId;

    /** 仓库 */
    @Excel(name = "仓库")
    private String warehouseName;
    /** 仓库地址 */
    private String warehouseAddress;

    /** 状态（1.未提交 2.已提交 3.已作废 4.采购终止 5.已完成） */
    @Excel(name = "状态", readConverterExp = "1=.未提交,2=.已提交,3=.已作废,4=.采购终止,5=.已完成")
    private Integer status;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    /**
     * 采购申请商品
     */
    @TableField(exist = false)
    private List<ConfirmPurchaseApplyVo> reorderInfoDtoList;
    /**
     * 状态
     */
    @TableField(exist = false)
    public List<Integer> statusList;
    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;

    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;
    /**
     *采购申请商品明细
     */
    @TableField(exist = false)
    private List<PurchaseProductOrder> productOrderList;
}
