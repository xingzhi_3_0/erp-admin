package com.xz.sales.vo;

import com.xz.optometry.vo.OptometryInfoVo;
import com.xz.sales.domain.SalesOrder;
import com.xz.sales.domain.SalesOrderDetail;
import lombok.Data;

import java.util.List;

@Data
public class ChooseSalesDetailVo extends SalesOrder {

    List<SalesOrderDetail> detailList;



}
