package com.xz.sales.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

@Data
public class SalesPurchaseNoFlowVo {


    @Excel(name = "id")
    private Long id;
    
    @Excel(name = "sales_no")
    private String salesNo;

    @Excel(name = "selling_num")
    private Integer sellingNum;


}
