package com.xz.sales.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.optometry.vo.OptometryInfoVo;
import com.xz.sales.domain.SalesOrder;
import com.xz.sales.domain.SalesOrderDetail;
import com.xz.sales.domain.SalesOriginalOrderDetail;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class SalesOrderDetailVo extends SalesOrderDetail {
    /** 销售单号 */
    @Excel(name = "销售单号")
    private String salesNo;

    /** 当前年龄 */
    @Excel(name = "当前年龄")
    private Integer currentAge;

    private Integer age;

    private Integer sex;

    private String patientInfo;

    /** 患者id */
    @Excel(name = "患者id")
    private Long patientId;

    /** 患者名称 */
    @Excel(name = "患者名称")
    private String patientName;

    /** 患者手机号 */
    @Excel(name = "患者手机号")
    private String patientPhone;

    /** 销售员id */
    @Excel(name = "销售员id")
    private Long salesUserId;

    /** 销售员名称 */
    @Excel(name = "销售员名称")
    private String salesUserName;

    private String SalesDeptName;

    /** 处方id */
    @Excel(name = "处方id")
    private Long optometryId;

    /** 商品总金额 */
    @Excel(name = "商品总金额")
    private BigDecimal productTotalAmount;

    /** 收款金额 */
    @Excel(name = "收款金额")
    private BigDecimal collectionAmount;

    /** 钱包支付金额 */
    @Excel(name = "钱包支付金额")
    private BigDecimal walletPay;

    private BigDecimal dish;

    private BigDecimal bank;

    private BigDecimal ePay;

    private BigDecimal emoPay;


    /** 钱包支付外金额 */
    @Excel(name = "钱包支付外金额")
    private BigDecimal pendingAmount;

    @Excel(name = "减价金额")
    private BigDecimal reduceAmount;
    @Excel(name = "打折后金额")
    private BigDecimal afterDiscountAmount;

    /** 支付方式 1-现金 2-银行卡 3-E支付  */
    @Excel(name = "1-现金 2-银行卡 3-E支付  ")
    private Integer paymentType;

    /** 付款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "付款时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date paymentTime;

    /** 会员卡号 */
    @Excel(name = "会员卡号")
    private String memberCardNo;

    private String memberLevel;

    /** 1-会员折扣 2-积分抵扣 3-手动折扣  */
    @Excel(name = "1-会员折扣 2-积分抵扣 3-手动折扣 ")
    private Integer preferentialType;

    private String preferentialTypeStr;

    /** 赠品抵扣金额  */
    @Excel(name = "赠品抵扣金额")
    private BigDecimal giftDeductionAmount;

    /** 优惠金额  */
    @Excel(name = "优惠金额")
    private BigDecimal preferentialAmount;

    @Excel(name = "优惠抵扣积分")
    private BigDecimal deductionIntegral;

    @Excel(name = "优惠折扣")
    private BigDecimal discount;

    /** 交付方式（1.门店自取 2.邮寄） */
    @Excel(name = "交付方式", readConverterExp = "1=.门店自取,2=.邮寄")
    private Integer deliveryMethod;

    private String deliveryMethodStr;


    private String salesCreateTime;

    private Integer salesStatus;

    private String salesStatusStr;

    private Integer isProvideOneself;
    private String isProvideOneselfStr;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    private String salesRemark;

    private String birthdayStr;

}
