package com.xz.sales.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@ContentRowHeight(25) //内容行高
@HeadRowHeight(20)//表头行高
public class ReportOrderBO implements Serializable {

    @ColumnWidth(20)//宽度
    @ExcelProperty(value = {"订单信息","销售门店"}, index = 0)
    private String salesDeptName;

    @ColumnWidth(10)
    @ExcelProperty(value = {"订单信息","销售员"}, index = 1)
    private String salesUserName;

    @ColumnWidth(15)
    @ExcelProperty(value = {"订单信息","制单时间"}, index = 2)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date salesCreateTime;

    @ColumnWidth(10)
    @ExcelProperty(value = {"订单信息","订单状态"}, index = 3)
    private String salesStatusStr;

    @ColumnWidth(10)
    @ExcelProperty(value = {"订单信息","销售单号"}, index = 4)
    private String salesNo;

    @ColumnWidth(10)
    @ExcelProperty(value = {"订单信息","客户信息"}, index = 5)
    private String patientInfo;

    @ColumnWidth(10)
    @ExcelProperty(value = {"订单信息","手机号码"}, index = 6)
    private String patientPhone;

    @ColumnWidth(10)
    @ExcelProperty(value = {"订单信息","关联会员卡号"}, index = 7)
    private String memberCardNo;

    @ColumnWidth(10)
    @ExcelProperty(value = {"订单信息","会员等级"}, index = 8)
    private String memberLevel;

    @ColumnWidth(10)
    @ExcelProperty(value = {"订单信息","出生日期"}, index = 9)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    @ColumnWidth(10)
    @ExcelProperty(value = {"订单信息","交付方式"}, index = 10)
    private String deliveryMethodStr;

    @ColumnWidth(10)
    @ExcelProperty(value = {"订单信息","客人提供镜框"}, index = 11)
    private String isProvideOneselfStr;

    @ColumnWidth(10)
    @ExcelProperty(value = {"订单信息","备注"}, index = 12)
    private String salesRemark;

    @ColumnWidth(10)
    @ExcelProperty(value = {"订单信息","商品总价"}, index = 13)
    private BigDecimal productTotalAmount;

    @ColumnWidth(10)
    @ExcelProperty(value = {"订单信息","优惠方式"}, index = 14)
    private String preferentialTypeStr;

    @ColumnWidth(10)
    @ExcelProperty(value = {"订单信息","优惠金额"}, index = 15)
    private BigDecimal preferentialAmount;

    @ColumnWidth(10)
    @ExcelProperty(value = {"订单信息","抹零金额"}, index = 16)
    private BigDecimal reduceAmount;

    @ColumnWidth(10)
    @ExcelProperty(value = {"订单信息","实收金额"}, index = 17)
    private BigDecimal collectionAmount;

    @ColumnWidth(10)
    @ExcelProperty(value = {"收款信息","钱包"}, index = 18)
    private BigDecimal walletPay;

    @ColumnWidth(10)
    @ExcelProperty(value = {"收款信息","现金"}, index = 19)
    private BigDecimal dish;

    @ColumnWidth(10)
    @ExcelProperty(value = {"收款信息","银行卡"}, index = 20)
    private BigDecimal bank;

    @ColumnWidth(10)
    @ExcelProperty(value = {"收款信息","E支付"}, index = 21)
    private BigDecimal ePay;

    @ColumnWidth(10)
    @ExcelProperty(value = {"商品信息","商品名称"}, index = 22)
    private String productName;

    @ColumnWidth(10)
    @ExcelProperty(value = {"商品信息","采购参数"}, index = 23)
    private String productParam;

    @ColumnWidth(10)
    @ExcelProperty(value = {"商品信息","仓库"}, index = 24)
    private String warehouseName;

    @ColumnWidth(10)
    @ExcelProperty(value = {"商品信息","加工类型"}, index = 25)
    private String machiningTypeStr;

    @ColumnWidth(10)
    @ExcelProperty(value = {"商品信息","售价"}, index = 26)
    private BigDecimal sellingPrice;

    @ColumnWidth(10)
    @ExcelProperty(value = {"商品信息","数量"}, index = 27)
    private Integer sellingNum;

    @ColumnWidth(10)
    @ExcelProperty(value = {"商品信息","赠品核销"}, index = 28)
    private String useReceiveGift;

    @ColumnWidth(10)
    @ExcelProperty(value = {"商品信息","优惠"}, index = 29)
    private BigDecimal discountAmount;


    @ColumnWidth(10)
    @ExcelProperty(value = {"商品信息","小计"}, index = 30)
    private BigDecimal subtotal;

    @ColumnWidth(10)
    @ExcelProperty(value = {"商品信息","备注"}, index = 31)
    private String remark;


}
