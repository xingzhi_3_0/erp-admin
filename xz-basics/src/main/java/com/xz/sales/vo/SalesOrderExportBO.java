package com.xz.sales.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class SalesOrderExportBO implements Serializable {

    @Excel(needMerge = true,name = "销售门店")
    private String salesDeptName;

    @Excel(needMerge = true,name = "销售员")
    private String salesUserName;

    @Excel(needMerge = true,name = "制单时间")
    private String salesCreateTime;

    @Excel(needMerge = true,name = "订单状态")
    private String salesStatusStr;

    @Excel(needMerge = true,name = "销售单号")
    private String salesNo;

    @Excel(needMerge = true,name = "客户信息")
    private String patientName;


    @Excel(needMerge = true,name = "手机号码")
    private String patientPhone;

    @Excel(needMerge = true,name = "关联会员卡号")
    private String memberCardNo;

    @Excel(needMerge = true,name = "会员等级")
    private String memberLevel;

    @Excel(needMerge = true,name = "出生日期")
    private String birthdayStr;

    @Excel(needMerge = true,name = "交付方式")
    private String deliveryMethodStr;

    @Excel(needMerge = true,name = "客人提供镜框")
    private String isProvideOneselfStr;

    @Excel(needMerge = true,name = "备注")
    private String salesRemark;

    @Excel(needMerge = true,name = "商品总价")
    private BigDecimal productTotalAmount;


    @Excel(needMerge = true,name = "优惠方式")
    private String preferentialTypeStr;

    @Excel(needMerge = true,name = "优惠金额")
    private BigDecimal preferentialAmount;

    @Excel(needMerge = true,name = "抹零金额")
    private BigDecimal reduceAmount;


    @Excel(needMerge = true,name = "实收金额")
    private BigDecimal collectionAmount;

//    @ExcelProperty(value = {"收款信息",""}, index = 18)
    @Excel(needMerge = true,name = "钱包")
    private BigDecimal walletPay;


    @ExcelCollection(name = "销售商品明细")
    private List<SalesDetail> salesDetailList;

    @Data
    public static class SalesDetail {

        @Excel(name = "现金")
        private BigDecimal dish;

        @Excel(name = "银行卡")
        private BigDecimal bank;

        @Excel(name = "E支付")
        private BigDecimal emoPay;

        @Excel(name = "商品名称")
        private String productName;

        @Excel(name = "采购参数")
        private String productParam;

        @Excel(name = "仓库")
        private String warehouseName;

        @Excel(name = "加工类型")
        private String machiningTypeStr;

        @Excel(name = "售价")
        private BigDecimal sellingPrice;

        @Excel(name = "数量")
        private Integer sellingNum;

        @Excel(name = "赠品核销")
        private String useReceiveGift;

        @Excel(name = "优惠")
        private BigDecimal discountAmount;

        @Excel(name = "小计")
        private BigDecimal subtotal;

        @Excel(name = "备注")
        private String remark;

    }

}
