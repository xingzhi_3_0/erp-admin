package com.xz.sales.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

@Data
public class SalesNoFlowVo {


    @Excel(name = "id")
    private Long id;

    @Excel(name = "returnDetailId")
    private Long returnDetailId;

    @Excel(name = "sales_no")
    private String salesNo;

    @Excel(name = "selling_num")
    private Integer sellingNum;


}
