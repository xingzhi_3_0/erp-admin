package com.xz.sales.vo;

import com.xz.sales.domain.SalesReturn;
import com.xz.sales.domain.SalesReturnDetail;
import com.xz.sales.param.SalesReturnDetailParam;
import lombok.Data;

import java.util.List;

@Data
public class SalesReturnVo extends SalesReturn {

    List<SalesReturnDetailParam> detailList;

    private String salesNo;

    private String patientName;

    private Integer sex;

    private String patientPhone;

    private Integer currAge;

    private String salesDeptName;

    private String userName;

    /*附件*/
    private List<String> voucherUrlList;

    private String voucherRemark;
}
