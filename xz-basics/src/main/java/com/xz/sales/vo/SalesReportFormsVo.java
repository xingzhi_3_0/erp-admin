package com.xz.sales.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class SalesReportFormsVo {

    private String salesNo;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private String userName;
    private String deptName;

    private String bizNo;

    private String bizId;

    private Integer bizType;

    private Integer bizStatus;

    private String patientName;

    private String patientPhone;

    private Integer age;

    private Long patientId;

}
