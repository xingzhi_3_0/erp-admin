package com.xz.sales.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.optometry.vo.OptometryInfoVo;
import com.xz.patient.domain.PatientInfo;
import com.xz.sales.domain.SalesOrder;
import com.xz.sales.domain.SalesOrderDetail;
import com.xz.sales.domain.SalesOriginalOrderDetail;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class SalesOrderVo extends SalesOrder {

    List<SalesOriginalOrderDetail> detailList;

    List<SalesOrderDetail> deliverDetailList;

    BigDecimal walletBalance;

    OptometryInfoVo optometryInfoVo;

    private BigDecimal walletRefundableAmount;
    private BigDecimal cashRefundableAmount;


    /** 0-不详 1-男 2-女 */
    @Excel(name = "0-不详 1-男 2-女")
    private Integer sex;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phoneNumber;

    /** 出生日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birthday;

    private String trackingNumber;

    /** 会员卡号 */
    @Excel(name = "会员卡号")
    private String memberCardNo;

    /** 交付时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "交付时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deliveryTime;
}
