package com.xz.sales.vo;

import com.xz.common.annotation.Excel;
import com.xz.sales.domain.SalesExchange;
import com.xz.sales.domain.SalesReturn;
import com.xz.sales.param.SalesExchangeDetailParam;
import com.xz.sales.param.SalesReturnDetailParam;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class SalesExchangeVo extends SalesExchange {

    List<SalesExchangeDetailParam> oldDetailList;

    List<SalesExchangeDetailParam> exchangeDetailList;

    BigDecimal walletBalance;

    private String salesNo;

    private String patientName;

    private Integer sex;

    private String patientPhone;

    private Integer currAge;

    private String salesDeptName;

    private String userName;
    /** 会员卡号 */
    private String memberCardNo;
    /*附件*/
    private List<String> voucherUrlList;

    private String voucherRemark;
}
