package com.xz.sales.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.optometry.vo.OptometryInfoVo;
import com.xz.sales.domain.SalesOrder;
import com.xz.sales.domain.SalesOrderDetail;
import com.xz.sales.domain.SalesOriginalOrderDetail;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class SalesServiceVo{

    private String salesNo;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private String userName;
    private String deptName;

    private String bizNo;

    private String bizId;

    private Integer bizType;

    private Integer bizStatus;

    private String patientName;

    private String patientPhone;

    private Integer age;

    private Long patientId;

}
