package com.xz.sales.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.sales.domain.SalesOrderDetail;
import org.apache.ibatis.annotations.Mapper;
import com.xz.sales.domain.SalesOriginalOrderDetail;
import org.apache.ibatis.annotations.Param;

/**
 * 销售单原始商品明细Mapper接口
 * 
 * @author xz
 * @date 2024-02-21
 */
@Mapper
public interface SalesOriginalOrderDetailMapper  extends BaseMapper<SalesOriginalOrderDetail> {
    /**
     * 查询销售单原始商品明细
     * 
     * @param id 销售单原始商品明细主键
     * @return 销售单原始商品明细
     */
    public SalesOriginalOrderDetail selectSalesOriginalOrderDetailById(Long id);

    /**
     * 查询销售单原始商品明细列表
     * 
     * @param salesOriginalOrderDetail 销售单原始商品明细
     * @return 销售单原始商品明细集合
     */
    public List<SalesOriginalOrderDetail> selectSalesOriginalOrderDetailList(SalesOriginalOrderDetail salesOriginalOrderDetail);

    List<SalesOriginalOrderDetail> selectBySalesId(@Param("salesId") Long salesId);

    SalesOriginalOrderDetail selectByDetailId(@Param("detailId") Long detailId);

    /**
     * 新增销售单原始商品明细
     * 
     * @param salesOriginalOrderDetail 销售单原始商品明细
     * @return 结果
     */
    public int insertSalesOriginalOrderDetail(SalesOriginalOrderDetail salesOriginalOrderDetail);

    /**
     * 修改销售单原始商品明细
     * 
     * @param salesOriginalOrderDetail 销售单原始商品明细
     * @return 结果
     */
    public int updateSalesOriginalOrderDetail(SalesOriginalOrderDetail salesOriginalOrderDetail);

    /**
     * 删除销售单原始商品明细
     * 
     * @param id 销售单原始商品明细主键
     * @return 结果
     */
    public int deleteSalesOriginalOrderDetailById(Long id);

    /**
     * 批量删除销售单原始商品明细
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSalesOriginalOrderDetailByIds(Long[] ids);
}
