package com.xz.sales.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.sales.param.SalesReturnDetailParam;
import org.apache.ibatis.annotations.Mapper;
import com.xz.sales.domain.SalesReturnDetail;

/**
 * 销售单退货明细Mapper接口
 * 
 * @author xz
 * @date 2024-02-07
 */
@Mapper
public interface SalesReturnDetailMapper  extends BaseMapper<SalesReturnDetail> {
    /**
     * 查询销售单退货明细
     * 
     * @param id 销售单退货明细主键
     * @return 销售单退货明细
     */
    public SalesReturnDetail selectSalesReturnDetailById(Long id);

    SalesReturnDetail selectByNewDetailId(Long detailId);

    /**
     * 查询销售单退货明细列表
     * 
     * @param salesReturnDetail 销售单退货明细
     * @return 销售单退货明细集合
     */
    public List<SalesReturnDetail> selectSalesReturnDetailList(SalesReturnDetail salesReturnDetail);

    public List<SalesReturnDetailParam> selectReturnDetailList(SalesReturnDetail salesReturnDetail);

    public List<SalesReturnDetailParam> selectSourceReturnDetailList(SalesReturnDetail salesReturnDetail);

    /**
     * 新增销售单退货明细
     * 
     * @param salesReturnDetail 销售单退货明细
     * @return 结果
     */
    public int insertSalesReturnDetail(SalesReturnDetail salesReturnDetail);

    /**
     * 修改销售单退货明细
     * 
     * @param salesReturnDetail 销售单退货明细
     * @return 结果
     */
    public int updateSalesReturnDetail(SalesReturnDetail salesReturnDetail);

    /**
     * 删除销售单退货明细
     * 
     * @param id 销售单退货明细主键
     * @return 结果
     */
    public int deleteSalesReturnDetailById(Long id);

    /**
     * 批量删除销售单退货明细
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSalesReturnDetailByIds(Long[] ids);
}
