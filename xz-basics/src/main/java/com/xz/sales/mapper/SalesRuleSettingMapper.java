package com.xz.sales.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.sales.domain.SalesReturn;
import com.xz.sales.domain.SalesRuleSetting;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 销售单退货Mapper接口
 * 
 * @author xz
 * @date 2024-02-05
 */
@Mapper
public interface SalesRuleSettingMapper extends BaseMapper<SalesRuleSetting> {

    /**
     * 查询销售单退货列表
     *
     * @param salesRuleSetting 销售单规则设置
     * @return 销售单退货集合
     */
    public List<SalesRuleSetting> selectSalesRuleSettingList(SalesRuleSetting salesRuleSetting);

    SalesRuleSetting queryMyRule(SalesRuleSetting salesRuleSetting);

    /**
     * 新增销售单退货
     *
     * @param salesRuleSetting 销售单规则设置
     * @return 结果
     */
    public int insertSalesRuleSetting(SalesRuleSetting salesRuleSetting);

    /**
     * 修改销售单退货
     *
     * @param salesRuleSetting 销售单规则设置
     * @return 结果
     */
    public int updateSalesRuleSetting(SalesRuleSetting salesRuleSetting);


    int deleteByDeptId(@Param("deptId")Long deptId);


}
