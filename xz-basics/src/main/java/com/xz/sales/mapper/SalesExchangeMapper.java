package com.xz.sales.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.sales.vo.SalesExchangeVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.sales.domain.SalesExchange;

/**
 * 销售单换货Mapper接口
 * 
 * @author xz
 * @date 2024-02-05
 */
@Mapper
public interface SalesExchangeMapper  extends BaseMapper<SalesExchange> {
    /**
     * 查询销售单换货
     * 
     * @param id 销售单换货主键
     * @return 销售单换货
     */
    public SalesExchangeVo selectSalesExchangeById(Long id);

    /**
     * 查询销售单换货列表
     * 
     * @param salesExchange 销售单换货
     * @return 销售单换货集合
     */
    public List<SalesExchange> selectSalesExchangeList(SalesExchange salesExchange);

    /**
     * 新增销售单换货
     * 
     * @param salesExchange 销售单换货
     * @return 结果
     */
    public int insertSalesExchange(SalesExchange salesExchange);

    /**
     * 修改销售单换货
     * 
     * @param salesExchange 销售单换货
     * @return 结果
     */
    public int updateSalesExchange(SalesExchange salesExchange);

    /**
     * 删除销售单换货
     * 
     * @param id 销售单换货主键
     * @return 结果
     */
    public int deleteSalesExchangeById(Long id);

    /**
     * 批量删除销售单换货
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSalesExchangeByIds(Long[] ids);
}
