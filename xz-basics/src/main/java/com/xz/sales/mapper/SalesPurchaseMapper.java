package com.xz.sales.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.purchase.domain.PurchaseProduct;
import org.apache.ibatis.annotations.Mapper;
import com.xz.sales.domain.SalesPurchase;

/**
 * 销售采购记录Mapper接口
 * 
 * @author xz
 * @date 2024-02-29
 */
@Mapper
public interface SalesPurchaseMapper  extends BaseMapper<SalesPurchase> {
    /**
     * 查询销售采购记录
     * 
     * @param id 销售采购记录主键
     * @return 销售采购记录
     */
    public SalesPurchase selectSalesPurchaseById(Long id);

    /**
     * 查询销售采购记录列表
     * 
     * @param salesPurchase 销售采购记录
     * @return 销售采购记录集合
     */
    public List<SalesPurchase> selectSalesPurchaseList(SalesPurchase salesPurchase);

    List<PurchaseProduct> selectPurchaseList(SalesPurchase salesPurchase);

    /**
     * 新增销售采购记录
     * 
     * @param salesPurchase 销售采购记录
     * @return 结果
     */
    public int insertSalesPurchase(SalesPurchase salesPurchase);

    /**
     * 修改销售采购记录
     * 
     * @param salesPurchase 销售采购记录
     * @return 结果
     */
    public int updateSalesPurchase(SalesPurchase salesPurchase);

    int deleteByInfo(SalesPurchase salesPurchase);

    int updateByInfo(SalesPurchase salesPurchase);

    /**
     * 删除销售采购记录
     * 
     * @param id 销售采购记录主键
     * @return 结果
     */
    public int deleteSalesPurchaseById(Long id);

    /**
     * 批量删除销售采购记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSalesPurchaseByIds(Long[] ids);
}
