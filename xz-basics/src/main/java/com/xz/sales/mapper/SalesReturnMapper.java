package com.xz.sales.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.report.param.SalesReportParam;
import com.xz.report.vo.SalesReportVo;
import com.xz.report.vo.SalesReturnReportVo;
import com.xz.sales.vo.SalesReturnVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.sales.domain.SalesReturn;

/**
 * 销售单退货Mapper接口
 * 
 * @author xz
 * @date 2024-02-05
 */
@Mapper
public interface SalesReturnMapper  extends BaseMapper<SalesReturn> {
    /**
     * 查询销售单退货
     * 
     * @param id 销售单退货主键
     * @return 销售单退货
     */
    public SalesReturn selectSalesReturnById(Long id);

    /**
     * 查询销售单退货
     *
     * @param id 销售单退货主键
     * @return 销售单退货
     */
    public SalesReturnVo getSalesReturnInfo(Long id);

    /**
     * 查询销售单退货列表
     * 
     * @param salesReturn 销售单退货
     * @return 销售单退货集合
     */
    public List<SalesReturn> selectSalesReturnList(SalesReturn salesReturn);

    /**
     * 新增销售单退货
     * 
     * @param salesReturn 销售单退货
     * @return 结果
     */
    public int insertSalesReturn(SalesReturn salesReturn);

    /**
     * 修改销售单退货
     * 
     * @param salesReturn 销售单退货
     * @return 结果
     */
    public int updateSalesReturn(SalesReturn salesReturn);

    /**
     * 删除销售单退货
     * 
     * @param id 销售单退货主键
     * @return 结果
     */
    public int deleteSalesReturnById(Long id);

    /**
     * 批量删除销售单退货
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSalesReturnByIds(Long[] ids);

    public List<SalesReturnReportVo> salesReturnReport(SalesReportParam salesReportParam);

    SalesReturnReportVo sumSalesReturnReportData(SalesReportParam salesReportParam);

}
