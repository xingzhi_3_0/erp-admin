package com.xz.sales.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.sales.domain.SalesOrder;
import com.xz.sales.param.SalesDetailParam;
import org.apache.ibatis.annotations.Mapper;
import com.xz.sales.domain.SalesOrderDetail;
import org.apache.ibatis.annotations.Param;

/**
 * 销售单商品明细Mapper接口
 * 
 * @author xz
 * @date 2024-01-31
 */
@Mapper
public interface SalesOrderDetailMapper  extends BaseMapper<SalesOrderDetail> {
    /**
     * 查询销售单商品明细
     * 
     * @param id 销售单商品明细主键
     * @return 销售单商品明细
     */
    public SalesOrderDetail selectSalesOrderDetailById(Long id);



    /**
     * 查询销售单商品明细列表
     * 
     * @param salesOrderDetail 销售单商品明细
     * @return 销售单商品明细集合
     */
    public List<SalesOrderDetail> selectSalesOrderDetailList(SalesOrderDetail salesOrderDetail);

    List<SalesOrderDetail> selectBySalesId(@Param("salesId") Long salesId);


    List<SalesOrderDetail> selectListBySales(SalesOrder salesOrder);

    List<SalesOrderDetail> chooseDetailList(SalesDetailParam salesDetailParam);

    /**
     * 新增销售单商品明细
     * 
     * @param salesOrderDetail 销售单商品明细
     * @return 结果
     */
    public int insertSalesOrderDetail(SalesOrderDetail salesOrderDetail);

    /**
     * 修改销售单商品明细
     * 
     * @param salesOrderDetail 销售单商品明细
     * @return 结果
     */
    public int updateSalesOrderDetail(SalesOrderDetail salesOrderDetail);

    /**
     * 删除销售单商品明细
     * 
     * @param id 销售单商品明细主键
     * @return 结果
     */
    public int deleteSalesOrderDetailById(Long id);

    /**
     * 批量删除销售单商品明细
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSalesOrderDetailByIds(Long[] ids);
}
