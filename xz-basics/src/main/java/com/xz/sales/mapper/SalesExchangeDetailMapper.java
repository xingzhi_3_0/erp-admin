package com.xz.sales.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.sales.domain.SalesOrderDetail;
import com.xz.sales.param.SalesExchangeDetailParam;
import org.apache.ibatis.annotations.Mapper;
import com.xz.sales.domain.SalesExchangeDetail;
import org.apache.ibatis.annotations.Param;

/**
 * 销售单换货明细Mapper接口
 * 
 * @author xz
 * @date 2024-02-21
 */
@Mapper
public interface SalesExchangeDetailMapper  extends BaseMapper<SalesExchangeDetail> {
    /**
     * 查询销售单换货明细
     * 
     * @param id 销售单换货明细主键
     * @return 销售单换货明细
     */
    public SalesExchangeDetail selectSalesExchangeDetailById(Long id);

    public SalesExchangeDetail selectOldReturnDetail(Long oldDetailId);

    List<SalesExchangeDetailParam> oldDetailList(Long exchangeId);

    List<SalesExchangeDetailParam> exchangeDetailList(Long exchangeId);

    /**
     * 查询销售单换货明细列表
     * 
     * @param salesExchangeDetail 销售单换货明细
     * @return 销售单换货明细集合
     */
    public List<SalesExchangeDetail> selectSalesExchangeDetailList(SalesExchangeDetail salesExchangeDetail);


    List<SalesOrderDetail> selectBySalesExchangeId(@Param("exchangeId") Long exchangeId);

    /**
     * 新增销售单换货明细
     * 
     * @param salesExchangeDetail 销售单换货明细
     * @return 结果
     */
    public int insertSalesExchangeDetail(SalesExchangeDetail salesExchangeDetail);

    /**
     * 修改销售单换货明细
     * 
     * @param salesExchangeDetail 销售单换货明细
     * @return 结果
     */
    public int updateSalesExchangeDetail(SalesExchangeDetail salesExchangeDetail);

    /**
     * 删除销售单换货明细
     * 
     * @param id 销售单换货明细主键
     * @return 结果
     */
    public int deleteSalesExchangeDetailById(Long id);

    /**
     * 批量删除销售单换货明细
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSalesExchangeDetailByIds(Long[] ids);
}
