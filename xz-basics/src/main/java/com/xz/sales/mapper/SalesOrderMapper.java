package com.xz.sales.mapper;

import java.math.BigDecimal;
import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.report.param.SalesReportParam;
import com.xz.report.vo.SalesFlowReportVo;
import com.xz.sales.vo.SalesOrderDetailVo;
import com.xz.report.vo.SalesReportVo;
import com.xz.sales.vo.SalesServiceVo;
import com.xz.staging.vo.FoldLineStagingVo;
import com.xz.staging.vo.FrameLensesRangeVo;
import com.xz.staging.vo.FrameLensesStagingVo;
import com.xz.staging.vo.WorkbenchesVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.sales.domain.SalesOrder;
import org.apache.ibatis.annotations.Param;

/**
 * 销售单信息Mapper接口
 *
 * @author xz
 * @date 2024-01-31
 */
@Mapper
public interface SalesOrderMapper  extends BaseMapper<SalesOrder> {
    /**
     * 查询销售单信息
     *
     * @param id 销售单信息主键
     * @return 销售单信息
     */
    public SalesOrder selectSalesOrderById(Long id);

    /**
     * 查询销售单信息列表
     *
     * @param salesOrder 销售单信息
     * @return 销售单信息集合
     */
    public List<SalesOrder> selectSalesOrderList(SalesOrder salesOrder);

    List<SalesOrderDetailVo> selectOrderDetailList(SalesOrder salesOrder);

    List<SalesServiceVo> serviceList(SalesOrder salesOrder);

    /**
     * 新增销售单信息
     *
     * @param salesOrder 销售单信息
     * @return 结果
     */
    public int insertSalesOrder(SalesOrder salesOrder);

    /**
     * 修改销售单信息
     *
     * @param salesOrder 销售单信息
     * @return 结果
     */
    public int updateSalesOrder(SalesOrder salesOrder);

  /**
   * 根据id更新订单状态
   * @param id
   * @param status
   * @return
   */
    int updateOrderStatus(@Param("id") Long id, @Param("orderStatus") Integer status);

    /**
     * 为了积分根据id更新订单状态
     * @param id
     * @param status
     * @return
     */
    int updateStatusForDeductionIntegral(@Param("id") Long id, @Param("orderStatus") Integer status,@Param("refundIntegral") BigDecimal refundIntegral);

    /**
     * 删除销售单信息
     *
     * @param id 销售单信息主键
     * @return 结果
     */
    public int deleteSalesOrderById(Long id);

    /**
     * 批量删除销售单信息
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSalesOrderByIds(Long[] ids);

    public List<SalesReportVo> salesReport(SalesReportParam salesReportParam);

    SalesReportVo sumSalesReportData(SalesReportParam salesReportParam);

    List<SalesFlowReportVo> salesFlowReport(SalesReportParam salesReportParam);

    WorkbenchesVo salesWorkbenches(SalesReportParam salesReportParam);

    List<WorkbenchesVo> hotSalesProduct(SalesReportParam salesReportParam);

    List<FoldLineStagingVo> foldLineStatistics(SalesReportParam salesReportParam);

    List<FoldLineStagingVo> foldLineMonthList(SalesReportParam salesReportParam);

    List<FrameLensesStagingVo> frameLensesList(SalesReportParam salesReportParam);

    List<FrameLensesStagingVo> frameLensesRangeList(SalesReportParam salesReportParam);

    List<FrameLensesRangeVo> rangeSalesDetailList(SalesReportParam salesReportParam);

}
