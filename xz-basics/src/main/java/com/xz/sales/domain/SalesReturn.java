package com.xz.sales.domain;

import java.math.BigDecimal;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 销售单退货对象 t_sales_return
 *
 * @author xz
 * @date 2024-02-05
 */
@Data
@TableName("t_sales_return")
public class SalesReturn extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 退货单号 */
    @Excel(name = "退货单号")
    private String returnNo;

    /** 1-仅退款 2-退货退款 */
    @Excel(name = "退货类型")
    private Integer returnType;

    /** 销售单id */
    @Excel(name = "销售单id")
    private Long salesOrderId;

    /** 退款金额 */
    @Excel(name = "退款金额")
    private BigDecimal returnAmount;

    /** 退款钱包金额 */
    @Excel(name = "钱包金额")
    private BigDecimal walletAmount;

    /** 返还积分  */
    @Excel(name = "返还积分")
    private BigDecimal refundIntegral;

    /** 退款现金金额 */
    @Excel(name = "现金金额")
    private BigDecimal cashAmount;

    /** 退货状态（1.待退款 2.已退款 3.已取消） */
    @Excel(name = "退货状态", readConverterExp = "1=.待退款,2=.已退款,3=.已取消")
    private Integer returnStatus;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 部门id(门店id) */
    @Excel(name = "部门id(门店id)")
    private Long deptId;

}
