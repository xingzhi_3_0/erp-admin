package com.xz.sales.domain;

import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 销售采购记录对象 t_sales_purchase
 * 
 * @author xz
 * @date 2024-02-29
 */
@Data
@TableName("t_sales_purchase")
public class SalesPurchase {
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 销售id */
    @Excel(name = "销售id")
    private Long salesId;

    /** 明细id */
    @Excel(name = "明细id")
    private Long detailId;

    /** 采购入库id */
    @Excel(name = "采购入库id")
    private Long purchaseProductId;

    /** 销售数量 */
    @Excel(name = "销售数量")
    private Integer purchaseProductNum;

}
