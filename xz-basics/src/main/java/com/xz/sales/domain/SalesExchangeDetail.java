package com.xz.sales.domain;

import java.math.BigDecimal;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 销售单换货明细对象 t_sales_exchange_detail
 * 
 * @author xz
 * @date 2024-02-21
 */
@Data
@TableName("t_sales_exchange_detail")
public class SalesExchangeDetail {
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 换货单ID */
    @Excel(name = "换货单ID")
    private Long salesExchangeId;

    /** 销售单id */
    @Excel(name = "销售单id")
    private Long salesOrderId;

    /** 源头销售单明细id */
    @Excel(name = "源头销售单明细id")
    private Long salesSourceDetailId;

    /** 源头拆分的销售单明细id */
    @Excel(name = "源头拆分的销售单明细id")
    private Long oldSalesOrderDetailId;

    /** 新换的销售单明细id */
    @Excel(name = "新换的销售单明细id")
    private Long newSalesOrderDetailId;

    /** 差价 */
    @Excel(name = "差价")
    private BigDecimal differenceAmount;
    @Excel(name = "换货仓库id")
    private Long exchangeWarehouseId;
    @Excel(name = "换货仓库名称")
    private String exchangeWarehouseName;
    @Excel(name = "换货单价")
    private BigDecimal exchangePrice;
    @Excel(name = "换货数量")
    private Integer exchangeNum;

    private Integer salesSourceDetailStatus;

    @Excel(name = "换货退回仓库id")
    private Long detailReturnWarehouseId;
    @Excel(name = "换货退回仓库名称")
    private String detailReturnWarehouseName;

}
