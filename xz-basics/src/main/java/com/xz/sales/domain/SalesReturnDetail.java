package com.xz.sales.domain;

import java.math.BigDecimal;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 销售单退货明细对象 t_sales_return_detail
 * 
 * @author xz
 * @date 2024-02-07
 */
@Data
@TableName("t_sales_return_detail")
public class SalesReturnDetail{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 退货单ID */
    @Excel(name = "退货单ID")
    private Long salesReturnId;

    /** 销售单id */
    @Excel(name = "销售单id")
    private Long salesOrderId;

    /** 销售单明细id */
    @Excel(name = "销售单明细id")
    private Long salesOrderDetailId;

    /** 原销售单明细id */
    @Excel(name = "原销售单明细id")
    private Long sourceSalesDetailId;

    private Integer sourceSalesDetailStatus;

    /** 退货仓库id */
    @Excel(name = "退货仓库id")
    private Long warehouseId;

    /** 退货仓库 */
    @Excel(name = "退货仓库")
    private String warehouseName;

    /** 退货单价 */
    @Excel(name = "退货单价")
    private BigDecimal returnPrice;

    /** 退货数量 */
    @Excel(name = "退货数量")
    private Integer returnNum;

    /** 退货金额 */
    @Excel(name = "退货金额")
    private BigDecimal returnAmount;

}
