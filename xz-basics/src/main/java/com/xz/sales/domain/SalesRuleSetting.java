package com.xz.sales.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 采购库存调整对象 t_adjust_info
 * 
 * @author xz
 * @date 2024-2-18 16:02:08
 */
@Data
@TableName("t_sales_rule_setting")
public class SalesRuleSetting extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 规则序号 第几个序号 */
    private Integer ruleSerialNo;

    /** 规则值 */
    private String ruleValue;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 部门id */
    private Long deptId;

    /** 租户id */
    private Long tenantId;

}
