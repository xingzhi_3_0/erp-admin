package com.xz.sales.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 销售单换货对象 t_sales_exchange
 * 
 * @author xz
 * @date 2024-02-05
 */
@Data
@TableName("t_sales_exchange")
public class SalesExchange extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 换货单号 */
    @Excel(name = "换货单号")
    private String exchangeNo;

    /** 销售单id */
    @Excel(name = "销售单id")
    private Long salesOrderId;

    /** 退款金额 */
    @Excel(name = "换货金额")
    private BigDecimal exchangeAmount;
    /** 差额 */
    @Excel(name = "差额")
    private BigDecimal priceDifference;
    /** 减免金额 */
    @Excel(name = "减免金额")
    private BigDecimal reductionAmount;

    /** 换货状态（1.待确认 2.已换货 3-已取消） */
    @Excel(name = "换货状态", readConverterExp = "1=.待确认,2=.已换货,3=.已取消")
    private Integer exchangeStatus;

    /** 钱包支付金额 */
    @Excel(name = "钱包支付金额")
    private BigDecimal walletPay;

    /** 钱包支付外金额 */
    @Excel(name = "钱包支付外金额")
    private BigDecimal pendingAmount;

    /** 支付方式 1-现金 2-银行卡 3-E支付  */
    @Excel(name = "1-现金 2-银行卡 3-E支付  ")
    private Integer paymentType;

    /** 付款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "付款时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date paymentTime;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 部门id(门店id) */
    @Excel(name = "部门id(门店id)")
    private Long deptId;

}
