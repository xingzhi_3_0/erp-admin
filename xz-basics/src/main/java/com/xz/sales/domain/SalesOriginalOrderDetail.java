package com.xz.sales.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;

import com.xz.member.domain.GiftReceive;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 销售单原始商品明细对象 t_sales_original_order_detail
 * 
 * @author xz
 * @date 2024-02-21
 */
@Data
@TableName("t_sales_original_order_detail")
public class SalesOriginalOrderDetail extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 销售单ID */
    @Excel(name = "销售单ID")
    private Long salesId;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;

    /** 商品参数 */
    @Excel(name = "商品参数")
    private String productParam;

    /** 生产批号 */
    @Excel(name = "生产批号")
    private String batchNumber;

    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "有效期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date validity;

    /** 单位 */
    @Excel(name = "单位")
    private String unit;

    /** 仓库id */
    @Excel(name = "仓库id")
    private Long warehouseId;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String warehouseName;

    /** 是否上传附件 0-否 1-是 */
    @Excel(name = "是否上传附件 0-否 1-是")
    private Integer isUploadFile;

    /** 附件地址 */
    @Excel(name = "附件地址")
    private String fileUrl;

    /** 商品采购类型 (1-库存 2-定制采购) */
    @Excel(name = "商品采购类型 (1-库存 2-定制采购)")
    private Integer productPurchaseType;

    /** 状态（0.不加工 1.内加工） */
    @Excel(name = "状态", readConverterExp = "0=.不加工,1=.内加工")
    private Integer machiningType;

    /** 使用会员卡记录 */
    @Excel(name = "使用会员卡记录")
    private String useReceiveGift;

    @TableField(exist = false)
    private List<GiftReceive> useGiftReceiveList;

    /** 会员卡抵扣金额总计 */
    @Excel(name = "会员卡抵扣金额总计")
    private BigDecimal giftDeductionTotal;

    /** 销售单价 */
    @Excel(name = "销售单价")
    private BigDecimal sellingPrice;

    /** 销售数量 */
    @Excel(name = "销售数量")
    private Integer sellingNum;

    /** 优惠金额 */
    @Excel(name = "优惠金额")
    private BigDecimal discountAmount;

    /** 小计 */
    @Excel(name = "小计")
    private BigDecimal subtotal;

    /** 商品状态（1-待付款 2-待移交加工 3-待上传参数 4-待采购 5-采购中 6-采购终止 7-派送中 8-待加工 9-加工中 10-待签收 11-待交付 12-已交付 13-待退款 14-待退货退款 15-待换货 16-仅退款 17-退货退款 18-已换货  ） */
    @Excel(name = "商品状态", readConverterExp = "1=-待付款,2=-待移交加工,3=-待上传参数,4=-待采购,5=-采购中,6=-采购终止,7=-派送中,8=-待加工,9=-加工中,1=0-待签收,1=1-待交付,1=2-已交付,1=3-待退款,1=4-待退货退款,1=5-待换货,1=6-仅退款,1=7-退货退款,1=8-已换货")
    private Integer productStatus;

    /** 售后状态（1.待退费 2.已退费） */
    @Excel(name = "售后状态", readConverterExp = "1=.待退费,2=.已退费")
    private Integer serviceStatus;

    /** 交付时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "交付时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deliveryTime;

    /** 交付门店id */
    @Excel(name = "交付门店id")
    private Long deliveryDeptId;

    private String deliveryDeptName;

    /** 商品id */
    @Excel(name = "商品id")
    private Long productId;

    private Long detailId;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    @NotNull(message = "租户id不能为空")
    private Long tenantId;

    /** 部门id(门店id) */
    @Excel(name = "部门id(门店id)")
    @NotNull(message = "部门id(门店id)不能为空")
    private Long deptId;

}
