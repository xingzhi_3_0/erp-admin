package com.xz.sales.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 销售单信息对象 t_sales_order
 *
 * @author xz
 * @date 2024-01-31
 */
@Data
@TableName("t_sales_order")
public class SalesOrder extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 销售单号 */
    @Excel(name = "销售单号")
    private String salesNo;

    /** 当前年龄 */
    @Excel(name = "当前年龄")
    private Integer currentAge;

    /** 患者id */
    @Excel(name = "患者id")
    private Long patientId;

    /** 患者名称 */
    @Excel(name = "患者名称")
    private String patientName;

    /** 患者手机号 */
    @Excel(name = "患者手机号")
    private String patientPhone;

    /** 销售员id */
    @Excel(name = "销售员id")
    private Long salesUserId;

    /** 销售员名称 */
    @Excel(name = "销售员名称")
    private String salesUserName;

    /** 处方id */
    @Excel(name = "处方id")
    private Long optometryId;

    /** 商品总金额 */
    @Excel(name = "商品总金额")
    private BigDecimal productTotalAmount;

    /** 收款金额 */
    @Excel(name = "收款金额")
    private BigDecimal collectionAmount;

    /** 钱包支付金额 */
    @Excel(name = "钱包支付金额")
    private BigDecimal walletPay;

    /** 钱包支付外金额 */
    @Excel(name = "钱包支付外金额")
    private BigDecimal pendingAmount;

    @Excel(name = "减价金额")
    private BigDecimal reduceAmount;
    @Excel(name = "打折后金额")
    private BigDecimal afterDiscountAmount;

    /** 支付方式 1-现金 2-银行卡 3-E支付  */
    @Excel(name = "1-现金 2-银行卡 3-E支付  ")
    private Integer paymentType;

    /** 付款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "付款时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date paymentTime;

    /** 会员卡号 */
    @Excel(name = "会员卡号")
    private String memberCardNo;

    /** 1-会员折扣 2-积分抵扣 3-手动折扣  */
    @Excel(name = "1-会员折扣 2-积分抵扣 3-手动折扣 ")
    private Integer preferentialType;

    /** 赠品抵扣金额  */
    @Excel(name = "赠品抵扣金额")
    private BigDecimal giftDeductionAmount;

    /** 优惠金额  */
    @Excel(name = "优惠金额")
    private BigDecimal preferentialAmount;

    @Excel(name = "优惠抵扣积分")
    private BigDecimal deductionIntegral;

    /** 剩余积分  */
    private BigDecimal residualIntegral;

    @Excel(name = "优惠折扣")
    private BigDecimal discount;

    /** 订单状态状态（1-待付款,2-销售处理,3-未交付,4-已交付,5-售后中,6-已售后 7-已取消） */
    @Excel(name = "商品状态状态", readConverterExp = "1=.待付款,2=.销售处理,3=.未交付,4=.已交付,5=.售后中,6=.已售后")
    private Integer orderStatus;

    /** 是否提供镜框（1.是 2.否） */
    @Excel(name = "客户是否自备镜框", readConverterExp = "1=.是,2=.否")
    private Integer isProvideOneself;

    /** 提供镜框 */
    @Excel(name = "提供镜框")
    private String provideFrames;

    /** 交付方式（1.门店自取 2.邮寄） */
    @Excel(name = "交付方式", readConverterExp = "1=.门店自取,2=.邮寄")
    private Integer deliveryMethod;


    /** 是否打印（0.否 1.是） */
    @Excel(name = "是否打印", readConverterExp = "0=.否,1=.是")
    private Integer isPrint;

    /** 售价审批状态（0.无需审批 1.待审批 2.审批通过 3.审批未通过） */
    @Excel(name = "售价审批状态", readConverterExp = "0=.无需审批,1=.待审批,2=.审批通过,3=.审批未通过")
    private Integer priceApprovalStatus;

    /** 审批意见 */
    @Excel(name = "审批意见")
    private String approvalOpinions;

    /** 邮寄地址 */
    @Excel(name = "邮寄地址")
    private String mailingAddress;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    private Long tenantId;

    /** 部门id(门店id) */
    private Long deptId;

    @TableField(exist = false)
    private String salesDeptName;

    @TableField(exist = false)
    private String queryDeptIdListStr;

    @TableField(exist = false)
    private String searchPatient;

    /** 关联门店id */
    private Long collaborateDeptId;

    @TableField(exist = false)
    private String collaborateDeptName;

    /** 0-不详 1-男 2-女 */
    @Excel(name = "0-不详 1-男 2-女")
    @TableField(exist = false)
    private Integer sex;

    /**
     * 状态
     */
    @TableField(exist = false)
    public List<Integer> statusList;

    /**
     * 单个交付销售明细id
     */
    @TableField(exist = false)
    private Long orderDetailId;

    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;
    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
    /** 售后编号（目前仅售后管理查询列表时用） */
    @TableField(exist = false)
    private String afterSalesNo;
    /** 售后门店Id（目前仅售后管理查询列表时用）*/
    @TableField(exist = false)
    private Long afterSalesDeptId;
    /** 售后门店（目前仅售后管理查询列表时用） */
    @TableField(exist = false)
    private String afterSalesDeptName;
    /** 售后类型（目前仅售后管理查询列表时用） */
    @TableField(exist = false)
    private Integer bizType;
    @TableField(exist = false)
    private String patientInfo;
}
