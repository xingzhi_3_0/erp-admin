package com.xz.sales.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;

import com.xz.member.domain.GiftReceive;
import com.xz.purchase.domain.PurchaseProductParam;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 销售单商品明细对象 t_sales_order_detail
 *
 * @author xz
 * @date 2024-01-31
 */
@Data
@TableName("t_sales_order_detail")
public class SalesOrderDetail extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 销售单ID */
    @Excel(name = "销售单号")
    private Long salesId;

    /** 销售单ID */
    @Excel(name = "销售单编号")
    private String salesNo;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;

    /** 商品参数 */
    @Excel(name = "商品参数")
    private String productParam;

    /** 生产批号 */
    @Excel(name = "生产批号")
    private String batchNumber;

    /** 有效期 */
    @Excel(name = "有效期")
    private Date validity;

    /** 单位 */
    @Excel(name = "单位")
    private String unit;

    /** 明细类型：0-销售明细 1-换货 2-退货 */
    private Integer detailType;

    /**
     * 采购商品属性
     */
    @TableField(exist = false)
    private List<PurchaseProductParam> purchaseProductParamList;

    @TableField(exist = false)
    private List<Long> giftReceiveList;
    @TableField(exist = false)
    private List<GiftReceive> useGiftReceiveList;
    @TableField(exist = false)
    private Long categoryId;

    private BigDecimal giftDeductionTotal;

    private String useReceiveGift;

    /** 仓库id */
    @Excel(name = "仓库id")
    private Long warehouseId;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String warehouseName;

    /** 退货仓库名称 */
    @TableField(exist = false)
    private String returnWarehouseName;

    /** 状态（0.不加工 1.内加工） */
    @Excel(name = "状态", readConverterExp = "0=.不加工,1=.内加工")
    private Integer machiningType;

    @TableField(exist = false)
    private String machiningTypeStr;

    /** 销售商品类型（1-库存 2-定制采购） */
    @Excel(name = "销售商品类型", readConverterExp = "1=.库存,2=.定制采购")
    private Integer productPurchaseType;

    /** 上传附件 */
    private String fileUrl;

    private String fileName;

    private Integer isUploadFile;

    /** 销售单价 */
    @Excel(name = "销售单价")
    private BigDecimal sellingPrice;

    /** 销售数量 */
    @Excel(name = "销售数量")
    private Integer sellingNum;

    /** 优惠金额 */
    @Excel(name = "优惠金额")
    private BigDecimal discountAmount;

    @Excel(name = "减免金额")
    private BigDecimal reductionAmount;

    /** 小计 */
    @Excel(name = "小计")
    private BigDecimal subtotal;

    /** 商品状态（1-待付款 2-待移交加工 3-待上传参数 4-待采购 5-采购中 6-采购终止 7-派送中 8-待加工 9-加工中 10-待签收 11-待交付 12-已交付 13-待退款 14-待退货退款 15-待换货 16-仅退款 17-退货退款 18-已换货 19-已取消 20-退换货取消） */
    private Integer productStatus;

    /** 售后状态（1.待退费 2.已退费） */
    @Excel(name = "售后状态", readConverterExp = "0=.待退费,1=.已退费")
    private Integer serviceStatus;

    /** 交付时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "交付时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deliveryTime;

    /** 移交方式（1.人工移交 2.邮寄） */
    private Integer transferMethod;
    /** 移交信息 */
    private String transferInfo;

    /** 交付门店id */
    @Excel(name = "交付门店id", width = 30)
    private Long deliveryDeptId;

    /** 交付门店id */
    @TableField(exist = false)
    private String deliveryDeptName;

    @TableField(exist = false)
    private String urlPath;

    /** 商品id */
    @Excel(name = "商品id")
    private Long productId;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 部门id(门店id) */
    @Excel(name = "部门id(门店id)")
    private Long deptId;

    private String beforeTreatmentNo;

    private String afterTreatmentNo;

    @TableField(exist = false)
    private String signer;
    @TableField(exist = false)
    private String trackingNumber;

    /**
     * 上传文件类型 1-上传附件 2-更新附件
     */
    @TableField(exist = false)
    private Integer uploadFileType;

    /**
     *  实际小计
     *
     *  确认销售付款商品 >>  实际小计 = 小计
     *  确认仅退款，退货退款商品 >> 实际小计 = 0
     *  确认换货--换前的商品 >> 实际小计 = 0
     *  确认换货--换后的商品 >> 实际小计 = 换前商品的实际小计 + 换货补差价
     *
     *  默认为0,用于统计售出成功的商品金额，减少连表及状态，类型的逻辑判断。
     * */
    //@TableField(exist = false)
    private BigDecimal actualSubtotal;

    /** 配送时间（交付时间） */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "加工单配送时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date processDeliveryTime;
}
