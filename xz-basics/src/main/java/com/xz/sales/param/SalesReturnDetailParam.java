package com.xz.sales.param;

import com.xz.sales.domain.SalesOrderDetail;
import lombok.Data;

import java.math.BigDecimal;


@Data
public class SalesReturnDetailParam extends SalesOrderDetail {

    private Long returnWarehouseId;

    private String returnWarehouseName;

    private BigDecimal returnPrice;

    private Integer returnNum;

    private BigDecimal returnAmount;

    private Long salesOrderDetailId;
}
