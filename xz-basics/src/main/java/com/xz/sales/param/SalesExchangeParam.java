package com.xz.sales.param;

import com.xz.sales.domain.SalesExchange;
import com.xz.sales.domain.SalesReturn;
import lombok.Data;

import java.util.List;

@Data
public class SalesExchangeParam extends SalesExchange {

    List<SalesExchangeDetailParam> exchangeDetailParamList;

}
