package com.xz.sales.param;

import com.xz.sales.domain.SalesReturn;
import lombok.Data;

import java.util.List;

@Data
public class SalesDetailParam {

    private Integer queryType;

    private Long salesId;

    private List<Integer> statusList;

}
