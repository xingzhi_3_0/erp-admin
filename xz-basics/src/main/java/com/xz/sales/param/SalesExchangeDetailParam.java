package com.xz.sales.param;

import com.xz.sales.domain.SalesOrderDetail;
import lombok.Data;

import java.math.BigDecimal;


@Data
public class SalesExchangeDetailParam extends SalesOrderDetail {

    private Long exchangeWarehouseId;

    private String exchangeWarehouseName;

    private BigDecimal exchangePrice;

    private Integer exchangeNum;

    private BigDecimal differenceAmount;

    private Long salesOrderDetailId;

    private Long detailReturnWarehouseId;

    private String detailReturnWarehouseName;

    private String oldProductName;

    private String oldProductParam;

    private String oldBatchNumber;

    private String oldValidity;

    private String oldSubtotal;
}
