package com.xz.sales.param;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xz.sales.domain.SalesOrder;
import com.xz.sales.domain.SalesOrderDetail;
import com.xz.sales.domain.SalesReturn;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class SalesReturnParam extends SalesReturn {

    List<SalesReturnDetailParam> returnDetailParamList;

    /*附件*/
    private List<String> voucherUrlList;

    private String voucherRemark;
}
