package com.xz.sales.param;

import com.xz.sales.domain.SalesOrder;
import com.xz.sales.domain.SalesOrderDetail;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class SalesOrderParam extends SalesOrder {

    List<SalesOrderDetail> detailList;

}
