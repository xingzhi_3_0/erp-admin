package com.xz.sales.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.sales.mapper.SalesExchangeDetailMapper;
import com.xz.sales.domain.SalesExchangeDetail;
import com.xz.sales.service.ISalesExchangeDetailService;

/**
 * 销售单换货明细Service业务层处理
 *
 * @author xz
 * @date 2024-02-21
 */
@Service
public class SalesExchangeDetailServiceImpl  extends ServiceImpl<SalesExchangeDetailMapper, SalesExchangeDetail> implements ISalesExchangeDetailService
{
    @Autowired
    private SalesExchangeDetailMapper salesExchangeDetailMapper;

    /**
     * 查询销售单换货明细
     *
     * @param id 销售单换货明细主键
     * @return 销售单换货明细
     */
    @Override
    public SalesExchangeDetail selectSalesExchangeDetailById(Long id)
    {
        return salesExchangeDetailMapper.selectSalesExchangeDetailById(id);
    }

    /**
     * 查询销售单换货明细列表
     *
     * @param salesExchangeDetail 销售单换货明细
     * @return 销售单换货明细
     */
    @Override
    public List<SalesExchangeDetail> selectSalesExchangeDetailList(SalesExchangeDetail salesExchangeDetail)
    {
        return salesExchangeDetailMapper.selectSalesExchangeDetailList(salesExchangeDetail);
    }

    /**
     * 新增销售单换货明细
     *
     * @param salesExchangeDetail 销售单换货明细
     * @return 结果
     */
    @Override
    public int insertSalesExchangeDetail(SalesExchangeDetail salesExchangeDetail)
    {
        return salesExchangeDetailMapper.insertSalesExchangeDetail(salesExchangeDetail);
    }

    /**
     * 修改销售单换货明细
     *
     * @param salesExchangeDetail 销售单换货明细
     * @return 结果
     */
    @Override
    public int updateSalesExchangeDetail(SalesExchangeDetail salesExchangeDetail)
    {
        return salesExchangeDetailMapper.updateSalesExchangeDetail(salesExchangeDetail);
    }

    /**
     * 批量删除销售单换货明细
     *
     * @param ids 需要删除的销售单换货明细主键
     * @return 结果
     */
    @Override
    public int deleteSalesExchangeDetailByIds(Long[] ids)
    {
        return salesExchangeDetailMapper.deleteSalesExchangeDetailByIds(ids);
    }

    /**
     * 删除销售单换货明细信息
     *
     * @param id 销售单换货明细主键
     * @return 结果
     */
    @Override
    public int deleteSalesExchangeDetailById(Long id)
    {
        return salesExchangeDetailMapper.deleteSalesExchangeDetailById(id);
    }
}
