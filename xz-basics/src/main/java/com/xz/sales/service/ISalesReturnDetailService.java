package com.xz.sales.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.sales.domain.SalesReturnDetail;

/**
 * 销售单退货明细Service接口
 * 
 * @author xz
 * @date 2024-02-07
 */
public interface ISalesReturnDetailService extends IService<SalesReturnDetail> {
    /**
     * 查询销售单退货明细
     * 
     * @param id 销售单退货明细主键
     * @return 销售单退货明细
     */
    public SalesReturnDetail selectSalesReturnDetailById(Long id);

    /**
     * 查询销售单退货明细列表
     * 
     * @param salesReturnDetail 销售单退货明细
     * @return 销售单退货明细集合
     */
    public List<SalesReturnDetail> selectSalesReturnDetailList(SalesReturnDetail salesReturnDetail);

    /**
     * 新增销售单退货明细
     * 
     * @param salesReturnDetail 销售单退货明细
     * @return 结果
     */
    public int insertSalesReturnDetail(SalesReturnDetail salesReturnDetail);

    /**
     * 修改销售单退货明细
     * 
     * @param salesReturnDetail 销售单退货明细
     * @return 结果
     */
    public int updateSalesReturnDetail(SalesReturnDetail salesReturnDetail);

    /**
     * 批量删除销售单退货明细
     * 
     * @param ids 需要删除的销售单退货明细主键集合
     * @return 结果
     */
    public int deleteSalesReturnDetailByIds(Long[] ids);

    /**
     * 删除销售单退货明细信息
     * 
     * @param id 销售单退货明细主键
     * @return 结果
     */
    public int deleteSalesReturnDetailById(Long id);
}
