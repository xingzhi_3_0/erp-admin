package com.xz.sales.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.sales.domain.SalesExchange;
import com.xz.sales.domain.SalesRuleSetting;
import com.xz.sales.dto.SalesRuleSettingDTO;

import java.util.List;

/**
 * 销售单换货Service接口
 * 
 * @author xz
 * @date 2024-2-19 11:13:40
 */
public interface ISalesRuleSettingService extends IService<SalesRuleSetting> {

    /**
     * 查询销售单设置
     *
     * @return 销售单换货集合
     */
    public AjaxResult queryInfo();

    /**
     * 新增销售单设置
     * 
     * @param salesRuleSettingDTO 销售单设置
     * @return 结果
     */
    public AjaxResult saveInfo(SalesRuleSettingDTO salesRuleSettingDTO);


}
