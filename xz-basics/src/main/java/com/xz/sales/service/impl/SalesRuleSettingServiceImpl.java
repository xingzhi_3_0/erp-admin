package com.xz.sales.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.utils.DateUtils;
import com.xz.common.utils.SecurityUtils;
import com.xz.member.domain.MemberRuleSetting;
import com.xz.sales.domain.SalesExchange;
import com.xz.sales.domain.SalesRuleSetting;
import com.xz.sales.dto.SalesRuleSettingDTO;
import com.xz.sales.mapper.SalesExchangeMapper;
import com.xz.sales.mapper.SalesRuleSettingMapper;
import com.xz.sales.service.ISalesExchangeService;
import com.xz.sales.service.ISalesRuleSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 销售单换货Service业务层处理
 *
 * @author xz
 * @date 2024-2-19 11:15:53
 */
@Service
public class SalesRuleSettingServiceImpl extends ServiceImpl<SalesRuleSettingMapper, SalesRuleSetting> implements ISalesRuleSettingService
{

    @Override
    public AjaxResult queryInfo() {
        SalesRuleSetting salesRuleSetting = new SalesRuleSetting();
        salesRuleSetting.setDeptId(SecurityUtils.getDeptId());
        List<SalesRuleSetting> salesRuleSettings = baseMapper.selectSalesRuleSettingList(salesRuleSetting);
        if(!CollectionUtils.isEmpty(salesRuleSettings)){
            SalesRuleSettingDTO dto = new SalesRuleSettingDTO();
            List<JSONObject> objectList = new ArrayList<>();
            for (SalesRuleSetting ruleSetting:salesRuleSettings) {
                JSONObject obj = new JSONObject();
                Integer ruleSerialNo = ruleSetting.getRuleSerialNo();
                String ruleValue = ruleSetting.getRuleValue();
                JSONObject jsonObject = JSONObject.parseObject(ruleValue);
                obj.put(ruleSerialNo+"",jsonObject);
                objectList.add(obj);
            }
            dto.setQueryParam(objectList);
            return AjaxResult.success(dto);
        }else {
            return AjaxResult.success(null);
        }
    }

    @Override
    public AjaxResult saveInfo(SalesRuleSettingDTO dto) {
        List<JSONObject> queryParam = dto.getQueryParam();
        // 先删除之前的设置
        baseMapper.deleteByDeptId(SecurityUtils.getDeptId());
        // 再新增
        for (int i = 0; i < queryParam.size(); i++) {
            JSONObject jsonObject = queryParam.get(i);
            JSONObject objData = jsonObject.getJSONObject(i+1 + "");
            SalesRuleSetting salesRuleSetting = new SalesRuleSetting();
            salesRuleSetting.setRuleSerialNo(i+1);
            salesRuleSetting.setRuleValue(objData.toJSONString());
            salesRuleSetting.setTenantId(SecurityUtils.getLoginUser().getTenantId());
            salesRuleSetting.setDeptId(SecurityUtils.getDeptId());
            baseMapper.insert(salesRuleSetting);
        }
        return AjaxResult.success("操作成功");
    }
}
