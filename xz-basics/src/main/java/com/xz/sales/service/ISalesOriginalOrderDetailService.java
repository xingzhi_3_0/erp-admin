package com.xz.sales.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.sales.domain.SalesOriginalOrderDetail;

/**
 * 销售单原始商品明细Service接口
 * 
 * @author xz
 * @date 2024-02-21
 */
public interface ISalesOriginalOrderDetailService extends IService<SalesOriginalOrderDetail> {
    /**
     * 查询销售单原始商品明细
     * 
     * @param id 销售单原始商品明细主键
     * @return 销售单原始商品明细
     */
    public SalesOriginalOrderDetail selectSalesOriginalOrderDetailById(Long id);

    /**
     * 查询销售单原始商品明细列表
     * 
     * @param salesOriginalOrderDetail 销售单原始商品明细
     * @return 销售单原始商品明细集合
     */
    public List<SalesOriginalOrderDetail> selectSalesOriginalOrderDetailList(SalesOriginalOrderDetail salesOriginalOrderDetail);

    /**
     * 新增销售单原始商品明细
     * 
     * @param salesOriginalOrderDetail 销售单原始商品明细
     * @return 结果
     */
    public int insertSalesOriginalOrderDetail(SalesOriginalOrderDetail salesOriginalOrderDetail);

    /**
     * 修改销售单原始商品明细
     * 
     * @param salesOriginalOrderDetail 销售单原始商品明细
     * @return 结果
     */
    public int updateSalesOriginalOrderDetail(SalesOriginalOrderDetail salesOriginalOrderDetail);

    /**
     * 批量删除销售单原始商品明细
     * 
     * @param ids 需要删除的销售单原始商品明细主键集合
     * @return 结果
     */
    public int deleteSalesOriginalOrderDetailByIds(Long[] ids);

    /**
     * 删除销售单原始商品明细信息
     * 
     * @param id 销售单原始商品明细主键
     * @return 结果
     */
    public int deleteSalesOriginalOrderDetailById(Long id);
}
