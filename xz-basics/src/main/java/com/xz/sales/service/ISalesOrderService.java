package com.xz.sales.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.sales.domain.SalesOrder;
import com.xz.sales.param.SalesOrderParam;
import com.xz.sales.vo.SalesNoFlowVo;
import com.xz.sales.vo.SalesOrderVo;
import com.xz.sales.vo.SalesPurchaseNoFlowVo;
import com.xz.sales.vo.SalesServiceVo;

import javax.servlet.http.HttpServletResponse;

/**
 * 销售单信息Service接口
 * 
 * @author xz
 * @date 2024-01-31
 */
public interface ISalesOrderService extends IService<SalesOrder> {

    /**
     * 根据销售单号查询销售单信息
     * @param salesNo
     * @return
     */
    SalesOrder selectSalesOrderByNo(String salesNo);

    /**
     * 查询销售单信息
     * 
     * @param id 销售单信息主键
     * @return 销售单信息
     */
    public SalesOrderVo selectSalesOrderById(Long id);

    /**
     * 查询销售单信息列表
     * 
     * @param salesOrder 销售单信息
     * @return 销售单信息集合
     */
    public List<SalesOrder> selectSalesOrderList(SalesOrder salesOrder);

    void export(SalesOrder salesOrder, HttpServletResponse response);

    void export111(SalesOrder salesOrder, HttpServletResponse response);

    void export222(SalesOrder salesOrder, HttpServletResponse response);

    public List<SalesServiceVo> serviceList(SalesOrder salesOrder);

    /**
     * 新增销售单信息
     * 
     * @param param 销售单信息
     * @return 结果
     */
    public AjaxResult insertSalesOrder(SalesOrderParam param);

    public AjaxResult confirmCollection(SalesOrderParam salesOrder);

    public AjaxResult confirmCollectionTwo(SalesOrderParam salesOrder);

    public AjaxResult cancel(SalesOrder salesOrder);

    AjaxResult importNoFlowExcel(List<SalesNoFlowVo> list);

    AjaxResult importSalesPurchaseNoFlowExcel(List<SalesPurchaseNoFlowVo> list);

    AjaxResult importUnUseExcel(List<SalesNoFlowVo> list);

    /**
     * 修改销售单信息
     * 
     * @param salesOrder 销售单信息
     * @return 结果
     */
    public int updateSalesOrder(SalesOrder salesOrder);

    /**
     * 批量删除销售单信息
     * 
     * @param ids 需要删除的销售单信息主键集合
     * @return 结果
     */
    public int deleteSalesOrderByIds(Long[] ids);

    /**
     * 删除销售单信息信息
     * 
     * @param id 销售单信息主键
     * @return 结果
     */
    public int deleteSalesOrderById(Long id);
}
