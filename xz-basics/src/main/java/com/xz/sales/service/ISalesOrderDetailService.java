package com.xz.sales.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.purchaseOrder.domain.PurchaseProductOrder;
import com.xz.sales.domain.SalesOrderDetail;
import com.xz.sales.param.SalesDetailParam;

/**
 * 销售单商品明细Service接口
 *
 * @author xz
 * @date 2024-01-31
 */
public interface ISalesOrderDetailService extends IService<SalesOrderDetail> {
    /**
     * 查询销售单商品明细
     *
     * @param id 销售单商品明细主键
     * @return 销售单商品明细
     */
    public SalesOrderDetail selectSalesOrderDetailById(Long id);

    /**
     * 查询销售单商品明细列表
     *
     * @param salesOrderDetail 销售单商品明细
     * @return 销售单商品明细集合
     */
    public List<SalesOrderDetail> selectSalesOrderDetailList(SalesOrderDetail salesOrderDetail);

    public List<SalesOrderDetail> chooseDetailList(SalesDetailParam salesDetailParam);

    /**
     * 新增销售单商品明细
     *
     * @param salesOrderDetail 销售单商品明细
     * @return 结果
     */
    public int insertSalesOrderDetail(SalesOrderDetail salesOrderDetail);

    AjaxResult updateStatus(SalesOrderDetail salesOrderDetail);

    /**
     * 获取下一个状态
     * @param orderDetailId
     * @param bizType
     * @return
     */
    Integer getNextStatus(Long orderDetailId,Integer bizType);

    /**
     * 修改销售单商品明细
     *
     * @param salesOrderDetail 销售单商品明细
     * @return 结果
     */
    public int updateSalesOrderDetail(SalesOrderDetail salesOrderDetail);

    /**
     * 批量删除销售单商品明细
     *
     * @param ids 需要删除的销售单商品明细主键集合
     * @return 结果
     */
    public int deleteSalesOrderDetailByIds(Long[] ids);

    /**
     * 删除销售单商品明细信息
     *
     * @param id 销售单商品明细主键
     * @return 结果
     */
    public int deleteSalesOrderDetailById(Long id);
}
