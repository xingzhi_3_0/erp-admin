package com.xz.sales.service.impl;

import java.util.List;
import com.xz.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.sales.mapper.SalesOriginalOrderDetailMapper;
import com.xz.sales.domain.SalesOriginalOrderDetail;
import com.xz.sales.service.ISalesOriginalOrderDetailService;

/**
 * 销售单原始商品明细Service业务层处理
 *
 * @author xz
 * @date 2024-02-21
 */
@Service
public class SalesOriginalOrderDetailServiceImpl  extends ServiceImpl<SalesOriginalOrderDetailMapper, SalesOriginalOrderDetail> implements ISalesOriginalOrderDetailService
{
    @Autowired
    private SalesOriginalOrderDetailMapper salesOriginalOrderDetailMapper;

    /**
     * 查询销售单原始商品明细
     *
     * @param id 销售单原始商品明细主键
     * @return 销售单原始商品明细
     */
    @Override
    public SalesOriginalOrderDetail selectSalesOriginalOrderDetailById(Long id)
    {
        return salesOriginalOrderDetailMapper.selectSalesOriginalOrderDetailById(id);
    }

    /**
     * 查询销售单原始商品明细列表
     *
     * @param salesOriginalOrderDetail 销售单原始商品明细
     * @return 销售单原始商品明细
     */
    @Override
    public List<SalesOriginalOrderDetail> selectSalesOriginalOrderDetailList(SalesOriginalOrderDetail salesOriginalOrderDetail)
    {
        return salesOriginalOrderDetailMapper.selectSalesOriginalOrderDetailList(salesOriginalOrderDetail);
    }

    /**
     * 新增销售单原始商品明细
     *
     * @param salesOriginalOrderDetail 销售单原始商品明细
     * @return 结果
     */
    @Override
    public int insertSalesOriginalOrderDetail(SalesOriginalOrderDetail salesOriginalOrderDetail)
    {
        salesOriginalOrderDetail.setCreateTime(DateUtils.getNowDate());
        return salesOriginalOrderDetailMapper.insertSalesOriginalOrderDetail(salesOriginalOrderDetail);
    }

    /**
     * 修改销售单原始商品明细
     *
     * @param salesOriginalOrderDetail 销售单原始商品明细
     * @return 结果
     */
    @Override
    public int updateSalesOriginalOrderDetail(SalesOriginalOrderDetail salesOriginalOrderDetail)
    {
        salesOriginalOrderDetail.setUpdateTime(DateUtils.getNowDate());
        return salesOriginalOrderDetailMapper.updateSalesOriginalOrderDetail(salesOriginalOrderDetail);
    }

    /**
     * 批量删除销售单原始商品明细
     *
     * @param ids 需要删除的销售单原始商品明细主键
     * @return 结果
     */
    @Override
    public int deleteSalesOriginalOrderDetailByIds(Long[] ids)
    {
        return salesOriginalOrderDetailMapper.deleteSalesOriginalOrderDetailByIds(ids);
    }

    /**
     * 删除销售单原始商品明细信息
     *
     * @param id 销售单原始商品明细主键
     * @return 结果
     */
    @Override
    public int deleteSalesOriginalOrderDetailById(Long id)
    {
        return salesOriginalOrderDetailMapper.deleteSalesOriginalOrderDetailById(id);
    }
}
