package com.xz.sales.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.sales.domain.SalesExchange;
import com.xz.sales.param.SalesExchangeParam;
import com.xz.sales.vo.SalesExchangeVo;

/**
 * 销售单换货Service接口
 * 
 * @author xz
 * @date 2024-02-05
 */
public interface ISalesExchangeService extends IService<SalesExchange> {
    /**
     * 查询销售单换货
     * 
     * @param id 销售单换货主键
     * @return 销售单换货
     */
    public SalesExchangeVo selectSalesExchangeById(Long id);

    /**
     * 查询销售单换货列表
     * 
     * @param salesExchange 销售单换货
     * @return 销售单换货集合
     */
    public List<SalesExchange> selectSalesExchangeList(SalesExchange salesExchange);

    /**
     * 新增销售单换货
     * 
     * @param param 销售单换货
     * @return 结果
     */
    public AjaxResult insertSalesExchange(SalesExchangeParam param);

    AjaxResult cancelSalesExchange(SalesExchangeParam exchangeParam);

    AjaxResult confirmSalesExchange(SalesExchangeParam exchangeParam);
    /**
     * 修改销售单换货
     * 
     * @param salesExchange 销售单换货
     * @return 结果
     */
    public int updateSalesExchange(SalesExchange salesExchange);

    /**
     * 批量删除销售单换货
     * 
     * @param ids 需要删除的销售单换货主键集合
     * @return 结果
     */
    public int deleteSalesExchangeByIds(Long[] ids);

    /**
     * 删除销售单换货信息
     * 
     * @param id 销售单换货主键
     * @return 结果
     */
    public int deleteSalesExchangeById(Long id);
}
