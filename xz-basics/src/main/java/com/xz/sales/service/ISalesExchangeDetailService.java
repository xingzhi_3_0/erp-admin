package com.xz.sales.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.sales.domain.SalesExchangeDetail;

/**
 * 销售单换货明细Service接口
 * 
 * @author xz
 * @date 2024-02-21
 */
public interface ISalesExchangeDetailService extends IService<SalesExchangeDetail> {
    /**
     * 查询销售单换货明细
     * 
     * @param id 销售单换货明细主键
     * @return 销售单换货明细
     */
    public SalesExchangeDetail selectSalesExchangeDetailById(Long id);

    /**
     * 查询销售单换货明细列表
     * 
     * @param salesExchangeDetail 销售单换货明细
     * @return 销售单换货明细集合
     */
    public List<SalesExchangeDetail> selectSalesExchangeDetailList(SalesExchangeDetail salesExchangeDetail);

    /**
     * 新增销售单换货明细
     * 
     * @param salesExchangeDetail 销售单换货明细
     * @return 结果
     */
    public int insertSalesExchangeDetail(SalesExchangeDetail salesExchangeDetail);

    /**
     * 修改销售单换货明细
     * 
     * @param salesExchangeDetail 销售单换货明细
     * @return 结果
     */
    public int updateSalesExchangeDetail(SalesExchangeDetail salesExchangeDetail);

    /**
     * 批量删除销售单换货明细
     * 
     * @param ids 需要删除的销售单换货明细主键集合
     * @return 结果
     */
    public int deleteSalesExchangeDetailByIds(Long[] ids);

    /**
     * 删除销售单换货明细信息
     * 
     * @param id 销售单换货明细主键
     * @return 结果
     */
    public int deleteSalesExchangeDetailById(Long id);
}
