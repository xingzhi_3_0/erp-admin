package com.xz.sales.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.sales.domain.SalesReturn;
import com.xz.sales.param.SalesReturnParam;
import com.xz.sales.vo.SalesNoFlowVo;
import com.xz.sales.vo.SalesReturnVo;

/**
 * 销售单退货Service接口
 * 
 * @author xz
 * @date 2024-02-05
 */
public interface ISalesReturnService extends IService<SalesReturn> {
    /**
     * 查询销售单退货
     * 
     * @param id 销售单退货主键
     * @return 销售单退货
     */
    public SalesReturnVo selectSalesReturnById(Long id);

    /**
     * 查询销售单退货列表
     * 
     * @param salesReturn 销售单退货
     * @return 销售单退货集合
     */
    public List<SalesReturn> selectSalesReturnList(SalesReturn salesReturn);

    /**
     * 新增销售单退货
     * 
     * @param salesReturn 销售单退货
     * @return 结果
     */
    public AjaxResult insertSalesReturn(SalesReturnParam returnParam);

    /**
     * 取消退货
     * @param returnParam
     * @return
     */
    AjaxResult cancelSalesReturn(SalesReturnParam returnParam);

    AjaxResult confirmReturn(SalesReturnParam returnParam);

    /**
     * 修改销售单退货
     * 
     * @param salesReturn 销售单退货
     * @return 结果
     */
    public int updateSalesReturn(SalesReturn salesReturn);

    /**
     * 批量删除销售单退货
     * 
     * @param ids 需要删除的销售单退货主键集合
     * @return 结果
     */
    public int deleteSalesReturnByIds(Long[] ids);

    AjaxResult importNoFlowExcel(List<SalesNoFlowVo> list);

    /**
     * 删除销售单退货信息
     * 
     * @param id 销售单退货主键
     * @return 结果
     */
    public int deleteSalesReturnById(Long id);
}
