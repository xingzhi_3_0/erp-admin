package com.xz.sales.service.impl;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.xz.apply.service.IPurchaseApplyService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.domain.entity.SysDept;
import com.xz.common.exception.ServiceException;
import com.xz.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.utils.RedisCode;
import com.xz.common.utils.SecurityUtils;
import com.xz.common.utils.StringUtils;
import com.xz.diagnosis.vo.SalesPurchaseApplyVo;
import com.xz.expense.domain.ExpenseBill;
import com.xz.expense.mapper.ExpenseBillMapper;
import com.xz.member.domain.MemberInfo;
import com.xz.member.mapper.MemberInfoMapper;
import com.xz.member.service.IWalletRecordService;
import com.xz.message.service.ISysTenantNotifyItemService;
import com.xz.message.vo.MemberMsgInfo;
import com.xz.patient.domain.PatientInfo;
import com.xz.patient.mapper.PatientInfoMapper;
import com.xz.product.domain.Product;
import com.xz.product.mapper.ProductMapper;
import com.xz.purchase.domain.Purchase;
import com.xz.purchase.domain.PurchaseProduct;
import com.xz.purchase.mapper.PurchaseProductMapper;
import com.xz.purchase.service.IPurchaseService;
import com.xz.repertory.domain.RepertoryFlow;
import com.xz.repertory.service.IRepertoryFlowService;
import com.xz.sales.domain.*;
import com.xz.sales.mapper.*;
import com.xz.sales.param.SalesExchangeDetailParam;
import com.xz.sales.param.SalesExchangeParam;
import com.xz.sales.param.SalesReturnDetailParam;
import com.xz.sales.param.SalesReturnParam;
import com.xz.sales.service.ISalesOrderService;
import com.xz.sales.util.ProcessOrderUtil;
import com.xz.sales.vo.SalesExchangeVo;
import com.xz.system.mapper.SysDeptMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.sales.service.ISalesExchangeService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;

/**
 * 销售单换货Service业务层处理
 *
 * @author xz
 * @date 2024-02-05
 */
@Service
public class SalesExchangeServiceImpl  extends ServiceImpl<SalesExchangeMapper, SalesExchange> implements ISalesExchangeService
{
    @Autowired
    private SalesExchangeMapper salesExchangeMapper;
    @Resource
    private SalesOrderMapper salesOrderMapper;
    @Resource
    private SalesOrderDetailMapper orderDetailMapper;
    @Resource
    private SalesExchangeDetailMapper exchangeDetailMapper;
    @Resource
    private ExpenseBillMapper expenseBillMapper;
    @Autowired
    private ISalesOrderService salesOrderService;
    @Resource
    private ProductMapper productMapper;
    @Resource
    private SalesRuleSettingMapper salesRuleSettingMapper;
    @Autowired
    private IRepertoryFlowService iRepertoryFlowService;
    @Resource
    private PurchaseProductMapper purchaseProductMapper;
    @Resource
    private SalesPurchaseMapper salesPurchaseMapper;
    @Resource
    private PatientInfoMapper patientInfoMapper;
    @Autowired
    private IPurchaseService purchaseService;
    @Resource
    private SysDeptMapper sysDeptMapper;
    @Resource
    private MemberInfoMapper memberInfoMapper;
    @Autowired
    private IWalletRecordService walletRecordService;
    @Autowired
    private ISysTenantNotifyItemService sysTenantNotifyItemService;
    @Autowired
    private IPurchaseApplyService purchaseApplyService;

    /**
     * 查询销售单换货
     *
     * @param id 销售单换货主键
     * @return 销售单换货
     */
    @Override
    public SalesExchangeVo selectSalesExchangeById(Long id)
    {
        SalesExchangeVo salesExchangeVo = salesExchangeMapper.selectSalesExchangeById(id);
        SalesOrder salesOrder = salesOrderMapper.selectById(salesExchangeVo.getSalesOrderId());
        salesExchangeVo.setSalesNo(salesOrder.getSalesNo());
        salesExchangeVo.setCurrAge(salesOrder.getCurrentAge());
        PatientInfo patientInfo = patientInfoMapper.selectById(salesOrder.getPatientId());
        salesExchangeVo.setPatientPhone(patientInfo.getPhoneNumber());
        salesExchangeVo.setPatientName(patientInfo.getPatientName());
        salesExchangeVo.setSex(patientInfo.getSex());
        SysDept sysDept = sysDeptMapper.selectDeptById(salesOrder.getDeptId());
        salesExchangeVo.setSalesDeptName(sysDept.getDeptName());
        BigDecimal walletBalance = new BigDecimal("0.00");
        if(null != salesOrder.getPatientId()){
            MemberInfo memberInfo = memberInfoMapper.memberInfoByPatientId(salesOrder.getPatientId());
            if(null != memberInfo){
              walletBalance = walletBalance.add(memberInfo.getWalletBalance());
              salesExchangeVo.setMemberCardNo(memberInfo.getMemberCardNo());
            }
        }
        salesExchangeVo.setWalletBalance(walletBalance);


        List<SalesExchangeDetailParam> exchangeDetailList = exchangeDetailMapper.exchangeDetailList(id);
        salesExchangeVo.setExchangeDetailList(exchangeDetailList);

        return salesExchangeVo;
    }

    /**
     * 查询销售单换货列表
     *
     * @param salesExchange 销售单换货
     * @return 销售单换货
     */
    @Override
    public List<SalesExchange> selectSalesExchangeList(SalesExchange salesExchange)
    {
        return salesExchangeMapper.selectSalesExchangeList(salesExchange);
    }

    /**
     * 新增销售单换货
     *
     * @param salesExchangeParam 销售单换货
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public AjaxResult insertSalesExchange(SalesExchangeParam salesExchangeParam)
    {
        Long salesOrderId = salesExchangeParam.getSalesOrderId();
        SalesOrder salesOrder = salesOrderMapper.selectById(salesOrderId);
        salesExchangeParam.setExchangeNo(RedisCode.getCode("XSHH"));
        salesExchangeParam.setCreateBy(SecurityUtils.getLoginUser().getUserId());
        salesExchangeMapper.insertSalesExchange(salesExchangeParam);

        List<SalesExchangeDetailParam> exchangeDetailParamList = salesExchangeParam.getExchangeDetailParamList();

        // 判断是否要上传参数的类别
        SalesRuleSetting queryRule = new SalesRuleSetting();
        queryRule.setRuleSerialNo(2);
        SalesRuleSetting salesRuleSetting = salesRuleSettingMapper.queryMyRule(queryRule);
        List<String> ruleStringList= new ArrayList<>();
        ruleStringList.add("-99");
        if(Objects.nonNull(salesRuleSetting)){
            String ruleValue = salesRuleSetting.getRuleValue();
            JSONObject jsonObject = JSONObject.parseObject(ruleValue);
            String approvalDiscount = jsonObject.getString("uploadCategory");
            String[] split = approvalDiscount.split(",");
            ruleStringList= Stream.of(split).collect(Collectors.toList());

        }

        List<SalesOrderDetail> exchangeOrderDetailList = new ArrayList<>();
        // 优惠折扣
        BigDecimal discount = salesOrder.getDiscount();
        for (SalesExchangeDetailParam param:exchangeDetailParamList) {
            SalesExchangeDetail  exchangeDetail = new SalesExchangeDetail();
            BeanUtils.copyProperties(param,exchangeDetail);
            // 退货拆分
            Long salesOrderDetailId = param.getSalesOrderDetailId();
            SalesOrderDetail salesOrderDetail = orderDetailMapper.selectById(salesOrderDetailId);
            if(salesOrderDetail.getProductPurchaseType().equals(2)){
                DateTime parse = DateUtil.parse("2024-05-15 00:00:00", "yyyy-MM-dd HH:mm:ss");
                if(salesOrderDetail.getCreateTime().getTime() < parse.getTime()){
                    throw new ServiceException("商品名称："+salesOrderDetail.getProductName()+"的商品为无流水定制商品,不能进行换货操作！");
                }
            }

            exchangeDetail.setSalesSourceDetailStatus(salesOrderDetail.getProductStatus());
            SalesOrderDetail newOrderDetail = new SalesOrderDetail();
            BeanUtils.copyProperties(param,newOrderDetail);
            BigDecimal oldDetailSubtotal = new BigDecimal("0.00");
            if(!salesOrderDetail.getSellingNum().equals(param.getExchangeNum())){
                // 说明部分换货
                SalesOrderDetail oldOrderDetail = new SalesOrderDetail();
                // 拆分
                BeanUtils.copyProperties(salesOrderDetail,oldOrderDetail);
                Integer sellingNum = salesOrderDetail.getSellingNum();
                String useReceiveGift = salesOrderDetail.getUseReceiveGift();
                Integer exchangeNum = param.getExchangeNum();
                // 重新计算 优惠金额 和 小计
                BigDecimal discountAmount = salesOrderDetail.getDiscountAmount();
                BigDecimal subtotal = salesOrderDetail.getSubtotal();
                BigDecimal giftDeductionTotal = salesOrderDetail.getGiftDeductionTotal();
                BigDecimal reductionAmount = salesOrderDetail.getReductionAmount();
                BigDecimal sellingPrice = salesOrderDetail.getSellingPrice();

                if(StringUtils.isNotEmpty(useReceiveGift)){
                    // 使用了优惠卡
                    String[] split = useReceiveGift.split(",");
                    List<String> stringList= Stream.of(split).collect(Collectors.toList());
                    int length = split.length;
                    // 用了几张优惠卡
                    int num = sellingNum - exchangeNum ;
                    int cardUseNum = length - num; // 要退的卡数
                    List<String> returnGiftList = new ArrayList<>();
                    if(cardUseNum>0){
                        BigDecimal giftDeductionTotalDivide = giftDeductionTotal.divide(BigDecimal.valueOf(length), 4, BigDecimal.ROUND_HALF_UP);
                        BigDecimal returnGiftDeductionTotal = giftDeductionTotalDivide.multiply(BigDecimal.valueOf(cardUseNum));
                        oldOrderDetail.setGiftDeductionTotal(returnGiftDeductionTotal);
                        BigDecimal subtractGiftDeductionTotal = giftDeductionTotal.subtract(returnGiftDeductionTotal);
                        salesOrderDetail.setGiftDeductionTotal(subtractGiftDeductionTotal);
                        // 需要退会员卡了
                        for (int j = 0; j < cardUseNum; j++) {
                            returnGiftList.add(split[j]);
                        }
                        oldOrderDetail.setUseReceiveGift(StringUtils.join(returnGiftList,","));
                    }else {
                        oldOrderDetail.setUseReceiveGift(null);
                        oldOrderDetail.setGiftDeductionTotal(new BigDecimal(0.00));
                    }
                    // 更改原来商品明细的使用会员卡号
                    stringList.removeAll(returnGiftList);
                    salesOrderDetail.setUseReceiveGift(StringUtils.join(stringList,","));

                }
                BigDecimal returnReductionAmount =  reductionAmount.divide(BigDecimal.valueOf(sellingNum) ,2, BigDecimal.ROUND_HALF_UP).multiply(BigDecimal.valueOf(exchangeNum));
                BigDecimal newSubtotal = new BigDecimal("0.00");
                if(discount != null && discount.doubleValue()>0){
                    newSubtotal = sellingPrice.multiply(BigDecimal.valueOf(exchangeNum)).subtract(oldOrderDetail.getGiftDeductionTotal()).multiply(discount).divide(new BigDecimal("10") ,4, BigDecimal.ROUND_HALF_UP).subtract(returnReductionAmount);// 因为先减了所以要加回去

                }else {
                    newSubtotal = sellingPrice.multiply(BigDecimal.valueOf(exchangeNum)).subtract(oldOrderDetail.getGiftDeductionTotal()).subtract(returnReductionAmount);

                }

                BigDecimal newDiscountAmount = sellingPrice.multiply(BigDecimal.valueOf(exchangeNum)).subtract(newSubtotal);
                oldOrderDetail.setSubtotal(newSubtotal);
                oldDetailSubtotal = oldDetailSubtotal.add(newSubtotal);
                oldOrderDetail.setReductionAmount(returnReductionAmount);
                oldOrderDetail.setDiscountAmount(newDiscountAmount);

                // 原来的也得重算
                BigDecimal subtract = discountAmount.subtract(newDiscountAmount);
                BigDecimal subtract1 = subtotal.subtract(newSubtotal);
                salesOrderDetail.setDiscountAmount(subtract);
                salesOrderDetail.setSubtotal(subtract1);
                // 原来抹零重新计算
                BigDecimal subtractReductionAmount = reductionAmount.subtract(returnReductionAmount);
                salesOrderDetail.setReductionAmount(subtractReductionAmount);

                oldOrderDetail.setSellingNum(exchangeNum);
                oldOrderDetail.setProductStatus(15);
                orderDetailMapper.insert(oldOrderDetail);
                exchangeDetail.setOldSalesOrderDetailId(oldOrderDetail.getId());
                int i1 = sellingNum - exchangeNum;
                salesOrderDetail.setSellingNum(i1);
                newOrderDetail.setGiftDeductionTotal(oldOrderDetail.getGiftDeductionTotal());
                newOrderDetail.setUseReceiveGift(oldOrderDetail.getUseReceiveGift());
                newOrderDetail.setAfterTreatmentNo(salesExchangeParam.getExchangeNo());
                newOrderDetail.setBeforeTreatmentNo(oldOrderDetail.getAfterTreatmentNo());
            }else {
                salesOrderDetail.setProductStatus(15);
                exchangeDetail.setOldSalesOrderDetailId(salesOrderDetail.getId());
                newOrderDetail.setGiftDeductionTotal(salesOrderDetail.getGiftDeductionTotal());
                newOrderDetail.setUseReceiveGift(salesOrderDetail.getUseReceiveGift());
                newOrderDetail.setAfterTreatmentNo(salesExchangeParam.getExchangeNo());
                newOrderDetail.setBeforeTreatmentNo(salesOrderDetail.getAfterTreatmentNo());
                exchangeDetail.setOldSalesOrderDetailId(salesOrderDetail.getId());
            }

            orderDetailMapper.updateById(salesOrderDetail);
            exchangeDetail.setSalesOrderId(salesOrderId);
            exchangeDetail.setSalesSourceDetailId(salesOrderDetail.getId());
            // 再把换货商品插入到销售明细里面
            // 判断状态
            newOrderDetail.setProductStatus(11);
            newOrderDetail.setDetailType(1);
            newOrderDetail.setDeptId(salesOrderDetail.getDeptId());
            newOrderDetail.setSalesId(salesOrderId);
            newOrderDetail.setSellingNum(param.getExchangeNum());
            newOrderDetail.setRemark(param.getRemark());
            //  判断需要上传附件  然后设置 isUploadFile值
            Product product = productMapper.selectById(param.getProductId());
            if(Objects.nonNull(product.getCategoryId())){
                if(ruleStringList.contains(product.getCategoryId().toString()) && param.getProductPurchaseType().equals(2)){
                    newOrderDetail.setProductStatus(3);
                    newOrderDetail.setIsUploadFile(1);
                }
            }
            newOrderDetail.setSubtotal(oldDetailSubtotal.add(param.getDifferenceAmount()));
            orderDetailMapper.insert(newOrderDetail);
            exchangeOrderDetailList.add(newOrderDetail);
            exchangeDetail.setNewSalesOrderDetailId(newOrderDetail.getId());
            exchangeDetail.setSalesExchangeId(salesExchangeParam.getId());
            exchangeDetail.setDetailReturnWarehouseId(param.getDetailReturnWarehouseId());

            exchangeDetailMapper.insert(exchangeDetail);

        }
        // 插入 费用账单管理
        ExpenseBill expenseBill = new ExpenseBill();
        expenseBill.setDeptId(salesOrder.getDeptId());
        expenseBill.setTenantId(salesOrder.getTenantId());
        expenseBill.setPatientId(salesOrder.getPatientId());
        expenseBill.setActCost(salesOrder.getCollectionAmount());
        expenseBill.setBizNo(salesExchangeParam.getExchangeNo());
        expenseBill.setBizType(2);
        expenseBill.setBizId(salesExchangeParam.getId());
        expenseBill.setReceivableCost(salesOrder.getProductTotalAmount());
        expenseBill.setPreferentialCost(salesOrder.getPreferentialAmount());
        expenseBill.setCreateBy(SecurityUtils.getLoginUser().getUserId());
        expenseBillMapper.insertExpenseBill(expenseBill);
        // 预占库存
        List<SalesOrderDetail> salesOrderDetails = orderDetailMapper.selectBySalesId(salesExchangeParam.getSalesOrderId());
//        List<SalesOrderDetail> exchangeDetailList = salesOrderDetails.stream().filter(o -> o.getDetailType() == 1).collect(Collectors.toList());
        boolean b = occupyPurchaseProduct(salesOrder, exchangeOrderDetailList, 1);
        if(false == b){
            throw new ServiceException("新增失败！原因：增加销售单库存流水失败。");
        }
        // 遍历更新销售单状态
        List<Integer> collect = salesOrderDetails.stream().map(SalesOrderDetail::getProductStatus).distinct().collect(Collectors.toList());
        Integer newOrderStatus = ProcessOrderUtil.processOrderStatus(collect);
        salesOrderMapper.updateOrderStatus(salesExchangeParam.getSalesOrderId(),newOrderStatus);
        return AjaxResult.success(salesExchangeParam.getId());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public AjaxResult cancelSalesExchange(SalesExchangeParam exchangeParam){
        SalesExchangeDetail salesSalesDetail = new SalesExchangeDetail();
        salesSalesDetail.setSalesExchangeId(exchangeParam.getId());
        SalesExchange salesExchange = salesExchangeMapper.selectById(exchangeParam.getId());
        salesExchange.setExchangeStatus(3);
        salesExchangeMapper.updateById(salesExchange);
        List<SalesExchangeDetail> salesExchangeDetails = exchangeDetailMapper.selectSalesExchangeDetailList(salesSalesDetail);
        // 取消之后就得恢复成之前的了
        for (SalesExchangeDetail detail:salesExchangeDetails) {
            Long sourceSalesDetailId = detail.getSalesSourceDetailId();
            Long oldSalesOrderDetailId = detail.getOldSalesOrderDetailId();
            SalesOrderDetail sourceSalesOrderDetail = orderDetailMapper.selectById(sourceSalesDetailId);
            if(!sourceSalesDetailId.equals(oldSalesOrderDetailId)){
                SalesOrderDetail oldSalesOrderDetail = orderDetailMapper.selectById(oldSalesOrderDetailId);
                sourceSalesOrderDetail.setSubtotal(sourceSalesOrderDetail.getSubtotal().add(oldSalesOrderDetail.getSubtotal()));
                sourceSalesOrderDetail.setGiftDeductionTotal(sourceSalesOrderDetail.getGiftDeductionTotal().add(oldSalesOrderDetail.getGiftDeductionTotal()));
                sourceSalesOrderDetail.setDiscountAmount(sourceSalesOrderDetail.getDiscountAmount().add(oldSalesOrderDetail.getDiscountAmount()));
                sourceSalesOrderDetail.setReductionAmount(sourceSalesOrderDetail.getReductionAmount().add(oldSalesOrderDetail.getReductionAmount()));
                // 原来的恢复
                String useReceiveGift = oldSalesOrderDetail.getUseReceiveGift();
                List<String> strs = new ArrayList<>();
                if(StringUtils.isNotEmpty(useReceiveGift)){
                    String[] split = useReceiveGift.split(",");
                    List<String> stringList= Stream.of(split).collect(Collectors.toList());
                    strs.addAll(stringList);
                }
                if(StringUtils.isNotEmpty(sourceSalesOrderDetail.getUseReceiveGift())){
                    String[] split = sourceSalesOrderDetail.getUseReceiveGift().split(",");
                    List<String> stringList= Stream.of(split).collect(Collectors.toList());
                    strs.addAll(stringList);
                }
                sourceSalesOrderDetail.setUseReceiveGift(StringUtils.join(strs,","));
                sourceSalesOrderDetail.setSellingNum(sourceSalesOrderDetail.getSellingNum()+oldSalesOrderDetail.getSellingNum());
                sourceSalesOrderDetail.setProductStatus(detail.getSalesSourceDetailStatus());
                orderDetailMapper.updateById(sourceSalesOrderDetail);
                // 拆开的删除
                oldSalesOrderDetail.setProductStatus(20);
                orderDetailMapper.updateById(oldSalesOrderDetail);
//                orderDetailMapper.deleteById(oldSalesOrderDetail.getId());
            }else {
                sourceSalesOrderDetail.setProductStatus(detail.getSalesSourceDetailStatus());
                orderDetailMapper.updateById(sourceSalesOrderDetail);
            }
            // 把新增换货的删掉
            SalesOrderDetail newSalesOrderDetail = orderDetailMapper.selectById(detail.getNewSalesOrderDetailId());
            newSalesOrderDetail.setProductStatus(20);
            orderDetailMapper.updateById(newSalesOrderDetail);
        }
        // 删除生成的费用账单
        ExpenseBill expenseBill = new ExpenseBill();
        expenseBill.setBizId(salesExchange.getId());
        expenseBill.setBizType(2);
        expenseBillMapper.deleteByParam(expenseBill);
        salesExchangeMapper.updateById(salesExchange);
        // 遍历更新销售单状态
        SalesOrder salesOrder = salesOrderMapper.selectById(salesExchange.getSalesOrderId());
        List<SalesOrderDetail> salesOrderDetails = orderDetailMapper.selectBySalesId(salesExchange.getSalesOrderId());
        List<SalesOrderDetail> exchangeDetailList = salesOrderDetails.stream().filter(o -> o.getDetailType() == 1).collect(Collectors.toList());
        boolean b = occupyPurchaseProduct(salesOrder, exchangeDetailList, 2);
        if(false == b){
            throw new ServiceException("新增失败！原因：取消销售单换货库存流水失败。");
        }
        List<Integer> collect = salesOrderDetails.stream().map(SalesOrderDetail::getProductStatus).distinct().collect(Collectors.toList());
        Integer newOrderStatus = ProcessOrderUtil.processOrderStatus(collect);
        salesOrderMapper.updateOrderStatus(salesExchange.getSalesOrderId(),newOrderStatus);
        return AjaxResult.success("操作成功！");
    }


    @Override
    public AjaxResult confirmSalesExchange(SalesExchangeParam exchangeParam){
        // 确认换货里面操作
        SalesExchange salesExchange = salesExchangeMapper.selectById(exchangeParam.getId());
        salesExchange.setExchangeStatus(2);

        SalesOrder salesOrder = salesOrderMapper.selectById(salesExchange.getSalesOrderId());

        if((Objects.nonNull(exchangeParam.getWalletPay()) && exchangeParam.getWalletPay().doubleValue()>0) ){
            // 说明使用了钱包支付
            MemberInfo memberInfo = memberInfoMapper.memberInfoByPatientId(salesOrder.getPatientId());
            BigDecimal walletBalance = memberInfo.getWalletBalance();
            if(Objects.nonNull(exchangeParam.getWalletPay()) && walletBalance.doubleValue()<exchangeParam.getWalletPay().doubleValue()){
                return AjaxResult.error("钱包余额不足！");
            }else {
                if(Objects.nonNull(exchangeParam.getWalletPay()) && exchangeParam.getWalletPay().doubleValue()>0){
                    walletBalance = walletBalance.subtract(exchangeParam.getWalletPay());
                    memberInfo.setWalletBalance(walletBalance);
                    // 新增消费流水新增
                    int i = walletRecordService.addBizRecord(salesOrder.getMemberCardNo(), 4, 2, salesOrder.getSalesNo(), exchangeParam.getWalletPay(), salesOrder.getDeptId());
                    if(i<=0){
                        throw new ServiceException("销售单换货确认付款失败！原因：钱包销售扣款新增流水失败。");
                    }
                }
            }
            memberInfoMapper.updateById(memberInfo);

            // 发送账户变动短信
            MemberMsgInfo msgInfo = new MemberMsgInfo();
            msgInfo.setId(memberInfo.getId());
            BeanUtils.copyProperties(memberInfo,msgInfo);
            msgInfo.setVariableIntegral(new BigDecimal("0"));
            msgInfo.setVariableAmount(exchangeParam.getWalletPay());
            sysTenantNotifyItemService.sendMemberInfoMsg(msgInfo,1, SecurityUtils.getLoginUser().getUser());
        }
        Date date = new Date();
        salesExchange.setPaymentType(exchangeParam.getPaymentType());
        salesExchange.setPaymentTime(date);
        salesExchange.setWalletPay(exchangeParam.getWalletPay());
        salesExchange.setPendingAmount(exchangeParam.getPendingAmount());
        salesExchangeMapper.updateById(salesExchange);
        Long patientId = salesOrder.getPatientId();
        // 更新客户患者累计消费值
        PatientInfo patientInfo = patientInfoMapper.selectById(patientId);
        BigDecimal accumulatedConsumption = patientInfo.getAccumulatedConsumption();
        patientInfo.setAccumulatedConsumption(accumulatedConsumption.add(salesOrder.getCollectionAmount()));
        patientInfoMapper.updateById(patientInfo);
        int nowYear = DateUtil.year(new Date());
        int year = DateUtil.ageOfNow(patientInfo.getBirthday());
        StringBuilder memberName = new StringBuilder();
        if(Objects.nonNull(patientInfo)){
            String sex = "";
            Integer sexNum = patientInfo.getSex();
            if(sexNum == null || sexNum == 0){
                sex = "-";
            }else if(sexNum == 1){
                sex = "男";
            }else if(sexNum == 2){
                sex = "女";
            }
            memberName.append(patientInfo.getPatientName()).append("/").append(sex).append("/").append(year).append("岁");
        }

        SalesExchangeDetail salesExchangeDetail = new SalesExchangeDetail();
        salesExchangeDetail.setSalesExchangeId(exchangeParam.getId());
        List<SalesExchangeDetail> salesExchangeDetails = exchangeDetailMapper.selectSalesExchangeDetailList(salesExchangeDetail);
        // 换货的商品是不是定制采购 如果定制采购 需要生成定制采购单
        List<SalesOrderDetail> needPurchaseDetailList = new ArrayList<>();
        for (SalesExchangeDetail detail:salesExchangeDetails) {
            // 检索换入，换出的商品
            SalesOrderDetail oldSalesOrderDetail = orderDetailMapper.selectById(detail.getOldSalesOrderDetailId());
            SalesOrderDetail newSalesOrderDetail = orderDetailMapper.selectById(detail.getNewSalesOrderDetailId());
            // 更新换出的商品
            // 计算换出商品的实际小计
            // 因为存在连续换货的情况，连续换货会存在多次补差价。这些差价要叠加计算，才是此“链”上的商品实际卖出去的钱
            // 这里只要检索出换货前的实际小计，然后与此次小计（差价）相加即可。
            BigDecimal actualSubtotal = oldSalesOrderDetail.getActualSubtotal().add(newSalesOrderDetail.getSubtotal());
            newSalesOrderDetail.setActualSubtotal(actualSubtotal);
            newSalesOrderDetail.setProductStatus(11);// 待交付
            orderDetailMapper.updateById(newSalesOrderDetail);
            // 更新换入的商品
            // 换入的商品实际卖出去的钱为0，因为该金额已传递到换出商品的实际金额中。
            oldSalesOrderDetail.setActualSubtotal(new BigDecimal(0));
            oldSalesOrderDetail.setProductStatus(18);// 已换货
            orderDetailMapper.updateById(oldSalesOrderDetail);

            SalesOrderDetail sourceSalesOrderDetail = orderDetailMapper.selectById(detail.getSalesSourceDetailId());

            Integer exchangeNum = detail.getExchangeNum();
            BigDecimal sumPrice2 = new BigDecimal("0.00");
            // 两个一样 没拆 直接换  把原来的要退回
            RepertoryFlow repertoryFlowParam = new RepertoryFlow();
            repertoryFlowParam.setStorageType(10);
            repertoryFlowParam.setBusinessId(detail.getSalesSourceDetailId());
            List<PurchaseProduct> repertoryFlows = iRepertoryFlowService.selectListByRepertoryFlow(repertoryFlowParam);
            if(detail.getSalesSourceDetailId().equals(detail.getOldSalesOrderDetailId())){

                // 说明是全部换货
                // 判断换货的退货仓库是否一致
                if(!sourceSalesOrderDetail.getWarehouseId().equals(oldSalesOrderDetail.getWarehouseId())) {
                    PurchaseProduct product = repertoryFlows.get(0);
                    PurchaseProduct purchaseProduct = new PurchaseProduct();
                    BeanUtils.copyProperties(product,purchaseProduct);
                    product.setWarehouseId(oldSalesOrderDetail.getWarehouseId());
                    product.setWarehouseName(oldSalesOrderDetail.getWarehouseName());
                    purchaseProduct.setMemberId(patientInfo.getId());
                    purchaseProduct.setMemberName(memberName.toString());
                    purchaseProduct.setPrice(oldSalesOrderDetail.getSellingPrice());
                    purchaseProduct.setAvailableStock(oldSalesOrderDetail.getSellingNum());
                    purchaseProduct.setStorageNum(oldSalesOrderDetail.getSellingNum());
                    purchaseProduct.setInventoryQuantity(oldSalesOrderDetail.getSellingNum());
                    purchaseProduct.setSubtotal(oldSalesOrderDetail.getSubtotal());
                    purchaseProduct.setCreateTime(new Date());
                    List<PurchaseProduct> purchaseProductList = Arrays.asList(purchaseProduct);
                    Purchase purchase = new Purchase();
                    purchase.setStorageType(14);
                    purchase.setPurchaseNumber(salesOrder.getSalesNo());
                    purchase.setSupplierId(purchaseProduct.getSupplierId());
                    purchase.setSupplierName(purchaseProduct.getSupplierName());
                    purchase.setWarehouseId(product.getWarehouseId());
                    purchase.setWarehouseName(product.getWarehouseName());
                    purchase.setStatus(2);
                    purchase.setPurchaseProductList(purchaseProductList);
                    purchaseService.insertPurchase(purchase);

                }else {
                    // 把原来的加回去
                    for (PurchaseProduct purchaseProduct:repertoryFlows) {
                        Integer inventoryQuantity = newSalesOrderDetail.getSellingNum();
                        Integer availableStock = purchaseProduct.getAvailableStock();
                        purchaseProduct.setAvailableStock(availableStock+inventoryQuantity);
                        purchaseProduct.setPrice(sourceSalesOrderDetail.getSellingPrice());
                        purchaseProduct.setMemberId(patientInfo.getId());
                        purchaseProduct.setMemberName(memberName.toString());
                        //先更新可用库存
                        purchaseProductMapper.updateById(purchaseProduct);
                    }
                    if(!CollectionUtils.isEmpty(repertoryFlows)){
                        //new一个时间放入流水记录中覆盖它，避免换货的出/入库流水记录的创建时间用的是“销售出/入库”时的创建时间。
                        repertoryFlows.forEach(data -> {
                            data.setCreateTime(new Date());
                            // 备注设为null，也是避免用的是“销售出/入库”的数据
                            data.setRemark(null);
                            // 换货单的退货流水对应的仓库应该设为退货的仓库名称
                            data.setWarehouseName(detail.getDetailReturnWarehouseName());
                        });
                        boolean repertoryFlow = iRepertoryFlowService.saveRepertoryFlow(repertoryFlows, 1, 14);
                        if (!repertoryFlow) {
                            throw new ServiceException("销售单退货失败！原因：销售单退货库存流水失败。");
                        }
                    }

                }

            }else {
                // 不一样了 说明是部分换货
                // 然后判断被换货的 退回仓库 是否和原来一样
                if(sourceSalesOrderDetail.getWarehouseId().equals(oldSalesOrderDetail.getWarehouseId())){
                    // 说明被拆出来  退回原来的库 换货退回

                    List<PurchaseProduct> newRepertoryFlows = new ArrayList<>();
                    // 说明从哪里来到哪里去
                    for (PurchaseProduct purchaseProduct:repertoryFlows) {
                        Integer inventoryQuantity = purchaseProduct.getInventoryQuantity();
                        exchangeNum = exchangeNum - inventoryQuantity;
                        Integer availableStock = purchaseProduct.getAvailableStock();

                        purchaseProduct.setMemberId(patientInfo.getId());
                        purchaseProduct.setMemberName(memberName.toString());

                        if(exchangeNum>0){
                            purchaseProduct.setAvailableStock(availableStock+inventoryQuantity);
                            //先更新可用库存
                            purchaseProductMapper.updateById(purchaseProduct);
                            newRepertoryFlows.add(purchaseProduct);
                        }else {
                            int i = inventoryQuantity + exchangeNum;
                            Integer oldInventoryQuantity = purchaseProduct.getInventoryQuantity();
                            BigDecimal oldSubtotal = purchaseProduct.getSubtotal();
                            BigDecimal dividePrice = oldSubtotal.divide(BigDecimal.valueOf(oldInventoryQuantity), 4, BigDecimal.ROUND_HALF_UP);
                            BigDecimal multiply = dividePrice.multiply(BigDecimal.valueOf(i));
                            purchaseProduct.setSubtotal(multiply);
                            purchaseProduct.setInventoryQuantity(i);
                            purchaseProduct.setAvailableStock(availableStock+inventoryQuantity+exchangeNum);
                            purchaseProduct.setPrice(sourceSalesOrderDetail.getSellingPrice());
                            //先更新可用库存
                            purchaseProductMapper.updateById(purchaseProduct);
                            newRepertoryFlows.add(purchaseProduct);
                            break;
                        }
                    }
                    if(!CollectionUtils.isEmpty(newRepertoryFlows)) {
                        //new一个时间放入流水记录中覆盖它，避免换货的出/入库流水记录的创建时间用的是“销售出/入库”时的创建时间
                        newRepertoryFlows.forEach(data -> {
                            data.setCreateTime(new Date());
                            // 备注设为null，也是避免用的是“销售出/入库”的数据
                            data.setRemark(null);
                            // 换货单的退货流水对应的仓库应该设为退货的仓库名称
                            data.setWarehouseName(detail.getDetailReturnWarehouseName());
                        });
                        boolean repertoryFlow = iRepertoryFlowService.saveRepertoryFlow(newRepertoryFlows, 1, 14);
                        if (!repertoryFlow) {
                            throw new ServiceException("销售单换货失败！原因：销售单换货退回仓库库存流水失败。");
                        }
                    }
                }else {
                    PurchaseProduct product = repertoryFlows.get(0);
                    PurchaseProduct purchaseProduct = new PurchaseProduct();
                    BeanUtils.copyProperties(product,purchaseProduct);
                    product.setWarehouseId(oldSalesOrderDetail.getWarehouseId());
                    product.setWarehouseName(oldSalesOrderDetail.getWarehouseName());
                    purchaseProduct.setMemberId(patientInfo.getId());
                    purchaseProduct.setMemberName(memberName.toString());
                    purchaseProduct.setPrice(oldSalesOrderDetail.getSellingPrice());
                    purchaseProduct.setAvailableStock(oldSalesOrderDetail.getSellingNum());
                    purchaseProduct.setStorageNum(oldSalesOrderDetail.getSellingNum());
                    purchaseProduct.setInventoryQuantity(oldSalesOrderDetail.getSellingNum());
                    purchaseProduct.setSubtotal(oldSalesOrderDetail.getSubtotal());
                    purchaseProduct.setCreateTime(new Date());
                    List<PurchaseProduct> purchaseProductList = Arrays.asList(purchaseProduct);
                    Purchase purchase = new Purchase();
                    purchase.setStorageType(14);
                    purchase.setPurchaseNumber(salesOrder.getSalesNo());
                    purchase.setSupplierId(purchaseProduct.getSupplierId());
                    purchase.setSupplierName(purchaseProduct.getSupplierName());
                    purchase.setWarehouseId(product.getWarehouseId());
                    purchase.setWarehouseName(product.getWarehouseName());
                    purchase.setStatus(2);
                    purchase.setPurchaseProductList(purchaseProductList);
                    purchaseService.insertPurchase(purchase);
                }
            }

            SalesOrderDetail salesOrderDetail = orderDetailMapper.selectById(detail.getNewSalesOrderDetailId());
            if(salesOrderDetail.getProductPurchaseType().equals(2)){
                salesOrderDetail.setProductStatus(4);
                // 如果需要上传参数附件
                if(salesOrderDetail.getIsUploadFile().equals(1)){
                    salesOrderDetail.setProductStatus(3);
                }
                orderDetailMapper.updateById(salesOrderDetail);
                salesOrderDetail.setRemark(salesExchange.getExchangeNo()+"换货商品定制采购");
                needPurchaseDetailList.add(salesOrderDetail);
            }
        }

        if(!CollectionUtils.isEmpty(needPurchaseDetailList)){
            List<SalesOrderDetail> collect = needPurchaseDetailList.stream().filter(o -> o.getProductStatus().equals(4)).collect(Collectors.toList());
            if(collect.size() == needPurchaseDetailList.size()){
                SalesPurchaseApplyVo salesPurchaseApplyVo = new SalesPurchaseApplyVo();
                salesPurchaseApplyVo.setSalesNo(salesOrder.getSalesNo());
                salesPurchaseApplyVo.setSalesDeptId(salesOrder.getDeptId());
                salesPurchaseApplyVo.setSalesOrderId(salesOrder.getId());
                salesPurchaseApplyVo.setOrderDetailList(needPurchaseDetailList);
                purchaseApplyService.addSalesPurchaseApply(salesPurchaseApplyVo);
            }
        }
        // 遍历更新销售单状态
        List<SalesOrderDetail> salesOrderDetails = exchangeDetailMapper.selectBySalesExchangeId(exchangeParam.getId());
        // 更新费用账单
        ExpenseBill expenseBill = new ExpenseBill();
        expenseBill.setBizId(exchangeParam.getId());
        expenseBill.setBizType(2);
        expenseBill.setStatus(1);
        expenseBillMapper.updateByParam(expenseBill);
        // 再把新换的 库存减掉
        // 查找这个换货单新增的数据
        List<Integer> collect = salesOrderDetails.stream().map(SalesOrderDetail::getProductStatus).distinct().collect(Collectors.toList());
        Integer newOrderStatus = ProcessOrderUtil.processOrderStatus(collect);
        salesOrderMapper.updateOrderStatus(exchangeParam.getSalesOrderId(),newOrderStatus);
        return AjaxResult.success("操作成功！");
    }

    private boolean occupyPurchaseProduct(SalesOrder salesOrder,List<SalesOrderDetail> salesOrderDetails,Integer editType){

        for (SalesOrderDetail salesDetail:salesOrderDetails) {
            if(salesDetail.getProductPurchaseType().equals(2)){
                continue;
            }
            if(editType.equals(1)){
                PurchaseProduct queryProductParam = new PurchaseProduct();
                BeanUtils.copyProperties(salesDetail,queryProductParam);
                List<PurchaseProduct> purchaseProductList = purchaseProductMapper.getSalesListByParam(queryProductParam);
                if(CollectionUtils.isEmpty(purchaseProductList)){
                    throw new ServiceException("销售单提交失败！原因：商品名称:"+queryProductParam.getProductName()+",参数:"+queryProductParam.getProductParam()+"库存为0，请重新选择商品！");
                }
                List<PurchaseProduct> purchaseProducts = iRepertoryFlowService.convertFlowData(purchaseProductList, salesDetail.getSellingNum(), salesOrder.getSalesNo(),salesDetail.getId());
                // setInventoryQuantity
                for (PurchaseProduct purchaseProduct:purchaseProducts) {
                    SalesPurchase salesPurchase = new SalesPurchase();
                    salesPurchase.setSalesId(salesOrder.getId());
                    salesPurchase.setDetailId(salesDetail.getId());
                    salesPurchase.setPurchaseProductId(purchaseProduct.getId());
                    salesPurchase.setPurchaseProductNum(purchaseProduct.getInventoryQuantity());
                    salesPurchaseMapper.insert(salesPurchase);
                }
            }else{
                SalesPurchase salesPurchase = new SalesPurchase();
                salesPurchase.setSalesId(salesOrder.getId());
                salesPurchase.setDetailId(salesDetail.getId());
                List<PurchaseProduct> purchaseProductList = salesPurchaseMapper.selectPurchaseList(salesPurchase);
                for (PurchaseProduct purchaseProduct:purchaseProductList) {
                    Integer inventoryQuantity = purchaseProduct.getInventoryQuantity();
                    Integer availableStock = purchaseProduct.getAvailableStock();
                    purchaseProduct.setAvailableStock(availableStock+inventoryQuantity);
                    purchaseProductMapper.updateById(purchaseProduct);
                }
            }
        }

        return true;

    }

    /**
     * 修改销售单换货
     *
     * @param salesExchange 销售单换货
     * @return 结果
     */
    @Override
    public int updateSalesExchange(SalesExchange salesExchange)
    {
        salesExchange.setUpdateTime(DateUtils.getNowDate());
        return salesExchangeMapper.updateSalesExchange(salesExchange);
    }

    /**
     * 批量删除销售单换货
     *
     * @param ids 需要删除的销售单换货主键
     * @return 结果
     */
    @Override
    public int deleteSalesExchangeByIds(Long[] ids)
    {
        return salesExchangeMapper.deleteSalesExchangeByIds(ids);
    }

    /**
     * 删除销售单换货信息
     *
     * @param id 销售单换货主键
     * @return 结果
     */
    @Override
    public int deleteSalesExchangeById(Long id)
    {
        return salesExchangeMapper.deleteSalesExchangeById(id);
    }
}
