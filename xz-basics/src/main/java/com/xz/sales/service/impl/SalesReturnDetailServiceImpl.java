package com.xz.sales.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.sales.mapper.SalesReturnDetailMapper;
import com.xz.sales.domain.SalesReturnDetail;
import com.xz.sales.service.ISalesReturnDetailService;

/**
 * 销售单退货明细Service业务层处理
 *
 * @author xz
 * @date 2024-02-07
 */
@Service
public class SalesReturnDetailServiceImpl  extends ServiceImpl<SalesReturnDetailMapper, SalesReturnDetail> implements ISalesReturnDetailService
{
    @Autowired
    private SalesReturnDetailMapper salesReturnDetailMapper;

    /**
     * 查询销售单退货明细
     *
     * @param id 销售单退货明细主键
     * @return 销售单退货明细
     */
    @Override
    public SalesReturnDetail selectSalesReturnDetailById(Long id)
    {
        return salesReturnDetailMapper.selectSalesReturnDetailById(id);
    }

    /**
     * 查询销售单退货明细列表
     *
     * @param salesReturnDetail 销售单退货明细
     * @return 销售单退货明细
     */
    @Override
    public List<SalesReturnDetail> selectSalesReturnDetailList(SalesReturnDetail salesReturnDetail)
    {
        return salesReturnDetailMapper.selectSalesReturnDetailList(salesReturnDetail);
    }

    /**
     * 新增销售单退货明细
     *
     * @param salesReturnDetail 销售单退货明细
     * @return 结果
     */
    @Override
    public int insertSalesReturnDetail(SalesReturnDetail salesReturnDetail)
    {

        return salesReturnDetailMapper.insertSalesReturnDetail(salesReturnDetail);
    }

    /**
     * 修改销售单退货明细
     *
     * @param salesReturnDetail 销售单退货明细
     * @return 结果
     */
    @Override
    public int updateSalesReturnDetail(SalesReturnDetail salesReturnDetail)
    {
        return salesReturnDetailMapper.updateSalesReturnDetail(salesReturnDetail);
    }

    /**
     * 批量删除销售单退货明细
     *
     * @param ids 需要删除的销售单退货明细主键
     * @return 结果
     */
    @Override
    public int deleteSalesReturnDetailByIds(Long[] ids)
    {
        return salesReturnDetailMapper.deleteSalesReturnDetailByIds(ids);
    }

    /**
     * 删除销售单退货明细信息
     *
     * @param id 销售单退货明细主键
     * @return 结果
     */
    @Override
    public int deleteSalesReturnDetailById(Long id)
    {
        return salesReturnDetailMapper.deleteSalesReturnDetailById(id);
    }
}
