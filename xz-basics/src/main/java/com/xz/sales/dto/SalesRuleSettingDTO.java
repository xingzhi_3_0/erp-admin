package com.xz.sales.dto;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.util.List;

@Data
public class SalesRuleSettingDTO {

    /** 设置数据集合 */
    private List<JSONObject> queryParam;
}
