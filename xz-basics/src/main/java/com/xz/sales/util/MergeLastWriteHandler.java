package com.xz.sales.util;


import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.write.merge.AbstractMergeStrategy;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;


public class MergeLastWriteHandler extends AbstractMergeStrategy {

    @Override
    protected void merge(Sheet sheet, Cell cell, Head head, Integer integer) {
        String strValue = cell.getStringCellValue();
        if (StringUtils.equals(strValue,"说明")){
            //如果最后一行是说明则重新赋值
            cell.setCellValue("说明:该表格仅用于测试学习哦！来看一看最终效果吧");
            //获取表格最后一行
            int lastRowNum = sheet.getLastRowNum();
            //合并单元格
            CellRangeAddress region = new CellRangeAddress(lastRowNum,lastRowNum,0,4);
            sheet.addMergedRegion(region);
        }
    }

}
