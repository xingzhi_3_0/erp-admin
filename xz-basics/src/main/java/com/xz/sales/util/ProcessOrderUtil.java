package com.xz.sales.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProcessOrderUtil {

    public static Integer processOrderStatus(List<Integer> productListStatus){

        List<Integer> waitPay = new ArrayList<>(Arrays.asList(1));
        waitPay.retainAll(productListStatus);
        if(waitPay.size()>0){
            return 1;
        }
        List<Integer> salesProcessing = new ArrayList<>(Arrays.asList(2, 3));
        salesProcessing.retainAll(productListStatus);
        if(salesProcessing.size()>0){
            return 2;
        }
        List<Integer> unDelivered = new ArrayList<>(Arrays.asList(4,5,6,7,8,9,10,11));
        unDelivered.retainAll(productListStatus);
        if(unDelivered.size()>0){
            return 3;
        }
        List<Integer> salesService = new ArrayList<>(Arrays.asList(13,14,15));
        salesService.retainAll(productListStatus);
        if(salesService.size()>0){
            return 5;
        }
        List<Integer> afterSales = new ArrayList<>(Arrays.asList(16,17,18));
        afterSales.retainAll(productListStatus);
        if(afterSales.size()>0){
            return 6;
        }
        List<Integer> delivered = new ArrayList<>(Arrays.asList(12));
        delivered.retainAll(productListStatus);
        if(delivered.size()>0){
            return 4;
        }
        return 0;
    }


}
