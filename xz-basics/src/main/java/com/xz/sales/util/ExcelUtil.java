package com.xz.sales.util;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.metadata.style.WriteFont;
import com.alibaba.excel.write.style.HorizontalCellStyleStrategy;
import com.xz.sales.vo.ReportOrderBO;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExcelUtil {

    public static void reportExcel(List<ReportOrderBO> listVo, HttpServletResponse response) throws IOException {

        //末尾备注
        List<List<String>> beizhu = new ArrayList<>();
        List<String> list = new ArrayList<>();
        //末尾添加说明 合并最后一行单元格时赋值在 MergeLastWriteHandler
        list.add("说明");
        beizhu.add(list);
        WriteCellStyle contentWriteCellStyle = new WriteCellStyle();
        WriteCellStyle headWriteCellStyle = new WriteCellStyle();
        // 标题字体大小
        WriteFont contentWriteFont = new WriteFont();
        contentWriteFont.setFontHeightInPoints((short) 12);
        headWriteCellStyle.setWriteFont(contentWriteFont);
        //标题黄色底纹
        headWriteCellStyle.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());

        //边框
        contentWriteCellStyle.setBorderBottom(BorderStyle.THIN);
        contentWriteCellStyle.setBorderLeft(BorderStyle.THIN);
        contentWriteCellStyle.setBorderRight(BorderStyle.THIN);
        contentWriteCellStyle.setBorderTop(BorderStyle.THIN);
        // 水平居中
        contentWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.CENTER);
        // 垂直居中
        contentWriteCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        // 设置自动换行，前提内容中需要加「\n」才有效
        contentWriteCellStyle.setWrapped(true);
        //初始化样式
        HorizontalCellStyleStrategy horizontalCellStyleStrategy =
                new HorizontalCellStyleStrategy(headWriteCellStyle, contentWriteCellStyle);
        //合并单元格所需参数
        //合并坐标
        int[] mergeColumeIndex = {1, 1,2,2};
        //从第二行后开始合并
        int mergeRowIndex = 1;
        try (ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream(), ReportOrderBO.class).autoCloseStream(Boolean.FALSE)
                //风格样式
                .registerWriteHandler(horizontalCellStyleStrategy)
                //合并说明并赋值
//                .registerWriteHandler(new MergeLastWriteHandler())
                //自动合并市级单元格 注意两个入参是出事
                .registerWriteHandler(new ExcelMergeUtil(mergeRowIndex, mergeColumeIndex))
                .build()) {
            WriteSheet sheet = EasyExcel.writerSheet("销售数据").build();
            excelWriter.write(listVo, sheet);
            excelWriter.write(beizhu, sheet);
        }

        response.getOutputStream().close();
    }

}
