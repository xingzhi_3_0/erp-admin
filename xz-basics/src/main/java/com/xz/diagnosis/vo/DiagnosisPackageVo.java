package com.xz.diagnosis.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 诊疗套餐对象 t_diagnosis_package
 *
 * @author xz
 * @date 2024-03-14
 */
@Data
public class DiagnosisPackageVo{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 套餐编码 */
    //private String packageNo;

    /** 套餐名称 */
    private String packageName;

    /** 项目名称组合 */
    private String itemNames;

    /** 套餐价（元） */
    private BigDecimal packagePrice;

    /** 组合价（元） */
    private BigDecimal price;

    /** 状态（0.停用，1.启用） */
    private String status;

    /** 套餐项目 */
    private List<DiagnosisPackageItemVo> items;

}
