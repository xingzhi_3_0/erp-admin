package com.xz.diagnosis.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 诊疗套餐项目对象 t_diagnosis_package_item
 *
 * @author xz
 * @date 2024-03-14
 */
@Data
public class DiagnosisPackageItemVo{

    /** 套餐ID */
    private Long packageId;
    /** 套餐名称 */
    private String packageName;
    /** 套餐价（元）*/
    private BigDecimal packagePrice;
    /** 项目ID */
    private Long itemId;
    /** 数量 */
    private Integer quantity;
    /** 项目名称 */
    private String itemName;
    /** 单价（元）*/
    private BigDecimal price;
    /** 单位 */
    private String unit;
    /** 总价 */
    private BigDecimal totalPrice;

    public BigDecimal getTotalPrice() {
      if(null == quantity || null == price){
        return BigDecimal.ZERO;
      }
      BigDecimal multiply = price.multiply(new BigDecimal(quantity));
      return multiply;
    }
}
