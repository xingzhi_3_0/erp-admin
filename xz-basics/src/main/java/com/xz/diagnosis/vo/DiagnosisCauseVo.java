package com.xz.diagnosis.vo;

import lombok.Data;

/**
 * 诊疗事项对象 t_diagnosis_cause
 *
 * @author xz
 * @date 2024-03-21
 */
@Data
public class DiagnosisCauseVo {

    /** id */
    private Long id;

    /** 事项名称 */
    private String cause;

    /** 排序 */
    private Integer sortNo;

}
