package com.xz.diagnosis.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 套餐混合项目
 *
 * @author xz
 * @date 2024-03-14
 */
@Data
public class PackageUnionItemVo {

  private String sid;
  private Long id;
  private Integer type;
  private String sname;
  private BigDecimal price;
  private List<PackageUnionItemChildrenVo> children;

  public PackageUnionItemVo() {
  }

  public PackageUnionItemVo(DiagnosisPackageItemVo vo) {
    this.id = vo.getItemId();
    this.sname = vo.getItemName();
    //this.price = vo.getPrice();
  }

  public List<PackageUnionItemChildrenVo> getChildren() {
    return children == null ? new ArrayList<>() : children;
  }
}
