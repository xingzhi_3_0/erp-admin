package com.xz.diagnosis.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * 诊疗套餐对象 t_diagnosis_registration_fee
 *
 * @author xz
 * @date 2024-03-17
 */
@Data
public class DiagnosisRegistrationFeeVo{

    /** id */
    private Long id;

    /** 医生名称 */
    @Excel(name = "医生名称")
    private String doctorName;

    /** 门诊类型 */
    @Excel(name = "门诊类型")
    private String registrationFeeType;

    /** 挂号费 */
    @Excel(name = "挂号费")
    private BigDecimal registrationFee;
}
