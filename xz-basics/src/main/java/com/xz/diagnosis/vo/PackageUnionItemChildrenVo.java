package com.xz.diagnosis.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 套餐混合项目
 *
 * @author xz
 * @date 2024-03-14
 */
@Data
public class PackageUnionItemChildrenVo {

  private Long id;
  private String itemName;
  private Integer quantity;

  public PackageUnionItemChildrenVo() {
  }

  public PackageUnionItemChildrenVo(DiagnosisPackageItemVo vo) {
    this.id = vo.getItemId();
    this.itemName = vo.getItemName();
    this.quantity = vo.getQuantity();
  }
}
