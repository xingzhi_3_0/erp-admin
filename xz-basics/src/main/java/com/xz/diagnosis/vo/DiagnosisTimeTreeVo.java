package com.xz.diagnosis.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 诊疗时间项对象 t_diagnosis_time
 *
 * @author xz
 * @date 2024-03-21
 */
@Data
public class DiagnosisTimeTreeVo{

    /** id */
    private Long id;
    /** 分组id */
    private Long groupId = 0L;
    /** 分组名 */
    private String groupName = "";
    /** 时间段 */
    private String timePeriod;
    /** 前端级联下拉框用，children */
    private List<DiagnosisTimeTreeVo> children;

}
