package com.xz.diagnosis.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 诊疗项目对象
 *
 * @author xz
 * @date 2024-03-14
 */
@Data
public class DiagnosisItemVo {

  /**
   * id
   */
  private Long id;

  /**
   * 项目名称
   */
  @Excel(name = "项目名称")
  private String itemName;

  /**
   * 单价（元）
   */
  @Excel(name = "单价", readConverterExp = "元=")
  private BigDecimal price;

  /**
   * 单位
   */
  @Excel(name = "单位")
  private String unit;

}
