package com.xz.diagnosis.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 诊疗套餐对象 t_diagnosis_registration_fee
 *
 * @author xz
 * @date 2024-03-17
 */
@Data
public class DiagnosisRegistrationFeeTreeVo {

  /**
   * id
   */
  private String id;

  /**
   * 医生名称
   */
  @Excel(name = "医生名称")
  private String doctorName;

  /**
   * 门诊类型
   */
  @Excel(name = "门诊类型")
  private String registrationFeeType;

  /**
   * 挂号费
   */
  @Excel(name = "挂号费")
  private BigDecimal registrationFee;

  /**
   * 前端级联下拉框用，value值
   */
  private List<DiagnosisRegistrationFeeTreeVo> children;

  public DiagnosisRegistrationFeeTreeVo() {
  }

  public DiagnosisRegistrationFeeTreeVo(DiagnosisRegistrationFeeVo vo) {
    this.id = vo.getId().toString();
    this.doctorName = vo.getDoctorName();
    this.registrationFeeType = vo.getRegistrationFeeType();
    this.registrationFee = vo.getRegistrationFee();
  }

  public static DiagnosisRegistrationFeeTreeVo initTreeVo(String registrationFeeType, List<DiagnosisRegistrationFeeVo> timePeriodList) {
    DiagnosisRegistrationFeeTreeVo vo = new DiagnosisRegistrationFeeTreeVo();
    vo.setId(registrationFeeType);
    vo.setRegistrationFeeType(registrationFeeType);
    vo.setDoctorName(registrationFeeType);
    // 找出子数据并且转成DiagnosisRegistrationFeeTreeVo对象
    List<DiagnosisRegistrationFeeTreeVo> children =  timePeriodList.stream()
      .filter(a -> a.getRegistrationFeeType().equals(registrationFeeType))
      .map(a -> new DiagnosisRegistrationFeeTreeVo(a))
      .collect(Collectors.toList());
    vo.setChildren(children);
    return vo;
  }
}
