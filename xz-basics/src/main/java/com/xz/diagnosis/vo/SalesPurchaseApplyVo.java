package com.xz.diagnosis.vo;

import com.xz.sales.domain.SalesOrderDetail;
import lombok.Data;

import java.util.List;

@Data
public class SalesPurchaseApplyVo {

    private Long salesOrderId;

    private String salesNo;

    private Long salesDeptId;

    private List<SalesOrderDetail> orderDetailList;
}
