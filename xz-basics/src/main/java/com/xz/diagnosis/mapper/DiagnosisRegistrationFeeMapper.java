package com.xz.diagnosis.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.diagnosis.vo.DiagnosisRegistrationFeeVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.diagnosis.domain.DiagnosisRegistrationFee;

/**
 * 诊疗套餐Mapper接口
 *
 * @author xz
 * @date 2024-03-17
 */
@Mapper
public interface DiagnosisRegistrationFeeMapper extends BaseMapper<DiagnosisRegistrationFee> {
  /**
   * 查询诊疗套餐
   *
   * @param id 诊疗套餐主键
   * @return 诊疗套餐
   */
  public DiagnosisRegistrationFee selectDiagnosisRegistrationFeeById(Long id);

  /**
   * 查询诊疗套餐列表
   *
   * @param diagnosisRegistrationFee 诊疗套餐
   * @return 诊疗套餐集合
   */
  public List<DiagnosisRegistrationFee> selectDiagnosisRegistrationFeeList(DiagnosisRegistrationFee diagnosisRegistrationFee);

  /**
   * 查询诊疗套餐列表
   *
   * @param diagnosisRegistrationFee 诊疗套餐
   * @return 诊疗套餐集合
   */
  public List<DiagnosisRegistrationFeeVo> selectDiagnosisRegistrationFeeVoList(DiagnosisRegistrationFee diagnosisRegistrationFee);

  /**
   * 查询诊疗套餐列表
   *
   * @param diagnosisRegistrationFee 诊疗套餐
   * @return 诊疗套餐集合
   */
  public List<DiagnosisRegistrationFeeVo> selectDiagnosisRegistrationFeeGroupList(DiagnosisRegistrationFee diagnosisRegistrationFee);
  /**
   * 新增诊疗套餐
   *
   * @param diagnosisRegistrationFee 诊疗套餐
   * @return 结果
   */
  public int insertDiagnosisRegistrationFee(DiagnosisRegistrationFee diagnosisRegistrationFee);

  /**
   * 修改诊疗套餐
   *
   * @param diagnosisRegistrationFee 诊疗套餐
   * @return 结果
   */
  public int updateDiagnosisRegistrationFee(DiagnosisRegistrationFee diagnosisRegistrationFee);

  /**
   * 删除诊疗套餐
   *
   * @param id 诊疗套餐主键
   * @return 结果
   */
  public int deleteDiagnosisRegistrationFeeById(Long id);

  /**
   * 批量删除诊疗套餐
   *
   * @param ids 需要删除的数据主键集合
   * @return 结果
   */
  public int deleteDiagnosisRegistrationFeeByIds(Long[] ids);


}
