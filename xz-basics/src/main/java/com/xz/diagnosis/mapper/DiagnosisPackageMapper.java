package com.xz.diagnosis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.diagnosis.domain.DiagnosisPackage;
import com.xz.diagnosis.vo.DiagnosisPackageVo;
import com.xz.diagnosis.vo.PackageUnionItemVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 诊疗套餐Mapper接口
 *
 * @author xz
 * @date 2024-03-14
 */
@Mapper
public interface DiagnosisPackageMapper extends BaseMapper<DiagnosisPackage> {
  /**
   * 查询诊疗套餐
   *
   * @param id 诊疗套餐主键
   * @return 诊疗套餐
   */
  public DiagnosisPackage selectDiagnosisPackageById(Long id);

  /**
   * 查询诊疗套餐列表
   *
   * @param diagnosisPackage 诊疗套餐
   * @return 诊疗套餐集合
   */
  public List<DiagnosisPackage> selectDiagnosisPackageList(DiagnosisPackage diagnosisPackage);

  /**
   * 新增诊疗套餐
   *
   * @param diagnosisPackage 诊疗套餐
   * @return 结果
   */
  public int insertDiagnosisPackage(DiagnosisPackage diagnosisPackage);

  /**
   * 修改诊疗套餐
   *
   * @param diagnosisPackage 诊疗套餐
   * @return 结果
   */
  public int updateDiagnosisPackage(DiagnosisPackage diagnosisPackage);

  /**
   * 删除诊疗套餐
   *
   * @param id 诊疗套餐主键
   * @return 结果
   */
  public int deleteDiagnosisPackageById(Long id);

  /**
   * 批量删除诊疗套餐
   *
   * @param ids 需要删除的数据主键集合
   * @return 结果
   */
  public int deleteDiagnosisPackageByIds(Long[] ids);

  /**
   * 查询诊疗套餐列表
   *
   * @param diagnosisPackage 诊疗套餐
   * @return 诊疗套餐集合
   */
  public List<DiagnosisPackageVo> selectDiagnosisPackageVoList(DiagnosisPackage diagnosisPackage);

  /**
   * 查询诊疗套餐与项目合并列表
   *
   * @param diagnosisPackage 诊疗套餐
   * @return
   */
  public List<PackageUnionItemVo> selectPackageUnionItemList(DiagnosisPackage diagnosisPackage);
}
