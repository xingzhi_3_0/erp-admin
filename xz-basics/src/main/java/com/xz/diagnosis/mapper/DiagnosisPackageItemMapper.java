package com.xz.diagnosis.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.diagnosis.vo.DiagnosisPackageItemVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.diagnosis.domain.DiagnosisPackageItem;
import org.apache.ibatis.annotations.Param;

/**
 * 诊疗套餐项目Mapper接口
 *
 * @author xz
 * @date 2024-03-14
 */
@Mapper
public interface DiagnosisPackageItemMapper extends BaseMapper<DiagnosisPackageItem> {
  /**
   * 查询诊疗套餐项目
   *
   * @param id 诊疗套餐项目主键
   * @return 诊疗套餐项目
   */
  public DiagnosisPackageItem selectDiagnosisPackageItemById(Long id);

  /**
   * 查询诊疗套餐项目列表
   *
   * @param diagnosisPackageItem 诊疗套餐项目
   * @return 诊疗套餐项目集合
   */
  public List<DiagnosisPackageItem> selectDiagnosisPackageItemList(DiagnosisPackageItem diagnosisPackageItem);

  /**
   * 新增诊疗套餐项目
   *
   * @param diagnosisPackageItem 诊疗套餐项目
   * @return 结果
   */
  public int insertDiagnosisPackageItem(DiagnosisPackageItem diagnosisPackageItem);

  /**
   * 修改诊疗套餐项目
   *
   * @param diagnosisPackageItem 诊疗套餐项目
   * @return 结果
   */
  public int updateDiagnosisPackageItem(DiagnosisPackageItem diagnosisPackageItem);

  /**
   * 删除诊疗套餐项目
   *
   * @param id 诊疗套餐项目主键
   * @return 结果
   */
  public int deleteDiagnosisPackageItemById(Long id);

  /**
   * 批量删除诊疗套餐项目
   *
   * @param ids 需要删除的数据主键集合
   * @return 结果
   */
  public int deleteDiagnosisPackageItemByIds(Long[] ids);

  /**
   * 根据套餐id批量删除诊疗套餐项目
   *
   * @param ids 需要删除的数据主键集合
   * @return 结果
   */
  public int deleteByPackageIds(Long[] ids);

  /**
   * 根据套餐ID查询项目列表
   *
   * @param packageId
   * @return
   */
  public List<DiagnosisPackageItemVo> selectByPackageId(@Param("packageId") Long packageId);
}
