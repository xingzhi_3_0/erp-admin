package com.xz.diagnosis.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.diagnosis.vo.DiagnosisCauseVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.diagnosis.domain.DiagnosisCause;

/**
 * 诊疗事项Mapper接口
 *
 * @author xz
 * @date 2024-03-21
 */
@Mapper
public interface DiagnosisCauseMapper  extends BaseMapper<DiagnosisCause> {
    /**
     * 查询诊疗事项
     *
     * @param id 诊疗事项主键
     * @return 诊疗事项
     */
    public DiagnosisCause selectDiagnosisCauseById(Long id);

    /**
     * 查询诊疗事项列表
     *
     * @param diagnosisCause 诊疗事项
     * @return 诊疗事项集合
     */
    public List<DiagnosisCause> selectDiagnosisCauseList(DiagnosisCause diagnosisCause);
    /**
     * 查询诊疗事项列表
     *
     * @param diagnosisCause 诊疗事项
     * @return 诊疗事项集合
     */
    public List<DiagnosisCauseVo> selectDiagnosisCauseVoList(DiagnosisCause diagnosisCause);

    /**
     * 新增诊疗事项
     *
     * @param diagnosisCause 诊疗事项
     * @return 结果
     */
    public int insertDiagnosisCause(DiagnosisCause diagnosisCause);

    /**
     * 修改诊疗事项
     *
     * @param diagnosisCause 诊疗事项
     * @return 结果
     */
    public int updateDiagnosisCause(DiagnosisCause diagnosisCause);

    /**
     * 删除诊疗事项
     *
     * @param id 诊疗事项主键
     * @return 结果
     */
    public int deleteDiagnosisCauseById(Long id);

    /**
     * 批量删除诊疗事项
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDiagnosisCauseByIds(Long[] ids);
}
