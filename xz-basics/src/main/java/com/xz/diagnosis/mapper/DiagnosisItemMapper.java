package com.xz.diagnosis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.diagnosis.domain.DiagnosisItem;
import com.xz.diagnosis.vo.DiagnosisItemVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 诊疗项目Mapper接口
 *
 * @author xz
 * @date 2024-03-14
 */
@Mapper
public interface DiagnosisItemMapper  extends BaseMapper<DiagnosisItem> {
    /**
     * 查询诊疗项目
     *
     * @param id 诊疗项目主键
     * @return 诊疗项目
     */
    public DiagnosisItem selectDiagnosisItemById(Long id);

    /**
     * 查询诊疗项目列表
     *
     * @param diagnosisItem 诊疗项目
     * @return 诊疗项目集合
     */
    public List<DiagnosisItem> selectDiagnosisItemList(DiagnosisItem diagnosisItem);

    /**
     * 查询诊疗项目列表
     *
     * @param diagnosisItem 诊疗项目
     * @return 诊疗项目集合
     */
    public List<DiagnosisItemVo> selectDiagnosisItemVoList(DiagnosisItem diagnosisItem);

    /**
     * 新增诊疗项目
     *
     * @param diagnosisItem 诊疗项目
     * @return 结果
     */
    public int insertDiagnosisItem(DiagnosisItem diagnosisItem);

    /**
     * 修改诊疗项目
     *
     * @param diagnosisItem 诊疗项目
     * @return 结果
     */
    public int updateDiagnosisItem(DiagnosisItem diagnosisItem);

    /**
     * 删除诊疗项目
     *
     * @param id 诊疗项目主键
     * @return 结果
     */
    public int deleteDiagnosisItemById(Long id);

    /**
     * 批量删除诊疗项目
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDiagnosisItemByIds(Long[] ids);
}
