package com.xz.diagnosis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.diagnosis.domain.DiagnosisTime;
import com.xz.diagnosis.vo.DiagnosisTimeTreeVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 诊疗时间项Mapper接口
 *
 * @author xz
 * @date 2024-03-21
 */
@Mapper
public interface DiagnosisTimeMapper  extends BaseMapper<DiagnosisTime> {
    /**
     * 查询诊疗时间项
     *
     * @param id 诊疗时间项主键
     * @return 诊疗时间项
     */
    public DiagnosisTime selectDiagnosisTimeById(Long id);

    /**
     * 查询诊疗时间项列表
     *
     * @param diagnosisTime 诊疗时间项
     * @return 诊疗时间项集合
     */
    public List<DiagnosisTime> selectDiagnosisTimeList(DiagnosisTime diagnosisTime);


    /**
     * 查询诊疗时间项分组列表
     *
     * @param diagnosisTime 诊疗时间项
     * @return 诊疗时间项集合
     */
    public List<DiagnosisTimeTreeVo> selectDiagnosisTimeGroupList(DiagnosisTime diagnosisTime);

    /**
     * 查询诊疗时间项treeVo列表
     *
     * @param diagnosisTime 诊疗时间项
     * @return 诊疗时间项集合
     */
    public List<DiagnosisTimeTreeVo> selectDiagnosisTimeVoList(DiagnosisTime diagnosisTime);
}
