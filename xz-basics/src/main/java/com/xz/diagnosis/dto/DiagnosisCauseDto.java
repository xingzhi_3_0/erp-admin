package com.xz.diagnosis.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 诊疗事项对象 t_diagnosis_cause
 *
 * @author xz
 * @date 2024-03-21
 */
@Data
public class DiagnosisCauseDto{

    /** id */
    private Long id;

    /** 就诊事项 */
    @NotBlank(message = "就诊事项不能为空")
    @Length(max = 32,message = "就诊事项最多{max}个字符")
    private String cause;

    /** 排序 */
    @NotNull(message = "排序不能为空")
    @Min(value = 0,message = "排序必须大于等于{value}")
    @Max(value = 100,message = "排序必须小于等于{value}")
    private Integer sortNo;

}
