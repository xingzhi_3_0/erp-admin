package com.xz.diagnosis.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 诊疗项目对象
 *
 * @author xz
 * @date 2024-03-14
 */
@Data
public class DiagnosisItemDto {
  private static final long serialVersionUID = 1L;

  /**
   * id
   */
  private Long id;

  /**
   * 项目名称
   */
  @NotBlank(message = "项目名称不能为空")
  @Length(max = 32,message = "项目名称最多{max}个字符")
  private String itemName;

  /**
   * 单价（元）
   */
  @NotNull(message = "单价不能为空")
  private BigDecimal price;
  /**
   * 单位
   */
  @NotBlank(message = "单位不能为空")
  @Length(max = 20,message = "单位最多{max}个字符")
  private String unit;


}
