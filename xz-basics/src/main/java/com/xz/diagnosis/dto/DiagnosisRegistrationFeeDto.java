package com.xz.diagnosis.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 诊疗套餐对象 t_diagnosis_registration_fee
 *
 * @author xz
 * @date 2024-03-17
 */
@Data
public class DiagnosisRegistrationFeeDto{

    /** id */
    private Long id;

    /** 医生名称 */
    @NotBlank(message = "医生名称不能为空")
    @Length(max = 32,message = "医生名称最多{max}个字符")
    private String doctorName;

    /** 医生手机号 */
    private String doctorPhone;

    /** 挂号类型 */
    @NotBlank(message = "挂号类型不能为空")
    @Length(max = 12,message = "挂号类型最多{max}个字符")
    private String registrationFeeType;

    /** 挂号费 */
    @NotNull(message = "挂号费不能为空")
    private BigDecimal registrationFee;

}
