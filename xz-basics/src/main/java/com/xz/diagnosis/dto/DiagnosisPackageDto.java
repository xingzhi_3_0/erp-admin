package com.xz.diagnosis.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * 诊疗套餐对象 t_diagnosis_package
 *
 * @author xz
 * @date 2024-03-14
 */
@Data
public class DiagnosisPackageDto {
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 套餐名称 */
    @Length(max = 32,message = "套餐名称最多{max}个字符")
    @NotBlank(message = "套餐名称不能为空")
    private String packageName;

    /** 套餐价（元） */
    @NotNull(message = "套餐价不能为空")
    @DecimalMin(value = "0",message = "套餐价金额必须大于等于{value}")
    private BigDecimal packagePrice;

    /** 状态（0.停用，1.启用） */
    @NotNull(message = "状态不能为空")
    private String status;

    /** 备注 */
    @Length(max = 200,message = "备注最多{max}个字符")
    private String remark;

    /** 套餐项目 */
    @NotEmpty(message = "套餐项目不能为空")
    private List<DiagnosisPackageItemDto> items;


}
