package com.xz.diagnosis.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 诊疗套餐项目对象
 *
 * @author xz
 * @date 2024-03-14
 */
@Data
public class DiagnosisPackageItemDto {
    private static final long serialVersionUID = 1L;

    /** 套餐ID */
    private Long packageId;

    /** 项目ID */
    @NotNull(message = "项目不能为空")
    private Long itemId;

    /** 数量 */
    @NotNull(message = "数量不能为空")
    private Integer quantity;

}
