package com.xz.diagnosis.service.impl;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.diagnosis.domain.DiagnosisTime;
import com.xz.diagnosis.mapper.DiagnosisTimeMapper;
import com.xz.diagnosis.service.IDiagnosisTimeService;
import com.xz.diagnosis.vo.DiagnosisTimeTreeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 诊疗时间项Service业务层处理
 *
 * @author xz
 * @date 2024-03-21
 */
@Service
public class DiagnosisTimeServiceImpl extends ServiceImpl<DiagnosisTimeMapper, DiagnosisTime> implements IDiagnosisTimeService {
  @Autowired
  private DiagnosisTimeMapper diagnosisTimeMapper;

  /**
   * 查询诊疗时间项
   *
   * @param id 诊疗时间项主键
   * @return 诊疗时间项
   */
  @Override
  public DiagnosisTime selectDiagnosisTimeById(Long id) {
    return diagnosisTimeMapper.selectDiagnosisTimeById(id);
  }

  /**
   * 查询诊疗时间项列表
   *
   * @param diagnosisTime 诊疗时间项
   * @return 诊疗时间项
   */
  @Override
  public List<DiagnosisTime> selectDiagnosisTimeList(DiagnosisTime diagnosisTime) {
    return diagnosisTimeMapper.selectDiagnosisTimeList(diagnosisTime);
  }

  /**
   * 查询诊疗时间项列表
   *
   * @param diagnosisTime 诊疗时间项
   * @return 诊疗时间项
   */
  @Override
  public List<DiagnosisTimeTreeVo> treeList(DiagnosisTime diagnosisTime) {

    List<DiagnosisTimeTreeVo> timePeriodList = diagnosisTimeMapper.selectDiagnosisTimeVoList(diagnosisTime);
    if(CollectionUtils.isEmpty(timePeriodList)){
      return new ArrayList<>();
    }
    List<DiagnosisTimeTreeVo> groupList = diagnosisTimeMapper.selectDiagnosisTimeGroupList(diagnosisTime);

    groupList.forEach(group->{
      List<DiagnosisTimeTreeVo> children =  timePeriodList.stream()
        .filter(a -> a.getGroupId().equals(group.getId()))
        .collect(Collectors.toList());
      group.setChildren(children);
    });
    return groupList;
  }
}
