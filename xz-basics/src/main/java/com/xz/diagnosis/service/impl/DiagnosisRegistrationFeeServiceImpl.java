package com.xz.diagnosis.service.impl;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.core.domain.model.LoginUser;
import com.xz.common.exception.ServiceException;
import com.xz.common.utils.DateUtils;
import com.xz.common.utils.SecurityUtils;
import com.xz.diagnosis.domain.DiagnosisRegistrationFee;
import com.xz.diagnosis.domain.DiagnosisTime;
import com.xz.diagnosis.dto.DiagnosisRegistrationFeeDto;
import com.xz.diagnosis.mapper.DiagnosisRegistrationFeeMapper;
import com.xz.diagnosis.service.IDiagnosisRegistrationFeeService;
import com.xz.diagnosis.vo.DiagnosisRegistrationFeeTreeVo;
import com.xz.diagnosis.vo.DiagnosisRegistrationFeeVo;
import com.xz.diagnosis.vo.DiagnosisTimeTreeVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 诊疗套餐Service业务层处理
 *
 * @author xz
 * @date 2024-03-17
 */
@Service
public class DiagnosisRegistrationFeeServiceImpl extends ServiceImpl<DiagnosisRegistrationFeeMapper, DiagnosisRegistrationFee> implements IDiagnosisRegistrationFeeService {
  @Autowired
  private DiagnosisRegistrationFeeMapper diagnosisRegistrationFeeMapper;

  /**
   * 查询诊疗套餐
   *
   * @param id 诊疗套餐主键
   * @return 诊疗套餐
   */
  @Override
  public DiagnosisRegistrationFee selectDiagnosisRegistrationFeeById(Long id) {
    return diagnosisRegistrationFeeMapper.selectDiagnosisRegistrationFeeById(id);
  }

  /**
   * 查询诊疗套餐列表
   *
   * @param diagnosisRegistrationFee 诊疗套餐
   * @return 诊疗套餐
   */
  @Override
  public List<DiagnosisRegistrationFee> selectDiagnosisRegistrationFeeList(DiagnosisRegistrationFee diagnosisRegistrationFee) {
    return diagnosisRegistrationFeeMapper.selectDiagnosisRegistrationFeeList(diagnosisRegistrationFee);
  }

  /**
   * 查询诊疗套餐列表
   *
   * @param diagnosisRegistrationFee 诊疗套餐
   * @return 诊疗套餐
   */
  @Override
  public List<DiagnosisRegistrationFeeVo> selectDiagnosisRegistrationFeeVoList(DiagnosisRegistrationFee diagnosisRegistrationFee) {
    return diagnosisRegistrationFeeMapper.selectDiagnosisRegistrationFeeVoList(diagnosisRegistrationFee);
  }
  /**
   * 新增诊疗套餐
   *
   * @param dto 诊疗套餐
   * @return 结果
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public int insertDiagnosisRegistrationFee(DiagnosisRegistrationFeeDto dto) {
    DiagnosisRegistrationFee entity = new DiagnosisRegistrationFee();
    BeanUtils.copyProperties(dto, entity);
    //填充创建人信息
    LoginUser user = SecurityUtils.getLoginUser();
    entity.setCreateBy(user.getUserId());
    entity.setCreateTime(DateUtils.getNowDate());
    entity.setTenantId(user.getTenantId());
    entity.setDeptId(user.getDeptId());
    return diagnosisRegistrationFeeMapper.insertDiagnosisRegistrationFee(entity);
  }

  /**
   * 修改诊疗套餐
   *
   * @param dto 诊疗套餐
   * @return 结果
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public int updateDiagnosisRegistrationFee(DiagnosisRegistrationFeeDto dto) {
    DiagnosisRegistrationFee entity = baseMapper.selectById(dto.getId());
    if(null != entity){
      BeanUtils.copyProperties(dto, entity);
      //填充更新人信息
      LoginUser user = SecurityUtils.getLoginUser();
      entity.setUpdateBy(user.getUserId());
      entity.setUpdateTime(DateUtils.getNowDate());
      return diagnosisRegistrationFeeMapper.updateDiagnosisRegistrationFee(entity);
    }
    return 0;
  }

  /**
   * 批量删除诊疗套餐
   *
   * @param ids 需要删除的诊疗套餐主键
   * @return 结果
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public int deleteDiagnosisRegistrationFeeByIds(Long[] ids) {
    return diagnosisRegistrationFeeMapper.deleteDiagnosisRegistrationFeeByIds(ids);
  }

  /**
   * 查询诊疗时间项treeVo列表
   *
   * @param fee 诊疗时间项
   * @return 诊疗时间项
   */
  @Override
  public List<DiagnosisRegistrationFeeTreeVo> treeList(DiagnosisRegistrationFee fee) {

    List<DiagnosisRegistrationFeeVo> timePeriodList = diagnosisRegistrationFeeMapper.selectDiagnosisRegistrationFeeVoList(fee);
    if(CollectionUtils.isEmpty(timePeriodList)){
      return new ArrayList<>();
    }
    List<DiagnosisRegistrationFeeVo> groupList = diagnosisRegistrationFeeMapper.selectDiagnosisRegistrationFeeGroupList(fee);

    List<DiagnosisRegistrationFeeTreeVo> feeTreeVos = new ArrayList<>();
    groupList.forEach(group->{
      DiagnosisRegistrationFeeTreeVo treeVo = DiagnosisRegistrationFeeTreeVo.initTreeVo(group.getRegistrationFeeType(),timePeriodList);
      feeTreeVos.add(treeVo);
    });
    return feeTreeVos;
  }
}
