package com.xz.diagnosis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.diagnosis.domain.DiagnosisTime;
import com.xz.diagnosis.vo.DiagnosisTimeTreeVo;

import java.util.List;

/**
 * 诊疗时间项Service接口
 *
 * @author xz
 * @date 2024-03-21
 */
public interface IDiagnosisTimeService extends IService<DiagnosisTime> {
  /**
   * 查询诊疗时间项
   *
   * @param id 诊疗时间项主键
   * @return 诊疗时间项
   */
  public DiagnosisTime selectDiagnosisTimeById(Long id);

  /**
   * 查询诊疗时间项列表
   *
   * @param diagnosisTime 诊疗时间项
   * @return 诊疗时间项集合
   */
  public List<DiagnosisTime> selectDiagnosisTimeList(DiagnosisTime diagnosisTime);

  /**
   * 查询诊疗时间项treeVo列表
   *
   * @param diagnosisTime 诊疗时间项
   * @return 诊疗时间项集合
   */
  public List<DiagnosisTimeTreeVo> treeList(DiagnosisTime diagnosisTime);
}
