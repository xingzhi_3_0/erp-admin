package com.xz.diagnosis.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.diagnosis.domain.DiagnosisCause;
import com.xz.diagnosis.dto.DiagnosisCauseDto;
import com.xz.diagnosis.vo.DiagnosisCauseVo;

/**
 * 诊疗事项Service接口
 *
 * @author xz
 * @date 2024-03-21
 */
public interface IDiagnosisCauseService extends IService<DiagnosisCause> {
    /**
     * 查询诊疗事项
     *
     * @param id 诊疗事项主键
     * @return 诊疗事项
     */
    public DiagnosisCause selectDiagnosisCauseById(Long id);

    /**
     * 查询诊疗事项列表
     *
     * @param diagnosisCause 诊疗事项
     * @return 诊疗事项集合
     */
    public List<DiagnosisCause> selectDiagnosisCauseList(DiagnosisCause diagnosisCause);

  /**
   * 查询诊疗事项Vo列表
   *
   * @param diagnosisCause 诊疗事项
   * @return 诊疗事项集合
   */
  public List<DiagnosisCauseVo> selectDiagnosisCauseVoList(DiagnosisCause diagnosisCause);
    /**
     * 新增诊疗事项
     *
     * @param dto 诊疗事项
     * @return 结果
     */
    public int insertDiagnosisCause(DiagnosisCauseDto dto);

    /**
     * 修改诊疗事项
     *
     * @param dto 诊疗事项
     * @return 结果
     */
    public int updateDiagnosisCause(DiagnosisCauseDto dto);

    /**
     * 批量删除诊疗事项
     *
     * @param ids 需要删除的诊疗事项主键集合
     * @return 结果
     */
    public int deleteDiagnosisCauseByIds(Long[] ids);
}
