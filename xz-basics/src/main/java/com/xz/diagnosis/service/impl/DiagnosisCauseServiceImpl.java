package com.xz.diagnosis.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.core.domain.model.LoginUser;
import com.xz.common.exception.ServiceException;
import com.xz.common.utils.DateUtils;
import com.xz.common.utils.SecurityUtils;
import com.xz.diagnosis.domain.DiagnosisCause;
import com.xz.diagnosis.dto.DiagnosisCauseDto;
import com.xz.diagnosis.mapper.DiagnosisCauseMapper;
import com.xz.diagnosis.service.IDiagnosisCauseService;
import com.xz.diagnosis.vo.DiagnosisCauseVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 诊疗事项Service业务层处理
 *
 * @author xz
 * @date 2024-03-21
 */
@Service
public class DiagnosisCauseServiceImpl extends ServiceImpl<DiagnosisCauseMapper, DiagnosisCause> implements IDiagnosisCauseService {
  @Autowired
  private DiagnosisCauseMapper diagnosisCauseMapper;

  /**
   * 查询诊疗事项
   *
   * @param id 诊疗事项主键
   * @return 诊疗事项
   */
  @Override
  public DiagnosisCause selectDiagnosisCauseById(Long id) {
    return diagnosisCauseMapper.selectDiagnosisCauseById(id);
  }

  /**
   * 查询诊疗事项列表
   *
   * @param diagnosisCause 诊疗事项
   * @return 诊疗事项
   */
  @Override
  public List<DiagnosisCause> selectDiagnosisCauseList(DiagnosisCause diagnosisCause) {
    return diagnosisCauseMapper.selectDiagnosisCauseList(diagnosisCause);
  }

  /**
   * 查询诊疗事项列表
   *
   * @param diagnosisCause 诊疗事项
   * @return 诊疗事项
   */
  @Override
  public List<DiagnosisCauseVo> selectDiagnosisCauseVoList(DiagnosisCause diagnosisCause) {
    return diagnosisCauseMapper.selectDiagnosisCauseVoList(diagnosisCause);
  }

  /**
   * 新增诊疗事项
   *
   * @param dto 诊疗事项
   * @return 结果
   */
  @Override
  public int insertDiagnosisCause(DiagnosisCauseDto dto) {
    DiagnosisCause entity = new DiagnosisCause();
    BeanUtils.copyProperties(dto, entity);
    //校验字段
    verifyFields(entity);
    //填充创建人信息
    LoginUser user = SecurityUtils.getLoginUser();
    entity.setCreateBy(user.getUserId());
    entity.setCreateTime(DateUtils.getNowDate());
    entity.setTenantId(user.getTenantId());
    entity.setDeptId(user.getDeptId());
    return diagnosisCauseMapper.insertDiagnosisCause(entity);
  }

  /**
   * 修改诊疗事项
   *
   * @param dto 诊疗事项
   * @return 结果
   */
  @Override
  public int updateDiagnosisCause(DiagnosisCauseDto dto) {

    DiagnosisCause entity = baseMapper.selectById(dto.getId());
    if (null != entity) {
      BeanUtils.copyProperties(dto, entity);
      //校验字段
      verifyFields(entity);
      //填充更新人信息
      LoginUser user = SecurityUtils.getLoginUser();
      entity.setUpdateBy(user.getUserId());
      entity.setUpdateTime(DateUtils.getNowDate());
      return diagnosisCauseMapper.updateDiagnosisCause(entity);
    }
    return 0;
  }

  private void verifyFields(DiagnosisCause entity) {

    // 校验名称唯一性
    LambdaQueryWrapper<DiagnosisCause> wrapper = Wrappers.lambdaQuery(DiagnosisCause.class);
    wrapper.eq(DiagnosisCause::getCause, entity.getCause());
    wrapper.ne(null != entity.getId(), DiagnosisCause::getId, entity.getId());
    wrapper.eq(DiagnosisCause::getDelFlag, 0);
    int count = diagnosisCauseMapper.selectCount(wrapper);
    if (0 < count) {
      throw new ServiceException("该事项已存在");
    }
  }

  /**
   * 批量删除诊疗事项
   *
   * @param ids 需要删除的诊疗事项主键
   * @return 结果
   */
  @Override
  public int deleteDiagnosisCauseByIds(Long[] ids) {
    return diagnosisCauseMapper.deleteDiagnosisCauseByIds(ids);
  }
}
