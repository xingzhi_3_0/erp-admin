package com.xz.diagnosis.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.diagnosis.domain.DiagnosisItem;
import com.xz.diagnosis.dto.DiagnosisItemDto;
import com.xz.diagnosis.vo.DiagnosisItemVo;

/**
 * 诊疗项目Service接口
 *
 * @author xz
 * @date 2024-03-14
 */
public interface IDiagnosisItemService extends IService<DiagnosisItem> {
    /**
     * 查询诊疗项目
     *
     * @param id 诊疗项目主键
     * @return 诊疗项目
     */
    public DiagnosisItem selectDiagnosisItemById(Long id);

    /**
     * 查询诊疗项目列表
     *
     * @param diagnosisItem 诊疗项目
     * @return 诊疗项目集合
     */
    public List<DiagnosisItem> selectDiagnosisItemList(DiagnosisItem diagnosisItem);
    /**
     * 查询诊疗项目列表
     *
     * @param diagnosisItem 诊疗项目
     * @return 诊疗项目集合
     */
    public List<DiagnosisItemVo> selectDiagnosisItemVoList(DiagnosisItem diagnosisItem);

    /**
     * 新增诊疗项目
     *
     * @param dto 诊疗项目
     * @return 结果
     */
    public int insertDiagnosisItem(DiagnosisItemDto dto);

    /**
     * 修改诊疗项目
     *
     * @param dto 诊疗项目
     * @return 结果
     */
    public int updateDiagnosisItem(DiagnosisItemDto dto);

    /**
     * 批量删除诊疗项目
     *
     * @param ids 需要删除的诊疗项目主键集合
     * @return 结果
     */
    public int deleteDiagnosisItemByIds(Long[] ids);

}
