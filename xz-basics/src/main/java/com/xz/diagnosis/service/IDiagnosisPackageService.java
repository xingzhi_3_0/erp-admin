package com.xz.diagnosis.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.diagnosis.domain.DiagnosisPackage;
import com.xz.diagnosis.dto.DiagnosisPackageDto;
import com.xz.diagnosis.vo.DiagnosisPackageVo;
import com.xz.diagnosis.vo.PackageUnionItemVo;

/**
 * 诊疗套餐Service接口
 *
 * @author xz
 * @date 2024-03-14
 */
public interface IDiagnosisPackageService extends IService<DiagnosisPackage> {
  /**
   * 查询诊疗套餐
   *
   * @param id 诊疗套餐主键
   * @return 诊疗套餐
   */
  public DiagnosisPackage selectDiagnosisPackageById(Long id);

  /**
   * 查询诊疗套餐列表
   *
   * @param diagnosisPackage 诊疗套餐
   * @return 诊疗套餐集合
   */
  public List<DiagnosisPackage> selectDiagnosisPackageList(DiagnosisPackage diagnosisPackage);

  /**
   * 查询诊疗套餐列表
   *
   * @param diagnosisPackage 诊疗套餐
   * @return 诊疗套餐
   */
  public List<DiagnosisPackageVo> selectDiagnosisPackageVoList(DiagnosisPackage diagnosisPackage);

  /**
   * 查询诊疗套餐与项目合并列表
   *
   * @param diagnosisPackage 诊疗套餐
   * @return
   */
  List<PackageUnionItemVo> selectPackageUnionItemList(DiagnosisPackage diagnosisPackage);

  /**
   * 新增诊疗套餐
   *
   * @param dto 诊疗套餐
   * @return 结果
   */
  public int insertDiagnosisPackage(DiagnosisPackageDto dto);

  /**
   * 修改诊疗套餐
   *
   * @param dto 诊疗套餐
   * @return 结果
   */
  public int updateDiagnosisPackage(DiagnosisPackageDto dto);

  /**
   * 批量删除诊疗套餐
   *
   * @param ids 需要删除的诊疗套餐主键集合
   * @return 结果
   */
  public int deleteDiagnosisPackageByIds(Long[] ids);

}
