package com.xz.diagnosis.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.diagnosis.domain.DiagnosisPackage;
import com.xz.diagnosis.domain.DiagnosisPackageItem;
import com.xz.diagnosis.dto.DiagnosisPackageItemDto;
import com.xz.diagnosis.vo.DiagnosisPackageItemVo;

/**
 * 诊疗套餐项目Service接口
 *
 * @author xz
 * @date 2024-03-14
 */
public interface IDiagnosisPackageItemService extends IService<DiagnosisPackageItem> {

  /**
   * 根据套餐ID查询项目列表
   * @param packageId
   * @return
   */
  List<DiagnosisPackageItemVo> selectByPackageId(Long packageId);

  /**
   * 新增诊疗套餐项目
   *
   * @param packageEntity
   * @param dtos
   * @param isUpdate
   */
  public void insertForPackage(DiagnosisPackage packageEntity, List<DiagnosisPackageItemDto> dtos, boolean isUpdate);

  /**
   * 根据套餐ID查询项目列表
   *
   * @param ids 需要删除的诊疗套餐主键
   * @return 结果
   */
  public int deleteByPackageIds(Long[] ids);
}
