package com.xz.diagnosis.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.diagnosis.domain.DiagnosisRegistrationFee;
import com.xz.diagnosis.dto.DiagnosisRegistrationFeeDto;
import com.xz.diagnosis.vo.DiagnosisRegistrationFeeTreeVo;
import com.xz.diagnosis.vo.DiagnosisRegistrationFeeVo;

/**
 * 诊疗套餐Service接口
 *
 * @author xz
 * @date 2024-03-17
 */
public interface IDiagnosisRegistrationFeeService extends IService<DiagnosisRegistrationFee> {
  /**
   * 查询诊疗套餐
   *
   * @param id 诊疗套餐主键
   * @return 诊疗套餐
   */
  public DiagnosisRegistrationFee selectDiagnosisRegistrationFeeById(Long id);

  /**
   * 查询诊疗套餐列表
   *
   * @param diagnosisRegistrationFee 诊疗套餐
   * @return 诊疗套餐集合
   */
  public List<DiagnosisRegistrationFee> selectDiagnosisRegistrationFeeList(DiagnosisRegistrationFee diagnosisRegistrationFee);

  /**
   * 查询诊疗套餐列表
   *
   * @param diagnosisRegistrationFee 诊疗套餐
   * @return 诊疗套餐集合
   */
  public List<DiagnosisRegistrationFeeVo> selectDiagnosisRegistrationFeeVoList(DiagnosisRegistrationFee diagnosisRegistrationFee);

  /**
   * 新增诊疗套餐
   *
   * @param diagnosisRegistrationFee 诊疗套餐
   * @return 结果
   */
  public int insertDiagnosisRegistrationFee(DiagnosisRegistrationFeeDto diagnosisRegistrationFee);

  /**
   * 修改诊疗套餐
   *
   * @param diagnosisRegistrationFee 诊疗套餐
   * @return 结果
   */
  public int updateDiagnosisRegistrationFee(DiagnosisRegistrationFeeDto diagnosisRegistrationFee);

  /**
   * 批量删除诊疗套餐
   *
   * @param ids 需要删除的诊疗套餐主键集合
   * @return 结果
   */
  public int deleteDiagnosisRegistrationFeeByIds(Long[] ids);

  /**
   * 查询诊疗时间项treeVo列表
   *
   * @param fee 诊疗时间项
   * @return 诊疗时间项
   */
  public List<DiagnosisRegistrationFeeTreeVo> treeList(DiagnosisRegistrationFee fee);
}
