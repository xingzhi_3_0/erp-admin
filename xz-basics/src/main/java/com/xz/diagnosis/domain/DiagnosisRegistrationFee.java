package com.xz.diagnosis.domain;

import java.math.BigDecimal;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 诊疗套餐对象 t_diagnosis_registration_fee
 *
 * @author xz
 * @date 2024-03-17
 */
@Data
@TableName("t_diagnosis_registration_fee")
public class DiagnosisRegistrationFee extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 医生名称 */
    @Excel(name = "医生名称")
    private String doctorName;

    /** 医生手机号 */
    @Excel(name = "医生手机号")
    private String doctorPhone;

    /** 门诊类型 */
    @Excel(name = "门诊类型")
    private String registrationFeeType;

    /** 挂号费 */
    @Excel(name = "挂号费")
    private BigDecimal registrationFee;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    @NotNull(message = "租户id不能为空")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

}
