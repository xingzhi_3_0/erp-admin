package com.xz.diagnosis.domain;

import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 诊疗套餐项目对象
 *
 * @author xz
 * @date 2024-03-14
 */
@Data
@TableName("t_diagnosis_package_item")
public class DiagnosisPackageItem extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 套餐ID */
    @Excel(name = "套餐ID")
    @NotNull(message = "套餐ID不能为空")
    private Long packageId;

    /** 项目ID */
    @Excel(name = "项目ID")
    @NotNull(message = "项目ID不能为空")
    private Long itemId;

    /** 数量 */
    @Excel(name = "数量")
    private Integer quantity;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    @NotNull(message = "租户id不能为空")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

}
