package com.xz.diagnosis.domain;

import java.math.BigDecimal;

import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import lombok.Data;
import com.xz.common.core.domain.BaseEntity;
import org.hibernate.validator.constraints.Length;

/**
 * 诊疗项目对象
 *
 * @author xz
 * @date 2024-03-14
 */
@Data
@TableName("t_diagnosis_item")
public class DiagnosisItem extends BaseEntity {
  private static final long serialVersionUID = 1L;

  /**
   * id
   */
  private Long id;

  /**
   * 项目编码
   */
  @Excel(name = "项目编码")
  private String itemNo;

  /**
   * 项目名称
   */
  @Excel(name = "项目名称")
  private String itemName;

  /**
   * 单价（元）
   */
  @Excel(name = "单价", readConverterExp = "元=")
  @DecimalMin(value = "0",message = "单价必须大于等于{value}")
  private BigDecimal price;

  /**
   * 单位
   */
  @Excel(name = "单位")
  private String unit;

  /**
   * 删除标志（0代表存在 2代表删除）
   */
  private Long delFlag;

  /**
   * 租户id
   */
  @Excel(name = "租户id")
  @NotNull(message = "租户id不能为空")
  private Long tenantId;

  /**
   * 部门id
   */
  @Excel(name = "部门id")
  private Long deptId;

}
