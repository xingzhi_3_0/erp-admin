package com.xz.diagnosis.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 诊疗套餐对象 t_diagnosis_package
 *
 * @author xz
 * @date 2024-03-14
 */
@Data
@TableName("t_diagnosis_package")
public class DiagnosisPackage extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 套餐编码 */
    @Excel(name = "套餐编码")
    private String packageNo;

    /** 套餐名称 */
    @Excel(name = "套餐名称")
    private String packageName;

    /** 套餐价（元） */
    @Excel(name = "套餐价", readConverterExp = "元=")
    private BigDecimal packagePrice;

    /** 组合价（元） */
    @Excel(name = "组合价", readConverterExp = "元=")
    private BigDecimal price;

    /** 状态（0.停用，1.启用） */
    @Excel(name = "状态", readConverterExp = "0=.停用，1.启用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    @NotNull(message = "租户id不能为空")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    /** 检查项目 */
    @Excel(name = "检查项目")
    @TableField(exist = false)
    private String itemNames;

    /** 检查套餐/项目名称  */
    @TableField(exist = false)
    private String nameKeyWord;
}
