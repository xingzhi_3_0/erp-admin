package com.xz.diagnosis.domain;

import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 诊疗事项对象 t_diagnosis_cause
 *
 * @author xz
 * @date 2024-03-21
 */
@Data
@TableName("t_diagnosis_cause")
public class DiagnosisCause extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 事项名称 */
    @Excel(name = "事项名称")
    private String cause;

    /** 排序 */
    @Excel(name = "排序")
    private Integer sortNo;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    @NotNull(message = "租户id不能为空")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

}
