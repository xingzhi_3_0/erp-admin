package com.xz.logistics.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.kuaidi100.sdk.api.QueryTrack;
import com.kuaidi100.sdk.core.IBaseClient;
import com.kuaidi100.sdk.pojo.HttpResult;
import com.kuaidi100.sdk.request.QueryTrackParam;
import com.kuaidi100.sdk.request.QueryTrackReq;
import com.kuaidi100.sdk.response.QueryTrackResp;
import com.kuaidi100.sdk.utils.SignUtils;
import com.xz.common.config.XzConfig;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.redis.RedisCache;
import com.xz.common.exception.ServiceException;
import com.xz.common.utils.StringUtils;
import com.xz.logistics.service.ILogisticsQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


@Service
public class LogisticsQueryServiceImpl implements ILogisticsQueryService {

  @Autowired
  private RedisCache redisCache;

  /**
   * 快递100的key
   **/
  @Value("${logistics.kuaidi.key}")
  private String kuaidiKey;
  /**
   * 快递100的授权码
   **/
  @Value("${logistics.kuaidi.customer}")
  private String kuaidiCustomer;
  /**
   * 查询时间间隔（分钟）
   **/
  @Value("${logistics.interval.expiretime}")
  private Integer intervalExpireTime;
  /**
   * 数据缓存时间分钟）
   **/
  @Value("${logistics.data.existencetime}")
  private Integer dataExistenceTime;

  /**
   * 快递100 实际数据缓存的key
   **/
  private String KUAIDI_DATA_KEY = XzConfig.getRedisPrefix() + "kuaidi:data:";
  /**
   * 快递100 间隔的key
   **/
  private String KUAIDI_INTERVAL_KEY = XzConfig.getRedisPrefix() + "kuaidi:interval:";


  /**
   * 构建物流的redis缓存key
   * @param key
   * @param phone
   * @return
   */
  private static String getKey(String key, String phone) {
    return key + phone;
  }

  /**
   * 查询快递（优先缓存，并且限制相同单号频繁查询）
   * @param param
   * @return
   */
  @Override
  public AjaxResult queryLogisticsByCaptcha(QueryTrackParam param) {
    //校验参数
    verifyParam(param);
    /*// 构建数据缓存的key
    String dataKey = getKey(KUAIDI_DATA_KEY, param.getNum());
    // 利用key先尝试在缓存中提取出数据
    String captchaData = redisCache.getCacheObject(dataKey);
    if (StringUtils.isNotEmpty(captchaData)) {
      // 提取成功，直接返回。
      return JSONObject.parseObject(captchaData, QueryTrackResp.class);
    }*/
    // 提取不成功，开始调用第三方。

    // 开始判断查询间隔是否存在，如果存在（大于0）则需要做相应的处理（限制相同单号频繁查询）
    /*if (null != intervalExpireTime && 0 != intervalExpireTime.intValue()) {
      // 构建查数据间隔缓存的key
      String intervalKey = getKey(KUAIDI_INTERVAL_KEY, param.getPhone());
      String interval = redisCache.getCacheObject(intervalKey);
      if (StringUtils.isNotEmpty(interval)) {
        // 存在间隔，说明查询频繁，直接new一个数据返回。
        QueryTrackResp queryTrackResp = new QueryTrackResp();
        queryTrackResp.setMessage("请勿频繁查询");
        queryTrackResp.setStatus("0");
        return queryTrackResp;
      }
      // 无论后续查询是否成功，都缓存此次调用，已避免在请求不到的数据情况下多次请求。
      redisCache.setCacheObject(intervalKey, ECDateUtils.getCurrentDateTimeAsString(), intervalExpireTime, TimeUnit.MINUTES);
    }*/

    // 开始调用第三方查快递信息
    QueryTrackResp queryTrackMapResp = null;
    try {
      queryTrackMapResp = queryLogistics(param);
    } catch (Exception e) {
      e.printStackTrace();
    }
    //查询成功，则缓存数据
    /*if (null != queryTrackMapResp) {
      if("200".equals(queryTrackMapResp.getStatus())){
        // 查询成功才缓存
        redisCache.setCacheObject(dataKey, JSONObject.toJSONString(queryTrackMapResp), dataExistenceTime, TimeUnit.MINUTES);
      }
    }*/
    // 查询快递信息
    if(null != queryTrackMapResp){
      if(null != queryTrackMapResp.getStatus() && "200".equals(queryTrackMapResp.getStatus())){
        return AjaxResult.success(queryTrackMapResp);
      }
      return AjaxResult.error(queryTrackMapResp.getMessage());
    }
    return AjaxResult.error();
  }

  /**
   * 校验参数
   * @param param
   */
  private void verifyParam(QueryTrackParam param) {
    if(StringUtils.isEmpty(param.getCom())){
      throw new ServiceException("查询失败，快递公司不能为空");
    }
    if(StringUtils.isEmpty(param.getNum())){
      throw new ServiceException("查询失败，快递单号不能为空");
    }

    if(param.getCom().equals("shunfengkuaiyun") || param.getCom().equals("shunfeng")){
      if(StringUtils.isEmpty(param.getPhone())){
        throw new ServiceException("顺丰公司的快递，其寄件人电话号码不能为空");
      }
    }

  }

  /**
   * 查询快递
   *
   * @param param
   * @return
   * @throws Exception
   */
  @Override
  public QueryTrackResp queryLogistics(QueryTrackParam param) throws Exception {
    QueryTrackReq queryTrackReq = new QueryTrackReq();
    String paramJson = new Gson().toJson(param);
    queryTrackReq.setParam(paramJson);
    queryTrackReq.setCustomer(kuaidiCustomer);
    queryTrackReq.setSign(SignUtils.querySign(paramJson, kuaidiKey, kuaidiCustomer));

    IBaseClient baseClient = new QueryTrack();
    HttpResult result = baseClient.execute(queryTrackReq);

    return JSONObject.parseObject(result.getBody(), QueryTrackResp.class);
  }



}
