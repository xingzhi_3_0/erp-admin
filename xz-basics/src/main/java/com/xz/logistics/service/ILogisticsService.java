package com.xz.logistics.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.logistics.domain.Logistics;
import com.xz.logistics.dto.LogisticsDto;
import com.xz.sales.domain.SalesOrder;
import com.xz.supplierOrder.domain.SupplierOrder;
import com.xz.supplierOrder.dto.SupplierOrderDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface ILogisticsService extends IService<Logistics> {

	/**
	 * 查询业务订单列表
	 *
	 * @param businessNo 业务单号，目前业务单号前缀没有出现重复的情况，所以可以直接锁定单据。
	如果后续出现新的业务，并且该业务单据的单号前缀会与其他一样，那就要优化这块代码了
	 * @return 贸易中间商订单
	 */
	List<Logistics> selectByBusinessNo(String businessNo);

	/**
	 * 保存
	 * @param entity
	 */
	AjaxResult saveLogistics(Logistics entity);

	/**
	 * 修改
	 * @param entity
	 */
	AjaxResult updateLogistics(Logistics entity);

	/**
	 * 根据id删除
	 * @param id
	 */
	void deleteById(Long id);

	/**
	 * 根据业务删除
	 * @param module
	 * @param businessId
	 * @param businessType
	 */
	void delByBusiness(Integer module, Long businessId, Integer businessType);

	/**
	 * 根据业务保存（先根据业务编号删，再批量新增）
	 * @param module
	 * @param businessId
	 * @param businessNo
	 * @param businessType
	 * @param dtos
	 */
	void saveByBusinessNo(Integer module, Long businessId, String businessNo, Integer businessType,
	                      List<LogisticsDto> dtos);

	/**
	 * 根据业务保存
	 * @param module
	 * @param businessId
	 * @param businessNo
	 * @param businessType
	 * @param entity
	 */
	void saveByBusinessNo(Integer module, Long businessId, String businessNo, Integer businessType, Logistics entity);
}
