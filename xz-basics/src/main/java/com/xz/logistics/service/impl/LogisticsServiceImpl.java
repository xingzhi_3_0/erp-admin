package com.xz.logistics.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.domain.model.LoginUser;
import com.xz.common.utils.DateUtils;
import com.xz.common.utils.SecurityUtils;
import com.xz.common.utils.StringUtils;
import com.xz.logistics.domain.Logistics;
import com.xz.logistics.dto.LogisticsDto;
import com.xz.logistics.mapper.LogisticsMapper;
import com.xz.logistics.service.ILogisticsService;
import com.xz.system.service.ISysDictDataService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


@Service
public class LogisticsServiceImpl extends ServiceImpl<LogisticsMapper, Logistics> implements ILogisticsService {
	@Resource
	private LogisticsMapper logisticsMapper;
	@Autowired
	private ISysDictDataService sysDictDataService;
	/**
	 * 查询业务订单列表
	 *
	 * @param businessNo 业务单号，目前业务单号前缀没有出现重复的情况，所以可以直接锁定单据。
	 *                   如果后续出现新的业务，并且该业务单据的单号前缀会与其他一样，那就要优化这块代码了
	 * @return 贸易中间商订单
	 */
	@Override
	public List<Logistics> selectByBusinessNo(String businessNo) {
		LambdaQueryWrapper<Logistics> wrapper = Wrappers.lambdaQuery(Logistics.class);
		wrapper.eq(Logistics::getBusinessNo, businessNo);
		wrapper.eq(Logistics::getDelFlag, 0);
		return logisticsMapper.selectList(wrapper);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public AjaxResult saveLogistics(Logistics entity) {

		String companies = entity.getExpressDeliveryCompanies();
		String companiesName =  entity.getExpressDeliveryCompaniesName();
		// 如果没有传入快递公司名称时，而有快递公司Code时，则需要把快递公司查出来，然后填充
		if (StringUtils.isEmpty(companiesName) && !StringUtils.isEmpty(companies)) {
			companiesName = sysDictDataService.selectDictLabel("delivery_companies",companies);
			entity.setExpressDeliveryCompaniesName(companiesName);
		}
		//填充创建人信息
		LoginUser user = SecurityUtils.getLoginUser();
		entity.setCreateBy(user.getUserId());
		entity.setCreateTime(DateUtils.getNowDate());
		entity.setTenantId(user.getTenantId());
		entity.setDeptId(user.getDeptId());

		entity.setId(null);//避免前端或者其他人调用该新增方法，乱传id值
		boolean flag = this.save(entity);
		return flag ? AjaxResult.success() : AjaxResult.error();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public AjaxResult updateLogistics(Logistics entity) {

		//填充更新人信息
		LoginUser user = SecurityUtils.getLoginUser();
		entity.setUpdateBy(user.getUserId());
		entity.setUpdateTime(DateUtils.getNowDate());
		boolean flag = this.updateById(entity);
		return flag ? AjaxResult.success() : AjaxResult.error();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteById(Long id) {
		if (null == id) {
			return;
		}
		logisticsMapper.deleteById(id);
	}

	/**
	 * 根据业务删除物流
	 * @param module
	 * @param businessId
	 * @param businessType
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public  void delByBusiness(Integer module, Long businessId, Integer businessType) {
		LambdaQueryWrapper<Logistics> wrapper = Wrappers.lambdaQuery(Logistics.class);
		wrapper.eq(Logistics::getModule, module);
		wrapper.eq(Logistics::getBusinessId, businessId);
		wrapper.eq(Logistics::getBusinessNo, businessType);
		logisticsMapper.delete(wrapper);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveByBusinessNo(Integer module, Long businessId, String businessNo, Integer businessType,
	                             List<LogisticsDto> dtos) {
		// 根据id删
		delByBusiness(module, businessId, businessType);
		// 为空说明没有，直接返回即可
		if(CollectionUtils.isEmpty(dtos)){
			return;
		}
		// 新增
		dtos.forEach(dto->{
			Logistics entity = new Logistics();
			BeanUtils.copyProperties(dto,entity);
			// 填充归属模块，业务信息字段
			entity.setModule(module);
			entity.setBusinessId(businessId);
			entity.setBusinessNo(businessNo);
			entity.setBusinessType(businessType);
			this.saveLogistics(entity);
		});
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveByBusinessNo(Integer module, Long businessId, String businessNo, Integer businessType, Logistics entity) {
		// 根据id删
		delByBusiness(module, businessId, businessType);
		// 填充归属模块，业务信息字段
		entity.setModule(module);
		entity.setBusinessId(businessId);
		entity.setBusinessNo(businessNo);
		entity.setBusinessType(businessType);
		this.saveLogistics(entity);
	}
}
