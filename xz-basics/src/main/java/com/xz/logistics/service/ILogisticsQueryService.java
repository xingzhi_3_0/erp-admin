package com.xz.logistics.service;

import com.kuaidi100.sdk.request.QueryTrackParam;
import com.kuaidi100.sdk.response.QueryTrackResp;
import com.xz.common.core.domain.AjaxResult;


public interface ILogisticsQueryService {
  /**
   * 查询快递（优先缓存，并且限制相同单号频繁查询）
   *
   * @param param 参数
   */
  QueryTrackResp queryLogistics(QueryTrackParam param) throws Exception ;
  /**
   * 查询快递
   *
   * @param param 参数
   */
  AjaxResult queryLogisticsByCaptcha(QueryTrackParam param);
}
