package com.xz.logistics.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.logistics.domain.Logistics;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface LogisticsMapper extends BaseMapper<Logistics> {

}
