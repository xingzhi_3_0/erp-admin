package com.xz.logistics.enums;


public enum LogisticsModuleEnum {


	ORDER(1, "订单模块"),

	SUPPLIER_ORDER(2, "贸易中间商订单模块"),
	;
	private final Integer module;

	private final String name;

	LogisticsModuleEnum(Integer module, String name) {
		this.module = module;
		this.name = name;
	}

	public Integer getModule() {
		return module;
	}

	public String getName() {
		return name;
	}
}
