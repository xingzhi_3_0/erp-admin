package com.xz.logistics.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;


@Data
@TableName("t_logistics")
public class Logistics extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    @Excel(name = "业务模块：1-订单模块,2-贸易中间商模块")
    private Integer module;
    @Excel(name = "业务ID")
    private Long businessId;
    @Excel(name = "业务编号")
    private String businessNo;
    @Excel(name = "业务类型（根据模块中各个类型单据来设定）")
    private Integer businessType;
    @Excel(name = "收件地址")
    private String address;
    @Excel(name = "快递公司字典值")
    private String expressDeliveryCompanies;
    @Excel(name = "快递公司")
    private String expressDeliveryCompaniesName;
    @Excel(name = "快递单号")
    private String expressTrackingNumber;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "寄件时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date expressShippingTime;
    @Excel(name = "寄件人")
    private String expressSender;
    @Excel(name = "寄件人号码")
    private String expressSenderPhone;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;
    /** 租户id */
    @Excel(name = "租户id")
    @NotNull(message = "租户id不能为空")
    private Long tenantId;
    /** 部门id(门店id) */
    @Excel(name = "部门id(门店id)")
    @NotNull(message = "部门id(门店id)不能为空")
    private Long deptId;
    /** 创建时间开始 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createStartTime;
    /** 创建时间截止 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createEndTime;

}
