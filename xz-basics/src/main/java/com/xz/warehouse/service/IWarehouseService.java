package com.xz.warehouse.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.warehouse.domain.Warehouse;
import com.xz.warehouse.dto.WarehouseDto;
import org.aspectj.weaver.loadtime.Aj;

/**
 * 仓库Service接口
 * 
 * @author xz
 * @date 2024-01-04
 */
public interface IWarehouseService extends IService<Warehouse> {
    /**
     * 查询仓库
     * 
     * @param id 仓库主键
     * @return 仓库
     */
    public WarehouseDto selectWarehouseById(Long id);
    /**
     * 查询仓库--通过部门id
     *
     * @param deptId 仓库主键
     * @return 仓库
     */
    public WarehouseDto getWarehouseById(Long deptId);

    /**
     * 查询仓库列表
     * 
     * @param warehouse 仓库
     * @return 仓库集合
     */
    public List<WarehouseDto> selectWarehouseList(Warehouse warehouse);
    /**
     * 查询仓库列表---权限+去重
     *
     * @param warehouse 仓库
     * @return 仓库集合
     */
    public List<WarehouseDto> getWarehouseList(Warehouse warehouse);

    /**
     * 查询全部仓库
     * @param warehouse
     * @return
     */
    public List<WarehouseDto> getAllWarehouseList(Warehouse warehouse);

    /**
     * 新增仓库
     * 
     * @param warehouse 仓库
     * @return 结果
     */
    public AjaxResult insertWarehouse(Warehouse warehouse);

    /**
     * 修改仓库
     * 
     * @param warehouse 仓库
     * @return 结果
     */
    public AjaxResult updateWarehouse(Warehouse warehouse);

    /**
     * 批量删除仓库
     * 
     * @param ids 需要删除的仓库主键集合
     * @return 结果
     */
    public int deleteWarehouseByIds(Long[] ids);

    /**
     * 删除仓库信息
     * 
     * @param id 仓库主键
     * @return 结果
     */
    public AjaxResult deleteWarehouseById(Long id);
}
