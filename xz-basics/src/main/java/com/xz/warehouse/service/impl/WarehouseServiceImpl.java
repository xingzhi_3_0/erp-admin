package com.xz.warehouse.service.impl;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xz.common.annotation.DataScope;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.utils.SecurityUtils;
import com.xz.purchase.domain.Purchase;
import com.xz.purchase.service.IPurchaseService;
import com.xz.supplier.domain.Supplier;
import com.xz.warehouse.dto.WarehouseDto;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.warehouse.mapper.WarehouseMapper;
import com.xz.warehouse.domain.Warehouse;
import com.xz.warehouse.service.IWarehouseService;
import org.springframework.util.CollectionUtils;

/**
 * 仓库Service业务层处理
 *
 * @author xz
 * @date 2024-01-04
 */
@Service
public class WarehouseServiceImpl extends ServiceImpl<WarehouseMapper, Warehouse> implements IWarehouseService {
    @Autowired
    private WarehouseMapper warehouseMapper;
    @Autowired
    private IPurchaseService iPurchaseService;

    /**
     * 查询仓库
     *
     * @param id 仓库主键
     * @return 仓库
     */
    @Override
    public WarehouseDto selectWarehouseById(Long id) {
        return warehouseMapper.selectWarehouseById(id);
    }

    /**
     * 查询仓库--通过部门id
     * @param deptId 仓库主键
     * @return
     */
    @Override
    public WarehouseDto getWarehouseById(Long deptId) {
        return warehouseMapper.getWarehouseById(deptId);
    }

    /**
     * 查询仓库列表
     *
     * @param warehouse 仓库
     * @return 仓库
     */
    @Override
    public List<WarehouseDto> selectWarehouseList(Warehouse warehouse) {
        return warehouseMapper.selectWarehouseList(warehouse);
    }
    /**
     * 查询仓库列表--权限仓库+共用仓库
     *
     * @param warehouse 仓库
     * @return 仓库
     */

    @Override
    public List<WarehouseDto> getWarehouseList(Warehouse warehouse) {
        //权限仓库
        List<WarehouseDto> warehouseDtoList = warehouseMapper.selectWarehouseList(warehouse);
        if(Objects.nonNull(warehouse.getQueryMyself()) && warehouse.getQueryMyself().equals(1)){
            return warehouseDtoList;
        }
        if(!CollectionUtils.isEmpty(warehouseDtoList)){
            //共用仓库
            List<WarehouseDto> shareWarehouseList = warehouseMapper.getShareWarehouseList();
            if(!CollectionUtils.isEmpty(shareWarehouseList)){
                warehouseDtoList.addAll(shareWarehouseList);
                warehouseDtoList=warehouseDtoList.stream().distinct().collect(Collectors.toList());
            }
        }


        return warehouseDtoList;
    }

    /**
     * 查询全部仓库
     * @param warehouse
     * @return
     */
    @Override
    public List<WarehouseDto> getAllWarehouseList(Warehouse warehouse) {
        return warehouseMapper.getAllWarehouseList(warehouse);
    }

    /**
     * 新增仓库
     *
     * @param warehouse 仓库
     * @return 结果
     */
    @Override
    public AjaxResult insertWarehouse(Warehouse warehouse) {
        //校验是否已经添加
        int count = this.count(new QueryWrapper<Warehouse>().eq("warehouse_name", warehouse.getWarehouseName()).eq("del_flag", 0));
        if(count>0){
            return AjaxResult.error("数据【"+warehouse.getWarehouseName()+"】已存在");
        }
        warehouse.setCreateTime(DateUtils.getNowDate());
        warehouse.setCreateBy(SecurityUtils.getUserId());
        warehouse.setTenantId(SecurityUtils.getLoginUser().getTenantId());
        int insertWarehouse = warehouseMapper.insertWarehouse(warehouse);
        return insertWarehouse>0? AjaxResult.success("新增成功"):AjaxResult.error("新增失败");
    }

    /**
     * 修改仓库
     *
     * @param warehouse 仓库
     * @return 结果
     */
    @Override
    public AjaxResult updateWarehouse(Warehouse warehouse) {
        if(warehouse.getId()==null){
            return AjaxResult.error("参数id不能为空");
        }
        //校验是否已经添加
        int count = this.count(new QueryWrapper<Warehouse>().eq("warehouse_name", warehouse.getWarehouseName()).eq("del_flag", 0).ne("id",warehouse.getId()));

        if(count>0){
            return AjaxResult.error("数据【"+warehouse.getWarehouseName()+"】已存在");
        }
        warehouse.setUpdateTime(DateUtils.getNowDate());
        warehouse.setUpdateBy(SecurityUtils.getUserId());
        int updateWarehouse = warehouseMapper.updateWarehouse(warehouse);
        return updateWarehouse>0? AjaxResult.success("修改成功"):AjaxResult.error("修改失败");
    }

    /**
     * 批量删除仓库
     *
     * @param ids 需要删除的仓库主键
     * @return 结果
     */
    @Override
    public int deleteWarehouseByIds(Long[] ids) {
        return warehouseMapper.deleteWarehouseByIds(ids);
    }

    /**
     * 删除仓库信息
     *
     * @param id 仓库主键
     * @return 结果
     */
    @Override
    public AjaxResult deleteWarehouseById(Long id) {
        //校验仓库是否存在
        int count = iPurchaseService.count(new QueryWrapper<Purchase>().eq("warehouse_id", id));
        if(count>0){
            return AjaxResult.error("当前仓库有库存商品，不能进行删除");
        }
        int deleted = warehouseMapper.deleteWarehouseById(id);
        if(deleted>0){
            return AjaxResult.success("删除成功");
        }
        return AjaxResult.error("删除识别");
    }
}
