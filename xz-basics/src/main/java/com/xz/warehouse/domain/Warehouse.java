package com.xz.warehouse.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 仓库对象 t_warehouse
 * 
 * @author xz
 * @date 2024-01-04
 */
@Data
@TableName("t_warehouse")
public class Warehouse extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 仓库名称 */
    @NotBlank(message = "仓库名称不能为空")
    @Excel(name = "仓库名称")
    private String warehouseName;

    /** 归属部门id */
    @NotNull(message = "归属部门不能为空")
    @Excel(name = "归属部门id")
    private Long deptId;

    /** 归属部门 */
    @Excel(name = "归属部门")
    private String deptName;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;
    /**
     * 是否共用（1.是 2.否）
     */
    private Integer isShare;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;
    /**
     * 状态
     */
    @TableField(exist = false)
    public List<Integer> statusList;

    @TableField(exist = false)
    private Integer queryMyself;
}
