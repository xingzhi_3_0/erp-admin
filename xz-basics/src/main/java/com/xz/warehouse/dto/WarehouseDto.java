package com.xz.warehouse.dto;

import com.xz.common.annotation.Excel;
import lombok.Data;

/**
 * @ClassName WarehouseDto * @Description TODO
 * @Author Administrator
 * @Date 16:59 2024/1/4
 * @Version 1.0
 **/
@Data
public class WarehouseDto {
    /** id */
    private Long id;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String warehouseName;

    /** 归属部门id */
    @Excel(name = "归属部门id")
    private Long deptId;

    /** 归属部门 */
    @Excel(name = "归属部门")
    private String deptName;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;
    /**
     * 是否共用（1.是 2.否）
     */
    private Integer isShare;
}
