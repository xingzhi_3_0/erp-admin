package com.xz.warehouse.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.common.annotation.DataScope;
import com.xz.warehouse.dto.WarehouseDto;
import org.apache.ibatis.annotations.Mapper;
import com.xz.warehouse.domain.Warehouse;

/**
 * 仓库Mapper接口
 * 
 * @author xz
 * @date 2024-01-04
 */
@Mapper
public interface WarehouseMapper  extends BaseMapper<Warehouse> {
    /**
     * 查询仓库
     * 
     * @param id 仓库主键
     * @return 仓库
     */
    public WarehouseDto selectWarehouseById(Long id);
    /**
     * 查询仓库--通过部门id
     *
     * @param deptId 仓库主键
     * @return 仓库
     */
    public WarehouseDto getWarehouseById(Long deptId);

    /**
     * 查询仓库列表
     * 
     * @param warehouse 仓库
     * @return 仓库集合
     */
    @DataScope(deptAlias = "d")
    public List<WarehouseDto> selectWarehouseList(Warehouse warehouse);


    public List<WarehouseDto> selectDeptList(Long deptId);

    /**
     * 查询全部仓库
     * @param warehouse
     * @return
     */
    public List<WarehouseDto> getAllWarehouseList(Warehouse warehouse);

    /**
     * 共用仓库
     * @return
     */
    public List<WarehouseDto> getShareWarehouseList();

    /**
     * 新增仓库
     * 
     * @param warehouse 仓库
     * @return 结果
     */
    public int insertWarehouse(Warehouse warehouse);

    /**
     * 修改仓库
     * 
     * @param warehouse 仓库
     * @return 结果
     */
    public int updateWarehouse(Warehouse warehouse);

    /**
     * 删除仓库
     * 
     * @param id 仓库主键
     * @return 结果
     */
    public int deleteWarehouseById(Long id);

    /**
     * 批量删除仓库
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWarehouseByIds(Long[] ids);
}
