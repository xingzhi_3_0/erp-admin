package com.xz.expense.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.annotation.DataScope;
import com.xz.expense.domain.ExpenseBill;

/**
 * 费用账单信息Service接口
 *
 * @author xz
 * @date 2024-02-25
 */
public interface IExpenseBillService extends IService<ExpenseBill> {
    /**
     * 查询费用账单信息
     *
     * @param id 费用账单信息主键
     * @return 费用账单信息
     */
    public ExpenseBill selectExpenseBillById(Long id);

    /**
     * 查询费用账单信息列表
     *
     * @param expenseBill 费用账单信息
     * @return 费用账单信息集合
     */
    public List<ExpenseBill> selectExpenseBillList(ExpenseBill expenseBill);

    /**
     * 查询费用账单信息列表（忽略作用域）
     *
     * @param expenseBill 费用账单信息
     * @return 费用账单信息集合
     */
    List<ExpenseBill> selectExpenseBillListScopeIgnore(ExpenseBill expenseBill);

    /**
     * 新增费用账单信息
     *
     * @param expenseBill 费用账单信息
     * @return 结果
     */
    public int insertExpenseBill(ExpenseBill expenseBill);

    /**
     * 修改费用账单信息
     *
     * @param expenseBill 费用账单信息
     * @return 结果
     */
    public int updateExpenseBill(ExpenseBill expenseBill);

    /**
     * 批量删除费用账单信息
     *
     * @param ids 需要删除的费用账单信息主键集合
     * @return 结果
     */
    public int deleteExpenseBillByIds(Long[] ids);

    /**
     * 删除费用账单信息信息
     *
     * @param id 费用账单信息主键
     * @return 结果
     */
    public int deleteExpenseBillById(Long id);
}
