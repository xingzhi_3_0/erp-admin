package com.xz.expense.service.impl;

import java.util.List;

import com.xz.common.annotation.DataScope;
import com.xz.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.expense.mapper.ExpenseBillMapper;
import com.xz.expense.domain.ExpenseBill;
import com.xz.expense.service.IExpenseBillService;

/**
 * 费用账单信息Service业务层处理
 *
 * @author xz
 * @date 2024-02-25
 */
@Service
public class ExpenseBillServiceImpl  extends ServiceImpl<ExpenseBillMapper, ExpenseBill> implements IExpenseBillService
{
    @Autowired
    private ExpenseBillMapper expenseBillMapper;

    /**
     * 查询费用账单信息
     *
     * @param id 费用账单信息主键
     * @return 费用账单信息
     */
    @Override
    public ExpenseBill selectExpenseBillById(Long id)
    {
        return expenseBillMapper.selectExpenseBillById(id);
    }

    /**
     * 查询费用账单信息列表
     *
     * @param expenseBill 费用账单信息
     * @return 费用账单信息
     */
    @DataScope(deptAlias = "d", userAlias = "u")
    @Override
    public List<ExpenseBill> selectExpenseBillList(ExpenseBill expenseBill)
    {
        return expenseBillMapper.selectExpenseBillList(expenseBill);
    }

    /**
     * 查询费用账单信息列表
     *
     * @param expenseBill 费用账单信息
     * @return 费用账单信息
     */
    @Override
    public List<ExpenseBill> selectExpenseBillListScopeIgnore(ExpenseBill expenseBill)
    {
      return expenseBillMapper.selectExpenseBillList(expenseBill);
    }


    /**
     * 新增费用账单信息
     *
     * @param expenseBill 费用账单信息
     * @return 结果
     */
    @Override
    public int insertExpenseBill(ExpenseBill expenseBill)
    {
        expenseBill.setCreateTime(DateUtils.getNowDate());
        return expenseBillMapper.insertExpenseBill(expenseBill);
    }

    /**
     * 修改费用账单信息
     *
     * @param expenseBill 费用账单信息
     * @return 结果
     */
    @Override
    public int updateExpenseBill(ExpenseBill expenseBill)
    {
        expenseBill.setUpdateTime(DateUtils.getNowDate());
        return expenseBillMapper.updateExpenseBill(expenseBill);
    }

    /**
     * 批量删除费用账单信息
     *
     * @param ids 需要删除的费用账单信息主键
     * @return 结果
     */
    @Override
    public int deleteExpenseBillByIds(Long[] ids)
    {
        return expenseBillMapper.deleteExpenseBillByIds(ids);
    }

    /**
     * 删除费用账单信息信息
     *
     * @param id 费用账单信息主键
     * @return 结果
     */
    @Override
    public int deleteExpenseBillById(Long id)
    {
        return expenseBillMapper.deleteExpenseBillById(id);
    }
}
