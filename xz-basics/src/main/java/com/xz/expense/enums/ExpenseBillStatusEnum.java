package com.xz.expense.enums;

/**
 * 到店状态
 */
public enum ExpenseBillStatusEnum {

  /** 待确认 **/
  TO_BE_CONFIRMED(0, "待确认"),
  /** 已确认 **/
  CONFIRMED(1, "已确认"),
  ;

  private final Integer status;

  private final String name;

  ExpenseBillStatusEnum(Integer status, String name) {
    this.status = status;
    this.name = name;
  }

  public Integer getStatus() {
    return status;
  }

  public String getName() {
    return name;
  }

  public boolean eq(Integer status) {
    if(null != status && status.equals(getStatus())){
      return true;
    }
    return false;
  }
}
