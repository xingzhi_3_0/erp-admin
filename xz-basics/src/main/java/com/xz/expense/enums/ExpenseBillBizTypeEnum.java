package com.xz.expense.enums;

/**
 * 到店状态
 */
public enum ExpenseBillBizTypeEnum {

  /** 销售单 **/
  SALES_ORDER(1, "销售单"),
  /** 销售换货单 **/
  SALES_EXCHANGE_ORDER(2, "销售换货单"),
  /** 销售退货单 **/
  SALES_RETURN_ORDER(3, "销售退货单"),
  /** 诊疗单 **/
  TREATMENT_ORDER(4, "诊疗单"),
  /** 挂号单 **/
  REGISTRATION_ORDER(5, "挂号单"),
  /** 挂号检查退费 **/
  TREATMENT_RETURN_ORDER(6, "挂号检查退费"),
  ;

  private final Integer type;

  private final String name;

  ExpenseBillBizTypeEnum(Integer status, String name) {
    this.type = status;
    this.name = name;
  }

  public Integer getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public boolean eq(Integer type) {
    if(null != type && type.equals(getType())){
      return true;
    }
    return false;
  }
}
