package com.xz.expense.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 费用账单信息对象 t_expense_bill
 *
 * @author xz
 * @date 2024-02-25
 */
@Data
@TableName("t_expense_bill")
public class ExpenseBill extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 业务单号 */
    @Excel(name = "业务单号")
    private String bizNo;

    private Long bizId;

    /** 1-销售单 2-销售换货单 3-销售退货单 4-诊疗单 5- 挂号单*/
    @Excel(name = "1-销售单 2-销售换货单 3-销售退货单 4-诊疗单 5-挂号单 6-挂号检查退费单")
    private Integer bizType;

    /** 0-待确认 1-已确认*/
    private Integer status;

    /** 患者id */
    @Excel(name = "患者id")
    private Long patientId;

    /** 应收金额 */
    @Excel(name = "应收金额")
    private BigDecimal receivableCost;

    /** 优惠金额 */
    @Excel(name = "优惠金额")
    private BigDecimal preferentialCost;

    /** 实际费用 */
    @Excel(name = "实际费用")
    private BigDecimal actCost;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    @NotNull(message = "租户id不能为空")
    private Long tenantId;

    /** 部门id(门店id) */
    @Excel(name = "部门id(门店id)")
    @NotNull(message = "部门id(门店id)不能为空")
    private Long deptId;

    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;
    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
    @TableField(exist = false)
    private Integer sex;
    @TableField(exist = false)
    private String patientName;
    @TableField(exist = false)
    private Integer currAge;
    @TableField(exist = false)
    private String patientPhone;
    @TableField(exist = false)
    private String salesDeptName;
    @TableField(exist = false)
    private String searchPatient;
    @TableField(exist = false)
    private String userName;
    @TableField(exist = false)
    private Integer queryStatus;
}
