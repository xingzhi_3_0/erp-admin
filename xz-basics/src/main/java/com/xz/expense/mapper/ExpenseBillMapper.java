package com.xz.expense.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.expense.domain.ExpenseBill;
import org.apache.ibatis.annotations.Param;

/**
 * 费用账单信息Mapper接口
 * 
 * @author xz
 * @date 2024-02-25
 */
@Mapper
public interface ExpenseBillMapper  extends BaseMapper<ExpenseBill> {
    /**
     * 查询费用账单信息
     * 
     * @param id 费用账单信息主键
     * @return 费用账单信息
     */
    public ExpenseBill selectExpenseBillById(Long id);

    /**
     * 查询费用账单信息列表
     * 
     * @param expenseBill 费用账单信息
     * @return 费用账单信息集合
     */
    public List<ExpenseBill> selectExpenseBillList(ExpenseBill expenseBill);

    /**
     * 新增费用账单信息
     * 
     * @param expenseBill 费用账单信息
     * @return 结果
     */
    public int insertExpenseBill(ExpenseBill expenseBill);

    /**
     * 修改费用账单信息
     * 
     * @param expenseBill 费用账单信息
     * @return 结果
     */
    public int updateExpenseBill(ExpenseBill expenseBill);

    /**
     * 删除费用账单信息
     * 
     * @param id 费用账单信息主键
     * @return 结果
     */
    public int deleteExpenseBillById(Long id);

    /**
     * 批量删除费用账单信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExpenseBillByIds(Long[] ids);

    int deleteByParam(ExpenseBill expenseBill);

    int updateByParam(ExpenseBill expenseBill);

    ExpenseBill queryByBizId(@Param("bizId") Long bizId, @Param("bizType")Integer bizType);
}
