package com.xz.apply.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.apply.vo.ConfirmPurchaseApplyVo;
import com.xz.apply.vo.PurchaseApplyInfoVo;
import com.xz.apply.vo.PurchaseApplyVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.apply.domain.PurchaseApply;

/**
 * 采购申请Mapper接口
 * 
 * @author xz
 * @date 2024-01-19
 */
@Mapper
public interface PurchaseApplyMapper  extends BaseMapper<PurchaseApply> {
    /**
     * 查询采购申请
     * 
     * @param id 采购申请主键
     * @return 采购申请
     */
    public PurchaseApplyInfoVo selectPurchaseApplyById(Long id);

    /**
     * 查询采购申请列表
     * 
     * @param purchaseApply 采购申请
     * @return 采购申请集合
     */
    public List<PurchaseApplyVo> selectPurchaseApplyList(PurchaseApply purchaseApply);

    /**
     * 确定采购申请列表
     * @param purchaseApply
     * @return
     */
    public List<ConfirmPurchaseApplyVo> confirmPurchaseApplyList(PurchaseApply purchaseApply);

    /**
     * 新增采购申请
     * 
     * @param purchaseApply 采购申请
     * @return 结果
     */
    public int insertPurchaseApply(PurchaseApply purchaseApply);

    /**
     * 修改采购申请
     * 
     * @param purchaseApply 采购申请
     * @return 结果
     */
    public int updatePurchaseApply(PurchaseApply purchaseApply);

    /**
     * 删除采购申请
     * 
     * @param id 采购申请主键
     * @return 结果
     */
    public int deletePurchaseApplyById(Long id);

    /**
     * 批量删除采购申请
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePurchaseApplyByIds(Long[] ids);

    /**
     * 采购申请转订单列表
     * @param idList
     * @return
     */
    List<ConfirmPurchaseApplyVo> reorderPurchaseApplyList(List<Long> idList);

    /*采购申请详情（采购申请单号）*/
    PurchaseApply getPurchaseApply(String applyNumber);
}
