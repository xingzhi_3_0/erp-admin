package com.xz.apply.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.apply.domain.PurchaseProductApply;
import org.apache.ibatis.annotations.Param;

/**
 * 采购申请商品明细Mapper接口
 * 
 * @author xz
 * @date 2024-01-19
 */
@Mapper
public interface PurchaseProductApplyMapper  extends BaseMapper<PurchaseProductApply> {
    /**
     * 查询采购申请商品明细
     * 
     * @param id 采购申请商品明细主键
     * @return 采购申请商品明细
     */
    public PurchaseProductApply selectPurchaseProductApplyById(Long id);


    public PurchaseProductApply selectSalesProduct(@Param("salesId") Long salesId,@Param("detailId")  Long detailId);

    /**
     * 查询采购申请商品明细列表
     * 
     * @param purchaseProductApply 采购申请商品明细
     * @return 采购申请商品明细集合
     */
    public List<PurchaseProductApply> selectPurchaseProductApplyList(PurchaseProductApply purchaseProductApply);

    /**
     * 新增采购申请商品明细
     * 
     * @param purchaseProductApply 采购申请商品明细
     * @return 结果
     */
    public int insertPurchaseProductApply(PurchaseProductApply purchaseProductApply);

    /**
     * 修改采购申请商品明细
     * 
     * @param purchaseProductApply 采购申请商品明细
     * @return 结果
     */
    public int updatePurchaseProductApply(PurchaseProductApply purchaseProductApply);

    /**
     * 删除采购申请商品明细
     * 
     * @param id 采购申请商品明细主键
     * @return 结果
     */
    public int deletePurchaseProductApplyById(Long id);
    /*更新商品明细*/
    public int updatePurchaseProductStatus(Long id);

    /**
     * 批量删除采购申请商品明细
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePurchaseProductApplyByIds(Long[] ids);
}
