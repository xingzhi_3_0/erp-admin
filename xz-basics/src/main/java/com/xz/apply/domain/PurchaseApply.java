package com.xz.apply.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

import java.util.Date;
import java.util.List;

/**
 * 采购申请对象 t_purchase_apply
 *
 * @author xz
 * @date 2024-01-19
 */
@Data
@TableName("t_purchase_apply")
public class PurchaseApply extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 仓库id */
    @Excel(name = "仓库id")
    private Long warehouseId;

    /** 仓库 */
    @Excel(name = "仓库")
    private String warehouseName;

    /** 状态（1.待提交 2.已提交 3.已作废） */
    @NotNull(message = "提交方式不能为空")
    @Excel(name = "状态", readConverterExp = "1=.待提交,2=.已提交,3=.已作废")
    private Integer status;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    /** 需求单号 */
    @Excel(name = "需求单号")
    private String applyNumber;

    /** 采购状态（1.采购未提单 2.采购已提单 3.采购部分提单） */
    @Excel(name = "采购状态", readConverterExp = "1=.未提单,2=.已提单,3=.部分提单")
    private Integer purchaseStatus;
    /** 采购申请数量 */
    @NotNull(message = "采购申请数量不能为空")
    @Excel(name = "采购申请数量")
    private Integer applyNum;

    /**
     * 状态
     */
    @TableField(exist = false)
    public List<Integer> statusList;
    /**
     * 采购状态
     */
    @TableField(exist = false)
    public List<Integer> purchaseStatusList;
    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;
    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
    /** 产品名称 */
    @TableField(exist = false)
    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;
    /** 商品参数 */
    @TableField(exist = false)
    @Excel(name = "商品参数")
    private String productParam;
    /**
     * 采购申请类型（1.门店申请 2.加工单采购申请 3.销售单采购申请）
     */
    private Integer productApplyType;
    /**
     * 业务单id
     */
    private Long bizId;
    /**
     * 供应商id
     */
    @TableField(exist = false)
    private Long supplierId;
    /**
     * 业务单号
     */
    private String bizNo;
    /**
     *采购申请商品明细
     */
    @TableField(exist = false)
    @NotNull(message = "采购申请商品不能为空")
    private List<PurchaseProductApply> productApplyList;
    /** 客户信息 */
    @TableField(exist = false)
    private String patientName;
    @TableField(exist = false)
    private Long patientId;
}
