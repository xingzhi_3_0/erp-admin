package com.xz.apply.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @ClassName PurchaseApplyVo * @Description TODO
 * @Author Administrator
 * @Date 17:08 2024/1/19
 * @Version 1.0  采购
 **/
@Data
public class PurchaseApplyVo {
    /** id */
    private Long id;
    /** 状态（1.待提交 2.已提交 3.已作废） */
    @Excel(name = "状态", readConverterExp = "1=.待提交,2=.已提交,3=.已作废")
    private Integer status;
    /** 采购状态（1=.未提单,2=.已提单,3=.部分提单） */
    @Excel(name = "采购状态", readConverterExp = "1=.未提单,2=.已提单,3=.部分提单")
    private Integer purchaseStatus;
    /** 采购申请数量 */
    @Excel(name = "采购申请数量")
    private Integer applyNum;
    /** 需求单号 */
    @Excel(name = "需求单号")
    private String applyNumber;
    /** 制单人 */
    @Excel(name = "制单人")
    private String userName;
    /** 制单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 申请部门 */
    @Excel(name = "申请部门")
    private String deptName;

}
