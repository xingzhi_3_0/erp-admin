package com.xz.apply.vo;

import lombok.Data;

import java.util.List;

/**
 * @ClassName PurchaseApplyInfoVo * @Description TODO
 * @Author Administrator
 * @Date 17:30 2024/1/19
 * @Version 1.0 采购申请详情
 **/
@Data
public class PurchaseApplyInfoVo extends PurchaseApplyVo{
    /** 备注 */
    private String remark;
    /**
     * 部门id
     */
    private Long deptId;
    /**
     *采购申请类型（1.门店申请 2.加工单采购申请 3.销售单采购申请）
     */
    private Integer productApplyType;
    /**
     * 采购申请商品明细
     */
    private List<PurchaseProductApplyVo> productApplyVoList;
}
