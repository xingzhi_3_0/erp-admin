package com.xz.apply.vo;

import com.xz.common.annotation.Excel;
import com.xz.purchase.domain.PurchaseProductParam;
import lombok.Data;

import java.util.List;

/**
 * @ClassName PurchaseProductApplyVo * @Description TODO
 * @Author Administrator
 * @Date 17:31 2024/1/19
 * @Version 1.0 采购商品明细
 **/
@Data
public class PurchaseProductApplyVo {

    /** 采购申请数量 */
    @Excel(name = "采购申请数量")
    private Integer applyNum;
    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;

    /** 商品id */
    @Excel(name = "商品id")
    private Long productId;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private String productEncoded;

    /** 状态（1.未提单 2.已提单） */
    @Excel(name = "状态", readConverterExp = "1.未提单 2.已提单")
    private Integer ppaStatus;
    /** 备注 */
    private String ppaRemark;
    /**参数*/
    private String productParam;
    /**
     * 实际采购数量
     */
    private Integer purchaseNum;
    /*仓库*/
    private String purchaseWarehouseName;
    /*采购单号*/
    private String purchaseNo;

    /** 单位 */
    @Excel(name = "单位")
    private String unit;

    /**
     * 采购商品属性
     */
    private List<PurchaseProductParam> purchaseProductParamList;
}
