package com.xz.apply.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName ConfirmPurchaseApplyVo * @Description TODO
 * @Author Administrator
 * @Date 11:52 2024/1/20
 * @Version 1.0 门店采购申请确认
 **/
@Data
public class ConfirmPurchaseApplyVo {
    /** 采购申请商品id */
    private Long id;
    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;

    /** 商品id */
    @Excel(name = "商品id")
    private Long productId;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private String productEncoded;
    /** 仓库id */
    @Excel(name = "仓库id")
    private Long warehouseId;

    /** 商品参数 */
    @Excel(name = "商品参数")
    private String productParam;

    /** 仓库 */
    @Excel(name = "仓库")
    private String warehouseName;
    /** 需求单号 */
    @Excel(name = "需求单号")
    private String applyNumber;
    /** 制单人 */
    @Excel(name = "制单人")
    private String userName;
    /** 制单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 申请部门 */
    @Excel(name = "申请部门")
    private String deptName;
    /**仓库地址*/
    private String address;
    /**
     * 部门id
     */
    private Long deptId;
    /** 采购申请数量 */
    @Excel(name = "采购申请数量")
    private Integer applyNum;

    /** 单位 */
    @Excel(name = "单位")
    private String unit;

    /** 供应商id */
    private Long supplierId;
    /** 供应商 */
    private String supplierName;
    /**
     * 实际采购单号
     */
    private String purchaseNo;
    /**
     * 实际采购数量
     */
    private Integer purchaseNum;
    /*销售单号*/
    private String bizNo;
    /*附件*/
    private String productUrl;
    private String productUrlName;
    /** 客户信息 */
    @Excel(name = "客户信息")
    private String patientName;
    private Long patientId;
    private String remark;
}
