package com.xz.apply.service.impl;

import java.util.List;

import com.xz.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.apply.mapper.PurchaseProductApplyMapper;
import com.xz.apply.domain.PurchaseProductApply;
import com.xz.apply.service.IPurchaseProductApplyService;

/**
 * 采购申请商品明细Service业务层处理
 *
 * @author xz
 * @date 2024-01-19
 */
@Service
public class PurchaseProductApplyServiceImpl extends ServiceImpl<PurchaseProductApplyMapper, PurchaseProductApply> implements IPurchaseProductApplyService {
    @Autowired
    private PurchaseProductApplyMapper purchaseProductApplyMapper;

    /**
     * 查询采购申请商品明细
     *
     * @param id 采购申请商品明细主键
     * @return 采购申请商品明细
     */
    @Override
    public PurchaseProductApply selectPurchaseProductApplyById(Long id) {
        return purchaseProductApplyMapper.selectPurchaseProductApplyById(id);
    }

    /**
     * 查询采购申请商品明细列表
     *
     * @param purchaseProductApply 采购申请商品明细
     * @return 采购申请商品明细
     */
    @Override
    public List<PurchaseProductApply> selectPurchaseProductApplyList(PurchaseProductApply purchaseProductApply) {
        return purchaseProductApplyMapper.selectPurchaseProductApplyList(purchaseProductApply);
    }

    /**
     * 新增采购申请商品明细
     *
     * @param purchaseProductApply 采购申请商品明细
     * @return 结果
     */
    @Override
    public int insertPurchaseProductApply(PurchaseProductApply purchaseProductApply) {
        purchaseProductApply.setCreateTime(DateUtils.getNowDate());
        return purchaseProductApplyMapper.insertPurchaseProductApply(purchaseProductApply);
    }

    /**
     * 修改采购申请商品明细
     *
     * @param purchaseProductApply 采购申请商品明细
     * @return 结果
     */
    @Override
    public int updatePurchaseProductApply(PurchaseProductApply purchaseProductApply) {
        purchaseProductApply.setUpdateTime(DateUtils.getNowDate());
        return purchaseProductApplyMapper.updatePurchaseProductApply(purchaseProductApply);
    }

    /**
     * 批量删除采购申请商品明细
     *
     * @param ids 需要删除的采购申请商品明细主键
     * @return 结果
     */
    @Override
    public int deletePurchaseProductApplyByIds(Long[] ids) {
        return purchaseProductApplyMapper.deletePurchaseProductApplyByIds(ids);
    }

    /**
     * 删除采购申请商品明细信息
     *
     * @param id 采购申请商品明细主键
     * @return 结果
     */
    @Override
    public int deletePurchaseProductApplyById(Long id) {
        return purchaseProductApplyMapper.deletePurchaseProductApplyById(id);
    }
    /*更新商品明细状态*/
    public int updatePurchaseProductStatus(Long id) {
        return purchaseProductApplyMapper.updatePurchaseProductStatus(id);
    }
}
