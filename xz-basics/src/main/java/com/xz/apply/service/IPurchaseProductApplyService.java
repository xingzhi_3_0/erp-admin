package com.xz.apply.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.apply.domain.PurchaseProductApply;

/**
 * 采购申请商品明细Service接口
 * 
 * @author xz
 * @date 2024-01-19
 */
public interface IPurchaseProductApplyService extends IService<PurchaseProductApply> {
    /**
     * 查询采购申请商品明细
     * 
     * @param id 采购申请商品明细主键
     * @return 采购申请商品明细
     */
    public PurchaseProductApply selectPurchaseProductApplyById(Long id);

    /**
     * 查询采购申请商品明细列表
     * 
     * @param purchaseProductApply 采购申请商品明细
     * @return 采购申请商品明细集合
     */
    public List<PurchaseProductApply> selectPurchaseProductApplyList(PurchaseProductApply purchaseProductApply);

    /**
     * 新增采购申请商品明细
     * 
     * @param purchaseProductApply 采购申请商品明细
     * @return 结果
     */
    public int insertPurchaseProductApply(PurchaseProductApply purchaseProductApply);

    /**
     * 修改采购申请商品明细
     * 
     * @param purchaseProductApply 采购申请商品明细
     * @return 结果
     */
    public int updatePurchaseProductApply(PurchaseProductApply purchaseProductApply);

    /**
     * 批量删除采购申请商品明细
     * 
     * @param ids 需要删除的采购申请商品明细主键集合
     * @return 结果
     */
    public int deletePurchaseProductApplyByIds(Long[] ids);

    /**
     * 删除采购申请商品明细信息
     * 
     * @param id 采购申请商品明细主键
     * @return 结果
     */
    public int deletePurchaseProductApplyById(Long id);
    public int updatePurchaseProductStatus(Long id);
}
