package com.xz.apply.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.apply.domain.PurchaseApply;
import com.xz.apply.vo.ConfirmPurchaseApplyVo;
import com.xz.apply.vo.PurchaseApplyInfoVo;
import com.xz.apply.vo.PurchaseApplyVo;
import com.xz.common.core.domain.AjaxResult;
import com.xz.diagnosis.vo.SalesPurchaseApplyVo;

/**
 * 采购申请Service接口
 * 
 * @author xz
 * @date 2024-01-19
 */
public interface IPurchaseApplyService extends IService<PurchaseApply> {
    /**
     * 查询采购申请
     * 
     * @param id 采购申请主键
     * @return 采购申请
     */
    public PurchaseApplyInfoVo selectPurchaseApplyById(Long id);

    /**
     * 查询采购申请列表
     * 
     * @param purchaseApply 采购申请
     * @return 采购申请集合
     */
    public List<PurchaseApplyVo> selectPurchaseApplyList(PurchaseApply purchaseApply);

    /**
     * 确定门店采购申请列表
     * @param purchaseApply
     * @return
     */
    public List<ConfirmPurchaseApplyVo> confirmPurchaseApplyList(PurchaseApply purchaseApply);

    /**
     * 新增采购申请
     * 
     * @param purchaseApply 采购申请
     * @return 结果
     */
    public AjaxResult insertPurchaseApply(PurchaseApply purchaseApply);


    public int addSalesPurchaseApply(SalesPurchaseApplyVo salesPurchaseApplyVo);
    /**
     * 修改采购申请
     * 
     * @param purchaseApply 采购申请
     * @return 结果
     */
    public AjaxResult updatePurchaseApply(PurchaseApply purchaseApply);

    /**
     * 批量删除采购申请
     * 
     * @param ids 需要删除的采购申请主键集合
     * @return 结果
     */
    public int deletePurchaseApplyByIds(Long[] ids);

    /**
     * 删除采购申请信息
     * 
     * @param id 采购申请主键
     * @return 结果
     */
    public int deletePurchaseApplyById(Long id);
    /**
     * 采购申请转订单列表
     * @param idList
     * @return
     */
    List<ConfirmPurchaseApplyVo> reorderPurchaseApplyList(List<Long> idList);


    /*采购申请详情（采购申请单号）*/
    PurchaseApply getPurchaseApply(String applyNumber);
}
