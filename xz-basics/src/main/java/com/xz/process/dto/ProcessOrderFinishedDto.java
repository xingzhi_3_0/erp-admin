package com.xz.process.dto;

import lombok.Data;

/**
 * 加工单对象 t_process_order
 * 
 * @author xz
 * @date 2024-02-04
 */
@Data
public class ProcessOrderFinishedDto{

    /** id */
    private Long id;

    /** 快递单号 */
    private String trackingNumber;
}
