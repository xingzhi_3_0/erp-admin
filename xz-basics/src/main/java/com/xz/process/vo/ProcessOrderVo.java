package com.xz.process.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName ProcessOrderVo * @Description TODO
 * @Author Administrator
 * @Date 10:48 2024/2/4
 * @Version 1.0 加工单
 **/
@Data
public class ProcessOrderVo {
    /** id */
    private Long id;
    /** 加工单号 */
    @Excel(name = "加工单号")
    private String processNo;
    /** 销售单号 */
    @Excel(name = "销售单号")
    private String salesNo;
    /** 销售门店 */
    @Excel(name = "销售门店")
    private String salesDeptName;
    /** 客户信息 */
    @Excel(name = "客户信息")
    private String patientName;
    /** 收费时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "收费时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date chargeTime;

    /** 客户是否自备镜框（1.是 2.否） */
    @Excel(name = "客户是否自备镜框", readConverterExp = "1=是,2=否")
    private Integer isProvideOneself;

    /** 是否收到自备镜框（1.是 2.否） */
    @Excel(name = "是否收到自备镜框", readConverterExp = "1=是,2=否")
    private Integer provideOneself;

    /** 是否短信通知 */
    @Excel(name = "是否短信通知", readConverterExp = "1=是,2=否")
    private Integer smsNotification;
    /** 加工时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "加工时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date processTime;
    /** 加工师id */
    @Excel(name = "加工师id")
    private Long processMasterId;
    /** 加工师 */
    @Excel(name = "加工师")
    private String processMaster;
    /** 质检师 */
    @Excel(name = "质检师")
    private String qualityInspector;
    /** 质检时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "质检时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date qualityInspectorTime;
    /** 质检意见 */
    @Excel(name = "质检意见")
    private String qualityInspectorNote;

    /** 预计交付方式(1.邮寄 2.门店自提) */
    @Excel(name = "预计交付方式(1.邮寄 2.门店自提)")
    private Integer deliveryMethod;

    /** 加工单状态（1.备货中 2.待领料 3.加工中 4.待质检 5.质检不合格 6.加工完成 7.已完成 8.取消加工） */
    @Excel(name = "加工单状态", readConverterExp = "1=.备货中,2=.待领料,3=.加工中,4=.待质检,5=.质检不合格,6=.加工完成,7=.已完成,8=.取消加工")
    private Integer processingStatus;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 收货地址 */
    private String harvestAddress;
    /** 客户自备镜框 */
    private String patientProvideOneself;
}
