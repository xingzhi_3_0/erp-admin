package com.xz.process.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName ProcessOrderItemVo * @Description TODO
 * @Author Administrator
 * @Date 19:05 2024/2/4
 * @Version 1.0  加工单明细
 **/
@Data
public class ProcessOrderItemVo {
    /** id */
    private Long id;
    /** 商品信息 */
    @Excel(name = "商品信息")
    private String productName;

    /** 商品参数 */
    @Excel(name = "商品参数")
    private String productParam;

    /** 仓库id */
    @Excel(name = "仓库id")
    private Long warehouseId;

    /** 仓库 */
    @Excel(name = "仓库")
    private String warehouseName;

    /** 数量 */
    @Excel(name = "数量")
    private Integer processNum;

    /** 物料状态（1.待签收 2.采购中 3.待领料 4.已领料 5.报损无库存 6.报损待领料 7.加工完成 8.取消加工 ） */
    @Excel(name = "物料状态", readConverterExp = "1=.待签收,2=.采购中,3=.待领料,4=.已领料,5=.报损无库存,6=.报损待领料,7=.加工完成,8=.取消加工")
    private Integer status;

    /** 物流单号 */
    @Excel(name = "物流单号")
    private String trackingNumber;

    /** 签收人 */
    @Excel(name = "签收人")
    private String signer;

    /** 签收时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "签收时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date signerTime;

    /** 签收备注 */
    @Excel(name = "签收备注")
    private String signerNote;
    /*备注*/
    private String itemRemark;


}
