package com.xz.process.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.optometry.vo.OptometryInfoVo;
import com.xz.process.domain.ProcessOrderItem;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @ClassName ProcessOrderInfoVo * @Description TODO
 * @Author Administrator
 * @Date 18:29 2024/2/4
 * @Version 1.0 加工单详情
 **/
@Data
public class ProcessOrderInfoVo extends ProcessOrderVo{
    /** 销售订单id */
    @Excel(name = "销售id")
    private Long salesId;
    /** 客户手机号 */
    @Excel(name = "客户手机号")
    private String patientPhone;
    /** 客户自备镜框 */
    @Excel(name = "客户自备镜框")
    private String patientProvideOneself;
    /** 镜框物料状态（1.未收到 2.已收到） */
    @Excel(name = "镜框物料状态", readConverterExp = "1=.未收到,2=.已收到")
    private Integer frameMaterialStatus;
    /** 最后通知客户时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "最后通知客户时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date notifyCustomerTime;
    /*备注*/
    private String remark;
    /*加工商品明细*/
    @TableField(exist = false)
    private List<ProcessOrderItemVo> processOrderItemList;

    /** 配送门店 */
    @Excel(name = "配送门店")
    private String distributionDeptName;

    /** 收件人 */
    @Excel(name = "收件人")
    private String recipients;

    /** 收件人联系方式 */
    @Excel(name = "收件人联系方式")
    private String recipientsPhone;

    /** 收获地址 */
    @Excel(name = "收获地址")
    private String harvestAddress;

    /** 快递单号 */
    @Excel(name = "快递单号")
    private String trackingNumber;

    /** 签收人 */
    @Excel(name = "签收人")
    private String signer;

    /** 签收时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "签收时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date signerTime;

    /** 签收备注 */
    @Excel(name = "签收备注")
    private String signerNote;
    /*配镜处方*/
    private OptometryInfoVo optometryInfoVo;
    /*会员等级*/
    private String memberLevel;
    /*手机号*/
    private String phoneNumber;
    /*销售单id*/
    private Long salesOrderId;

    /** 配送时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "配送时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deliveryTime;

}
