package com.xz.process.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @ClassName MaterialRequisitionProcessingVo * @Description TODO
 * @Author Administrator
 * @Date 19:48 2024/2/5
 * @Version 1.0 领料加工
 **/
@Data
public class MaterialRequisitionProcessingVo {

    /*加工单id*/
    @NotNull(message = "参数不能为空")
    private Long id;
    /*类型 1.全部 2.部分 */
    @NotNull(message = "类型不能为空")
    private Integer type;
    /*加工单物料id*/
    private Long itemId;
}
