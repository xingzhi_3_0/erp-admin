package com.xz.process.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName ProcessOrderVo * @Description TODO
 * @Author Administrator
 * @Date 10:48 2024/2/4
 * @Version 1.0 加工单
 **/
@Data
public class ProcessOrderExportVo {

    @Excel(name = "加工单号")
    private String processNo;
    @Excel(name = "销售单号")
    private String salesNo;
    @Excel(name = "销售门店")
    private String salesDeptName;
    @Excel(name = "客户信息")
    private String patientName;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "收费时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date chargeTime;
    /** 客户是否自备镜框（1.是 2.否） */
    @Excel(name = "客户是否自备镜框", readConverterExp = "1=是,2=否")
    private Integer isProvideOneself;
    @Excel(name = "是否收到自备镜框", readConverterExp = "1=是,2=否")
    private Integer provideOneself;
    @Excel(name = "是否短信通知", readConverterExp = "1=是,2=否")
    private Integer smsNotification;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "领料加工时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date processTime;
    @Excel(name = "加工师")
    private String processMaster;
    @Excel(name = "质检师")
    private String qualityInspector;
    @Excel(name = "质检意见")
    private String qualityInspectorNote;
    @Excel(name = "预计交付方式", readConverterExp = "1=邮寄,2=门店自提")
    private Integer deliveryMethod;
    @Excel(name = "加工单状态", readConverterExp = "1=备货中,2=待领料,3=加工中,4=待质检,5=质检不合格,6=加工完成,7=已完成,8=取消加工,9=已派送")
    private Integer processingStatus;
    @Excel(name = "商品信息")
    private String productName;
    @Excel(name = "商品参数")
    private String productParam;
    @Excel(name = "仓库")
    private String warehouseName;
    @Excel(name = "数量")
    private Integer processNum;
    @Excel(name = "物料状态", readConverterExp = "1=待签收,2=采购中,3=待领料,4=已领料,5=报损无库存,6=报损待领料,7=加工完成,8=取消加工")
    private Integer status;
    @Excel(name = "签收人")
    private String signer;
    @Excel(name = "签收备注")
    private String signerNote;
    @Excel(name = "物流单号")
    private String trackingNumber;
}
