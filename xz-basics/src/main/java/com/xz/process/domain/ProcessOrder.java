package com.xz.process.domain;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 加工单对象 t_process_order
 * 
 * @author xz
 * @date 2024-02-04
 */
@Data
@TableName("t_process_order")
public class ProcessOrder extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 客户id */
    @Excel(name = "客户id")
    private Long patientId;

    /** 客户信息 */
    @Excel(name = "客户信息")
    private String patientName;

    /** 加工单号 */
    @Excel(name = "加工单号")
    private String processNo;

    /** 销售单号 */
    @Excel(name = "销售单号")
    private String salesNo;

    /** 销售门店id */
    @Excel(name = "销售门店id")
    private Long salesDeptId;

    /** 销售门店 */
    @Excel(name = "销售门店")
    private String salesDeptName;

    /** 销售单id */
    @Excel(name = "销售单id")
    private Long salesId;

    /** 收费时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "收费时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date chargeTime;

    /** 客户是否自备镜框（1.是 2.否） */
    @Excel(name = "客户是否自备镜框", readConverterExp = "1=.是,2=.否")
    private Integer isProvideOneself;

    /** 是否收到自备镜框（1.是 2.否） */
    @Excel(name = "是否收到自备镜框", readConverterExp = "1=.是,2=.否")
    private Integer provideOneself;

    /** 是否短信通知 */
    @Excel(name = "是否短信通知")
    private Integer smsNotification;

    /** 加工时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "加工时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date processTime;

    /** 加工师id */
    @Excel(name = "加工师id")
    private Long processMasterId;

    /** 加工师 */
    @Excel(name = "加工师")
    private String processMaster;

    /** 质检师id */
    @Excel(name = "质检师id")
    private Long qualityInspectorId;

    /** 质检师 */
    @Excel(name = "质检师")
    private String qualityInspector;

    /** 质检时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "质检时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date qualityInspectorTime;

    /** 质检意见 */
    @Excel(name = "质检意见")
    private String qualityInspectorNote;

    /** 预计交付方式(1.邮寄 2.门店自提) */
    @Excel(name = "预计交付方式(2.邮寄 1.门店自提)")
    private Integer deliveryMethod;

    /** 加工单状态（1.备货中 2.待领料 3.加工中 4.待质检 5.质检不合格 6.加工完成 7.已完成 8.取消加工 9.已配送） */
    @Excel(name = "加工单状态", readConverterExp = "1=.备货中,2=.待领料,3=.加工中,4=.待质检,5=.质检不合格,6=.加工完成,7=.已完成,8=.取消加工")
    private Integer processingStatus;

    /** 客户手机号 */
    @Excel(name = "客户手机号")
    private String patientPhone;

    /** 客户自备镜框 */
    @Excel(name = "客户自备镜框")
    private String patientProvideOneself;

    /** 镜框物料状态（1.未收到 2.已收到） */
    @Excel(name = "镜框物料状态", readConverterExp = "1=.未收到,2=.已收到")
    private Integer frameMaterialStatus;

    /** 最后通知客户时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "最后通知客户时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date notifyCustomerTime;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;
    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

    /*加工商品明细*/
    @TableField(exist = false)
    private List<ProcessOrderItem> processOrderItemList;

    /** 配送门店 */
    @Excel(name = "配送门店")
    private String distributionDeptName;

    /** 收件人 */
    @Excel(name = "收件人")
    private String recipients;

    /** 收件人联系方式 */
    @Excel(name = "收件人联系方式")
    private String recipientsPhone;

    /** 收货地址 */
    @Excel(name = "收货地址")
    private String harvestAddress;

    /** 快递单号 */
    @Excel(name = "快递单号")
    private String trackingNumber;

    /** 签收人 */
    @Excel(name = "签收人")
    private String signer;

    /** 签收时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "签收时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date signerTime;

    /** 签收备注 */
    @Excel(name = "签收备注")
    private String signerNote;
    /**
     * 状态
     */
    @TableField(exist = false)
    public List<Integer> statusList;
    /*销售单id*/
    private Long salesOrderId;

    private Integer oldProcessingStatus;

    /** 配送时间（交付时间） */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "配送时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deliveryTime;
}
