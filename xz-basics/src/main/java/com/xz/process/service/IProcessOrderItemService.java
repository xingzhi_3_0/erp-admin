package com.xz.process.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.process.domain.ProcessOrderItem;

/**
 * 加工单明细Service接口
 * 
 * @author xz
 * @date 2024-02-04
 */
public interface IProcessOrderItemService extends IService<ProcessOrderItem> {
    /**
     * 查询加工单明细
     * 
     * @param id 加工单明细主键
     * @return 加工单明细
     */
    public ProcessOrderItem selectProcessOrderItemById(Long id);

    /**
     * 查询加工单明细列表
     * 
     * @param processOrderItem 加工单明细
     * @return 加工单明细集合
     */
    public List<ProcessOrderItem> selectProcessOrderItemList(ProcessOrderItem processOrderItem);

    /**
     * 新增加工单明细
     * 
     * @param processOrderItem 加工单明细
     * @return 结果
     */
    public int insertProcessOrderItem(ProcessOrderItem processOrderItem);

    /**
     * 修改加工单明细
     * 
     * @param processOrderItem 加工单明细
     * @return 结果
     */
    public int updateProcessOrderItem(ProcessOrderItem processOrderItem);

    /**
     * 批量删除加工单明细
     * 
     * @param ids 需要删除的加工单明细主键集合
     * @return 结果
     */
    public int deleteProcessOrderItemByIds(Long[] ids);

    /**
     * 修改加工单状态
     * 
     * @param id 加工单明细主键
     * @return 结果
     */
    public int updateProcessOrderItemById(Long id,Integer status);
    public int updateProcessOrderItemByDetailId(Long id,Integer status);
    /**
     * 通过销售单商品id获取详情
     * @param id
     * @return
     */
    public ProcessOrderItem selectProcessInfo(Long id);
}
