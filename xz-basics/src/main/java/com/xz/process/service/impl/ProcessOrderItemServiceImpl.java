package com.xz.process.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.process.mapper.ProcessOrderItemMapper;
import com.xz.process.domain.ProcessOrderItem;
import com.xz.process.service.IProcessOrderItemService;

/**
 * 加工单明细Service业务层处理
 *
 * @author xz
 * @date 2024-02-04
 */
@Service
public class ProcessOrderItemServiceImpl extends ServiceImpl<ProcessOrderItemMapper, ProcessOrderItem> implements IProcessOrderItemService {
    @Autowired
    private ProcessOrderItemMapper processOrderItemMapper;

    /**
     * 查询加工单明细
     *
     * @param id 加工单明细主键
     * @return 加工单明细
     */
    @Override
    public ProcessOrderItem selectProcessOrderItemById(Long id) {
        return processOrderItemMapper.selectProcessOrderItemById(id);
    }

    /**
     * 查询加工单明细列表
     *
     * @param processOrderItem 加工单明细
     * @return 加工单明细
     */
    @Override
    public List<ProcessOrderItem> selectProcessOrderItemList(ProcessOrderItem processOrderItem) {
        return processOrderItemMapper.selectProcessOrderItemList(processOrderItem);
    }

    /**
     * 新增加工单明细
     *
     * @param processOrderItem 加工单明细
     * @return 结果
     */
    @Override
    public int insertProcessOrderItem(ProcessOrderItem processOrderItem) {
        return processOrderItemMapper.insertProcessOrderItem(processOrderItem);
    }

    /**
     * 修改加工单明细
     *
     * @param processOrderItem 加工单明细
     * @return 结果
     */
    @Override
    public int updateProcessOrderItem(ProcessOrderItem processOrderItem) {
        return processOrderItemMapper.updateProcessOrderItem(processOrderItem);
    }

    /**
     * 批量删除加工单明细
     *
     * @param ids 需要删除的加工单明细主键
     * @return 结果
     */
    @Override
    public int deleteProcessOrderItemByIds(Long[] ids) {
        return processOrderItemMapper.deleteProcessOrderItemByIds(ids);
    }

    /**
     * 修改加工单明细信息
     *
     * @param id 加工单明细主键
     * @return 结果
     */
    @Override
    public int updateProcessOrderItemById(Long id,Integer status) {
        return processOrderItemMapper.updateProcessOrderItemById(id,status);
    }
    public int updateProcessOrderItemByDetailId(Long id,Integer status) {
        return processOrderItemMapper.updateProcessOrderItemByDetailId(id,status);
    }
    /**
     * 通过销售单商品id获取详情
     * @param id
     * @return
     */
    public ProcessOrderItem selectProcessInfo(Long id){
        return baseMapper.selectProcessInfo(id);
    }
}
