package com.xz.process.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.process.domain.ProcessOrder;
import com.xz.process.domain.ProcessOrderItem;
import com.xz.process.dto.ProcessOrderFinishedDto;
import com.xz.process.vo.MaterialRequisitionProcessingVo;
import com.xz.process.vo.ProcessOrderExportVo;
import com.xz.process.vo.ProcessOrderInfoVo;
import com.xz.process.vo.ProcessOrderVo;
import com.xz.sales.domain.SalesOrderDetail;

/**
 * 加工单Service接口
 * 
 * @author xz
 * @date 2024-02-04
 */
public interface IProcessOrderService extends IService<ProcessOrder> {
    /**
     * 查询加工单
     * 
     * @param id 加工单主键
     * @return 加工单
     */
    public ProcessOrderInfoVo selectProcessOrderById(Long id);

    /**
     * 查询加工单列表
     * 
     * @param processOrder 加工单
     * @return 加工单集合
     */
    public List<ProcessOrderVo> selectProcessOrderList(ProcessOrder processOrder);
    /**
     * 查询加工单列表(导出)
     *
     * @param processOrder 加工单
     * @return 加工单集合
     */
    public List<ProcessOrderExportVo> selectProcessOrderExportList(ProcessOrder processOrder);
    /**
     * 新增加工单
     * 
     * @param processOrder 加工单
     * @return 结果
     */
    public boolean insertProcessOrder(ProcessOrder processOrder);

    /**
     * 修改加工单
     * 
     * @param processOrder 加工单
     * @return 结果
     */
    public AjaxResult updateProcessOrder(ProcessOrder processOrder);

    /**
     * 变更加工师
     * @param processOrder
     * @return
     */
    public AjaxResult changeMachinist(ProcessOrder processOrder);

    /**
     * 批量删除加工单
     * 
     * @param ids 需要删除的加工单主键集合
     * @return 结果
     */
    public int deleteProcessOrderByIds(Long[] ids);

    /**
     * 删除加工单信息
     * 
     * @param id 加工单主键
     * @return 结果
     */
    public int deleteProcessOrderById(Long id);

    /**
     * 签收
     * @return
     */
    AjaxResult materielSigner(ProcessOrderItem processOrderItem);

    /**
     * 客户自备镜框签收
     * @param processOrder
     * @return
     */
    AjaxResult eyeglassFramesSigner(ProcessOrder processOrder);

    /**
     * 短信通知
     * @param processOrder
     * @return
     */
    AjaxResult eyeglassFramesSmsNotification(ProcessOrder processOrder);

    /**
     * 查看短信模板
     * @param orderId
     * @param msgType
     * @return
     */
	AjaxResult getSmsNotificationContext(Long orderId, Integer msgType);

	/**
     * 领料加工
     * @param processingVo
     * @return
     */
    AjaxResult materialRequisitionProcessing(MaterialRequisitionProcessingVo processingVo);

    /**
     * 报损
     * @param orderItem
     * @return
     */
    AjaxResult reportDamage(ProcessOrderItem orderItem);

    /**
     * 成品配送
     * @param processOrder
     * @return
     */
    AjaxResult finishedProductDistribution(ProcessOrderFinishedDto processOrder);

    /**
     * 定制采购
     * @param orderItem
     * @return
     */
    AjaxResult customPurchasing(ProcessOrderItem orderItem);

    /**
     *
     * @param orderDetailList
     * @return
     */
    AjaxResult salesCustomPurchasing(List<SalesOrderDetail> orderDetailList);
}
