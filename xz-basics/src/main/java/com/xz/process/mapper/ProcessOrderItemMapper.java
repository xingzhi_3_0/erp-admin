package com.xz.process.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.process.domain.ProcessOrderItem;
import org.apache.ibatis.annotations.Param;

/**
 * 加工单明细Mapper接口
 * 
 * @author xz
 * @date 2024-02-04
 */
@Mapper
public interface ProcessOrderItemMapper  extends BaseMapper<ProcessOrderItem> {
    /**
     * 查询加工单明细
     * 
     * @param id 加工单明细主键
     * @return 加工单明细
     */
    public ProcessOrderItem selectProcessOrderItemById(Long id);

    /**
     * 通过销售单商品id获取详情
     * @param id
     * @return
     */
    public ProcessOrderItem selectProcessInfo(Long id);

    /**
     * 查询加工单明细列表
     * 
     * @param processOrderItem 加工单明细
     * @return 加工单明细集合
     */
    public List<ProcessOrderItem> selectProcessOrderItemList(ProcessOrderItem processOrderItem);

    List<ProcessOrderItem> selectListByProcessOrderId(@Param("processOrderId") Long processOrderId);

    /**
     * 新增加工单明细
     * 
     * @param processOrderItem 加工单明细
     * @return 结果
     */
    public int insertProcessOrderItem(ProcessOrderItem processOrderItem);

    /**
     * 修改加工单明细
     * 
     * @param processOrderItem 加工单明细
     * @return 结果
     */
    public int updateProcessOrderItem(ProcessOrderItem processOrderItem);

    /**
     * 更新加工单状态
     * @param id 加工单明细主键
     * @return 结果
     */
    public int updateProcessOrderItemById(@Param("id") Long id, @Param("status") Integer status);
    public int updateProcessOrderItemByDetailId(@Param("id") Long id, @Param("status") Integer status);

    /**
     * 批量删除加工单明细
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProcessOrderItemByIds(Long[] ids);
}
