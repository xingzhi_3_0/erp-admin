package com.xz.process.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.process.vo.ProcessOrderExportVo;
import com.xz.process.vo.ProcessOrderInfoVo;
import com.xz.process.vo.ProcessOrderVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.process.domain.ProcessOrder;
import org.apache.ibatis.annotations.Param;

/**
 * 加工单Mapper接口
 * 
 * @author xz
 * @date 2024-02-04
 */
@Mapper
public interface ProcessOrderMapper  extends BaseMapper<ProcessOrder> {
    /**
     * 查询加工单
     * 
     * @param id 加工单主键
     * @return 加工单
     */
    public ProcessOrderInfoVo selectProcessOrderById(Long id);

    /**
     * 查询加工单列表
     * 
     * @param processOrder 加工单
     * @return 加工单集合
     */
    public List<ProcessOrderVo> selectProcessOrderList(ProcessOrder processOrder);
    /**
     * 查询加工单列表(导出)
     *
     * @param processOrder 加工单
     * @return 加工单集合
     */
    public List<ProcessOrderExportVo> selectProcessOrderExportList(ProcessOrder processOrder);

    /**
     * 新增加工单
     * 
     * @param processOrder 加工单
     * @return 结果
     */
    public int insertProcessOrder(ProcessOrder processOrder);

    /**
     * 修改加工单
     * 
     * @param processOrder 加工单
     * @return 结果
     */
    public int updateProcessOrder(ProcessOrder processOrder);

    /**
     * 删除加工单
     * 
     * @param id 加工单主键
     * @return 结果
     */
    public int deleteProcessOrderById(Long id);

    /**
     * 批量删除加工单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProcessOrderByIds(Long[] ids);

    int updateProcessOrderStatus(@Param("id") Long id,@Param("processingStatus") Integer processingStatus);

    ProcessOrder queryBySalesOrderId(@Param("salesOrderId") Long salesOrderId);
}
