package com.xz.workbench.dto;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.workbench.enums.QueryTypeEnum;
import common.ECDateUtils;
import common.RRException;
import lombok.Data;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * @program: xz
 * @description:
 * @author: pengyuyan
 * @create: 2024-06-27 16:55
 **/
@Data
public class WorkbenchTopDto {

	/**
	 * 0.期间(时间范围);1-本周;2.本月;3.近12月
	 */
	private Integer queryType;
	/**
	 * 开始时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date startTime;
	/**
	 * 结束时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date endTime;
	/**
	 * 部门id
	 **/
	private Long deptId;
	/**
	 * 部门id
	 **/
	private List<Long> deptIds;
	/**
	 * 期间天数（用计算日均值的天数）
	 **/
	private BigDecimal periodDays;

	/**
	 * 初始化参数
	 */
	public void initParam() {
		// 如果前端未传入查询类型
		if (null == this.queryType) {
			// 判断是否传入日期（开始时间，结束时间）来决定查询类型
			if (null != this.startTime && null != this.endTime) {
				this.queryType = QueryTypeEnum.PERIOD.getType();// 按“期间”来查询
			} else {
				this.queryType = QueryTypeEnum.THIS_MONTH.getType();// “按月”来查询
			}
		}

		// 根据类型，设置 类型差异的值
		if (QueryTypeEnum.PERIOD.eq(this.queryType)) {
			// 如果是按“期间”来查询，则期间天数 = 开始时间，结束时间的天数差
			this.periodDays = new BigDecimal(ECDateUtils.differDays(this.startTime, this.endTime));
		} else if (QueryTypeEnum.THIS_MONTH.eq(this.queryType)) {
			// 如果是“按月”来查询，则期间天数 =  本月已过的天数;
			this.periodDays = new BigDecimal(DateUtil.dayOfMonth(new Date()));
			// 如果是“按月”来查询，目前业务上只需本月，上月，昨日，为了优化查询效率，所以查数据时，可以从上个月初开始查。
			this.startTime = ECDateUtils.getBeforeMonthFristDay(1);
		}else{

			// 目前顶部统计只支持这两种，如果有其他，报错即可
			throw new RRException("顶部统计不支持该类型查询");
		}
	}

	public void resetEndTime() {
		if(null != endTime){
			try {
				this.endTime = ECDateUtils.getCurrentDateEndTime(endTime);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
	}
}
