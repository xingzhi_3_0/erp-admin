package com.xz.workbench.vo;

import com.xz.common.annotation.Excel;
import com.xz.workbench.dto.WorkbenchTopDto;
import com.xz.workbench.enums.QueryTypeEnum;
import com.xz.workbench.enums.StatBizTypeEnum;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: xz
 * @description:
 * @author: pengyuyan
 * @create: 2024-06-27 16:55
 **/
@Data
public class WorkbenchesTopStaticVo {

	@Excel(name = "业务类型编码")
	private Integer bizType;
	@Excel(name = "业务类型名称")
	private String bizTypeName;
	@Excel(name = "今日数额")
	private BigDecimal todayCount;
	@Excel(name = "昨日数额")
	private BigDecimal yesterdayCount;
	@Excel(name = "本月数额")
	private BigDecimal thisMonthCount;
	@Excel(name = "上月数额")
	private BigDecimal lastMonthCount;
	@Excel(name = "本年数额")
	private BigDecimal thisYearCount;
	@Excel(name = "去年数额")
	private BigDecimal lastYearCount;
	@Excel(name = "总数额")
	private BigDecimal totalCount;
	@Excel(name = "日均数额")
	private BigDecimal dayAvgCount;

	List<WorkbenchesTopStaticVo> items;

	/**
	 * 初始化
	 * @param bizTypeEnum
	 * @return
	 */
	public static WorkbenchesTopStaticVo init(StatBizTypeEnum bizTypeEnum) {
		WorkbenchesTopStaticVo vo = new WorkbenchesTopStaticVo();
		vo.setBizType(bizTypeEnum.getType());
		vo.setBizTypeName(bizTypeEnum.getName());
		vo.setTodayCount(BigDecimal.ZERO);
		//vo.setYesterdayCount(BigDecimal.ZERO);
		vo.setThisMonthCount(BigDecimal.ZERO);
		vo.setLastMonthCount(BigDecimal.ZERO);
		//vo.setThisYearCount(BigDecimal.ZERO);
		//vo.setLastYearCount(BigDecimal.ZERO);
		vo.setTotalCount(BigDecimal.ZERO);
		vo.setDayAvgCount(BigDecimal.ZERO);
		return vo;
	}

	/**
	 * 初始化
	 * @param bizTypeEnum
	 * @return
	 */
	public static WorkbenchesTopStaticVo setDefault(WorkbenchesTopStaticVo vo,StatBizTypeEnum bizTypeEnum) {
		if(null == vo){
			// 如果门店销售统计没有值,则初始化一个对象,来此顶替返回.
			return init(bizTypeEnum);
		}
		// 避免字段返回null，至少返回值0（默认值）；
		vo.setTodayCount(setDefaultVal(vo.getTodayCount()));
		//vo.setYesterdayCount(setDefaultVal(vo.getYesterdayCount()));
		vo.setThisMonthCount(setDefaultVal(vo.getThisMonthCount()));
		vo.setLastMonthCount(setDefaultVal(vo.getLastMonthCount()));
		//vo.setThisYearCount(setDefaultVal(vo.getThisYearCount()));
		//vo.setLastYearCount(setDefaultVal(vo.getLastYearCount()));
		vo.setTotalCount(setDefaultVal(vo.getTotalCount()));
		vo.setDayAvgCount(setDefaultVal(vo.getDayAvgCount()));
		// 指定类型
		vo.setBizType(bizTypeEnum.getType());
		vo.setBizTypeName(bizTypeEnum.getName());
		return vo;
	}

	/**
	 * 装饰字段值
	 * @param dto 查询时传入的DTO
	 * @param bizTypeEnum 结果VO的类型
	 * @param vo 结果VO
	 * @return 装饰后的结果VO
	 */
	public static WorkbenchesTopStaticVo decorateVo(WorkbenchTopDto dto, StatBizTypeEnum bizTypeEnum,
	                                          WorkbenchesTopStaticVo vo) {

		// 先规范返回值（设置默认值），避免返回对象为null，或者返回的字段为null；
		// 设置默认值同时也对bizType赋值；
		vo = WorkbenchesTopStaticVo.setDefault(vo,bizTypeEnum);

		// 计算日均金额
		// 如果是“按月”来查询，则总数=本月数
		// 如果是按“期间”来查则不做任何操作，因为期间查的就是总数。
		if (QueryTypeEnum.THIS_MONTH.eq(dto.getQueryType())) {
			vo.setTotalCount(vo.getThisMonthCount());
		}
		// 日均 = 总额 / 期间天数
		vo.setDayAvgCountByPeriodDays(dto.getPeriodDays());
		// 返回
		return vo;
	}

	/**
	 * 合并WorkbenchesTopStaticVo对象
	 * @param vos
	 */
	public void add(WorkbenchesTopStaticVo... vos) {
		items = new ArrayList<>();
		for (WorkbenchesTopStaticVo vo : vos) {
			this.todayCount = this.todayCount.add(vo.getTodayCount());
			//this.yesterdayCount = this.yesterdayCount.add(vo.getYesterdayCount());
			this.thisMonthCount = this.thisMonthCount.add(vo.getThisMonthCount());
			this.lastMonthCount = this.lastMonthCount.add(vo.getLastMonthCount());
			//this.thisYearCount = this.thisYearCount.add(vo.getThisYearCount());
			//this.lastYearCount = this.lastYearCount.add(vo.getLastYearCount());
			this.dayAvgCount = this.dayAvgCount.add(vo.getDayAvgCount());
			this.totalCount = this.totalCount.add(vo.getTotalCount());
			this.items.add(vo);
		}
	}

	/**
	 * 设置日均值(日均 = 总额 / 期间天数)
	 * @param periodDays
	 */
	public void setDayAvgCountByPeriodDays(BigDecimal periodDays) {
		if (null == this.totalCount
				|| null == periodDays
				|| 0 == this.totalCount.compareTo(BigDecimal.ZERO)
				|| 0 == periodDays.compareTo(BigDecimal.ZERO)) {
			this.dayAvgCount = BigDecimal.ZERO;
		}else{
			// 用总额度除于期间天数，得出日均额是多少。
			this.dayAvgCount = this.totalCount.divide(periodDays, 4, BigDecimal.ROUND_HALF_UP);
		}
	}

	/**
	 * bigDecimal字段默认值
	 * @param bigDecimal
	 */
	public static BigDecimal setDefaultVal(BigDecimal bigDecimal) {
		return null != bigDecimal ? bigDecimal : BigDecimal.ZERO;
	}
}
