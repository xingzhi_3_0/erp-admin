package com.xz.workbench.enums;

import lombok.Getter;

/**
 * @author Administrator
 */

@Getter
public enum QueryTypeEnum {

	/** 期间(时间范围) **/
	PERIOD(0,"期间(时间范围)"),
	/** 本周 **/
	THIS_WEEK(1,"本周"),
	/** 本月 **/
	THIS_MONTH(2,"本月"),
	/** 近12月 **/
	RECENT_DECEMBER(3,"近12月"),
	;

	private final Integer type;

	private final String name;

	QueryTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	public boolean eq(Integer type) {
		if(null == type){
			return false;
		}
		return this.type.equals(type);
	}
}
