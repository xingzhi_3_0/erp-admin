package com.xz.workbench.enums;

import lombok.Getter;

/**
 * @author Administrator
 */

@Getter
public enum StatBizTypeEnum {

	/** 门店销售额 **/
	INDEX_MDSS(1,"门店销售额"),
	/** 诊疗人数 **/
	INDEX_ZLRS(2,"诊疗人数"),
	/** 已交付 **/
	INDEX_JRJF(3,"已交付"),
	/** 充值金额 **/
	INDEX_CZJE(4,"充值金额"),
	/** 新增患者 **/
	INDEX_XZHZ(5,"新增患者"),
	/** 新增会员 **/
	INDEX_XZHY(6,"新增会员"),


	/** 销售单售额 **/
	SUB_INDEX_XS(101,"销售单售额"),
	/** 挂号单售额 **/
	SUB_INDEX_GHZL(102,"挂号诊疗单售额"),

	/** 销售单退款 **/
	SUB_INDEX_XSTK(104,"销售退款单售额"),
	/** 销售单换货补价 **/
	SUB_INDEX_XSHH(105,"销售单换货补价"),
	;
	private final Integer type;

	private final String name;

	StatBizTypeEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}
}
