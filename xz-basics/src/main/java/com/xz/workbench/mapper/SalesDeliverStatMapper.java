package com.xz.workbench.mapper;

import com.xz.workbench.dto.WorkbenchTopDto;
import com.xz.workbench.vo.WorkbenchesTopStaticVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 交付统计
 */
@Mapper
public interface SalesDeliverStatMapper {
	/**
	 *
	 * 获取统计信息
	 * @param dto
	 * @return
	 */
	WorkbenchesTopStaticVo selectStatistics(WorkbenchTopDto dto);
}
