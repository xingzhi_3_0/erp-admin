package com.xz.workbench.mapper;

import com.xz.workbench.dto.WorkbenchTopDto;
import com.xz.workbench.vo.WorkbenchesTopStaticVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 销售退货单统计
 */
@Mapper
public interface SalesReturnedStatMapper {
	/**
	 *
	 * 获取统计信息
	 * @param dto
	 * @return
	 */
	WorkbenchesTopStaticVo selectStatistics(WorkbenchTopDto dto);
}
