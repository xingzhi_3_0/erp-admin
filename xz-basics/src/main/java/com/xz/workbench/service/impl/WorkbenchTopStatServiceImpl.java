package com.xz.workbench.service.impl;

import com.xz.workbench.dto.WorkbenchTopDto;
import com.xz.workbench.enums.StatBizTypeEnum;
import com.xz.workbench.mapper.*;
import com.xz.workbench.service.IWorkbenchTopStatService;
import com.xz.workbench.vo.WorkbenchesTopStaticVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 控制台顶部统计服务实现类
 *
 * @author xz
 * @date 2024-2-19 11:15:53
 */
@Service
public class WorkbenchTopStatServiceImpl implements IWorkbenchTopStatService {

	@Resource
	private SalesStatMapper salesStatMapper;
	@Resource
	private SalesReturnedStatMapper salesReturnedStatMapper;
	@Resource
	private SalesExchangedStatMapper salesExchangedStatMapper;
	@Resource
	private DiagnosisLogStatMapper diagnosisLogStatMapper;
	@Resource
	private DiagnosisPatientStatMapper diagnosisPatientStatMapper;
	@Resource
	private RechargeStatMapper rechargeStatMapper;
	@Resource
	private SalesDeliverStatMapper salesDeliverStatMapper;
	@Resource
	private PatientInfoStatMapper patientInfoStatMapper;
	@Resource
	private MemberInfoStatMapper memberInfoStatMapper;
	@Resource
	private DiagnosisStatMapper diagnosisStatMapper;
	@Resource
	private DiagnosisTreatmentStatMapper diagnosisTreatmentStatMapper;

	@Override
	public List<WorkbenchesTopStaticVo> selectStatistics(WorkbenchTopDto dto) {

		// 初始化参数
		dto.initParam();

		// 销售比较复杂，需要汇总多方数据,具体见方法内
		WorkbenchesTopStaticVo systemSalesVo = getSystemSalesVo(dto);
		// 诊疗人数统计
		WorkbenchesTopStaticVo diagnosisPatientVo = diagnosisPatientStatMapper.selectStatistics(dto);
		// 门店的交付统计
		WorkbenchesTopStaticVo deliverVo = salesDeliverStatMapper.selectStatistics(dto);
		// 充值统计
		WorkbenchesTopStaticVo rechargeVo = rechargeStatMapper.selectStatistics(dto);
		// 新增患者统计
		WorkbenchesTopStaticVo patientVo = patientInfoStatMapper.selectStatistics(dto);
		// 新增会员统计
		WorkbenchesTopStaticVo memberVo = memberInfoStatMapper.selectStatistics(dto);

		//装饰字段值，优化结果，便于前端显示
		diagnosisPatientVo = WorkbenchesTopStaticVo.decorateVo(dto, StatBizTypeEnum.INDEX_ZLRS, diagnosisPatientVo);
		deliverVo = WorkbenchesTopStaticVo.decorateVo(dto, StatBizTypeEnum.INDEX_JRJF, deliverVo);
		rechargeVo = WorkbenchesTopStaticVo.decorateVo(dto, StatBizTypeEnum.INDEX_CZJE, rechargeVo);
		patientVo = WorkbenchesTopStaticVo.decorateVo(dto, StatBizTypeEnum.INDEX_XZHZ, patientVo);
		memberVo = WorkbenchesTopStaticVo.decorateVo(dto, StatBizTypeEnum.INDEX_XZHY, memberVo);

		// 开始构建结果数据
		List<WorkbenchesTopStaticVo> resultList = new ArrayList<>();
		resultList.add(systemSalesVo);
		resultList.add(diagnosisPatientVo);
		resultList.add(deliverVo);
		resultList.add(rechargeVo);
		resultList.add(patientVo);
		resultList.add(memberVo);
		// 返回结果
		return resultList;
	}

	/**
	 * 获取平台总的销售金额
	 *
	 * @param dto
	 */
	private WorkbenchesTopStaticVo getSystemSalesVo(WorkbenchTopDto dto) {

		// 销售单统计
		WorkbenchesTopStaticVo salesVo = salesStatMapper.selectStatistics(dto);
		// 退款单统计
		WorkbenchesTopStaticVo salesReturnedVo = salesReturnedStatMapper.selectStatistics(dto);
		// 换货单统计
		WorkbenchesTopStaticVo salesExchangedVo = salesExchangedStatMapper.selectStatistics(dto);
		// 挂号诊疗金额统计
		WorkbenchesTopStaticVo diagnosisVo = diagnosisLogStatMapper.selectStatistics(dto);

		// 装饰字段值，优化结果，便于前端显示
		salesVo = WorkbenchesTopStaticVo.decorateVo(dto, StatBizTypeEnum.SUB_INDEX_XS, salesVo);
		diagnosisVo = WorkbenchesTopStaticVo.decorateVo(dto, StatBizTypeEnum.SUB_INDEX_GHZL, diagnosisVo);
		salesReturnedVo = WorkbenchesTopStaticVo.decorateVo(dto, StatBizTypeEnum.SUB_INDEX_XSTK, salesReturnedVo);
		salesExchangedVo = WorkbenchesTopStaticVo.decorateVo(dto, StatBizTypeEnum.SUB_INDEX_XSHH, salesExchangedVo);

		// 初始化》统计合并的vo对象
		WorkbenchesTopStaticVo res = WorkbenchesTopStaticVo.init(StatBizTypeEnum.INDEX_MDSS);
		// 开始合并
		res.add(salesVo, diagnosisVo,salesExchangedVo,salesReturnedVo);
		// 重新计算统计合并的vo对象的日均金额(日均 = 总额 / 期间天数)
		res.setDayAvgCountByPeriodDays(dto.getPeriodDays());
		// 返回结果
		return res;
	}



}
