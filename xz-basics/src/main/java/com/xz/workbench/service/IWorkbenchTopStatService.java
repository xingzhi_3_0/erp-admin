package com.xz.workbench.service;

import com.xz.workbench.dto.WorkbenchTopDto;
import com.xz.workbench.vo.WorkbenchesTopStaticVo;

import java.util.List;

/**
 * 控制台顶部统计服务类
 */
public interface IWorkbenchTopStatService {

    List<WorkbenchesTopStaticVo> selectStatistics(WorkbenchTopDto dto);
}
