package com.xz.enums;

/**
 * @author Administrator
 */

public enum ChangeTypeEnum {

	/**
	 * 充值
	 */
	WALLET_RECHARGE(1,"充值"),

	WALLET_REFUND(2,"钱包退款"),

	MANUAL_INCREASE(99,"手动增加"),

	MANUAL_REDUCTION(98,"手动减少"),


	/*** 下面是公用的 ***/

	SALES_CONSUMPTION(3,"销售单退款"),

	SALES_REFUND(4,"销售单消费"),

	DIAGNOSIS_CONSUMPTION(5,"诊疗单退款"),

	DIAGNOSIS_REFUND(6,"诊疗单消费");


	private final Integer status;

	private final String name;

	ChangeTypeEnum(Integer status, String name) {
		this.status = status;
		this.name = name;
	}

	public Integer getStatus() {
		return status;
	}

	public String getName() {
		return name;
	}
}
