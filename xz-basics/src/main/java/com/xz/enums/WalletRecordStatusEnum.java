package com.xz.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * @author Administrator
 */

public enum WalletRecordStatusEnum {


	/**
	 * 充值
	 */
	WALLET_RECHARGE(1,1),

	WALLET_REFUND(2,3),

	SALES_CONSUMPTION(3,2),

	SALES_REFUND(4,4),

	DIAGNOSIS_CONSUMPTION(5,2),

	DIAGNOSIS_REFUND(6,4);


	private final Integer status;

	private final Integer name;

	WalletRecordStatusEnum(Integer status, Integer name) {
		this.status = status;
		this.name = name;
	}

	public Integer getStatus() {
		return status;
	}

	public Integer getName() {
		return name;
	}

	public static WalletRecordStatusEnum typeOf(Integer value) {
		if (Objects.isNull(value)) {
			return null;
		}
		for (WalletRecordStatusEnum item : values()) {
			if (value.equals(item.getStatus())) {
				return item;
			}
		}
		return null;
	}
}
