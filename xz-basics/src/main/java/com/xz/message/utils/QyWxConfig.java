package com.xz.message.utils;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class QyWxConfig {

    /**
     *  请求路径
     */
    @Value("${qiyewx.url}")
    private String url;

    /**
     * 企业微信ID
     */
    @Value("${qiyewx.corpid}")
    private String corpid;

    /**
     * 企业应用的凭证密钥
     */
    @Value("${qiyewx.corpsecret}")
    private String corpsecret;

    /**
     * 开发者设置的token
     */
    @Value("${qiyewx.token}")
    private String token;

    /**
     * 开发者设置的EncodingAESKey
     */
    @Value("${qiyewx.encodingAESKey}")
    private String encodingAESKey;
}
