package com.xz.message.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.xz.common.annotation.Excel;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 消息模板对象 t_mmstemplate
 *
 * @author xz
 * @date 2022-03-04
 */
@Data
@TableName("t_mmstemplate")
public class Mmstemplate extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @TableId
    private Long id;

    /** 推送标题 */
    @Excel(name = "推送标题")
    private String title;

    /** 模板名称 */
    @Excel(name = "模板名称")
    private String name;

    /** 模板内容 */
    @Excel(name = "模板内容")
    private String content;

    /** 状态(1停用,2启用) */
    @Excel(name = "状态(1停用,2启用)")
    private Long status;

    /** 模板参数 */
    @Excel(name = "模板参数")
    private String param;

    /** 可选模板参数 */
    @Excel(name = "可选模板参数")
    private String optionaParam;

    /** 推送节点 */
    @Excel(name = "推送节点")
    private Integer pushNode;

    /** 删除状态（0未删除，1已删除） */
    private String delFlag;
    //门店id
    private String storeId;
    //门店名称
    private String storeName;
    //推送人员id
    private String pushStaff;
    private String pushStaffName;
    private String alarmType;
    private String alarmName;
    /**
     * 是否授权(1已授权,2 未授权)
     */
    private Integer isAuthorization;

    /** 租户id */
    private Long tenantId;
    //部门id
    @TableField(exist = false)
    private Long deptId;
    //订单通知
    @TableField(exist = false)
    public List<Mmstemplate> newOrderList;
    //业务员通知
    @TableField(exist = false)
    public List<Mmstemplate> salespersonNoticeList;
    //用户取车通知
    @TableField(exist = false)
    public List<Mmstemplate> userPickupNotificationList;
    //gps告警通知
    @TableField(exist = false)
    public List<Mmstemplate> gpsAlarmNotificationList;
}
