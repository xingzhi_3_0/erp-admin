package com.xz.message.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;

import com.xz.message.vo.StoreConfigVo;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 消息模板配置对象 sys_tenant_notify_item
 *
 * @author xz
 * @date 2023-08-08
 */
@Data
@TableName("sys_tenant_notify_item")
public class SysTenantNotifyItem extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 通知类型
        0 会员钱包充值
        1 会员钱包消费
        2 会员钱包消费退款
        3 会员钱包退款
        4 会员钱包绑定患者
        5 会员钱包解绑患者
        6 通知客户寄送镜框
        7 成品邮寄
        8 就诊关怀1
        9 就诊关怀2
        10 活动通知1
        11 活动通知2
     */
    private Integer msgType;

    /** 通知内容 */
    @Excel(name = "通知内容")
    private String msgContent;

    /** 门店ID */
    @Excel(name = "门店ID")
    private String storeId;

    /** 门店名称 */
    @Excel(name = "门店名称")
    private String storeName;

    /** 推送人员id */
    @Excel(name = "推送人员id")
    private String pushStaff;

    /** 企微单入通知 */
    @Excel(name = "企微单入通知")
    private Long qyDrEnable;

    /** 钉钉单入通知 */
    @Excel(name = "钉钉单入通知")
    private Long ddDrEnable;

    /** 推送人员名称 */
    @Excel(name = "推送人员名称")
    private String pushStaffName;

    /** 告警类型 */
    @Excel(name = "告警类型")
    private String alarmType;

    /** 短信通知 */
    @Excel(name = "短信通知")
    private Long dxEnable;

    /** 企微群通知 */
    @Excel(name = "企微群通知")
    private Long qyQunEnable;

    /** 告警类型名称 */
    @Excel(name = "告警类型名称")
    private String alarmName;

    /** 企微webhook */
    @Excel(name = "企微webhook")
    private String qyWebhook;

    /** 钉钉群通知 */
    @Excel(name = "钉钉群通知")
    private Long ddQunEnable;

    /** 钉钉webhook */
    @Excel(name = "钉钉webhook")
    private String ddWebhook;

    /** 提前N分钟 */
    @Excel(name = "提前N分钟")
    private Long leadTime;

    @TableField(exist = false)
    private List<String> storeIds;

    public SysTenantNotifyItem(StoreConfigVo storeConfigVo) {
        this.storeId = storeConfigVo.getStoreId();
        this.storeName = storeConfigVo.getStoreName();
        this.pushStaff = storeConfigVo.getPushStaff();
        this.qyDrEnable = storeConfigVo.getQyDrEnable();
        this.ddDrEnable = storeConfigVo.getDdDrEnable();
        this.pushStaffName = storeConfigVo.getPushStaffName();
        this.alarmType = storeConfigVo.getAlarmType();
        this.dxEnable = storeConfigVo.getDxEnable();
        this.qyQunEnable = storeConfigVo.getQyQunEnable();
        this.alarmName = storeConfigVo.getAlarmName();
        this.qyWebhook = storeConfigVo.getQyWebhook();
        this.ddQunEnable = storeConfigVo.getDdQunEnable();
        this.ddWebhook = storeConfigVo.getDdWebhook();
    }
    public SysTenantNotifyItem() {
    }
}
