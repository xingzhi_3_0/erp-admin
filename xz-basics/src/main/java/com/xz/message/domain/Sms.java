package com.xz.message.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 短信对象 t_sms
 *
 * @author xz
 * @date 2022-03-07
 */
@Data
@TableName("t_sms")
public class Sms extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 短信内容 */
    @Excel(name = "短信内容")
    private String content;

    /** 模板id */
    private Long templateId;

    /** 发送状态（1=未发布,0=已发布 */
    @Excel(name = "发送状态", readConverterExp = "1=未发布,0=已发布")
    private String status;

    /** 发送时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "发送时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date sendTime;

    /** 收件人(客户id,逗号隔开) */
    private String customerId;

    /** 收件人姓名(客户name,逗号隔开) */
    @Excel(name = "收件人姓名")
    private String customerName;

    /** $column.columnComment */
    private Long tenantId;
    /** 推送节点 */
    private Integer pushNode;
    /**
     * 业务编号
     */
    private String bizNo;

    @TableField(exist = false)
    private String time;


}
