package com.xz.message.domain;

import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 租户消息通知配置对象 sys_tenant_notify_config
 *
 * @author xz
 * @date 2023-08-04
 */
@Data
@TableName("sys_tenant_notify_config")
public class SysTenantNotifyConfig extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 租户id */
    @Excel(name = "根部门id")
    private String departmentId;

    /** 企业微信状态 0停用 1启用 */
    @Excel(name = "企业微信状态 0停用 1启用")
    private Integer qyEnable;

    /** 企业微信企业ID */
    @Excel(name = "企业微信企业ID")
    private String qyCorpId;

    /** 企业微信应用ID */
    @Excel(name = "企业微信应用ID")
    private String qyAgentId;

    /** 企业微信应用密钥 */
    @Excel(name = "企业微信应用密钥")
    private String qyCorpSecret;

    /** 钉钉状态 0停用 1启用 */
    @Excel(name = "钉钉状态 0停用 1启用")
    private Integer ddEnable;

    /** 钉钉企业ID */
    @Excel(name = "钉钉企业ID")
    private String ddCorpId;

    /** 钉钉应用ID */
    @Excel(name = "钉钉应用ID")
    private String ddAgentId;

    /** 钉钉应用密钥 */
    @Excel(name = "钉钉应用密钥")
    private String ddCorpSecret;

    /** 短信状态 0停用 1启用 */
    @Excel(name = "短信状态 0停用 1启用")
    private Integer dxEnable;

    /** 取车前N分钟通知员工 */
    @Excel(name = "取车前N分钟通知员工")
    private Long nearPickTimeStaff;

    /** 还车前N分钟通知员工 */
    @Excel(name = "还车前N分钟通知员工")
    private Long nearReturnTimeStaff;

    /** 取车前N分钟通知客户 */
    @Excel(name = "取车前N分钟通知客户")
    private Long nearPickTimeCustomer;

    /** 还车前N分钟通知客户 */
    @Excel(name = "还车前N分钟通知客户")
    private Long nearReturnTimeCustomer;

    public SysTenantNotifyConfig(Long tenantId) {
        this.tenantId = tenantId;
        this.qyEnable = 0;
        this.ddEnable = 0;
        this.dxEnable = 0;
        this.nearPickTimeStaff = 60L;
        this.nearReturnTimeStaff = 60L;
        this.nearPickTimeCustomer = 60L;
        this.nearReturnTimeCustomer = 60L;
    }
    public SysTenantNotifyConfig() {

    }
}
