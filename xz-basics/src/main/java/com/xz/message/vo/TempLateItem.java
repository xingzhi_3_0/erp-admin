package com.xz.message.vo;

import com.xz.message.domain.SysTenantNotifyItem;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @class name:TempLateItem
 * @description: TODO
 * @author: LiWangJun
 * @create Time: 2023/8/8 11:25
 **/
@Data
public class TempLateItem {
    private Boolean loading;

    private String content0;
    private String content1;
    private String content2;
    private String content3;
    private String content4;
    private String content5;

    /** 预警时间 */
    private Integer warningTime;

    private Long leadTimeOfPick;
    private Long leadTimeOfReturn;

    private List<StoreConfigVo> storeConfigList;

    public List<StoreConfigVo> getStoreConfigList() {
        return Optional.ofNullable(storeConfigList).orElse(new ArrayList<>());
    }

    public TempLateItem() {
    }

    public TempLateItem(List<SysTenantNotifyItem> list) {
        this.loading = false;
        if(Objects.nonNull(list)&&list.size()>0){
            this.content0 = list.get(0).getMsgContent();
            this.storeConfigList = list.stream().map(StoreConfigVo::new).collect(Collectors.toList());
        }
    }

    public TempLateItem(List<SysTenantNotifyItem> list,Integer warningTime) {
        this.loading = false;
        this.warningTime = warningTime;
        if(Objects.nonNull(list)&&list.size()>0){
            this.content0 = list.get(0).getMsgContent();
            this.storeConfigList = list.stream().map(StoreConfigVo::new).collect(Collectors.toList());
        }
    }
}
