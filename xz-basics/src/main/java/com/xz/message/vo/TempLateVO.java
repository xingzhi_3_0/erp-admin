package com.xz.message.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

/**
 * @class name:TempLateVO
 **/
@Data
public class TempLateVO{
    private Integer qyEnable;
    private Integer ddEnable;
    private Integer dxEnable;
    private Long nearPickTimeStaff;
    private Long nearReturnTimeStaff;
    private Long nearPickTimeCustomer;
    private Long nearReturnTimeCustomer;
    private TempLateItem template1;
    private TempLateItem template2;
    private TempLateItem template3;
    private TempLateItem template4;
    private TempLateItem template5;
    private TempLateItem template6;
    private TempLateItem template7;
    private TempLateItem template8;
    private TempLateItem template9;
    private TempLateItem template10;
}
