package com.xz.message.vo;

import lombok.Data;

import java.util.HashMap;
import java.util.List;

/**
 * @class name:MessageBody
 * @description: TODO
 * @author: LiWangJun
 * @create Time: 2023/8/4 11:31
 **/
@Data
public class MessageBody {

    /**
     * 指定接收消息的成员，成员ID列表（多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为"@all"，则向该企业应用的全部成员发送
     */
    private String touser;
    /**
     * 	指定接收消息的部门，部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为"@all"时忽略本参数
     */
    private String toparty;
    /**
     * 指定接收消息的标签，标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为"@all"时忽略本参数
     */
    private String totag;
    /**
     * 消息类型，此时固定为：text
     */
    private String msgtype;
    /**
     * 企业应用的id，整型。企业内部开发，可在应用的设置页面查看；第三方服务商，可通过接口 获取企业授权信息 获取该参数值
     */
    private String agentid;
    /**
     * 	消息内容，最长不超过2048个字节
     */
    private HashMap<String,Object> text;
    /**
     * 	否是保密消息，0表示可对外分享，1表示不能分享且内容显示水印，默认为0
     */
    private Integer safe;
    /**
     * 是否开启id转译，0表示否，1表示是，默认0
     */
    private Integer enable_id_trans;
    /**
     * 是否开启重复消息检查，0表示否，1表示是，默认0
     */
    private Integer enable_duplicate_check;
    /**
     * 是否重复消息检查的时间间隔，默认1800s，最大不超过4小时
     */
    private Integer duplicate_check_interval;


    public MessageBody() {
    }

    /**
     * 应用文本消息
     * @author ZhangZhiJia
     * @date 2023/8/4 11:44
     * @param touser
     * @param agentid
     * @param content
     * @return null
     */
    public MessageBody(String touser, String agentid, String content) {
        this.touser = touser;
        this.agentid = agentid;
        HashMap<String,Object> map =new HashMap<>();
        map.put("content",content);
        this.text = map;
        this.msgtype = "text";
    }

    /**
     * 群机器人文本消息
     * @author ZhangZhiJia
     * @date 2023/8/4 11:46
     * @param content 文本内容
     * @param mentionedList userid的列表
     * @param mentionedMobileList 手机号列表
     * @return null
     */
    public MessageBody(String content, List<String> mentionedList) {
        HashMap<String,Object> map =new HashMap<>();
        map.put("content",content);
        map.put("mentioned_list",mentionedList);
        this.msgtype = "text";
        this.text = map;
    }
    /**
     * 群机器人文本消息
     * @author ZhangZhiJia
     * @date 2023/8/4 11:46
     * @param content 文本内容
     * @return null
     */
    public MessageBody(String content) {
        HashMap<String,Object> map =new HashMap<>();
        map.put("content",content);
        this.msgtype = "text";
        this.text = map;
    }
}
