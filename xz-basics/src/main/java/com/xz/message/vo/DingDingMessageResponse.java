package com.xz.message.vo;

import lombok.Data;

/**
 * @class name:MessageResponse
 * @description: TODO
 * @author: LiWangJun
 * @create Time: 2023/8/4 11:50
 **/
@Data
public class DingDingMessageResponse {

    /**
     * 返回码
     **/
    private Integer errcode;

    /**
     * 对返回码的文本描述内容
     **/
    private String errmsg;

    /**
     * 	创建的异步发送任务ID
     **/
    private String task_id;
    /**
     * 	请求ID
     **/
    private String request_id;

}
