package com.xz.message.vo;

import lombok.Data;

/**
 * @class name:MessageResponse
 * @description: TODO
 * @author: LiWangJun
 * @create Time: 2023/8/4 11:50
 **/
@Data
public class MessageResponse {

    /**
     * 返回码
     **/
    private Integer errcode;

    /**
     * 对返回码的文本描述内容
     **/
    private String errmsg;

    /**
     * 	不合法的userid
     **/
    private String invaliduser;
    /**
     * 	不合法的partyid
     **/
    private String invalidparty;
    /**
     * 	不合法的标签id
     **/
    private String invalidtag;
    /**
     * 	没有基础接口许可(包含已过期)的userid
     **/
    private String unlicenseduser;
    /**
     * 消息id，用于撤回应用消息
     **/
    private String msgid;
    /**
     * 仅消息类型为“按钮交互型”，“投票选择型”和“多项选择型”的模板卡片消息返回，应用可使用response_code调用更新模版卡片消息接口，72小时内有效，且只能使用一次
     */
    private String response_code;
}
