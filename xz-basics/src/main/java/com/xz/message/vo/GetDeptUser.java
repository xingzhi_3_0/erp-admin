package com.xz.message.vo;

import lombok.Data;

import java.util.List;

/**
 * @class name:GetDeptUser
 * @description: TODO
 * @author: LiWangJun
 * @create Time: 2023/8/4 11:24
 **/
@Data
public class GetDeptUser {

    private Integer errcode;
    private String errmsg;
    private List<DeptUser> userlist;

}
