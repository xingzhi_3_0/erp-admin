package com.xz.message.vo;

import lombok.Data;

/**
 * @class name:DingDingUser
 * @description: TODO
 * @author: LiWangJun
 * @create Time: 2023/8/11 17:42
 **/
@Data
public class QiWeiOrDingDingUser {

    private String userid;
    private String name;
    private String mobile;

}
