package com.xz.message.vo;

import lombok.Data;

import java.util.HashMap;
import java.util.List;

/**
 * @class name:MessageBody
 * @description: TODO
 * @author: LiWangJun
 * @create Time: 2023/8/4 11:31
 **/
@Data
public class DingDingMessageBody {

    /**
     * 发送消息时使用的微应用的AgentID。
     */
    private String agent_id;
    /**
     * 接收者的userid列表，最大用户列表长度100  user123,user456
     */
    private String userid_list;
    /**
     * 接收者的部门id列表，最大列表长度20。 接收者是部门ID时，包括子部门下的所有用户。
     */
    private String dept_id_list;
    /**
     * 是否发送给企业全部用户
     */
    private Boolean to_all_user;

    /**
     * 	消息内容，最长不超过2048个字节
     */
    private HashMap<String,Object> msg;



    public DingDingMessageBody() {
    }

    /**
     * 应用文本消息
     * @author ZhangZhiJia
     * @date 2023/8/4 11:44
     * @param touser
     * @param agentid
     * @param content
     * @return null
     */
    public DingDingMessageBody(String touser, String agentid, String content) {
        this.userid_list = touser;
        this.agent_id = agentid;
        HashMap<String,Object> text =new HashMap<>();
        text.put("content",content);
        HashMap<String,Object> msg =new HashMap<>();
        msg.put("msgtype","text");
        msg.put("text",text);

        this.msg = msg;
    }

    /**
     * 群机器人文本消息
     * @author ZhangZhiJia
     * @date 2023/8/4 11:46
     * @param content 文本内容
     * @param mentionedList userid的列表
     */
    public DingDingMessageBody(String content, List<String> mentionedList) {
        HashMap<String,Object> text =new HashMap<>();
        text.put("content",content);
        HashMap<String,Object> msg =new HashMap<>();
        msg.put("msgtype","text");
        msg.put("text",text);

        HashMap<String,Object> at =new HashMap<>();
        at.put("isAtAll",false);
        at.put("atUserIds",mentionedList);
        msg.put("at",at);

        this.msg = msg;

    }
}
