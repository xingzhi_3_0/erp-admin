package com.xz.message.vo;

import com.xz.common.annotation.Excel;
import com.xz.message.domain.SysTenantNotifyItem;
import lombok.Data;

/**
 * @class name:StoreConfigVo
 * @description: TODO
 * @author: LiWangJun
 * @create Time: 2023/8/8 11:31
 **/
@Data
public class StoreConfigVo {

    /** 门店ID */
    private String storeId;

    /** 门店名称 */
    private String storeName;

    /** 推送人员id */
    private String pushStaff;

    /** 企微单入通知 */
    private Long qyDrEnable;

    /** 钉钉单入通知 */
    private Long ddDrEnable;

    /** 推送人员名称 */
    private String pushStaffName;

    /** 告警类型 */
    private String alarmType;

    /** 短信通知 */
    private Long dxEnable;

    /** 企微群通知 */
    private Long qyQunEnable;

    /** 告警类型名称 */
    private String alarmName;

    /** 企微webhook */
    private String qyWebhook;

    /** 钉钉群通知 */
    private Long ddQunEnable;

    /** 钉钉webhook */
    private String ddWebhook;

    public StoreConfigVo(SysTenantNotifyItem sysTenantNotifyItem) {
        this.storeId = sysTenantNotifyItem.getStoreId();
        this.storeName = sysTenantNotifyItem.getStoreName();
        this.pushStaff = sysTenantNotifyItem.getPushStaff();
        this.qyDrEnable = sysTenantNotifyItem.getQyDrEnable();
        this.ddDrEnable = sysTenantNotifyItem.getDdDrEnable();
        this.pushStaffName = sysTenantNotifyItem.getPushStaffName();
        this.alarmType = sysTenantNotifyItem.getAlarmType();
        this.dxEnable = sysTenantNotifyItem.getDxEnable();
        this.qyQunEnable = sysTenantNotifyItem.getQyQunEnable();
        this.alarmName = sysTenantNotifyItem.getAlarmName();
        this.qyWebhook = sysTenantNotifyItem.getQyWebhook();
        this.ddQunEnable = sysTenantNotifyItem.getDdQunEnable();
        this.ddWebhook = sysTenantNotifyItem.getDdWebhook();
    }

    public StoreConfigVo() {
    }
}
