package com.xz.message.vo;

import lombok.Data;

import java.util.List;

/**
 * @class name:DeptUser
 * @description: TODO
 * @author: LiWangJun
 * @create Time: 2023/8/4 11:12
 **/
@Data
public class DeptUser {
//    {
//        "userid": "zhangsan",
//            "name": "张三",
//            "department": [1, 2],
//        "open_userid": "xxxxxx"
//    }
    private String userid;
    private String name;
    private List<Integer> department;
    private String open_userid;
}
