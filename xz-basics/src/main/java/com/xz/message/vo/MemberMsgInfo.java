package com.xz.message.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class MemberMsgInfo {

    private Long id;

    /** 会员卡号 */
    private String memberCardNo;

    /** 钱包余额 */
    private BigDecimal walletBalance;

    /** 积分余额 */
    private BigDecimal integralBalance;

    /** 门店联系电话 */
    private String storePhone;

    /** 联系电话 */
    private String phoneNumber;

    /** 变动金额 */
    private BigDecimal variableAmount;

    /** 变动积分 */
    private BigDecimal variableIntegral;

    private Long tenantId;

    private Long deptId;

    /** 消费/绑定/退款的操作门店 */
    private String operateStores;

    /** 患者名称 */
    private String patientName;
}
