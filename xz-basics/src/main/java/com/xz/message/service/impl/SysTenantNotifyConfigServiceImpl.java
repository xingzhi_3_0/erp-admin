package com.xz.message.service.impl;

import java.util.List;
import com.xz.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.message.mapper.SysTenantNotifyConfigMapper;
import com.xz.message.domain.SysTenantNotifyConfig;
import com.xz.message.service.ISysTenantNotifyConfigService;

/**
 * 租户消息通知配置Service业务层处理
 *
 * @author xz
 * @date 2023-08-04
 */
@Service
public class SysTenantNotifyConfigServiceImpl  extends ServiceImpl<SysTenantNotifyConfigMapper, SysTenantNotifyConfig> implements ISysTenantNotifyConfigService
{
    @Autowired
    private SysTenantNotifyConfigMapper sysTenantNotifyConfigMapper;

    /**
     * 查询租户消息通知配置
     *
     * @param id 租户消息通知配置主键
     * @return 租户消息通知配置
     */
    @Override
    public SysTenantNotifyConfig selectSysTenantNotifyConfigById(Long id)
    {
        return sysTenantNotifyConfigMapper.selectSysTenantNotifyConfigById(id);
    }

    @Override
    public SysTenantNotifyConfig selectNotifyConfigByTenantId(Long tenantId) {
        return sysTenantNotifyConfigMapper.selectNotifyConfigByTenantId(tenantId);
    }

    /**
     * 查询租户消息通知配置列表
     *
     * @param sysTenantNotifyConfig 租户消息通知配置
     * @return 租户消息通知配置
     */
    @Override
    public List<SysTenantNotifyConfig> selectSysTenantNotifyConfigList(SysTenantNotifyConfig sysTenantNotifyConfig)
    {
        return sysTenantNotifyConfigMapper.selectSysTenantNotifyConfigList(sysTenantNotifyConfig);
    }

    /**
     * 新增租户消息通知配置
     *
     * @param sysTenantNotifyConfig 租户消息通知配置
     * @return 结果
     */
    @Override
    public int insertSysTenantNotifyConfig(SysTenantNotifyConfig sysTenantNotifyConfig)
    {
        sysTenantNotifyConfig.setCreateTime(DateUtils.getNowDate());
        return sysTenantNotifyConfigMapper.insertSysTenantNotifyConfig(sysTenantNotifyConfig);
    }

    /**
     * 修改租户消息通知配置
     *
     * @param sysTenantNotifyConfig 租户消息通知配置
     * @return 结果
     */
    @Override
    public int updateSysTenantNotifyConfig(SysTenantNotifyConfig sysTenantNotifyConfig)
    {
        sysTenantNotifyConfig.setUpdateTime(DateUtils.getNowDate());
        return sysTenantNotifyConfigMapper.updateSysTenantNotifyConfig(sysTenantNotifyConfig);
    }

    /**
     * 批量删除租户消息通知配置
     *
     * @param ids 需要删除的租户消息通知配置主键
     * @return 结果
     */
    @Override
    public int deleteSysTenantNotifyConfigByIds(Long[] ids)
    {
        return sysTenantNotifyConfigMapper.deleteSysTenantNotifyConfigByIds(ids);
    }

    /**
     * 删除租户消息通知配置信息
     *
     * @param id 租户消息通知配置主键
     * @return 结果
     */
    @Override
    public int deleteSysTenantNotifyConfigById(Long id)
    {
        return sysTenantNotifyConfigMapper.deleteSysTenantNotifyConfigById(id);
    }
}
