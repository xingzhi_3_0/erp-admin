package com.xz.message.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.entity.SysUser;
import com.xz.member.domain.MemberInfo;
import com.xz.message.domain.SysTenantNotifyItem;
import com.xz.message.vo.MemberMsgInfo;
import com.xz.message.vo.TempLateVO;
import com.xz.patient.domain.PatientInfo;
import com.xz.process.domain.ProcessOrder;

import java.util.List;
import java.util.Objects;

/**
 * 消息模板配置Service接口
 *
 * @author xz
 * @date 2023-08-08
 */
public interface ISysTenantNotifyItemService extends IService<SysTenantNotifyItem> {


	/**
     * 查询消息模板配置
     *
     * @return 消息模板配置
     */
    public TempLateVO selectSysTenantNotifyItemByTenantId(Long tenantId);

    /**
     * 根据类型获取指定的模板
     * @param msgType
     * @return
     */
    String selecContexttByType(Integer msgType);

    List<SysTenantNotifyItem> selectListByType();

    /**
     * 修改消息模板配置
     *
     * @param tempLateVO 消息模板配置
     * @return 结果
     */
    public boolean updateSysTenantNotifyItem(TempLateVO tempLateVO);

    /**
     * 会员余额变动通知
     * @param info
     * @param msgTypes
     */
    public void sendMemberInfoMsg(MemberMsgInfo info, Integer msgTypes, SysUser sysUser);

    public void sendListMemberInfoMsg(List<MemberMsgInfo> info, Integer msgTypes, SysUser sysUser);


    /**
     * 加工中心通知客户寄送镜框、成品已邮寄
     * @param processOrder
     * @param msgTypes
     */
    public void sendMachiningCenterMsg(ProcessOrder processOrder, Integer msgTypes, SysUser sysUser);
}
