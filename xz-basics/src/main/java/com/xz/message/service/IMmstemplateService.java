package com.xz.message.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.annotation.DataScope;
import com.xz.common.annotation.DataSource;
import com.xz.message.domain.Mmstemplate;

/**
 * 消息模板Service接口
 * 
 * @author xz
 * @date 2022-03-04
 */
public interface IMmstemplateService extends IService<Mmstemplate> {
    /**
     * 查询消息模板
     * 
     * @param id 消息模板主键
     * @return 消息模板
     */
    public Mmstemplate selectMmstemplateById(Long id);

    /**
     * 查询消息模板列表
     * 
     * @param mmstemplate 消息模板
     * @return 消息模板集合
     */
    @DataScope(deptAlias = "d")
    public List<Mmstemplate> selectMmstemplateList(Mmstemplate mmstemplate);

    /**
     * 新增消息模板
     * 
     * @param mmstemplate 消息模板
     * @return 结果
     */
    public int insertMmstemplate(Mmstemplate mmstemplate);

    /**
     * 修改消息模板
     * 
     * @param mmstemplate 消息模板
     * @return 结果
     */
    public int updateMmstemplate(Mmstemplate mmstemplate);

    /**
     * 批量删除消息模板
     * 
     * @param ids 需要删除的消息模板主键集合
     * @return 结果
     */
    public int deleteMmstemplateByIds(Long[] ids);

    /**
     * 删除消息模板信息
     * 
     * @param id 消息模板主键
     * @return 结果
     */
    public int deleteMmstemplateById(Long id);
}
