package com.xz.message.service.impl;

import com.xz.common.core.domain.entity.SysDept;
import com.xz.message.service.ISysTenantNotifyParamService;
import com.xz.message.vo.MemberMsgInfo;
import com.xz.patient.domain.PatientInfo;
import com.xz.patient.mapper.PatientInfoMapper;
import com.xz.process.domain.ProcessOrder;
import com.xz.system.service.ISysConfigService;
import com.xz.system.service.ISysDeptService;
import common.ECDateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
public class SysTenantNotifyParamServiceImpl implements ISysTenantNotifyParamService {

    @Autowired
    private ISysDeptService sysDeptService;
    @Autowired
    private ISysConfigService iSysConfigService;
    @Autowired
    private PatientInfoMapper patientInfoMapper;

    @Override
    public String replaceTplContent(String content, Map<String, String> param) {
        if (CollectionUtils.isEmpty(param)) {
            return content;
        }
        Set<String> keySet = param.keySet();
        for (String key : keySet) {
            content = content.replace("{" + key + "}", Optional.ofNullable(param.get(key)).orElse("--"));
        }
        return editContent(content);
    }

    /**
     * 编辑发送内容
     */
    private static String editContent(String content) {
        StringBuilder sub = new StringBuilder("");
        String editResult = content.replaceAll("\\s*", "").replaceAll("\u200B","");
        return sub.append(editResult).toString();
    }

    @Override
    public Map<String, String> getForMember(MemberMsgInfo info, Integer msgTypes) {
        Map<String, String> map = new HashMap<>();
        map.put("会员卡尾号", info.getMemberCardNo());
        map.put("门店联系电话", info.getStorePhone());
        if(null != info.getWalletBalance()){
            map.put("钱包余额", info.getWalletBalance().toString());
        }
        if(null != info.getIntegralBalance()){
            map.put("会员积分", info.getIntegralBalance().toString());
        }
        switch (msgTypes) {
            case 0://会员钱包充值
                map.put("充值门店", info.getOperateStores());
                if(null != info.getVariableAmount()){
                    map.put("充值金额", info.getVariableAmount().toString());
                }
                break;
            case 1://会员钱包消费
                map.put("消费门店", info.getOperateStores());
                if(null != info.getVariableAmount()){
                    map.put("消费金额", info.getVariableAmount().toString());
                }
                if(null != info.getVariableIntegral()){
                    map.put("消费积分", info.getVariableIntegral().toString());
                }
                break;
            case 2://会员钱包消费退款
                map.put("消费门店", info.getOperateStores());
                if(null != info.getVariableAmount()){
                    map.put("钱包退款金额", info.getVariableAmount().toString());
                }
                if(null != info.getVariableIntegral()){
                    map.put("消费积分", info.getVariableIntegral().toString());
                }
                break;
            case 3://会员钱包退款
                map.put("退款门店", info.getOperateStores());
                if(null != info.getVariableAmount()){
                    map.put("钱包退款金额", info.getVariableAmount().toString());
                }
                break;
            case 4://会员钱包绑定患者
                map.put("绑定门店", info.getOperateStores());
                map.put("患者姓名", info.getPatientName());
            case 5://会员钱包解绑患者
                map.put("解绑门店", info.getOperateStores());
                map.put("患者姓名", info.getPatientName());
                break;
            case 8://就诊关怀1
                map.put("患者姓名", info.getPatientName());
                map.put("建档门店", info.getOperateStores());
            case 9://就诊关怀2
                map.put("患者姓名", info.getPatientName());
                map.put("建档门店", info.getOperateStores());
                break;
            case 10://活动通知1
                map.put("患者姓名", info.getPatientName());
                map.put("建档门店", info.getOperateStores());
                break;
            case 11://活动通知2
                map.put("患者姓名", info.getPatientName());
                map.put("建档门店", info.getOperateStores());
                break;
        }
        return map;
    }

    @Override
    public Map<String, String> getForProcessOrder(ProcessOrder processOrder, Integer msgTypes) {
        Map<String, String> map = new HashMap<>();
        String patientName = processOrder.getPatientName();
        // 因为加工单里面存的患者名称是拼接的字符串，不符合要求，所以要优先从患者本身实体类中获取。
        PatientInfo patientInfo = patientInfoMapper.selectById(processOrder.getPatientId());
        if(null != patientInfo){
           patientName = patientInfo.getPatientName();
       }
        map.put("患者姓名", patientName);
        map.put("销售门店", processOrder.getSalesDeptName());
        map.put("下单日期", ECDateUtils.formatDate(processOrder.getChargeTime(),ECDateUtils.Format_Date2));
        SysDept dept = sysDeptService.selectDeptById(processOrder.getSalesDeptId());
        map.put("门店联系电话",dept.getPhone());
        switch (msgTypes) {
            case 6://通知客户寄送镜框
                String address = iSysConfigService.selectConfigByKey("machining_center_address");
                map.put("邮寄地址", address);
                break;
            case 7://成品邮寄
                map.put("快递单号", processOrder.getTrackingNumber());
                break;
        }
        return map;
    }


}
