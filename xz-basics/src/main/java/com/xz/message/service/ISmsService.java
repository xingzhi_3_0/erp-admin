package com.xz.message.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.message.domain.Sms;
import com.xz.store.domain.StoreBasic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 短信Service接口
 * 
 * @author xz
 * @date 2022-03-07
 */
public interface ISmsService extends IService<Sms> {
    /**
     * 查询短信
     * 
     * @param id 短信主键
     * @return 短信
     */
    public Sms selectSmsById(Long id);

    /**
     * 查询短信列表
     * 
     * @param sms 短信
     * @return 短信集合
     */
    public List<Sms> selectSmsList(Sms sms);

    /**
     * 新增短信
     * 
     * @param sms 短信
     * @return 结果
     */
    public int insertSms(Sms sms);

    /**
     * 修改短信
     * 
     * @param sms 短信
     * @return 结果
     */
    public int updateSms(Sms sms);

    /**
     * 批量删除短信
     * 
     * @param ids 需要删除的短信主键集合
     * @return 结果
     */
    public int deleteSmsByIds(Long[] ids);

    /**
     * 删除短信信息
     * 
     * @param id 短信主键
     * @return 结果
     */
    public int deleteSmsById(Long id);
    boolean sendSms(Sms sms);



    boolean sendBuinessMsgPostToStore(StoreBasic store, HashMap<String, String> param, int i, Long tenantId);
}
