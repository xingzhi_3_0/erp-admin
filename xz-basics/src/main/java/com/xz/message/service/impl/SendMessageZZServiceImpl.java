package com.xz.message.service.impl;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 
 */
@Service
@Slf4j
public class SendMessageZZServiceImpl extends SendMsgCommonInterfaceServiceImpl {



	// 简易形式发送短信 不需要登录退出 --参数 UserID Account Password SMSType Content Phones
//	public static final String DIRECT_SEND_SMSS = "http://www.lanz.net.cn/LANZGateway/DirectSendSMSs.asp";

	public static final String UID = "xzwl123456_2";
	public static final String PASSWORD = "123456";
	public static final String URL = "http://service.winic.org:8009/sys_port/gateway/index.asp";

	@Override
	public String doGet(String urlStr, String param) throws IOException {
		return null;
	}

	@Override
	public boolean sendMsgPost(String phone, String content, String tplId) throws Exception {
		boolean flag = false;// 发送成功标志
		String result = "";
		result = HTTP_POST("https://service.winic.org/sys_port/gateway/index.asp", "id=xzwl123456&pwd=123456xzwl&to="+phone+"&content=" + URLEncoder.encode("您好，您的验证码是"+content, "gb2312") + "&time=");
		System.out.println(result);
		if (result.contains("000")) {
			flag = true;
		}
		return flag;
	}

	@Override
	public String getPath() throws UnsupportedEncodingException {
		return null;
	}

	/**
	 * 替换短信模板中的变量内容
	 */
	public String getSmsTemplateTypeContent(String emplateContent , String content) {
		String replaceStr = emplateContent.substring(emplateContent.indexOf("["), emplateContent.lastIndexOf("]") + 1);
		if (content != null && !"".equals(content)) {
			emplateContent = emplateContent.replace(replaceStr, content);
		}
		return emplateContent;
	}

	/**
	 * 获取用户信息
	 */
	public static Map<String, String> getAdminMessage() {
		// 定义userInfo存储账户信息
		Map<String, String> userInfo = new HashMap<String, String>();
		String userId = "";
		String account = "";
		String password = "";
		InputStream in = SendMessageZZServiceImpl.class.getResourceAsStream("/message.properties");
		Properties prop = new Properties();
		try {
			prop.load(in);
			userId = prop.getProperty("UserID").trim();
			account = prop.getProperty("Account").trim();
			password = prop.getProperty("Password").trim();

			userInfo.put("UserId", userId);
			userInfo.put("Account", account);
			userInfo.put("Password", password);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return userInfo;
	}

	/**
	 * 编辑发送内容
	 */
	public static String editContent(String content) {
		String result = "";
		StringBuffer sub = new StringBuffer();
		String editResult = content.replaceAll("\\s*", "");
		result = sub.append(editResult).toString();
		return result;
	}



	@Override
	public boolean sendMsgPost(String phone, String content, String tplId, String type) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}



	public String formatDate(Date date, String format) {
		String s = "";
		if (date != null) {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			s = sdf.format(date);
		}

		return s;
	}


	@Override
	public void setSingleSms(String appId, String secretKey, String host, String algorithm, String content,
			String customSmsId, String extendCode, String mobile, boolean isGzip, String encode) {
		// TODO Auto-generated method stub
		
	}

	public boolean sendBuinessMsgPost(String phone, String content, String  sms_id, String sms_psw) throws Exception {
		boolean flag = false;// 发送成功标志
		String result = "";
		result = HTTP_POST("https://service.winic.org/sys_port/gateway/index.asp", "id="+sms_id+"&pwd="+sms_psw+"&to="+phone+"&content=" + URLEncoder.encode(content, "gb2312") + "&time="+ formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
		System.out.println(result);
		if (result.contains("000")) {
			flag = true;
		}
		return flag;
	}


	@Override
	public boolean sendMakeAnAppointmentMsgPost(String phone, String content) throws Exception {
		if (StringUtils.isNotBlank(phone)) {
			String result = doPost(URL, "id=" + UID + "&pwd=" + PASSWORD + "&to=" + phone + "&content="
					+ URLEncoder.encode(content, "gb2312") + "&time=" + formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			log.info(result);
			if (result.contains("000")) {
				return true;
			}
		}
		return false;
	}
	public static String HTTP_POST(String URL, String Data) throws Exception {
		BufferedReader In = null;
		PrintWriter Out = null;
		HttpURLConnection HttpConn = null;
		try {
			java.net.URL url=new URL(URL);
			HttpConn=(HttpURLConnection)url.openConnection();
			HttpConn.setRequestMethod("POST");
			HttpConn.setDoInput(true);
			HttpConn.setDoOutput(true);

			Out=new PrintWriter(HttpConn.getOutputStream());
			Out.println(Data);
			Out.flush();

			if(HttpConn.getResponseCode() == HttpURLConnection.HTTP_OK){
				StringBuffer content = new StringBuffer();
				String tempStr = "";
				In = new BufferedReader(new InputStreamReader(HttpConn.getInputStream()));
				while((tempStr = In.readLine()) != null){
					content.append(tempStr);
				}
				In.close();
				return content.toString();
			}
			else
			{
				throw new Exception("HTTP_POST_ERROR_RETURN_STATUS：" + HttpConn.getResponseCode());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			Out.close();
			HttpConn.disconnect();
		}
		return null;
	}

	public static void main(String[] args) throws Exception{
		String HTTP_BACK_MESSAGE = "";
		HTTP_BACK_MESSAGE = HTTP_POST("https://service.winic.org/sys_port/gateway/index.asp", "id=xzwl123456&pwd=123456xzwl&to=13892006829&content=" + URLEncoder.encode("您好，您的验证码是884192【中正云通信】", "gb2312") + "&time=");
		System.out.println(HTTP_BACK_MESSAGE);
	}
}
