package com.xz.message.service.impl;

import java.util.List;

import com.xz.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.message.mapper.MmstemplateMapper;
import com.xz.message.domain.Mmstemplate;
import com.xz.message.service.IMmstemplateService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 消息模板Service业务层处理
 *
 * @author xz
 * @date 2022-03-04
 */
@Service
public class MmstemplateServiceImpl extends ServiceImpl<MmstemplateMapper, Mmstemplate> implements IMmstemplateService {
    @Autowired
    private MmstemplateMapper mmstemplateMapper;

    /**
     * 查询消息模板
     *
     * @param id 消息模板主键
     * @return 消息模板
     */
    @Override
    public Mmstemplate selectMmstemplateById(Long id) {
        return mmstemplateMapper.selectMmstemplateById(id);
    }

    /**
     * 查询消息模板列表
     *
     * @param mmstemplate 消息模板
     * @return 消息模板
     */
    @Override
    public List<Mmstemplate> selectMmstemplateList(Mmstemplate mmstemplate) {
        return mmstemplateMapper.selectMmstemplateList(mmstemplate);
    }

    /**
     * 新增消息模板
     *
     * @param mmstemplate 消息模板
     * @return 结果
     */
   @Transactional
    public int insertMmstemplate(Mmstemplate mmstemplate) {
       //订单通知
       List<Mmstemplate> newOrderList = mmstemplate.getNewOrderList();

       newOrderList.stream().forEach(item->{
           mmstemplateOrder(item,1);
       });

       //业务员通知
       List<Mmstemplate> salespersonNoticeList = mmstemplate.getSalespersonNoticeList();
       salespersonNoticeList.stream().forEach(item->{
           mmstemplateOrder(item,2);
       });
       //用户取车通知
       List<Mmstemplate> userPickupNotificationList = mmstemplate.getUserPickupNotificationList();
       userPickupNotificationList.stream().forEach(item->{
           mmstemplateOrder(item,3);
       });
       //用户取车通知
       List<Mmstemplate> gpsAlarmNotificationList = mmstemplate.getGpsAlarmNotificationList();
       gpsAlarmNotificationList.stream().forEach(item->{
           mmstemplateOrder(item,4);
       });
        return 1;
    }

    public void mmstemplateOrder(Mmstemplate item,Integer type){
        Mmstemplate mmstemplateOrder=null;
        if(item.getId()==null){
            mmstemplateOrder=new Mmstemplate();
            mmstemplateOrder.setStatus(item.getStatus());
            mmstemplateOrder.setPushStaff(item.getPushStaff());
            mmstemplateOrder.setPushStaffName(item.getPushStaffName());
            mmstemplateOrder.setStoreId(item.getStoreId());
            mmstemplateOrder.setStoreName(item.getStoreName());
            mmstemplateOrder.setCreateTime(DateUtils.getNowDate());
            mmstemplateOrder.setCreateBy(SecurityUtils.getUserId());
            mmstemplateOrder.setUpdateTime(DateUtils.getNowDate());
            mmstemplateOrder.setUpdateBy(SecurityUtils.getUserId());
            if(type==1){
                mmstemplateOrder.setContent("您有一笔新订单！客户:{memberName} 已预定：{modelName}，请及时处理。");
                mmstemplateOrder.setParam("memberName,modelName");
            }else if(type==2){
                mmstemplateOrder.setContent("已为您分配新订单：{orderNo}，请及时处理！");
                mmstemplateOrder.setParam("orderNo");
            }else if(type==3){
                mmstemplateOrder.setContent("尊敬的用户：您有一笔租车订单({orderNo})已临近取车时间，请及时前往{takeAddress}取车。友情提示：请携带本人身份证、驾驶证前往。/尊敬的用户：您有一笔租车订单({orderNo})已临近用车时间，业务员将为您送车上门。");
                mmstemplateOrder.setParam("orderNo,takeAddress/orderNo");
            }else if(type==4){
                mmstemplateOrder.setContent("车辆{carNo}绑定的设备{imei}触发{alarmName}，定位地点为：{address}。关联用车中订单{orderNo}，客户为{customerInfo}。");
                mmstemplateOrder.setParam("carNo,imei,alarmName,address,orderNo,customerInfo");
            }

            mmstemplateOrder.setTenantId(SecurityUtils.getLoginUser().getTenantId());
            mmstemplateOrder.setPushNode(type);
            mmstemplateOrder.setIsAuthorization(2);
            baseMapper.insertMmstemplate(mmstemplateOrder);
        }else{
            mmstemplateOrder = baseMapper.selectMmstemplateById(item.getId());
            mmstemplateOrder.setStatus(item.getStatus());
            mmstemplateOrder.setPushStaff(item.getPushStaff());
            mmstemplateOrder.setPushStaffName(item.getPushStaffName());
            mmstemplateOrder.setAlarmType(item.getAlarmType());
            mmstemplateOrder.setAlarmName(item.getAlarmName());
            baseMapper.updateMmstemplate(mmstemplateOrder);
        }

    }
    /**
     * 修改消息模板
     *
     * @param mmstemplate 消息模板
     * @return 结果
     */
    @Override
    public int updateMmstemplate(Mmstemplate mmstemplate) {
        mmstemplate.setUpdateTime(DateUtils.getNowDate());
        return mmstemplateMapper.updateMmstemplate(mmstemplate);
    }

    /**
     * 批量删除消息模板
     *
     * @param ids 需要删除的消息模板主键
     * @return 结果
     */
    @Override
    public int deleteMmstemplateByIds(Long[] ids) {
        return mmstemplateMapper.deleteMmstemplateByIds(ids);
    }

    /**
     * 删除消息模板信息
     *
     * @param id 消息模板主键
     * @return 结果
     */
    @Override
    public int deleteMmstemplateById(Long id) {
        return mmstemplateMapper.deleteMmstemplateById(id);
    }
}
