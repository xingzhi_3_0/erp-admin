package com.xz.message.service.impl;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xz.common.core.domain.entity.SysUser;
import com.xz.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.message.domain.Mmstemplate;
import com.xz.message.mapper.MmstemplateMapper;
import com.xz.message.service.SendMsgCommonInterfaceService;
import com.xz.store.domain.StoreBasic;
import com.xz.system.domain.SysTenantConfig;
import com.xz.system.mapper.SysTenantMapper;
import com.xz.system.service.ISysUserService;
import common.ECDateUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.message.mapper.SmsMapper;
import com.xz.message.domain.Sms;
import com.xz.message.service.ISmsService;

import javax.annotation.Resource;

/**
 * 短信Service业务层处理
 *
 * @author xz
 * @date 2022-03-07
 */
@Service
public class SmsServiceImpl extends ServiceImpl<SmsMapper, Sms> implements ISmsService {
    @Autowired
    private SmsMapper smsMapper;
    @Autowired
    private SysTenantMapper sysTenantMapper;
    @Autowired
    private MmstemplateMapper mmstemplateMapper;
    @Autowired
    private ISysUserService sysUserService;
    @Resource
    private SendMsgCommonInterfaceService sendMessageZZService;

    @Override
    public boolean sendSms(Sms sms) {
        return false;
    }

    /**
     * 查询短信
     *
     * @param id 短信主键
     * @return 短信
     */
    @Override
    public Sms selectSmsById(Long id) {
        return smsMapper.selectSmsById(id);
    }

    /**
     * 查询短信列表
     *
     * @param sms 短信
     * @return 短信
     */
    @Override
    public List<Sms> selectSmsList(Sms sms) {
        return smsMapper.selectSmsList(sms);
    }

    /**
     * 新增短信
     *
     * @param sms 短信
     * @return 结果
     */
    @Override
    public int insertSms(Sms sms) {
        sms.setCreateTime(DateUtils.getNowDate());
        return smsMapper.insertSms(sms);
    }

    /**
     * 修改短信
     *
     * @param sms 短信
     * @return 结果
     */
    @Override
    public int updateSms(Sms sms) {
        sms.setUpdateTime(DateUtils.getNowDate());
        return smsMapper.updateSms(sms);
    }

    /**
     * 批量删除短信
     *
     * @param ids 需要删除的短信主键
     * @return 结果
     */
    @Override
    public int deleteSmsByIds(Long[] ids) {
        return smsMapper.deleteSmsByIds(ids);
    }

    /**
     * 删除短信信息
     *
     * @param id 短信主键
     * @return 结果
     */
    @Override
    public int deleteSmsById(Long id) {
        return smsMapper.deleteSmsById(id);
    }




    @Override
    public boolean sendBuinessMsgPostToStore(StoreBasic store, HashMap<String, String> param, int pushNode, Long tenantId) {
        //短信模板
        Mmstemplate mmstemplate = mmstemplateMapper.selectMmstemplateByPushNode(pushNode,null);
        if(mmstemplate==null||mmstemplate.getStatus()==0){
            return false;
        }
        //租户信息
        SysTenantConfig sysTenantConfig = sysTenantMapper.selectSysTenantConfigById(tenantId);

        try {
            // 创建请求连接并处理文本内容
            String emplateContent = mmstemplate.getContent();
            String sendMsg = editContent(getSmsTemplateTypeContent(emplateContent, param));
            return sendMessageZZService.sendBuinessMsgPost(store.getContactPhone(), sendMsg, sysTenantConfig.getSmsId(), sysTenantConfig.getSmsPsw());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    /**
     * 编辑发送内容
     */
    public static String editContent(String content) {
        String result = "";
        StringBuffer sub = new StringBuffer();
        String editResult = content.replaceAll("\\s*", "");
        result = sub.append(editResult).toString();
        return result;
    }

    /**
     * 替换短信模板中的变量内容
     */
    public String getSmsTemplateTypeContent(String emplateContent, Map<String,String> param) {
        if(param==null){
            return emplateContent;
        }
        Set<String> keySet = param.keySet();
        for (String key:keySet){
            emplateContent = emplateContent.replace("{"+key+"}", param.get(key));
        }

        return emplateContent;
    }
    //插入短信记录
    public void insertSms(String content,String id,String name,Long tenantId,Integer type){
        Sms sms=new Sms();
        sms.setContent(content);
        sms.setStatus("0");
        sms.setSendTime(new Date());
        sms.setCustomerId(id);
        sms.setCustomerName(name);
        sms.setTenantId(tenantId);
        sms.setPushNode(type);
        baseMapper.insertSms(sms);
    }
}
