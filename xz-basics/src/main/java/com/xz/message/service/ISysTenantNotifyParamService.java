package com.xz.message.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.message.domain.SysTenantNotifyItem;
import com.xz.message.vo.MemberMsgInfo;
import com.xz.patient.domain.PatientInfo;
import com.xz.process.domain.ProcessOrder;

import java.util.Map;


public interface ISysTenantNotifyParamService {

    /**
     * 替换模板内容
     * @param content
     * @param param
     * @return
     */
    public String replaceTplContent(String content, Map<String, String> param);
    /**
     * 会员营销/钱包·相关短信参数处理
     * @param info
     * @param msgTypes
     * @return
     */
    Map<String, String> getForMember(MemberMsgInfo info, Integer msgTypes);

    /**
     * 加工单·相关短信参数处理
     * @param processOrder
     * @param msgTypes
     * @return
     */
    Map<String, String> getForProcessOrder(ProcessOrder processOrder, Integer msgTypes);

}
