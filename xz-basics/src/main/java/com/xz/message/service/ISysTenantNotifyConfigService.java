package com.xz.message.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.message.domain.SysTenantNotifyConfig;

/**
 * 租户消息通知配置Service接口
 *
 * @author xz
 * @date 2023-08-04
 */
public interface ISysTenantNotifyConfigService extends IService<SysTenantNotifyConfig> {
    /**
     * 查询租户消息通知配置
     *
     * @param id 租户消息通知配置主键
     * @return 租户消息通知配置
     */
    public SysTenantNotifyConfig selectSysTenantNotifyConfigById(Long id);
    public SysTenantNotifyConfig selectNotifyConfigByTenantId(Long tenantId);

    /**
     * 查询租户消息通知配置列表
     *
     * @param sysTenantNotifyConfig 租户消息通知配置
     * @return 租户消息通知配置集合
     */
    public List<SysTenantNotifyConfig> selectSysTenantNotifyConfigList(SysTenantNotifyConfig sysTenantNotifyConfig);

    /**
     * 新增租户消息通知配置
     *
     * @param sysTenantNotifyConfig 租户消息通知配置
     * @return 结果
     */
    public int insertSysTenantNotifyConfig(SysTenantNotifyConfig sysTenantNotifyConfig);

    /**
     * 修改租户消息通知配置
     *
     * @param sysTenantNotifyConfig 租户消息通知配置
     * @return 结果
     */
    public int updateSysTenantNotifyConfig(SysTenantNotifyConfig sysTenantNotifyConfig);

    /**
     * 批量删除租户消息通知配置
     *
     * @param ids 需要删除的租户消息通知配置主键集合
     * @return 结果
     */
    public int deleteSysTenantNotifyConfigByIds(Long[] ids);

    /**
     * 删除租户消息通知配置信息
     *
     * @param id 租户消息通知配置主键
     * @return 结果
     */
    public int deleteSysTenantNotifyConfigById(Long id);
}
