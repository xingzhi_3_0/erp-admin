package com.xz.message.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.message.domain.Mmstemplate;
import org.apache.ibatis.annotations.Param;

/**
 * 消息模板Mapper接口
 * 
 * @author xz
 * @date 2022-03-04
 */
@Mapper
public interface MmstemplateMapper  extends BaseMapper<Mmstemplate> {
    /**
     * 查询消息模板
     * 
     * @param id 消息模板主键
     * @return 消息模板
     */
    public Mmstemplate selectMmstemplateById(Long id);
    public Mmstemplate selectMmstemplateByPushNode(@Param("pushNode")  Integer pushNode,@Param("storeId") String storeId);

    /**
     * 查询消息模板列表
     * 
     * @param mmstemplate 消息模板
     * @return 消息模板集合
     */
    public List<Mmstemplate> selectMmstemplateList(Mmstemplate mmstemplate);

    /**
     * 新增消息模板
     * 
     * @param mmstemplate 消息模板
     * @return 结果
     */
    public int insertMmstemplate(Mmstemplate mmstemplate);

    /**
     * 修改消息模板
     * 
     * @param mmstemplate 消息模板
     * @return 结果
     */
    public int updateMmstemplate(Mmstemplate mmstemplate);

    /**
     * 删除消息模板
     * 
     * @param id 消息模板主键
     * @return 结果
     */
    public int deleteMmstemplateById(Long id);

    /**
     * 批量删除消息模板
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMmstemplateByIds(Long[] ids);
}
