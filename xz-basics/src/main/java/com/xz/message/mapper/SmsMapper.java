package com.xz.message.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.message.domain.Sms;

/**
 * 短信Mapper接口
 * 
 * @author xz
 * @date 2022-03-07
 */
@Mapper
public interface SmsMapper  extends BaseMapper<Sms> {
    /**
     * 查询短信
     * 
     * @param id 短信主键
     * @return 短信
     */
    public Sms selectSmsById(Long id);

    /**
     * 查询短信列表
     * 
     * @param sms 短信
     * @return 短信集合
     */
    public List<Sms> selectSmsList(Sms sms);

    /**
     * 新增短信
     * 
     * @param sms 短信
     * @return 结果
     */
    public int insertSms(Sms sms);

    /**
     * 修改短信
     * 
     * @param sms 短信
     * @return 结果
     */
    public int updateSms(Sms sms);

    /**
     * 删除短信
     * 
     * @param id 短信主键
     * @return 结果
     */
    public int deleteSmsById(Long id);

    /**
     * 批量删除短信
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSmsByIds(Long[] ids);
}
