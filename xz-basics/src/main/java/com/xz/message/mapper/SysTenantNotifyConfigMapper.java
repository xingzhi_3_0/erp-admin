package com.xz.message.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.message.domain.SysTenantNotifyConfig;

/**
 * 租户消息通知配置Mapper接口
 *
 * @author xz
 * @date 2023-08-04
 */
@Mapper
public interface SysTenantNotifyConfigMapper  extends BaseMapper<SysTenantNotifyConfig> {
    /**
     * 查询租户消息通知配置
     *
     * @param id 租户消息通知配置主键
     * @return 租户消息通知配置
     */
    public SysTenantNotifyConfig selectSysTenantNotifyConfigById(Long id);

    /**
     * 查询租户消息通知配置列表
     *
     * @param sysTenantNotifyConfig 租户消息通知配置
     * @return 租户消息通知配置集合
     */
    public List<SysTenantNotifyConfig> selectSysTenantNotifyConfigList(SysTenantNotifyConfig sysTenantNotifyConfig);

    /**
     * 新增租户消息通知配置
     *
     * @param sysTenantNotifyConfig 租户消息通知配置
     * @return 结果
     */
    public int insertSysTenantNotifyConfig(SysTenantNotifyConfig sysTenantNotifyConfig);

    /**
     * 修改租户消息通知配置
     *
     * @param sysTenantNotifyConfig 租户消息通知配置
     * @return 结果
     */
    public int updateSysTenantNotifyConfig(SysTenantNotifyConfig sysTenantNotifyConfig);

    /**
     * 删除租户消息通知配置
     *
     * @param id 租户消息通知配置主键
     * @return 结果
     */
    public int deleteSysTenantNotifyConfigById(Long id);

    /**
     * 批量删除租户消息通知配置
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysTenantNotifyConfigByIds(Long[] ids);

    SysTenantNotifyConfig selectNotifyConfigByTenantId(Long tenantId);
}
