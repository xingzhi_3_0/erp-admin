package com.xz.message.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.store.domain.StoreBasic;
import org.apache.ibatis.annotations.Mapper;
import com.xz.message.domain.SysTenantNotifyItem;
import org.apache.ibatis.annotations.Param;

/**
 * 消息模板配置Mapper接口
 *
 * @author xz
 * @date 2023-08-08
 */
@Mapper
public interface SysTenantNotifyItemMapper  extends BaseMapper<SysTenantNotifyItem> {

    /**
     * 查询消息模板配置列表
     *
     * @param sysTenantNotifyItem 消息模板配置
     * @return 消息模板配置集合
     */
    public List<SysTenantNotifyItem> selectSysTenantNotifyItemList(SysTenantNotifyItem sysTenantNotifyItem);

    public List<SysTenantNotifyItem> selectListByType(@Param("typeList") List<Integer> typeList);


    /**
     * 新增消息模板配置
     *
     * @param sysTenantNotifyItem 消息模板配置
     * @return 结果
     */
    public int insertSysTenantNotifyItem(SysTenantNotifyItem sysTenantNotifyItem);


    /**
     * 删除消息模板配置
     *
     * @param tenantId 消息模板配置主键
     * @return 结果
     */
    public int deleteSysTenantNotifyItemByTenantId(Long tenantId);


    public int insertItemByStoreId(StoreBasic storeBasic);
    public int updateItemByStoreId(StoreBasic storeBasic);
    public int deleteItemByStoreId(String storeId);

}
