package com.xz.staging.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 销售框架镜片工作台统计详情列表
 */
@Data
public class FrameLensesRangeVo {

    @Excel(name = "门店")
    private String deptName;
    @Excel
    private Long patientId;
    @Excel(name = "患者")
    private String patientInfo;
    @Excel(name = "销售单号")
    private String salesNo;
    @Excel(name = "分类")
    private String productCategory;
    @Excel(name = "商品名称")
    private String productName;
    @Excel(name = "售价")
    private BigDecimal sellingPrice;
    @Excel(name = "实收(小计)")
    private BigDecimal subtotal;
    @Excel(name = "销售员")
    private String salesUserName;


}