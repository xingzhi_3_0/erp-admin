package com.xz.staging.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 销售工作台统计
 */
@Data
public class WorkbenchesVo {

    private Integer bizType;

    private BigDecimal todaySum;

    private BigDecimal currMonthSum;

    private BigDecimal lastMonthSum;

    private BigDecimal avgDayMonth;

    private String productName;

    private Integer salesNum;

    private BigDecimal salesAmount;
}