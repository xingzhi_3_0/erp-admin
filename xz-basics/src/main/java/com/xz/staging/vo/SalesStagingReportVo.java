package com.xz.staging.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 销售报表统计类
 */
@Data
public class SalesStagingReportVo extends BaseEntity {

    private String productCategory;

    private String productName;

    private BigDecimal sellingPrice;

    private BigDecimal returnNum;

    private BigDecimal returnNumTotal;
    private BigDecimal returnNumRatio;

    private BigDecimal returnAmount;

    private BigDecimal returnAmountTotal;

    private BigDecimal returnAmountRatio;


    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;
    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

    private Long categoryId;

    private Long deptId;

    private String queryDeptIdListStr;
}