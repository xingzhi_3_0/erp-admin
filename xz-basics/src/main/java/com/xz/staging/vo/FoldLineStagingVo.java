package com.xz.staging.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 销售工作台统计
 */
@Data
public class FoldLineStagingVo {

    /** 部门名称 **/
    private Long deptId;
    /** 部门名称 **/
    private String deptName;
    /** 患者人数 **/
    private BigDecimal patientCount;
    /** 部门月总金额 **/
    private BigDecimal deptMonthAmount;
    /** 部门月客单价 **/
    private BigDecimal customerUnitPrice;

    /**  **/
    private Integer diagnosisNum;
    /**  **/
    private BigDecimal monthAmount;
    /**  **/
    private String xValue;
    /**  **/
    private BigDecimal yValue;
}