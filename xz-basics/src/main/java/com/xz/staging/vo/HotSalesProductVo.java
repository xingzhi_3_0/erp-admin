package com.xz.staging.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 销售工作台统计
 */
@Data
public class HotSalesProductVo {

    private String productName;

    private Integer salesNum;

    private BigDecimal salesAmount;


}