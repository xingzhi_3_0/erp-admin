package com.xz.staging.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 销售框架镜片工作台统计
 */
@Data
public class FrameLensesStagingVo {

    private Long salesId;

    private String priceRange;

    private Integer orderNum;

    private BigDecimal priceRangeAmount;

    private Double orderSellingPrice;

    private BigDecimal orderSubtotal;
}