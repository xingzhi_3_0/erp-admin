package com.xz.staging.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.report.param.SalesReportParam;
import com.xz.report.vo.SalesFlowReportVo;
import com.xz.report.vo.SalesReportVo;
import com.xz.report.vo.SalesReturnReportVo;
import com.xz.staging.vo.FoldLineStagingVo;
import com.xz.staging.vo.FrameLensesRangeVo;
import com.xz.staging.vo.FrameLensesStagingVo;
import com.xz.staging.vo.WorkbenchesVo;

import java.util.List;

/**
 * 销售单换货Service接口
 * 
 * @author xz
 * @date 2024-2-19 11:13:40
 */
public interface ISalesStagingService extends IService {

    List<WorkbenchesVo> queryData(SalesReportParam salesReportParam);

    List<WorkbenchesVo> hotSalesProduct(SalesReportParam salesReportParam);

    List<FoldLineStagingVo> foldLineStatistics(SalesReportParam salesReportParam);

    List<FoldLineStagingVo> foldLineMonthList(SalesReportParam salesReportParam);

    List<FrameLensesStagingVo> frameLensesList(SalesReportParam salesReportParam);

    List<FrameLensesStagingVo> findFrameLensesList(SalesReportParam salesReportParam);

    List<FrameLensesRangeVo> frameLensesRangList(SalesReportParam salesReportParam);

}
