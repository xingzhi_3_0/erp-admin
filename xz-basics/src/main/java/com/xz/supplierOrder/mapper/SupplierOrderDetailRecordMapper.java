package com.xz.supplierOrder.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.supplierOrder.domain.SupplierOrderDetailRecord;

/**
 * 贸易中间商订单商品出入库记录Mapper接口
 *
 * @author xz
 * @date 2024-03-04
 */
@Mapper
public interface SupplierOrderDetailRecordMapper  extends BaseMapper<SupplierOrderDetailRecord> {
    /**
     * 查询贸易中间商订单商品出入库记录
     *
     * @param id 贸易中间商订单商品出入库记录主键
     * @return 贸易中间商订单商品出入库记录
     */
    public SupplierOrderDetailRecord selectSupplierOrderDetailRecordById(Long id);

    /**
     * 查询贸易中间商订单商品出入库记录列表
     *
     * @param supplierOrderDetailRecord 贸易中间商订单商品出入库记录
     * @return 贸易中间商订单商品出入库记录集合
     */
    public List<SupplierOrderDetailRecord> selectSupplierOrderDetailRecordList(SupplierOrderDetailRecord supplierOrderDetailRecord);

    /**
     * 新增贸易中间商订单商品出入库记录
     *
     * @param supplierOrderDetailRecord 贸易中间商订单商品出入库记录
     * @return 结果
     */
    public int insertSupplierOrderDetailRecord(SupplierOrderDetailRecord supplierOrderDetailRecord);

    /**
     * 修改贸易中间商订单商品出入库记录
     *
     * @param supplierOrderDetailRecord 贸易中间商订单商品出入库记录
     * @return 结果
     */
    public int updateSupplierOrderDetailRecord(SupplierOrderDetailRecord supplierOrderDetailRecord);

    /**
     * 删除贸易中间商订单商品出入库记录
     *
     * @param id 贸易中间商订单商品出入库记录主键
     * @return 结果
     */
    public int deleteSupplierOrderDetailRecordById(Long id);

    /**
     * 批量删除贸易中间商订单商品出入库记录
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSupplierOrderDetailRecordByIds(Long[] ids);
}
