package com.xz.supplierOrder.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.supplierOrder.domain.SupplierOrder;

/**
 * 贸易中间商订单Mapper接口
 *
 * @author xz
 * @date 2024-02-27
 */
@Mapper
public interface SupplierOrderMapper  extends BaseMapper<SupplierOrder> {
    /**
     * 查询贸易中间商订单
     *
     * @param id 贸易中间商订单主键
     * @return 贸易中间商订单
     */
    public SupplierOrder selectSupplierOrderById(Long id);

    /**
     * 查询贸易中间商订单列表
     *
     * @param supplierOrder 贸易中间商订单
     * @return 贸易中间商订单集合
     */
    public List<SupplierOrder> selectSupplierOrderList(SupplierOrder supplierOrder);

    /**
     * 新增贸易中间商订单
     *
     * @param supplierOrder 贸易中间商订单
     * @return 结果
     */
    public int insertSupplierOrder(SupplierOrder supplierOrder);

    /**
     * 修改贸易中间商订单
     *
     * @param supplierOrder 贸易中间商订单
     * @return 结果
     */
    public int updateSupplierOrder(SupplierOrder supplierOrder);

    /**
     * 删除贸易中间商订单
     *
     * @param id 贸易中间商订单主键
     * @return 结果
     */
    public int deleteSupplierOrderById(Long id);

    /**
     * 批量删除贸易中间商订单
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSupplierOrderByIds(Long[] ids);
}
