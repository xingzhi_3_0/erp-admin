package com.xz.supplierOrder.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.supplierOrder.vo.SupplierOrderDetailFlowReportVo;
import com.xz.supplierOrder.vo.SupplierOrderDetailFlowSummaryVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.supplierOrder.domain.SupplierOrderDetail;

/**
 * 贸易中间商订单商品Mapper接口
 *
 * @author xz
 * @date 2024-02-27
 */
@Mapper
public interface SupplierOrderDetailMapper  extends BaseMapper<SupplierOrderDetail> {
    /**
     * 查询贸易中间商订单商品
     *
     * @param id 贸易中间商订单商品主键
     * @return 贸易中间商订单商品
     */
    public SupplierOrderDetail selectSupplierOrderDetailById(Long id);

    /**
     * 查询贸易中间商订单商品列表
     *
     * @param supplierOrderDetail 贸易中间商订单商品
     * @return 贸易中间商订单商品集合
     */
    public List<SupplierOrderDetail> selectSupplierOrderDetailList(SupplierOrderDetail supplierOrderDetail);

    /**
     * 流水报表
     * @param supplierOrderDetail
     * @return
     */
    public List<SupplierOrderDetailFlowReportVo> selectReport(SupplierOrderDetail supplierOrderDetail);

    /**
     * 汇总报表
     * @param supplierOrderDetail
     * @return
     */
    public List<SupplierOrderDetailFlowSummaryVo> selectSummary(SupplierOrderDetail supplierOrderDetail);
    /**
     * 汇总报表
     * @param supplierOrderDetail
     * @return
     */
    public Integer selectSummaryQuantity(SupplierOrderDetail supplierOrderDetail);

    /**
     * 新增贸易中间商订单商品
     *
     * @param supplierOrderDetail 贸易中间商订单商品
     * @return 结果
     */
    public int insertSupplierOrderDetail(SupplierOrderDetail supplierOrderDetail);

    /**
     * 修改贸易中间商订单商品
     *
     * @param supplierOrderDetail 贸易中间商订单商品
     * @return 结果
     */
    public int updateSupplierOrderDetail(SupplierOrderDetail supplierOrderDetail);

    /**
     * 删除贸易中间商订单商品
     *
     * @param id 贸易中间商订单商品主键
     * @return 结果
     */
    public int deleteSupplierOrderDetailById(Long id);

    /**
     * 批量删除贸易中间商订单商品
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSupplierOrderDetailByIds(Long[] ids);
}
