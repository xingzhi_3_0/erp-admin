package com.xz.supplierOrder.enums;

/**
 * @author Administrator
 */

public enum SupplierOrderStatusEnum {

  //订单状态：1-待提交(待发货，待退货，待换货),2-已提交(已发货，已退货，已换货),3-已冲红,4-已作废,5-已收货待换货,6-已换货待收货
  /** 1-待发货*/
  TO_BE_DELIVERY(1,"待提交"),
  /** 2-已提交*/
  SUBMITTED(2,"已提交"),
  /** 5-已冲红*/
  REVERSALED(3,"已冲红"),
  /** 4-已作废*/
  NULLIFIED(4,"已作废"),
  /** 5-已收货待换货(先退后换)*/
  RECEIVED_AWAITING_EXCHANGE(5,"已收货待换货"),
  /** 6-已换货待收货(先换后退)*/
  EXCHANGE_AWAITING_RECEIPT(6,"已换货待收货"),
  ;
	private final int status;

	private final String name;

	SupplierOrderStatusEnum(int status, String name) {
		this.status = status;
		this.name = name;
	}

	public int getStatus() {
		return status;
	}

	public String getName() {
		return name;
	}

  public boolean eq(Integer status) {
    if(null != status && status.equals(getStatus())){
      return true;
    }
    return false;
  }

  public static SupplierOrderTypeEnum valOf(Integer type) {
    if (null != type) {
      for (SupplierOrderTypeEnum enu : SupplierOrderTypeEnum.values()) {
        if (enu.getType().equals(type)) {
          return enu;
        }
      }
    }
    return null;
  }

  public static String nameOf(Integer status) {
    SupplierOrderTypeEnum enu = valOf(status);
    return null == enu ? null : enu.getName();
  }
}
