package com.xz.supplierOrder.enums;

/**
 * @author Administrator
 */

public enum SupplierOrderExchangeMethodEnum {

  /** 0-无*/
  DEFAULT(0,"无"),
  /** 1-先退货后换货*/
  RETURN_FIRST(1,"先退货后换货"),
  /** 2-先换货后退货*/
  EXCHANGE_FIRST(2,"先换货后退货"),
  ;
	private final Integer type;

	private final String name;

	SupplierOrderExchangeMethodEnum(Integer type, String name) {
		this.type = type;
		this.name = name;
	}

	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}

  public boolean eq(Integer type) {
    if(null != type && type.equals(getType())){
      return true;
    }
    return false;
  }

  public static SupplierOrderTypeEnum valOf(Integer type) {
    if (null != type) {
      for (SupplierOrderTypeEnum enu : SupplierOrderTypeEnum.values()) {
        if (enu.getType().equals(type)) {
          return enu;
        }
      }
    }
    return null;
  }

  public static String nameOf(Integer type) {
    SupplierOrderTypeEnum enu = valOf(type);
    return null == enu ? null : enu.getName();
  }
}
