package com.xz.supplierOrder.enums;

/**
 * @author Administrator
 */

public enum SupplierOrderTypeEnum {

  /** 1-发货单*/
  DELIVERY_ORDER(1,"发货单","HSTFH","supplierDeliveryOrderServiceImpl"),
  /** 2-退货单*/
  RETURN_ORDER(2,"退货单","HSTTH","supplierReturnOrderServiceImpl"),
  /** 3-换货单*/
  EXCHANGE_ORDER(3,"换货单","HSTHH","supplierExchangeOrderServiceImpl"),
  ;
	private final Integer type;

	private final String name;

	private final String noPrefix;

	private final String serviceName;

  SupplierOrderTypeEnum(Integer type, String name, String noPrefix, String serviceName) {
    this.type = type;
    this.name = name;
    this.noPrefix = noPrefix;
    this.serviceName = serviceName;
  }

  public Integer getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public String getNoPrefix() {
    return noPrefix;
  }

  public String getServiceName() {
    return serviceName;
  }

  public boolean eq(Integer type) {
    if (null != type && type.equals(getType())) {
      return true;
    }
    return false;
  }

  public static SupplierOrderTypeEnum valOf(Integer type) {
    if (null != type) {
      for (SupplierOrderTypeEnum enu : SupplierOrderTypeEnum.values()) {
        if (enu.getType().equals(type)) {
          return enu;
        }
      }
    }
    return null;
  }

  public static String nameOf(Integer status) {
    SupplierOrderTypeEnum enu = valOf(status);
    return null == enu ? null : enu.getName();
  }

  public static String noPrefixOf(Integer status) {
    SupplierOrderTypeEnum enu = valOf(status);
    return null == enu ? null : enu.getNoPrefix();
  }
}
