package com.xz.supplierOrder.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 贸易中间商订单商品对象 t_supplier_order_detail
 *
 * @author xz
 * @date 2024-02-27
 */
@Data
public class SupplierOrderDetailFlowSummaryVo {

  /** 企业客户 */
  @Excel(name = "客户名称")
  private String entSupplierName;
  /** 商品类别 */
  @Excel(name = "商品类别")
  private String productCategory;
  /** 商品名称 */
  @Excel(name = "商品名称")
  private String productName;
  /** 合同单价范围 */
  @Excel(name = "合同单价（范围）")
  private String contractPrice;
  /** 最小合同单价 */
  private BigDecimal minPrice;
  /** 最大合同单价 */
  private BigDecimal maxPrice;
  /** 数量 */
  @Excel(name = "数量")
  private Integer quantity;
  /** 总数 */
  private Integer summaryQuantity;
  /** 数量占比 */
  @Excel(name = "数量占比")
  private BigDecimal quantityProportion;
  /** 销售金额 */
  @Excel(name = "销售金额")
  private BigDecimal totalAmount;

  public String getContractPrice() {
    if(null == minPrice && null == maxPrice){
      return "0.00";
    }else if(null == minPrice){
      return maxPrice.toString();
    }else if(null == maxPrice){
      return minPrice.toString();
    }else if(maxPrice.compareTo(minPrice) == 0){
      return maxPrice.toString();
    }
    return minPrice.toString() + " ~ " + maxPrice.toString();
  }

  public BigDecimal getQuantityProportion() {
    if(null == quantity
      || quantity.intValue() == 0
      || null == summaryQuantity
      || summaryQuantity.intValue() == 0){
      return BigDecimal.ZERO.setScale(4,BigDecimal.ROUND_HALF_UP);
    }
    BigDecimal res = new BigDecimal(quantity).divide(new BigDecimal(summaryQuantity),4,BigDecimal.ROUND_HALF_UP);
    return res;
  }
}
