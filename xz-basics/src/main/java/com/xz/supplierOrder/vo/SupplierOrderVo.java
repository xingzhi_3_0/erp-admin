package com.xz.supplierOrder.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 贸易中间商订单对象 t_supplier_order
 *
 * @author xz
 * @date 2024-02-27
 */
@Data
public class SupplierOrderVo{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 订单编号 */
    private String orderNo;

    /** 订单类型：1-发货单,2-退货单,3-换货单 */
    private Integer orderType;

    /** 换货方式：0-无,1-先退货后换货,2-先换货后退货 */
    private Integer exchangeMethod;

    /** 订单状态：1-待提交(待发货，待退货，待换货),2-已提交(已发货，已退货，已换货),3-已冲红,4-已作废,5-已收货待换货,6-已换货待收货  */
    private Integer orderStatus;

    /** 合同ID */
    private Long contractId;

    /** 合同编号 */
    private String contractNo;

    /** 合同开始日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date contractStartTime;

    /** 合同终止日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date contractEndTime;

    /** 贸易中间商id */
    private Long supplierId;

    /** 贸易中间商名称 */
    private String supplierName;

    /** 收件地址 */
    private String address;

    /** 联系人 */
    private String contactPerson;

    /** 联系方式 */
    private String contactInformation;

    /** 快递公司字典值 */
    private String expressDeliveryCompanies;

    /** 快递公司 */
    private String expressDeliveryCompaniesName;

    /** 快递单号 */
    private String expressTrackingNumber;

    /** 寄件时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date expressShippingTime;

    /** 寄件人 */
    private String expressSender;

    /** 寄件人 */
    private String expressSenderPhone;

    /** 发货日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date deliveryTime;

    /** 退货日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date returnTime;

    /** 制单人 */
    private String createByName;

    /** 商品数量 */
    private Integer detailSize;

    /** 患者名称 */
    private String patientNames;

    /** 发货详情 */
    private List<SupplierOrderDetailVo> deliveryDetails = new ArrayList<>();

    /** 退货详情 */
    private List<SupplierOrderDetailVo> returnDetails = new ArrayList<>();

    public Integer getDetailSize() {
      int deliveryDetailsSize = null == deliveryDetails ? 0 :deliveryDetails.size();
      int returnDetailsSzie = null == returnDetails ? 0 : returnDetails.size();
      return deliveryDetailsSize + returnDetailsSzie;
    }
}
