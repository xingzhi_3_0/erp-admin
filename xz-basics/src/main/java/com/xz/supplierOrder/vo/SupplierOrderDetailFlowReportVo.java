package com.xz.supplierOrder.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 贸易中间商订单商品对象 t_supplier_order_detail
 *
 * @author xz
 * @date 2024-02-27
 */
@Data
public class SupplierOrderDetailFlowReportVo{

    /** id */
    private Long id;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "销售日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 数据类型：1-发货,2-退货 */
    @Excel(name = "出入库类型",readConverterExp = "1=销售出库,2=销售退货")
    private Integer dataType;
    /** 订单类型：1-发货单,2-退货单,3-换货单 */
    @Excel(name = "来源单据",readConverterExp = "1=发货登记,2=退货登记,3=换货登记")
    private Integer orderType;
    /** 贸易中间商名称 */
    @Excel(name = "客户名称")
    private String supplierName;
    /** 患者姓名 */
    @Excel(name = "患者")
    private String patientName;
    /** 商品类别 */
    @Excel(name = "商品类别")
    private String productCategory;
    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;
    /** 产品名称 */
    @Excel(name = "产品名称")
    private String prodName;
    /** 规格 */
    @Excel(name = "规格型号")
    private String specification;
    /** 系列 */
    @Excel(name = "系列")
    private String series;    /** 商品参数 */
    @Excel(name = "参数")
    private String productParam;
    /** 生产批号/sn码 */
    @Excel(name = "sn码")
    private String batchNumber;
    /** 生产日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生产日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date batchDate;
    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "有效期指至", width = 30, dateFormat = "yyyy-MM-dd")
    private Date validity;
    /** 生产厂商 */
    @Excel(name = "生产厂家")
    private String manufacturer;
    /** 注册证号 */
    @Excel(name = "注册证号")
    private String registrationNo;
    /** 商品数量 */
    @Excel(name = "数量")
    private Integer productNum;
    /** 合同单价 */
    @Excel(name = "合同单价")
    private BigDecimal contractPrice;
    /** 小计 */
    @Excel(name = "金额（元）")
    private BigDecimal subtotal;
    @Excel(name = "备注")
    private String remark;
    /** 创建人名称 */
    @Excel(name = "销售员")
    private String createByName;
    /** 订单编号 */
    @Excel(name = "业务单号")
    private String orderNo;
    @Excel(name = "单据备注")
    private String orderRemark;
}
