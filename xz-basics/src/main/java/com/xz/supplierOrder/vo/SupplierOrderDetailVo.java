package com.xz.supplierOrder.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 贸易中间商订单商品对象 t_supplier_order_detail
 *
 * @author xz
 * @date 2024-02-27
 */
@Data
public class SupplierOrderDetailVo{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 订单ID */
    private Long orderId;

    /** 数据类型：1-发货,2-退货 */
    private Integer dataType;

    /** 商品id */
    private Long productId;

    /** 商品名称 */
    private String productName;

    /** 商品参数 */
    private String productParam;

    /** 生产日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date batchDate;

    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date validity;

    /** 生产批号/sn码 */
    private String batchNumber;

    /** 单位 */
    private String unit;

    /** 生产厂商 */
    private String manufacturer;

    /** 注册证号 */
    private String registrationNo;

    /** 仓库id */
    private Long warehouseId;

    /** 仓库名称 */
    private String warehouseName;

    /** 供应商id */
    private Long supplierId;

    /** 供应商 */
    private String supplierName;

    /** 患者姓名 */
    private String patientName;

    /** 零售价 */
    private BigDecimal productPrice;

    /** 合同单价 */
    private BigDecimal contractPrice;

    /** 商品数量 */
    private Integer productNum;

    /** 小计 */
    private BigDecimal subtotal;

    /** 可用库存 */
    private Integer availableStock;

    /** 可用库存 */
    private String remark;
}
