package com.xz.supplierOrder.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import com.xz.purchase.domain.PurchaseProductParam;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 贸易中间商订单商品对象 t_supplier_order_detail
 *
 * @author xz
 * @date 2024-02-27
 */
@Data
public class SupplierOrderDetailDto extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 订单ID */
    private Long orderId;

    /** 数据类型：1-发货,2-退货 */
    private Integer dataType;

    /** 商品id */
    @NotNull(message = "商品ID不能为空")
    private Long productId;

    /** 商品名称 */
    private String productName;

    /** 商品参数 */
    private String productParam;

    /**
     * 采购商品属性(商品参数优先)
     */
    private List<PurchaseProductParam> purchaseProductParamList;

    /** 品牌 */
    private String brandName;

    /** 产品名称id */
    private Integer prodNameId;

    /** 产品名称 */
    private String prodName;

    /** 规格 */
    private String specification;

    /** 系列 */
    private String series;

    /** 商品类别 */
    private String productCategory;

    /** 编码 */
    private String productEncoded;
    /** 生产日期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date batchDate;

    /** 有效期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date validity;

    /** 生产批号/sn码 */
    private String batchNumber;

    /** 单位 */
    private String unit;

    /** 生产厂商 */
    private String manufacturer;

    /** 注册证号 */
    private String registrationNo;

    /** 仓库id */
    private Long warehouseId;

    /** 仓库名称 */
    private String warehouseName;

    /** 供应商id */
    private Long supplierId;

    /** 供应商 */
    private String supplierName;

    /** 患者姓名 */
    @Length(max = 200,message = "患者姓名最多{max}个字符")
    private String patientName;

    /** 零售价 */
    private BigDecimal productPrice;

    /** 合同单价 */
    @NotNull(message = "合同单价不能为空")
    private BigDecimal contractPrice;

    /** 商品数量 */
    @NotNull(message = "商品数量不能为空")
    private Integer productNum;

    /** 小计 */
    private BigDecimal subtotal;

    /** 备注 */
    @Length(max = 200,message = "备注最多{max}个字符")
    private String remark;
}
