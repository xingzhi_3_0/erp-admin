package com.xz.supplierOrder.dto;

import com.xz.common.utils.StringUtils;
import com.xz.common.validation.SituationGroup;
import com.xz.logistics.domain.Logistics;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.CollectionUtils;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 贸易中间商订单对象 t_supplier_order
 *
 * @author xz
 * @date 2024-02-27
 */
@Data
public class SupplierOrderDto{
  private static final long serialVersionUID = 1L;

  /** id */
  private Long id;

  /** 订单编号 */
  private String orderNo;

  /** 订单类型：1-发货单,2-退货单,3-换货单 */
  @NotNull(message = "订单类型不能为空")
  private Integer orderType;

  /** 订单状态：1-待提交(待发货，待退货，待换货),2-已提交(已发货，已退货，已换货),3-已冲红,4-已作废,5-已收货待换货,6-已换货待收货 */
  @NotNull(message = "订单状态不能为空")
  private Integer orderStatus;

  /** 换货方式：0-无,1-先退货后换货,2-先换货后退货 */
  private Integer exchangeMethod;

  /** 合同ID */
  @NotNull(message = "合同不能为空")
  private Long contractId;

  /** 合同开始日期 */
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private Date contractStartTime;

  /** 合同终止日期 */
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private Date contractEndTime;

  /** 贸易中间商id */
  @NotNull(message = "客户名称不能为空")
  private Long supplierId;

  /** 贸易中间商名称 */
  @NotNull(message = "客户名称不能为空")
  @Length(max = 64,message = "客户名称最多{max}个字符")
  private String supplierName;

  /** 收件地址 */
  @NotNull(message = "收件地址不能为空",groups = {SituationGroup.A.class})
  @Length(max = 128,message = "收件地址最多{max}个字符")
  private String address;

  /** 联系人 */
  @NotNull(message = "联系人不能为空",groups = {SituationGroup.A.class})
  @Length(max = 32,message = "联系人最多{max}个字符")
  private String contactPerson;

  /** 联系方式 */
  @NotNull(message = "联系方式不能为空",groups = {SituationGroup.A.class})
  @Length(max = 32,message = "联系方式最多{max}个字符")
  private String contactInformation;

  /** 快递公司字典值 */
  @NotNull(message = "快递公司不能为空",groups = {SituationGroup.A.class})
  private String expressDeliveryCompanies;

  /** 快递公司 */
  private String expressDeliveryCompaniesName;

  /** 快递单号 */
  @NotBlank(message = "快递单号不能为空",groups = {SituationGroup.A.class})
  @Length(max = 64,message = "快递单号最多{max}个字符")
  private String expressTrackingNumber;

  /** 备注 */
  @Length(max = 200,message = "备注最多{max}个字符")
  private String remark;

  /** 寄件时间 */
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date expressShippingTime;
  /** 寄件人 */
  @NotBlank(message = "寄件人手机号不能为空",groups = {SituationGroup.A.class})
  @Length(min = 11,max = 11,message = "寄件人手机号必须为11位数",groups = {SituationGroup.A.class})
  private String expressSenderPhone;
  /** 寄件人 */
  @Length(max = 32,message = "寄件人最多{max}个字符")
  private String expressSender;
  /** 发货详情 */
  @Valid()
  @NotEmpty(message = "商品信息不能为空",groups = {SituationGroup.A.class})
  @NotEmpty(message = "换货信息不能为空",groups = {SituationGroup.C.class})
  private List<SupplierOrderDetailDto> deliveryDetails;

  /** 退货详情 */
  @Valid()
  @NotEmpty(message = "商品信息不能为空",groups = {SituationGroup.B.class})
  @NotEmpty(message = "收货信息不能为空",groups = {SituationGroup.C.class})
  private List<SupplierOrderDetailDto> returnDetails;

  /** 旧状态 */
  private Integer beforeStatus;
  /**
   * 计算商品数量
   * @return
   */
  public int countTotalProductNum() {
    int total = countProductNum(deliveryDetails) + countProductNum(returnDetails);
    return total;
  }
  /**
   * 计算商品数量
   * @return
   */
  public int countProductNum(List<SupplierOrderDetailDto> dtos) {
    if(CollectionUtils.isEmpty(dtos)){
      //默认缺省情况下的旧状态为待提交
      return 0;
    }
    return dtos.stream().mapToInt(SupplierOrderDetailDto :: getProductNum).sum();
  }

  /**
   * 拼接患者名称
   * @return
   */
  public String getPatientNames() {
    Set<String> nameSet = new HashSet<>();
    if(!CollectionUtils.isEmpty(deliveryDetails)){
      List<String> list = deliveryDetails.stream().map(SupplierOrderDetailDto::getPatientName).collect(Collectors.toList());
      nameSet.addAll(list);
    }
    if(!CollectionUtils.isEmpty(returnDetails)){
      List<String> list = returnDetails.stream().map(SupplierOrderDetailDto::getPatientName).collect(Collectors.toList());
      nameSet.addAll(list);
    }
    String name = StringUtils.join(nameSet,",");
    return name;
  }

  public Integer getBeforeStatus() {
    return null == beforeStatus ? 1 : beforeStatus;
  }

  public Logistics getLogistics(){

    Logistics entity = new Logistics();
    entity.setAddress(getAddress());
    entity.setExpressDeliveryCompanies(getExpressDeliveryCompanies());
    entity.setExpressDeliveryCompaniesName(getExpressDeliveryCompaniesName());
    entity.setExpressSender(getExpressSender());
    entity.setExpressSenderPhone(getExpressSenderPhone());
    entity.setExpressShippingTime(getExpressShippingTime());
    entity.setExpressTrackingNumber(getExpressTrackingNumber());
    return entity;
  }
}
