package com.xz.supplierOrder.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * 贸易中间商订单商品出入库记录对象 t_supplier_order_detail_record
 *
 * @author xz
 * @date 2024-03-04
 */
@Data
@TableName("t_supplier_order_detail_record")
public class SupplierOrderDetailRecord extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 订单ID */
    @Excel(name = "订单ID")
    private Long orderId;

    /** 订单详情ID */
    @Excel(name = "订单详情ID")
    private Long orderDetailId;

    /** 采购商品明细ID */
    @Excel(name = "采购商品明细ID")
    private Long purchaseProductId;

    /** 采购ID */
    @Excel(name = "采购ID")
    private Long purchaseId;

    /** 商品id */
    @Excel(name = "商品id")
    private Long productId;

    /** 数据类型：1-出库,2-入库 */
    @Excel(name = "数据类型：1-出库,2-入库")
    private Integer dataType;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private Integer productNum;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    @NotNull(message = "租户id不能为空")
    private Long tenantId;

    /** 部门id(门店id) */
    @Excel(name = "部门id(门店id)")
    @NotNull(message = "部门id(门店id)不能为空")
    private Long deptId;

    @TableField(exist = false)
    private SupplierOrderDetail detail;
}
