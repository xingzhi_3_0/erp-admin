package com.xz.supplierOrder.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.NotNull;

import lombok.Data;
import com.xz.common.core.domain.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 贸易中间商订单商品对象 t_supplier_order_detail
 *
 * @author xz
 * @date 2024-02-27
 */
@Data
@TableName("t_supplier_order_detail")
public class SupplierOrderDetail extends BaseEntity {
  private static final long serialVersionUID = 1L;

  /**
   * id
   */
  private Long id;

  /**
   * 订单ID
   */
  @Excel(name = "订单ID")
  private Long orderId;

  /**
   * 数据类型：1-发货,2-退货
   */
  @Excel(name = "数据类型：1-发货,2-退货")
  private Integer dataType;

  /**
   * 贸易中间商id
   */
  @Excel(name = "贸易中间商id")
  private Long entSupplierId;

  /**
   * 商品id
   */
  @Excel(name = "商品id")
  private Long productId;

  /**
   * 贸易中间商名称
   */
  @Excel(name = "贸易中间商名称")
  private String entSupplierName;

  /**
   * 商品名称
   */
  @Excel(name = "商品名称")
  private String productName;

  /**
   * 产品名称id
   */
  @Excel(name = "产品名称id")
  private Long prodNameId;

  /**
   * 商品参数
   */
  @Excel(name = "商品参数")
  private String productParam;

  /**
   * 商品品牌
   */
  @Excel(name = "商品品牌")
  private String brandName;

  /**
   * 产品名称
   */
  @Excel(name = "产品名称")
  private String prodName;

  /**
   * 生产日期
   */
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  @Excel(name = "生产日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
  private Date batchDate;

  /**
   * 有效期
   */
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  @Excel(name = "有效期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
  private Date validity;

  /**
   * 商品类别
   */
  @Excel(name = "商品类别")
  private String productCategory;

  /**
   * 规格
   */
  @Excel(name = "规格")
  private String specification;

  /**
   * 生产批号/sn码
   */
  @Excel(name = "生产批号/sn码")
  private String batchNumber;

  /**
   * 商品编码
   */
  @Excel(name = "商品编码")
  private String productEncoded;

  /**
   * 系列
   */
  @Excel(name = "系列")
  private String series;

  /**
   * 单位
   */
  @Excel(name = "单位")
  private String unit;

  /**
   * 生产厂商
   */
  @Excel(name = "生产厂商")
  private String manufacturer;

  /**
   * 注册证号
   */
  @Excel(name = "注册证号")
  private String registrationNo;

  /**
   * 商品品牌id
   */
  @Excel(name = "商品品牌id")
  private Long brandId;

  /**
   * 仓库id
   */
  @Excel(name = "仓库id")
  private Long warehouseId;

  /**
   * 仓库名称
   */
  @Excel(name = "仓库名称")
  private String warehouseName;

  /**
   * 商品类别id
   */
  @Excel(name = "商品类别id")
  private Long productCategoryId;

  /**
   * 供应商id
   */
  @Excel(name = "供应商id")
  private Long supplierId;

  /**
   * 供应商
   */
  @Excel(name = "供应商")
  private String supplierName;

  /**
   * 患者姓名
   */
  @Excel(name = "患者姓名")
  private String patientName;

  /**
   * 零售价
   */
  @Excel(name = "零售价")
  private BigDecimal productPrice;

  /**
   * 合同单价
   */
  @Excel(name = "合同单价")
  private BigDecimal contractPrice;

  /**
   * 商品数量
   */
  @Excel(name = "商品数量")
  private Integer productNum;

  /**
   * 单位id
   */
  @Excel(name = "单位id")
  private Long unitId;

  /**
   * 小计
   */
  @Excel(name = "小计")
  private BigDecimal subtotal;

  /**
   * 删除标志（0代表存在 2代表删除）
   */
  private Long delFlag;

  /**
   * 租户id
   */
  @Excel(name = "租户id")
  @NotNull(message = "租户id不能为空")
  private Long tenantId;

  /**
   * 部门id(门店id)
   */
  @Excel(name = "部门id(门店id)")
  @NotNull(message = "部门id(门店id)不能为空")
  private Long deptId;

  /**
   * 创建人名称
   */
  @Excel(name = "创建人名称")
  private String createByName;


  /**
   * 订单类型：1-发货单,2-退货单,3-换货单
   */
  @TableField(exist = false)
  private Integer orderType;
  /**
   * 汇总总数
   */
  @TableField(exist = false)
  private Integer summaryQuantity;
  /**
   * 就诊日期-开始
   */
  @TableField(exist = false)
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private Date createStartTime;
  /**
   * 就诊日期-截止
   */
  @TableField(exist = false)
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private Date createEndTime;
}
