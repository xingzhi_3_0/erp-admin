package com.xz.supplierOrder.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * 贸易中间商订单对象 t_supplier_order
 *
 * @author xz
 * @date 2024-02-27
 */
@Data
@TableName("t_supplier_order")
public class SupplierOrder extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String orderNo;

    /** 订单类型：1-发货单,2-退货单,3-换货单 */
    @Excel(name = "订单类型：1-发货单,2-退货单,3-换货单")
    private Integer orderType;

    /** 换货方式：0-无,1-先退货后换货,2-先换货后退货 */
    @Excel(name = "换货方式：0-无,1-先退货后换货,2-先换货后退货")
    private Integer exchangeMethod;

    /** 订单状态：1-待提交(待发货，待退货，待换货),2-已提交(已发货，已退货，已换货),3-已冲红,4-已作废,5-已收货待换货,6-已换货待收货  */
    //@Excel(name = "订单状态：1-待提交(待发货，待退货，待换货),2-已提交(已发货，已退货，已换货),3-已冲红,4-已作废,5-已收货待换货,6-已换货待收货 ")
    private Integer orderStatus;

    /** 合同ID */
    @Excel(name = "合同ID")
    private Long contractId;

    /** 合同编号 */
    @Excel(name = "合同编号")
    private String contractNo;

    /** 合同开始日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "合同开始日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date contractStartTime;

    /** 合同终止日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "合同终止日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date contractEndTime;

    /** 贸易中间商id */
    @Excel(name = "贸易中间商id")
    private Long supplierId;

    /** 贸易中间商名称 */
    @Excel(name = "贸易中间商名称")
    private String supplierName;

    /** 收件地址 */
    @Excel(name = "收件地址")
    private String address;

    /** 联系人 */
    @Excel(name = "联系人")
    private String contactPerson;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String contactInformation;

    /** 快递公司字典值 */
    @Excel(name = "快递公司字典值")
    private String expressDeliveryCompanies;

    /** 快递公司 */
    @Excel(name = "快递公司")
    private String expressDeliveryCompaniesName;

    /** 快递单号 */
    @Excel(name = "快递单号")
    private String expressTrackingNumber;

    /** 寄件时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "寄件时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date expressShippingTime;

    /** 寄件人 */
    @Excel(name = "寄件人")
    private String expressSender;

    /** 寄件人 */
    @Excel(name = "寄件人号码")
    private String expressSenderPhone;


    /** 发货日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "发货日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deliveryTime;

    /** 退货日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "退货日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date returnTime;

    /** 制单人 */
    @Excel(name = "制单人")
    private String createByName;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private Integer productNum;

    /** 患者名称 */
    @Excel(name = "患者名称")
    private String patientNames;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    @NotNull(message = "租户id不能为空")
    private Long tenantId;

    /** 部门id(门店id) */
    @Excel(name = "部门id(门店id)")
    @NotNull(message = "部门id(门店id)不能为空")
    private Long deptId;

    /** 创建时间开始 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createStartTime;
    /** 创建时间截止 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createEndTime;

    /** 旧状态 */
    @TableField(exist = false)
    private Integer beforeStatus;

    @TableField(exist = false)
    private List<String> statusList;

    public Integer getBeforeStatus() {
      return null == beforeStatus ? 1 : beforeStatus;
    }
}
