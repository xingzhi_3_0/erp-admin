package com.xz.supplierOrder.service;

import com.xz.supplierOrder.domain.SupplierOrder;
import com.xz.supplierOrder.dto.SupplierOrderDto;

public interface ISupplierOrderTypeFactoryService {

  /**
   * 校验单据
   * @param supplierOrder （贸易中间商）单据商品
   */
  void verifyFields(SupplierOrderDto supplierOrder);

  /**
   * 提交单据
   * @param supplierOrder （贸易中间商）单据商品
   */
  void submitOrder(SupplierOrder supplierOrder);

  /**
   * 冲红单据
   * @param supplierOrder （贸易中间商）单据商品
   */
  void reversalOrder(SupplierOrder supplierOrder);
}

