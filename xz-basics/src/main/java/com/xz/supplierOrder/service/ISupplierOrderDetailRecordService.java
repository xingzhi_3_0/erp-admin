package com.xz.supplierOrder.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.purchase.domain.PurchaseProduct;
import com.xz.supplierOrder.domain.SupplierOrderDetail;
import com.xz.supplierOrder.domain.SupplierOrderDetailRecord;

import java.util.List;

/**
 * 贸易中间商订单商品出入库记录Service接口
 *
 * @author xz
 * @date 2024-03-04
 */
public interface ISupplierOrderDetailRecordService extends IService<SupplierOrderDetailRecord> {

  /**
   * 获取单据的出入库记录
   *
   * @param orderId
   * @param orderDetailId
   * @param dataType
   * @return
   */
  List<SupplierOrderDetailRecord> getByOrder(Long orderId, Long orderDetailId, Integer dataType);

  /**
   * 初始化贸易中间商订单商品出入库记录
   *
   * @param detail=
   * @param purchaseProducts
   */
  List<SupplierOrderDetailRecord> initEntityList(SupplierOrderDetail detail, List<PurchaseProduct> purchaseProducts, Integer dataType);
}
