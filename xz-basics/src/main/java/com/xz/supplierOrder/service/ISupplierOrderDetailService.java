package com.xz.supplierOrder.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.supplierOrder.domain.SupplierOrder;
import com.xz.supplierOrder.domain.SupplierOrderDetail;
import com.xz.supplierOrder.domain.SupplierOrderDetailRecord;
import com.xz.supplierOrder.dto.SupplierOrderDetailDto;
import com.xz.supplierOrder.dto.SupplierOrderDto;
import com.xz.supplierOrder.vo.SupplierOrderDetailFlowReportVo;
import com.xz.supplierOrder.vo.SupplierOrderDetailFlowSummaryVo;
import org.springframework.transaction.annotation.Transactional;

/**
 * 贸易中间商订单商品Service接口
 *
 * @author xz
 * @date 2024-02-27
 */
public interface ISupplierOrderDetailService {
  /**
   * 查询贸易中间商订单商品
   *
   * @param id 贸易中间商订单商品主键
   * @return 贸易中间商订单商品
   */
  SupplierOrderDetail selectSupplierOrderDetailById(Long id);

  /**
   * 根据订单id查询贸易中间商订单商品
   *
   * @param orderId 订单id
   * @return 贸易中间商订单商品
   */
  List<SupplierOrderDetail> selectByOrderId(Long orderId);

  /**
   * 把详情塞入出库记录中
   *
   * @param supplierOrder
   * @param records
   */
  void setRecordDetail(SupplierOrder supplierOrder, List<SupplierOrderDetailRecord> records);

  /**
   * 根据订单id，数据类型，查询贸易中间商订单商品
   *
   * @param orderId
   * @param dataType
   * @return
   */
  List<SupplierOrderDetail> selectByOrderIdAndDataType(Long orderId, Integer dataType);

  /**
   * 流水报表
   *
   * @param supplierOrderDetail 贸易中间商订单商品
   * @return 贸易中间商订单商品
   */
  List<SupplierOrderDetailFlowReportVo> selectReport(SupplierOrderDetail supplierOrderDetail);

  /**
   * 汇总报表
   *
   * @param supplierOrderDetail 贸易中间商订单商品
   * @return 贸易中间商订单商品
   */
  List<SupplierOrderDetailFlowSummaryVo> selectSummary(SupplierOrderDetail supplierOrderDetail);

  /**
   * 查询贸易中间商订单商品列表
   *
   * @param supplierOrderDetail 贸易中间商订单商品
   * @return 贸易中间商订单商品集合
   */
  List<SupplierOrderDetail> selectSupplierOrderDetailList(SupplierOrderDetail supplierOrderDetail);

  /**
   * 保存贸易中间商订单商品
   *
   * @param order 贸易中间商订单
   * @param dto   前端传入的(贸易中间商订单)对象
   */
  void insertBatchForOrder(SupplierOrder order, SupplierOrderDto dto);

  /**
   * 保存贸易中间商订单商品
   *
   * @param order 贸易中间商订单
   * @param dto   前端传入的(贸易中间商订单)对象
   */
  void updateBatchForOrder(SupplierOrder order, SupplierOrderDto dto);


  /**
   * 批量删除贸易中间商订单商品
   *
   * @param ids 需要删除的贸易中间商订单商品主键集合
   * @return 结果
   */
  int deleteSupplierOrderDetailByIds(Long[] ids);

  /**
   * 删除贸易中间商订单商品信息
   *
   * @param id 贸易中间商订单商品主键
   * @return 结果
   */
  int deleteSupplierOrderDetailById(Long id);
}
