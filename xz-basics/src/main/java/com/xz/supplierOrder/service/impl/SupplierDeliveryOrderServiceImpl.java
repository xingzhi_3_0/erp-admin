package com.xz.supplierOrder.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.xz.common.exception.ServiceException;
import com.xz.common.utils.DateUtils;
import com.xz.common.utils.SecurityUtils;
import com.xz.common.validation.SituationGroup;
import com.xz.common.validation.ValidatorUtils;
import com.xz.supplierOrder.domain.SupplierOrder;
import com.xz.supplierOrder.domain.SupplierOrderDetail;
import com.xz.supplierOrder.domain.SupplierOrderDetailRecord;
import com.xz.supplierOrder.dto.SupplierOrderDetailDto;
import com.xz.supplierOrder.dto.SupplierOrderDto;
import com.xz.supplierOrder.enums.SupplierOrderStatusEnum;
import com.xz.supplierOrder.service.ISupplierOrderDetailRecordService;
import com.xz.supplierOrder.service.ISupplierOrderDetailService;
import com.xz.supplierOrder.service.ISupplierOrderProductService;
import com.xz.supplierOrder.service.ISupplierOrderTypeProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

/**
 * 贸易中间商-发货单处理类
 */
@Service("supplierDeliveryOrderServiceImpl")
public class SupplierDeliveryOrderServiceImpl implements ISupplierOrderTypeProcessorService {

  @Autowired
  private ISupplierOrderDetailService supplierOrderDetailService;
  @Resource
  private ISupplierOrderProductService supplierOrderProductService;
  @Autowired
  private ISupplierOrderDetailRecordService supplierOrderDetailRecordService;

  /**
   * 校验单据
   *
   * @param supplierOrder （贸易中间商）单据商品
   */
  @Override
  public void verifyFields(SupplierOrderDto supplierOrder) {

    //SituationGroup.A：发货校验组；SituationGroup.B：退货校验组；SituationGroup.C：换货校验组；
    try {
      ValidatorUtils.validate(supplierOrder, SituationGroup.A.class);
    } catch (ConstraintViolationException ex) {
      String message = new ArrayList<>(ex.getConstraintViolations()).get(0).getMessage();
      throw new ServiceException(message);
    }
  }

  /**
   * 提交单据
   *
   * @param supplierOrder
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public void submitOrder(SupplierOrder supplierOrder) {

    //查询单据的商品信息
    List<SupplierOrderDetail> details = supplierOrderDetailService.selectByOrderId(supplierOrder.getId());
    if (CollectionUtils.isEmpty(details)) {
      return;
    }
    // 开始发货 16.贸易中间商发货单（发货）
    supplierOrderProductService.deliverySupplierOrder(supplierOrder, details, 16);
  }

  /**
   * 冲红单据
   *
   * @param supplierOrder
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public void reversalOrder(SupplierOrder supplierOrder) {

    // 查询对应的贸易中间商订单商品出库记录
    List<SupplierOrderDetailRecord> records = supplierOrderDetailRecordService.getByOrder(
      supplierOrder.getId(), null, 1);
    // 把详情塞入出库记录中
    supplierOrderDetailService.setRecordDetail(supplierOrder, records);
    // 开始冲红操作 17.贸易中间商发货单（冲红）
    supplierOrderProductService.reversalOrderForDelivery(supplierOrder, records, 17);
    // 更新冲红状态
    supplierOrderProductService.updateForReversal(supplierOrder);
  }
}
