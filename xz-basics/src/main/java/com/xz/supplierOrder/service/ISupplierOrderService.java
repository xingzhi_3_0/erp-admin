package com.xz.supplierOrder.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.supplierOrder.domain.SupplierOrder;
import com.xz.supplierOrder.dto.SupplierOrderDto;

/**
 * 贸易中间商订单Service接口
 *
 * @author xz
 * @date 2024-02-27
 */
public interface ISupplierOrderService extends IService<SupplierOrder> {
  /**
   * 查询贸易中间商订单
   *
   * @param id 贸易中间商订单主键
   * @return 贸易中间商订单
   */
  public SupplierOrder selectSupplierOrderById(Long id);

  /**
   * 查询贸易中间商订单列表
   *
   * @param supplierOrder 贸易中间商订单
   * @return 贸易中间商订单集合
   */
  public List<SupplierOrder> selectSupplierOrderList(SupplierOrder supplierOrder);

  /**
   * 新增贸易中间商订单
   *
   * @param supplierOrder 贸易中间商订单
   * @return 结果
   */
  public int insertSupplierOrder(SupplierOrderDto dto);

  /**
   * 修改贸易中间商订单
   *
   * @param dto 贸易中间商订单
   * @return 结果
   */
  public int updateSupplierOrder(SupplierOrderDto dto);

  /**
   * 修改贸易中间商订单状态
   *
   * @param id
   * @param orderStatus
   * @return
   */
  boolean editStatus(Long id, Integer orderStatus);

  /**
   * 作废单据
   *
   * @param id
   * @return
   */
  boolean nullifyOrder(Long id);

  /**
   * 冲红单据
   */
  boolean reversalOrder(Long id);



}
