package com.xz.supplierOrder.service;

import com.xz.purchase.domain.PurchaseProduct;
import com.xz.supplierOrder.domain.SupplierOrder;
import com.xz.supplierOrder.domain.SupplierOrderDetail;
import com.xz.supplierOrder.domain.SupplierOrderDetailRecord;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 贸易中间商订单Service接口
 *
 * @author xz
 * @date 2024-02-27
 */
public interface ISupplierOrderProductService {


  /**
   * 查询对应的采购商品明细
   *
   * @param detail （贸易中间商）单据商品
   * @return
   */
  List<PurchaseProduct> getPurchaseProducts(SupplierOrderDetail detail);

  /**
   * 查询可用库存数量
   *
   * @param detail （贸易中间商）单据商品
   * @return
   */
  int countPurchaseProducts(SupplierOrderDetail detail);

  /**
   * 发货--更新采购商品明细数量
   *
   * @param supplierOrder （贸易中间商）单据
   * @param details       （贸易中间商）单据商品
   * @param storageType   16.贸易中间商发货单（发货） 21.贸易中间商换货单（换货）
   */
  void deliverySupplierOrder(SupplierOrder supplierOrder, List<SupplierOrderDetail> details, int storageType);


  /**
   * 发货--冲红单据
   *
   * @param records     （贸易中间商）单据商品库存出入库记录数据
   * @param storageType 17.贸易中间商发货单（冲红） 23.贸易中间商换货单（换货冲红）
   */
  void reversalOrderForDelivery(SupplierOrder supplierOrder, List<SupplierOrderDetailRecord> records, Integer storageType);

  /**
   * 退货--新增采购，采购商品明细
   *
   * @param supplierOrder （贸易中间商）单据
   * @param details       （贸易中间商）单据商品
   * @param storageType   18.贸易中间商退货单（退货） 20.贸易中间商换货单（退货）
   * @return
   */
  void returnSupplierOrder(SupplierOrder supplierOrder, List<SupplierOrderDetail> details, int storageType);

  /**
   * 退货--冲红单据
   *
   * @param records     （贸易中间商）单据商品库存出入库记录数据
   * @param storageType 19.贸易中间商退货单（冲红） 22.贸易中间商换货单（退货冲红）
   */
  void reversalOrderForReturn(SupplierOrder supplierOrder, List<SupplierOrderDetailRecord> records, Integer storageType);

  /**
   * 更新冲红状态
   *
   * @param supplierOrder （贸易中间商）单据
   */
  void updateForReversal(SupplierOrder supplierOrder);
}
