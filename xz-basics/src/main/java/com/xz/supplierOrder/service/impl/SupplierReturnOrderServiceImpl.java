package com.xz.supplierOrder.service.impl;

import com.xz.common.exception.ServiceException;
import com.xz.common.validation.SituationGroup;
import com.xz.common.validation.ValidatorUtils;
import com.xz.supplierOrder.domain.SupplierOrder;
import com.xz.supplierOrder.domain.SupplierOrderDetail;
import com.xz.supplierOrder.domain.SupplierOrderDetailRecord;
import com.xz.supplierOrder.dto.SupplierOrderDto;
import com.xz.supplierOrder.service.ISupplierOrderDetailRecordService;
import com.xz.supplierOrder.service.ISupplierOrderDetailService;
import com.xz.supplierOrder.service.ISupplierOrderProductService;
import com.xz.supplierOrder.service.ISupplierOrderTypeProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

/**
 * 贸易中间商-退货单处理类
 */
@Service("supplierReturnOrderServiceImpl")
public class SupplierReturnOrderServiceImpl implements ISupplierOrderTypeProcessorService {

  @Autowired
  private ISupplierOrderDetailService supplierOrderDetailService;
  @Resource
  private ISupplierOrderProductService supplierOrderProductService;
  @Autowired
  private ISupplierOrderDetailRecordService supplierOrderDetailRecordService;

  /**
   * 校验单据
   * @param supplierOrder （贸易中间商）单据商品
   */
  @Override
  public void verifyFields(SupplierOrderDto supplierOrder) {

    //SituationGroup.A：发货校验组；SituationGroup.B：退货校验组；SituationGroup.C：换货校验组；
    try {
      ValidatorUtils.validate(supplierOrder, SituationGroup.B.class);
    } catch (ConstraintViolationException ex) {
      String message = new ArrayList<>(ex.getConstraintViolations()).get(0).getMessage();
      throw new ServiceException(message);
    }
  }

  /**
   * 提交单据
   *
   * @param supplierOrder
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public void submitOrder(SupplierOrder supplierOrder) {

    //查询单据的商品信息
    List<SupplierOrderDetail> details = supplierOrderDetailService.selectByOrderId(supplierOrder.getId());
    if (CollectionUtils.isEmpty(details)) {
      return;
    }
    // 开始退货 18.贸易中间商退货单（退货）
    supplierOrderProductService.returnSupplierOrder(supplierOrder, details, 18);
  }

  /**
   * 冲红单据
   *
   * @param supplierOrder
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public void reversalOrder(SupplierOrder supplierOrder) {

    // 查询对应的贸易中间商订单商品出库记录
    List<SupplierOrderDetailRecord> records = supplierOrderDetailRecordService.getByOrder(
      supplierOrder.getId(), null, 2);
    // 把详情塞入出库记录中
    supplierOrderDetailService.setRecordDetail(supplierOrder, records);
    // 开始冲红操作19.贸易中间商退货单（冲红）
    supplierOrderProductService.reversalOrderForReturn(supplierOrder,records, 19);
    // 更新冲红状态
    supplierOrderProductService.updateForReversal(supplierOrder);
  }

}
