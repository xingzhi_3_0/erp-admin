package com.xz.member.vo;

import com.xz.member.domain.MemberPatient;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class MemberPatientVo extends MemberPatient {

    /** 患者名称 */
    private String patientName;

    /** 患者手机号 */
    private String phoneNumber;

    /** 患者性别 0=.不详,1=.男,2=.女 */
    private Integer sex;

    private Integer age;

    /** 累计消费 */
    private BigDecimal accumulatedConsumption;

    /** 最近就诊日期 */
    private Date lastVisitDate;
}
