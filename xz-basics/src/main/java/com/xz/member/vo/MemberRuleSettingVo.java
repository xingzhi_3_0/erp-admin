package com.xz.member.vo;

import com.alibaba.fastjson.JSONObject;
import com.xz.member.domain.RechargeActivity;
import com.xz.member.domain.RechargeActivityGift;
import lombok.Data;

import java.util.List;

@Data
public class MemberRuleSettingVo {

    private Integer setType;
    /** 设置数据集合 */
    private List<JSONObject> setDataList;
}
