package com.xz.member.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import com.xz.diagnosisOrder.utils.DiagnosisOrderUtils;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;


@Data
public class WalletRecordListVo{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;
    /** 会员名称 */
    private String memberName;
    /** 0-不详 1-男 2-女 */
    private Integer sex;
    /** 生日 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date birthday;
    @Excel(name = "会员名称")
    private String memberInfo;
    @Excel(name = "会员卡号")
    private String memberCardNo;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "变动时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @Excel(name = "变动类型", readConverterExp = "1=充值,2=钱包退款,3=销售单退款,4=销售单消费,5=诊疗单退款,6=诊疗单消费,7=挂号单退款,8=挂号单消费,11=历史充值,12=历史消费")
    private Integer changeType;
    @Excel(name = "活动名称")
    private String activityName;
    /** 变动值类型（1-加. 2.减 */
    private Integer changeValueType;
    /** 变动值  */
    private BigDecimal changeValue;
    @Excel(name = "充值金额")
    private BigDecimal rechargeAmount;
    @Excel(name = "钱包支付")
    private BigDecimal paymentAmount;
    @Excel(name = "关联单号")
    private String relatedNo;
    /** 操作人名称 */
    @Excel(name = "操作人")
    private String operateUser;
    /** 操作部门名称 */
    @Excel(name = "操作部门")
    private String deptName;
    @Excel(name = "备注")
    private String remark;
    @Excel(name = "变动类型", readConverterExp = "1=充值成功,2=扣费成功,3=待退款,4=退款成功,5=充值作废,6=退款作废")
    private Integer status;



}
