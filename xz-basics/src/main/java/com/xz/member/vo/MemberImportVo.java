package com.xz.member.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName MemberImportVo * @Description TODO
 * @Author Administrator
 * @Date 2024-3-4 14:33:07
 * @Version 1.0 会员导入
 **/
@Data
public class MemberImportVo {
    /** 会员卡号 */
    private String memberCardNo;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phoneNumber;


    /** 钱包余额 */
    @Excel(name = "钱包余额")
    private BigDecimal walletBalance;

    /** 积分余额 */
    @Excel(name = "积分余额")
    private BigDecimal integralBalance;
}
