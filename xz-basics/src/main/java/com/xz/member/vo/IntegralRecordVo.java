package com.xz.member.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class IntegralRecordVo {

    /** 积分余额 */
    private BigDecimal integralBalance;
    /** 累计积分 */
    private BigDecimal accumulatedIntegral;

}
