package com.xz.member.vo;

import com.xz.member.domain.RechargeActivity;
import com.xz.member.domain.RechargeActivityGift;
import lombok.Data;

import java.util.List;

@Data
public class RechargeActivityVo extends RechargeActivity {

    /** 礼物列表 */
    private List<RechargeActivityGift> giftList;
}
