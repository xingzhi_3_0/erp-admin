package com.xz.member.vo;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class MemberRuleSettingInfoVo {

    private BigDecimal quota;
    private BigDecimal deductionCash;
    private BigDecimal deductionLimit;
}
