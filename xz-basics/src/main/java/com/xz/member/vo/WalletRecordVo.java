package com.xz.member.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class WalletRecordVo {

    /** 钱包余额 */
    private BigDecimal walletBalance;
    /** 累计充值 */
    private BigDecimal accumulatedRecharge;

}
