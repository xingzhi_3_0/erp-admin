package com.xz.member.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.member.domain.MemberInfo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class MemberInfoVo extends MemberInfo {

    @Excel(name = "年龄")
    private Integer age;

    @Excel(name = "注册门店")
    private String deptName;

    /** 绑定患者 */
    @Excel(name = "绑定患者")
    private String bindingPatients;

    @Excel(name = "会员注册日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date registerDate;

}
