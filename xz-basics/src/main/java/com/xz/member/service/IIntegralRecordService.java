package com.xz.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.member.domain.GiftReceive;
import com.xz.member.domain.IntegralRecord;

import java.math.BigDecimal;
import java.util.List;

/**
 * 积分记录Service接口
 * 
 * @author xz
 * @date 2024-1-24 15:22:12
 */
public interface IIntegralRecordService extends IService<IntegralRecord> {

    /**
     * 查询列表
     *
     * @param integralRecord
     * @return
     */
    List<IntegralRecord> listByMemberId(IntegralRecord integralRecord);

    /**
     * 添加记录
     * @param integralRecord
     * @return
     */
    AjaxResult addRecord(IntegralRecord integralRecord);

    AjaxResult memberIntegral(IntegralRecord integralRecord);

    int addBizRecord(Integer changeType, BigDecimal changeValue, String memberCardNo, Long deptId, String relatedNo);
}
