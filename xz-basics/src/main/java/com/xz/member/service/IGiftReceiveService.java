package com.xz.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.member.domain.GiftReceive;
import com.xz.member.domain.RechargeActivity;
import com.xz.member.vo.RechargeActivityVo;

import java.util.List;

/**
 * 赠品领取Service接口
 * 
 * @author xz
 * @date 2024-1-23 10:56:42
 */
public interface IGiftReceiveService extends IService<GiftReceive> {

    /**
     * 查询列表
     *
     * @param giftReceive
     * @return
     */
    List<GiftReceive> listByMemberId(GiftReceive giftReceive);

    List<GiftReceive> listByMemberCardNo(GiftReceive giftReceive);


}
