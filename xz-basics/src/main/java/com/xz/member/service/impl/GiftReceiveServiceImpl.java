package com.xz.member.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.utils.SecurityUtils;
import com.xz.member.domain.GiftReceive;
import com.xz.member.domain.MemberInfo;
import com.xz.member.domain.RechargeActivity;
import com.xz.member.domain.RechargeActivityGift;
import com.xz.member.mapper.GiftReceiveMapper;
import com.xz.member.mapper.MemberInfoMapper;
import com.xz.member.mapper.RechargeActivityGiftMapper;
import com.xz.member.mapper.RechargeActivityMapper;
import com.xz.member.service.IGiftReceiveService;
import com.xz.member.service.IRechargeActivityService;
import com.xz.member.vo.RechargeActivityVo;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * 会员领取Service业务层处理
 *
 * @author xz
 * @date 2024-1-24 11:52:20
 */
@Service
public class GiftReceiveServiceImpl extends ServiceImpl<GiftReceiveMapper, GiftReceive> implements IGiftReceiveService {

    @Resource
    private MemberInfoMapper memberInfoMapper;

    @Override
    public List<GiftReceive> listByMemberId(GiftReceive giftReceive) {

        return baseMapper.queryList(giftReceive);
    }

    @Override
    public List<GiftReceive> listByMemberCardNo(GiftReceive giftReceive){
        giftReceive.setStatus(0);
        return baseMapper.queryList(giftReceive);
    }

}
