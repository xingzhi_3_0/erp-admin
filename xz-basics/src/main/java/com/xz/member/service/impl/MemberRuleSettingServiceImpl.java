package com.xz.member.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.core.domain.AjaxResult;
import com.xz.member.domain.MemberRuleSetting;
import com.xz.member.domain.RechargeActivity;
import com.xz.member.domain.RechargeActivityGift;
import com.xz.member.dto.MemberRuleSettingDTO;
import com.xz.member.mapper.MemberRuleSettingMapper;
import com.xz.member.mapper.RechargeActivityGiftMapper;
import com.xz.member.mapper.RechargeActivityMapper;
import com.xz.member.service.IMemberRuleSettingService;
import com.xz.member.service.IRechargeActivityService;
import com.xz.member.vo.MemberRuleSettingInfoVo;
import com.xz.member.vo.RechargeActivityVo;
import org.eclipse.jetty.util.StringUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 客户信息Service业务层处理
 *
 * @author xz
 * @date 2024-1-22 11:10:32
 */
@Service
public class MemberRuleSettingServiceImpl extends ServiceImpl<MemberRuleSettingMapper, MemberRuleSetting> implements IMemberRuleSettingService {

    @Override
    public MemberRuleSettingDTO getSettingInfo(Integer setType) {
        MemberRuleSettingDTO dto = new MemberRuleSettingDTO();
        List<JSONObject> objectList = new ArrayList<>();
        List<MemberRuleSetting> ruleSettings = baseMapper.findBySetType(setType);
        for (MemberRuleSetting ruleSetting:ruleSettings) {
            JSONObject obj = new JSONObject();
            Integer ruleSerialNo = ruleSetting.getRuleSerialNo();
            String ruleValue = ruleSetting.getRuleValue();
            JSONObject jsonObject = JSONObject.parseObject(ruleValue);
            obj.put(ruleSerialNo+1+"",jsonObject);
            objectList.add(obj);
        }
        dto.setSetType(setType);
        dto.setQueryParam(objectList);
        return dto;
    }

    @Override
    public AjaxResult saveSettingInfo(MemberRuleSettingDTO dto) {
        List<JSONObject> queryParam = dto.getQueryParam();
        Integer setType = dto.getSetType();
        // 先删除之前的设置
        baseMapper.deleteBySetTypeAfter(setType);
        // 再新增
        for (int i = 0; i < queryParam.size(); i++) {
            JSONObject jsonObject = queryParam.get(i);
            JSONObject objData = jsonObject.getJSONObject(i+1 + "");
            MemberRuleSetting memberRuleSetting = new MemberRuleSetting();
            memberRuleSetting.setSetType(dto.getSetType());
            memberRuleSetting.setRuleSerialNo(i);
            memberRuleSetting.setRuleValue(objData.toJSONString());
            baseMapper.insert(memberRuleSetting);
        }
        return AjaxResult.success("操作成功");
    }

    @Override
    public AjaxResult getInfoBySetType(Integer setType){
        List<MemberRuleSetting> bySetTypes = baseMapper.findBySetType(2);
        MemberRuleSettingInfoVo vo = new MemberRuleSettingInfoVo();
        if(!CollectionUtils.isEmpty(bySetTypes)){
            List<MemberRuleSetting> collect = bySetTypes.stream().filter(o -> o.getRuleSerialNo().equals(0)).collect(Collectors.toList());
            MemberRuleSetting memberRuleSetting = collect.get(0);
            String ruleValue = memberRuleSetting.getRuleValue();
            JSONObject jsonObject = JSONObject.parseObject(ruleValue);
            String quotaValue = jsonObject.getString("quota");
            String deductionCashValue = jsonObject.getString("deductionCash");
            String deductionLimitValue = jsonObject.getString("deductionLimit");
            if(!StringUtil.isEmpty(quotaValue)){
                vo.setQuota(new BigDecimal(quotaValue));
            }
            if(!StringUtil.isEmpty(deductionCashValue)){
                vo.setDeductionCash(new BigDecimal(deductionCashValue));
            }
            if(!StringUtil.isEmpty(deductionLimitValue)){
                vo.setDeductionLimit(new BigDecimal(deductionLimitValue));
            }

        }
        return AjaxResult.success(vo);
    }
}
