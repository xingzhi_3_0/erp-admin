package com.xz.member.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.member.domain.MemberPatient;
import com.xz.member.vo.MemberPatientVo;

/**
 * 会员患者关联信息Service接口
 *
 * @author xz
 * @date 2024-01-31
 */
public interface IMemberPatientService extends IService<MemberPatient> {
  /**
   * 查询会员患者关联信息
   *
   * @param id 会员患者关联信息主键
   * @return 会员患者关联信息
   */
  public MemberPatient selectMemberPatientById(Long id);

  /**
   * 查询会员患者关联信息列表
   *
   * @param memberPatient 会员患者关联信息
   * @return 会员患者关联信息集合
   */
  public List<MemberPatient> selectMemberPatientList(MemberPatient memberPatient);

  List<MemberPatientVo> queryList(MemberPatient memberPatient);

  /**
   * 新增会员患者关联信息
   *
   * @param memberPatient 会员患者关联信息
   * @return 结果
   */
  public AjaxResult insertMemberPatient(MemberPatient memberPatient);

  public AjaxResult unbind(MemberPatient memberPatient);

  /**
   * 修改会员患者关联信息
   *
   * @param memberPatient 会员患者关联信息
   * @return 结果
   */
  public int updateMemberPatient(MemberPatient memberPatient);

  /**
   * 批量删除会员患者关联信息
   *
   * @param ids 需要删除的会员患者关联信息主键集合
   * @return 结果
   */
  public int deleteMemberPatientByIds(Long[] ids);

  /**
   * 删除会员患者关联信息信息
   *
   * @param id 会员患者关联信息主键
   * @return 结果
   */
  public int deleteMemberPatientById(Long id);

  /**
   * 根据患者id查询会员患者关联信息
   * @param patientId
   * @return
   */
  public MemberPatient selectByPatientId(Long patientId);
}
