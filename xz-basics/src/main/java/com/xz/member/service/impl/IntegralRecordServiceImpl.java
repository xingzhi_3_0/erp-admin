package com.xz.member.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.core.domain.AjaxResult;
import com.xz.member.domain.GiftReceive;
import com.xz.member.domain.IntegralRecord;
import com.xz.member.domain.MemberInfo;
import com.xz.member.mapper.GiftReceiveMapper;
import com.xz.member.mapper.IntegralRecordMapper;
import com.xz.member.mapper.MemberInfoMapper;
import com.xz.member.service.IGiftReceiveService;
import com.xz.member.service.IIntegralRecordService;
import com.xz.member.vo.IntegralRecordVo;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * 积分记录Service业务层处理
 *
 * @author xz
 * @date 2024-1-24 15:23:19
 */
@Service
public class IntegralRecordServiceImpl extends ServiceImpl<IntegralRecordMapper, IntegralRecord> implements IIntegralRecordService {

    @Resource
    private MemberInfoMapper memberInfoMapper;

    @Override
    public List<IntegralRecord> listByMemberId(IntegralRecord integralRecord) {

        return baseMapper.queryList(integralRecord);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public AjaxResult addRecord(IntegralRecord integralRecord) {
        Integer changeType = integralRecord.getChangeType();
        BigDecimal changeValue = integralRecord.getChangeValue();
        MemberInfo memberInfo = memberInfoMapper.selectById(integralRecord.getMemberId());
        integralRecord.setMemberCardNo(memberInfo.getMemberCardNo());
        BigDecimal integralBalance = memberInfo.getIntegralBalance();
        if(changeType % 2 == 0){
            integralBalance = integralBalance.subtract(changeValue);
        }else {
            integralBalance = integralBalance.add(changeValue);
        }
        memberInfo.setIntegralBalance(integralBalance);
        memberInfoMapper.updateById(memberInfo);
        int insert = baseMapper.insert(integralRecord);
        if(insert>0){
            return AjaxResult.success("操作成功！");
        }else {
            return AjaxResult.success("操作失败！");
        }

    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int addBizRecord(Integer changeType,BigDecimal changeValue,String memberCardNo,Long deptId,String relatedNo) {
        IntegralRecord integralRecord = new IntegralRecord();
        integralRecord.setChangeType(changeType);
        integralRecord.setChangeValue(changeValue);
        integralRecord.setMemberCardNo(memberCardNo);
        integralRecord.setDeptId(deptId);
        integralRecord.setRelatedNo(relatedNo);
        int insert = baseMapper.insert(integralRecord);
        return insert;
    }


    @Override
    public AjaxResult memberIntegral(IntegralRecord integralRecord) {
        MemberInfo memberInfo = memberInfoMapper.selectById(integralRecord.getMemberId());
        BigDecimal accumulatedIntegral = baseMapper.sumMemberIntegral(memberInfo.getMemberCardNo());
        IntegralRecordVo vo = new IntegralRecordVo();
        vo.setAccumulatedIntegral(accumulatedIntegral);
        BigDecimal integralBalance = memberInfo.getIntegralBalance();
        vo.setIntegralBalance(integralBalance);
        return AjaxResult.success(vo);
    }
}
