package com.xz.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.member.domain.MemberLevel;
import com.xz.member.domain.MemberRuleSetting;
import com.xz.member.dto.MemberRuleSettingDTO;

import java.util.List;

/**
 * 会员等级Service接口
 *
 * @author xz
 * @date 2024-1-24 09:31:50
 */
public interface IMemberLevelService extends IService<MemberLevel> {

    /**
     * 列表
     * @return
     */
    List<MemberLevel> getList(MemberLevel memberLevel);

    AjaxResult editLevelInfo(MemberLevel memberLevel);

    /**
     * 根据会员卡号查询会员等级
     * @param memberCardNo
     * @return
     */
    MemberLevel getByMemberCardNo(String memberCardNo);

}
