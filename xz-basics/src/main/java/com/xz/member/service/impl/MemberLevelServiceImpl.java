package com.xz.member.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.core.domain.AjaxResult;
import com.xz.member.domain.MemberLevel;
import com.xz.member.domain.RechargeActivity;
import com.xz.member.domain.RechargeActivityGift;
import com.xz.member.dto.MemberRuleSettingDTO;
import com.xz.member.mapper.MemberLevelMapper;
import com.xz.member.mapper.RechargeActivityGiftMapper;
import com.xz.member.mapper.RechargeActivityMapper;
import com.xz.member.service.IMemberLevelService;
import com.xz.member.service.IRechargeActivityService;
import com.xz.member.vo.RechargeActivityVo;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * 客户等级Service业务层处理
 *
 * @author xz
 * @date 2024-1-24 09:33:38
 */
@Service
public class MemberLevelServiceImpl extends ServiceImpl<MemberLevelMapper, MemberLevel> implements IMemberLevelService {

  @Override
  public List<MemberLevel> getList(MemberLevel memberLevel) {
    return baseMapper.queryList(memberLevel);
  }

  @Override
  public AjaxResult editLevelInfo(MemberLevel memberLevel) {
    if (Objects.isNull(memberLevel.getId())) {
      return AjaxResult.error("参数不能为空！");
    }
    baseMapper.updateById(memberLevel);
    return AjaxResult.success("操作成功！");
  }

  /**
   * 根据会员卡号查询会员等级
   *
   * @param memberCardNo
   * @return
   */
  @Override
  public MemberLevel getByMemberCardNo(String memberCardNo) {
    return baseMapper.getByMemberCardNo(memberCardNo);
  }
}
