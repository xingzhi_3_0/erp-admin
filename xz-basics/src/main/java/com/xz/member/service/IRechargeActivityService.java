package com.xz.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.member.domain.MemberInfo;
import com.xz.member.domain.RechargeActivity;
import com.xz.member.param.MemberQueryParam;
import com.xz.member.vo.MemberInfoVo;
import com.xz.member.vo.RechargeActivityVo;

import java.util.List;

/**
 * 充值活动Service接口
 * 
 * @author xz
 * @date 2024-1-23 10:56:42
 */
public interface IRechargeActivityService extends IService<RechargeActivity> {

    /**
     * 查询列表
     *
     * @param activity
     * @return
     */
    List<RechargeActivity> queryList(RechargeActivity activity);

    /**
     * 详情
     * @param id
     * @return
     */
    RechargeActivityVo activityDetailById(Long id);

    AjaxResult addActivity(RechargeActivityVo rechargeActivityVo);

    AjaxResult updateActivity(RechargeActivityVo rechargeActivityVo);

    AjaxResult invalid(RechargeActivity rechargeActivity);

    int stopActivity();
}
