package com.xz.member.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.member.domain.MemberRuleSetting;
import com.xz.member.domain.RechargeActivity;
import com.xz.member.dto.MemberRuleSettingDTO;
import com.xz.member.vo.RechargeActivityVo;

import java.util.List;

/**
 * 会员规则Service接口
 * 
 * @author xz
 * @date 2024-1-23 10:56:42
 */
public interface IMemberRuleSettingService extends IService<MemberRuleSetting> {

    /**
     * 详情
     * @param setType
     * @return
     */
    MemberRuleSettingDTO getSettingInfo(Integer setType);

    AjaxResult saveSettingInfo(MemberRuleSettingDTO dto);

    AjaxResult getInfoBySetType(Integer setType);

}
