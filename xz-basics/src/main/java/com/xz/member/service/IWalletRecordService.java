package com.xz.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.member.domain.IntegralRecord;
import com.xz.member.domain.WalletRecord;
import com.xz.member.vo.WalletRecordListVo;

import java.math.BigDecimal;
import java.util.List;

/**
 * 钱包记录Service接口
 * 
 * @author xz
 * @date 2024-1-24 16:59:59
 */
public interface IWalletRecordService extends IService<WalletRecord> {

    /**
     * 查询列表
     *
     * @param walletRecord
     * @return
     */
    List<WalletRecord> listByMemberId(WalletRecord walletRecord);


	List<WalletRecordListVo> queryVoList(WalletRecord walletRecord);

	AjaxResult memberWallet(WalletRecord walletRecord);

    AjaxResult addRecord(WalletRecord walletRecord);

    AjaxResult cancelWalletRecord(WalletRecord walletRecord);

    int addBizRecord(String memberCardNo, Integer changeType, Integer changeValueType, String relatedNo, BigDecimal changeValue,Long deptId);
}
