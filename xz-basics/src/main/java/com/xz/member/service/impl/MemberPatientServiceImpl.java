package com.xz.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.domain.entity.SysDept;
import com.xz.common.core.domain.entity.SysUser;
import com.xz.common.utils.SecurityUtils;
import com.xz.member.domain.MemberInfo;
import com.xz.member.domain.MemberPatient;
import com.xz.member.mapper.MemberInfoMapper;
import com.xz.member.mapper.MemberPatientMapper;
import com.xz.member.service.IMemberPatientService;
import com.xz.member.vo.MemberPatientVo;
import com.xz.message.service.ISysTenantNotifyItemService;
import com.xz.message.vo.MemberMsgInfo;
import com.xz.patient.domain.PatientInfo;
import com.xz.patient.service.IPatientInfoService;
import com.xz.system.service.ISysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 会员患者关联信息Service业务层处理
 *
 * @author xz
 * @date 2024-01-31
 */
@Service
public class MemberPatientServiceImpl extends ServiceImpl<MemberPatientMapper, MemberPatient> implements IMemberPatientService {
  @Autowired
  private MemberPatientMapper memberPatientMapper;
  @Autowired
  private ISysTenantNotifyItemService sysTenantNotifyItemService;
  @Autowired
  private IPatientInfoService patientInfoService;
  @Resource
  private MemberInfoMapper memberInfoMapper;
  @Resource
  private ISysDeptService sysDeptService;
  /**
   * 查询会员患者关联信息
   *
   * @param id 会员患者关联信息主键
   * @return 会员患者关联信息
   */
  @Override
  public MemberPatient selectMemberPatientById(Long id) {
    return memberPatientMapper.selectMemberPatientById(id);
  }

  /**
   * 查询会员患者关联信息列表
   *
   * @param memberPatient 会员患者关联信息
   * @return 会员患者关联信息
   */
  @Override
  public List<MemberPatient> selectMemberPatientList(MemberPatient memberPatient) {
    return memberPatientMapper.selectMemberPatientList(memberPatient);
  }

  @Override
  public List<MemberPatientVo> queryList(MemberPatient memberPatient) {
    return baseMapper.queryList(memberPatient);
  }

  /**
   * 新增会员患者关联信息
   *
   * @param memberPatient 会员患者关联信息
   * @return 结果
   */
  @Transactional(rollbackFor = Exception.class)
  @Override
  public AjaxResult insertMemberPatient(MemberPatient memberPatient) {
    int i = memberPatientMapper.insertMemberPatient(memberPatient);
    if (i > 0) {
      // 发送短信
      sendSms(memberPatient, 4);//短信的类型：4.会员钱包绑定患者
    }

    return AjaxResult.success("操作成功！");
  }

  @Transactional(rollbackFor = Exception.class)
  @Override
  public AjaxResult unbind(MemberPatient memberPatient) {
    int i = memberPatientMapper.deleteByPatientIdAndMemberCardNo(memberPatient);
    if (i > 0) {
      // 发送短信
      sendSms(memberPatient, 5);//短信的类型：5.会员钱包解绑患者
    }
    return AjaxResult.success("操作成功！");
  }

  /**
   * 会员钱包绑定/解绑患者·短信发送
   * @param memberPatient
   * @param msgTypes
   */
  private void sendSms(MemberPatient memberPatient, int msgTypes) {
    // 发消息
    MemberMsgInfo msgInfo = new MemberMsgInfo();
    msgInfo.setMemberCardNo(memberPatient.getMemberCardNo());

    // 填充绑定的患者名称
    PatientInfo patientInfo = patientInfoService.selectPatientInfoById(memberPatient.getPatientId());
    msgInfo.setPatientName(patientInfo.getPatientName());

    // 填充绑定的钱包，积分
    MemberInfo memberInfo = memberInfoMapper.memberInfoByCardNo(msgInfo.getMemberCardNo());
    msgInfo.setWalletBalance(memberInfo.getWalletBalance());
    msgInfo.setIntegralBalance(memberInfo.getIntegralBalance());

    // 填充当前的操作部门等信息
    SysUser user = SecurityUtils.getLoginUser().getUser();
    msgInfo.setDeptId(user.getDeptId());
    msgInfo.setOperateStores(user.getDeptName());
    SysDept dept = sysDeptService.selectDeptById(user.getDeptId());
    if(null != dept){
      msgInfo.setStorePhone(dept.getPhone());
    }
    msgInfo.setTenantId(user.getTenantId());
    sysTenantNotifyItemService.sendMemberInfoMsg(msgInfo, msgTypes, user);
  }

  /**
   * 修改会员患者关联信息
   *
   * @param memberPatient 会员患者关联信息
   * @return 结果
   */
  @Override
  public int updateMemberPatient(MemberPatient memberPatient) {
    return memberPatientMapper.updateMemberPatient(memberPatient);
  }

  /**
   * 批量删除会员患者关联信息
   *
   * @param ids 需要删除的会员患者关联信息主键
   * @return 结果
   */
  @Override
  public int deleteMemberPatientByIds(Long[] ids) {
    return memberPatientMapper.deleteMemberPatientByIds(ids);
  }

  /**
   * 删除会员患者关联信息信息
   *
   * @param id 会员患者关联信息主键
   * @return 结果
   */
  @Override
  public int deleteMemberPatientById(Long id) {
    return memberPatientMapper.deleteMemberPatientById(id);
  }

  /**
   * 根据患者id查询会员患者关联信息
   *
   * @param patientId 患者id
   * @return 会员患者关联信息
   */
  @Override
  public MemberPatient selectByPatientId(Long patientId) {
    LambdaQueryWrapper<MemberPatient> wrapper = Wrappers.lambdaQuery(MemberPatient.class);
    wrapper.eq(MemberPatient::getPatientId, patientId);
    List<MemberPatient> list = memberPatientMapper.selectList(wrapper);
    return CollectionUtils.isEmpty(list) ? null : list.get(0);
  }

}
