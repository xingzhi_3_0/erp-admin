package com.xz.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.annotation.DataScope;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.exception.ServiceException;
import com.xz.common.utils.StringUtils;
import com.xz.diagnosis.domain.DiagnosisCause;
import com.xz.member.domain.MemberInfo;
import com.xz.member.mapper.MemberInfoMapper;
import com.xz.member.param.MemberQueryParam;
import com.xz.member.service.IMemberInfoService;
import com.xz.member.vo.MemberImportVo;
import com.xz.member.vo.MemberInfoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 客户信息Service业务层处理
 *
 * @author xz
 * @date 2024-1-22 11:10:32
 */
@Service
public class MemberInfoServiceImpl extends ServiceImpl<MemberInfoMapper, MemberInfo> implements IMemberInfoService {

    @Override
    @DataScope(deptAlias = "d", userAlias = "u")
    public List<MemberInfoVo> queryList(MemberQueryParam queryParam){
        List<MemberInfoVo> memberInfoVos = baseMapper.queryList(queryParam);
        return memberInfoVos;
    }

    @Override
    public MemberInfoVo memberDetailById(Long id){
        MemberInfoVo memberInfoVo = baseMapper.memberDetailById(id);
//        List<ReturnProductVo> returnProductVoList = returnProductMapper.findListByReturnId(id);
//        returnInfoVo.setReturnProductVoList(returnProductVoList);
        return memberInfoVo;
    }

    @Override
    public AjaxResult edit(MemberInfo memberInfo){
        if(Objects.isNull(memberInfo.getId())){
            return AjaxResult.error("数据不存在");
        }
        baseMapper.updateById(memberInfo);
        return AjaxResult.success("修改成功");
    }

    @Override
    public AjaxResult importExcel(List<MemberImportVo> list){
        if(CollectionUtils.isEmpty(list)){
            return AjaxResult.error("导入数据不能为空");
        }
        List<MemberImportVo> newList = new ArrayList<>();
        //会员数据
        StringBuilder errorMsg=new StringBuilder();
        Map<String, List<MemberImportVo>> collectMap = list.stream().collect(Collectors.groupingBy(MemberImportVo::getPhoneNumber));
        for (Map.Entry<String, List<MemberImportVo>> entry : collectMap.entrySet()) {
            List<MemberImportVo> value = entry.getValue();
            if(value.size()>1){
                errorMsg.append("<br/>【会员号：" + entry.getKey() + "】重复! ");
            }else{
                newList.addAll(value);
            }
        }

        List<MemberInfo> memberInfos = baseMapper.selectAllList();
        Map<String,MemberInfo> map = new HashMap<>();
        for (MemberInfo memberInfo:memberInfos) {
            map.put(memberInfo.getPhoneNumber(),memberInfo);
        }

        List<MemberInfo> memberInfoList = new ArrayList<>();
        for(MemberImportVo importVo:newList){
            if(Objects.nonNull(map.get(importVo.getPhoneNumber()))){
                errorMsg.append("<br/>【会员号:" + importVo.getPhoneNumber() + "】已存在! ");
                continue;
            }
            MemberInfo memberInfo = new MemberInfo();
            BeanUtils.copyProperties(importVo,memberInfo);
            memberInfoList.add(memberInfo);
        }
        if(memberInfoList.size()==0){
            return AjaxResult.error("导入失败"+(StringUtils.isEmpty(errorMsg)?"":","+errorMsg.toString()));
        }
        boolean savedBatch = this.saveBatch(memberInfoList);
        if(!savedBatch){
            throw new ServiceException("导入失败");
        }
        return AjaxResult.success("导入成功"+(StringUtils.isEmpty(errorMsg)?"":",部分数导入失败<br/>"+errorMsg.toString()));

    }

  @Override
  public MemberInfo selectByMemberCarNo(String cardNo) {
    LambdaQueryWrapper<MemberInfo> wrapper = Wrappers.lambdaQuery(MemberInfo.class);
    wrapper.eq(MemberInfo::getMemberCardNo, cardNo);
    wrapper.eq(MemberInfo::getDelFlag, 0);
    return baseMapper.selectOne(wrapper);
  }
}
