package com.xz.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.member.domain.MemberInfo;
import com.xz.member.param.MemberQueryParam;
import com.xz.member.vo.MemberImportVo;
import com.xz.member.vo.MemberInfoVo;
import com.xz.purchase.domain.AdjustInfo;
import com.xz.purchase.param.AdjustInfoParam;
import com.xz.purchase.vo.AdjustInfoVo;

import java.util.List;

/**
 * 客户信心Service接口
 *
 * @author xz
 * @date 2024-1-22 11:07:04
 */
public interface IMemberInfoService extends IService<MemberInfo> {

  /**
   * 查询列表
   *
   * @param queryParam
   * @return
   */
  List<MemberInfoVo> queryList(MemberQueryParam queryParam);

  /**
   * 根据id获取会员详情
   *
   * @param id
   * @return
   */
  MemberInfoVo memberDetailById(Long id);

  /**
   * 会员修改
   *
   * @param memberInfo
   * @return
   */
  AjaxResult edit(MemberInfo memberInfo);

  /**
   * 会员导入
   *
   * @param list
   * @return
   */
  AjaxResult importExcel(List<MemberImportVo> list);

  /**
   * 根据卡号查找会员信息
   *
   * @param cardNo
   * @return
   */
  MemberInfo selectByMemberCarNo(String cardNo);
}
