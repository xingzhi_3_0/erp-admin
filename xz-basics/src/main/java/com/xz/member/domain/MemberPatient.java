package com.xz.member.domain;

import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 会员患者关联信息对象 t_member_patient
 * 
 * @author xz
 * @date 2024-01-31
 */
@Data
@TableName("t_member_patient")
public class MemberPatient{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 会员卡号 */
    @Excel(name = "会员卡号")
    private String memberCardNo;

    /** 患者id */
    @Excel(name = "患者id")
    private Long patientId;

    /** 关系（1本人. 2.配偶, 3-子女 4-父母, 5-亲属, 6-朋友, 7-其他） */
    @Excel(name = "关系", readConverterExp = "1=本人.,2=.配偶,,3=-子女,4=-父母,,5=-亲属,,6=-朋友,,7=-其他")
    private Integer relationship;

    /** 绑定状态（1-已绑定. 2-解除绑定 ） */
    @Excel(name = "绑定状态", readConverterExp = "1=-已绑定.,2=-解除绑定")
    private Integer bindStatus;

}
