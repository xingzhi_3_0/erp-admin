package com.xz.member.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 会员等级对象 t_member_level
 * 
 * @author xz
 * @date 2024-1-23 14:01:35
 */
@Data
@TableName("t_member_level")
public class MemberLevel extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 会员等级 */
    private String levelNo;

    /** 会员名称 */
    private String levelName;

    /** 会员规则 0-无规则 1-累计消费额 2-累计充值 */
    private Integer ruleType;

    /** 规则-累计充值额 */
    private BigDecimal accumulateAmount;

    /** 折扣 几折 */
    @Excel(name = "折扣-几折 0-100")
    private BigDecimal discount;

    /** 权益说明 */
    private String discountExplain;

    /** 使用须知 */
    private String useExplain;

    /** 状态0-停用. 1.启用 */
    @Excel(name = "状态", readConverterExp = "0=.停用,1=.启用")
    private Integer status;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

}
