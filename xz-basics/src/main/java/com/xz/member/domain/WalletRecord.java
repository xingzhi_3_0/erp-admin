package com.xz.member.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 钱包记录对象
 *
 * @author xz
 * @date 2024-1-24 16:51:21
 */
@Data
@TableName("t_wallet_record")
public class WalletRecord extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    @TableField(exist = false)
    private Long memberId;

    /** 会员卡号 */
    private String memberCardNo;

    /** 变动类型（1-充值. 2.钱包退款 3-销售单退款 4-销售单消费 5-诊疗单退款 6-诊疗单消费 7-挂号单退款 8-挂号单消费） */
    private Integer changeType;

    /** 变动值类型（1-加. 2.减 */
    private Integer changeValueType;

    /** 变动值  */
    private BigDecimal changeValue;

    /** 关联单号 */
    private String relatedNo;

    /** 状态 1.充值成功 2-扣费成功 3-待退款 4-退款成功 5-充值作废 6-退款作废 */
    private Integer status;

    /** 活动id（充值用） */
    private Long activityId;

    /** 是否注销会员（充值用） */
    private Integer whetherCancel;

    /** 充值方式（充值用） 1-活动充值. 2.自由充值 */
    private Integer rechargeMethod;

    /** 支付方式（充值用） 1-现金 2-银行卡 3-E支付 */
    private Integer paymentMethod;

    /** 操作部门 */
    private Long deptId;

    /** 操作部门名称 */
    private String deptName;

    /** 操作人名称 */
    private String operateUser;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;

    /***
     * 调整会员等级
     */
    @TableField(exist = false)
    private Long adjustLevelId;

    /***
     * 调整会员等级
     */
    @TableField(exist = false)
    private String adjustLevel;

    /***
     * 活动名称
     */
    @TableField(exist = false)
    private String activityName;

    /** 会员名称 */
    @TableField(exist = false)
    private String memberName;
    /** 会员信息检索 */
    @TableField(exist = false)
    private String searchMember;
    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
    /**
     * 状态
     */
    @TableField(exist = false)
    public List<Integer> statusList;
    /**
     * 状态
     */
    @TableField(exist = false)
    public List<Integer> changeTypeList;
}
