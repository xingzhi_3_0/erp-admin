package com.xz.member.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 采购库存调整对象 t_adjust_info
 * 
 * @author xz
 * @date 2024-1-23 15:39:42
 */
@Data
@TableName("t_member_rule_setting")
public class MemberRuleSetting extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 设置类型 1-钱包设置 2-积分设置 */
    private Integer setType;

    /** 规则序号 第几个序号 */
    private Integer ruleSerialNo;

    /** 规则值 */
    private String ruleValue;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

}
