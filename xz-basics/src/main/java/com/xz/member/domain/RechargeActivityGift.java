package com.xz.member.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 采购库存调整对象 t_adjust_info
 * 
 * @author xz
 * @date 2024-1-16 14:10:46
 */
@Data
@TableName("t_recharge_activity_gift")
public class RechargeActivityGift {
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 活动id */
    private Long activityId;

    /** 类别 */
    private String giftCategory;

    /** 充值金额 */
    private BigDecimal giftValue;

}
