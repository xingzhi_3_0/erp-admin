package com.xz.member.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 采购库存调整对象 t_adjust_info
 * 
 * @author xz
 * @date 2024-1-16 14:10:46
 */
@Data
@TableName("t_gift_receive")
public class GiftReceive extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /**  会员id */
    @TableField(exist = false)
    private String memberId;

    /** 会员卡号 */
    private String memberCardNo;

    /** 赠品名称 */
    private String giftName;

    /** 赠品价值 */
    private BigDecimal giftValue;

    /** 领取原因 */
    private String receiveReason;

    /** 状态（0-未核销. 1.已核销 2-已作废 3-占用中） */
    @Excel(name = "核销状态", readConverterExp = "1=.已核销,0=.未核销 3-核销占用中")
    private Integer status;

    @Excel(name = "关联充值单号")
    private String relatedRechargeNo;

    @Excel(name = "关联销售单号")
    private String relatedSalesNo;

    /** 核销人id */
    private Long writtenOffUserId;

    /** 核销人 */
    private String writtenOffUser;

    /** 核销部门id */
    private Long writtenOffDeptId;

    /** 核销部门 */
    private String writtenOffDept;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;


    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;

}
