package com.xz.member.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 积分记录对象 t_adjust_info
 * 
 * @author xz
 * @date 2024-1-24 15:10:20
 */
@Data
@TableName("t_integral_record")
public class IntegralRecord extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /**  会员id */
    @TableField(exist = false)
    private String memberId;

    /** 会员卡号 */
    private String memberCardNo;


    /** 变动类型（1-手动增加. 2.手动减少 3-销售单消费 4-销售单退款） */
    private Integer changeType;

    /** 变动值类型（1-加. 2.减 */
    private Integer changeValueType;

    /** 变动值  */
    private BigDecimal changeValue;

    /** 关联单号 */
    private String relatedNo;

    /** 操作部门 */
    private Long deptId;

    /** 操作部门名称 */
    private String deptName;

    /** 操作人名称 */
    private String operateUser;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;

}
