package com.xz.member.domain;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 采购库存调整对象 t_adjust_info
 *
 * @author xz
 * @date 2024-1-16 14:10:46
 */
@Data
@TableName("t_member_info")
public class MemberInfo extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 会员卡号 */
    private String memberCardNo;

    /** 会员名称 */
    @Excel(name = "会员名称")
    private String memberName;

    /** 性别 ） */
    @Excel(name = "性别", readConverterExp = "0=.不详,1=.男,2=.女")
    private Integer sex;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phoneNumber;

    /** 出生日期 */
    @Excel(name = "出生日期")
    private Date birthday;

    /** 会员等级id */
    private Long memberLevelId;

    /** 会员等级 */
    private String memberLevel;

    /** 钱包余额 */
    @Excel(name = "钱包余额")
    private BigDecimal walletBalance;

    /** 积分余额 */
    @Excel(name = "积分余额")
    private BigDecimal integralBalance;

    /** 累计充值 */
    @Excel(name = "累计充值")
    private BigDecimal accumulatedRecharge;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    @TableField(exist = false)
    private Integer age;

    /**
     * 状态
     */
    @TableField(exist = false)
    public List<Integer> statusList;
    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;

    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

    /** 门店联系电话 */
    @TableField(exist = false)
    private String storePhone;
}
