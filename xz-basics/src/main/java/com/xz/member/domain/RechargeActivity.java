package com.xz.member.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 采购库存调整对象 t_adjust_info
 * 
 * @author xz
 * @date 2024-1-16 14:10:46
 */
@Data
@TableName("t_recharge_activity")
public class RechargeActivity extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 活动名称 */
    private String activityName;

    /** 充值金额 */
    @Excel(name = "充值金额")
    private BigDecimal recharge;

    /** 活动起止时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /** 手机号 */
    @Excel(name = "活动结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    /** 有无赠品 0-无 1-有 */
    @Excel(name = "有无赠品", readConverterExp = "0=.无,1=.有")
    private Integer ifGift;

    @Excel(name = "赠品数量")
    private Integer giftNumber;

    /** 赠品价值 */
    private BigDecimal giftTotalValue;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    /**
     * */
    @Excel(name = "活动状态", readConverterExp = "1=.未开始,2=.进行中,3=.已结束,4=.已作废")
    private Integer status;

    /** 会员等级ID */
    private Long memberLevelId;

    @TableField(exist = false)
    private BigDecimal discount;

    /**
     * 状态
     */
    @TableField(exist = false)
    public List<Integer> statusList;
    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;

    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

}
