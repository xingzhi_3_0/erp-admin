package com.xz.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.member.domain.MemberRuleSetting;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 会员规则设置Mapper接口
 * 
 * @author xz
 * @date 2024-1-23 15:41:48
 */
@Mapper
public interface MemberRuleSettingMapper extends BaseMapper<MemberRuleSetting> {

    List<MemberRuleSetting> findBySetType(@Param("setType") Integer setType);

    int deleteBySetTypeAfter(@Param("setType") Integer setType);
}
