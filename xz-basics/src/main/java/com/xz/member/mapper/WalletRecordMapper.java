package com.xz.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.member.domain.IntegralRecord;
import com.xz.member.domain.WalletRecord;
import com.xz.member.vo.WalletRecordListVo;
import com.xz.report.param.SalesReportParam;
import com.xz.staging.vo.FoldLineStagingVo;
import com.xz.staging.vo.WorkbenchesVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;


/**
 * 钱包记录信息Mapper接口
 * 
 * @author xz
 * @date 2024-1-24 16:52:41
 */
@Mapper
public interface WalletRecordMapper extends BaseMapper<WalletRecord> {

    List<WalletRecord> queryList(WalletRecord walletRecord);

    BigDecimal sumMemberWallet(@Param("memberCardNo") String memberCardNo);

    WorkbenchesVo rechargeWorkbenches(SalesReportParam salesReportParam);

    List<FoldLineStagingVo> foldLineStatistics(SalesReportParam salesReportParam);

    List<FoldLineStagingVo> foldLineMonthList(SalesReportParam salesReportParam);

    /**
     * 查询WalletRecordListVo集合
     * @param walletRecord
     * @return
     */
    List<WalletRecordListVo> queryVoList(WalletRecord walletRecord);

}
