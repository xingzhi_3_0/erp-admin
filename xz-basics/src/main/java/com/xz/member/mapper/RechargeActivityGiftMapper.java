package com.xz.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.member.domain.RechargeActivity;
import com.xz.member.domain.RechargeActivityGift;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 会员信息Mapper接口
 * 
 * @author xz
 * @date 2024-1-23 10:58:48
 */
@Mapper
public interface RechargeActivityGiftMapper extends BaseMapper<RechargeActivityGift> {

    List<RechargeActivityGift> findListByActivityId(@Param("activityId")Long activityId);

    int deleteByActivityId(@Param("activityId")Long activityId);

}
