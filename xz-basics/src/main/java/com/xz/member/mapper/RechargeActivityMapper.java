package com.xz.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.member.domain.RechargeActivity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * 会员信息Mapper接口
 * 
 * @author xz
 * @date 2024-1-23 10:58:48
 */
@Mapper
public interface RechargeActivityMapper extends BaseMapper<RechargeActivity> {

    List<RechargeActivity> queryList(RechargeActivity activity);

    int stopActivity();

    int startActivity();
}
