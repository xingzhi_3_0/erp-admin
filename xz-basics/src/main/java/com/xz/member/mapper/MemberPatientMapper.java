package com.xz.member.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.member.vo.MemberPatientVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.member.domain.MemberPatient;

/**
 * 会员患者关联信息Mapper接口
 * 
 * @author xz
 * @date 2024-01-31
 */
@Mapper
public interface MemberPatientMapper  extends BaseMapper<MemberPatient> {
    /**
     * 查询会员患者关联信息
     * 
     * @param id 会员患者关联信息主键
     * @return 会员患者关联信息
     */
    public MemberPatient selectMemberPatientById(Long id);

    /**
     * 查询会员患者关联信息列表
     * 
     * @param memberPatient 会员患者关联信息
     * @return 会员患者关联信息集合
     */
    public List<MemberPatient> selectMemberPatientList(MemberPatient memberPatient);

    List<MemberPatientVo> queryList(MemberPatient memberPatient);


    /**
     * 新增会员患者关联信息
     * 
     * @param memberPatient 会员患者关联信息
     * @return 结果
     */
    public int insertMemberPatient(MemberPatient memberPatient);

    /**
     * 修改会员患者关联信息
     * 
     * @param memberPatient 会员患者关联信息
     * @return 结果
     */
    public int updateMemberPatient(MemberPatient memberPatient);

    /**
     * 删除会员患者关联信息
     * 
     * @param id 会员患者关联信息主键
     * @return 结果
     */
    public int deleteMemberPatientById(Long id);

    /**
     * 批量删除会员患者关联信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMemberPatientByIds(Long[] ids);

    int deleteByPatientIdAndMemberCardNo(MemberPatient memberPatient);
}
