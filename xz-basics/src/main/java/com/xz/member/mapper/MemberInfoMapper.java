package com.xz.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.member.domain.MemberInfo;
import com.xz.member.param.MemberQueryParam;
import com.xz.member.vo.MemberInfoVo;
import com.xz.patient.domain.PatientInfo;
import com.xz.report.param.SalesReportParam;
import com.xz.staging.vo.WorkbenchesVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;


/**
 * 会员信息Mapper接口
 *
 * @author xz
 * @date 2024-1-22 10:22:34
 */
@Mapper
public interface MemberInfoMapper extends BaseMapper<MemberInfo> {

  /**
   * 查询会员列表
   *
   * @param queryParam
   * @return
   */
  List<MemberInfoVo> queryList(@Param("query") MemberQueryParam queryParam);

  /**
   * 根据id查询会员信息
   *
   * @param id
   * @return
   */
  MemberInfoVo memberDetailById(@Param("id") Long id);

  /**
   * 取消会员
   *
   * @param id
   * @return
   */
  int cancelMember(@Param("id") Long id);

  /**
   * 根据会员卡号查询会员信息
   *
   * @param memberCardNo
   * @return
   */
  MemberInfo memberInfoByCardNo(@Param("memberCardNo") String memberCardNo);

  /**
   * 根据客户id查询会员信息
   *
   * @param patientId
   * @return
   */
  MemberInfo memberInfoByPatientId(@Param("patientId") Long patientId);

  /**
   * 查询所有会员信息
   *
   * @return
   */
  List<MemberInfo> selectAllList();

  /**
   * 查询会员信息（工作台）
   *
   * @param salesReportParam
   * @return
   */
  WorkbenchesVo memberWorkbenches(SalesReportParam salesReportParam);

}
