package com.xz.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.member.domain.GiftReceive;
import com.xz.member.domain.IntegralRecord;
import com.xz.member.vo.IntegralRecordVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;


/**
 * 会员信息Mapper接口
 * 
 * @author xz
 * @date 2024-1-24 11:41:38
 */
@Mapper
public interface IntegralRecordMapper extends BaseMapper<IntegralRecord> {

    List<IntegralRecord> queryList(IntegralRecord integralRecord);

    BigDecimal sumMemberIntegral(@Param("memberCardNo") String memberCardNo);
}
