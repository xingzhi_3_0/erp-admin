package com.xz.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.member.domain.GiftReceive;
import com.xz.member.domain.RechargeActivity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 会员信息Mapper接口
 * 
 * @author xz
 * @date 2024-1-24 11:41:38
 */
@Mapper
public interface GiftReceiveMapper extends BaseMapper<GiftReceive> {

    List<GiftReceive> queryList(GiftReceive giftReceive);

    List<GiftReceive> selectIdList(@Param("idStr") String idStr);

    int updateByGiftReceiveId(GiftReceive giftReceive);
}
