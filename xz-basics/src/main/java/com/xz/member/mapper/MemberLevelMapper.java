package com.xz.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.member.domain.MemberLevel;
import com.xz.member.domain.RechargeActivity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 会员信息Mapper接口
 *
 * @author xz
 * @date 2024-1-23 10:58:48
 */
@Mapper
public interface MemberLevelMapper extends BaseMapper<MemberLevel> {

  List<MemberLevel> queryList(MemberLevel memberLevel);

  List<MemberLevel> findList();

  /**
   * 根据会员卡号查询会员等级
   * @param memberCardNo
   * @return
   */
  MemberLevel getByMemberCardNo(@Param("memberCardNo") String memberCardNo);
}
