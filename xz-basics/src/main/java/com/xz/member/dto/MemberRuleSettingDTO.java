package com.xz.member.dto;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.util.List;

@Data
public class MemberRuleSettingDTO {

    /** 1-钱包设置  2-积分设置 */
    private Integer setType;
    /** 设置数据集合 */
    private List<JSONObject> queryParam;
}
