package com.xz.member.param;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.member.domain.MemberInfo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class MemberQueryParam extends MemberInfo {


    private String searchMember;

    private BigDecimal startWallet;

    private BigDecimal endWallet;

    private BigDecimal startRecharge;

    private BigDecimal endRecharge;

    private BigDecimal startIntegral;

    private BigDecimal endIntegral;

    private Long deptId;
}
