package com.xz.common.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2022/3/3 9:17
 * @description：日志列表
 */
@Data
public class OperationLogVo {
    /** 操作人名称 */
    private String createName;
    //操作时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    //操作记录
    private String remark;
}
