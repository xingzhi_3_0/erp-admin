package com.xz.common.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.supplier.dto.SupplierAccessoryDto;
import org.apache.ibatis.annotations.Mapper;
import com.xz.common.domain.SysAccessory;
import org.apache.ibatis.annotations.Param;

/**
 * 附件Mapper接口
 * 
 * @author xz
 * @date 2022-03-01
 */
@Mapper
public interface SysAccessoryMapper  extends BaseMapper<SysAccessory> {
    /**
     * 查询附件
     * 
     * @param id 附件主键
     * @return 附件
     */
    public SysAccessory selectSysAccessoryById(Long id);

    /**
     * 查询附件列表
     * 
     * @param sysAccessory 附件
     * @return 附件集合
     */
    public List<SysAccessory> selectSysAccessoryList(SysAccessory sysAccessory);

    /**
     * 新增附件
     * 
     * @param sysAccessory 附件
     * @return 结果
     */
    public int insertSysAccessory(SysAccessory sysAccessory);

    /**
     * 修改附件
     * 
     * @param sysAccessory 附件
     * @return 结果
     */
    public int updateSysAccessory(SysAccessory sysAccessory);

    /**
     * 删除附件
     * 
     * @param id 附件主键
     * @return 结果
     */
    public int deleteSysAccessoryById(Long id);

    /**
     * 批量删除附件
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysAccessoryByIds(Long[] ids);

    /**
     * 根据业务id删除附件
     * @author ZhangZhiJia
     * @date 2022/3/2 14:12
     * @param bizId 业务id
     * @return int
     */
    int deleteSysAccessoryByBizId(String bizId,Integer bizType);

    /**
     * 供应商附件
     * @param bizId
     * @param bizType
     * @return
     */
    List<SupplierAccessoryDto> selectSupplierAccessoryList(String bizId, Integer bizType);

    /**
     * 附件
     * @param bizId
     * @param bizType
     * @return
     */
    List<String> selectSupplierAccessoryUrlList(String bizId, Integer bizType);

}
