package com.xz.common.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.xz.common.config.XzConfig;
import com.xz.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.utils.SecurityUtils;
import com.xz.common.utils.file.FileUtils;
import com.xz.supplier.dto.SupplierAccessoryDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import com.xz.common.mapper.SysAccessoryMapper;
import com.xz.common.domain.SysAccessory;
import com.xz.common.service.ISysAccessoryService;
import org.springframework.util.CollectionUtils;

/**
 * 附件Service业务层处理
 *
 * @author xz
 * @date 2022-03-01
 */
@Service("ISysAccessoryService")
public class SysAccessoryServiceImpl extends ServiceImpl<SysAccessoryMapper, SysAccessory> implements ISysAccessoryService {
    @Autowired
    private SysAccessoryMapper sysAccessoryMapper;
    @Autowired
    private XzConfig xzConfig;
    /**
     * 查询附件
     *
     * @param id 附件主键
     * @return 附件
     */
    @Override
    public SysAccessory selectSysAccessoryById(Long id) {
        return sysAccessoryMapper.selectSysAccessoryById(id);
    }

    /**
     * 查询附件列表
     *
     * @param sysAccessory 附件
     * @return 附件
     */
    @Override
    public List<SysAccessory> selectSysAccessoryList(SysAccessory sysAccessory) {
        return sysAccessoryMapper.selectSysAccessoryList(sysAccessory);
    }

    /**
     * 新增附件
     *
     * @param sysAccessory 附件
     * @return 结果
     */
    @Override
    public int insertSysAccessory(SysAccessory sysAccessory) {
        sysAccessory.setCreateTime(DateUtils.getNowDate());
        return sysAccessoryMapper.insertSysAccessory(sysAccessory);
    }

    /**
     * 修改附件
     *
     * @param sysAccessory 附件
     * @return 结果
     */
    @Override
    public int updateSysAccessory(SysAccessory sysAccessory) {
        sysAccessory.setUpdateTime(DateUtils.getNowDate());
        return sysAccessoryMapper.updateSysAccessory(sysAccessory);
    }

    /**
     * 批量删除附件
     *
     * @param ids 需要删除的附件主键
     * @return 结果
     */
    @Override
    public int deleteSysAccessoryByIds(Long[] ids) {
        return sysAccessoryMapper.deleteSysAccessoryByIds(ids);
    }

    /**
     * 删除附件信息
     *
     * @param id 附件主键
     * @return 结果
     */
    @Override
    public int deleteSysAccessoryById(Long id) {
        return sysAccessoryMapper.deleteSysAccessoryById(id);
    }

    @Override
    public int deleteSysAccessoryByBizId(String bizId, Integer bizType) {
        SysAccessory sysAccessory=new SysAccessory();
        sysAccessory.setBizId(bizId);
        sysAccessory.setBizType(bizType);
        List<SysAccessory> accessoryList = sysAccessoryMapper.selectSysAccessoryList(sysAccessory);
        for ( SysAccessory accessory:accessoryList){
            String filePath = xzConfig.getProfile() + accessory.getUrl();
            FileUtils.deleteFile(filePath);
        }

        return sysAccessoryMapper.deleteSysAccessoryByBizId(bizId,bizType);
    }

    /**
     * 生成附件对象--单个
     * @param bizNo 业务编号
     * @param bizType 业务联系
     * @param tenantId 租户id
     * @param fileType 附件类型 1、图片 2 文件
     * @param url 附件
     * @return
     */
    public void generateObject(String bizNo, Integer bizType,Long tenantId,Integer fileType,String url,String operator) {
        SysAccessory sysAccessory=new SysAccessory();
        sysAccessory.setBizId(bizNo);
        sysAccessory.setBizType(bizType);
        sysAccessory.setTenantId(tenantId);
        sysAccessory.setFileType(fileType);
        sysAccessory.setFileName(url.substring(0,url.lastIndexOf(".")));
        sysAccessory.setUrl(url);
        sysAccessory.setCreateTime(new Date());
        sysAccessory.setUpdateTime(new Date());
        baseMapper.insert(sysAccessory);
    }

    /**
     * 生成附件对象--多图
     * @param bizNo 业务编号
     * @param bizType 业务联系
     * @param tenantId 租户id
     * @param fileType 附件类型 1、图片 2 文件
     * @param url 附件
     * @return
     */
    public void generateObjectMore(String bizNo, Integer bizType, Long tenantId, Integer fileType, List<String> url,String operator) {
        SysAccessory sysAccessory=null;
        for(String str:url){
            if(StringUtils.isBlank(str)){
                continue;
            }
            sysAccessory=new SysAccessory();
            sysAccessory.setBizId(bizNo);
            sysAccessory.setBizType(bizType);
            sysAccessory.setTenantId(tenantId);
            sysAccessory.setFileType(fileType);
            sysAccessory.setFileName(str.substring(0,str.lastIndexOf(".")));
            sysAccessory.setUrl(str);
            sysAccessory.setCreateTime(new Date());
            sysAccessory.setUpdateTime(new Date());
            baseMapper.insert(sysAccessory);
        }
    }

    /**
     * 批量插入附件
     * @param accessoryList 附件
     * @param bizId 业务id
     * @param bizType 业务类型  （1.供应商）
     * @param fileType 文件类型 1、图片 2 文件
     * @return
     */
    @Override
    public Boolean insertUrlList(List<Object> accessoryList, String bizId, Integer bizType, Integer fileType) {
        if(!CollectionUtils.isEmpty(accessoryList)){
            List<SysAccessory> sysAccessoryList= JSON.parseArray(JSON.toJSONString(accessoryList), SysAccessory.class);
            sysAccessoryList.stream().forEach(sysAccessory->{
                sysAccessory.setBizId(bizId);
                sysAccessory.setBizType(bizType);
                sysAccessory.setFileType(fileType);
                sysAccessory.setCreateTime(DateUtils.getNowDate());
                sysAccessory.setCreateBy(SecurityUtils.getUserId());
                sysAccessory.setTenantId(SecurityUtils.getLoginUser().getTenantId());
            });
            return this.saveBatch(sysAccessoryList);
        }
        return false;
    }
    /**
     * 批量插入附件
     * @param accessoryList 附件
     * @param bizId 业务id
     * @param bizType 业务类型  （1.供应商 2-验光配镜 3-销售退货凭证 4-销售明细参数附件 5-挂号检查退费附件）
     * @param fileType 文件类型 1、图片 2 文件
     * @return
     */
    @Override
    public Boolean insertStringUrlList(List<String> accessoryList, String bizId, Integer bizType, Integer fileType) {
        if(!CollectionUtils.isEmpty(accessoryList)){
            List<SysAccessory> sysAccessoryList =new ArrayList<>();
            accessoryList.stream().forEach(item->{
                SysAccessory sysAccessory=new SysAccessory();
                sysAccessory.setUrl(item);
                sysAccessory.setBizId(bizId);
                sysAccessory.setBizType(bizType);
                sysAccessory.setFileType(fileType);
                sysAccessory.setCreateTime(DateUtils.getNowDate());
                sysAccessory.setCreateBy(SecurityUtils.getUserId());
                sysAccessory.setTenantId(SecurityUtils.getLoginUser().getTenantId());
                sysAccessoryList.add(sysAccessory);
            });
            return this.saveBatch(sysAccessoryList);
        }
        return false;
    }

    /**
     * 供应商附件
     * @param bizId
     * @param bizType
     * @return
     */
    @Override
    public List<SupplierAccessoryDto> selectSupplierAccessoryList(String bizId, Integer bizType) {
        return baseMapper.selectSupplierAccessoryList(bizId,bizType);
    }

    /**
     * 附件
     * @param bizId
     * @param bizType
     * @return
     */
    @Override
    public List<String> selectSupplierAccessoryUrlList(String bizId, Integer bizType) {
        return baseMapper.selectSupplierAccessoryUrlList(bizId,bizType);
    }
}
