package com.xz.common.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.domain.SysAccessory;
import com.xz.supplier.dto.SupplierAccessoryDto;

/**
 * 附件Service接口
 * 
 * @author xz
 * @date 2022-03-01
 */
public interface ISysAccessoryService extends IService<SysAccessory> {
    /**
     * 查询附件
     * 
     * @param id 附件主键
     * @return 附件
     */
    public SysAccessory selectSysAccessoryById(Long id);

    /**
     * 查询附件列表
     * 
     * @param sysAccessory 附件
     * @return 附件集合
     */
    public List<SysAccessory> selectSysAccessoryList(SysAccessory sysAccessory);

    /**
     * 新增附件
     * 
     * @param sysAccessory 附件
     * @return 结果
     */
    public int insertSysAccessory(SysAccessory sysAccessory);

    /**
     * 修改附件
     * 
     * @param sysAccessory 附件
     * @return 结果
     */
    public int updateSysAccessory(SysAccessory sysAccessory);

    /**
     * 批量删除附件
     * 
     * @param ids 需要删除的附件主键集合
     * @return 结果
     */
    public int deleteSysAccessoryByIds(Long[] ids);

    /**
     * 删除附件信息
     * 
     * @param id 附件主键
     * @return 结果
     */
    public int deleteSysAccessoryById(Long id);

    /**
     * 批量插入附件
     * @param accessoryList 附件
     * @param bizId 业务id
     * @param bizType 业务类型  （1.供应商）
     * @param fileType 文件类型 1、图片 2 文件
     * @return
     */
    Boolean insertUrlList(List<Object> accessoryList,String bizId,Integer bizType,Integer fileType);
    Boolean insertStringUrlList(List<String> accessoryList,String bizId,Integer bizType,Integer fileType);


    /**
     *
     * @author ZhangZhiJia
     * @date 2022/3/2 14:11
     * @param bizId 业务id
     * @return int
     */
    public int deleteSysAccessoryByBizId(String bizId, Integer bizType);
    /**
     * 生成附件对象--单个
     */
    void generateObject(String bizNo, Integer bizType,Long tenantId,Integer fileType,String url,String operator);
    /**
     * 生成附件对象--多图
     */
    void generateObjectMore(String bizNo, Integer bizType,Long tenantId,Integer fileType,List<String> url,String operator);
    /**
     * 供应商附件
     * @param bizId
     * @param bizType
     * @return
     */
    List<SupplierAccessoryDto> selectSupplierAccessoryList(String bizId, Integer bizType);

    /**
     * 附件
     * @param bizId
     * @param bizType
     * @return
     */
    List<String> selectSupplierAccessoryUrlList(String bizId, Integer bizType);
}
