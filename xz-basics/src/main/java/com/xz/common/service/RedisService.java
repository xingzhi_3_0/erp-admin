package com.xz.common.service;

/**
 * @Description: ${Description}
 * @ClassName: RedisService
 * @Author: daiyuanbao
 * @Date: 2022/1/21 13:30
 */
public interface RedisService {
    /**
     * 新增key
     * @param key
     * @param value
     */
    void addKey(String key, String value);


    /**
     * 新增key
     * @param key
     * @param value
     */
    void addKey(String key, String value,Long timeout);

    /**
     * 通过key获取对应的value
     * @param key
     * @return
     */
    String getValueByKey(String key);
}
