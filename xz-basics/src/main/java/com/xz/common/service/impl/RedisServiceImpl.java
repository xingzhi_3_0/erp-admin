package com.xz.common.service.impl;

import com.xz.common.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @Description: ${Description}
 * @ClassName: RedisServiceImpl
 * @Author: daiyuanbao
 * @Date: 2022/1/21 13:31
 */
@Service
public class RedisServiceImpl implements RedisService {

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;	//Spring自动注入RedisTemplate

    @Override
    public void addKey(String key, String value) {
        // 20: 表示该数据在缓存中存在的时间，TimeUnit.SECONDS为单位秒，
        // 20秒后缓存中的数据会自动消失
        redisTemplate.opsForValue().set(key,value,200, TimeUnit.SECONDS);
    }

    @Override
    public void addKey(String key, String value,Long timeOut) {
        redisTemplate.opsForValue().set(key,value,timeOut, TimeUnit.SECONDS);
    }


    @Override
    public String getValueByKey(String key) {
        return (String) redisTemplate.opsForValue().get(key);
    }
}

