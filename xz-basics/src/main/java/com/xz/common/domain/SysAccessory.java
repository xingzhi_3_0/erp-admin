package com.xz.common.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 附件对象 t_sys_accessory
 *
 * @author xz
 * @date 2022-03-01
 */
@Data
@TableName("t_sys_accessory")
public class SysAccessory extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 附件名称 */
    @Excel(name = "附件名称")
    private String fileName;

    /** 文件url */
    @Excel(name = "文件url")
    private String url;

    /** 业务id（根据不同的业务类型，对应不同业务表的主键） */
    @Excel(name = "业务id", readConverterExp = "根=据不同的业务类型，对应不同业务表的主键")
    private String bizId;

    /** 业务类型 （1.供应商 2-验光配镜 3-销售退货凭证 4-销售明细参数附件 5-挂号检查退费附件） */
    @Excel(name = "业务类型 ", readConverterExp = "1=供应商,2=验光配镜,3=销售退货凭证,4=销售明细参数附件,5=挂号检查退费附件")
    private Integer bizType;

    /** 文件类型（1、图片 2 文件） */
    @Excel(name = "文件类型", readConverterExp = "1=图片,2=文件")
    private Integer fileType;

    /** 删除状态（0未删除，1已删除） */
    private Integer delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date validity;

    /**
     *
     * @author ZhangZhiJia
     * @date 2022/3/2 14:04
     * @param fileName 附件名称
     * @param url 文件url
     * @param bizId 业务id（根据不同的业务类型，对应不同业务表的主键）
     * @param bizType 业务类型 （1.客户身份证 2.客户驾驶证 3.门店）
     * @param fileType 文件类型（1、图片 2 文件）
     * @param tenantId 租户id
     * @return null
     */
    public SysAccessory( String fileName, String url, String bizId, Integer bizType, Integer fileType, Long tenantId) {
        this.fileName = fileName;
        this.url = url;
        this.bizId = bizId;
        this.bizType = bizType;
        this.fileType = fileType;
        this.tenantId = tenantId;
    }

    public SysAccessory() {
    }
}
