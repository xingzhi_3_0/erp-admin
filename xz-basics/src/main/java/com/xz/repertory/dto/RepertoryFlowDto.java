package com.xz.repertory.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName RepertoryFlowDto * @Description TODO
 * @Author Administrator
 * @Date 11:31 2024/1/8
 * @Version 1.0
 **/
@Data
public class RepertoryFlowDto {
    /** id */
    private Long id;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 单号 */
    @Excel(name = "单号")
    private String number;

    /** 类型（1.入库 2.出库） */
    private Integer flowType;

    /** 出入库类型(1.采购入库 2.采购入库（冲红） 3.采购退货 4.采购退货（冲红）5.库存调拨（出）6.库存调拨（入）7.库存调整出库 8.库存调整入库 9.库存报损 10.销售出库 ) */
    @Excel(name = "出入库类型",readConverterExp = "1=采购入库,2=采购入库（冲红）,3=采购退货,4=采购退货（冲红）,5=库存调拨（出）,6=库存调拨（入）,7=库存调整出库,8=库存调整入库,9=库存报损,10=销售出库,11=库存调整入库（冲红）,12=取消销售单且商品入库,13=销售单退货入库,14=销售单换货入库,15=销售单换货出库,16=贸易中间商发货单（发货）,17=贸易中间商发货单（冲红）,18=贸易中间商退货单（退货）,19=贸易中间商退货单（冲红）,20=贸易中间商换货单（退货）,21=贸易中间商换货单（换货）,22=贸易中间商换货单（退货冲红）,23=贸易中间商换货单（换货冲红）,24=诊疗领药")
    private Integer storageType;

    /** 贸易客户 */
    @Excel(name = "贸易客户")
    private String enterpriseName;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String warehouseName;

    /** 客户信息 */
    @Excel(name = "客户信息")
    private String memberName;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;
    /** 商品编码 */
    @Excel(name = "商品编码")
    private String productEncoded;

    /** 参数 */
    @Excel(name = "参数")
    private String productParameter;

    /** 生成批号/sn码 */
    @Excel(name = "生成批号/sn码")
    private String batchNumber;

    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "有效期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date validity;
    /** 数量 */
    @Excel(name = "入库")
    private Integer storage;
    @Excel(name = "出库")
    private Integer deliveryStorage;
    private Integer quantity;
    /** 单价 */
    @Excel(name = "单价")
    private BigDecimal price;

    /** 实际收款金额 */
    @Excel(name = "实际收款金额")
    private BigDecimal amount;

    /** 优惠 */
    @Excel(name = "优惠")
    private BigDecimal discount;
    /** 单位*/
    @Excel(name = "单位")
    private String unit;
    /** 商品类别 */
    @Excel(name = "商品类别")
    private String productCategory;
    /** 制单人 */
    @Excel(name = "制单人")
    private String userName;
    /** 所属部门 */
    @Excel(name = "所属部门")
    private String deptName;
    /** 备注 */
    @Excel(name = "备注")
    private String remark;
}
