package com.xz.repertory.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.purchase.domain.PurchaseProduct;
import com.xz.repertory.dto.RepertoryFlowBillDto;
import com.xz.repertory.dto.RepertoryFlowDto;
import org.apache.ibatis.annotations.Mapper;
import com.xz.repertory.domain.RepertoryFlow;

/**
 * 库存流水Mapper接口
 * 
 * @author xz
 * @date 2024-01-06
 */
@Mapper
public interface RepertoryFlowMapper  extends BaseMapper<RepertoryFlow> {
    /**
     * 查询库存流水
     * 
     * @param id 库存流水主键
     * @return 库存流水
     */
    public RepertoryFlow selectRepertoryFlowById(Long id);

    /**
     * 查询库存流水列表
     * 
     * @param repertoryFlow 库存流水
     * @return 库存流水集合
     */
    public List<RepertoryFlowDto> selectRepertoryFlowList(RepertoryFlow repertoryFlow);
    public List<RepertoryFlowBillDto> selectRepertoryFlowBillList(RepertoryFlow repertoryFlow);
    List<PurchaseProduct> selectListByRepertoryFlow(RepertoryFlow repertoryFlow);
    /**
     * 新增库存流水
     * 
     * @param repertoryFlow 库存流水
     * @return 结果
     */
    public int insertRepertoryFlow(RepertoryFlow repertoryFlow);

    /**
     * 修改库存流水
     * 
     * @param repertoryFlow 库存流水
     * @return 结果
     */
    public int updateRepertoryFlow(RepertoryFlow repertoryFlow);

    /**
     * 删除库存流水
     * 
     * @param id 库存流水主键
     * @return 结果
     */
    public int deleteRepertoryFlowById(Long id);

    /**
     * 批量删除库存流水
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteRepertoryFlowByIds(Long[] ids);
}
