package com.xz.repertory.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 库存流水对象 t_repertory_flow
 *
 * @author xz
 * @date 2024-01-06
 */
@Data
@TableName("t_repertory_flow")
public class RepertoryFlow extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 单号 */
    @Excel(name = "单号")
    private String number;

    /** 类型（1.入库 2.出库） */
    @Excel(name = "类型", readConverterExp = "1=.入库,2=.出库")
    private Integer flowType;

    /** 出入库类型(
     *          1.采购入库 2.采购入库（冲红） 3.采购退货 4.采购退货（冲红）5.库存调拨（出）6.库存调拨（入）7.库存调整出库 8.库存调整入库
     *          9.库存报损 10.销售出库 11.库存调整入库（冲红）12.取消销售单且商品入库 13.销售单退货入库 14.销售单换货入库 15.销售单换货出库
     *          16.贸易中间商发货单（发货） 17.贸易中间商发货单（冲红） 18.贸易中间商退货单（退货） 19.贸易中间商退货单（冲红）
     *          20.贸易中间商换货单（退货） 21.贸易中间商换货单（换货） 22.贸易中间商换货单（退货冲红） 23.贸易中间商换货单（换货冲红） 24.诊疗领药
     *  )*/
    @Excel(name = "出入库类型")
    private Integer storageType;

    @TableField(exist = false)
    private List<Integer> storageTypeList;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String warehouseName;

    /** 客户id (根据不同单据来，有可能是会员id，患者id)*/
    @Excel(name = "客户id")
    private Long memberId;

    /** 客户信息(贸易中间商则为d，患者id) */
    @Excel(name = "客户信息")
    private String memberName;

    /** 贸易中间商id*/
    @Excel(name = "贸易中间商id")
    private Long enterpriseId;

    /** 贸易中间商信息 */
    @Excel(name = "贸易中间商")
    private String enterpriseName;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;
    /** 商品id */
    @Excel(name = "商品id")
    private Long productId;
    /** 采购商品id */
    @Excel(name = "采购商品id")
    private Long purchaseProductId;
    /** 商品编码 */
    @Excel(name = "商品编码")
    private String productEncoded;

    /** 参数 */
    @Excel(name = "参数")
    private String productParameter;

    /** 生成批号 */
    @Excel(name = "生成批号")
    private String batchNumber;

    /** sn码 */
    @Excel(name = "sn码")
    private String sn;

    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "有效期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date validity;

    /** 数量 */
    @Excel(name = "数量")
    private Integer quantity;

    /** 成本单价 */
    @NotNull(message = "成本金额不能为空")
    @Excel(name = "成本单价")
    private BigDecimal costPrice;

    /** 成本金额 */
    @Excel(name = "成本金额")
    private BigDecimal costAmount;

    /** 单价 */
    @Excel(name = "单价")
    private BigDecimal price;

    /** 金额 */
    @Excel(name = "金额")
    private BigDecimal amount;

    /** 折扣(%) */
    @Excel(name = "优惠")
    private BigDecimal discount;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;
    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
    //业务id
    private Long businessId;
    /** 供应商id */
    @Excel(name = "供应商id")
    private Long supplierId;
    /** 供应商 */
    @Excel(name = "供应商")
    private String supplierName;

}
