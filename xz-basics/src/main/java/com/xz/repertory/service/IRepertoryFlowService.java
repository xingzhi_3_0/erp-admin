package com.xz.repertory.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.purchase.domain.PurchaseProduct;
import com.xz.repertory.domain.RepertoryFlow;
import com.xz.repertory.dto.RepertoryFlowBillDto;
import com.xz.repertory.dto.RepertoryFlowDto;

/**
 * 库存流水Service接口
 * 
 * @author xz
 * @date 2024-01-06
 */
public interface IRepertoryFlowService extends IService<RepertoryFlow> {
    /**
     * 查询库存流水
     * 
     * @param id 库存流水主键
     * @return 库存流水
     */
    public RepertoryFlow selectRepertoryFlowById(Long id);

    /**
     * 查询库存流水列表
     * 
     * @param repertoryFlow 库存流水
     * @return 库存流水集合
     */
    public List<RepertoryFlowDto> selectRepertoryFlowList(RepertoryFlow repertoryFlow);
    public List<RepertoryFlowBillDto> selectRepertoryFlowBillList(RepertoryFlow repertoryFlow);
    public List<PurchaseProduct> selectListByRepertoryFlow(RepertoryFlow repertoryFlow);
    /**
     * 新增库存流水
     * 
     * @param repertoryFlow 库存流水
     * @return 结果
     */
    public int insertRepertoryFlow(RepertoryFlow repertoryFlow);

    /**
     * 流水日志处理
     * @param purchaseProduct
     * @param type 1.采购入库
     * @return
     */
    RepertoryFlow dataRepertoryFlow(PurchaseProduct purchaseProduct, Integer type, Integer storageType);

    /**
     * 批量处理--商品单价
     * @param list
     * @return
     */
    boolean saveRepertoryFlowList(List<PurchaseProduct> list, Integer type, Integer storageType);

    /**
     * 、公用处理流水接口
     * @param list
     * @return
     */
    boolean saveRepertoryFlow(List<PurchaseProduct> list, Integer type, Integer storageType);

    List<PurchaseProduct> convertFlowData(List<PurchaseProduct> list,Integer operateNumber,String purchaseNumber,Long businessId);

    /**
     * 修改库存流水
     * 
     * @param repertoryFlow 库存流水
     * @return 结果
     */
    public AjaxResult updateRepertoryFlow(RepertoryFlow repertoryFlow);

    /**
     * 批量删除库存流水
     * 
     * @param ids 需要删除的库存流水主键集合
     * @return 结果
     */
    public int deleteRepertoryFlowByIds(Long[] ids);

    /**
     * 删除库存流水信息
     * 
     * @param id 库存流水主键
     * @return 结果
     */
    public int deleteRepertoryFlowById(Long id);
}
