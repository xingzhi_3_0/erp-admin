package com.xz.purchase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.common.annotation.DataScope;
import com.xz.purchase.domain.AllotInfo;
import com.xz.purchase.vo.AllotInfoDataVo;
import com.xz.purchase.vo.AllotInfoVo;
import com.xz.purchase.vo.AllotProductVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 库存调拨Mapper接口
 * 
 * @author xz
 * @date 2024-01-10
 */
@Mapper
public interface AllotInfoMapper  extends BaseMapper<AllotInfo> {
    /**
     * 查询库存调拨
     * 
     * @param id 库存调拨主键
     * @return 库存调拨
     */
    public AllotProductVo selectAllotInfoById(Long id);

    /**
     * 查询库存调拨列表
     * 
     * @param allotInfo 库存调拨
     * @return 库存调拨集合
     */
    public List<AllotInfoVo> selectAllotInfoList(AllotInfo allotInfo);

    /**
     * 新增库存调拨
     * 
     * @param allotInfo 库存调拨
     * @return 结果
     */
    public int insertAllotInfo(AllotInfo allotInfo);

    /**
     * 修改库存调拨
     * 
     * @param allotInfo 库存调拨
     * @return 结果
     */
    public int updateAllotInfo(AllotInfo allotInfo);

    /**
     * 删除库存调拨
     * 
     * @param id 库存调拨主键
     * @return 结果
     */
    public int deleteAllotInfoById(Long id);

    /**
     * 批量删除库存调拨
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAllotInfoByIds(Long[] ids);

    /**
     * 调拨导出
     * @return
     */
    List<AllotInfoDataVo> getAllotInfoData();

    /**
     * 入库导出
     * @return
     */
    List<AllotInfoDataVo> getPurchaseData();
}
