package com.xz.purchase.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.common.annotation.DataScope;
import com.xz.purchase.vo.ChoosePurchaseProductVo;
import com.xz.purchase.vo.ProductVo;
import com.xz.purchase.vo.ReturnInfoNoFlowVo;
import com.xz.repertory.domain.RepertoryFlow;
import org.apache.ibatis.annotations.Mapper;
import com.xz.purchase.domain.PurchaseProduct;
import org.apache.ibatis.annotations.Param;

/**
 * 采购商品明细Mapper接口
 *
 * @author xz
 * @date 2024-01-06
 */
@Mapper
public interface PurchaseProductMapper  extends BaseMapper<PurchaseProduct> {
    /**
     * 查询采购商品明细
     *
     * @param id 采购商品明细主键
     * @return 采购商品明细
     */
    public PurchaseProduct selectPurchaseProductById(Long id);

    /**
     * 查询采购商品明细列表
     *
     * @param purchaseProduct 采购商品明细
     * @return 采购商品明细集合
     */
    public List<PurchaseProduct> selectPurchaseProductList(PurchaseProduct purchaseProduct);

    /**
     * 新增采购商品明细
     *
     * @param purchaseProduct 采购商品明细
     * @return 结果
     */
    public int insertPurchaseProduct(PurchaseProduct purchaseProduct);

    /**
     * 修改采购商品明细
     *
     * @param purchaseProduct 采购商品明细
     * @return 结果
     */
    public int updatePurchaseProduct(PurchaseProduct purchaseProduct);

    /**
     * 删除采购商品明细
     *
     * @param id 采购商品明细主键
     * @return 结果
     */
    public int deletePurchaseProductById(Long id);

    /**
     * 批量删除采购商品明细
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePurchaseProductByIds(Long[] ids);
    /**
     * 冲红
     * @param idList 主键集合
     * @return 结果
     */
    public int blush(@Param("idList") List<Long> idList);

    /**
     * 获取采购商品信息
     * @param id
     * @return
     */
    PurchaseProduct getPurchaseProductInfo(Long id);
    /**
     * 更新商品单价
     */
    int updateCostPrice(RepertoryFlow repertoryFlow);

    /**
     * 获取采购入库id
     * @param repertoryFlow
     * @return
     */
    Long selectPurchaseId(RepertoryFlow repertoryFlow);


    List<ProductVo> chooseProduct(PurchaseProduct purchaseProduct);

    List<ProductVo> chooseSalesProduct(PurchaseProduct purchaseProduct);

    List<ChoosePurchaseProductVo> findByNameAndParam(PurchaseProduct purchaseProduct);

    List<ChoosePurchaseProductVo> findGysCkByNameAndParam(PurchaseProduct purchaseProduct);

    Integer findCountByNameAndParam(PurchaseProduct purchaseProduct);

    @DataScope(deptAlias = "d", userAlias = "u")
    List<PurchaseProduct> getListByParam(PurchaseProduct purchaseProduct);

    List<PurchaseProduct> getSalesListByParam(PurchaseProduct purchaseProduct);

    List<PurchaseProduct> getSalesListByMatchParam(PurchaseProduct purchaseProduct);


    List<PurchaseProduct> getSalesNoFlowListByParam(PurchaseProduct purchaseProduct);

    List<PurchaseProduct> getAllotListByParam(PurchaseProduct purchaseProduct);
    @DataScope(deptAlias = "d", userAlias = "u")
    List<PurchaseProduct> getPurchaseProductListByParam(PurchaseProduct purchaseProduct);
    //调拨入库商品记录
    List<PurchaseProduct> getAllotPurchaseProductMapper(Long allotId);


    List<PurchaseProduct> validityProductList(PurchaseProduct purchaseProduct);

    PurchaseProduct queryByParam(ReturnInfoNoFlowVo vo);

  /**
   * 查询诊疗商品
   * @param purchaseProduct
   * @return
   */
  List<ChoosePurchaseProductVo> chooseProductForDiagnosisae(PurchaseProduct purchaseProduct);
}
