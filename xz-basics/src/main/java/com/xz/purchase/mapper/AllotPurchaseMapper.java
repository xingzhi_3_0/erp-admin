package com.xz.purchase.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.purchase.domain.AllotPurchase;

/**
 * 调拨入库商品记录Mapper接口
 * 
 * @author xz
 * @date 2024-04-18
 */
@Mapper
public interface AllotPurchaseMapper  extends BaseMapper<AllotPurchase> {
    /**
     * 查询调拨入库商品记录
     * 
     * @param id 调拨入库商品记录主键
     * @return 调拨入库商品记录
     */
    public AllotPurchase selectAllotPurchaseById(Long id);

    /**
     * 查询调拨入库商品记录列表
     * 
     * @param allotPurchase 调拨入库商品记录
     * @return 调拨入库商品记录集合
     */
    public List<AllotPurchase> selectAllotPurchaseList(AllotPurchase allotPurchase);

    /**
     * 新增调拨入库商品记录
     * 
     * @param allotPurchase 调拨入库商品记录
     * @return 结果
     */
    public int insertAllotPurchase(AllotPurchase allotPurchase);

    /**
     * 修改调拨入库商品记录
     * 
     * @param allotPurchase 调拨入库商品记录
     * @return 结果
     */
    public int updateAllotPurchase(AllotPurchase allotPurchase);

    /**
     * 删除调拨入库商品记录
     * 
     * @param id 调拨入库商品记录主键
     * @return 结果
     */
    public int deleteAllotPurchaseById(Long id);

    /**
     * 批量删除调拨入库商品记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAllotPurchaseByIds(Long[] ids);
}
