package com.xz.purchase.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.purchase.dto.PurchasePriceDto;
import org.apache.ibatis.annotations.Mapper;
import com.xz.purchase.domain.PurchasePrice;

/**
 * 进货价Mapper接口
 * 
 * @author xz
 * @date 2024-01-08
 */
@Mapper
public interface PurchasePriceMapper  extends BaseMapper<PurchasePrice> {
    /**
     * 查询进货价
     * 
     * @param id 进货价主键
     * @return 进货价
     */
    public PurchasePrice selectPurchasePriceById(Long id);

    /**
     * 查询进货价列表
     * 
     * @param purchasePrice 进货价
     * @return 进货价集合
     */
    public List<PurchasePriceDto> selectPurchasePriceList(PurchasePrice purchasePrice);

    /**
     * 通过供应商和商品id获取商品单价
     * @param purchasePrice
     * @return
     */
    public PurchasePrice selectPurchasePrice(PurchasePrice purchasePrice);

    /**
     * 新增进货价
     * 
     * @param purchasePrice 进货价
     * @return 结果
     */
    public int insertPurchasePrice(PurchasePrice purchasePrice);

    /**
     * 修改进货价
     * 
     * @param purchasePrice 进货价
     * @return 结果
     */
    public int updatePurchasePrice(PurchasePrice purchasePrice);

    /**
     * 删除进货价
     * 
     * @param id 进货价主键
     * @return 结果
     */
    public int deletePurchasePriceById(Long id);

    /**
     * 批量删除进货价
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePurchasePriceByIds(Long[] ids);
}
