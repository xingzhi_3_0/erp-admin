package com.xz.purchase.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.common.annotation.DataScope;
import com.xz.purchase.domain.PurchaseProduct;
import com.xz.purchase.dto.PurchaseDto;
import com.xz.purchase.dto.PurchaseInfoDto;
import com.xz.purchase.dto.PurchaseStatisticsDto;
import com.xz.purchase.vo.*;
import org.apache.ibatis.annotations.Mapper;
import com.xz.purchase.domain.Purchase;

/**
 * 采购Mapper接口
 * 
 * @author xz
 * @date 2024-01-06
 */
@Mapper
public interface PurchaseMapper  extends BaseMapper<Purchase> {
    /**
     * 查询采购
     * 
     * @param id 采购主键
     * @return 采购
     */
    public PurchaseInfoDto selectPurchaseById(Long id);

    /**
     * 查询采购列表
     * 
     * @param purchase 采购
     * @return 采购集合
     */
    public List<PurchaseDto> selectPurchaseList(Purchase purchase);

    /**
     * 新增采购
     * 
     * @param purchase 采购
     * @return 结果
     */
    public int insertPurchase(Purchase purchase);

    /**
     * 修改采购
     * 
     * @param purchase 采购
     * @return 结果
     */
    public int updatePurchase(Purchase purchase);

    /**
     * 删除采购
     * 
     * @param id 采购主键
     * @return 结果
     */
    public int deletePurchaseById(Long id);

    /**
     * 批量删除采购
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePurchaseByIds(Long[] ids);

    /**
     * 商品维度统计
     * @param purchaseProduct
     * @return
     */
    @DataScope(deptAlias = "d", userAlias = "u")
   List<CommodityDimensionStatisticsVo> commodityDimensionStatistics(PurchaseProduct purchaseProduct);
    /**
     * 财务维度统计
     * @param purchaseProduct
     * @return
     */
    @DataScope(deptAlias = "d", userAlias = "u")
   List<BillDimensionStatisticsVo> billDimensionStatistics(PurchaseProduct purchaseProduct);

    /**
     * 调拨在途库存--商品为敌
     * @param purchaseProduct
     * @return
     */
   List<TransferStockVo> transferStock(PurchaseProduct purchaseProduct);

    /**
     * 采购在途
     * @param purchaseProduct
     * @return
     */
   List<PurchaseTransitVo> purchaseTransit(PurchaseProduct purchaseProduct);
    /**
     * 批次维度统计
     * @param purchaseProduct
     * @return
     */
    @DataScope(deptAlias = "d", userAlias = "u")
   List<BatchDimensionStatisticsVo> batchDimensionStatistics(PurchaseProduct purchaseProduct);

    /**
     * 调拨在途库存--批次纬度
     * @param purchaseProduct
     * @return
     */
   List<TransferStockVo> transferBatchStock(PurchaseProduct purchaseProduct);

    /**
     * 销售在途
     * @param purchaseProduct
     * @return
     */
    List<SalesStockVo> salesStock(PurchaseProduct purchaseProduct);
    /**
     * 销售在途--批次纬度
     * @param purchaseProduct
     * @return
     */
    List<SalesStockVo> salesBatchStock(PurchaseProduct purchaseProduct);

    /**
     * 进销存期末统计
     * @param purchase
     * @return
     */
    @DataScope(deptAlias = "d", userAlias = "u")
    List<PurchaseStatisticsVo> purchaseEndStatistics(Purchase purchase);

    /**
     * 进销存期初统计
     * @param purchaseStatisticsDto
     * @return
     */
    @DataScope(deptAlias = "d", userAlias = "u")
    List<PurchaseStatisticsVo> purchaseBeginStatistics(PurchaseStatisticsDto purchaseStatisticsDto);

    /**
     * 进销存库存流水
     * @param purchaseStatisticsDto
     * @return
     */
    @DataScope(deptAlias = "d", userAlias = "u")
    List<PurchaseFlowStatisticsVo> purchaseFlowStatistics(PurchaseStatisticsDto purchaseStatisticsDto);
}
