package com.xz.purchase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.purchase.domain.AdjustProduct;
import com.xz.purchase.domain.PurchaseProduct;
import com.xz.purchase.vo.AdjustProductVo;
import com.xz.purchase.vo.ReturnProductVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 采购退货明细Mapper接口
 * 
 * @author xz
 * @date 2024-1-10 09:24:39
 */
@Mapper
public interface AdjustProductMapper extends BaseMapper<AdjustProduct> {

    int deleteByAdjustId(@Param("adjustId") Long adjustId);

    List<PurchaseProduct> productListByAdjustId(@Param("adjustId")Long adjustId);

    List<AdjustProductVo> findListByAdjustId(@Param("adjustId")Long adjustId);

    List<AdjustProduct> getListByAdjustId(@Param("adjustId")Long adjustId);
}
