package com.xz.purchase.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.purchase.domain.AllotProduct;

/**
 * 调拨商品明细Mapper接口
 * 
 * @author xz
 * @date 2024-01-10
 */
@Mapper
public interface AllotProductMapper  extends BaseMapper<AllotProduct> {
    /**
     * 查询调拨商品明细
     * 
     * @param id 调拨商品明细主键
     * @return 调拨商品明细
     */
    public AllotProduct selectAllotProductById(Long id);

    /**
     * 查询调拨商品明细列表
     * 
     * @param allotProduct 调拨商品明细
     * @return 调拨商品明细集合
     */
    public List<AllotProduct> selectAllotProductList(AllotProduct allotProduct);

    /**
     * 新增调拨商品明细
     * 
     * @param allotProduct 调拨商品明细
     * @return 结果
     */
    public int insertAllotProduct(AllotProduct allotProduct);

    /**
     * 修改调拨商品明细
     * 
     * @param allotProduct 调拨商品明细
     * @return 结果
     */
    public int updateAllotProduct(AllotProduct allotProduct);

    /**
     * 删除调拨商品明细
     * 
     * @param id 调拨商品明细主键
     * @return 结果
     */
    public int deleteAllotProductById(Long id);

    /**
     * 批量删除调拨商品明细
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAllotProductByIds(Long[] ids);
}
