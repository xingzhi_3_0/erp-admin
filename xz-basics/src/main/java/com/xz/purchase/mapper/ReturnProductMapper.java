package com.xz.purchase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.purchase.domain.PurchaseProduct;
import com.xz.purchase.domain.ReturnInfo;
import com.xz.purchase.domain.ReturnProduct;
import com.xz.purchase.vo.ReturnProductVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 采购退货明细Mapper接口
 * 
 * @author xz
 * @date 2024-1-10 09:24:39
 */
@Mapper
public interface ReturnProductMapper extends BaseMapper<ReturnProduct> {

    int deleteByReturnId(@Param("returnId") Long returnId);

    List<ReturnProductVo> findListByReturnId(@Param("returnId")Long returnId);

    List<PurchaseProduct> productListByReturnId(@Param("returnId")Long returnId);

    List<ReturnProduct> getListByReturnId(@Param("returnId")Long returnId);
}
