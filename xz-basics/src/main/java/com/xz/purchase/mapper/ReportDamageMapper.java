package com.xz.purchase.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.purchase.domain.ProcessingReportDamageVo;
import com.xz.purchase.vo.ReportDamageInfoVo;
import com.xz.purchase.vo.ReportDamageVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.purchase.domain.ReportDamage;

/**
 * 库存报损Mapper接口
 * 
 * @author xz
 * @date 2024-01-16
 */
@Mapper
public interface ReportDamageMapper  extends BaseMapper<ReportDamage> {
    /**
     * 查询库存报损
     * 
     * @param id 库存报损主键
     * @return 库存报损
     */
    public ReportDamageInfoVo selectReportDamageById(Long id);

    /**
     * 查询库存报损列表
     * 
     * @param reportDamage 库存报损
     * @return 库存报损集合
     */
    public List<ReportDamageVo> selectReportDamageList(ReportDamage reportDamage);

    /**
     * 新增库存报损
     * 
     * @param reportDamage 库存报损
     * @return 结果
     */
    public int insertReportDamage(ReportDamage reportDamage);

    /**
     * 修改库存报损
     * 
     * @param reportDamage 库存报损
     * @return 结果
     */
    public int updateReportDamage(ReportDamage reportDamage);

    /**
     * 删除库存报损
     * 
     * @param id 库存报损主键
     * @return 结果
     */
    public int deleteReportDamageById(Long id);

    /**
     * 批量删除库存报损
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteReportDamageByIds(Long[] ids);

    /**
     * 加工单报损
     * @return
     */
    List<ProcessingReportDamageVo> getProcessingReportDamageInfo(Long id);
}
