package com.xz.purchase.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.purchase.domain.ReportDamageProduct;

/**
 * 报损商品明细Mapper接口
 * 
 * @author xz
 * @date 2024-01-16
 */
@Mapper
public interface ReportDamageProductMapper  extends BaseMapper<ReportDamageProduct> {
    /**
     * 查询报损商品明细
     * 
     * @param id 报损商品明细主键
     * @return 报损商品明细
     */
    public ReportDamageProduct selectReportDamageProductById(Long id);

    /**
     * 查询报损商品明细列表
     * 
     * @param reportDamageProduct 报损商品明细
     * @return 报损商品明细集合
     */
    public List<ReportDamageProduct> selectReportDamageProductList(ReportDamageProduct reportDamageProduct);

    /**
     * 新增报损商品明细
     * 
     * @param reportDamageProduct 报损商品明细
     * @return 结果
     */
    public int insertReportDamageProduct(ReportDamageProduct reportDamageProduct);

    /**
     * 修改报损商品明细
     * 
     * @param reportDamageProduct 报损商品明细
     * @return 结果
     */
    public int updateReportDamageProduct(ReportDamageProduct reportDamageProduct);

    /**
     * 删除报损商品明细
     * 
     * @param id 报损商品明细主键
     * @return 结果
     */
    public int deleteReportDamageProductById(Long id);

    /**
     * 批量删除报损商品明细
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteReportDamageProductByIds(Long[] ids);
}
