package com.xz.purchase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.purchase.domain.AdjustInfo;
import com.xz.purchase.domain.WarningSettings;
import com.xz.purchase.vo.WarningSettingsVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * 采购退货Mapper接口
 * 
 * @author xz
 * @date 2024-1-10 09:24:39
 */
@Mapper
public interface WarningSettingsMapper extends BaseMapper<WarningSettings> {

    List<WarningSettings> setWarningList(WarningSettings warningSettings);

    List<WarningSettingsVo> queryList(WarningSettings warningSettings);
}
