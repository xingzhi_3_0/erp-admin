package com.xz.purchase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.purchase.domain.ReturnInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * 采购退货Mapper接口
 * 
 * @author xz
 * @date 2024-1-10 09:24:39
 */
@Mapper
public interface ReturnInfoMapper extends BaseMapper<ReturnInfo> {

    List<ReturnInfo> queryList(ReturnInfo returnInfo);
}
