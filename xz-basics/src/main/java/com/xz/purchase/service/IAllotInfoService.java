package com.xz.purchase.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.purchase.domain.AllotInfo;
import com.xz.purchase.vo.AllotInfoDataVo;
import com.xz.purchase.vo.AllotInfoVo;
import com.xz.purchase.vo.AllotProductVo;
import com.xz.audit.dto.AuditDto;
import com.xz.common.core.domain.AjaxResult;

/**
 * 库存调拨Service接口
 * 
 * @author xz
 * @date 2024-01-10
 */
public interface IAllotInfoService extends IService<AllotInfo> {
    /**
     * 查询库存调拨
     * 
     * @param id 库存调拨主键
     * @return 库存调拨
     */
    public AllotProductVo selectAllotInfoById(Long id);

    /**
     * 查询库存调拨列表
     * 
     * @param allotInfo 库存调拨
     * @return 库存调拨集合
     */
    public List<AllotInfoVo> selectAllotInfoList(AllotInfo allotInfo);

    /**
     * 新增库存调拨
     * 
     * @param allotInfo 库存调拨
     * @return 结果
     */
    public AjaxResult insertAllotInfo(AllotInfo allotInfo);

    /**
     * 修改库存调拨
     * 
     * @param allotInfo 库存调拨
     * @return 结果
     */
    public AjaxResult updateAllotInfo(AllotInfo allotInfo);

    /**
     * 批量删除库存调拨
     * 
     * @param ids 需要删除的库存调拨主键集合
     * @return 结果
     */
    public int deleteAllotInfoByIds(Long[] ids);

    /**
     * 删除库存调拨信息
     * 
     * @param id 库存调拨主键
     * @return 结果
     */
    public int deleteAllotInfoById(Long id);

    /**
     * 审核
     * @param auditDto
     * @return
     */
    AjaxResult allotInfoAudit(AuditDto auditDto);
    /**
     * 调拨导出
     * @return
     */
    List<AllotInfoDataVo> getAllotInfoData();
}
