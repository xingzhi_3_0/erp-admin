package com.xz.purchase.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.purchase.domain.ReturnInfo;
import com.xz.purchase.domain.WarningSettings;
import com.xz.purchase.param.ReturnInfoParam;
import com.xz.purchase.param.WarningSettingParam;
import com.xz.purchase.vo.ReturnInfoVo;
import com.xz.purchase.vo.WarningSettingsVo;

import java.util.List;

/**
 * 采购退货Service接口
 * 
 * @author xz
 * @date 2024-1-19 11:42:42
 */
public interface IWarningSettingsService extends IService<WarningSettings> {

    /**
     * 查询列表
     *
     * @param warningSettings
     * @return
     */
    List<WarningSettingsVo> queryList(WarningSettings warningSettings);

    /**
     * 查询列表
     *
     * @param warningSettings
     * @return
     */
    List<WarningSettings> setWarningList(WarningSettings warningSettings);

    /**
     * 批量设置上下限
     * @return
     */
    AjaxResult setUp(WarningSettingParam warningSettingParam);


}
