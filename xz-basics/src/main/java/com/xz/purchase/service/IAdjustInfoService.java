package com.xz.purchase.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.purchase.domain.AdjustInfo;
import com.xz.purchase.domain.ReturnInfo;
import com.xz.purchase.param.AdjustInfoParam;
import com.xz.purchase.param.ReturnInfoParam;
import com.xz.purchase.vo.AdjustInfoNoFlowVo;
import com.xz.purchase.vo.AdjustInfoVo;
import com.xz.purchase.vo.AdjustOutVo;
import com.xz.purchase.vo.ReturnInfoVo;
import com.xz.sales.vo.SalesNoFlowVo;

import java.util.List;

/**
 * 采购退货Service接口
 * 
 * @author xz
 * @date 2024-1-10 10:23:03
 */
public interface IAdjustInfoService extends IService<AdjustInfo> {

    /**
     * 查询列表
     *
     * @param adjustInfo
     * @return
     */
    List<AdjustInfo> queryList(AdjustInfo adjustInfo);

    /**
     * 新增采购调整
     *
     * @param param 采购退货
     * @return 结果
     */
    public AjaxResult addAdjustInfo(AdjustInfoParam param);

    /**
     * 修改采购退货
     *
     * @param param 采购退货
     * @return 结果
     */
    AjaxResult updateAdjustInfo(AdjustInfoParam param);

    /**
     * 修改采购退货状态
     *
     * @param adjustInfo 采购退货
     * @return 结果
     */
    AjaxResult editStatus(AdjustInfo adjustInfo);

    AjaxResult importNoFlowExcel(List<AdjustInfoNoFlowVo> list);

    AjaxResult importAddOutAdjustExcel(List<AdjustOutVo> list);

    AjaxResult importAddOutAdjustExport(List<AdjustOutVo> list);

    AjaxResult importAddOutMatchExport(List<AdjustOutVo> list);

    AdjustInfoVo adjustDetailById(Long id);

    /**
     * @param returnInfo
     * @return
     */
//    AjaxResult redInkEntry(ReturnInfo returnInfo);

}
