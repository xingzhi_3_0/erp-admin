package com.xz.purchase.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.audit.dto.AuditDto;
import com.xz.common.core.domain.AjaxResult;
import com.xz.purchase.domain.ProcessingReportDamageVo;
import com.xz.purchase.domain.ReportDamage;
import com.xz.purchase.vo.ReportDamageInfoVo;
import com.xz.purchase.vo.ReportDamageVo;

/**
 * 库存报损Service接口
 * 
 * @author xz
 * @date 2024-01-16
 */
public interface IReportDamageService extends IService<ReportDamage> {
    /**
     * 查询库存报损
     * 
     * @param id 库存报损主键
     * @return 库存报损
     */
    public ReportDamageInfoVo selectReportDamageById(Long id);

    /**
     * 查询库存报损列表
     * 
     * @param reportDamage 库存报损
     * @return 库存报损集合
     */
    public List<ReportDamageVo> selectReportDamageList(ReportDamage reportDamage);

    /**
     * 新增库存报损
     * 
     * @param reportDamage 库存报损
     * @return 结果
     */
    public AjaxResult insertReportDamage(ReportDamage reportDamage);

    /**
     * 修改库存报损
     * 
     * @param reportDamage 库存报损
     * @return 结果
     */
    public AjaxResult updateReportDamage(ReportDamage reportDamage);

    /**
     * 批量删除库存报损
     * 
     * @param ids 需要删除的库存报损主键集合
     * @return 结果
     */
    public int deleteReportDamageByIds(Long[] ids);

    /**
     * 删除库存报损信息
     * 
     * @param id 库存报损主键
     * @return 结果
     */
    public int deleteReportDamageById(Long id);
    /**
     * 审核
     * @param auditDto
     * @return
     */
    AjaxResult reportDamageAudit(AuditDto auditDto);
    /**
     * 加工单报损
     * @return
     */
    List<ProcessingReportDamageVo> getProcessingReportDamageInfo(Long id);
}
