package com.xz.purchase.service;

import java.math.BigDecimal;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.purchase.domain.PurchasePrice;
import com.xz.purchase.dto.PurchasePriceDto;
import com.xz.purchase.vo.PurchasePriceImportVo;

/**
 * 进货价Service接口
 * 
 * @author xz
 * @date 2024-01-08
 */
public interface IPurchasePriceService extends IService<PurchasePrice> {
    /**
     * 查询进货价
     * 
     * @param id 进货价主键
     * @return 进货价
     */
    public PurchasePrice selectPurchasePriceById(Long id);

    /**
     * 查询进货价列表
     * 
     * @param purchasePrice 进货价
     * @return 进货价集合
     */
    public List<PurchasePriceDto> selectPurchasePriceList(PurchasePrice purchasePrice);

    /**
     * 新增进货价
     * 
     * @param purchasePrice 进货价
     * @return 结果
     */
    public BigDecimal insertPurchasePrice(PurchasePrice purchasePrice);

    /**
     * 修改进货价
     * 
     * @param purchasePrice 进货价
     * @return 结果
     */
    public AjaxResult updatePurchasePrice(PurchasePrice purchasePrice);

    /**
     * 导入进货单价
     * @param list
     * @return
     */
    AjaxResult importExcel(List<PurchasePriceImportVo> list);

    /**
     * 批量删除进货价
     * 
     * @param ids 需要删除的进货价主键集合
     * @return 结果
     */
    public int deletePurchasePriceByIds(Long[] ids);

    /**
     * 删除进货价信息
     * 
     * @param id 进货价主键
     * @return 结果
     */
    public int deletePurchasePriceById(Long id);
}
