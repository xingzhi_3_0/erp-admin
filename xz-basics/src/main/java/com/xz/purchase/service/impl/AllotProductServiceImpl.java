package com.xz.purchase.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.purchase.mapper.AllotProductMapper;
import com.xz.purchase.domain.AllotProduct;
import com.xz.purchase.service.IAllotProductService;

/**
 * 调拨商品明细Service业务层处理
 *
 * @author xz
 * @date 2024-01-10
 */
@Service
public class AllotProductServiceImpl extends ServiceImpl<AllotProductMapper, AllotProduct> implements IAllotProductService {
    @Autowired
    private AllotProductMapper allotProductMapper;

    /**
     * 查询调拨商品明细
     *
     * @param id 调拨商品明细主键
     * @return 调拨商品明细
     */
    @Override
    public AllotProduct selectAllotProductById(Long id) {
        return allotProductMapper.selectAllotProductById(id);
    }

    /**
     * 查询调拨商品明细列表
     *
     * @param allotProduct 调拨商品明细
     * @return 调拨商品明细
     */
    @Override
    public List<AllotProduct> selectAllotProductList(AllotProduct allotProduct) {
        return allotProductMapper.selectAllotProductList(allotProduct);
    }

    /**
     * 新增调拨商品明细
     *
     * @param allotProduct 调拨商品明细
     * @return 结果
     */
    @Override
    public int insertAllotProduct(AllotProduct allotProduct) {
        return allotProductMapper.insertAllotProduct(allotProduct);
    }

    /**
     * 修改调拨商品明细
     *
     * @param allotProduct 调拨商品明细
     * @return 结果
     */
    @Override
    public int updateAllotProduct(AllotProduct allotProduct) {
        return allotProductMapper.updateAllotProduct(allotProduct);
    }

    /**
     * 批量删除调拨商品明细
     *
     * @param ids 需要删除的调拨商品明细主键
     * @return 结果
     */
    @Override
    public int deleteAllotProductByIds(Long[] ids) {
        return allotProductMapper.deleteAllotProductByIds(ids);
    }

    /**
     * 删除调拨商品明细信息
     *
     * @param id 调拨商品明细主键
     * @return 结果
     */
    @Override
    public int deleteAllotProductById(Long id) {
        return allotProductMapper.deleteAllotProductById(id);
    }
}
