package com.xz.purchase.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.core.domain.AjaxResult;
import com.xz.purchase.domain.Purchase;
import com.xz.purchase.domain.ReturnInfo;
import com.xz.purchase.dto.PurchaseDto;
import com.xz.purchase.dto.PurchaseInfoDto;
import com.xz.purchase.param.ReturnInfoParam;
import com.xz.purchase.vo.BlushVo;
import com.xz.purchase.vo.ReturnInfoNoFlowVo;
import com.xz.purchase.vo.ReturnInfoRepeatFlowVo;
import com.xz.purchase.vo.ReturnInfoVo;

import java.util.List;

/**
 * 采购退货Service接口
 * 
 * @author xz
 * @date 2024-1-10 10:23:03
 */
public interface IReturnInfoService extends IService<ReturnInfo> {

    /**
     * 新增采购退货
     *
     * @param param 采购退货
     * @return 结果
     */
    public AjaxResult addReturnInfo(ReturnInfoParam param);

    AjaxResult importNoFlowExcel(List<ReturnInfoNoFlowVo> list);

    AjaxResult importRepeatFlowExcel(List<ReturnInfoRepeatFlowVo> list);

    AjaxResult importKcExcel(List<ReturnInfoNoFlowVo> list);

    /**
     * 修改采购退货
     *
     * @param param 采购退货
     * @return 结果
     */
    AjaxResult updateReturnInfo(ReturnInfoParam param);

    /**
     * 修改采购退货状态
     *
     * @param returnInfo 采购退货
     * @return 结果
     */
    AjaxResult editStatus(ReturnInfo returnInfo);

    /**
     * 查询列表
     *
     * @param returnInfo
     * @return
     */
    List<ReturnInfo> queryList(ReturnInfo returnInfo);

    ReturnInfoVo returnDetailById(Long id);

    /**
     * @param returnInfo
     * @return
     */
    AjaxResult redInkEntry(ReturnInfo returnInfo);

}
