package com.xz.purchase.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.annotation.DataScope;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.domain.entity.SysUser;
import com.xz.common.exception.ServiceException;
import com.xz.common.utils.RedisCode;
import com.xz.common.utils.SecurityUtils;
import com.xz.common.utils.StringUtils;
import com.xz.purchase.domain.*;
import com.xz.purchase.mapper.*;
import com.xz.purchase.param.AdjustInfoParam;
import com.xz.purchase.param.AdjustProductParam;
import com.xz.purchase.param.ReturnInfoParam;
import com.xz.purchase.service.IAdjustInfoService;
import com.xz.purchase.service.IPurchaseService;
import com.xz.purchase.service.IReturnInfoService;
import com.xz.purchase.vo.*;
import com.xz.repertory.service.IRepertoryFlowService;
import com.xz.system.service.ISysUserService;
import com.xz.warehouse.dto.WarehouseDto;
import com.xz.warehouse.mapper.WarehouseMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 采购调整Service业务层处理
 *
 * @author xz
 * @date 2024-1-10 10:38:46
 */
@Service
public class AdjustInfoServiceImpl extends ServiceImpl<AdjustInfoMapper, AdjustInfo> implements IAdjustInfoService {


    @Autowired
    private PurchaseProductMapper purchaseProductMapper;

    @Autowired
    private AdjustProductMapper adjustProductMapper;

    @Autowired
    private IRepertoryFlowService iRepertoryFlowService;

    @Autowired
    private IPurchaseService purchaseService;

    @Autowired
    private ISysUserService sysUserService;

    @Resource
    private WarehouseMapper warehouseMapper;


    @Transactional(rollbackFor = Exception.class)
    @Override
    public AjaxResult addAdjustInfo(AdjustInfoParam param) {
        List<AdjustProduct> productList = param.getProductList();
        if(CollectionUtils.isEmpty(productList)){
            return AjaxResult.error("商品不能为空");
        }
        //仓库所属部门
        AdjustInfo adjustInfo = new AdjustInfo();
        adjustInfo.setStatus(param.getStatus());
        adjustInfo.setRemark(param.getRemark());
        adjustInfo.setAdjustType(param.getAdjustType());
        adjustInfo.setAdjustReason(param.getAdjustReason());
        if(param.getAdjustType().equals(1)){
            adjustInfo.setAdjustNo(RedisCode.getCode("TZRK"));
        }else {
            adjustInfo.setAdjustNo(RedisCode.getCode("TZCK"));
        }
        adjustInfo.setDeptId(SecurityUtils.getDeptId());
        Date now = new Date();
        adjustInfo.setCreateTime(now);
        adjustInfo.setCreateBy(SecurityUtils.getUserId());
        adjustInfo.setTenantId(SecurityUtils.getLoginUser().getTenantId());
        int insert = baseMapper.insert(adjustInfo);
        if(insert>0){
            if(param.getAdjustType().equals(1)){
                if(adjustInfo.getStatus().equals(2)){
                    for (AdjustProduct adjustProduct:productList) {
                        PurchaseProduct purchaseProduct = new PurchaseProduct();
                        BeanUtils.copyProperties(adjustProduct,purchaseProduct);
//                        List<PurchaseProduct> queryPurchaseProductList = purchaseProductMapper.getListByParam(purchaseProduct);
//                        if(!CollectionUtils.isEmpty(queryPurchaseProductList)){
//                            PurchaseProduct product = queryPurchaseProductList.get(0);
//                            BeanUtils.copyProperties(product,purchaseProduct);
//                        }
                        purchaseProduct.setOtherPurchaseNumber(adjustInfo.getAdjustNo());
                        purchaseProduct.setAvailableStock(adjustProduct.getAdjustNum());
                        purchaseProduct.setStorageNum(adjustProduct.getAdjustNum());
                        List<PurchaseProduct> purchaseProductList = Arrays.asList(purchaseProduct);
                        Purchase purchase = new Purchase();
                        // 8 才是调整入库
                        purchase.setStorageType(8);
                        purchase.setSupplierId(purchaseProduct.getSupplierId());
                        purchase.setSupplierName(purchaseProduct.getSupplierName());
                        purchase.setWarehouseId(purchaseProduct.getWarehouseId());
                        purchase.setWarehouseName(purchaseProduct.getWarehouseName());
                        purchase.setDeptId(adjustInfo.getDeptId());
                        purchase.setStatus(2);
                        purchase.setDeptId(purchaseProduct.getDeptId());
                        purchase.setPurchaseProductList(purchaseProductList);
                        AjaxResult ajaxResult = purchaseService.insertPurchase(purchase);
                        Long purchaseId = (Long) ajaxResult.get("data");
                        adjustProduct.setPurchaseId(purchaseId);
                    }
                }
                productList.stream().forEach(i-> {
                    i.setAdjustId(adjustInfo.getId());
                    adjustProductMapper.insert(i);
                });
            }else {
                productList.stream().forEach(i-> {
                    i.setAdjustId(adjustInfo.getId());
                    adjustProductMapper.insert(i);
                });
                if(adjustInfo.getStatus().equals(2)){
                    // 直接确认退出库
                    updateStorageAddFlow(adjustInfo);
                }
            }
            return AjaxResult.success("新增库存调整成功");

        }
        return AjaxResult.error("新增库存调整失败");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public AjaxResult updateAdjustInfo(AdjustInfoParam param) {
        if(param.getId()==null){
            return AjaxResult.error("参数id不能为空");
        }
        AdjustInfo updateAdjustInfo = baseMapper.selectById(param.getId());
        Date now = new Date();
        updateAdjustInfo.setUpdateTime(now);
        updateAdjustInfo.setUpdateBy(SecurityUtils.getUserId());
        updateAdjustInfo.setStatus(param.getStatus());
        updateAdjustInfo.setRemark(param.getRemark());
        updateAdjustInfo.setAdjustReason(param.getAdjustReason());
        List<AdjustProduct> productList = param.getProductList();
        if(CollectionUtils.isEmpty(productList)){
            return AjaxResult.error("商品不能为空");
        }
        baseMapper.updateById(updateAdjustInfo);
        //删除库存商品重新录入
        adjustProductMapper.deleteByAdjustId(updateAdjustInfo.getId());
        if(param.getAdjustType().equals(1)){
            if(updateAdjustInfo.getStatus().equals(2)){
                for (AdjustProduct adjustProduct:productList) {
                    PurchaseProduct purchaseProduct = new PurchaseProduct();
                    BeanUtils.copyProperties(adjustProduct,purchaseProduct);
//                    List<PurchaseProduct> queryPurchaseProductList = purchaseProductMapper.getListByParam(purchaseProduct);
//                    if(!CollectionUtils.isEmpty(queryPurchaseProductList)){
//                        PurchaseProduct product = queryPurchaseProductList.get(0);
//                        BeanUtils.copyProperties(product,purchaseProduct);
//                    }
                    purchaseProduct.setAvailableStock(adjustProduct.getAdjustNum());
                    purchaseProduct.setStorageNum(adjustProduct.getAdjustNum());
                    purchaseProduct.setOtherPurchaseNumber(updateAdjustInfo.getAdjustNo());
                    List<PurchaseProduct> purchaseProductList = Arrays.asList(purchaseProduct);
                    Purchase purchase = new Purchase();
                    purchase.setStorageType(8);
                    purchase.setSupplierId(purchaseProduct.getSupplierId());
                    purchase.setSupplierName(purchaseProduct.getSupplierName());
                    purchase.setWarehouseId(purchaseProduct.getWarehouseId());
                    purchase.setWarehouseName(purchaseProduct.getWarehouseName());
                    purchase.setStatus(2);
                    purchase.setDeptId(purchaseProduct.getDeptId());
                    purchase.setPurchaseProductList(purchaseProductList);
                    AjaxResult ajaxResult = purchaseService.insertPurchase(purchase);
                    Long purchaseId = (Long) ajaxResult.get("data");
                    adjustProduct.setPurchaseId(purchaseId);
                }
            }
            productList.stream().forEach(i-> {
                i.setAdjustId(updateAdjustInfo.getId());
                adjustProductMapper.insert(i);
            });
        }else {
            productList.stream().forEach(i-> {
                i.setAdjustId(updateAdjustInfo.getId());
                adjustProductMapper.insert(i);
            });
            if(updateAdjustInfo.getStatus().equals(2)){
                // 直接确认退货
                updateStorageAddFlow(updateAdjustInfo);
            }
        }
        return AjaxResult.success("修改成功");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public AjaxResult editStatus(AdjustInfo adjustInfo){
        if(Objects.isNull(adjustInfo.getId())){
            return AjaxResult.error("参数id不能为空");
        }
//        if(!adjustInfo.getStatus().equals(2) || !adjustInfo.getStatus().equals(4)){
//            return AjaxResult.error("状态参数错误");
//        }
        AdjustInfo obj = baseMapper.selectById(adjustInfo.getId());
        obj.setStatus(adjustInfo.getStatus());
        baseMapper.updateById(obj);
        //库存流水
        if(adjustInfo.getStatus().equals(2)){
            updateStorageAddFlow(adjustInfo);
        }else if(adjustInfo.getStatus().equals(3)){
            // 入库冲红
            List<AdjustProduct> listByAdjustId = adjustProductMapper.getListByAdjustId(adjustInfo.getId());
            for (AdjustProduct adjustProduct:listByAdjustId) {
                BlushVo blushVo = new BlushVo();
                blushVo.setId(adjustProduct.getPurchaseId());
                blushVo.setType(2);
                purchaseService.blush(blushVo);
            }
        }
        return AjaxResult.success("操作成功");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public AjaxResult importNoFlowExcel(List<AdjustInfoNoFlowVo> list){
        for (AdjustInfoNoFlowVo vo:list) {
            AdjustInfo adjustInfo = baseMapper.selectById(vo.getId());
            updateStorageAddNoFlow(adjustInfo);
        }
        return AjaxResult.success();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public AjaxResult importAddOutAdjustExcel(List<AdjustOutVo> list){

        // 仓库
        List<WarehouseDto> allWarehouseList = warehouseMapper.getAllWarehouseList(null);
        Map<String,Long> warehouseMap = new HashMap<>();
        for (WarehouseDto dto:allWarehouseList) {
            warehouseMap.put(dto.getWarehouseName(),dto.getId());
        }

        Date now = new Date();
        for (int i = 0; i < list.size(); i++) {
            AdjustOutVo adjustOutVo = list.get(i);
            System.out.println("第"+(i+1)+"条数据");
            PurchaseProduct queryProductParam = new PurchaseProduct();
            BeanUtils.copyProperties(adjustOutVo,queryProductParam);
            queryProductParam.setWarehouseId(warehouseMap.get(adjustOutVo.getWarehouseName()));
            List<PurchaseProduct> purchaseProductList = purchaseProductMapper.getSalesListByParam(queryProductParam);
            if(CollectionUtils.isEmpty(purchaseProductList)){
                throw new ServiceException(adjustOutVo.getProductName()+"_"+adjustOutVo.getProductParam()+"_"+adjustOutVo.getBatchNumber()+"_"+adjustOutVo.getWarehouseName()+"的商品信息不存在库存记录");

            }
            adjustOutVo.setProductId(purchaseProductList.get(0).getProductId());
            adjustOutVo.setBatchDate(purchaseProductList.get(0).getBatchDate());
            adjustOutVo.setUnit(purchaseProductList.get(0).getUnit());

            AdjustInfo adjustInfo = new AdjustInfo();
            adjustInfo.setStatus(2);
            adjustInfo.setRemark(adjustOutVo.getRemark());
            adjustInfo.setAdjustType(2);
            adjustInfo.setAdjustReason(4);
            adjustInfo.setAdjustNo(RedisCode.getCode("TZCK"));
            adjustInfo.setDeptId(SecurityUtils.getLoginUser().getDeptId());
            adjustInfo.setCreateTime(now);
            adjustInfo.setCreateBy(SecurityUtils.getUserId());
            adjustInfo.setTenantId(SecurityUtils.getLoginUser().getTenantId());
            baseMapper.insert(adjustInfo);

            AdjustProduct adjustProduct = new AdjustProduct();
            BeanUtils.copyProperties(adjustOutVo,adjustProduct);
            adjustProduct.setAdjustId(adjustInfo.getId());
            adjustProduct.setRemark(adjustOutVo.getProductRemark());
            adjustProductMapper.insert(adjustProduct);

            List<PurchaseProduct> purchaseProducts = iRepertoryFlowService.convertFlowData(purchaseProductList, adjustProduct.getAdjustNum(), adjustInfo.getAdjustNo(),adjustInfo.getId());
            purchaseProducts.stream().forEach(s->{
                s.setCreateTime(adjustInfo.getCreateTime());
                s.setRemark(adjustProduct.getRemark());
                s.setCreateBy(adjustInfo.getCreateBy());
            });

            if(purchaseProducts.size() == 0){
                throw new ServiceException("添加库存调整流水失败！");
            }
            boolean repertoryFlow = iRepertoryFlowService.saveRepertoryFlow(purchaseProducts, 2, 7);
            if(!repertoryFlow){
                throw new ServiceException("库存调整失败！原因：增加调整出库流水失败。");
            }
        }
        return AjaxResult.success();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public AjaxResult importAddOutAdjustExport(List<AdjustOutVo> list){

        List<AdjustOutVo> errorList = new ArrayList<>();

        // 仓库
        List<WarehouseDto> allWarehouseList = warehouseMapper.getAllWarehouseList(null);
        Map<String,Long> warehouseMap = new HashMap<>();
        for (WarehouseDto dto:allWarehouseList) {
            warehouseMap.put(dto.getWarehouseName(),dto.getId());
        }

        Date now = new Date();
        for (int i = 0; i < list.size(); i++) {
            AdjustOutVo adjustOutVo = list.get(i);
            int hs = i+1;
            PurchaseProduct queryProductParam = new PurchaseProduct();
            BeanUtils.copyProperties(adjustOutVo,queryProductParam);
            queryProductParam.setWarehouseId(warehouseMap.get(adjustOutVo.getWarehouseName()));
            List<PurchaseProduct> purchaseProductList = purchaseProductMapper.getSalesListByParam(queryProductParam);
            if(CollectionUtils.isEmpty(purchaseProductList)){
                adjustOutVo.setErrorMsg("商品信息不存在库存记录");
                adjustOutVo.setHangshu(hs);
                errorList.add(adjustOutVo);
                continue;
            }
            adjustOutVo.setProductId(purchaseProductList.get(0).getProductId());
            adjustOutVo.setBatchDate(purchaseProductList.get(0).getBatchDate());
            adjustOutVo.setUnit(purchaseProductList.get(0).getUnit());

            AdjustInfo adjustInfo = new AdjustInfo();
            adjustInfo.setStatus(2);
            adjustInfo.setRemark(adjustOutVo.getRemark());
            adjustInfo.setAdjustType(2);
            adjustInfo.setAdjustReason(4);
            adjustInfo.setAdjustNo(RedisCode.getCode("TZCK"));
            adjustInfo.setDeptId(SecurityUtils.getLoginUser().getDeptId());
            adjustInfo.setCreateTime(now);
            adjustInfo.setCreateBy(SecurityUtils.getUserId());
            adjustInfo.setTenantId(SecurityUtils.getLoginUser().getTenantId());
            baseMapper.insert(adjustInfo);

            AdjustProduct adjustProduct = new AdjustProduct();
            BeanUtils.copyProperties(adjustOutVo,adjustProduct);
            adjustProduct.setAdjustId(adjustInfo.getId());
            adjustProduct.setRemark(adjustOutVo.getProductRemark());
            adjustProductMapper.insert(adjustProduct);

            List<PurchaseProduct> purchaseProducts = iRepertoryFlowService.convertFlowData(purchaseProductList, adjustProduct.getAdjustNum(), adjustInfo.getAdjustNo(),adjustInfo.getId());
            purchaseProducts.stream().forEach(s->{
                s.setCreateTime(adjustInfo.getCreateTime());
                s.setRemark(adjustProduct.getRemark());
                s.setCreateBy(adjustInfo.getCreateBy());
            });

            if(purchaseProducts.size() == 0){
                throw new ServiceException("添加库存调整流水失败！");
            }
            boolean repertoryFlow = iRepertoryFlowService.saveRepertoryFlow(purchaseProducts, 2, 7);
            if(!repertoryFlow){
                throw new ServiceException("库存调整失败！原因：增加调整出库流水失败。");
            }
        }
        return AjaxResult.success(errorList);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public AjaxResult importAddOutMatchExport(List<AdjustOutVo> list){

        List<AdjustOutVo> errorList = new ArrayList<>();

        // 仓库
        List<WarehouseDto> allWarehouseList = warehouseMapper.getAllWarehouseList(null);
        Map<String,Long> warehouseMap = new HashMap<>();
        for (WarehouseDto dto:allWarehouseList) {
            warehouseMap.put(dto.getWarehouseName(),dto.getId());
        }

        Date now = new Date();
        for (int i = 0; i < list.size(); i++) {
            AdjustOutVo adjustOutVo = list.get(i);
            int hs = i+1;
            PurchaseProduct queryProductParam = new PurchaseProduct();
            BeanUtils.copyProperties(adjustOutVo,queryProductParam);
            String matchParam = adjustOutVo.getMatchParam();
            queryProductParam.setMatchParam(matchParam.replaceAll("球镜柱镜",""));
            queryProductParam.setWarehouseId(warehouseMap.get(adjustOutVo.getWarehouseName()));
            List<PurchaseProduct> purchaseProductList = purchaseProductMapper.getSalesListByMatchParam(queryProductParam);
            if(CollectionUtils.isEmpty(purchaseProductList)){
                adjustOutVo.setErrorMsg("商品信息不存在库存记录");
                adjustOutVo.setHangshu(hs);
                errorList.add(adjustOutVo);
                continue;
            }
            adjustOutVo.setProductId(purchaseProductList.get(0).getProductId());
            adjustOutVo.setBatchDate(purchaseProductList.get(0).getBatchDate());
            adjustOutVo.setUnit(purchaseProductList.get(0).getUnit());

            AdjustInfo adjustInfo = new AdjustInfo();
            adjustInfo.setStatus(2);
            adjustInfo.setRemark(adjustOutVo.getRemark());
            adjustInfo.setAdjustType(2);
            adjustInfo.setAdjustReason(4);
            adjustInfo.setAdjustNo(RedisCode.getCode("TZCK"));
            adjustInfo.setDeptId(SecurityUtils.getLoginUser().getDeptId());
            adjustInfo.setCreateTime(now);
            adjustInfo.setCreateBy(SecurityUtils.getUserId());
            adjustInfo.setTenantId(SecurityUtils.getLoginUser().getTenantId());
            baseMapper.insert(adjustInfo);

            AdjustProduct adjustProduct = new AdjustProduct();
            BeanUtils.copyProperties(adjustOutVo,adjustProduct);
            adjustProduct.setAdjustId(adjustInfo.getId());
            adjustProduct.setRemark(adjustOutVo.getProductRemark());
            adjustProductMapper.insert(adjustProduct);

            List<PurchaseProduct> purchaseProducts = iRepertoryFlowService.convertFlowData(purchaseProductList, adjustProduct.getAdjustNum(), adjustInfo.getAdjustNo(),adjustInfo.getId());
            purchaseProducts.stream().forEach(s->{
                s.setCreateTime(adjustInfo.getCreateTime());
                s.setRemark(adjustProduct.getRemark());
                s.setCreateBy(adjustInfo.getCreateBy());
            });

            if(purchaseProducts.size() == 0){
                throw new ServiceException("添加库存调整流水失败！");
            }
            boolean repertoryFlow = iRepertoryFlowService.saveRepertoryFlow(purchaseProducts, 2, 7);
            if(!repertoryFlow){
                throw new ServiceException("库存调整失败！原因：增加调整出库流水失败。");
            }
        }
        return AjaxResult.success(errorList);
    }

    private void updateStorageAddNoFlow(AdjustInfo adjustInfo){
        // 因为这边是确认收货 所以先更新采购库存
//            List<PurchaseProduct> returnProductVoList = adjustProductMapper.productListByAdjustId(adjustInfo.getId());
            List<AdjustProduct> adjustProductList = adjustProductMapper.getListByAdjustId(adjustInfo.getId());

            List<PurchaseProduct> flowDataList = new ArrayList<>();
            for (AdjustProduct adjustProduct:adjustProductList) {
                PurchaseProduct queryProductParam = new PurchaseProduct();
                BeanUtils.copyProperties(adjustProduct,queryProductParam);
                List<PurchaseProduct> purchaseProductList = purchaseProductMapper.getSalesListByParam(queryProductParam);
                List<PurchaseProduct> purchaseProducts = iRepertoryFlowService.convertFlowData(purchaseProductList, adjustProduct.getAdjustNum(), adjustInfo.getAdjustNo(),adjustInfo.getId());
                for (PurchaseProduct purchaseProduct:purchaseProducts) {
                    purchaseProduct.setCreateTime(adjustInfo.getCreateTime());
                    purchaseProduct.setRemark(adjustProduct.getRemark());
                    purchaseProduct.setDeptId(adjustInfo.getDeptId());
                }
                flowDataList.addAll(purchaseProducts);

            }
            boolean repertoryFlow = iRepertoryFlowService.saveRepertoryFlow(flowDataList, 2, 7);
            if(!repertoryFlow){
                throw new ServiceException("退货失败！原因：增加退货库存流水失败。");
            }

    }


    private void updateStorageAddFlow(AdjustInfo adjustInfo){
        // 因为这边是确认收货 所以先更新采购库存

//            List<PurchaseProduct> returnProductVoList = adjustProductMapper.productListByAdjustId(adjustInfo.getId());
            List<AdjustProduct> adjustProductList = adjustProductMapper.getListByAdjustId(adjustInfo.getId());

            List<PurchaseProduct> flowDataList = new ArrayList<>();
            for (AdjustProduct adjustProduct:adjustProductList) {
                PurchaseProduct queryProductParam = new PurchaseProduct();
                BeanUtils.copyProperties(adjustProduct,queryProductParam);
                List<PurchaseProduct> purchaseProductList = purchaseProductMapper.getSalesListByParam(queryProductParam);
                List<PurchaseProduct> purchaseProducts = iRepertoryFlowService.convertFlowData(purchaseProductList, adjustProduct.getAdjustNum(), adjustInfo.getAdjustNo(),adjustInfo.getId());
                purchaseProducts.stream().forEach(s->{
                    s.setCreateTime(adjustInfo.getCreateTime());
                    s.setRemark(adjustProduct.getRemark());
                    s.setCreateBy(adjustInfo.getCreateBy());
                });
                flowDataList.addAll(purchaseProducts);
            }
            if(flowDataList.size() == 0){
                throw new ServiceException("添加库存调整流水失败！");
            }
            boolean repertoryFlow = iRepertoryFlowService.saveRepertoryFlow(flowDataList, 2, 7);
            if(!repertoryFlow){
                throw new ServiceException("库存调整失败！原因：增加调整出库流水失败。");
            }
    }

    @DataScope(deptAlias = "d", userAlias = "u")
    @Override
    public List<AdjustInfo> queryList(AdjustInfo adjustInfo){
        return baseMapper.queryList(adjustInfo);
    }

    @Override
    public AdjustInfoVo adjustDetailById(Long id){
        AdjustInfo adjustInfo = baseMapper.selectById(id);
        AdjustInfoVo adjustInfoVo = new AdjustInfoVo();
        SysUser sysUser = sysUserService.selectUserById(adjustInfo.getCreateBy());
        adjustInfo.setUserName(sysUser.getUserName());
        BeanUtils.copyProperties(adjustInfo,adjustInfoVo);
        List<AdjustProductVo> adjustProductVoList = adjustProductMapper.findListByAdjustId(id);
        if(!CollectionUtils.isEmpty(adjustProductVoList)){
            adjustProductVoList.stream().forEach(o->{
                PurchaseProduct purchaseProduct = new PurchaseProduct();
                BeanUtils.copyProperties(o,purchaseProduct);
                if(adjustInfo.getAdjustType().equals(1)){
                    purchaseProduct.setIfAddSupplier(1);
                }
                Integer countByNameAndParam = purchaseProductMapper.findCountByNameAndParam(purchaseProduct);
                o.setAvailableStock(countByNameAndParam);
            });
        }
        adjustInfoVo.setAdjustProductVoList(adjustProductVoList);
        return adjustInfoVo;
    }

//    @Override
//    public AjaxResult redInkEntry(ReturnInfo returnInfo){
//        if(Objects.isNull(returnInfo.getId())){
//            return AjaxResult.error("参数不能为空！");
//        }
//        ReturnInfo returnInfoData = baseMapper.selectById(returnInfo.getId());
//        returnInfoData.setStatus(3);
//        baseMapper.updateById(returnInfoData);
//        // 库存流水
//        updateStorageAddFlow(returnInfoData);
//        return AjaxResult.success("冲红成功");
//    }


//    /**
//     * 冲红
//     * @param blushVo
//     * @return
//     */
//    @Override
//    public AjaxResult blush(BlushVo blushVo) {
//        if(blushVo.getType()==1){
//            if(CollectionUtils.isEmpty(blushVo.getIdList())){
//                return AjaxResult.error("请选择冲红商品");
//            }
//        }
//        if(blushVo.getType()==2){
//            List<PurchaseProduct> list = iPurchaseProductService.list(new QueryWrapper<PurchaseProduct>().eq("purchase_id", blushVo.getId()).eq("status", 1));
//            if(!CollectionUtils.isEmpty(list)){
//                List<Long> collected = list.stream().map(i -> i.getId()).collect(Collectors.toList());
//                blushVo.setIdList(collected);
//            }
//        }
//        if(CollectionUtils.isEmpty(blushVo.getIdList())){
//            return AjaxResult.success("暂无可冲红商品");
//        }
//        int blush = iPurchaseProductService.blush(blushVo.getIdList());
//        if(blush>0){
//            int count = iPurchaseProductService.count(new QueryWrapper<PurchaseProduct>().eq("purchase_id", blushVo.getId()).eq("status", 1));
//            Purchase purchase = baseMapper.selectById(blushVo.getId());
//            if(Objects.isNull(purchase)){
//                return AjaxResult.error("冲红失败");
//            }
//            purchase.setStatus(count>0?4:3);
//            purchase.setUpdateTime(DateUtils.getNowDate());
//            purchase.setUpdateBy(SecurityUtils.getUserId());
//            int updatePurchase = purchaseMapper.updatePurchase(purchase);
//            if(updatePurchase>0){
//                blushVo.getIdList().stream().forEach(i->{
//                    PurchaseProduct purchaseProductInfo = iPurchaseProductService.getPurchaseProductInfo(i);
//                    purchaseProductInfo.setCreateBy(SecurityUtils.getUserId());
//                    if(!Objects.isNull(purchaseProductInfo)){
//                        Object object = JSON.parseObject(JSON.toJSONString(purchaseProductInfo), Object.class);
//                        iRepertoryFlowService.saveRepertoryFlow(iRepertoryFlowService.dataRepertoryFlow(object, 1,2));
//                    }
//                });
//            }
//            return AjaxResult.success("冲红成功");
//        }
//        return AjaxResult.error("冲红失败");
//    }
}
