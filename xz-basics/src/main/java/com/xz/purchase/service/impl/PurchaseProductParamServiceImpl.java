package com.xz.purchase.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.purchase.mapper.PurchaseProductParamMapper;
import com.xz.purchase.domain.PurchaseProductParam;
import com.xz.purchase.service.IPurchaseProductParamService;

/**
 * 采购商品属性Service业务层处理
 *
 * @author xz
 * @date 2024-01-06
 */
@Service
public class PurchaseProductParamServiceImpl extends ServiceImpl<PurchaseProductParamMapper, PurchaseProductParam> implements IPurchaseProductParamService {
    @Autowired
    private PurchaseProductParamMapper purchaseProductParamMapper;

    /**
     * 查询采购商品属性
     *
     * @param id 采购商品属性主键
     * @return 采购商品属性
     */
    @Override
    public PurchaseProductParam selectPurchaseProductParamById(Long id) {
        return purchaseProductParamMapper.selectPurchaseProductParamById(id);
    }

    /**
     * 查询采购商品属性列表
     *
     * @param purchaseProductParam 采购商品属性
     * @return 采购商品属性
     */
    @Override
    public List<PurchaseProductParam> selectPurchaseProductParamList(PurchaseProductParam purchaseProductParam) {
        return purchaseProductParamMapper.selectPurchaseProductParamList(purchaseProductParam);
    }

    /**
     * 新增采购商品属性
     *
     * @param purchaseProductParam 采购商品属性
     * @return 结果
     */
    @Override
    public int insertPurchaseProductParam(PurchaseProductParam purchaseProductParam) {
        return purchaseProductParamMapper.insertPurchaseProductParam(purchaseProductParam);
    }

    /**
     * 修改采购商品属性
     *
     * @param purchaseProductParam 采购商品属性
     * @return 结果
     */
    @Override
    public int updatePurchaseProductParam(PurchaseProductParam purchaseProductParam) {
        return purchaseProductParamMapper.updatePurchaseProductParam(purchaseProductParam);
    }

    /**
     * 批量删除采购商品属性
     *
     * @param ids 需要删除的采购商品属性主键
     * @return 结果
     */
    @Override
    public int deletePurchaseProductParamByIds(Long[] ids) {
        return purchaseProductParamMapper.deletePurchaseProductParamByIds(ids);
    }

    /**
     * 删除采购商品属性信息
     *
     * @param id 采购商品属性主键
     * @return 结果
     */
    @Override
    public int deletePurchaseProductParamById(Long id) {
        return purchaseProductParamMapper.deletePurchaseProductParamById(id);
    }
}
