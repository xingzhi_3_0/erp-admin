package com.xz.purchase.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.annotation.DataScope;
import com.xz.purchase.domain.PurchaseProduct;
import com.xz.purchase.vo.ChoosePurchaseProductVo;
import com.xz.purchase.vo.ProductVo;
import com.xz.repertory.domain.RepertoryFlow;

/**
 * 采购商品明细Service接口
 *
 * @author xz
 * @date 2024-01-06
 */
public interface IPurchaseProductService extends IService<PurchaseProduct> {
  /**
   * 查询采购商品明细
   *
   * @param id 采购商品明细主键
   * @return 采购商品明细
   */
  public PurchaseProduct selectPurchaseProductById(Long id);

  /**
   * 查询采购商品明细列表
   *
   * @param purchaseProduct 采购商品明细
   * @return 采购商品明细集合
   */
  public List<PurchaseProduct> selectPurchaseProductList(PurchaseProduct purchaseProduct);

  /**
   * 新增采购商品明细
   *
   * @param purchaseProduct 采购商品明细
   * @return 结果
   */
  public int insertPurchaseProduct(PurchaseProduct purchaseProduct);

  /**
   * 修改采购商品明细
   *
   * @param purchaseProduct 采购商品明细
   * @return 结果
   */
  public int updatePurchaseProduct(PurchaseProduct purchaseProduct);

  /**
   * 批量删除采购商品明细
   *
   * @param ids 需要删除的采购商品明细主键集合
   * @return 结果
   */
  public int deletePurchaseProductByIds(Long[] ids);

  /**
   * 冲红
   *
   * @param idList
   * @return
   */
  public int blush(List<Long> idList);

  /**
   * 删除采购商品明细信息
   *
   * @param id 采购商品明细主键
   * @return 结果
   */
  public int deletePurchaseProductById(Long id);

  /**
   * 获取采购商品信息
   *
   * @param id
   * @return
   */
  PurchaseProduct getPurchaseProductInfo(Long id);

  /**
   * 更新商品单价
   */
  int updateCostPrice(RepertoryFlow repertoryFlow);

  /**
   * 获取采购入库id
   *
   * @param repertoryFlow
   * @return
   */
  Long selectPurchaseId(RepertoryFlow repertoryFlow);

  List<ProductVo> chooseProductList(PurchaseProduct purchaseProduct);

  List<ProductVo> choosePurchaseProductList(PurchaseProduct purchaseProduct);

  List<PurchaseProduct> validityProductList(PurchaseProduct purchaseProduct);

  List<PurchaseProduct> getListByParam(PurchaseProduct purchaseProduct);

  /**
   * 查询有效的采购商品（贸易中间商）
   *
   * @param purchaseProduct
   * @return
   */
  List<ProductVo> chooseProductListForSupplier(PurchaseProduct purchaseProduct);

  /**
   * 诊疗开单新增药品专用
   *
   * @param purchaseProduct
   * @return
   */
  List<ChoosePurchaseProductVo> chooseProductForDiagnosisae(PurchaseProduct purchaseProduct);
}
