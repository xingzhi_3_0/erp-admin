package com.xz.purchase.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.purchase.mapper.AllotPurchaseMapper;
import com.xz.purchase.domain.AllotPurchase;
import com.xz.purchase.service.IAllotPurchaseService;

/**
 * 调拨入库商品记录Service业务层处理
 *
 * @author xz
 * @date 2024-04-18
 */
@Service
public class AllotPurchaseServiceImpl extends ServiceImpl<AllotPurchaseMapper, AllotPurchase> implements IAllotPurchaseService {
    @Autowired
    private AllotPurchaseMapper allotPurchaseMapper;

    /**
     * 查询调拨入库商品记录
     *
     * @param id 调拨入库商品记录主键
     * @return 调拨入库商品记录
     */
    @Override
    public AllotPurchase selectAllotPurchaseById(Long id) {
        return allotPurchaseMapper.selectAllotPurchaseById(id);
    }

    /**
     * 查询调拨入库商品记录列表
     *
     * @param allotPurchase 调拨入库商品记录
     * @return 调拨入库商品记录
     */
    @Override
    public List<AllotPurchase> selectAllotPurchaseList(AllotPurchase allotPurchase) {
        return allotPurchaseMapper.selectAllotPurchaseList(allotPurchase);
    }

    /**
     * 新增调拨入库商品记录
     *
     * @param allotPurchase 调拨入库商品记录
     * @return 结果
     */
    @Override
    public int insertAllotPurchase(AllotPurchase allotPurchase) {
        return allotPurchaseMapper.insertAllotPurchase(allotPurchase);
    }

    /**
     * 修改调拨入库商品记录
     *
     * @param allotPurchase 调拨入库商品记录
     * @return 结果
     */
    @Override
    public int updateAllotPurchase(AllotPurchase allotPurchase) {
        return allotPurchaseMapper.updateAllotPurchase(allotPurchase);
    }

    /**
     * 批量删除调拨入库商品记录
     *
     * @param ids 需要删除的调拨入库商品记录主键
     * @return 结果
     */
    @Override
    public int deleteAllotPurchaseByIds(Long[] ids) {
        return allotPurchaseMapper.deleteAllotPurchaseByIds(ids);
    }

    /**
     * 删除调拨入库商品记录信息
     *
     * @param id 调拨入库商品记录主键
     * @return 结果
     */
    @Override
    public int deleteAllotPurchaseById(Long id) {
        return allotPurchaseMapper.deleteAllotPurchaseById(id);
    }
}
