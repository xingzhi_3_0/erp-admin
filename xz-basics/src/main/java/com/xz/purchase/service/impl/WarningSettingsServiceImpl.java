package com.xz.purchase.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.annotation.DataScope;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.exception.ServiceException;
import com.xz.common.utils.RedisCode;
import com.xz.common.utils.SecurityUtils;
import com.xz.purchase.domain.AdjustInfo;
import com.xz.purchase.domain.AdjustProduct;
import com.xz.purchase.domain.PurchaseProduct;
import com.xz.purchase.domain.WarningSettings;
import com.xz.purchase.mapper.*;
import com.xz.purchase.param.AdjustInfoParam;
import com.xz.purchase.param.WarningSettingParam;
import com.xz.purchase.service.IAdjustInfoService;
import com.xz.purchase.service.IWarningSettingsService;
import com.xz.purchase.vo.*;
import com.xz.repertory.service.IRepertoryFlowService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * 预警设置Service业务层处理
 *
 * @author xz
 * @date 2024-1-19 11:44:11
 */
@Service
public class WarningSettingsServiceImpl extends ServiceImpl<WarningSettingsMapper, WarningSettings> implements IWarningSettingsService {

    @Resource
    private PurchaseMapper purchaseMapper;


    @DataScope(deptAlias = "d", userAlias = "u")
    @Override
    public List<WarningSettingsVo> queryList(WarningSettings warningSettings) {
        PurchaseProduct purchaseProduct = new PurchaseProduct();
        List<WarningSettingsVo> warningSettingsVos = baseMapper.queryList(warningSettings);
        //调拨在途
        List<TransferStockVo> stockVoList = purchaseMapper.transferStock(purchaseProduct);
        //采购在途
        List<PurchaseTransitVo> transitVoList = purchaseMapper.purchaseTransit(purchaseProduct);

        for (WarningSettingsVo o:warningSettingsVos) {
            o.setPurchaseInventory(0);
            Integer purchaseInventory=0;//采购在途
            //采购在途
            long purchaseTransitCount = transitVoList.stream().filter(i ->
                    i.getProductName().equals(o.getProductName())
                            && i.getProductParam().equals(o.getProductParam())
            ).count();
            purchaseInventory=new Long(purchaseTransitCount).intValue();
            o.setPurchaseInventory(purchaseInventory);
            if(o.getAvailableStockTotal()==0){
                o.setTransferStock(0);
                o.setAvailableStock(0);
                o.setPreoccupiedInventory(0);
                continue;
            }
            Integer availableStockTotal = o.getAvailableStockTotal();//总库存
            Integer preoccupiedInventory=0;//预占库存

            //调拨在途
            Integer transferStock=transferStock(stockVoList,o.getProductName(),o.getProductParam());
            o.setTransferStock(transferStock);

            o.setTransferStock(transferStock);
            o.setPreoccupiedInventory(0);
            o.setAvailableStock(availableStockTotal-transferStock-preoccupiedInventory);

        }
        return warningSettingsVos;
    }

    //调拨在途
    public Integer transferStock(List<TransferStockVo> stockVoList,String productName,String productParam){
        //调拨在途
        long count = stockVoList.stream().filter(i ->
                i.getProductName().equals(productName)
                        && i.getProductParam().equals(productParam)
        ).count();
        return  new Long(count).intValue();
    }

    @Override
    public List<WarningSettings> setWarningList(WarningSettings warningSettings) {
        return baseMapper.setWarningList(warningSettings);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public AjaxResult setUp(WarningSettingParam param){
        List<WarningSettings> warningSettingsList = param.getWarningSettingsList();
        try {
            for (WarningSettings obj:warningSettingsList) {
                if(Objects.isNull(obj.getId())){
                    baseMapper.insert(obj);
                }else {
                    WarningSettings warningSettings = baseMapper.selectById(obj.getId());
                    warningSettings.setMinNum(obj.getMinNum());
                    warningSettings.setMaxNum(obj.getMaxNum());
                    baseMapper.updateById(warningSettings);
                }
            }
        }catch (Exception e){
            throw new ServiceException("设置失败！");
        }
        return AjaxResult.success("设置成功！");
    }
}
