package com.xz.purchase.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.purchase.domain.AllotPurchase;

/**
 * 调拨入库商品记录Service接口
 * 
 * @author xz
 * @date 2024-04-18
 */
public interface IAllotPurchaseService extends IService<AllotPurchase> {
    /**
     * 查询调拨入库商品记录
     * 
     * @param id 调拨入库商品记录主键
     * @return 调拨入库商品记录
     */
    public AllotPurchase selectAllotPurchaseById(Long id);

    /**
     * 查询调拨入库商品记录列表
     * 
     * @param allotPurchase 调拨入库商品记录
     * @return 调拨入库商品记录集合
     */
    public List<AllotPurchase> selectAllotPurchaseList(AllotPurchase allotPurchase);

    /**
     * 新增调拨入库商品记录
     * 
     * @param allotPurchase 调拨入库商品记录
     * @return 结果
     */
    public int insertAllotPurchase(AllotPurchase allotPurchase);

    /**
     * 修改调拨入库商品记录
     * 
     * @param allotPurchase 调拨入库商品记录
     * @return 结果
     */
    public int updateAllotPurchase(AllotPurchase allotPurchase);

    /**
     * 批量删除调拨入库商品记录
     * 
     * @param ids 需要删除的调拨入库商品记录主键集合
     * @return 结果
     */
    public int deleteAllotPurchaseByIds(Long[] ids);

    /**
     * 删除调拨入库商品记录信息
     * 
     * @param id 调拨入库商品记录主键
     * @return 结果
     */
    public int deleteAllotPurchaseById(Long id);
}
