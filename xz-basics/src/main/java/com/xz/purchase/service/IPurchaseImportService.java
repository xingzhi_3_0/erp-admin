package com.xz.purchase.service;

import com.xz.common.core.domain.AjaxResult;
import com.xz.purchase.dto.PurchaseProductImportDto;
import com.xz.purchase.dto.PurchaseInfoDto;

import java.util.List;

/**
 * 采购导入Service接口
 *
 * @author xz
 * @date 2024-01-06
 */
public interface IPurchaseImportService {

	/**
	 * 导入库存商品
	 * @param importBatchNo 此次系统批量入库的批次号
	 * @param importDtos 导入的数据
	 * @return 导入失败的数据
	 */
	public List<PurchaseProductImportDto>  importExcel(String importBatchNo,List<PurchaseProductImportDto> importDtos);

}
