package com.xz.purchase.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.purchase.domain.ReportDamageProduct;

/**
 * 报损商品明细Service接口
 * 
 * @author xz
 * @date 2024-01-16
 */
public interface IReportDamageProductService extends IService<ReportDamageProduct> {
    /**
     * 查询报损商品明细
     * 
     * @param id 报损商品明细主键
     * @return 报损商品明细
     */
    public ReportDamageProduct selectReportDamageProductById(Long id);

    /**
     * 查询报损商品明细列表
     * 
     * @param reportDamageProduct 报损商品明细
     * @return 报损商品明细集合
     */
    public List<ReportDamageProduct> selectReportDamageProductList(ReportDamageProduct reportDamageProduct);

    /**
     * 新增报损商品明细
     * 
     * @param reportDamageProduct 报损商品明细
     * @return 结果
     */
    public int insertReportDamageProduct(ReportDamageProduct reportDamageProduct);

    /**
     * 修改报损商品明细
     * 
     * @param reportDamageProduct 报损商品明细
     * @return 结果
     */
    public int updateReportDamageProduct(ReportDamageProduct reportDamageProduct);

    /**
     * 批量删除报损商品明细
     * 
     * @param ids 需要删除的报损商品明细主键集合
     * @return 结果
     */
    public int deleteReportDamageProductByIds(Long[] ids);

    /**
     * 删除报损商品明细信息
     * 
     * @param id 报损商品明细主键
     * @return 结果
     */
    public int deleteReportDamageProductById(Long id);
}
