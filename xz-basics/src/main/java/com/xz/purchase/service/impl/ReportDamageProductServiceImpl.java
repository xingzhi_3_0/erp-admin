package com.xz.purchase.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.purchase.mapper.ReportDamageProductMapper;
import com.xz.purchase.domain.ReportDamageProduct;
import com.xz.purchase.service.IReportDamageProductService;

/**
 * 报损商品明细Service业务层处理
 *
 * @author xz
 * @date 2024-01-16
 */
@Service
public class ReportDamageProductServiceImpl extends ServiceImpl<ReportDamageProductMapper, ReportDamageProduct> implements IReportDamageProductService {
    @Autowired
    private ReportDamageProductMapper reportDamageProductMapper;

    /**
     * 查询报损商品明细
     *
     * @param id 报损商品明细主键
     * @return 报损商品明细
     */
    @Override
    public ReportDamageProduct selectReportDamageProductById(Long id) {
        return reportDamageProductMapper.selectReportDamageProductById(id);
    }

    /**
     * 查询报损商品明细列表
     *
     * @param reportDamageProduct 报损商品明细
     * @return 报损商品明细
     */
    @Override
    public List<ReportDamageProduct> selectReportDamageProductList(ReportDamageProduct reportDamageProduct) {
        return reportDamageProductMapper.selectReportDamageProductList(reportDamageProduct);
    }

    /**
     * 新增报损商品明细
     *
     * @param reportDamageProduct 报损商品明细
     * @return 结果
     */
    @Override
    public int insertReportDamageProduct(ReportDamageProduct reportDamageProduct) {
        return reportDamageProductMapper.insertReportDamageProduct(reportDamageProduct);
    }

    /**
     * 修改报损商品明细
     *
     * @param reportDamageProduct 报损商品明细
     * @return 结果
     */
    @Override
    public int updateReportDamageProduct(ReportDamageProduct reportDamageProduct) {
        return reportDamageProductMapper.updateReportDamageProduct(reportDamageProduct);
    }

    /**
     * 批量删除报损商品明细
     *
     * @param ids 需要删除的报损商品明细主键
     * @return 结果
     */
    @Override
    public int deleteReportDamageProductByIds(Long[] ids) {
        return reportDamageProductMapper.deleteReportDamageProductByIds(ids);
    }

    /**
     * 删除报损商品明细信息
     *
     * @param id 报损商品明细主键
     * @return 结果
     */
    @Override
    public int deleteReportDamageProductById(Long id) {
        return reportDamageProductMapper.deleteReportDamageProductById(id);
    }
}
