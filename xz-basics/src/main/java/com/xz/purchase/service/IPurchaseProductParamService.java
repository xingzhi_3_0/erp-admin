package com.xz.purchase.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.purchase.domain.PurchaseProductParam;

/**
 * 采购商品属性Service接口
 * 
 * @author xz
 * @date 2024-01-06
 */
public interface IPurchaseProductParamService extends IService<PurchaseProductParam> {
    /**
     * 查询采购商品属性
     * 
     * @param id 采购商品属性主键
     * @return 采购商品属性
     */
    public PurchaseProductParam selectPurchaseProductParamById(Long id);

    /**
     * 查询采购商品属性列表
     * 
     * @param purchaseProductParam 采购商品属性
     * @return 采购商品属性集合
     */
    public List<PurchaseProductParam> selectPurchaseProductParamList(PurchaseProductParam purchaseProductParam);

    /**
     * 新增采购商品属性
     * 
     * @param purchaseProductParam 采购商品属性
     * @return 结果
     */
    public int insertPurchaseProductParam(PurchaseProductParam purchaseProductParam);

    /**
     * 修改采购商品属性
     * 
     * @param purchaseProductParam 采购商品属性
     * @return 结果
     */
    public int updatePurchaseProductParam(PurchaseProductParam purchaseProductParam);

    /**
     * 批量删除采购商品属性
     * 
     * @param ids 需要删除的采购商品属性主键集合
     * @return 结果
     */
    public int deletePurchaseProductParamByIds(Long[] ids);

    /**
     * 删除采购商品属性信息
     * 
     * @param id 采购商品属性主键
     * @return 结果
     */
    public int deletePurchaseProductParamById(Long id);
}
