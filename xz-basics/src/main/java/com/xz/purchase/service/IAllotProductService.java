package com.xz.purchase.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.purchase.domain.AllotProduct;

/**
 * 调拨商品明细Service接口
 * 
 * @author xz
 * @date 2024-01-10
 */
public interface IAllotProductService extends IService<AllotProduct> {
    /**
     * 查询调拨商品明细
     * 
     * @param id 调拨商品明细主键
     * @return 调拨商品明细
     */
    public AllotProduct selectAllotProductById(Long id);

    /**
     * 查询调拨商品明细列表
     * 
     * @param allotProduct 调拨商品明细
     * @return 调拨商品明细集合
     */
    public List<AllotProduct> selectAllotProductList(AllotProduct allotProduct);

    /**
     * 新增调拨商品明细
     * 
     * @param allotProduct 调拨商品明细
     * @return 结果
     */
    public int insertAllotProduct(AllotProduct allotProduct);

    /**
     * 修改调拨商品明细
     * 
     * @param allotProduct 调拨商品明细
     * @return 结果
     */
    public int updateAllotProduct(AllotProduct allotProduct);

    /**
     * 批量删除调拨商品明细
     * 
     * @param ids 需要删除的调拨商品明细主键集合
     * @return 结果
     */
    public int deleteAllotProductByIds(Long[] ids);

    /**
     * 删除调拨商品明细信息
     * 
     * @param id 调拨商品明细主键
     * @return 结果
     */
    public int deleteAllotProductById(Long id);
}
