package com.xz.purchase.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.common.annotation.DataScope;
import com.xz.common.core.domain.AjaxResult;
import com.xz.product.domain.Product;
import com.xz.purchase.domain.PurchaseProduct;
import com.xz.purchase.dto.PurchaseDto;
import com.xz.purchase.dto.PurchaseImportVo;
import com.xz.purchase.dto.PurchaseInfoDto;
import com.xz.purchase.domain.Purchase;
import com.xz.purchase.dto.PurchaseStatisticsDto;
import com.xz.purchase.vo.*;

/**
 * 采购Service接口
 * 
 * @author xz
 * @date 2024-01-06
 */
public interface IPurchaseService extends IService<Purchase> {
    /**
     * 查询采购
     * 
     * @param id 采购主键
     * @return 采购
     */
    public PurchaseInfoDto selectPurchaseById(Long id);
    /**
     * 导入库存商品
     * @param list
     * @return
     */
    AjaxResult importExcel(List<PurchaseImportVo> list);

    /**
     * 查询采购列表
     * 
     * @param purchase 采购
     * @return 采购集合
     */
    public List<PurchaseDto> selectPurchaseList(Purchase purchase);

    /**
     * 新增采购
     * 
     * @param purchase 采购
     * @return 结果
     */
    public AjaxResult insertPurchase(Purchase purchase);

    /**
     * 采购订单入库
     * @param purchase
     * @return
     */
    public AjaxResult purchaseWarehousing(Purchase purchase);

    /**
     * 修改采购
     * 
     * @param purchase 采购
     * @return 结果
     */
    public AjaxResult updatePurchase(Purchase purchase);

    /**
     * 批量删除采购
     * 
     * @param ids 需要删除的采购主键集合
     * @return 结果
     */
    public int deletePurchaseByIds(Long[] ids);

    /**
     * 删除采购信息
     * 
     * @param id 采购主键
     * @return 结果
     */
    public int deletePurchaseById(Long id);

    /**
     * blushVo
     * @param blushVo
     * @return
     */
    AjaxResult blush(BlushVo blushVo);
    /**
     * 商品维度统计
     * @param purchaseProduct
     * @return
     */
    List<CommodityDimensionStatisticsVo> commodityDimensionStatisticsList(PurchaseProduct purchaseProduct);
    /**
     * 财务维度统计
     * @param purchaseProduct
     * @return
     */
    List<BillDimensionStatisticsVo> billDimensionStatisticsList(PurchaseProduct purchaseProduct);
    /**
     * 批次维度统计
     * @param purchaseProduct
     * @return
     */
    List<BatchDimensionStatisticsVo> batchDimensionStatistics(PurchaseProduct purchaseProduct);

    /**
     * 进销存统计
     * @param purchase
     * @return
     */
    List<PurchaseStatisticsVo> purchaseStatistics(Purchase purchase);
}
