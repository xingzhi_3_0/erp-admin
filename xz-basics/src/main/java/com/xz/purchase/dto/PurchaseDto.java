package com.xz.purchase.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName PurchaseDto * @Description TODO
 * @Author Administrator
 * @Date 9:53 2024/1/6
 * @Version 1.0
 **/
@Data
public class PurchaseDto {
    /** id */
    private Long id;
    /**
     * 入库单号
     */
    private String purchaseNumber;

    /** 供应商 */
    @Excel(name = "供应商")
    private String supplierName;

    /** 仓库 */
    @Excel(name = "仓库")
    private String warehouseName;

    /** 状态（1.待入库 2.已入库 3.已冲红 4.部分冲红 5.待审核 6.审核未过） */
    @Excel(name = "状态", readConverterExp = "1=.待入库,2=.已入库,3=.已冲红,4=.部分冲红,5=.待审核,6=.审核未过")
    private Integer status;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 创建人 */
    private String userName;
    /** 入库数量 */
    private Integer number;
    /** 入库类型（1.自制入库 2.采购入库 3.库存调整 4.调拨入库） */
    @Excel(name = "入库类型", readConverterExp = "1=.自制入库,2=.采购入库,3=.库存调整,4=.调拨入库")
    private Integer storageType;

}
