package com.xz.purchase.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName PurchasePriceDto * @Description TODO
 * @Author Administrator
 * @Date 9:18 2024/1/9
 * @Version 1.0 进货价表
 **/
@Data
public class PurchasePriceDto {
    /** id */
    private Long id;

    /** 供应商 */
    @Excel(name = "供应商")
    private String supplierName;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private String productEncoded;
    /** 单位 */
    private String unit;

    /** 采购单价 */
    @Excel(name = "采购单价")
    private BigDecimal purchasePrice;
    /** 更新时间 */

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
