package com.xz.purchase.dto;

import com.xz.common.annotation.Excel;
import com.xz.common.utils.StringUtils;
import com.xz.product.dto.AttributeParamDto;
import com.xz.product.dto.ProductDto;
import com.xz.purchase.domain.Purchase;
import com.xz.purchase.domain.PurchasePrice;
import com.xz.purchase.domain.PurchaseProduct;
import com.xz.supplier.domain.Supplier;
import com.xz.warehouse.domain.Warehouse;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 采购导入
 */
@Data
public class PurchaseResourceDto {

	// 仓库
	private Map<String, Warehouse> warehouseMap;
	// 供应商
	private Map<String, Supplier> supplierMap;
	// 商品
	private Map<String, ProductDto> productMap;
	// 供应商-商品采购价格
	private Map<String, BigDecimal> purchasePriceMap;
	// 商品属性参数
	private Map<Long, List<AttributeParamDto>> productParamMap = new HashMap<>();

	public PurchaseResourceDto() {
	}

	public PurchaseResourceDto(Map<String, Warehouse> warehouseMap, Map<String, Supplier> supplierMap, Map<String,
			ProductDto> productMap) {
		this.warehouseMap = warehouseMap;
		this.supplierMap = supplierMap;
		this.productMap = productMap;
	}

	public Warehouse getWarehouse(String name) {
		return StringUtils.isEmpty(name) ? null : warehouseMap.get(name.trim());
	}

	public Supplier getSupplier(String name) {
		return StringUtils.isEmpty(name) ? null : supplierMap.get(name.trim());
	}

	public ProductDto getProduct(String name) {
		return StringUtils.isEmpty(name) ? null : productMap.get(name.trim());
	}

	public List<AttributeParamDto> getProductParam(Long productId) {
		return null == productId ? null : productParamMap.get(productId);
	}

	public BigDecimal getPurchasePrice(Long productId, Long supplierId) {
		if(null == productId || null == supplierId){
			return null;
		}
		return purchasePriceMap.get(productId + "--" +supplierId);
	}
}
