package com.xz.purchase.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.xz.common.annotation.Excel;
import com.xz.common.utils.StringUtils;
import com.xz.purchase.domain.PurchaseProduct;
import lombok.Data;
import org.apache.poi.ss.usermodel.CellType;

import java.util.Date;

/**
 * 采购导入
 */
@Data
public class PurchaseProductImportDto {

	// 是否失败
	private boolean isFail;
	// 校验结果
	@Excel(name = "校验结果",width = 40)
	private String resultInfo;

	@Excel(name = "*商品名称")
	private String productName;
	/*@Excel(name = "商品参数")
	private String productParam;*/
	@Excel(name = "球镜")
	private String parameterOne;
	@Excel(name = "柱镜")
	private String parameterTwo;
	@Excel(name = "轴向")
	private String parameterThree;
	@Excel(name = "眼别")
	private String parameterFour;
	@Excel(name = "尺寸")
	private String parameterFive;
	@Excel(name = "颜色")
	private String parameterSix;
	@Excel(name = "其他")
	private String parameterSeven;

	@Excel(name = "*仓库")
	private String warehouseName;
	@Excel(name = "*入库数量")
	private Integer storageNum;

	@Excel(name = "供货单号")
	private String deliveryNumber;
	@Excel(name = "生产批号/SN码")
	private String batchNumber;
	@Excel(name = "生产日期", dateFormat = "yyyy-MM-dd")
	private Date batchDate;
	@Excel(name = "有效期至", dateFormat = "yyyy-MM-dd")
	private Date validity;
	@Excel(name = "*供应商")
	private String supplierName;
	@Excel(name = "商品备注")
	private String remark;
	/** 成本单价 */
	//@Excel(name = "进货单价")
	//private BigDecimal costPrice;
	// 转换后的商品数据
	//private PurchaseProduct purchaseProduct;
}
