package com.xz.purchase.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName PurchaseImportVo * @Description TODO
 * @Author Administrator
 * @Date 10:07 2024/3/2
 * @Version 1.0 库存导入
 **/
@Data
public class PurchaseImportVo {
    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;
    @Excel(name = "球镜")
    private String parameterOne;
    @Excel(name = "柱镜")
    private String parameterTwo;
    @Excel(name = "轴向")
    private String parameterThree;
    @Excel(name = "尺寸")
    private String parameterFour;
    @Excel(name = "颜色")
    private String parameterFive;
    @Excel(name = "其他参数")
    private String parameterSix;
    /** 仓库 */
    @Excel(name = "仓库")
    private String warehouseName;
    /** 生成批号 */
    @Excel(name = "生产批号/SN码")
    private String batchNumber;
    /** 生成日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生成日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date batchDate;
    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "有效期至", width = 30, dateFormat = "yyyy-MM-dd")
    private Date validity;
    /** 入库数量 */
    @Excel(name = "数量")
    private Integer storageNum;
    /** 单位*/
    @Excel(name = "单位")
    private String unit;
    /** 供应商 */
    @Excel(name = "供应商")
    private String supplierName;
    /** 成本单价 */
    @Excel(name = "进货单价")
    private BigDecimal costPrice;
}
