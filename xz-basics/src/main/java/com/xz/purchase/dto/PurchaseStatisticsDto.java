package com.xz.purchase.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName PurchaseStatisticsDto * @Description TODO
 * @Author Administrator
 * @Date 13:59 2024/3/29
 * @Version 1.0 进销存统计
 **/
@Data
public class PurchaseStatisticsDto {
    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
    /** 仓库id */
    private String warehouseName;
    /*分类*/
    private String productCategory;
    /*商品名称*/
    private String productName;
    /** 请求参数 */
    @TableField(exist = false)
    private Map<String, Object> params;

    public Map<String, Object> getParams()
    {
        if (params == null)
        {
            params = new HashMap<>();
        }
        return params;
    }
}
