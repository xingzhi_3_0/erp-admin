package com.xz.purchase.domain;

import com.xz.common.annotation.Excel;
import lombok.Data;

/**
 * @ClassName ProcessingReportDamageVo * @Description TODO
 * @Author Administrator
 * @Date 11:35 2024/2/19
 * @Version 1.0  加工单报损
 **/
@Data
public class ProcessingReportDamageVo {
    //加工单号
    private String processNo;
    private String id;
    //价格
    private String price;
    //商品名称
    private String productName;
    //商品参数
    private String productParam;
    //生产批号/SN码
    private String batchNumber;
    //有效期
    private String validity;
    //单价
    private String unit;
    @Excel(name = "仓库id")
    private Long warehouseId;

    /** 调出仓库 */
    @Excel(name = "调出仓库")
    private String warehouseName;
    private Integer processNum;
}
