package com.xz.purchase.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 采购对象 t_purchase
 * 
 * @author xz
 * @date 2024-1-9 18:11:18
 */
@Data
@TableName("t_warning_settings")
public class WarningSettings extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /**
     * 商品名称
     */
    @Excel(name = "商品名称")
    private String productName;

    /**
     * 产品id
     */
    @Excel(name = "产品id")
    private Long productId;

    /** 商品参数 */
    @Excel(name = "商品参数")
    private String productParam;

    /** 品牌 */
    @Excel(name = "品牌")
    private String brandName;

    /** 类别 */
    @Excel(name = "类别")
    private String productCategory;

    /** 最低库存 */
    @Excel(name = "最低库存")
    private Integer minNum;

    /** 最高库存 */
    @Excel(name = "最高库存")
    private Integer maxNum;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    /**
     * 状态
     */
    @TableField(exist = false)
    public List<Integer> warningStatusList;
    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;

    /**
     * 产品名称
     */
    private String prodName;

    @TableField(exist = false)
    private Integer availableStock;

    @TableField(exist = false)
    @Excel(name = "预警状态", readConverterExp = "1=.高于上限,2=.低于下限")
    private String warningStatus;

}
