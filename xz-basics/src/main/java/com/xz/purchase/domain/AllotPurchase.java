package com.xz.purchase.domain;

import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 调拨入库商品记录对象 t_allot_purchase
 * 
 * @author xz
 * @date 2024-04-18
 */
@Data
@TableName("t_allot_purchase")
public class AllotPurchase{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 调拨单id */
    @Excel(name = "调拨单id")
    private Long allotId;

    /** 明细id */
    @Excel(name = "明细id")
    private Long detailId;

    /** 采购入库id */
    @Excel(name = "采购入库id")
    private Long purchaseProductId;

    /** 销售数量 */
    @Excel(name = "销售数量")
    private Integer purchaseProductNum;

}
