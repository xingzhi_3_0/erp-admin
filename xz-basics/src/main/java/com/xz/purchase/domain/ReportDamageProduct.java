package com.xz.purchase.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 报损商品明细对象 t_report_damage_product
 * 
 * @author xz
 * @date 2024-01-16
 */
@Data
@TableName("t_report_damage_product")
public class ReportDamageProduct{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 报损id */
    @Excel(name = "报损id")
    private Long reportDamageId;

    /** 报损数量 */
    @Excel(name = "报损数量")
    private Integer reportDamageNum;


    /** 单价 */
    @Excel(name = "单价")
    private BigDecimal price;

    /** 金额 */
    @Excel(name = "金额")
    private BigDecimal amount;
    /** 仓库id */
    private Long warehouseId;
    /** 仓库名称 */
    private String warehouseName;
    /** 生产批号 */
    private String batchNumber;
    /** 生产日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date batchDate;
    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date validity;
    /** 商品参数 */
    private String productParam;
    /** 单位 */
    private String unit;
    private String remark;
    /** 商品名称 */
    private String productName;
    /** 可用库存 */
    private Integer availableStock;
    /*加工单明细id*/
    private Long processItemId;

}
