package com.xz.purchase.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 采购商品明细对象 t_purchase_product
 *
 * @author xz
 * @date 2024-01-06
 */
@Data
@TableName("t_purchase_product")
public class PurchaseProduct extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;
    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;
    /** 商品id */
    @Excel(name = "商品id")
    private Long productId;
    @Excel(name = "关联采购id")
    private Long relevancePurchaseId;
    /** 商品编码 */
    @Excel(name = "商品编码")
    private String productEncoded;

    /** 采购id */
    @Excel(name = "采购id")
    private Long purchaseId;

    /** 入库数量 */
    @Excel(name = "入库数量")
    private Integer storageNum;
    /*实时库存*/
    private Integer availableStock;

    /** 供货单号 */
    @Excel(name = "供货单号")
    private String deliveryNumber;

    /** 生成批号 */
    @Excel(name = "生成批号")
    private String batchNumber;

    /** 生成日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生成日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date batchDate;

    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "有效期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date validity;

    /** 生产厂商 */
    @Excel(name = "生产厂商")
    private String manufacturer;

    /** 注册证号 */
    @Excel(name = "注册证号")
    private String registrationNumber;
    private String remark;
    /**
     * 部门id
     */
    private Long deptId;
    /** 租户id */
    private Long tenantId;
    /**
     * 采购商品属性
     */
    @TableField(exist = false)
    private List<PurchaseProductParam> purchaseProductParamList;
    /**
     * 采购商品参数
     */
    private String productParam;
    /**
     * 单号
     */
    @TableField(exist = false)
    private String purchaseNumber;
    /** 状态（1.未冲红 2.已冲红） */
    private Integer status;
    /** 供应商id */
    @Excel(name = "供应商id")
    private Long supplierId;
    /** 仓库id */
    @Excel(name = "仓库id")
    private Long warehouseId;
    /** 供应商 */
    @Excel(name = "供应商")
    private String supplierName;

    /** 批量导入--入库的批次号（仅通过系统导入时的才会有值） */
    private String importBatchNo;

    /** 仓库 */
    @Excel(name = "仓库")
    private String warehouseName;
    /** 成本单价 */
    @Excel(name = "成本单价")
    private BigDecimal costPrice;
    /**
     * 售价
     */
    @TableField(exist = false)
    private BigDecimal price;
    /** 单位*/
    private String unit;

    /*操作库存数量*/
    @TableField(exist = false)
    private Integer inventoryQuantity;

    @TableField(exist = false)
    private Long categoryId;
    @TableField(exist = false)
    private Long brandId;
    @TableField(exist = false)
    private String productNameCode;
    @TableField(exist = false)
    private Long businessId;
    /** 商品类别 */
    @Excel(name = "商品类别")
    private String productCategory;
    /** 品牌 */
    @Excel(name = "品牌")
    private String brandName;
    /** 产品名称 */
    private String prodName;
    //采购订单商品id
    @TableField(exist = false)
    private Long purchaseProductOrderId;

    @TableField(exist = false)
    private Integer remainingDays;

    /** 是否加供应商字段 */
    @TableField(exist = false)
    private Integer ifAddSupplier;

    @TableField(exist = false)
    private BigDecimal subtotal;
    @TableField(exist = false)
    private Long memberId;
    /*客户信息*/
    private String memberName;
    @TableField(exist = false)
    private List<Long> warehouseIdList;

    //贸易中间商，就是供应商模块（"单位性质"字段值为"企业客户"的）数据。
    /** 贸易中间商ID */
    @TableField(exist = false)
    private Long entSupplierId;
    /** 贸易中间商 合同ID */
    @TableField(exist = false)
    private Long entContractId;

    @TableField(exist = false)
    private Long salesPurchaseId;

    /** 采购编号 */
    @Excel(name = "采购编号")
    private String purchaseNo;

    /** 关联采购单号 */
    @Excel(name = "关联采购单号")
    private String relevancePurchaseNo;
    /** 采购附件 */
    private String purchaseUrl;
    /*采购名称*/
    private String productUrlName;

    /** 球镜 */
    @TableField(exist = false)
    private String sphericalMirror;
    /** 柱镜 */
    @TableField(exist = false)
    private String columnarMirror;
    /** 尺寸 */
    @TableField(exist = false)
    private String size;
    /** 颜色 */
    @TableField(exist = false)
    private String colour;
    /** 匹配商品参数 */
    @TableField(exist = false)
    private String matchParam;
    @TableField(exist = false)
    private String paramResult;
    /**
     * 部门id
     */
    @TableField(exist = false)
    private String deptName;
    /**
     * 备注
     */
    @TableField(exist = false)
    private String taprRemark;

    @TableField(exist = false)
    private String otherPurchaseNumber;

    @TableField(exist = false)
    private String mh;
    @TableField(exist = false)
    private String xmh;
    @TableField(exist = false)
    private String fh;
    @TableField(exist = false)
    private String xfh;
    @TableField(exist = false)
    private String wh;
}
