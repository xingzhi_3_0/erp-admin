package com.xz.purchase.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

import java.util.Date;
import java.util.List;

/**
 * 采购对象 t_purchase
 * 
 * @author xz
 * @date 2024-01-06
 */
@Data
@TableName("t_purchase")
public class Purchase extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 供应商id */
    @Excel(name = "供应商id")
    private Long supplierId;
    /**
     * 入库单号
     */
    private String purchaseNumber;

    /** 供应商 */
    @Excel(name = "供应商")
    private String supplierName;

    /** 仓库id */
    @Excel(name = "仓库id")
    private Long warehouseId;

    /** 仓库 */
    @Excel(name = "仓库")
    private String warehouseName;

    /** 状态（1.待入库 2.已入库 3.已冲红 4.部分冲红 5.待审核 6.审核未过） */
    @Excel(name = "状态", readConverterExp = "1=.待入库,2=.已入库,3=.已冲红,4=.部分冲红,5=.待审核,6=.审核未过")
    private Integer status;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;
    /**
     * 采购商品明细
     */
    @TableField(exist = false)
    private List<PurchaseProduct> purchaseProductList;
    /**
     * 状态
     */
    @TableField(exist = false)
    public List<Integer> statusList;
    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;

    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
    /** 入库类型（1.自制入库 2.采购入库 8.库存调整 4.调拨入库 13.退货入库  14换货退货入库） */
    @Excel(name = "入库类型", readConverterExp = "1=.自制入库,2=.采购入库,8=.库存调整,4=.调拨入库,13=.退货入库")
    private Integer storageType;

    @TableField(exist = false)
    /*分类*/
    private String productCategory;
    /*商品名称*/
    @TableField(exist = false)
    private String productName;

    /** 批量导入--入库的批次号（仅通过系统导入时的才会有值） */
    private String importBatchNo;
}
