package com.xz.purchase.domain;

import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 采购商品属性对象 t_purchase_product_param
 * 
 * @author xz
 * @date 2024-01-06
 */
@Data
@TableName("t_purchase_product_param")
public class PurchaseProductParam{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 采购商品id */
    @Excel(name = "采购商品id")
    private Long purchaseProductId;

    /** 属性名称 */
    @Excel(name = "属性名称")
    private String attributeName;

    /** 属性参数 */
    @Excel(name = "属性参数")
    private String attributeParameter;
    /** 采购id */
    @Excel(name = "采购id")
    private Long purchaseId;

}
