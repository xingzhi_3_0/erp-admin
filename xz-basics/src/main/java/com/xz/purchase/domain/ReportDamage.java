package com.xz.purchase.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 库存报损对象 t_report_damage
 * 
 * @author xz
 * @date 2024-01-16
 */
@Data
@TableName("t_report_damage")
public class ReportDamage extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 报损单号 */
    @Excel(name = "报损单号")
    private String reportDamageNo;

    /** 供应商id */
    @Excel(name = "供应商id")
    private Long supplierId;

    /** 供应商 */
    @Excel(name = "供应商")
    private String supplierName;

    /** 仓库id */
    @Excel(name = "仓库id")
    private Long warehouseId;

    /** 仓库 */
    @Excel(name = "仓库")
    private String warehouseName;

    /** 状态（1.待提交 2.已完成 3.待审核 4.审核未通过 5.已作废） */
    @NotNull(message = "状态不能为空")
    @Excel(name = "状态", readConverterExp = "1=.待提交,2=.已完成,3=.待审核,4=.审核未通过,5=.已作废")
    private Integer status;

    /** 报损数量 */
    @Excel(name = "报损数量")
    private Integer reportDamageNum;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    /** 报损原因（1.加工报损 2.常规报损） */
    @NotNull(message = "报损原因不能为空")
    @Excel(name = "报损原因", readConverterExp = "1=.加工报损,2=.常规报损")
    private Integer reportDamageReason;

    /** 加工单号 */
    @Excel(name = "加工单号")
    private String processNumber;

    /** 个人承担比例 */
    @NotNull(message = "个人承担比例不能为空")
    @Excel(name = "个人承担比例")
    private BigDecimal personalCommitmentRatio;

    /** 个人承担金额 */
    @Excel(name = "个人承担金额")
    private BigDecimal personalCommitment;

    /** 公司承担金额 */
    @Excel(name = "公司承担金额")
    private BigDecimal companyCommitment;

    /** 报损金额 */
    @Excel(name = "报损金额")
    private BigDecimal reportDamageAmount;

    /** 承担人类型（1.个人 2.部门） */
    @NotNull(message = "承担人类型不能为空")
    @Excel(name = "承担人类型", readConverterExp = "1=.个人,2=.部门")
    private Integer commitmentType;

    /** 承担人id */
    @NotBlank(message = "承担人id不能为空")
    @Excel(name = "承担人id")
    private String commitmentId;

    /** 承担人 */
    @NotBlank(message = "承担人不能为空")
    @Excel(name = "承担人")
    private String commitmentName;
    /**
     * 报损商品信息
     */

    @TableField(exist = false)
    @NotNull(message = "报损商品信息不能为空")
    private List<ReportDamageProduct> damageProductList;
    /**
     * 状态
     */
    @TableField(exist = false)
    public List<Integer> statusList;
    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;

    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

}
