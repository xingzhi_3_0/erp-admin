package com.xz.purchase.domain;

import java.math.BigDecimal;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 进货价对象 t_purchase_price
 * 
 * @author xz
 * @date 2024-01-08
 */
@Data
@TableName("t_purchase_price")
public class PurchasePrice extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 供应商id */
    @Excel(name = "供应商id")
    private Long supplierId;

    /** 供应商 */
    @Excel(name = "供应商")
    private String supplierName;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;

    /** 商品id */
    @Excel(name = "商品id")
    private Long productId;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private String productEncoded;

    /** 采购单价 */
    @NotNull(message = "采购单价不能为空")
    @Excel(name = "采购单价")
    private BigDecimal purchasePrice;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    public PurchasePrice(){}
    public PurchasePrice(Long supplierId,Long productId,Long tenantId){
        this.supplierId=supplierId;
        this.productId=productId;
        this.tenantId=tenantId;
    }

}
