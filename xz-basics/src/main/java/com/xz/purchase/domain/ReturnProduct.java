package com.xz.purchase.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 采购退货明细对象 t_return_product
 * 
 * @author xz
 * @date 2024-1-9 18:10:02
 */
@Data
@TableName("t_return_product")
public class ReturnProduct {
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 退货单id */
    private Long returnId;


    /** 退货数量 */
    private Integer returnNum;
    /** 仓库id */
    private Long warehouseId;
    /** 仓库名称 */
    private String warehouseName;
    /** 生产批号 */
    private String batchNumber;
    /** 生产日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date batchDate;
    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date validity;
    /** 商品参数 */
    private String productParam;
    /** 单位 */
    private String unit;
    /** 商品名称 */
    private String productName;

    private String remark;

}
