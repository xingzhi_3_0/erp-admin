package com.xz.purchase.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

import java.util.Date;
import java.util.List;

/**
 * 库存调拨对象 t_allot_info
 * 
 * @author xz
 * @date 2024-01-10
 */
@Data
@TableName("t_allot_info")
public class AllotInfo extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 调拨单号 */
    @Excel(name = "调拨单号")
    private String allotNo;

    /** 供应商id */
    @Excel(name = "供应商id")
    private Long supplierId;

    /** 供应商 */
    @Excel(name = "供应商")
    private String supplierName;

    /** 调出仓库id */
    @Excel(name = "调出仓库id")
    private Long outWarehouseId;

    /** 调出仓库 */
    @Excel(name = "调出仓库")
    private String outWarehouseName;

    /** 状态（1.待提交 2.已完成 3.待审核 4.审核未通过 5.已作废） */
    @Excel(name = "状态", readConverterExp = "1=.待提交,2=.已完成,3=.待审核,4=.审核未通过,5=.已作废")
    private Integer status;

    /** 调拨数量 */
    @Excel(name = "调拨数量")
    private Integer allotNum;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    /** 调入仓库id */
    @Excel(name = "调入仓库id")
    private Long toWarehouseId;

    /** 调入仓库 */
    @Excel(name = "调入仓库")
    private String toWarehouseName;
    /**
     * 调拨商品
     */
    @TableField(exist = false)
    private List<AllotProduct> allotProductList;

    /**
     * 状态
     */
    @TableField(exist = false)
    public List<Integer> statusList;
    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;

    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
    /*权限标识*/
    @TableField(exist = false)
    private String dataScope;

}
