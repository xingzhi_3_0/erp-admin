package com.xz.purchase.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 采购库存调整对象 t_adjust_info
 * 
 * @author xz
 * @date 2024-1-16 14:10:46
 */
@Data
@TableName("t_adjust_info")
public class AdjustInfo extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /**
     * 调整单号
     */
    @Excel(name = "调整单号")
    private String adjustNo;

    /** 调整类型：1-入库 2-出库 */
    private Integer adjustType;

    /** 调整原因：1-报溢 3-其他入库 2-领用 4-其他出库 */
    private Integer adjustReason;

    /** 状态（1.待提交 2.已完成 3.已冲红 4.已作废 ） */
    @Excel(name = "状态", readConverterExp = "1=.待提交,2=.已完成,3.已冲红,4=.已作废")
    private Integer status;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    /**
     * 状态
     */
    @TableField(exist = false)
    public List<Integer> statusList;
    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;

    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
    /** 制单人 */
    @TableField(exist = false)
    private String userName;
}
