package com.xz.purchase.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 采购库存调整明细对象 t_adjust_product
 * 
 * @author xz
 * @date 2024-1-16 14:21:14
 */
@Data
@TableName("t_adjust_product")
public class AdjustProduct {
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 库存调整id */
    private Long adjustId;

    /** 退货数量 */
    private Integer adjustNum;

    /** 仓库id */
    private Long warehouseId;
    /** 仓库名称 */
    private String warehouseName;

    /** 供应商id */
    private Long supplierId;
    /** 供应商名称 */
    private String supplierName;

    /** 生产批号 */
    private String batchNumber;
    /** 生产日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date batchDate;
    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date validity;
    /** 商品参数 */
    private String productParam;
    /** 单位 */
    private String unit;
    /** 商品名称 */
    private String productName;

    private Long purchaseId;

    private Long productId;

    private String remark;

}
