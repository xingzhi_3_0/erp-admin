package com.xz.purchase.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 调拨商品明细对象 t_allot_product
 * 
 * @author xz
 * @date 2024-01-10
 */
@Data
@TableName("t_allot_product")
public class AllotProduct{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 调拨id */
    @Excel(name = "调拨id")
    private Long allotId;

    /** 调拨数量 */
    @Excel(name = "调拨数量")
    private Integer allotNum;
    /**
     * 备注
     */
    private String remark;
    /** 仓库id */
    private Long warehouseId;
    /** 仓库名称 */
    private String warehouseName;
    /** 生产批号 */
    private String batchNumber;
    /** 生产日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date batchDate;
    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date validity;
    /** 商品参数 */
    private String productParam;
    /** 单位 */
    private String unit;
    /** 商品名称 */
    private String productName;
    /** 可用库存 */
    private Integer availableStock;

}
