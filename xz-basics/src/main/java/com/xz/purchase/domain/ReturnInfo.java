package com.xz.purchase.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 采购对象 t_purchase
 * 
 * @author xz
 * @date 2024-1-9 18:11:18
 */
@Data
@TableName("t_return_info")
public class ReturnInfo extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /**
     * 退库单号
     */
    @Excel(name = "退库单号")
    private String returnNo;

    /** 供应商id */
    @Excel(name = "供应商id")
    private Long supplierId;

    /** 供应商 */
    @Excel(name = "供应商")
    private String supplierName;

    /** 退货仓库ids */
    private String warehouseIds;

    /** 退货仓库s */
    @Excel(name = "退货仓库s")
    private String warehouseNames;

    /** 状态（1.待提交 2.已完成 3.已冲红 4.已作废 ） */
    @Excel(name = "状态", readConverterExp = "1=.待提交,2=.已完成,3=.已冲红,4=.已作废")
    private Integer status;

    /** 退货数量 */
    @Excel(name = "退货数量")
    private Integer returnNum;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    /**
     * 状态
     */
    @TableField(exist = false)
    public List<Integer> statusList;
    /**
     * 排序（1.降序 2.升序）
     */
    @TableField(exist = false)
    private Integer sort;

    /** 开始时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /** 结束时间 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
    @TableField(exist = false)
    private Long warehouseId;
    @TableField(exist = false)
    private String userName;

}
