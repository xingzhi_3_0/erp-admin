package com.xz.purchase.param;

import com.xz.purchase.domain.PurchaseProduct;
import com.xz.purchase.domain.ReturnInfo;
import lombok.Data;

import java.util.List;

/**
 * 采购库存调整参数接口
 *
 * @author xz
 * @date 2024-1-10 10:28:52
 */
@Data
public class AdjustProductParam {

    private Integer adjustNum;

    private Long adjustId;

    private Long purchaseProductId;
}
