package com.xz.purchase.param;

import com.xz.purchase.domain.AdjustInfo;
import com.xz.purchase.domain.AdjustProduct;
import com.xz.purchase.domain.WarningSettings;
import lombok.Data;

import java.util.List;

/**
 * 预警设置参数接口
 *
 * @author xz
 * @date 2024-1-10 10:28:52
 */
@Data
public class WarningSettingParam {

    private List<WarningSettings> warningSettingsList;
}
