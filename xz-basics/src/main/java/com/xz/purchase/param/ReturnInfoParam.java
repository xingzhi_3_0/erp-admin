package com.xz.purchase.param;

import com.xz.purchase.domain.PurchaseProduct;
import com.xz.purchase.domain.ReturnInfo;
import com.xz.purchase.domain.ReturnProduct;
import lombok.Data;

import java.util.List;

/**
 * 采购退货参数接口
 *
 * @author xz
 * @date 2024-1-10 10:28:52
 */
@Data
public class ReturnInfoParam extends ReturnInfo {

    List<ReturnProduct> productList;
}
