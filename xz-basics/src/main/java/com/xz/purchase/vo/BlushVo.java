package com.xz.purchase.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @ClassName BlushVo * @Description TODO
 * @Author Administrator
 * @Date 16:41 2024/1/6
 * @Version 1.0  冲红
 **/
@Data
public class BlushVo {
    /**
     * 采购id
     */
    @NotNull(message = "采购id不能为空")
    private Long id;
    /**
     * 冲红商品id
     */

    private List<Long> idList;

    /**
     * 冲红类型（1.部分冲红 2.全部冲红）
     */
    @NotNull(message = "冲红类型不能为空")
    private Integer type;
}
