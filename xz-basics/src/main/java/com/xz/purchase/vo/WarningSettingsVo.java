package com.xz.purchase.vo;

import com.xz.common.annotation.Excel;
import com.xz.purchase.domain.WarningSettings;
import lombok.Data;

@Data
public class WarningSettingsVo extends WarningSettings {

    /** 调拨在途 */
    @Excel(name = "调拨在途")
    private Integer dbztNum;

    /** 采购在途 */
    @Excel(name = "采购在途")
    private Integer cgztNum;

    /**
     * 总库存
     */
    @Excel(name = "总库存")
    private Integer availableStockTotal;
    /**
     * 预占库存
     */
    @Excel(name = "预占库存")
    private Integer preoccupiedInventory;

    /**
     * 调拨在途库存
     */
    @Excel(name = "调拨在途库存")
    private Integer transferStock;
    /**
     * 采购在途库存
     */
    @Excel(name = "采购在途库存")
    private Integer purchaseInventory;

}
