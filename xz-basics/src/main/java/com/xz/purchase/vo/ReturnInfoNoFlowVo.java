package com.xz.purchase.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

@Data
public class ReturnInfoNoFlowVo {

    @Excel(name = "id")
    private Long id;

    @Excel(name = "商品名称")
    private String productName;

    @Excel(name = "参数")
    private String productParam;

    @Excel(name = "仓库")
    private String warehouseName;

    @Excel(name = "生成批号")
    private String batchNumber;


    @Excel(name = "生产厂商")
    private String manufacturer;

    @Excel(name = "差异")
    private Integer num;
}
