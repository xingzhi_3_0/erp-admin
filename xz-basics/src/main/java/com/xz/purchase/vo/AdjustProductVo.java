package com.xz.purchase.vo;

import com.xz.purchase.domain.PurchaseProduct;
import lombok.Data;

@Data
public class AdjustProductVo extends PurchaseProduct {

    /** 库存调整单id */
    private Long adjustId;

    /** 调整数量 */
    private Integer adjustNum;

    /** 调整采购商品id */
    private Long purchaseProductId;

    /** 调整数量 */
    private String remark;

}
