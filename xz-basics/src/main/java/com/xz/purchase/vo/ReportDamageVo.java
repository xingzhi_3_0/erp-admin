package com.xz.purchase.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @ClassName ReportDamageVo * @Description TODO
 * @Author Administrator
 * @Date 14:01 2024/1/16
 * @Version 1.0 报损列表
 **/
@Data
public class ReportDamageVo {
    /** id */
    private Long id;
    /** 报损单号 */
    @Excel(name = "报损单号")
    private String reportDamageNo;
    /** 状态（1.待提交 2.已完成 3.待审核 4.审核未通过 5.已作废） */
    @NotNull(message = "状态不能为空")
    @Excel(name = "状态", readConverterExp = "1=.待提交,2=.已完成,3=.待审核,4=.审核未通过,5=.已作废")
    private Integer status;
    /** 报损数量 */
    @NotNull(message = "报损数量不能为空")
    @Excel(name = "报损数量")
    private Integer reportDamageNum;
    /** 报损原因（1.加工报损 2.常规报损） */
    @NotNull(message = "报损原因不能为空")
    @Excel(name = "报损原因", readConverterExp = "1=.加工报损,2=.常规报损")
    private Integer reportDamageReason;

    /** 加工单号 */
    @Excel(name = "加工单号")
    private String processNumber;
    /** 制单人 */
    @Excel(name = "制单人")
    private String userName;
    /** 备注 */
    private String remark;
    /** 制单人id */
    private Long createBy;
    /** 制单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    //审核按钮（1.待审核 2.无操作 3.重新发起）
    @TableField(exist = false)
    private Integer auditBut;
    //审核节点名称
    @TableField(exist = false)
    //审核类型 1.待审核 2.已完成 3.已驳回 4.重新发起
    private String auditName;
    @TableField(exist = false)
    private Integer auditType;
    private Long deptId;
}
