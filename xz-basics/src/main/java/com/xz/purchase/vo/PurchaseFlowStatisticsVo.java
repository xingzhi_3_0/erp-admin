package com.xz.purchase.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName PurchaseFlowStatisticsVo * @Description TODO
 * @Author Administrator
 * @Date 13:25 2024/3/30
 * @Version 1.0 进销存统计--里露水
 **/
@Data
public class PurchaseFlowStatisticsVo {
    /*分类*/
    private String productCategory;
    /*商品名称*/
    private String productName;
    /** 仓库 */
    private String warehouseName;
    /**
     * 采购商品参数
     */
    private String productParam;
    /** 类型（1.入库 2.出库） */
    @Excel(name = "类型", readConverterExp = "1=.入库,2=.出库")
    private Integer flowType;
    /** 数量 */
    @Excel(name = "数量")
    private Integer quantity;

    /** 成本金额 */
    @Excel(name = "成本金额")
    private BigDecimal costAmount;
}
