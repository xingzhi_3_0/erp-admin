package com.xz.purchase.vo;

import com.xz.purchase.domain.AdjustInfo;
import com.xz.purchase.domain.ReturnInfo;
import lombok.Data;

import java.util.List;

@Data
public class AdjustInfoVo extends AdjustInfo {

    /**
     * 库存调整出库商品列表
     */
    private List<AdjustProductVo> adjustProductVoList;
}
