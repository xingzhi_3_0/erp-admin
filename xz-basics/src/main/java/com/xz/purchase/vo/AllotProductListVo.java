package com.xz.purchase.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName AllotProductListVo * @Description TODO
 * @Author Administrator
 * @Date 11:05 2024/1/12
 * @Version 1.0
 **/
@Data
public class AllotProductListVo {
    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;
    /** 商品id */
    @Excel(name = "商品id")
    private Long productId;
    /*实时库存*/
    private Integer availableStock;
    /** 生成批号 */
    @Excel(name = "生成批号")
    private String batchNumber;

    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "有效期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date validity;
    //备注
    private String productRemark;
    /** 调拨数量 */
    @Excel(name = "调拨数量")
    private Integer allotNum;
    /** 单位*/
    private String unit;
    /**
     * 采购商品参数
     */
    private String productParam;
}
