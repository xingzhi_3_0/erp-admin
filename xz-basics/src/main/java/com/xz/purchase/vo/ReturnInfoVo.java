package com.xz.purchase.vo;

import com.xz.purchase.domain.ReturnInfo;
import lombok.Data;

import java.util.List;

@Data
public class ReturnInfoVo extends ReturnInfo {

    /**
     * 退货商品列表
     */
    private List<ReturnProductVo> returnProductVoList;
}
