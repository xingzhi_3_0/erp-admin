package com.xz.purchase.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

@Data
public class ReturnInfoRepeatFlowVo {

    @Excel(name = "business_id")
    private Long businessId;

    @Excel(name = "purchase_product_id")
    private Long purchaseProductId;

    @Excel(name = "quantity")
    private Integer quantity;
}
