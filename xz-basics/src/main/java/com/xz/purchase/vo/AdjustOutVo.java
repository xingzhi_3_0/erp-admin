package com.xz.purchase.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

@Data
public class AdjustOutVo {

    @Excel(name = "调整类型")
    private Integer adjustType;

    @Excel(name = "调整原因")
    private Integer adjustReason;

    @Excel(name = "调整数量")
    private Integer adjustNum;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date batchDate;

    @Excel(name = "生产批号")
    private String batchNumber;
    @Excel(name = "商品名称")
    private String productName;
    @Excel(name = "参数")
    private String productParam;
    @Excel(name = "球镜柱镜")
    private String matchParam;
    @Excel(name = "单位")
    private String unit;

    @Excel(name = "仓库id")
    private Long warehouseId;

    @Excel(name = "仓库")
    private String warehouseName;
    @Excel(name = "出库单备注")
    private String remark;
    @Excel(name = "出库商品备注")
    private String productRemark;
    @Excel(name = "问题原因")
    private String errorMsg;
    @Excel(name = "行数")
    private Integer hangshu;


    private Long productId;
}
