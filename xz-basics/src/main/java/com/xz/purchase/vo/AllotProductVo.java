package com.xz.purchase.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.List;

/**
 * @ClassName AllotProductVo * @Description TODO
 * @Author Administrator
 * @Date 11:02 2024/1/12
 * @Version 1.0  调拨详情
 **/
@Data
public class AllotProductVo  extends AllotInfoVo{
    /*备注*/
    private String allotRemark;
    /** 调入仓库id */
    @Excel(name = "调入仓库id")
    private Long toWarehouseId;
    /** 调出仓库id */
    @Excel(name = "调出仓库id")
    private Long outWarehouseId;
    //调拨商品
    private List<AllotProductListVo> productVoList;
}
