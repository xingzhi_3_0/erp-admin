package com.xz.purchase.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName ChoosePurchaseProductVo * @Description TODO
 * @Author Administrator
 * @Date 19:43 2024/1/15
 * @Version 1.0
 **/
@Data
public class ChoosePurchaseProductVo {
    /** 商品名称 */
    private String productName;
    /*实时库存*/
    private Integer availableStock;

    /** 生成批号 */
    private String batchNumber;

    /** 生成日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date batchDate;

    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date validity;


    private String remark;
    /**
     * 采购商品参数
     */
    private String productParam;
    /** 仓库id */
    private Long warehouseId;
    /** 仓库 */
    private String warehouseName;
    /** 供应商id */
    private Long supplierId;
    /** 供应商名称 */
    private String supplierName;
    /** 单价 */
    private BigDecimal price;
    /** 单位*/
    private String unit;
    /** 商品id */
    private Long productId;
    private Long categoryId;
    private Integer isMake;
    /** 贸易中间商合同商品价格 **/
    private BigDecimal supllierContractPrice;
    /** 临时id,在某些下拉框选择中使用 **/
    private String tempId;

  public BigDecimal getSupllierContractPrice() {
    return null == supllierContractPrice ?  getPrice() :  supllierContractPrice;
  }
}
