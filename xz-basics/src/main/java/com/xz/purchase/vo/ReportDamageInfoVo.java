package com.xz.purchase.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * @ClassName ReportDamageInfoVo * @Description TODO
 * @Author Administrator
 * @Date 14:08 2024/1/16
 * @Version 1.0  报损详情
 **/
@Data
public class ReportDamageInfoVo extends ReportDamageVo{
    /** 个人承担比例 */
    @NotNull(message = "个人承担比例不能为空")
    @Excel(name = "个人承担比例")
    private BigDecimal personalCommitmentRatio;

    /** 个人承担金额 */
    @Excel(name = "个人承担金额")
    private BigDecimal personalCommitment;

    /** 公司承担金额 */
    @Excel(name = "公司承担金额")
    private BigDecimal companyCommitment;

    /** 报损金额 */
    @Excel(name = "报损金额")
    private BigDecimal reportDamageAmount;

    /** 承担人类型（1.个人 2.部门） */
    @NotNull(message = "承担人类型不能为空")
    @Excel(name = "承担人类型", readConverterExp = "1=.个人,2=.部门")
    private Integer commitmentType;

    /** 承担人id */
    @NotBlank(message = "承担人id不能为空")
    @Excel(name = "承担人id")
    private String commitmentId;

    /** 承担人 */
    @NotBlank(message = "承担人不能为空")
    @Excel(name = "承担人")
    private String commitmentName;

    /**
     * 商品明细
     */
    private List<ReportDamageProductVo> reportDamageProductVoList;
}
