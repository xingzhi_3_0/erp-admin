package com.xz.purchase.vo;

import com.xz.purchase.domain.PurchaseProduct;
import lombok.Data;
@Data
public class ReturnProductVo extends PurchaseProduct {


    /** 退货单id */
    private Long returnId;

    /** 退货数量 */
    private Integer returnNum;

    /** 退货采购商品id */
    private Long purchaseProductId;


}
