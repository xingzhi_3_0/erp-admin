package com.xz.purchase.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName BillDimensionStatisticsVo * @Description TODO
 * @Author Administrator
 * @Date 11:39 2024/1/19
 * @Version 1.0 库存统计--财务纬度
 **/
@Data
public class BillDimensionStatisticsVo extends CommodityDimensionStatisticsVo{
    /**
     * 财务合计
     */
    @Excel(name = "金额合计")
    private BigDecimal amountTotal;
    /**
     * 成本单价
     */
    private BigDecimal costPrice;
}
