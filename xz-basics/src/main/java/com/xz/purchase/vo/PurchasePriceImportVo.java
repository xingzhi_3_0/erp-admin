package com.xz.purchase.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName PurchasePriceImportVo * @Description TODO
 * @Author Administrator
 * @Date 10:22 2024/1/9
 * @Version 1.0
 **/
@Data
public class PurchasePriceImportVo {
    /** 供应商 */
    @Excel(name = "*供应商")
    private String supplierName;

    /** 商品编号 */
    @Excel(name = "*商品编号")
    private String productEncoded;

    /** 采购单价 */
    @Excel(name = "*采购单价")
    private BigDecimal purchasePrice;
}
