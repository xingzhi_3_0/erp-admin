package com.xz.purchase.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

/**
 * @ClassName SalesStockVo * @Description TODO
 * @Author Administrator
 * @Date 17:11 2024/3/6
 * @Version 1.0  销售在途
 **/
@Data
public class SalesStockVo {
    /**
     * 商品名称
     */
    private String productName;
    /**
     * 参数
     */
    private String productParam;
    /**
     * 销售在途库存
     */
    private Integer salesStock;
    /** 仓库id */
    private Long warehouseId;
    /** 生成批号 */
    private String batchNumber;
}
