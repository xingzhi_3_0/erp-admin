package com.xz.purchase.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

/**
 * @ClassName TransferStockVo * @Description TODO
 * @Author Administrator
 * @Date 20:06 2024/1/18
 * @Version 1.0 调拨在途
 **/
@Data
public class TransferStockVo {
    /**
     * 调拨在途库存
     */
    private Integer transferStock;
    /**
     * 类别
     */
    private String productCategory;
    /**
     * 品牌
     */
    private String brandName;
    /**
     * 商品名称
     */
    private String productName;
    /**
     * 参数
     */
    private String productParam;
    /**
     * 生产厂商
     */
    private String manufacturer;
    /**
     * 库存单位
     */
    private String unit;
    /** 生成批号 */
    @Excel(name = "生成批号")
    private String batchNumber;
    /** 仓库 */
    @Excel(name = "仓库")
    private String warehouseName;
    /** 仓库id */
    private Long warehouseId;
}
