package com.xz.purchase.vo;

import com.xz.purchase.domain.PurchaseProduct;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 选择商品
 */
@Data
public class ProductVo {

    private String productName;

    private String productParam;

    private Long categoryId;
    private Long warehouseId;
    /** 是否加工  字典biz_yes_no*/
    private Integer isMake;

    List<ChoosePurchaseProductVo> purchaseProductList;
}
