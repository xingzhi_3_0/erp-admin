package com.xz.purchase.vo;

import com.xz.common.annotation.Excel;
import com.xz.purchase.domain.AdjustInfo;
import lombok.Data;

import java.util.List;

@Data
public class AdjustInfoNoFlowVo {

    @Excel(name = "id")
    private Long id;
}
