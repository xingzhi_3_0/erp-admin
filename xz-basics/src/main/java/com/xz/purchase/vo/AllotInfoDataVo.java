package com.xz.purchase.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName AllotInfoDataVo * @Description TODO
 * @Author Administrator
 * @Date 10:43 2024/3/29
 * @Version 1.0 调拨导出
 **/
@Data
public class AllotInfoDataVo {
    /** 调拨单号 */
    @Excel(name = "调拨单号")
    private String allotNo;
    /** 调出仓库 */
    @Excel(name = "调出仓库")
    private String outWarehouseName;

    /** 调入仓库 */
    @Excel(name = "调入仓库")
    private String toWarehouseName;
    /** 制单时间 */
    @Excel(name = "制单时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 调拨数量 */
    @Excel(name = "调拨数量")
    private Integer allotNum;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;
    /** 商品参数 */
    @Excel(name = "商品参数")
    private String productParam;
}
