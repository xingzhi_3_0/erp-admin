package com.xz.purchase.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName AllotInfoVo * @Description TODO
 * @Author Administrator
 * @Date 9:28 2024/1/12
 * @Version 1.0  调拨列表
 **/
@Data
public class AllotInfoVo {
    /** id */
    private Long id;

    /** 调拨单号 */
    @Excel(name = "调拨单号")
    private String allotNo;
    /** 调出仓库 */
    @Excel(name = "调出仓库")
    private String outWarehouseName;

    /** 状态（1.待提交 2.已完成 3.待审核 4.审核未通过 5.已作废） */
    @Excel(name = "状态", readConverterExp = "1=.待提交,2=.已完成,3=.待审核,4=.审核未通过,5=.已作废")
    private Integer status;
    /** 调入仓库 */
    @Excel(name = "调入仓库")
    private String toWarehouseName;
    /*调入仓库部门id*/
    private Long toDeptId;
    /** 制单人 */
    @Excel(name = "制单人")
    private String userName;
    /** 制单人id */
    private Long createBy;
    /** 制单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 调拨数量 */
    @Excel(name = "调拨数量")
    private Integer allotNum;
    //审核按钮（1.待审核 2.无操作 3.重新发起）
    @TableField(exist = false)
    private Integer auditBut;
    //审核节点名称
    @TableField(exist = false)
    //审核类型 1.待审核 2.已完成 3.已驳回 4.重新发起
    private String auditName;
    @TableField(exist = false)
    private Integer auditType;
    private Long deptId;
}
