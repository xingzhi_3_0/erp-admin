package com.xz.purchase.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName ReportDamageProductVo * @Description TODO
 * @Author Administrator
 * @Date 14:10 2024/1/16
 * @Version 1.0 报损商品明细
 **/
@Data
public class ReportDamageProductVo {
    /** 商品名称 */
    @Excel(name = "商品名称")
    private String productName;
    /** 商品id */
    @Excel(name = "商品id")
    private Long productId;
    /*实时库存*/
    private Integer availableStock;
    /** 生成批号 */
    @Excel(name = "生成批号")
    private String batchNumber;

    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "有效期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date validity;
    //备注
    private String productRemark;
    /** 报损数量 */
    private Integer reportDamageNum;
    /** 单位*/
    private String unit;
    /**
     * 采购商品参数
     */
    private String productParam;

    /** 单价 */
    @Excel(name = "单价")
    private BigDecimal price;

    /** 金额 */
    @Excel(name = "金额")
    private BigDecimal amount;
    /** 生成日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生成日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date batchDate;
    /** 仓库 */
    private String warehouseName;
    private String warehouseId;
}
