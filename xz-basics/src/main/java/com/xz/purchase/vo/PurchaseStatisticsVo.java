package com.xz.purchase.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName PurchaseStatisticsVo * @Description TODO
 * @Author Administrator
 * @Date 14:04 2024/3/29
 * @Version 1.0 进销存统计
 **/

@Data
public class PurchaseStatisticsVo {
    /** 仓库id */
    private Long warehouseId;
    /*分类*/
    @Excel(name = "分类")
    private String productCategory;
    /*商品名称*/
    @Excel(name = "商品名称")
    private String productName;

    /**
     * 采购商品参数
     */
    @Excel(name = "参数")
    private String productParam;

    /** 仓库 */
    @Excel(name = "仓库")
    private String warehouseName;

    /*期初*/

    /*数量*/
    @Excel(name = "期数数量")
    private Integer beginNum;
    /*金额*/
    @Excel(name = "期数金额")
    private BigDecimal beginAmount;
    /*入库*/

    /*数量*/
    @Excel(name = "入库数量")
    private Integer toNum;
    /*金额*/
    @Excel(name = "入库金额")
    private BigDecimal toAmount;
    /*出库*/

    /*数量*/
    @Excel(name = "出库数量")
    private Integer outNum;
    /*金额*/
    @Excel(name = "出库金额")
    private BigDecimal outAmount;
    /*期末*/

    /*数量*/
    @Excel(name = "期末数量")
    private Integer endNum;
    /*金额*/
    @Excel(name = "期末金额")
    private BigDecimal endAmount;
}
