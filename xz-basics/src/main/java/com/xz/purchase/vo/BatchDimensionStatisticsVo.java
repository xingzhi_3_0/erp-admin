package com.xz.purchase.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

/**
 * @ClassName BatchDimensionStatisticsVo * @Description TODO
 * @Author Administrator
 * @Date 11:14 2024/1/19
 * @Version 1.0库存统计--批次纬度
 **/
@Data
public class BatchDimensionStatisticsVo extends CommodityDimensionStatisticsVo{
    /** 生成批号 */
    @Excel(name = "生成批号")
    private String batchNumber;
    /** 仓库 */
    @Excel(name = "仓库")
    private String warehouseName;

}
