package com.xz.purchase.vo;

import com.xz.common.annotation.Excel;
import lombok.Data;

/**
 * @ClassName CommodityDimensionStatisticsVo * @Description TODO
 * @Author Administrator
 * @Date 19:35 2024/1/18
 * @Version 1.0 库存统计--商品纬度
 **/
@Data
public class CommodityDimensionStatisticsVo {
    /**
     * 类别
     */
    @Excel(name = "类别")
    private String productCategory;
    /**
     * 品牌
     */
    @Excel(name = "品牌")
    private String brandName;
    /**
     * 商品名称
     */
    @Excel(name = "商品名称")
    private String productName;
    /**
     * 参数
     */
    @Excel(name = "参数")
    private String productParam;
    /**
     * 生产厂商
     */
    @Excel(name = "生产厂商")
    private String manufacturer;
    /**
     * 库存单位
     */
    @Excel(name = "库存单位")
    private String unit;
    /**
     * 总库存
     */
    @Excel(name = "总库存")
    private Integer availableStockTotal;
    /**
     * 预占库存
     */
    @Excel(name = "预占库存")
    private Integer preoccupiedInventory;
    /**
     * 可用库存
     */
    @Excel(name = "可用库存")
    private Integer availableStock;
    /**
     * 调拨在途库存
     */
    @Excel(name = "调拨在途库存")
    private Integer transferStock;
    /**
     * 采购在途库存
     */
    @Excel(name = "采购在途库存")
    private Integer purchaseInventory;
    /** 仓库id */
    private Long warehouseId;
}
