package com.xz.audit.dto;

import lombok.Data;

/**
 * @ClassName AuditInfoDto * @Description TODO
 * @Author Administrator
 * @Date 13:54 2024/1/12
 * @Version 1.0 流程发起
 **/
@Data
public class AuditInfoDto {
    //业务类型（1.库存调拨 2.库存报损）
    private Integer bizType;
    //发起流程业务编号
    private String bizNo;
    //状态 1.正常 2.重新发起
    private Integer status;
    public AuditInfoDto(){};
    public AuditInfoDto(Integer bizType,String bizNo,Integer status){
        this.bizNo=bizNo;
        this.bizType=bizType;
        this.status=status;
    };
}
