package com.xz.audit.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @ClassName AuditDto * @Description TODO
 * @Author Administrator
 * @Date 16:57 2024/1/12
 * @Version 1.0  审核
 **/
@Data
public class AuditDto {
    /** 审核状态（1.审核通过 2.审核拒绝） */
    @NotNull(message = "审核状态不能为空")
    private Integer auditStatus;
    /**
     * 审核备注
     */
    private String remark;
    /**
     * 业务编号
     */
    @NotBlank(message = "业务编号不能为空")
    private String bizNo;
    /**
     * 业务类型（1.库存调拨 2.库存报损）
     */
    private Integer bizType;
}
