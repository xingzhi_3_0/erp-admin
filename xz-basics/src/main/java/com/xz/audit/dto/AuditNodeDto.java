package com.xz.audit.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xz.common.annotation.Excel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @ClassName AuditNodeDto * @Description TODO
 * @Author Administrator
 * @Date 17:39 2024/1/9
 * @Version 1.0
 *
 **/
@Data
public class AuditNodeDto {
    /** id */
    private Long id;

    /** 审核顺序 */
    @Excel(name = "审核顺序")
    private Integer auditSort;
    /** 流程id */
    @NotBlank(message = "流程id不能为空")
    @Excel(name = "流程id")
    private String processId;

    /** 节点名称 */
    @NotBlank(message = "节点名称不能为空")
    @Excel(name = "节点名称")
    private String nodeName;
    /** 审核方式对应ID */
    @NotNull(message = "审核名称不能为空")
    @Excel(name = "审核方式对应ID")
    private Long auditId;

    /** 审核方式对应名称 */
    @NotBlank(message = "审核名称不能为空")
    @Excel(name = "审核方式对应名称")
    private String auditName;
    private String remark;

    /** 审核方式 审核方式（1.角色 2.审核人 3.部门） */
    @NotNull(message = "审核方式不能为空")
    private Integer auditMethod;
    /*上一节点id*/
    @TableField(exist = false)
    private Long beforeId;
}
