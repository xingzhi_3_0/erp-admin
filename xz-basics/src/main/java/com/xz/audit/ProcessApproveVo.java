package com.xz.audit;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName ProcessApproveVo * @Description TODO
 * @Author Administrator
 * @Date 9:23 2024/1/24
 * @Version 1.0  审核列表
 **/
@Data
public class ProcessApproveVo {

    /** id */
    private Long id;

    /** 业务编号 */
    @Excel(name = "业务编号")
    private String bizNo;
    /** 节点id */
    private Long nodeId;
    /** 业务类型（1.库存调拨 2.库存报损） */
    private Integer bizType;

    /** 审核人id */
    @Excel(name = "审核人id")
    private Long auditId;

    /** 审核人名称 */
    @Excel(name = "审核人名称")
    private String auditName;

    /** 审核时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "审核时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date auditTime;

    /** 审核备注 */
    @Excel(name = "审核备注")
    private String auditNote;

    /** 审核附件 */
    @Excel(name = "审核附件")
    private String auditUrl;

    /** 审核状态（1.待审核 2.审核通过 3.审核拒绝） */
    @Excel(name = "审核状态", readConverterExp = "1=.待审核,2=.审核通过,3=.审核拒绝")
    private Integer auditStatus;

    /** 流程实例id */
    @Excel(name = "流程实例id")
    private Long processInstanceId;
}
