package com.xz.audit.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.audit.ProcessApproveVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.audit.domain.ProcessApprove;

/**
 * 流程审批Mapper接口
 * 
 * @author xz
 * @date 2024-01-15
 */
@Mapper
public interface ProcessApproveMapper  extends BaseMapper<ProcessApprove> {
    /**
     * 查询流程审批
     * 
     * @param id 流程审批主键
     * @return 流程审批
     */
    public ProcessApprove selectProcessApproveById(Long id);

    /**
     * 查询流程审批列表
     * 
     * @param processApprove 流程审批
     * @return 流程审批集合
     */
    public List<ProcessApproveVo> selectProcessApproveList(ProcessApprove processApprove);

    /**
     * 新增流程审批
     * 
     * @param processApprove 流程审批
     * @return 结果
     */
    public int insertProcessApprove(ProcessApprove processApprove);

    /**
     * 修改流程审批
     * 
     * @param processApprove 流程审批
     * @return 结果
     */
    public int updateProcessApprove(ProcessApprove processApprove);

    /**
     * 删除流程审批
     * 
     * @param id 流程审批主键
     * @return 结果
     */
    public int deleteProcessApproveById(Long id);

    /**
     * 批量删除流程审批
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProcessApproveByIds(Long[] ids);
}
