package com.xz.audit.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.audit.domain.ProcessInstance;
import org.apache.ibatis.annotations.Param;

/**
 * 流程实例Mapper接口
 * 
 * @author xz
 * @date 2024-01-15
 */
@Mapper
public interface ProcessInstanceMapper  extends BaseMapper<ProcessInstance> {
    /**
     * 查询流程实例
     * 
     * @param id 流程实例主键
     * @return 流程实例
     */
    public ProcessInstance selectProcessInstanceById(Long id);

    /**
     * 查询流程实例列表
     * 
     * @param processInstance 流程实例
     * @return 流程实例集合
     */
    public List<ProcessInstance> selectProcessInstanceList(ProcessInstance processInstance);

    /**
     * 新增流程实例
     * 
     * @param processInstance 流程实例
     * @return 结果
     */
    public int insertProcessInstance(ProcessInstance processInstance);

    /**
     * 修改流程实例
     * 
     * @param processInstance 流程实例
     * @return 结果
     */
    public int updateProcessInstance(ProcessInstance processInstance);

    /**
     * 删除流程实例
     * 
     * @return 结果
     */
    public int updateProcessInstanceauditStatus(@Param("auditStatus") Integer auditStatus,@Param("bizNo") String bizNo);

    /**
     * 批量删除流程实例
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProcessInstanceByIds(Long[] ids);
}
