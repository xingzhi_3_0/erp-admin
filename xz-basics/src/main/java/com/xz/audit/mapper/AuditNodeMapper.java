package com.xz.audit.mapper;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.audit.dto.AuditNodeDto;
import org.apache.ibatis.annotations.Mapper;
import com.xz.audit.domain.AuditNode;

/**
 * 审核Mapper接口
 * 
 * @author xz
 * @date 2023-09-27
 */
@Mapper
public interface AuditNodeMapper  extends BaseMapper<AuditNode> {
    /**
     * 查询审核
     * 
     * @param id 审核主键
     * @return 审核
     */
    public AuditNode selectAuditNodeById(Long id);

    /**
     * 查询审核列表
     * 
     * @param auditNode 审核
     * @return 审核集合
     */
    public List<AuditNodeDto> selectAuditNodeList(AuditNode auditNode);

    /**
     * 新增审核
     * 
     * @param auditNode 审核
     * @return 结果
     */
    public int insertAuditNode(AuditNode auditNode);

    /**
     * 修改审核
     * 
     * @param auditNode 审核
     * @return 结果
     */
    public int updateAuditNode(AuditNode auditNode);

    /**
     * 删除审核
     * 
     * @param id 审核主键
     * @return 结果
     */
    public int deleteAuditNodeById(Long id);

    /**
     * 批量删除审核
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAuditNodeByIds(Long[] ids);

    /**
     * 节点明细
     * @param auditNode
     * @return
     */
    List<Map<String, Objects>> getSortList(AuditNode auditNode);
}
