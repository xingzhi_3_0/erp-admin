package com.xz.audit.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.audit.domain.AuditProcess;

/**
 * 流程定义Mapper接口
 * 
 * @author xz
 * @date 2024-01-09
 */
@Mapper
public interface AuditProcessMapper  extends BaseMapper<AuditProcess> {
    /**
     * 查询流程定义
     * 
     * @param id 流程定义主键
     * @return 流程定义
     */
    public AuditProcess selectAuditProcessById(Long id);

    /**
     * 查询流程定义列表
     * 
     * @param auditProcess 流程定义
     * @return 流程定义集合
     */
    public List<AuditProcess> selectAuditProcessList(AuditProcess auditProcess);

    /**
     * 新增流程定义
     * 
     * @param auditProcess 流程定义
     * @return 结果
     */
    public int insertAuditProcess(AuditProcess auditProcess);

    /**
     * 修改流程定义
     * 
     * @param auditProcess 流程定义
     * @return 结果
     */
    public int updateAuditProcess(AuditProcess auditProcess);

    /**
     * 删除流程定义
     * 
     * @param id 流程定义主键
     * @return 结果
     */
    public int deleteAuditProcessById(Long id);

    /**
     * 批量删除流程定义
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAuditProcessByIds(Long[] ids);
}
