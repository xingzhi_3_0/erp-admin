package com.xz.audit.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.audit.domain.AuditInfo;

/**
 * 审核流程Mapper接口
 * 
 * @author xz
 * @date 2023-09-27
 */
@Mapper
public interface AuditInfoMapper  extends BaseMapper<AuditInfo> {
    /**
     * 查询审核流程
     * 
     * @param id 审核流程主键
     * @return 审核流程
     */
    public AuditInfo selectAuditInfoById(Long id);

    /**
     * 查询审核流程列表
     * 
     * @param auditInfo 审核流程
     * @return 审核流程集合
     */
    public List<AuditInfo> selectAuditInfoList(AuditInfo auditInfo);

    /**
     * 新增审核流程
     * 
     * @param auditInfo 审核流程
     * @return 结果
     */
    public int insertAuditInfo(AuditInfo auditInfo);

    /**
     * 修改审核流程
     * 
     * @param auditInfo 审核流程
     * @return 结果
     */
    public int updateAuditInfo(AuditInfo auditInfo);

    /**
     * 删除审核流程
     * 
     * @param id 审核流程主键
     * @return 结果
     */
    public int deleteAuditInfoById(Long id);

    /**
     * 批量删除审核流程
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAuditInfoByIds(Long[] ids);
}
