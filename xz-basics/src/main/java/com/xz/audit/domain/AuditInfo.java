package com.xz.audit.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 审核流程对象 t_audit_info
 * 
 * @author xz
 * @date 2023-09-27
 */
@Data
@TableName("t_audit_info")
public class AuditInfo extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 业务编号 */
    @Excel(name = "业务编号")
    private String bizNo;

    /** 当前节点 */
    @Excel(name = "当前节点")
    private Long nodeId;

    /** 审核人id */
    @Excel(name = "审核人id")
    private Long auditId;

    /** 审核人名称 */
    @Excel(name = "审核人名称")
    private String auditName;

    /** 审核时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "审核时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date auditTime;

    /** 审核备注 */
    @Excel(name = "审核备注")
    private String auditNote;

    /** 审核附件 */
    @Excel(name = "审核附件")
    private String auditUrl;

    /** 审核状态（1.待审核 2.已审核） */
    @Excel(name = "审核状态", readConverterExp = "1=.待审核,2=.已审核,3=审核拒绝")
    private Integer auditStatus;

    /** 商户id */
    @Excel(name = "商户id")
    private Long tenantId;

    /** 删除标识 */
    private String delFlag;
    /** 审核节点名称 */
    @Excel(name = "审核节点名称")
    private String nodeName;
    /** 业务类型 */
    @Excel(name = "业务类型")
    private Integer bizType;
    /** 审核方式对应ID */
    @NotNull(message = "审核名称不能为空")
    @Excel(name = "审核方式对应ID")
    private Long auditTypeId;

    /** 审核方式对应名称 */
    @NotBlank(message = "审核名称不能为空")
    @Excel(name = "审核方式对应名称")
    private String auditTypeName;

    /** 审核方式 审核方式（1.角色 2.审核人 3.部门） */
    @NotNull(message = "审核方式不能为空")
    private Integer auditTypeMethod;

    //审核按钮（1.显示 2.隐藏）
    @TableField(exist = false)
    private Integer auditBut;


}
