package com.xz.audit.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 流程实例对象 t_process_instance
 * 
 * @author xz
 * @date 2024-01-15
 */
@Data
@TableName("t_process_instance")
public class ProcessInstance extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 业务编号 */
    @Excel(name = "业务编号")
    private String bizNo;

    /** 当前节点 */
    @Excel(name = "当前节点")
    private Long nodeId;

    /** 流程状态（1.待审核 2.已完成 3.已驳回 4.重新发起） */
    @Excel(name = "流程状态", readConverterExp = "1=.待审核,2=.已完成,3=.已驳回,4=.重新发起")
    private Integer auditStatus;

    /** 商户id */
    @Excel(name = "商户id")
    private Long tenantId;

    /** 删除标识 */
    private String delFlag;

    /** 审核节点名称 */
    @Excel(name = "审核节点名称")
    private String nodeName;

    /** 业务类型（1.库存调拨 2.库存报损） */
    @Excel(name = "业务类型", readConverterExp = "1=.库存调拨,2=.库存报损")
    private Integer bizType;

    /** 审核方式对应ID */
    @Excel(name = "审核方式对应ID")
    private Long auditTypeId;

    /** 审核方式对应名称 */
    @Excel(name = "审核方式对应名称")
    private String auditTypeName;

    /** 审核方式（1.角色 2.审核人 3.部门） */
    @Excel(name = "审核方式", readConverterExp = "1=.角色,2=.审核人,3=.部门")
    private Integer auditTypeMethod;
    //审核按钮（1.显示 2.隐藏）
    @TableField(exist = false)
    private Integer auditBut;
}
