package com.xz.audit.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 审核对象 t_audit_node
 * 
 * @author xz
 * @date 2023-09-27
 */
@Data
@TableName("t_audit_node")
public class AuditNode extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 审核顺序 */
    @Excel(name = "审核顺序")
    private Integer auditSort;
    /** 流程id */
    @NotBlank(message = "流程id不能为空")
    @Excel(name = "流程id")
    private String processId;

    /** 节点名称 */
    @NotBlank(message = "节点名称不能为空")
    @Excel(name = "节点名称")
    private String nodeName;

    /** 商户id */
    @Excel(name = "商户id")
    private Long tenantId;

    /** 删除标识 */
    private String delFlag;

    /** 审核方式对应ID */
    @NotNull(message = "审核名称不能为空")
    @Excel(name = "审核方式对应ID")
    private Long auditId;

    /** 审核方式对应名称 */
    @NotBlank(message = "审核名称不能为空")
    @Excel(name = "审核方式对应名称")
    private String auditName;

    /** 审核方式 审核方式（1.角色 2.审核人 3.部门） */
    @NotNull(message = "审核方式不能为空")
    private Integer auditMethod;
    /*上一节点id*/
    @TableField(exist = false)
    private Long beforeId;

}
