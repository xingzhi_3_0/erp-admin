package com.xz.audit.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.audit.domain.AuditInfo;
import com.xz.audit.domain.ProcessInstance;
import com.xz.audit.dto.AuditDto;
import com.xz.audit.dto.AuditInfoDto;
import com.xz.common.core.domain.AjaxResult;
import com.xz.common.core.domain.entity.SysUser;

/**
 * 流程实例Service接口
 * 
 * @author xz
 * @date 2024-01-15
 */
public interface IProcessInstanceService extends IService<ProcessInstance> {
    /**
     * 查询流程实例
     * 
     * @param id 流程实例主键
     * @return 流程实例
     */
    public ProcessInstance selectProcessInstanceById(Long id);

    /**
     * 查询流程实例列表
     * 
     * @param processInstance 流程实例
     * @return 流程实例集合
     */
    public List<ProcessInstance> selectProcessInstanceList(ProcessInstance processInstance);

    /**
     * 新增流程实例
     * 
     * @param auditInfoDto 流程实例
     * @return 结果
     */
    public Boolean insertProcessInstance(AuditInfoDto auditInfoDto);

    /**
     * 修改流程实例
     * 
     * @param auditDto 流程实例
     * @return 结果
     */
    public int updateProcessInstance(AuditDto auditDto);

    /**
     * 批量删除流程实例
     * 
     * @param ids 需要删除的流程实例主键集合
     * @return 结果
     */
    public int deleteProcessInstanceByIds(Long[] ids);

    /**
     * 删除流程实例信息
     * 
     * @return 结果
     */
    public int updateProcessInstanceauditStatus(Integer auditStatus,String bizNo);
    /**
     * 审核权限
     * @return
     */
    public ProcessInstance iAuditInfoService(Long deptId,Long id, SysUser sysUser, List<ProcessInstance> auditInfoList);
}
