package com.xz.audit.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.xz.audit.ProcessApproveVo;
import com.xz.audit.domain.ProcessInstance;
import com.xz.audit.service.IProcessInstanceService;
import com.xz.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.audit.mapper.ProcessApproveMapper;
import com.xz.audit.domain.ProcessApprove;
import com.xz.audit.service.IProcessApproveService;
import org.springframework.util.CollectionUtils;

/**
 * 流程审批Service业务层处理
 *
 * @author xz
 * @date 2024-01-15
 */
@Service
public class ProcessApproveServiceImpl extends ServiceImpl<ProcessApproveMapper, ProcessApprove> implements IProcessApproveService {
    @Autowired
    private ProcessApproveMapper processApproveMapper;
    @Autowired
    private IProcessInstanceService processInstanceService;

    /**
     * 查询流程审批
     *
     * @param id 流程审批主键
     * @return 流程审批
     */
    @Override
    public ProcessApprove selectProcessApproveById(Long id) {
        return processApproveMapper.selectProcessApproveById(id);
    }

    /**
     * 查询流程审批列表
     *
     * @param processApprove 流程审批
     * @return 流程审批
     */
    @Override
    public List<ProcessApproveVo> selectProcessApproveList(ProcessApprove processApprove) {
        List<ProcessApproveVo> processApproveList = processApproveMapper.selectProcessApproveList(processApprove);
        if(!CollectionUtils.isEmpty(processApproveList)){
            for(ProcessApproveVo processApproveVo:processApproveList){
                if(processApproveVo.getAuditStatus()==1){
                    ProcessInstance processInstance = processInstanceService.getById(processApproveVo.getProcessInstanceId());
                    processApproveVo.setAuditName(processInstance.getAuditTypeName());
                }
            }
        }
        return processApproveList;
    }

    /**
     * 新增流程审批
     *
     * @param processApprove 流程审批
     * @return 结果
     */
    @Override
    public int insertProcessApprove(ProcessApprove processApprove) {
        return processApproveMapper.insertProcessApprove(processApprove);
    }

    /**
     * 修改流程审批
     *
     * @param processApprove 流程审批
     * @return 结果
     */
    @Override
    public int updateProcessApprove(ProcessApprove processApprove) {
        processApprove.setUpdateTime(DateUtils.getNowDate());
        return processApproveMapper.updateProcessApprove(processApprove);
    }

    /**
     * 批量删除流程审批
     *
     * @param ids 需要删除的流程审批主键
     * @return 结果
     */
    @Override
    public int deleteProcessApproveByIds(Long[] ids) {
        return processApproveMapper.deleteProcessApproveByIds(ids);
    }

    /**
     * 删除流程审批信息
     *
     * @param id 流程审批主键
     * @return 结果
     */
    @Override
    public int deleteProcessApproveById(Long id) {
        return processApproveMapper.deleteProcessApproveById(id);
    }
}
