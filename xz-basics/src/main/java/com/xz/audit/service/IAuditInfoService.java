package com.xz.audit.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.audit.domain.AuditInfo;
import com.xz.audit.dto.AuditDto;
import com.xz.audit.dto.AuditInfoDto;
import com.xz.common.core.domain.entity.SysUser;

import java.util.List;
import java.util.Objects;

/**
 * 审核流程Service接口
 * 
 * @author xz
 * @date 2023-09-27
 */
public interface IAuditInfoService extends IService<AuditInfo> {
    /**
     * 查询审核流程
     * 
     * @param id 审核流程主键
     * @return 审核流程
     */
    public AuditInfo selectAuditInfoById(Long id);

    /**
     * 查询审核流程列表
     * 
     * @param auditInfo 审核流程
     * @return 审核流程集合
     */
    public List<AuditInfo> selectAuditInfoList(AuditInfo auditInfo);

    /**
     * 新增审核流程
     * 
     * @param auditInfoDto 审核流程
     * @return 结果
     */
    public int insertAuditInfo(AuditInfoDto auditInfoDto);



    /**
     * 修改审核流程
     * 
     * @param auditDto 审核流程
     * @return 结果
     */
    public int updateAuditInfo(AuditDto auditDto);

    /**
     * 批量删除审核流程
     * 
     * @param ids 需要删除的审核流程主键集合
     * @return 结果
     */
    public int deleteAuditInfoByIds(Long[] ids);

    /**
     * 删除审核流程信息
     * 
     * @param id 审核流程主键
     * @return 结果
     */
    public int deleteAuditInfoById(Long id);

    /**
     * 审核权限
     * @return
     */
    public AuditInfo iAuditInfoService(Long id, SysUser sysUser,List<AuditInfo> auditInfoList);
}
