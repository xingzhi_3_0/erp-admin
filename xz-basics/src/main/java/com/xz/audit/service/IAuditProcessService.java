package com.xz.audit.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.audit.domain.AuditProcess;

/**
 * 流程定义Service接口
 * 
 * @author xz
 * @date 2024-01-09
 */
public interface IAuditProcessService extends IService<AuditProcess> {
    /**
     * 查询流程定义
     * 
     * @param id 流程定义主键
     * @return 流程定义
     */
    public AuditProcess selectAuditProcessById(Long id);

    /**
     * 查询流程定义列表
     * 
     * @param auditProcess 流程定义
     * @return 流程定义集合
     */
    public List<AuditProcess> selectAuditProcessList(AuditProcess auditProcess);

    /**
     * 新增流程定义
     * 
     * @param auditProcess 流程定义
     * @return 结果
     */
    public int insertAuditProcess(AuditProcess auditProcess);

    /**
     * 修改流程定义
     * 
     * @param auditProcess 流程定义
     * @return 结果
     */
    public int updateAuditProcess(AuditProcess auditProcess);

    /**
     * 批量删除流程定义
     * 
     * @param ids 需要删除的流程定义主键集合
     * @return 结果
     */
    public int deleteAuditProcessByIds(Long[] ids);

    /**
     * 删除流程定义信息
     * 
     * @param id 流程定义主键
     * @return 结果
     */
    public int deleteAuditProcessById(Long id);
    /**
     * 校验流程是否配置流程
     */
    boolean verifyProcess(Integer bizType);

}
