package com.xz.audit.service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.audit.domain.AuditNode;
import com.xz.audit.dto.AuditNodeDto;
import com.xz.common.core.domain.AjaxResult;

/**
 * 审核Service接口
 * 
 * @author xz
 * @date 2023-09-27
 */
public interface IAuditNodeService extends IService<AuditNode> {
    /**
     * 查询审核
     * 
     * @param id 审核主键
     * @return 审核
     */
    public AuditNode selectAuditNodeById(Long id);

    /**
     * 查询审核列表
     * 
     * @param auditNode 审核
     * @return 审核集合
     */
    public List<AuditNodeDto> selectAuditNodeList(AuditNode auditNode);

    /**
     * 新增审核
     * 
     * @param auditNode 审核
     * @return 结果
     */
    public AjaxResult insertAuditNode(AuditNode auditNode);

    /**
     * 修改审核
     * 
     * @param auditNode 审核
     * @return 结果
     */
    public AjaxResult updateAuditNode(AuditNode auditNode);

    /**
     * 批量删除审核
     * 
     * @param ids 需要删除的审核主键集合
     * @return 结果
     */
    public AjaxResult deleteAuditNodeByIds(Long[] ids);

    /**
     * 删除审核信息
     * 
     * @param id 审核主键
     * @return 结果
     */
    public AjaxResult deleteAuditNodeById(Long id);
    /**
     * 节点明细
     * @param auditNode
     * @return
     */
    List<Map<String, Objects>> getSortList(AuditNode auditNode);
}
