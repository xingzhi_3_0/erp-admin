package com.xz.audit.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.audit.ProcessApproveVo;
import com.xz.audit.domain.ProcessApprove;

/**
 * 流程审批Service接口
 * 
 * @author xz
 * @date 2024-01-15
 */
public interface IProcessApproveService extends IService<ProcessApprove> {
    /**
     * 查询流程审批
     * 
     * @param id 流程审批主键
     * @return 流程审批
     */
    public ProcessApprove selectProcessApproveById(Long id);

    /**
     * 查询流程审批列表
     * 
     * @param processApprove 流程审批
     * @return 流程审批集合
     */
    public List<ProcessApproveVo> selectProcessApproveList(ProcessApprove processApprove);

    /**
     * 新增流程审批
     * 
     * @param processApprove 流程审批
     * @return 结果
     */
    public int insertProcessApprove(ProcessApprove processApprove);

    /**
     * 修改流程审批
     * 
     * @param processApprove 流程审批
     * @return 结果
     */
    public int updateProcessApprove(ProcessApprove processApprove);

    /**
     * 批量删除流程审批
     * 
     * @param ids 需要删除的流程审批主键集合
     * @return 结果
     */
    public int deleteProcessApproveByIds(Long[] ids);

    /**
     * 删除流程审批信息
     * 
     * @param id 流程审批主键
     * @return 结果
     */
    public int deleteProcessApproveById(Long id);
}
