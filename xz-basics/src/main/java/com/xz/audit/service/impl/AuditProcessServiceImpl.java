package com.xz.audit.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xz.audit.domain.AuditNode;
import com.xz.audit.service.IAuditNodeService;
import com.xz.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.audit.mapper.AuditProcessMapper;
import com.xz.audit.domain.AuditProcess;
import com.xz.audit.service.IAuditProcessService;
import org.springframework.util.CollectionUtils;

/**
 * 流程定义Service业务层处理
 *
 * @author xz
 * @date 2024-01-09
 */
@Service
public class AuditProcessServiceImpl extends ServiceImpl<AuditProcessMapper, AuditProcess> implements IAuditProcessService {
    @Autowired
    private AuditProcessMapper auditProcessMapper;
    @Autowired
    private IAuditNodeService auditNodeService;
    /**
     * 查询流程定义
     *
     * @param id 流程定义主键
     * @return 流程定义
     */
    @Override
    public AuditProcess selectAuditProcessById(Long id) {
        return auditProcessMapper.selectAuditProcessById(id);
    }

    /**
     * 查询流程定义列表
     *
     * @param auditProcess 流程定义
     * @return 流程定义
     */
    @Override
    public List<AuditProcess> selectAuditProcessList(AuditProcess auditProcess) {
        return auditProcessMapper.selectAuditProcessList(auditProcess);
    }

    /**
     * 新增流程定义
     *
     * @param auditProcess 流程定义
     * @return 结果
     */
    @Override
    public int insertAuditProcess(AuditProcess auditProcess) {
        auditProcess.setCreateTime(DateUtils.getNowDate());
        return auditProcessMapper.insertAuditProcess(auditProcess);
    }

    /**
     * 修改流程定义
     *
     * @param auditProcess 流程定义
     * @return 结果
     */
    @Override
    public int updateAuditProcess(AuditProcess auditProcess) {
        auditProcess.setUpdateTime(DateUtils.getNowDate());
        return auditProcessMapper.updateAuditProcess(auditProcess);
    }

    /**
     * 批量删除流程定义
     *
     * @param ids 需要删除的流程定义主键
     * @return 结果
     */
    @Override
    public int deleteAuditProcessByIds(Long[] ids) {
        return auditProcessMapper.deleteAuditProcessByIds(ids);
    }

    /**
     * 删除流程定义信息
     *
     * @param id 流程定义主键
     * @return 结果
     */
    @Override
    public int deleteAuditProcessById(Long id) {
        return auditProcessMapper.deleteAuditProcessById(id);
    }
    /**
     * 校验流程是否配置流程
     * @return
     */
    @Override
    public boolean verifyProcess(Integer bizType) {
        //校验是否已经配置审核流程
        List<AuditProcess> auditProcessList = baseMapper.selectList(new QueryWrapper<AuditProcess>().eq("biz_type", bizType).eq("status", 0));
        if(CollectionUtils.isEmpty(auditProcessList)){
            return false;
        }
        //校验流程是否有有配置节点
        AuditProcess auditProcess = auditProcessList.get(0);
        List<AuditNode> auditNodeList = auditNodeService.list(new QueryWrapper<AuditNode>().eq("process_id",auditProcess.getId()));
        if(CollectionUtils.isEmpty(auditNodeList)){
            return false;
        }


        return true;
    }
}
