package com.xz.diagnosisOrder.enums;

/**
 * 到店状态
 */
public enum ArrivalStatusEnum {

  NOT_ARRIVED(0, "未到店"),
  ARRIVED(1, "已到店"),
  CANCEL(2, "取消预约"),
  ;

  private final Integer status;

  private final String name;

  ArrivalStatusEnum(Integer status, String name) {
    this.status = status;
    this.name = name;
  }

  public Integer getStatus() {
    return status;
  }

  public String getName() {
    return name;
  }

  public boolean eq(Integer status) {
    if(null != status && status.equals(getStatus())){
      return true;
    }
    return false;
  }
}
