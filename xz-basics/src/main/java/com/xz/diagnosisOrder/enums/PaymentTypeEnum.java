package com.xz.diagnosisOrder.enums;

/**
 * 支付方式
 */
public enum PaymentTypeEnum {

  DEFAULT_VALUE(0, ""),
  CASH(1, "现金"),
  BANK_CARD(2, "银行卡"),
  E_PAYMENT(3, "E支付"),
  ;

  private final Integer type;

  private final String name;

  PaymentTypeEnum(Integer type, String name) {
    this.type = type;
    this.name = name;
  }

  public Integer getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public boolean eq(Integer type) {
    if(null != type && type.equals(getType())){
      return true;
    }
    return false;
  }
}
