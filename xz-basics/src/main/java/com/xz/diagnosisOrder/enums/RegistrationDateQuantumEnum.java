package com.xz.diagnosisOrder.enums;

/**
 * 挂号时间段
 */
public enum RegistrationDateQuantumEnum {

  MORNING(1, "上午"),
  AFTERNOON(2, "下午"),
  ;

  private final Integer type;

  private final String name;

  RegistrationDateQuantumEnum(Integer type, String name) {
    this.type = type;
    this.name = name;
  }

  public Integer getType() {
    return type;
  }

  public String getName() {
    return name;
  }
}
