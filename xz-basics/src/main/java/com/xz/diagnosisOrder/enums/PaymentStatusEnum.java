package com.xz.diagnosisOrder.enums;

/**
 * 支付方式
 */
public enum PaymentStatusEnum {

  UNPAID(0, "未付款"),
  PAID(1, "已付款"),
  PENDING_REFUND(2, "待退款"),
  REFUNDED(3, "已退款"),
  CANCELED(4, "已取消"),
  ;

  private final Integer status;

  private final String name;

  PaymentStatusEnum(Integer status, String name) {
    this.status = status;
    this.name = name;
  }

  public Integer getStatus() {
    return status;
  }

  public String getName() {
    return name;
  }

  public boolean eq(Integer status) {
    if(null != status && status.equals(getStatus())){
      return true;
    }
    return false;
  }
}
