package com.xz.diagnosisOrder.enums;

/**
 * 支付方式
 */
public enum TreatmentOrderTypeEnum {

  PACKAGES(1, "套餐"),
  ITEMS(2, "项目"),
  ;

  private final Integer type;

  private final String name;

  TreatmentOrderTypeEnum(Integer status, String name) {
    this.type = status;
    this.name = name;
  }

  public Integer getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public boolean eq(Integer type) {
    if(null != type && type.equals(getType())){
      return true;
    }
    return false;
  }
}
