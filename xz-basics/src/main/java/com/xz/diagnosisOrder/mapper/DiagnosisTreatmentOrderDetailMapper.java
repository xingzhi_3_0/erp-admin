package com.xz.diagnosisOrder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrderDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 就诊单详情Mapper接口
 *
 * @author xz
 * @date 2024-03-25
 */
@Mapper
public interface DiagnosisTreatmentOrderDetailMapper  extends BaseMapper<DiagnosisTreatmentOrderDetail> {
    /**
     * 查询就诊单详情
     *
     * @param id 就诊单详情主键
     * @return 就诊单详情
     */
    public DiagnosisTreatmentOrderDetail selectDiagnosisTreatmentOrderDetailById(Long id);

    /**
     * 查询就诊单详情列表
     *
     * @param diagnosisTreatmentOrderDetail 就诊单详情
     * @return 就诊单详情集合
     */
    public List<DiagnosisTreatmentOrderDetail> selectDiagnosisTreatmentOrderDetailList(DiagnosisTreatmentOrderDetail diagnosisTreatmentOrderDetail);

    /**
     * 新增就诊单详情
     *
     * @param diagnosisTreatmentOrderDetail 就诊单详情
     * @return 结果
     */
    public int insertDiagnosisTreatmentOrderDetail(DiagnosisTreatmentOrderDetail diagnosisTreatmentOrderDetail);

    /**
     * 修改就诊单详情
     *
     * @param diagnosisTreatmentOrderDetail 就诊单详情
     * @return 结果
     */
    public int updateDiagnosisTreatmentOrderDetail(DiagnosisTreatmentOrderDetail diagnosisTreatmentOrderDetail);

    /**
     * 删除就诊单详情
     *
     * @param id 就诊单详情主键
     * @return 结果
     */
    public int deleteDiagnosisTreatmentOrderDetailById(Long id);

    /**
     * 批量删除就诊单详情
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDiagnosisTreatmentOrderDetailByIds(Long[] ids);

  /**
   * 查询就诊单详情
   *
   * @param orderNo 就诊单订单编号
   * @return 就诊单详情
   */
  public List<DiagnosisTreatmentOrderDetail> selectByOrderNo(@Param("orderNo")String orderNo);
}
