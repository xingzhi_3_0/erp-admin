package com.xz.diagnosisOrder.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.diagnosisOrder.domain.DiagnosisOrderLog;

/**
 * 就诊单Mapper接口
 *
 * @author xz
 * @date 2024-03-27
 */
@Mapper
public interface DiagnosisOrderLogMapper  extends BaseMapper<DiagnosisOrderLog> {
    /**
     * 查询就诊单
     *
     * @param id 就诊单主键
     * @return 就诊单
     */
    public DiagnosisOrderLog selectDiagnosisOrderLogById(Long id);

    /**
     * 查询就诊单列表
     *
     * @param diagnosisOrderLog 就诊单
     * @return 就诊单集合
     */
    public List<DiagnosisOrderLog> selectDiagnosisOrderLogList(DiagnosisOrderLog diagnosisOrderLog);

    /**
     * 新增就诊单
     *
     * @param diagnosisOrderLog 就诊单
     * @return 结果
     */
    public int insertDiagnosisOrderLog(DiagnosisOrderLog diagnosisOrderLog);

    /**
     * 修改就诊单
     *
     * @param diagnosisOrderLog 就诊单
     * @return 结果
     */
    public int updateDiagnosisOrderLog(DiagnosisOrderLog diagnosisOrderLog);

    /**
     * 删除就诊单
     *
     * @param id 就诊单主键
     * @return 结果
     */
    public int deleteDiagnosisOrderLogById(Long id);

    /**
     * 批量删除就诊单
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDiagnosisOrderLogByIds(Long[] ids);
}
