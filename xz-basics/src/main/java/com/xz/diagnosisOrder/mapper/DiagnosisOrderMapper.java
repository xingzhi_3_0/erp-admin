package com.xz.diagnosisOrder.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.diagnosisOrder.vo.DiagnosisOrderListVo;
import com.xz.diagnosisOrder.vo.OrderServiceDeskListVo;
import com.xz.report.param.SalesReportParam;
import com.xz.staging.vo.FoldLineStagingVo;
import com.xz.staging.vo.WorkbenchesVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.diagnosisOrder.domain.DiagnosisOrder;

/**
 * 诊疗（预约/挂号）单Mapper接口
 *
 * @author xz
 * @date 2024-03-17
 */
@Mapper
public interface DiagnosisOrderMapper  extends BaseMapper<DiagnosisOrder> {
    /**
     * 查询诊疗（预约/挂号）单
     *
     * @param id 诊疗（预约/挂号）单主键
     * @return 诊疗（预约/挂号）单
     */
    public DiagnosisOrder selectDiagnosisOrderById(Long id);

    /**
     * 查询诊疗（预约/挂号）单列表
     *
     * @param diagnosisOrder 诊疗（预约/挂号）单
     * @return 诊疗（预约/挂号）单集合
     */
    public List<DiagnosisOrder> selectDiagnosisOrderList(DiagnosisOrder diagnosisOrder);
    /**
     * 查询就诊单列表
     *
     * @param diagnosisOrder 诊疗（预约/挂号）单就诊单
     * @return 就诊单集合
     */
    public List<DiagnosisOrderListVo> selectDiagnosisOrderVoList(DiagnosisOrder diagnosisOrder);
    /**
     * 查询诊疗（预约/挂号）单列表
     *
     * @param diagnosisOrder 诊疗（预约/挂号）单
     * @return 诊疗（预约/挂号）单集合
     */
    public List<OrderServiceDeskListVo> selectOrderServiceDeskList(DiagnosisOrder diagnosisOrder);

    /**
     * 新增诊疗（预约/挂号）单
     *
     * @param diagnosisOrder 诊疗（预约/挂号）单
     * @return 结果
     */
    public int insertDiagnosisOrder(DiagnosisOrder diagnosisOrder);

    /**
     * 修改诊疗（预约/挂号）单
     *
     * @param diagnosisOrder 诊疗（预约/挂号）单
     * @return 结果
     */
    public int updateDiagnosisOrder(DiagnosisOrder diagnosisOrder);

    /**
     * 删除诊疗（预约/挂号）单
     *
     * @param id 诊疗（预约/挂号）单主键
     * @return 结果
     */
    public int deleteDiagnosisOrderById(Long id);

    /**
     * 批量删除诊疗（预约/挂号）单
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDiagnosisOrderByIds(Long[] ids);

    WorkbenchesVo diagnosisWorkbenches(SalesReportParam salesReportParam);

    List<FoldLineStagingVo> foldLineStatistics(SalesReportParam salesReportParam);

    List<FoldLineStagingVo> foldLineMonthList(SalesReportParam salesReportParam);
}
