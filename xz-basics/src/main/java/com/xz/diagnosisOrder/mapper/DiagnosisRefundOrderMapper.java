package com.xz.diagnosisOrder.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.diagnosisOrder.vo.DiagnosisRefundOrderListVo;
import com.xz.diagnosisOrder.vo.DiagnosisRefundOrderVo;
import org.apache.ibatis.annotations.Mapper;
import com.xz.diagnosisOrder.domain.DiagnosisRefundOrder;

/**
 * 挂号/诊疗退费单Mapper接口
 *
 * @author xz
 * @date 2024-03-27
 */
@Mapper
public interface DiagnosisRefundOrderMapper  extends BaseMapper<DiagnosisRefundOrder> {
    /**
     * 查询挂号/诊疗退费单
     *
     * @param id 挂号/诊疗退费单主键
     * @return 挂号/诊疗退费单
     */
    public DiagnosisRefundOrder selectDiagnosisRefundOrderById(Long id);

    /**
     * 查询挂号/诊疗退费单列表
     *
     * @param diagnosisRefundOrder 挂号/诊疗退费单
     * @return 挂号/诊疗退费单集合
     */
    public List<DiagnosisRefundOrder> selectDiagnosisRefundOrderList(DiagnosisRefundOrder diagnosisRefundOrder);

    /**
     * 查询挂号/诊疗退费单列表
     *
     * @param diagnosisRefundOrder 挂号/诊疗退费单
     * @return 挂号/诊疗退费单集合
     */
    public List<DiagnosisRefundOrderListVo> selectDiagnosisRefundOrderVoList(DiagnosisRefundOrder diagnosisRefundOrder);

    /**
     * 新增挂号/诊疗退费单
     *
     * @param diagnosisRefundOrder 挂号/诊疗退费单
     * @return 结果
     */
    public int insertDiagnosisRefundOrder(DiagnosisRefundOrder diagnosisRefundOrder);

    /**
     * 修改挂号/诊疗退费单
     *
     * @param diagnosisRefundOrder 挂号/诊疗退费单
     * @return 结果
     */
    public int updateDiagnosisRefundOrder(DiagnosisRefundOrder diagnosisRefundOrder);

    /**
     * 删除挂号/诊疗退费单
     *
     * @param id 挂号/诊疗退费单主键
     * @return 结果
     */
    public int deleteDiagnosisRefundOrderById(Long id);

    /**
     * 批量删除挂号/诊疗退费单
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDiagnosisRefundOrderByIds(Long[] ids);


}
