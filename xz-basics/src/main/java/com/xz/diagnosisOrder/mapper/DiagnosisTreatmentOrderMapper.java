package com.xz.diagnosisOrder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrder;
import com.xz.diagnosisOrder.vo.DiagnosisTreatmentOrderVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 就诊单Mapper接口
 *
 * @author xz
 * @date 2024-03-25
 */
@Mapper
public interface DiagnosisTreatmentOrderMapper extends BaseMapper<DiagnosisTreatmentOrder> {
  /**
   * 查询就诊单
   *
   * @param id 就诊单主键
   * @return 就诊单
   */
  public DiagnosisTreatmentOrder selectDiagnosisTreatmentOrderById(Long id);
  /**
   * 查询就诊单
   *
   * @param id 就诊单主键
   * @return 就诊单
   */
  public DiagnosisTreatmentOrder selectDiagnosisTreatmentOrderByNo(Long id);
  /**
   * 查询就诊单列表
   *
   * @param diagnosisTreatmentOrder 就诊单
   * @return 就诊单集合
   */
  public List<DiagnosisTreatmentOrder> selectDiagnosisTreatmentOrderList(DiagnosisTreatmentOrder diagnosisTreatmentOrder);

  /**
   * 查询就诊单列表
   *
   * @param diagnosisTreatmentOrder 就诊单
   * @return 就诊单集合
   */
  public List<DiagnosisTreatmentOrderVo> selectDiagnosisTreatmentOrderVoList(DiagnosisTreatmentOrder diagnosisTreatmentOrder);

  /**
   * 新增就诊单
   *
   * @param diagnosisTreatmentOrder 就诊单
   * @return 结果
   */
  public int insertDiagnosisTreatmentOrder(DiagnosisTreatmentOrder diagnosisTreatmentOrder);

  /**
   * 修改就诊单
   *
   * @param diagnosisTreatmentOrder 就诊单
   * @return 结果
   */
  public int updateDiagnosisTreatmentOrder(DiagnosisTreatmentOrder diagnosisTreatmentOrder);

  /**
   * 删除就诊单
   *
   * @param id 就诊单主键
   * @return 结果
   */
  public int deleteDiagnosisTreatmentOrderById(Long id);

  /**
   * 批量删除就诊单
   *
   * @param ids 需要删除的数据主键集合
   * @return 结果
   */
  public int deleteDiagnosisTreatmentOrderByIds(Long[] ids);
}
