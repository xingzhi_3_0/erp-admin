package com.xz.diagnosisOrder.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.diagnosisOrder.domain.DiagnosisRefundOrderDetail;

/**
 * 挂号/诊疗退费单详情Mapper接口
 *
 * @author xz
 * @date 2024-03-27
 */
@Mapper
public interface DiagnosisRefundOrderDetailMapper  extends BaseMapper<DiagnosisRefundOrderDetail> {
    /**
     * 查询挂号/诊疗退费单详情
     *
     * @param id 挂号/诊疗退费单详情主键
     * @return 挂号/诊疗退费单详情
     */
    public DiagnosisRefundOrderDetail selectDiagnosisRefundOrderDetailById(Long id);

    /**
     * 查询挂号/诊疗退费单详情列表
     *
     * @param diagnosisRefundOrderDetail 挂号/诊疗退费单详情
     * @return 挂号/诊疗退费单详情集合
     */
    public List<DiagnosisRefundOrderDetail> selectDiagnosisRefundOrderDetailList(DiagnosisRefundOrderDetail diagnosisRefundOrderDetail);

    /**
     * 新增挂号/诊疗退费单详情
     *
     * @param diagnosisRefundOrderDetail 挂号/诊疗退费单详情
     * @return 结果
     */
    public int insertDiagnosisRefundOrderDetail(DiagnosisRefundOrderDetail diagnosisRefundOrderDetail);

    /**
     * 修改挂号/诊疗退费单详情
     *
     * @param diagnosisRefundOrderDetail 挂号/诊疗退费单详情
     * @return 结果
     */
    public int updateDiagnosisRefundOrderDetail(DiagnosisRefundOrderDetail diagnosisRefundOrderDetail);

    /**
     * 删除挂号/诊疗退费单详情
     *
     * @param id 挂号/诊疗退费单详情主键
     * @return 结果
     */
    public int deleteDiagnosisRefundOrderDetailById(Long id);

    /**
     * 批量删除挂号/诊疗退费单详情
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDiagnosisRefundOrderDetailByIds(Long[] ids);
}
