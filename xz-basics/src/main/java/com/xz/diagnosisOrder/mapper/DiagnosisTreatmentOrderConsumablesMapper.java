package com.xz.diagnosisOrder.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrderConsumables;

/**
 * 就诊单耗品Mapper接口
 *
 * @author xz
 * @date 2024-03-30
 */
@Mapper
public interface DiagnosisTreatmentOrderConsumablesMapper  extends BaseMapper<DiagnosisTreatmentOrderConsumables> {
    /**
     * 查询就诊单耗品
     *
     * @param id 就诊单耗品主键
     * @return 就诊单耗品
     */
    public DiagnosisTreatmentOrderConsumables selectDiagnosisTreatmentOrderConsumablesById(Long id);

    /**
     * 查询就诊单耗品列表
     *
     * @param diagnosisTreatmentOrderConsumables 就诊单耗品
     * @return 就诊单耗品集合
     */
    public List<DiagnosisTreatmentOrderConsumables> selectDiagnosisTreatmentOrderConsumablesList(DiagnosisTreatmentOrderConsumables diagnosisTreatmentOrderConsumables);

    /**
     * 新增就诊单耗品
     *
     * @param diagnosisTreatmentOrderConsumables 就诊单耗品
     * @return 结果
     */
    public int insertDiagnosisTreatmentOrderConsumables(DiagnosisTreatmentOrderConsumables diagnosisTreatmentOrderConsumables);

    /**
     * 修改就诊单耗品
     *
     * @param diagnosisTreatmentOrderConsumables 就诊单耗品
     * @return 结果
     */
    public int updateDiagnosisTreatmentOrderConsumables(DiagnosisTreatmentOrderConsumables diagnosisTreatmentOrderConsumables);

    /**
     * 删除就诊单耗品
     *
     * @param id 就诊单耗品主键
     * @return 结果
     */
    public int deleteDiagnosisTreatmentOrderConsumablesById(Long id);

    /**
     * 批量删除就诊单耗品
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDiagnosisTreatmentOrderConsumablesByIds(Long[] ids);
}
