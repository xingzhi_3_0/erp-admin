package com.xz.diagnosisOrder.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 挂号/诊疗退费单对象 t_diagnosis_refund_order
 *
 * @author xz
 * @date 2024-03-27
 */
@Data
@TableName("t_diagnosis_refund_order")
public class DiagnosisRefundOrder extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 单据编码 */
    @Excel(name = "单据编码")
    private String orderNo;

    /** 单据类型（1.挂号费，2.诊疗费） */
    @Excel(name = "单据类型", readConverterExp = "1=.挂号费，2.诊疗费")
    private Integer orderType;

    /** 患者id */
    @Excel(name = "患者id")
    private Long patientId;

    /** 退款总额 */
    @Excel(name = "退款总额")
    private BigDecimal totalRefundAmount;

    /** 钱包退款总额 */
    @Excel(name = "钱包退款总额")
    private BigDecimal totalWalletPay;

    /** 待退款总额 */
    @Excel(name = "待退款总额")
    private BigDecimal totalPendingAmount;

    /** 状态（1.待提交,2.待退款，3.已退款,4.已取消） */
    @Excel(name = "状态", readConverterExp = "2=.待退款，3.已退款,4.已取消")
    private Integer status;

    /** 创建人名称 */
    @Excel(name = "创建人名称")
    private String createByName;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    @NotNull(message = "租户id不能为空")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    private String deptName;

    /** 创建日期-开始 */
    @TableField(exist = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createStartTime;
    /** 创建日期-截止 */
    @TableField(exist = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createEndTime;
    /** 客户名称/手机号 */
    @TableField(exist = false)
    private String patientInfo;
    /**
     * 状态
     */
    @TableField(exist = false)
    public List<Integer> statusList;
}
