package com.xz.diagnosisOrder.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 诊疗（预约/挂号）单对象 t_diagnosis_order
 *
 * @author xz
 * @date 2024-03-17
 */
@Data
@TableName("t_diagnosis_order")
public class DiagnosisOrder extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 单号 */
    @Excel(name = "单号")
    private String orderNo;

    /** 单据类型（1.预约，2.挂号） */
    @Excel(name = "单据类型", readConverterExp = "1=.预约，2.挂号")
    private Integer orderType;

    /** 患者id */
    @Excel(name = "患者id")
    private Long patientId;

    /** 挂号时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "挂号时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date registrationDate;

    /** 挂号时间段项ID */
    @Excel(name = "挂号时间段项ID")
    private Long diagnosisTimeId;

    /** 挂号时间段项 */
    @Excel(name = "挂号时间段项")
    private String diagnosisTime;

    /** 挂号时间段项分组 */
    @Excel(name = "挂号时间段项分组")
    private String diagnosisTimeGroup;

    /** 医生挂号费id */
    @Excel(name = "医生挂号费id")
    @NotNull(message = "医生挂号费id不能为空")
    private Long registrationFeeId;

    /** 就诊事项ID */
    @Excel(name = "就诊事项ID")
    private Long diagnosisItemId;

    /** 医生名称 */
    @Excel(name = "医生名称")
    private String doctorName;

    /** 就诊事项 */
    @Excel(name = "就诊事项")
    private String diagnosisItem;

    /** 门诊类型 */
    @Excel(name = "门诊类型")
    private String registrationFeeType;

    /** 挂号费 */
    @Excel(name = "挂号费")
    private BigDecimal registrationFee;

    /** 优惠金额 */
    @Excel(name = "优惠金额")
    private BigDecimal preferentialAmount;

    /** 1-现金 2-银行卡 3-E支付  */
    @Excel(name = "0-默认，1-现金 2-银行卡 3-E支付 ")
    private Integer paymentType;

    /** 收款金额 */
    @Excel(name = "收款金额")
    private BigDecimal collectionAmount;

    /** 钱包支付金额 */
    @Excel(name = "钱包支付金额")
    private BigDecimal walletPay;

    /** 待收金额 */
    @Excel(name = "待收金额")
    private BigDecimal pendingAmount;

    /** 减价金额 */
    @Excel(name = "减价金额")
    private BigDecimal reduceAmount;

    /** 打折后金额 */
    @Excel(name = "打折后金额")
    private BigDecimal afterDiscountAmount;

    /** 付款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "付款时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date paymentTime;

    /** 会员卡号 */
    @Excel(name = "会员卡号")
    private String memberCardNo;

    /** 到店状态（0.未到店，1.已到店，2取消预约） */
    @Excel(name = "到店状态", readConverterExp = "0=.未到店，1.已到店，2取消预约")
    private Integer arrivalStatus;

    /** 是否开检查单（0.否，1.是）ps：预留字段，暂未使用 */
    @Excel(name = "是否开检查单", readConverterExp = "0=.否，1.是")
    private Integer checkupStatus;

    /** 缴费状态（0.未付款,1.已付款，2.待退款，3.已退款） */
    @Excel(name = "缴费状态", readConverterExp = "0=.未付款，1=.已付款，2.待退款，3.已退款")
    private Integer paymentStatus;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    @NotNull(message = "租户id不能为空")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    /** 就诊日期-开始 */
    @TableField(exist = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date registrationStartDate;
    /** 就诊日期-截止 */
    @TableField(exist = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date registrationEndDate;
    /** 客户名称/手机号 */
    @TableField(exist = false)
    private String patientInfo;
    /** 状态list */
    @TableField(exist = false)
    private List<Integer> paymentStatusList;
    /** 状态list */
    @TableField(exist = false)
    private List<Integer> arrivalStatusList;
    /** 创建人名称 */
    @TableField(exist = false)
    private String createByName;
    /** 部门名称 */
    @TableField(exist = false)
    private String deptName;
}
