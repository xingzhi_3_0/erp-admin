package com.xz.diagnosisOrder.domain;

import java.math.BigDecimal;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 就诊单详情对象 t_diagnosis_treatment_order_detail
 *
 * @author xz
 * @date 2024-03-25
 */
@Data
@TableName("t_diagnosis_treatment_order_detail")
public class DiagnosisTreatmentOrderDetail extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 就诊单id */
    @Excel(name = "就诊单id")
    @NotNull(message = "就诊单id不能为空")
    private Long treatmentOrderId;

    /** 单据类型（1.套餐，2.项目） */
    @Excel(name = "单据类型", readConverterExp = "1=.套餐，2.项目")
    private Integer orderType;

    /** 套餐ID */
    @Excel(name = "套餐ID")
    @NotNull(message = "套餐ID不能为空")
    private Long packageId;

    /** 套餐名称 */
    @Excel(name = "套餐名称")
    @NotNull(message = "套餐名称不能为空")
    private String packageName;

    /** 套餐单价（元） */
    @Excel(name = "套餐单价", readConverterExp = "元=")
    private BigDecimal packagePrice;

    /** 套餐数量 */
    @Excel(name = "套餐数量")
    private Integer packageQuantity;

    /** 项目ID */
    @Excel(name = "项目ID")
    @NotNull(message = "项目ID不能为空")
    private Long itemId;

    /** 项目名称 */
    @Excel(name = "项目名称")
    @NotNull(message = "项目名称不能为空")
    private String itemName;

    /** 项目单位 */
    @Excel(name = "项目单位")
    private String itemUnit;

    /** 项目单价（元） */
    @Excel(name = "项目单价", readConverterExp = "元=")
    private BigDecimal itemPrice;

    /** 项目数量 */
    @Excel(name = "项目数量")
    private Integer itemQuantity;

    /** 总数(套餐数量*项目数量) */
    @Excel(name = "总数(套餐数量*项目数量)")
    private Integer totalQuantity;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    @NotNull(message = "租户id不能为空")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

}
