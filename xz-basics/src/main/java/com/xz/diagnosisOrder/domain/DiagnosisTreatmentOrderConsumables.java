package com.xz.diagnosisOrder.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.NotNull;

import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 就诊单耗品对象 t_diagnosis_treatment_order_consumables
 *
 * @author xz
 * @date 2024-03-30
 */
@Data
@TableName("t_diagnosis_treatment_order_consumables")
public class DiagnosisTreatmentOrderConsumables extends BaseEntity {
  private static final long serialVersionUID = 1L;

  /**
   * id
   */
  private Long id;

  /**
   * 就诊单id
   */
  @Excel(name = "就诊单id")
  @NotNull(message = "就诊单id不能为空")
  private Long treatmentOrderId;

  /**
   * 商品id
   */
  @Excel(name = "商品id")
  private Long productId;

  /**
   * 商品名称
   */
  @Excel(name = "商品名称")
  private String productName;

  /**
   * 商品参数
   */
  @Excel(name = "商品参数")
  private String productParam;

  /**
   * 商品品牌
   */
  @Excel(name = "商品品牌")
  private String brandName;

  /**
   * 商品类别
   */
  @Excel(name = "商品类别")
  private String productCategory;

  /**
   * 商品编码
   */
  @Excel(name = "商品编码")
  private String productEncoded;

  /**
   * 生产日期
   */
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  @Excel(name = "生产日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
  private Date batchDate;

  /**
   * 有效期
   */
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  @Excel(name = "有效期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
  private Date validity;

  /**
   * 生产批号/sn码
   */
  @Excel(name = "生产批号/sn码")
  private String batchNumber;

  /**
   * 单位
   */
  @Excel(name = "单位")
  private String unit;

  /**
   * 生产厂商
   */
  @Excel(name = "生产厂商")
  private String manufacturer;

  /**
   * 注册证号
   */
  @Excel(name = "注册证号")
  private String registrationNo;

  /**
   * 仓库id
   */
  @Excel(name = "仓库id")
  private Long warehouseId;

  /**
   * 仓库名称
   */
  @Excel(name = "仓库名称")
  private String warehouseName;

  /**
   * 供应商id
   */
  @Excel(name = "供应商id")
  private Long supplierId;

  /**
   * 供应商
   */
  @Excel(name = "供应商")
  private String supplierName;

  /**
   * 零售价
   */
  @Excel(name = "零售价")
  private BigDecimal productPrice;

  /**
   * 商品数量
   */
  @Excel(name = "商品数量")
  private Integer productNum;

  /**
   * 删除标志（0代表存在 2代表删除）
   */
  private Long delFlag;

  /**
   * 租户id
   */
  @Excel(name = "租户id")
  @NotNull(message = "租户id不能为空")
  private Long tenantId;

  /**
   * 部门id
   */
  @Excel(name = "部门id")
  private Long deptId;

}
