package com.xz.diagnosisOrder.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 就诊单对象 t_diagnosis_treatment_order
 *
 * @author xz
 * @date 2024-03-25
 */
@Data
@TableName("t_diagnosis_treatment_order")
public class DiagnosisTreatmentOrder extends BaseEntity{
  private static final long serialVersionUID = 1L;

  /** id */
  private Long id;

  /** 关联的挂号单id */
  @Excel(name = "关联的挂号单id")
  @NotNull(message = "关联的挂号单id不能为空")
  private Long diagnosisOrderId;

  /** 单据编码 */
  @Excel(name = "单据编码")
  private String orderNo;

  /** 单据类型（0.订单，1.套餐，2.项目） */
  @Excel(name = "单据类型", readConverterExp = "0.主订单，1.套餐，2.项目")
  private Integer orderType;

  /** 关联套餐/项目id */
  @Excel(name = "关联套餐/项目id")
  private Long associationId;

  /** 套餐/项目标题 */
  @Excel(name = "套餐/项目标题")
  private String orderTitle;

  /** 患者id */
  @Excel(name = "患者id")
  private Long patientId;

  /** 就诊日期 */
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  @Excel(name = "就诊日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
  private Date registrationDate;

  /** 医生挂号费id */
  @Excel(name = "医生挂号费id")
  private Long registrationFeeId;

  /** 医生名称 */
  @Excel(name = "医生名称")
  private String doctorName;

  /** 套餐/项目单价 */
  @Excel(name = "套餐/项目单价")
  private BigDecimal price;

  /** 套餐/项目数量 */
  @Excel(name = "套餐/项目数量")
  private Integer quantity;

  /** 套餐/项目总价 */
  @Excel(name = "套餐/项目总价")
  private BigDecimal totalPrice;

  /** 优惠金额 */
  @Excel(name = "优惠金额")
  private BigDecimal preferentialAmount;

  /** 1-现金 2-银行卡 3-E支付  */
  @Excel(name = "1-现金 2-银行卡 3-E支付 ")
  private Integer paymentType;

  /** 收款金额 */
  @Excel(name = "收款金额")
  private BigDecimal collectionAmount;

  /** 钱包支付金额 */
  @Excel(name = "钱包支付金额")
  private BigDecimal walletPay;

  /** 待收金额 */
  @Excel(name = "待收金额")
  private BigDecimal pendingAmount;

  /** 减价金额 */
  @Excel(name = "减价金额")
  private BigDecimal reduceAmount;

  /** 打折后金额 */
  @Excel(name = "打折后金额")
  private BigDecimal afterDiscountAmount;

  /** 缴费状态（0.未付款,1.已付款，2.待退款，3.已退款） */
  @Excel(name = "缴费状态", readConverterExp = "0=.未付款,1.已付款，2.待退款，3.已退款")
  private Integer paymentStatus;

  /** 付款时间 */
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  @Excel(name = "付款时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
  private Date paymentTime;

  /** 会员卡号 */
  @Excel(name = "会员卡号")
  private String memberCardNo;

  /** 删除标志（0代表存在 2代表删除） */
  private Long delFlag;

  /** 租户id */
  @Excel(name = "租户id")
  @NotNull(message = "租户id不能为空")
  private Long tenantId;

  /** 部门id */
  @Excel(name = "部门id")
  private Long deptId;

  /** 金额 */
  @TableField(exist = false)
  private BigDecimal totalWalletPay;
  /** 金额 */
  @TableField(exist = false)
  private BigDecimal subtracteAmount;

  /** 详情数据 */
  @TableField(exist = false)
  private List<DiagnosisTreatmentOrderDetail> details;

  /** 就诊日期-开始 */
  @TableField(exist = false)
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private Date createStartTime;
  /** 就诊日期-截止 */
  @TableField(exist = false)
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private Date createEndTime;
  /** 客户名称/手机号 */
  @TableField(exist = false)
  private String patientInfo;
  /** 状态list */
  @TableField(exist = false)
  private List<Integer> statusList;
  /** 项目名称 */
  @TableField(exist = false)
  private String itemNames;
  @TableField(exist = false)
  private BigDecimal totalItemPrice;
}
