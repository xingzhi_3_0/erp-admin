package com.xz.diagnosisOrder.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 就诊单对象 t_diagnosis_order_log
 *
 * @author xz
 * @date 2024-03-27
 */
@Data
@TableName("t_diagnosis_order_log")
public class DiagnosisOrderLog extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 业务单id */
    @Excel(name = "业务单id")
    @NotNull(message = "业务单id不能为空")
    private Long bizId;

    /** 业务日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "业务日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date bizDate;

    /** 业务编码 */
    @Excel(name = "业务编码")
    private String bizNo;

    /** 业务类型（1.挂号单，2.挂号退费，3.检查单，4.检查退费） */
    @Excel(name = "业务类型", readConverterExp = "1=.挂号单，2.挂号退费，3.检查单，4.检查退费")
    private Integer bizType;

    /** 患者id */
    @Excel(name = "患者id")
    private Long patientId;

    /** 费用类型（1.挂号费，2.诊疗费） */
    @Excel(name = "费用类型", readConverterExp = "1=.挂号费，2.诊疗费")
    private Integer costCategory;

    /** 套餐名称 */
    @Excel(name = "套餐名称")
    private String packageName;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String itemNames;

    /** 数量 */
    @Excel(name = "数量")
    private Integer quantity;

    /** 售价 */
    @Excel(name = "售价")
    private BigDecimal price;

    /** 总价 */
    @Excel(name = "总价")
    private BigDecimal totalPrice;

    /** 优惠金额 */
    @Excel(name = "优惠金额")
    private BigDecimal preferentialAmount;

    /** 收款金额 */
    @Excel(name = "收款金额")
    private BigDecimal collectionAmount;

    /** 钱包支付金额 */
    @Excel(name = "钱包支付金额")
    private BigDecimal walletPay;

    /** 现金支付金额 */
    @Excel(name = "现金支付金额")
    private BigDecimal cashPaymentAmount;

    /** 银行卡金额 */
    @Excel(name = "银行卡金额")
    private BigDecimal bankCardPaymentAmount;

    /** E支付金额 */
    @Excel(name = "E支付金额")
    private BigDecimal ePaymentAmount;

    /** 创建人名称 */
    @Excel(name = "创建人名称")
    private String createByName;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    @NotNull(message = "租户id不能为空")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    private String deptName;

}
