package com.xz.diagnosisOrder.domain;

import java.math.BigDecimal;
import com.xz.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.xz.common.core.domain.BaseEntity;

/**
 * 挂号/诊疗退费单详情对象 t_diagnosis_refund_order_detail
 *
 * @author xz
 * @date 2024-03-27
 */
@Data
@TableName("t_diagnosis_refund_order_detail")
public class DiagnosisRefundOrderDetail extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 关联的挂号单id */
    @Excel(name = "关联的退费单id")
    @NotNull(message = "关联的退费单id不能为空")
    private Long refundOrderId;

    /** 业务单id */
    @Excel(name = "业务单id")
    @NotNull(message = "业务单id不能为空")
    private Long bizId;

    /** 业务编码 */
    @Excel(name = "业务编码")
    private String bizNo;

    /** 费用类型（1.挂号费，2.诊疗费） */
    @Excel(name = "费用类型", readConverterExp = "1=.挂号费，2.诊疗费")
    private Integer costCategory;

    /** 套餐名称 */
    @Excel(name = "套餐名称")
    private String packageName;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String itemNames;

    /** 数量 */
    @Excel(name = "数量")
    private Integer quantity;

    /** 收款金额 */
    @Excel(name = "收款金额")
    private BigDecimal collectionAmount;

    /** 钱包支付金额 */
    @Excel(name = "钱包支付金额")
    private BigDecimal walletPay;

    /** 待收金额 */
    @Excel(name = "待收金额")
    private BigDecimal pendingAmount;

    /** 创建人名称 */
    @Excel(name = "创建人名称")
    private String createByName;

    /** 删除标志（0代表存在 2代表删除） */
    private Long delFlag;

    /** 租户id */
    @Excel(name = "租户id")
    @NotNull(message = "租户id不能为空")
    private Long tenantId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    private String deptName;

}
