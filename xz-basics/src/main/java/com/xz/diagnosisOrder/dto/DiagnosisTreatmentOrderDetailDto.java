package com.xz.diagnosisOrder.dto;

import com.xz.common.annotation.Excel;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 就诊单对象 t_diagnosis_treatment_order
 *
 * @author xz
 * @date 2024-03-25
 */
@Data
public class DiagnosisTreatmentOrderDetailDto {

    /** 检查项目/套餐 */
    @NotNull(message = "检查项目/套餐id不能为空")
    private Long id;

    /** 检查项目/套餐类型（1.套餐，2.项目） */
    @NotNull(message = "类型不能为空")
    private Integer type;

    /** 检查项目/套餐类型名称 */
    @NotNull(message = "检查项目/套餐id名称不能为空")
    private String name;
    /** 套餐/项目数量 */
    @NotNull(message = "数量不能为空")
    @Min(value = 1, message = "数量必须大于0")
    private Integer quantity;
    /** 金额 */
    @NotNull(message = "单价不能为空")
    @DecimalMin(value = "0",message = "单价必须大于等于{value}")
    private BigDecimal price;
    /** 套餐/项目总价 */
    @DecimalMin(value = "0",message = "金额必须大于等于{value}")
    private BigDecimal totalPrice;
    /** 优惠金额 */
    @NotNull(message = "优惠金额不能为空")
    @DecimalMin(value = "0",message = "优惠金额必须大于等于{value}")
    private BigDecimal preferentialAmount;
    /** 减价金额 */
    @NotNull(message = "减价金额不能为空")
    @DecimalMin(value = "0",message = "减价金额必须大于等于{value}")
    private BigDecimal reduceAmount;
    /** 打折后金额 */
    @NotNull(message = "打折后金额不能为空")
    @DecimalMin(value = "0",message = "打折后金额必须大于等于{value}")
    private BigDecimal afterDiscountAmount;
    /** 小计 */
    @NotNull(message = "小计不能为空")
    @DecimalMin(value = "0",message = "小计必须大于等于{value}")
    private BigDecimal collectionAmount;


}
