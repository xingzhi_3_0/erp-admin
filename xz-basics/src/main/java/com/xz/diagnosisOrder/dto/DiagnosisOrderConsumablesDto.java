package com.xz.diagnosisOrder.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 诊疗（预约/挂号）单对象 t_diagnosis_order
 *
 * @author xz
 * @date 2024-03-17
 */
@Data
public class DiagnosisOrderConsumablesDto {

    /** id */
    private Long id;

    /** 患者id */
    @NotNull(message = "客户不能为空")
    private Long patientId;

    /** 挂号时间 */
    @NotNull(message = "就诊日期不能为空")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date registrationDate;

    /** 挂号时间段 */
    @NotNull(message = "挂号时间段不能为空")
    private Long diagnosisTimeId;

    /** 就诊事项 */
    @NotNull(message = "就诊事项不能为空")
    private Long diagnosisItemId;

    /** 医生挂号费id */
    //@NotNull(message = "医生不能为空")
    private Long registrationFeeId;

    /** 优惠金额 */
    //@DecimalMin(value = "0",message = "优惠金额必须大于等于{value}")
    //private BigDecimal preferentialAmount;

    /** 支付方式：1-现金 2-银行卡 3-E支付  */
    private Integer paymentType;

    /** 收款金额 */
    //@DecimalMin(value = "0",message = "收款金额必须大于等于{value}")
    //private BigDecimal collectionAmount;

    /** 钱包支付金额 */
    @DecimalMin(value = "0",message = "钱包支付金额必须大于等于{value}")
    private BigDecimal walletPay;

      /** 待收金额 */
    //@DecimalMin(value = "0",message = "待收金额必须大于等于{value}")
    //private BigDecimal pendingAmount;

    /** 减价金额 */
    @DecimalMin(value = "0",message = "减免金额必须大于等于{value}")
    private BigDecimal reduceAmount;

    /** 打折后金额 */
    //private BigDecimal afterDiscountAmount;

    /** 会员卡号 */
    private String memberCardNo;

    /** 到店状态（0.未到店，1.已到店，2取消预约） */
    @NotNull(message = "状态不能为空")
    private Integer arrivalStatus;

    /** 备注 */
    @Length(max = 200,message = "备注最多{max}个字符")
    private String remark;
}
