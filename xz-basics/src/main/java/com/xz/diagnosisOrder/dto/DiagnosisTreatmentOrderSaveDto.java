package com.xz.diagnosisOrder.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import com.xz.common.validation.SituationGroup;
import com.xz.patient.domain.PatientInfo;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 就诊单对象 t_diagnosis_treatment_order
 *
 * @author xz
 * @date 2024-03-25
 */
@Data
public class DiagnosisTreatmentOrderSaveDto{

    /** id */
    private Long id;

    /** 关联的挂号单id */
    @NotNull(message = "挂号单id不能为空")
    private Long diagnosisOrderId;

    /** 医生挂号费id */
    //@NotNull(message = "医生不能为空")
    private Long registrationFeeId;

    /** 优惠金额 */
    @DecimalMin(value = "0",message = "优惠金额必须大于等于{value}")
    private BigDecimal preferentialAmount;

    /** 1-现金 2-银行卡 3-E支付  */
    private Integer paymentType;

    /** 收款金额 */
    @DecimalMin(value = "0",message = "收款金额必须大于等于{value}")
    private BigDecimal collectionAmount;

    /** 钱包支付金额 */
    @DecimalMin(value = "0",message = "钱包支付金额必须大于等于{value}")
    private BigDecimal walletPay;

    /** 待收金额 */
    @DecimalMin(value = "0",message = "待收金额必须大于等于{value}")
    private BigDecimal pendingAmount;

    /** 减免金额 */
    private BigDecimal reduceAmount;

    /** 折后金额 */
    private BigDecimal afterDiscountAmount;

    /** 会员卡号 */
    private String memberCardNo;

    /** 备注 */
    private String remark;

    @Valid()
    @NotEmpty(message = "检查项目/套餐不能为空")
    private List<DiagnosisTreatmentOrderDetailDto> details;

    @Valid()
    private List<DiagnosisTreatmentOrderConsumablesDto> consumables;


    // 患者信息，后台根据挂号单自行查出
    private PatientInfo patient;
}
