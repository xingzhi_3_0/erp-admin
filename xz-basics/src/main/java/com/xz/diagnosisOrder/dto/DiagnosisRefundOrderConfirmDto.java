package com.xz.diagnosisOrder.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 退费单确认对象
 *
 * @author xz
 * @date 2024-03-27
 */
@Data
public class DiagnosisRefundOrderConfirmDto {

  /**
   * id
   */
  @NotNull(message = "退费记录id不能为空")
  private Long id;

  /**
   * 附件
   */
  @NotEmpty(message = "请上传退款凭证")
  private List<String> voucherUrlList;

  /**
   * 备注
   */
  @Length(max = 200,message = "备注最多{max}个字符")
  private String voucherRemark;

}
