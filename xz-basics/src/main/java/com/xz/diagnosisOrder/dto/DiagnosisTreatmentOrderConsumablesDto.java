package com.xz.diagnosisOrder.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import com.xz.purchase.domain.PurchaseProduct;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 就诊单耗品对象 t_diagnosis_treatment_order_consumables
 *
 * @author xz
 * @date 2024-03-30
 */
@Data
public class DiagnosisTreatmentOrderConsumablesDto {

  /**
   * 商品id
   */
  @NotNull(message = "药品不能为空")
  private Long productId;
  /**
   * 药品名称
   */
  @NotNull(message = "药品名称不能为空")
  private String productName;
  /**
   * 仓库id
   */
  @NotNull(message = "仓库不能为空")
  private Long warehouseId;
  /**
   * 商品数量
   */
  @NotNull(message = "数量不能为空")
  private Integer productNum;
  /**
   * 产品参数
   */
  @NotNull(message = "药品参数不能为空")
  private String productParam;
  /**
   * 生产批号/sn码
   */
  @NotNull(message = "生产批号不能为空")
  private String batchNumber;
  /**
   * 生产日期
   */
  private Date batchDate;

  /**
   * 有效期
   */
  private Date validity;
  /**
   * 供应商id
   */
  @NotNull(message = "药品供应商不能为空")
  private Long supplierId;
}
