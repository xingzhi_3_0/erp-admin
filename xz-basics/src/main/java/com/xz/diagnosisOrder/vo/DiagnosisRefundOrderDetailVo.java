package com.xz.diagnosisOrder.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 挂号/诊疗退费单详情对象 t_diagnosis_refund_order_detail
 *
 * @author xz
 * @date 2024-03-27
 */
@Data
public class DiagnosisRefundOrderDetailVo{

    /** id */
    private Long id;

    /** 业务单id */
    @Excel(name = "业务单id")
    private Long bizId;

    /** 业务编码 */
    @Excel(name = "业务编码")
    private String bizNo;

    /** 费用类型（1.挂号费，2.诊疗费） */
    @Excel(name = "费用类型", readConverterExp = "1=.挂号费，2.诊疗费")
    private Integer costCategory;

    /** 套餐名称 */
    @Excel(name = "套餐名称")
    private String packageName;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String itemNames;

    /** 数量 */
    @Excel(name = "数量")
    private Integer quantity;

    /** 收款金额 */
    @Excel(name = "收款金额")
    private BigDecimal collectionAmount;

    /** 钱包支付金额 */
    @Excel(name = "钱包支付金额")
    private BigDecimal walletPay;

    /** 待收金额 */
    @Excel(name = "待收金额")
    private BigDecimal pendingAmount;

}
