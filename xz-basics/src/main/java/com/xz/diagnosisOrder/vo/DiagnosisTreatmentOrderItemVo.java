package com.xz.diagnosisOrder.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrder;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrderDetail;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * 就诊单详情对象 t_diagnosis_treatment_order_detail
 *
 * @author xz
 * @date 2024-03-25
 */
@Data
public class DiagnosisTreatmentOrderItemVo{

  /** 检查项目/套餐 */
  private Long id;
  /** 检查项目/套餐类型（1.套餐，2.项目） */
  private Integer type;
  /** 检查项目/套餐类型名称 */
  private String name;
  /** 套餐/项目数量 */
  private Integer quantity;
  /** 金额 */
  private BigDecimal price;
  /** 套餐/项目总价 */
  private BigDecimal totalPrice;
  /** 优惠金额 */
  private BigDecimal preferentialAmount;
  /** 小计 */
  private BigDecimal collectionAmount;
  /** 减价金额 */
  private BigDecimal reduceAmount;
  /** 打折后金额 */
  private BigDecimal afterDiscountAmount;
  /** 实际id */
  private Long rid;

  List<DiagnosisTreatmentOrderItemVo> items;

  public DiagnosisTreatmentOrderItemVo() {
  }

  public DiagnosisTreatmentOrderItemVo(DiagnosisTreatmentOrder order) {
    this.rid = order.getId();
    this.id = order.getAssociationId();
    this.type = order.getOrderType();
    this.name = order.getOrderTitle();
    this.quantity = order.getQuantity();
    this.price = order.getPrice();
    this.totalPrice = order.getTotalPrice();
    this.preferentialAmount = order.getPreferentialAmount();
    this.reduceAmount = order.getReduceAmount();
    this.afterDiscountAmount = order.getAfterDiscountAmount();
    this.collectionAmount = order.getCollectionAmount();
  }

  public DiagnosisTreatmentOrderItemVo(DiagnosisTreatmentOrderDetail detail) {
    this.id = detail.getItemId();
    this.name = detail.getItemName();
    this.quantity = detail.getItemQuantity();
  }
}
