package com.xz.diagnosisOrder.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 诊疗（预约/挂号）单对象 t_diagnosis_order
 *
 * @author xz
 * @date 2024-03-17
 */
@Data
public class OrderServiceRecordListVo {

    /** id */
    private Long id;
    /** 挂号时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "就诊日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date registrationDate;
    /** 客户信息 */
    @Excel(name = "客户信息",  dateFormat = "yyyy-MM-dd")
    private String patientInfo;
    /** 就诊事项 */
    @Excel(name = "就诊事项")
    private String diagnosisItem;
    /** 医生名称 */
    @Excel(name = "医生")
    private String doctorName;
    /** 类型 */
    @Excel(name = "门诊类型")
    private String registrationFeeType;
    /** 挂号时间段 */
    @Excel(name = "时间段")
    private String diagnosisTime;
    /** 挂号费 */
    @Excel(name = "费用")
    private BigDecimal registrationFee;
    /** 挂号费 */
    @Excel(name = "实收")
    private BigDecimal collectionAmount;
    /** 创建者 */
    @Excel(name = "制单人")
    private String createByName;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "制单时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private String createTime;
    /** 制单部门 */
    @Excel(name = "制单部门")
    private String deptName;
    /** 单号 */
    @Excel(name = "单号")
    private String orderNo;
    /** 缴费状态（0.未付款,1.已付款，2.待退款，3.已退款） */
    @Excel(name = "状态", readConverterExp = "0=.未付款，1=.已付款，2.待退款，3.已退款")
    private Integer paymentStatus;
}
