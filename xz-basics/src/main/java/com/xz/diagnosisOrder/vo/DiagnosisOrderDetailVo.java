package com.xz.diagnosisOrder.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.diagnosisOrder.utils.DiagnosisOrderUtils;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 诊疗（预约/挂号）单对象 t_diagnosis_order
 *
 * @author xz
 * @date 2024-03-17
 */
@Data
public class DiagnosisOrderDetailVo {

    /** id */
    private Long id;

    /** 单号 */
    private String orderNo;

    /** 单据类型（1.预约，2.挂号） */
    private Integer orderType;

    /** 患者id */
    private Long patientId;

    /** 就诊日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date registrationDate;

    /** 挂号时间段ID*/
    private Long diagnosisTimeId;

    /** 挂号时间段*/
    private String diagnosisTime;

    /** 就诊事项ID */
    private Long diagnosisItemId;

    /** 就诊事项 */
    private String diagnosisItem;

    /** 医生挂号费id */
    private Long registrationFeeId;

    /** 医生名称 */
    private String doctorName;

    /** 门诊类型 */
    private String registrationFeeType;

    /** 挂号费 */
    private BigDecimal registrationFee;

    /** 优惠金额 */
    private BigDecimal preferentialAmount;

    /** 收款金额 */
    private BigDecimal collectionAmount;

    /** 钱包支付金额 */
    private BigDecimal walletPay;

    /** 1-现金 2-银行卡 3-E支付  */
    private Integer paymentType;

    /** 待收金额 */
    private BigDecimal pendingAmount;

    /** 到店状态（0.未到店，1.已到店，2取消预约） */
    private Integer arrivalStatus;

    /** 客户名称 */
    private String patientName;

    /** 手机号 */
    private String phoneNumber;

    /** 档案号 */
    private String fileNo;

    /** 推荐类型:1-父母 2-子女 3-配偶 4-亲属 5-朋友 6-其他 */
    private Integer referenceType;

    /** 推荐人 */
    private String referenceName;

    /** 0-不详 1-男 2-女 */
    private Integer sex;

    /** 生日 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date birthday;

    /** 年龄 */
    private Integer age;

    /** 地址 */
    private String address;

    /** 证件类型:1-身份证 2-就诊卡 3-护照 4-军官证 5-台湾居民来往大陆通行证 6-港澳居民来往大陆通行证 */
    private Integer documentType;

    /** 证件号码 */
    private String documentNo;

    /** 会员卡号 */
    private String memberCardNo;

    /** 会员等级折扣 */
    private BigDecimal memberLevelDiscount;

    /** 创建人名称 */
    private String createByName;
    /** 部门名称 */
    private String deptName;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 缴费状态（0.未付款,1.已付款，2.待退款，3.已退款） */
    private Integer paymentStatus;
    /** 钱包余额 */
    private BigDecimal walletBalance;
    /** 积分余额 */
    private BigDecimal integralBalance;
    /** 累计充值 */
    private BigDecimal accumulatedRecharge;

    /**
     * 组装客户信息（名称/性别/年龄）
     * @return
     */
    public String getPatientInfo() {
    return DiagnosisOrderUtils.getPatientInfo(patientName,sex,birthday);
  }

}
