package com.xz.diagnosisOrder.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import com.xz.common.core.domain.model.LoginUser;
import com.xz.common.utils.SecurityUtils;
import com.xz.common.utils.StringUtils;
import com.xz.diagnosisOrder.domain.DiagnosisOrder;
import com.xz.diagnosisOrder.utils.DiagnosisOrderUtils;
import com.xz.patient.domain.PatientInfo;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 挂号/诊疗退费单对象 t_diagnosis_refund_order
 *
 * @author xz
 * @date 2024-03-27
 */
@Data
public class DiagnosisRefundOrderVo {


    /** id */
    private Long id;

    /** 单据编码 */
    @Excel(name = "单据编码")
    private String orderNo;

    /** 单据类型（1.挂号费，2.诊疗费） */
    @Excel(name = "单据类型", readConverterExp = "1=.挂号费，2.诊疗费")
    private Integer orderType;

    /** 患者id */
    @Excel(name = "患者id")
    private Long patientId;

    /** 患者信息 */
    @Excel(name = "患者")
    private String patientInfo;

    /** 客户名称 */
    private String patientName;

    /** 客户年龄 */
    private Integer sex;

    /** 生日 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date birthday;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phoneNumber;

    /** 退款总额 */
    @Excel(name = "退款总额")
    private BigDecimal totalRefundAmount;

    /** 钱包退款总额 */
    @Excel(name = "钱包退款总额")
    private BigDecimal totalWalletPay;

    /** 待退款总额 */
    @Excel(name = "待退款总额")
    private BigDecimal totalPendingAmount;

    /** 状态（1.待提交，2.待退款，3.已退款,4.已取消） */
    @Excel(name = "状态", readConverterExp = "2=.待退款，3.已退款,4.已取消")
    private Integer status;

    /** 创建人名称 */
    @Excel(name = "创建人名称")
    private String createByName;

    /** 部门名称 */
    @Excel(name = "部门名称")
    private String deptName;

    /** 制单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "制单时间")
    private Date createTime;

    /**
     * 附件
     */
    @NotEmpty(message = "请上传退款凭证")
    private List<String> voucherUrlList;

    /**
     * 备注
     */
    @Length(max = 200,message = "备注最多{max}个字符")
    private String voucherRemark;

    List<DiagnosisRefundOrderDetailVo> details = new ArrayList<>();

  /**
     * 组装客户信息（名称/性别/年龄）
     * @return
     */
    public String getPatientInfo() {
      return DiagnosisOrderUtils.getPatientInfo(patientName,sex,birthday);
    }
}
