package com.xz.diagnosisOrder.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrderDetail;
import com.xz.diagnosisOrder.dto.DiagnosisTreatmentOrderConsumablesDto;
import com.xz.diagnosisOrder.dto.DiagnosisTreatmentOrderDetailDto;
import com.xz.diagnosisOrder.utils.DiagnosisOrderUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 就诊单对象 t_diagnosis_treatment_order
 *
 * @author xz
 * @date 2024-03-25
 */
@Data
public class DiagnosisTreatmentOrderDetailVo {

  /** id */
  private Long id;

  /** 部门名称 */
  private String deptName;
  /** 创建人名称 */
  private String createByName;
  /** 创建时间 */
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date createTime;
  /** 单据编码 */
  private String orderNo;
  /** 缴费状态（0.未付款,1.已付款，2.待退款，3.已退款） */
  private Integer paymentStatus;
  /** 患者id */
  private Long patientId;
  /** 客户名称 */
  private String patientName;
  /** 0-不详 1-男 2-女 */
  private Integer sex;
  /** 生日 */
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date birthday;
  /** 年龄 */
  private Integer age;
  private String patientInfo;
  /** 手机号 */
  private String phoneNumber;
  /** 档案号 */
  private String fileNo;
  /** 会员卡号 */
  private String memberCardNo;
  /** 医生挂号费id */
  private Long registrationFeeId;
  /** 医生名称 */
  private String doctorName;
  /** 就诊日期 */
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date registrationDate;
  /** 套餐/项目总价 */
  private BigDecimal totalPrice;
  /** 优惠金额 */
  private BigDecimal preferentialAmount;
  /** 收款金额 */
  private BigDecimal collectionAmount;
  /** 钱包支付金额 */
  private BigDecimal walletPay;
  /** 1-现金 2-银行卡 3-E支付  */
  private Integer paymentType;
  /** 待收金额 */
  private BigDecimal pendingAmount;
  /** 减价金额 */
  private BigDecimal reduceAmount;
  /** 打折后金额 */
  private BigDecimal afterDiscountAmount;
  /** 套餐/项目 */
  private List<DiagnosisTreatmentOrderItemVo> details;
  /** 药品 */
  private List<DiagnosisTreatmentOrderConsumablesVo> consumables;
  /** 备注 */
  private String remark;

  /**
   * 组装客户信息（名称/性别/年龄）
   * @return
   */
  public String getPatientInfo() {
    return DiagnosisOrderUtils.getPatientInfo(patientName,sex,birthday);
  }
}
