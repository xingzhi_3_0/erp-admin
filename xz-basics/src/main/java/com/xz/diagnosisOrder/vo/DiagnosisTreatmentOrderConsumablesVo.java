package com.xz.diagnosisOrder.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 就诊单耗品对象 t_diagnosis_treatment_order_consumables
 *
 * @author xz
 * @date 2024-03-30
 */
@Data
public class DiagnosisTreatmentOrderConsumablesVo {
  /**
   * id
   */
  private Long id;

  /**
   * 商品名称
   */
  private String productName;

  /**
   * 单位
   */
  private String unit;

  /**
   * 仓库名称
   */
  private String warehouseName;

  /**
   * 商品数量
   */
  private Integer productNum;
}
