package com.xz.diagnosisOrder.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import com.xz.common.utils.StringUtils;
import com.xz.diagnosisOrder.utils.DiagnosisOrderUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 诊疗（预约/挂号）单对象 t_diagnosis_order
 *
 * @author xz
 * @date 2024-03-17
 */
@Data
@TableName("t_diagnosis_order")
public class DiagnosisOrderListVo{

    /** id */
    private Long id;

    /** 挂号时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "就诊日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date registrationDate;

    /** 患者信息 */
    @Excel(name = "患者")
    private String patientInfo;

    /** 客户名称 */
    private String patientName;

    /** 客户年龄 */
    private Integer sex;

    /** 生日 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date birthday;

    /** 手机号 */
    //@Excel(name = "手机号")
    //private String phoneNumber;

    /** 就诊事项 */
    @Excel(name = "就诊事项")
    private String diagnosisItem;

    /** 医生名称 */
    @Excel(name = "医生")
    private String doctorName;

    /** 门诊类型 */
    @Excel(name = "门诊类型")
    private String registrationFeeType;

    /** 挂号时间段项 */
    @Excel(name = "时间段")
    private String diagnosisTime;

    /** 挂号费 */
    @Excel(name = "费用")
    private BigDecimal registrationFee;

    /** 收款金额 */
    @Excel(name = "实收")
    private BigDecimal collectionAmount;

    /** 制单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "制单时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 制单人 */
    @Excel(name = "制单人")
    private String createByName;

    /** 门店 */
    @Excel(name = "门店")
    private String deptName;

    /** 单号 */
    @Excel(name = "单号")
    private String orderNo;

    /** 缴费状态（0.未付款,1.已付款，2.待退款，3.已退款） */
    @Excel(name = "状态", readConverterExp = "0=未付款,1=已付款,2=待退款,3=已退款")
    private Integer paymentStatus;

  /**
   * 组装客户信息（名称/性别/年龄）
   * @return
   */
  public String getPatientInfo() {
    return DiagnosisOrderUtils.getPatientInfo(patientName,sex,birthday);
  }
}
