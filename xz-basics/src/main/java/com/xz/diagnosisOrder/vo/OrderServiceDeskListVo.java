package com.xz.diagnosisOrder.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 诊疗（预约/挂号）单对象 t_diagnosis_order
 *
 * @author xz
 * @date 2024-03-17
 */
@Data
public class OrderServiceDeskListVo{

    /** id */
    private Long id;

    /** 客户id */
    private Long patientId;

    /** 客户名称 */
    @Excel(name = "客户名称")
    private String patientName;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phoneNumber;

    /** 档案号 */
    @Excel(name = "档案号")
    private String fileNo;

    /** 推荐类型:1-父母 2-子女 3-配偶 4-亲属 5-朋友 6-其他 */
    private Integer referenceType;

    /** 推荐人 */
    @Excel(name = "推荐人")
    private String referenceName;

    /** 单据类型（1.预约，2.挂号） */
    @Excel(name = "单据类型", readConverterExp = "1=预约,2=挂号")
    private Integer orderType;

    /** 挂号时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "就诊日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date registrationDate;

    /** 挂号时间段 */
    @Excel(name = "时间段")
    private String diagnosisTime;

    /** 就诊事项 */
    @Excel(name = "就诊事项")
    private String diagnosisItem;

    /** 医生名称 */
    @Excel(name = "挂号医生")
    private String doctorName;

    /** 到店状态（0.未到店，1.已到店，2取消预约） */
    @Excel(name = "到店状态", readConverterExp = "0=未到店,1=已到店,2=已取消预约")
    private Integer arrivalStatus;

    /** 缴费状态（0.未付款,1.已付款，2.待退款，3.已退款） */
    private Integer paymentStatus;

}
