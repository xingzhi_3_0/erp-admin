package com.xz.diagnosisOrder.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.core.domain.BaseEntity;
import com.xz.common.utils.StringUtils;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrderDetail;
import com.xz.diagnosisOrder.utils.DiagnosisOrderUtils;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 就诊单对象
 *
 * @author xz
 * @date 2024-03-25
 */
@Data
public class DiagnosisTreatmentOrderVo{

  /** id */
  private Long id;

  /** 患者信息 */
  @Excel(name = "患者")
  private String patientInfo;

  /** 客户名称 */
  private String patientName;

  /** 客户年龄 */
  private Integer sex;

  /** 生日 */
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date birthday;

  /** 手机号 */
  @Excel(name = "手机号")
  private String phoneNumber;

  /** 套餐/项目标题 */
  @Excel(name = "检查项目")
  private String orderTitle;

  /** 套餐/项目数量 */
  @Excel(name = "数量")
  private Integer quantity;

  /** 就诊日期 */
  @JsonFormat(pattern = "yyyy-MM-dd")
  @Excel(name = "就诊日期", width = 30, dateFormat = "yyyy-MM-dd")
  private Date registrationDate;

  /** 医生名称 */
  @Excel(name = "医生名称")
  private String doctorName;

  /** 1-现金 2-银行卡 3-E支付  */
  //@Excel(name = "缴费状态", readConverterExp = "1=现金,2=银行卡,3=E支付")
  private Integer paymentType;

  /** 实收金额 */
  @Excel(name = "实收")
  private BigDecimal collectionAmount;

  /** 门店 */
  @Excel(name = "门店")
  private String deptName;

  /** 缴费状态（0.未付款,1.已付款，2.待退款，3.已退款） */
  @Excel(name = "状态", readConverterExp = "0=未付款,1=已付款,2=待退款,3=已退款")
  private Integer paymentStatus;

  /** 单据编码 */
  @Excel(name = "单号")
  private String orderNo;

  /** 制单时间 */
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  @Excel(name = "制单时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
  private Date createTime;

  /** 制单人 */
  @Excel(name = "制单人")
  private String createByName;

  /**
   * 组装客户信息（名称/性别/年龄）
   * @return
   */
  public String getPatientInfo() {
    return DiagnosisOrderUtils.getPatientInfo(patientName,sex,birthday);
  }
}
