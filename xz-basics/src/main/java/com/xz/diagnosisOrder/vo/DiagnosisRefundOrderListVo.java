package com.xz.diagnosisOrder.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xz.common.annotation.Excel;
import com.xz.common.utils.StringUtils;
import com.xz.diagnosisOrder.utils.DiagnosisOrderUtils;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 挂号/诊疗退费单对象 t_diagnosis_refund_order
 *
 * @author xz
 * @date 2024-03-27
 */
@Data
public class DiagnosisRefundOrderListVo {


    /** id */
    private Long id;

    /** 制单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "退费申请时间")
    private Date createTime;

    /** 单据编码 */
    @Excel(name = "退费单号")
    private String orderNo;

    /** 患者id */
    private Long patientId;

    /** 患者信息 */
    @Excel(name = "客户信息")
    private String patientInfo;

    /** 客户名称 */
    private String patientName;

    /** 客户年龄 */
    private Integer sex;

    /** 生日 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date birthday;

    /** 单据类型（1.挂号费，2.诊疗费） */
    @Excel(name = "类型", readConverterExp = "1=挂号费，2=诊疗费")
    private Integer orderType;

    /** 退款金额 */
    @Excel(name = "退款金额")
    private BigDecimal totalRefundAmount;

    /** 状态（1.待提交，2.待退款，3.已退款,4.已取消） */
    @Excel(name = "状态", readConverterExp = "1=待提交，2=待退款，3=已退款，4=已取消")
    private Integer status;

    /**
     * 组装客户信息（名称/性别/年龄）
     * @return
     */
    public String getPatientInfo() {
      return DiagnosisOrderUtils.getPatientInfo(patientName,sex,birthday);
    }

}
