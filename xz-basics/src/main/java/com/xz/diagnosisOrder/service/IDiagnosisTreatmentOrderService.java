package com.xz.diagnosisOrder.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrder;
import com.xz.diagnosisOrder.dto.DiagnosisTreatmentOrderSaveDto;
import com.xz.diagnosisOrder.vo.DiagnosisTreatmentOrderDetailVo;
import com.xz.diagnosisOrder.vo.DiagnosisTreatmentOrderVo;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 就诊单Service接口
 *
 * @author xz
 * @date 2024-03-25
 */
public interface IDiagnosisTreatmentOrderService extends IService<DiagnosisTreatmentOrder> {
  /**
   * 查询就诊单
   *
   * @param id 就诊单主键
   * @return 就诊单
   */
  DiagnosisTreatmentOrderDetailVo selectDiagnosisTreatmentOrderById(Long id);

  /**
   * 查询就诊单
   *
   * @param orderNo 就诊单主键
   * @return 就诊单
   */
  DiagnosisTreatmentOrderDetailVo selectDiagnosisTreatmentOrderByNo(String orderNo);

  List<DiagnosisTreatmentOrder> selectById(Long[] id);

  /**
   * 查询就诊单列表
   *
   * @param diagnosisTreatmentOrder 就诊单
   * @return 就诊单集合
   */
  public List<DiagnosisTreatmentOrderVo> selectDiagnosisTreatmentOrderVoList(DiagnosisTreatmentOrder diagnosisTreatmentOrder);

  /**
   * 新增就诊单
   *
   * @param dto 就诊单
   * @return 结果
   */
  public int insertDiagnosisTreatmentOrder(DiagnosisTreatmentOrderSaveDto dto);

  /**
   * 退费
   *
   * @param ids
   * @return
   */
  int refundFeeById(Long[] ids);


  /**
   * 批量取消退号
   *
   * @param id
   * @return
   */
  boolean setPaymentStatusByBatchId(List<Long> id, Integer status);

}
