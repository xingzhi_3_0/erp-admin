package com.xz.diagnosisOrder.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.diagnosisOrder.domain.DiagnosisOrder;
import com.xz.diagnosisOrder.domain.DiagnosisRefundOrder;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrder;
import com.xz.diagnosisOrder.dto.DiagnosisRefundOrderConfirmDto;
import com.xz.diagnosisOrder.vo.DiagnosisRefundOrderListVo;
import com.xz.diagnosisOrder.vo.DiagnosisRefundOrderVo;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 挂号/诊疗退费单Service接口
 *
 * @author xz
 * @date 2024-03-27
 */
public interface IDiagnosisRefundOrderService extends IService<DiagnosisRefundOrder> {
  /**
   * 查询挂号/诊疗退费单
   *
   * @param id 挂号/诊疗退费单主键
   * @return 挂号/诊疗退费单
   */
  DiagnosisRefundOrder selectDiagnosisRefundOrderById(Long id);

  /**
   * 查询挂号/诊疗退费单列表
   *
   * @param diagnosisRefundOrder 挂号/诊疗退费单
   * @return 挂号/诊疗退费单集合
   */
  List<DiagnosisRefundOrder> selectDiagnosisRefundOrderList(DiagnosisRefundOrder diagnosisRefundOrder);

  /**
   * 查询挂号/诊疗退费单列表
   *
   * @param diagnosisRefundOrder 挂号/诊疗退费单
   * @return 挂号/诊疗退费单集合
   */
  List<DiagnosisRefundOrderListVo> selectDiagnosisRefundOrderVoList(DiagnosisRefundOrder diagnosisRefundOrder);
  /**
   * 新增挂号/诊疗退费单
   *
   * @param order 挂号/诊疗退费单
   * @return 结果
   */
  void insertByOrder(DiagnosisOrder order);

  /**
   * 新增挂号/诊疗退费单
   *
   * @param order 挂号/诊疗退费单
   * @return 结果
   */
  void insertByOrder(List<DiagnosisTreatmentOrder> order);

  /**
   * 初始化退费单(挂号单)
   * @param order
   * @return
   */
  DiagnosisRefundOrder initOrder(DiagnosisOrder order);

  /**
   * 初始化退费单(就诊单)
   * @param order
   * @return
   */
  DiagnosisRefundOrder initOrder(List<DiagnosisTreatmentOrder> order);


  /**
   * 取消退款
   * @param id
   * @return
   */
  boolean cancelOrderById(Long id);

  /**
   * 确认退费
   * @param dto 退费单确认对象
   * @return
   */
  boolean confirmOrder(DiagnosisRefundOrderConfirmDto dto);
}
