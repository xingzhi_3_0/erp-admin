package com.xz.diagnosisOrder.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.diagnosisOrder.domain.DiagnosisOrder;
import com.xz.diagnosisOrder.dto.DiagnosisOrderSaveDto;
import com.xz.diagnosisOrder.vo.DiagnosisOrderDetailVo;
import com.xz.diagnosisOrder.vo.DiagnosisOrderListVo;
import com.xz.diagnosisOrder.vo.OrderServiceDeskListVo;

/**
 * 诊疗（预约/挂号）单Service接口
 *
 * @author xz
 * @date 2024-03-17
 */
public interface IDiagnosisOrderService extends IService<DiagnosisOrder> {
  /**
   * 查询诊疗（预约/挂号）单
   *
   * @param id 诊疗（预约/挂号）单主键
   * @return 诊疗（预约/挂号）单
   */
  public DiagnosisOrder selectDiagnosisOrderById(Long id);

  List<DiagnosisOrder> selectDiagnosisOrderDetailVoByIds(Long[] ids);

  /**
   * 查询诊疗（预约/挂号）单
   *
   * @param id 诊疗（预约/挂号）单主键
   * @return 诊疗（预约/挂号）单
   */
  public DiagnosisOrderDetailVo selectDiagnosisOrderDetailVoById(Long id);

  /**
   * 查询诊疗（预约/挂号）单列表
   *
   * @param diagnosisOrder 诊疗（预约/挂号）单
   * @return 诊疗（预约/挂号）单集合
   */
  public List<DiagnosisOrder> selectDiagnosisOrderList(DiagnosisOrder diagnosisOrder);


  List<DiagnosisOrderListVo> selectDiagnosisOrderVoList(DiagnosisOrder diagnosisOrder);

  /**
   * 查询诊疗（预约/挂号）单列表
   *
   * @param diagnosisOrder 诊疗（预约/挂号）单
   * @return 诊疗（预约/挂号）单集合
   */
  public List<OrderServiceDeskListVo> selectOrderServiceDeskList(DiagnosisOrder diagnosisOrder);

  /**
   * 新增诊疗（预约/挂号）单
   *
   * @param diagnosisOrder 诊疗（预约/挂号）单
   * @return 结果
   */
  public int insertDiagnosisOrder(DiagnosisOrderSaveDto diagnosisOrder);

  /**
   * 修改诊疗（预约/挂号）单
   *
   * @param diagnosisOrder 诊疗（预约/挂号）单
   * @return 结果
   */
  public int updateDiagnosisOrder(DiagnosisOrderSaveDto diagnosisOrder);

  /**
   * 取消预约
   *
   * @param id
   * @return
   */
  int cancelById(Long id);

  /**
   * 退号
   *
   * @param id
   * @return
   */
  boolean refundFeeById(Long id);

  /**
   * 更新支付状态，根据id
   *
   * @param id
   * @return
   */
  boolean setPaymentStatusByBatchId(List<Long> id,Integer status);
}
