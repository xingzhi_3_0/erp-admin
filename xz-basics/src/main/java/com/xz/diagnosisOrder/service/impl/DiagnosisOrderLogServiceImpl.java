package com.xz.diagnosisOrder.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.core.domain.entity.SysDept;
import com.xz.common.core.domain.entity.SysUser;
import com.xz.common.utils.DateUtils;
import com.xz.diagnosisOrder.domain.DiagnosisOrder;
import com.xz.diagnosisOrder.domain.DiagnosisOrderLog;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrder;
import com.xz.diagnosisOrder.enums.PaymentTypeEnum;
import com.xz.diagnosisOrder.mapper.DiagnosisOrderLogMapper;
import com.xz.diagnosisOrder.service.IDiagnosisOrderLogService;
import com.xz.system.service.ISysDeptService;
import com.xz.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 就诊单Service业务层处理
 *
 * @author xz
 * @date 2024-03-27
 */
@Service
public class DiagnosisOrderLogServiceImpl extends ServiceImpl<DiagnosisOrderLogMapper, DiagnosisOrderLog> implements IDiagnosisOrderLogService {
  @Autowired
  private DiagnosisOrderLogMapper diagnosisOrderLogMapper;
  @Autowired
  private ISysDeptService sysDeptService;
  @Autowired
  private ISysUserService userService;
  @Autowired
  private ISysDeptService deptService;
  /**
   * 新增挂号单日志
   * @param order 挂号单
   * @param bizType 业务类型（1.挂号单，2.挂号退费，3.检查单，4.检查退费）
   * @param costCategory 费用类型（1.挂号费，2.诊疗费）
   * @return
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public int insertByOrder(DiagnosisOrder order, Integer bizType, Integer costCategory){
    DiagnosisOrderLog log = new DiagnosisOrderLog();
    //diagnosisOrderLog.setId();
    log.setBizId(order.getId());
    log.setBizDate(order.getRegistrationDate());
    log.setBizNo(order.getOrderNo());
    log.setBizType(bizType);
    log.setPatientId(order.getPatientId());
    log.setCostCategory(costCategory);
    log.setRemark(order.getRemark());
    //商品类字段
    //diagnosisOrderLog.setPackageName();
    //diagnosisOrderLog.setItemNames();
    log.setQuantity(1);
    log.setPrice(order.getRegistrationFee());

    //金额类字段
    log.setTotalPrice(order.getRegistrationFee());
    log.setPreferentialAmount(order.getPreferentialAmount());
    log.setCollectionAmount(order.getCollectionAmount());
    log.setWalletPay(order.getWalletPay());

    Integer paymentType = order.getPaymentType();
    if(null != paymentType){
      if(PaymentTypeEnum.CASH.eq(paymentType)){
        log.setCashPaymentAmount(order.getPendingAmount());
      }else if(PaymentTypeEnum.BANK_CARD.eq(paymentType)){
        log.setBankCardPaymentAmount(order.getPendingAmount());
      }else if(PaymentTypeEnum.E_PAYMENT.eq(paymentType)){
        log.setEPaymentAmount(order.getPendingAmount());
      }
    }

    log.setCreateBy(order.getCreateBy());
    log.setDeptId(order.getDeptId());
    log.setTenantId(order.getTenantId());
    log.setCreateTime(DateUtils.getNowDate());

    SysUser user = userService.selectUserById(order.getCreateBy());
    if(null != user){
      log.setCreateByName(user.getUserName());
    }

    SysDept sysDept = deptService.selectDeptById(log.getDeptId());
    if(null != sysDept){
      log.setDeptName(sysDept.getDeptName());
    }
    //SysDept sysDept = sysDeptService.selectDeptById();
    //填充创建人信息
    /*LoginUser user = SecurityUtils.getLoginUser();
    log.setCreateBy(user.getUserId());
    log.setCreateByName(user.getUsername());
    log.setCreateTime(DateUtils.getNowDate());
    log.setTenantId(user.getTenantId());
    log.setDeptId(user.getDeptId());
    //SysDept sysDept = sysDeptService.selectDeptById();
    log.setDeptName(user.getUser().getDept().getDeptName());*/
    return diagnosisOrderLogMapper.insertDiagnosisOrderLog(log);
  }

  /**
   * 新增就诊单日志
   * @param order 就诊单
   * @param bizType 业务类型（1.挂号单，2.挂号退费，3.检查单，4.检查退费）
   * @param costCategory 费用类型（1.挂号费，2.诊疗费）
   * @return
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public int insertByOrder(DiagnosisTreatmentOrder order, Integer bizType, Integer costCategory){
    DiagnosisOrderLog log = new DiagnosisOrderLog();
    //diagnosisOrderLog.setId();
    log.setBizId(order.getId());
    log.setBizDate(order.getRegistrationDate());
    log.setBizNo(order.getOrderNo());
    log.setBizType(bizType);
    log.setPatientId(order.getPatientId());
    log.setCostCategory(costCategory);
    log.setRemark(order.getRemark());
    //商品类字段
    log.setPackageName(order.getOrderTitle());
    log.setItemNames(order.getItemNames());
    log.setQuantity(order.getQuantity());
    log.setPrice(order.getPrice());

    //金额类字段
    log.setTotalPrice(order.getTotalPrice());
    log.setPreferentialAmount(order.getPreferentialAmount());
    log.setCollectionAmount(order.getCollectionAmount());
    log.setWalletPay(order.getWalletPay());

    Integer paymentType = order.getPaymentType();
    if(null != paymentType){
      if(PaymentTypeEnum.CASH.eq(paymentType)){
        log.setCashPaymentAmount(order.getPendingAmount());
      }else if(PaymentTypeEnum.BANK_CARD.eq(paymentType)){
        log.setBankCardPaymentAmount(order.getPendingAmount());
      }else if(PaymentTypeEnum.E_PAYMENT.eq(paymentType)){
        log.setEPaymentAmount(order.getPendingAmount());
      }
    }

    log.setCreateBy(order.getCreateBy());
    log.setDeptId(order.getDeptId());
    log.setTenantId(order.getTenantId());
    log.setCreateTime(DateUtils.getNowDate());

    SysUser user = userService.selectUserById(order.getCreateBy());
    if(null != user){
      log.setCreateByName(user.getUserName());
    }

    SysDept sysDept = deptService.selectDeptById(order.getDeptId());
    if(null != sysDept){
      log.setDeptName(sysDept.getDeptName());
    }
   /* //填充创建人信息
    LoginUser user = SecurityUtils.getLoginUser();
    log.setCreateBy(user.getUserId());
    log.setCreateByName(user.getUsername());
    log.setCreateTime(DateUtils.getNowDate());
    log.setTenantId(user.getTenantId());
    log.setDeptId(user.getDeptId());
    //SysDept sysDept = sysDeptService.selectDeptById();
    log.setDeptName(user.getUser().getDept().getDeptName());*/
    return diagnosisOrderLogMapper.insertDiagnosisOrderLog(log);
  }
}
