package com.xz.diagnosisOrder.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.diagnosisOrder.domain.DiagnosisOrder;
import com.xz.diagnosisOrder.domain.DiagnosisRefundOrder;
import com.xz.diagnosisOrder.domain.DiagnosisRefundOrderDetail;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrder;

/**
 * 挂号/诊疗退费单详情Service接口
 *
 * @author xz
 * @date 2024-03-27
 */
public interface IDiagnosisRefundOrderDetailService extends IService<DiagnosisRefundOrderDetail> {


  /**
   * 查询挂号/诊疗退费单详情列表
   *
   * @param retfundId 退款单id
   * @return 挂号/诊疗退费单详情
   */
  List<DiagnosisRefundOrderDetail> selectDiagnosisRefundOrderDetailList(Long retfundId);

  /**
   * 新增挂号/诊疗退费单详情(挂号单)
   *
   * @param order
   * @param refundOrder
   * @return
   */
  void insertByOrder(DiagnosisOrder order, DiagnosisRefundOrder refundOrder);

  /**
   * 新增挂号/诊疗退费单详情(就诊单)
   *
   * @param order
   * @param refundOrder
   * @return
   */
  void insertByOrder(List<DiagnosisTreatmentOrder> order, DiagnosisRefundOrder refundOrder);

  /**
   * 初始化详情(挂号单)
   *
   * @param order
   * @param refundOrder
   * @return
   */
  DiagnosisRefundOrderDetail initDetail(DiagnosisOrder order, DiagnosisRefundOrder refundOrder);

  /**
   * 初始化详情(就诊单)
   *
   * @param order
   * @param refundOrder
   * @return
   */
  DiagnosisRefundOrderDetail initOrderDetail(DiagnosisTreatmentOrder order, DiagnosisRefundOrder refundOrder);
}
