package com.xz.diagnosisOrder.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.diagnosis.domain.DiagnosisItem;
import com.xz.diagnosis.service.IDiagnosisItemService;
import com.xz.diagnosis.service.IDiagnosisPackageItemService;
import com.xz.diagnosis.vo.DiagnosisPackageItemVo;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrder;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrderDetail;
import com.xz.diagnosisOrder.mapper.DiagnosisTreatmentOrderDetailMapper;
import com.xz.diagnosisOrder.service.IDiagnosisTreatmentOrderDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 就诊单详情Service业务层处理
 *
 * @author xz
 * @date 2024-03-25
 */
@Service
public class DiagnosisTreatmentOrderDetailServiceImpl extends ServiceImpl<DiagnosisTreatmentOrderDetailMapper, DiagnosisTreatmentOrderDetail> implements IDiagnosisTreatmentOrderDetailService {
  @Autowired
  private DiagnosisTreatmentOrderDetailMapper diagnosisTreatmentOrderDetailMapper;
  @Autowired
  private IDiagnosisPackageItemService diagnosisPackageItemService;
  //@Autowired
  //private IDiagnosisItemService diagnosisItemService;

  /**
   * 查询就诊单详情
   *
   * @param orderId 就诊单详情主键
   * @return 就诊单详情
   */
  @Override
  public List<DiagnosisTreatmentOrderDetail> selectByOrderId(Long orderId) {
    DiagnosisTreatmentOrderDetail detail = new DiagnosisTreatmentOrderDetail();
    detail.setTreatmentOrderId(orderId);
    return diagnosisTreatmentOrderDetailMapper.selectDiagnosisTreatmentOrderDetailList(detail);
  }


  /**
   * 新增就诊单详情
   *
   * @param order 就诊单
   * @return 结果
   */
  @Override
  public List<DiagnosisTreatmentOrderDetail> initForPackage(DiagnosisTreatmentOrder order) {

    Long packageId = order.getAssociationId();
    //查询套餐下的项目数据
    List<DiagnosisPackageItemVo> voList = diagnosisPackageItemService.selectByPackageId(packageId);
    //开始构建结果
    List<DiagnosisTreatmentOrderDetail> details = new ArrayList<>();
    voList.forEach(packageItem -> {
      DiagnosisTreatmentOrderDetail detail = new DiagnosisTreatmentOrderDetail();
      // 填充基本信息
      //detail.setTreatmentOrderId(order.getId());
      detail.setOrderType(order.getOrderType());
      detail.setPackageId(packageId);
      detail.setPackageQuantity(order.getQuantity());
      // 填充套餐-项目信息
      detail.setPackageName(packageItem.getPackageName());
      detail.setPackagePrice(packageItem.getPackagePrice());
      detail.setItemId(packageItem.getItemId());
      detail.setItemName(packageItem.getItemName());
      detail.setItemPrice(packageItem.getPrice());
      detail.setItemUnit(packageItem.getUnit());
      detail.setItemQuantity(packageItem.getQuantity());
      detail.setTotalQuantity(detail.getPackageQuantity() * detail.getItemQuantity());
      // 填充创建人信息
      detail.setCreateBy(order.getCreateBy());
      detail.setCreateTime(order.getCreateTime());
      detail.setTenantId(order.getTenantId());
      detail.setDeptId(order.getDeptId());
      details.add(detail);
    });
    // 填充入订单中待后续处理
    order.setDetails(details);
    return details;
  }

  /**
   * 新增就诊单详情
   *
   * @param order 就诊单
   * @return 结果
   */
  @Override
  public DiagnosisTreatmentOrderDetail initForItem(DiagnosisTreatmentOrder order, DiagnosisItem item) {
    Long itemId = order.getAssociationId();
    DiagnosisTreatmentOrderDetail detail = new DiagnosisTreatmentOrderDetail();
    // 填充基本信息
    detail.setTreatmentOrderId(order.getId());
    detail.setOrderType(order.getOrderType());
    // 填充项目信息
    detail.setItemId(itemId);
    detail.setItemName(item.getItemName());
    detail.setItemPrice(item.getPrice());
    detail.setItemUnit(item.getUnit());
    detail.setItemQuantity(order.getQuantity());
    detail.setTotalQuantity(detail.getItemQuantity());
    // 填充创建人信息
    detail.setCreateBy(order.getCreateBy());
    detail.setCreateTime(order.getCreateTime());
    detail.setTenantId(order.getTenantId());
    detail.setDeptId(order.getDeptId());
    // 填充入订单中待后续处理
    order.setDetails(new ArrayList<>());
    order.getDetails().add(detail);
    return detail;
  }
}
