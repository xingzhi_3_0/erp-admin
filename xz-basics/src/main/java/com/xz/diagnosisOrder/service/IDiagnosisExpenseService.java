package com.xz.diagnosisOrder.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.diagnosisOrder.domain.DiagnosisOrder;
import com.xz.diagnosisOrder.domain.DiagnosisRefundOrder;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrder;
import com.xz.diagnosisOrder.dto.DiagnosisOrderSaveDto;
import com.xz.diagnosisOrder.dto.DiagnosisRefundOrderConfirmDto;
import com.xz.diagnosisOrder.vo.DiagnosisOrderDetailVo;
import com.xz.diagnosisOrder.vo.OrderServiceDeskListVo;
import com.xz.expense.domain.ExpenseBill;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * 诊疗（预约/挂号）单费用Service接口
 *
 * @author xz
 * @date 2024-03-17
 */
public interface IDiagnosisExpenseService {

  /**
   * 扣减会员的钱包余额(挂号单)
   *
   * @param order
   */
  void deductionOfAccountBalance(DiagnosisOrder order);

  /**
   * 扣减会员的钱包余额(诊疗单)
   *
   * @param mainOrder
   */
  void deductionOfAccountBalance(DiagnosisTreatmentOrder mainOrder);

	@Transactional(rollbackFor = Exception.class)
	void refundAccountBalance(BigDecimal walletPay, String memberNo);

	/**
   * 插入费用账单数据
   * @param order 诊疗单
   * @param bizType 业务类型 ExpenseBillBizTypeEnum
   * @param status 状态 ExpenseBillStatusEnum
   * @return
   */
  ExpenseBill insertExpenseBill(DiagnosisOrder order, Integer bizType, Integer status);

  /**
   * 插入费用账单数据
   * @param order 诊疗单
   * @param bizType 业务类型 ExpenseBillBizTypeEnum
   * @param status 状态 ExpenseBillStatusEnum
   * @return
   */
  ExpenseBill insertExpenseBill(DiagnosisTreatmentOrder order, Integer bizType, Integer status);

  @Transactional(rollbackFor = Exception.class)
  ExpenseBill insertExpenseBill(DiagnosisRefundOrder order);

  /**
   * 确认费用账单数据
   * @param dto
   * @return
   */
  int confirmExpenseBill(DiagnosisRefundOrderConfirmDto dto);

  /**
   * 插入费用账单数据
   *
   * @param bizId   业务id
   * @param bizType 业务类型 ExpenseBillBizTypeEnum
   * @return
   */
  int delExpenseBill(Long bizId, Integer bizType);

}
