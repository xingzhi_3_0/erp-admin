package com.xz.diagnosisOrder.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.diagnosisOrder.domain.DiagnosisOrder;
import com.xz.diagnosisOrder.domain.DiagnosisOrderLog;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrder;

/**
 * 就诊单Service接口
 *
 * @author xz
 * @date 2024-03-27
 */
public interface IDiagnosisOrderLogService extends IService<DiagnosisOrderLog> {


  /**
   * 新增挂号单日志
   * @param order 挂号单
   * @param bizType 业务类型（1.挂号单，2.挂号退费，3.检查单，4.检查退费）
   * @param costCategory 费用类型（1.挂号费，2.诊疗费）
   * @return
   */
  int insertByOrder(DiagnosisOrder order, Integer bizType, Integer costCategory);

  /**
   * 新增就诊单日志
   * @param order 就诊单
   * @param bizType 业务类型（1.挂号单，2.挂号退费，3.检查单，4.检查退费）
   * @param costCategory 费用类型（1.挂号费，2.诊疗费）
   * @return
   */
  int insertByOrder(DiagnosisTreatmentOrder order, Integer bizType, Integer costCategory);
}
