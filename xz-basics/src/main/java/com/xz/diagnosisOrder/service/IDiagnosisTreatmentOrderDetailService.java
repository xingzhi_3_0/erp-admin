package com.xz.diagnosisOrder.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.diagnosis.domain.DiagnosisItem;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrder;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrderDetail;

import java.util.List;

/**
 * 就诊单详情Service接口
 *
 * @author xz
 * @date 2024-03-25
 */
public interface IDiagnosisTreatmentOrderDetailService extends IService<DiagnosisTreatmentOrderDetail> {

  /**
   * 查询就诊单详情
   *
   * @param orderId 就诊单详情主键
   * @return 就诊单详情
   */
  public List<DiagnosisTreatmentOrderDetail> selectByOrderId(Long orderId);

  /**
   * 初始化就诊单详情（套餐）
   *
   * @param order 就诊单
   * @return 结果
   */
  public List<DiagnosisTreatmentOrderDetail> initForPackage(DiagnosisTreatmentOrder order);
  /**
   * 初始化就诊单详情（项目）
   *
   * @param order 就诊单
   * @return 结果
   */
  public DiagnosisTreatmentOrderDetail initForItem(DiagnosisTreatmentOrder order, DiagnosisItem item);
}
