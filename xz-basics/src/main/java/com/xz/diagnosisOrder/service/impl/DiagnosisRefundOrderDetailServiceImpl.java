package com.xz.diagnosisOrder.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.xz.common.core.domain.entity.SysDept;
import com.xz.common.core.domain.model.LoginUser;
import com.xz.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xz.common.utils.SecurityUtils;
import com.xz.diagnosisOrder.domain.DiagnosisOrder;
import com.xz.diagnosisOrder.domain.DiagnosisRefundOrder;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrder;
import com.xz.diagnosisOrder.enums.PaymentTypeEnum;
import com.xz.system.service.ISysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xz.diagnosisOrder.mapper.DiagnosisRefundOrderDetailMapper;
import com.xz.diagnosisOrder.domain.DiagnosisRefundOrderDetail;
import com.xz.diagnosisOrder.service.IDiagnosisRefundOrderDetailService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 挂号/诊疗退费单详情Service业务层处理
 *
 * @author xz
 * @date 2024-03-27
 */
@Service
public class DiagnosisRefundOrderDetailServiceImpl extends ServiceImpl<DiagnosisRefundOrderDetailMapper, DiagnosisRefundOrderDetail> implements IDiagnosisRefundOrderDetailService {
  @Autowired
  private DiagnosisRefundOrderDetailMapper diagnosisRefundOrderDetailMapper;


  /**
   * 查询挂号/诊疗退费单详情列表
   *
   * @param retfundId 退款单id
   * @return 挂号/诊疗退费单详情
   */
  @Override
  public List<DiagnosisRefundOrderDetail> selectDiagnosisRefundOrderDetailList(Long retfundId) {
    LambdaUpdateWrapper<DiagnosisRefundOrderDetail> wrapper = Wrappers.lambdaUpdate(DiagnosisRefundOrderDetail.class);
    wrapper.eq(DiagnosisRefundOrderDetail::getRefundOrderId,retfundId);
    return diagnosisRefundOrderDetailMapper.selectList(wrapper);
  }

  /**
   * 新增挂号/诊疗退费单详情(挂号单)
   *
   * @param order
   * @param refundOrder
   * @return
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public void insertByOrder(DiagnosisOrder order, DiagnosisRefundOrder refundOrder) {
    DiagnosisRefundOrderDetail detail = initDetail(order, refundOrder);
    diagnosisRefundOrderDetailMapper.insertDiagnosisRefundOrderDetail(detail);
  }

  /**
   * 新增挂号/诊疗退费单详情(就诊单)
   *
   * @param order
   * @param refundOrder
   * @return
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public void insertByOrder(List<DiagnosisTreatmentOrder> orders, DiagnosisRefundOrder refundOrder) {
    orders.forEach(order->{
      DiagnosisRefundOrderDetail detail = initOrderDetail(order, refundOrder);
      diagnosisRefundOrderDetailMapper.insertDiagnosisRefundOrderDetail(detail);
    });
  }

  /**
   * 初始化详情(挂号单)
   * @param order
   * @param refundOrder
   * @return
   */
  @Override
  public DiagnosisRefundOrderDetail initDetail(DiagnosisOrder order, DiagnosisRefundOrder refundOrder) {
    DiagnosisRefundOrderDetail detail = new DiagnosisRefundOrderDetail();
    detail.setId(refundOrder.getId());
    detail.setBizId(order.getId());
    detail.setBizNo(order.getOrderNo());
    detail.setCostCategory(1);
    //商品类字段
    //diagnosisOrderdetail.setPackageName();
    //diagnosisOrderdetail.setItemNames();
    detail.setQuantity(1);
    //金额类字段
    detail.setCollectionAmount(order.getCollectionAmount());
    detail.setWalletPay(order.getWalletPay());
    detail.setPendingAmount(order.getPendingAmount());
    //填充创建人信息
    if(null != refundOrder){
      detail.setRefundOrderId(refundOrder.getId());
      //填充创建人信息
      detail.setCreateBy(refundOrder.getCreateBy());
      detail.setCreateByName(refundOrder.getCreateByName());
      detail.setCreateTime(refundOrder.getCreateTime());
      detail.setTenantId(refundOrder.getTenantId());
      detail.setDeptId(refundOrder.getDeptId());
      detail.setDeptName(refundOrder.getDeptName());
    }
    return detail;
  }

  /**
   * 初始化详情(就诊单)
   * @param order
   * @param refundOrder
   * @return
   */
  @Override
  public DiagnosisRefundOrderDetail initOrderDetail(DiagnosisTreatmentOrder order, DiagnosisRefundOrder refundOrder) {
    DiagnosisRefundOrderDetail detail = new DiagnosisRefundOrderDetail();
    //detail.setId();
    detail.setBizId(order.getId());
    detail.setBizNo(order.getOrderNo());
    detail.setCostCategory(2);
    //商品类字段
    detail.setPackageName(order.getOrderTitle());
    detail.setItemNames(order.getItemNames());
    detail.setQuantity(1);
    //金额类字段
    detail.setCollectionAmount(order.getCollectionAmount());
    detail.setWalletPay(order.getWalletPay());
    detail.setPendingAmount(order.getPendingAmount());
    if(null != refundOrder){
      detail.setRefundOrderId(refundOrder.getId());
      //填充创建人信息
      detail.setCreateBy(refundOrder.getCreateBy());
      detail.setCreateByName(refundOrder.getCreateByName());
      detail.setCreateTime(refundOrder.getCreateTime());
      detail.setTenantId(refundOrder.getTenantId());
      detail.setDeptId(refundOrder.getDeptId());
      detail.setDeptName(refundOrder.getDeptName());
    }
    return detail;
  }
}
