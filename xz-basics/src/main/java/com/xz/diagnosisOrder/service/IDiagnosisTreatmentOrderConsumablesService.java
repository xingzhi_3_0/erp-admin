package com.xz.diagnosisOrder.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrder;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrderConsumables;
import com.xz.diagnosisOrder.dto.DiagnosisTreatmentOrderConsumablesDto;
import com.xz.diagnosisOrder.dto.DiagnosisTreatmentOrderSaveDto;
import com.xz.diagnosisOrder.vo.DiagnosisTreatmentOrderConsumablesVo;
import com.xz.purchase.domain.PurchaseProduct;
import org.springframework.transaction.annotation.Transactional;

/**
 * 就诊单耗品Service接口
 *
 * @author xz
 * @date 2024-03-30
 */
public interface IDiagnosisTreatmentOrderConsumablesService extends IService<DiagnosisTreatmentOrderConsumables> {

  /**
   * 查询就诊单耗品列表
   *
   * @param diagnosisTreatmentOrderConsumables 就诊单耗品
   * @return 就诊单耗品集合
   */
  public List<DiagnosisTreatmentOrderConsumables> selectDiagnosisTreatmentOrderConsumablesList(DiagnosisTreatmentOrderConsumables diagnosisTreatmentOrderConsumables);

  /**
   * 查询就诊单耗品列表(根据就诊单id)
   *
   * @param treatmentOrderId 就诊单id
   * @return 就诊单耗品
   */
  List<DiagnosisTreatmentOrderConsumables> selectByTreatmentOrderId(Long treatmentOrderId);

  /**
   * 保存消耗品
   *
   * @param dto
   */
  void saveForOrder(DiagnosisTreatmentOrder mainOrder, DiagnosisTreatmentOrderSaveDto dto);

  /**
   * 获取药品Vo集合
   * @param id
   * @return
   */
  List<DiagnosisTreatmentOrderConsumablesVo> getDiagnosisTreatmentOrderConsumablesVo(Long id);
}
