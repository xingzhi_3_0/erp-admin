package com.xz.diagnosisOrder.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.xz.diagnosisOrder.domain.DiagnosisTreatmentOrder;
import com.xz.diagnosisOrder.dto.DiagnosisTreatmentOrderDetailDto;
import com.xz.diagnosisOrder.dto.DiagnosisTreatmentOrderSaveDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 就诊单数据初始化接口
 *
 * @author xz
 * @date 2024-03-25
 */
public interface IDiagnosisTreatmentOrderInitService {

  /**
   * 初始化主订单数据
   *
   * @param dto
   * @return
   */
  DiagnosisTreatmentOrder initMainOrder(DiagnosisTreatmentOrderSaveDto dto);

  /**
   * 初始化“开单”（子订单）数据
   * @param mainOrder 主订单信息
   * @param orderDetailDtos 子订单dto集合
   */
  List<DiagnosisTreatmentOrder> initSubOrder(DiagnosisTreatmentOrder mainOrder,List<DiagnosisTreatmentOrderDetailDto> orderDetailDtos);

  /**
   * 计算费用
   * @param mainOrder 主订单
   * @param subOrders 子订单
   */
  void calculateCosts(DiagnosisTreatmentOrder mainOrder, List<DiagnosisTreatmentOrder> subOrders);
}
