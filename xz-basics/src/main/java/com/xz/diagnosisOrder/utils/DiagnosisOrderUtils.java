package com.xz.diagnosisOrder.utils;

import cn.hutool.core.date.DateUtil;
import com.xz.common.utils.StringUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * @program: xz
 * @description:
 * @author: pengyuyan
 * @create: 2024-03-28 18:36
 **/

public class DiagnosisOrderUtils {

  /**
   * 组装客户信息（名称/性别/年龄）
   * @return
   */
  public static String getPatientInfo(String patientName, Integer sex, Date birthday) {
    // 客户名称
    StringBuilder res = new StringBuilder();
    if(StringUtils.isEmpty(patientName)){
      res.append("--");
    }else{
      res.append(patientName);
    }
    // 性别
    res.append("/");
    if(null == sex || sex.equals(0)){
      res.append("-");
    }else if(sex.equals(1)){
      res.append("男");
    }else if(sex.equals(2)){
      res.append("女");
    }
    // 年龄
    res.append("/");
    int age = 0;
    if(null != birthday){
      age = DateUtil.ageOfNow(birthday);
    }
    res.append(age);
    res.append("岁");
    return res.toString();
  }
}
