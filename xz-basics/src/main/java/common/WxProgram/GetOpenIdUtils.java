package common.WxProgram;

import com.xz.common.utils.sign.Base64;
import net.sf.json.JSONObject;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.AlgorithmParameters;
import java.security.Security;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 获取微信信息工具类
 */
public class GetOpenIdUtils {
    private static Logger logger = LoggerFactory.getLogger(GetOpenIdUtils.class);

    public static JSONObject getSessionKey(String appid, String secret, String code) {
        //官方接口，需要自己提供appid，secret和js_code
            try {
                String requestUrl = "https://api.weixin.qq.com/sns/jscode2session";
                Map<String, Object> param=new HashMap<>();
                param.put("appid",appid);
                param.put("secret",secret);
                param.put("js_code",code);
                param.put("grant_type","authorization_code");
                String data = HttpClientUtil.sendGet(requestUrl,param);
                JSONObject jsonObject = JSONObject.fromObject(data);
                logger.info("微信CODE调用接口数据:"+data);
                return jsonObject.has("errcode") ?null :JSONObject.fromObject(data);
            }catch (Exception e){
                e.printStackTrace();
                logger.error("微信CODE调用接口失败:"+e.getMessage());
                return null;
            }

    }


    /**
     * 获取信息
     */
    public static JSONObject getUserInfo(String encryptedData,String sessionkey,String iv){
        // 被加密的数据
        byte[] dataByte = Base64.decode(encryptedData);
        // 加密秘钥
        byte[] keyByte = Base64.decode(sessionkey);
        // 偏移量
        byte[] ivByte = Base64.decode(iv);
        try {
            // 如果密钥不足16位，那么就补足.  这个if 中的内容很重要
            int base = 16;
            if (keyByte.length % base != 0) {
                int groups = keyByte.length / base + (keyByte.length % base != 0 ? 1 : 0);
                byte[] temp = new byte[groups * base];
                Arrays.fill(temp, (byte) 0);
                System.arraycopy(keyByte, 0, temp, 0, keyByte.length);
                keyByte = temp;
            }
            // 初始化
            Security.addProvider(new BouncyCastleProvider());
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding","BC");
            SecretKeySpec spec = new SecretKeySpec(keyByte, "AES");
            AlgorithmParameters parameters = AlgorithmParameters.getInstance("AES");
            parameters.init(new IvParameterSpec(ivByte));
            cipher.init(Cipher.DECRYPT_MODE, spec, parameters);// 初始化
            byte[] resultByte = cipher.doFinal(dataByte);
            if (null != resultByte && resultByte.length > 0) {
                String result = new String(resultByte, "UTF-8");
                logger.info("微信用户数据: "+result);
                return JSONObject.fromObject(result);
            }
        } catch (Exception e) {
            //getUserInfo(encryptedData,sessionkey,iv);
            logger.error("获取微信用户数据失败:"+e.getMessage());
        }
        return null;
    }




}
