package common.WxProgram;

/**
 * @program: yc
 * @description: 定义消息的内容
 * @author: daiyuanbao
 * @create: 2021-03-26 10:03
 */
public class TemplateData {
    private String value;//

    public TemplateData(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
