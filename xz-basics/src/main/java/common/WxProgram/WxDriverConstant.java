package common.WxProgram;

/**
 * @program: xz-security-pro
 * @description: 微信小程序司机端
 * @author: daiyuanbao
 * @create: 2021-04-13 09:23
 */
public class WxDriverConstant {
    /**redis存储微信小程序access_token*/
    public static final String ACCESS_TOKEN="wxAccessToken_";


    /**客户端微信小程序APPID*/
    public static final String CLIENT_APPID = "wxb5f489d29a5d778a";
    /**客户端微信小程序APPSECRET*/
    public static final String CLIENT_APPSECRET = "4673b05506382accf2243bc4dac76237";

    /**微信小程序消息订阅模板数据*/
    //分配司机通知模板
    public static final String driver_ID="MjXjNAaCdCS37Hq8FdUdf3yeomT69Ao5hPt7ObLC52U"; //客户端分配司机模板id
    public static final String driver_name="thing9"; //客户姓名
    public static final String driver_phone="phone_number10";  //联系电话
    public static final String driver_order="character_string5";  //订单编号
    public static final String driver_date="date6";  //时间
    public static final String driver_carModel="thing7";  //车型
}
