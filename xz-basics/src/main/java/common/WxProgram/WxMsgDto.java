package common.WxProgram;

import lombok.Data;

/**
 * 消息模板数据参数
 */
@Data
public class WxMsgDto {
    //参数一
    private String key1;
    //值一
    private String value1;
    //参数二
    private String key2;
    //值二
    private String value2;
    //参数三
    private String key3;
    //值三
    private String value3;
    //参数四
    private String key4;
    //值四
    private String value4;
    //参数五
    private String key5;
    //值五
    private String value5;
}
