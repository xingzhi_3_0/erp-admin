package common.WxProgram;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @program: xz-security-pro
 * @description: 小程序订阅模板
 * @author: daiyuanbao
 * @create: 2021-09-15 14:22
 */
public class SubscribeTemplate {
    private static final Logger log= LoggerFactory.getLogger(SubscribeTemplate.class);

    //订单到期模板
    private static final String TEMPLATE_EXPIRE="CLQyX9m85MBm2JKSaFRbkwunhihDQZEjTqBQBU_kpMs";
    //订单支付成功通知
    private static final String TEMPLATE_ORDER="nNJsuCE0ih0Z2l3vOyg0x2L3S5zqiESw-yxrLnMa_CE";
    //投诉
    private static final String TEMPLATE_COMPLAINTS="vLxXBzUooFmEbtqxdvS1pFrQlSCwmk52ji-5_ROYpZE";
    //预约取消
    private static final String TEMPLATE_CANCEL_RESERVATION="tPCbXEqY1-ygAKYmgk7AfraNhmfQZUMSFK2EG3GAsaw";
    //预约成功
    private static final String TEMPLATE_ESERVATION="54ymgL524b9DW-itnX_n_x9jaxFA3s56ai1eZGY4EIY";
    //优惠券领取成功
    private static final String TEMPLATE_COUPON_SUCCESS="828Y59jGZiFNerbvA6pZWvKE3K1bCAKbb1v0Rn9IVTU";
    //优惠券到期
    private static final String TEMPLATE_COUPON_EXPIRE="_jtA3XmzN6bbk64hJmwikpyhhRL4yXpcH6bAO_juFgQ";
    //申请提现
    private static final String TEMPLATE_REQUEST_WITHDRAWAL="YIycgag6x3JPbGnas4lilczfkGeUr7z7TxXP9bAj_Qk";
    //提现结果
    private static final String TEMPLATE_SHOW_RESULT="W9ax48CEKQmjVGPrNe5JIVBd9RK0vCr4VVW3ds1s2mo";
    //提现打款结果通知
    private static final String TEMPLATE_PAY_RESULT="stEpYwgvIdzMHI0BqRIyow8Fk0Ta1nT-6VI9WrmLcAs";
    //同行拼团成功
    private static final String TEMPLATE_SHOW_PEER="UoXcjsbetBOLv5FIp49pk_z5UkwfYeUu40Hdbzl4V8o";

    /**
     * 订单支付成功通知
     * @param orderNo 订单编号
     * @param page 跳转页面
     * @param amount 订单金额
     * @param time 下单时间
     * @param prompt 提示
     * @param orderType 订单类型
     * @param openid 用户openId
     */
    public static void templateOrder(String orderNo, String page, String amount, String time,String prompt,String orderType,String openid){
        WxMsgDto wxMsgDto=new WxMsgDto();
        wxMsgDto.setKey1("character_string1");//订单编号
        wxMsgDto.setValue1(orderNo);
        wxMsgDto.setKey2("amount2");//订单金额
        wxMsgDto.setValue2(amount);
        wxMsgDto.setKey3("date3");//下单时间
        wxMsgDto.setValue3(time);
        wxMsgDto.setKey4("thing4");//温馨提示
        wxMsgDto.setValue4(prompt);
        wxMsgDto.setKey5("thing6");//订单类型
        wxMsgDto.setValue5(orderType);
        String push = SendWxMessage.push(openid, page, "", "", TEMPLATE_ORDER, wxMsgDto,5);
        JSONObject msg = JSONObject.parseObject(push);
        String msgVal= msg.getString("errcode");
        if(!msgVal.equals("0")){
            log.error("订单支付消息推送失败,失败原因:"+msg.toString());
        }
    }
    /**
     * 活动到期通知
     * @param obzName 活动项目
     * @param page 跳转页面
     * @param time 下单时间
     * @param prompt 提示
     * @param openid 用户openId
     */
    public static void templateExpire(String obzName, String page, String time,String prompt,String openid){
        WxMsgDto wxMsgDto=new WxMsgDto();
        wxMsgDto.setKey1("thing1");//到期项目
        wxMsgDto.setValue1(obzName);
        wxMsgDto.setKey2("date2");//到期时间
        wxMsgDto.setValue2(time);
        wxMsgDto.setKey3("thing6");//备注信息
        wxMsgDto.setValue3(prompt);
        String push = SendWxMessage.push(openid, page, "", "", TEMPLATE_EXPIRE, wxMsgDto,3);
        JSONObject msg = JSONObject.parseObject(push);
        String msgVal= msg.getString("errcode");
        if(!msgVal.equals("0")){
            log.error("取消订单消息推送失败,失败原因:"+msg.toString());
        }
    }
    /**
     * 投诉
     * @param obzName 服务项目
     * @param page 跳转页面
     * @param orderNo 订单编号
     * @param prompt 提示
     * @param openid 用户openId
     */
    public static void templateComplaints(String orderNo, String page, String obzName,String prompt,String openid){
        WxMsgDto wxMsgDto=new WxMsgDto();
        wxMsgDto.setKey1("character_string1");//订单编号
        wxMsgDto.setValue1(orderNo);
        wxMsgDto.setKey2("thing2");//服务项目
        wxMsgDto.setValue2(obzName);
        wxMsgDto.setKey3("thing3");//温馨提示
        wxMsgDto.setValue3(prompt);
        String push = SendWxMessage.push(openid, page, "", "", TEMPLATE_COMPLAINTS, wxMsgDto,3);
        JSONObject msg = JSONObject.parseObject(push);
        String msgVal= msg.getString("errcode");
        if(!msgVal.equals("0")){
            log.error("取消订单消息推送失败,失败原因:"+msg.toString());
        }
    }
    /**
     * 预约成功
     * @param obzName 预约业务
     * @param page 跳转页面
     * @param orderNo 订单编号
     * @param time 预约时间
     * @param storeName 服务门店
     * @param prompt 提示
     * @param openid 用户openId
     */
    public static void templateEservation(String orderNo, String page, String time,String obzName,String storeName,String prompt,String openid){
        WxMsgDto wxMsgDto=new WxMsgDto();
        wxMsgDto.setKey1("character_string6");//订单号
        wxMsgDto.setValue1(orderNo);
        wxMsgDto.setKey2("date5");//预约时间
        wxMsgDto.setValue2(time);
        wxMsgDto.setKey3("thing7");//预约业务
        wxMsgDto.setValue3(obzName);
        wxMsgDto.setKey4("thing10");//服务门店
        wxMsgDto.setValue4(storeName);
        wxMsgDto.setKey5("thing15");//备注
        wxMsgDto.setValue5(prompt);
        String push = SendWxMessage.push(openid, page, "", "", TEMPLATE_ESERVATION, wxMsgDto,5);
        JSONObject msg = JSONObject.parseObject(push);
        String msgVal= msg.getString("errcode");
        if(!msgVal.equals("0")){
            log.error("取消订单消息推送失败,失败原因:"+msg.toString());
        }
    }
    /**
     * 取消预约
     * @param obzName 服务项目
     * @param page 跳转页面
     * @param why 原因
     * @param time 服务时间
     * @param storeName 服务门店
     * @param prompt 提示
     * @param openid 用户openId
     */
    public static void templateCancelReservation(String why, String page, String time,String obzName,String storeName,String prompt,String openid){
        WxMsgDto wxMsgDto=new WxMsgDto();
        wxMsgDto.setKey1("thing1");//服务项目
        wxMsgDto.setValue1(obzName);
        wxMsgDto.setKey2("time2");//服务时间
        wxMsgDto.setValue2(time);
        wxMsgDto.setKey3("thing6");//取消原因
        wxMsgDto.setValue3(why);
        wxMsgDto.setKey4("thing3");//服务门店
        wxMsgDto.setValue4(storeName);
        wxMsgDto.setKey5("thing8");//备注
        wxMsgDto.setValue5(prompt);
        String push = SendWxMessage.push(openid, page, "", "", TEMPLATE_CANCEL_RESERVATION, wxMsgDto,5);
        JSONObject msg = JSONObject.parseObject(push);
        String msgVal= msg.getString("errcode");
        if(!msgVal.equals("0")){
            log.error("取消订单消息推送失败,失败原因:"+msg.toString());
        }
    }
    /**
     * 领取优惠券成功
     * @param couponName 卡券名称
     * @param page 跳转页面
     * @param time 领取时间
     * @param validity 有效期至
     * @param prompt 备注
     * @param openid 用户openId
     */
    public static void templateCouponSuccess(String couponName, String page, String time,String validity,String prompt,String openid){
        WxMsgDto wxMsgDto=new WxMsgDto();
        wxMsgDto.setKey1("name1");//卡券名称
        wxMsgDto.setValue1(couponName);
        wxMsgDto.setKey2("date2");//领取时间
        wxMsgDto.setValue2(time);
        wxMsgDto.setKey3("thing5");//备注
        wxMsgDto.setValue3(prompt);
        wxMsgDto.setKey4("date3");//有效期至
        wxMsgDto.setValue4(validity);
        String push = SendWxMessage.push(openid, page, "", "", TEMPLATE_COUPON_SUCCESS, wxMsgDto,4);
        JSONObject msg = JSONObject.parseObject(push);
        String msgVal= msg.getString("errcode");
        if(!msgVal.equals("0")){
            log.error("取消订单消息推送失败,失败原因:"+msg.toString());
        }
    }
    /**
     * 优惠券到期提醒
     * @param couponName 卡券名称
     * @param page 跳转页面
     * @param time 到期时间
     * @param couponType 卡券类型
     * @param prompt 温馨提示
     * @param openid 用户openId
     */
    public static void templateCouponExpire(String couponName, String page, String time,String couponType,String prompt,String openid){
        WxMsgDto wxMsgDto=new WxMsgDto();
        wxMsgDto.setKey1("thing1");//卡券类型
        wxMsgDto.setValue1(couponType);
        wxMsgDto.setKey2("date2");//到期时间
        wxMsgDto.setValue2(time);
        wxMsgDto.setKey3("thing3");//温馨提示
        wxMsgDto.setValue3(prompt);
        wxMsgDto.setKey4("thing4");//优惠券名称
        wxMsgDto.setValue4(couponName);
        String push = SendWxMessage.push(openid, page, "", "", TEMPLATE_COUPON_EXPIRE, wxMsgDto,4);
        JSONObject msg = JSONObject.parseObject(push);
        String msgVal= msg.getString("errcode");
        if(!msgVal.equals("0")){
            log.error("取消订单消息推送失败,失败原因:"+msg.toString());
        }
    }
    /**
     * 申请提现通知
     * @param amount 提现金额
     * @param page 跳转页面
     * @param phrase 提现至
     * @param status 提现状态
     * @param prompt 备注
     * @param openid 用户openId
     */
    public static void templateRequestWithdrawal(String amount, String page, String status,String prompt,String orderNo,String phrase,String openid){
        WxMsgDto wxMsgDto=new WxMsgDto();
        wxMsgDto.setKey1("amount1");//提现金额
        wxMsgDto.setValue1(amount);
        wxMsgDto.setKey2("phrase4");//提现状态
        wxMsgDto.setValue2(status);
        wxMsgDto.setKey3("thing5");//备注
        wxMsgDto.setValue3(prompt);
        wxMsgDto.setKey4("character_string3");//订单编号
        wxMsgDto.setValue4(orderNo);
        wxMsgDto.setKey5("phrase2");//提现至
        wxMsgDto.setValue5(phrase);
        String push = SendWxMessage.push(openid, page, "", "", TEMPLATE_REQUEST_WITHDRAWAL, wxMsgDto,5);
        JSONObject msg = JSONObject.parseObject(push);
        String msgVal= msg.getString("errcode");
        if(!msgVal.equals("0")){
            log.error("取消订单消息推送失败,失败原因:"+msg.toString());
        }
    }
    /**
     * 申请结果通知
     * @param amount 提现金额
     * @param page 跳转页面
     * @param time 提现时间
     * @param status 提现状态
     * @param poundage 手续费
     * @param openid 用户openId
     */
    public static void templateShowResult(String amount, String page, String status,String poundage,String orderNo,String time,String openid){
        WxMsgDto wxMsgDto=new WxMsgDto();
        wxMsgDto.setKey1("amount2");//提现金额
        wxMsgDto.setValue1(amount);
        wxMsgDto.setKey2("phrase3");//提现状态
        wxMsgDto.setValue2(status);
        wxMsgDto.setKey3("amount4");//手续费
        wxMsgDto.setValue3(poundage);
        wxMsgDto.setKey4("character_string5");//订单编号
        wxMsgDto.setValue4(orderNo);
        wxMsgDto.setKey5("date1");//提现时间
        wxMsgDto.setValue5(time);
        String push = SendWxMessage.push(openid, page, "", "", TEMPLATE_SHOW_RESULT, wxMsgDto,5);
        JSONObject msg = JSONObject.parseObject(push);
        String msgVal= msg.getString("errcode");
        if(!msgVal.equals("0")){
            log.error("取消订单消息推送失败,失败原因:"+msg.toString());
        }
    }

    /**
     * 申请结果通知
     * @param amount 提现金额
     * @param page 跳转页面
     * @param time 提现时间
     * @param phrase 处理结果
     * @param openid 用户openId
     */
    public static void templateShowResult(String amount, String page, String phrase,String orderNo,String time,String openid){
        WxMsgDto wxMsgDto=new WxMsgDto();
        wxMsgDto.setKey1("time1");//提现金额
        wxMsgDto.setValue1(time);
        wxMsgDto.setKey2("phrase2");//处理结果
        wxMsgDto.setValue2(phrase);
        wxMsgDto.setKey3("amount3");//提现金额
        wxMsgDto.setValue3(amount);
        wxMsgDto.setKey4("character_string5");//订单编号
        wxMsgDto.setValue4(orderNo);
        String push = SendWxMessage.push(openid, page, "", "", TEMPLATE_PAY_RESULT, wxMsgDto,4);
        JSONObject msg = JSONObject.parseObject(push);
        String msgVal= msg.getString("errcode");
        if(!msgVal.equals("0")){
            log.error("取消订单消息推送失败,失败原因:"+msg.toString());
        }
    }
    /**
     * 拼团成功
     * @param phrase1 拼团结果
     * @param page
     * @param thing2 拼团成员
     * @param thing3 拼团商品
     * @param thing4 温馨提示
     * @param openid 用户openId
     */
    public static void templateShowPeer(String phrase1, String page, String thing2,String thing3,String thing4,String openid){
        WxMsgDto wxMsgDto=new WxMsgDto();
        wxMsgDto.setKey1("phrase1");//拼团结果
        wxMsgDto.setValue1(phrase1);
        wxMsgDto.setKey2("thing2");//拼团成员
        wxMsgDto.setValue2(thing2);
        wxMsgDto.setKey3("thing3");//拼团商品
        wxMsgDto.setValue3(thing3);
        wxMsgDto.setKey4("thing4");//温馨提示
        wxMsgDto.setValue4(thing4);
        String push = SendWxMessage.push(openid, page, "", "", TEMPLATE_SHOW_PEER, wxMsgDto,4);
        JSONObject msg = JSONObject.parseObject(push);
        String msgVal= msg.getString("errcode");
        if(!msgVal.equals("0")){
            log.error("取消订单消息推送失败,失败原因:"+msg.toString());
        }
    }




    public static void main(String[] args) {
        templateOrder("AO202109151647247941","pages/main/index/index",0.01+"","2021-09-12 16:50:12","支付成功","秒杀活动","oH1Qe40oNcDxiMO3y2emCAuu6k9U");
    }
}
