package common;
import java.io.Closeable;

/** IO流关闭相关工具类 */
public final class CloseUtils {
	private CloseUtils() {
		throw new UnsupportedOperationException("u can't instantiate me...");
	}
	/**
	 * 关闭 IO
	 *
	 * @param closeables
	 *            closeables
	 */
	public static void closeIO(final Closeable... closeables) {
		if (closeables == null) return;
		for (Closeable closeable : closeables) {
			if (closeable != null) {
				try {
					closeable.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	/**
	 * 安静关闭 IO
	 *
	 * @param closeables
	 *            closeables
	 */
	public static void closeIOQuietly(final Closeable... closeables) {
		if (closeables == null) return;
		for (Closeable closeable : closeables) {
			if (closeable != null) {
				try {
					closeable.close();
				} catch (Exception ignored) {}
			}
		}
	}
}
