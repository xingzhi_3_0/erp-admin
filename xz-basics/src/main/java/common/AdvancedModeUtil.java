package common;

import com.alibaba.fastjson.JSONObject;
import com.xz.common.core.domain.AjaxResult;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ：daiyuanbao
 * @date ：Created in 2022/3/2 13:33
 * @description：阿里高级认证
 */

public class AdvancedModeUtil {
    /**
     * 身份证高级认证
     * @param idcard
     * @param name
     * @return
     */
    public static AjaxResult advancedMode(String idcard,String name,String appCode){
        String host = "https://idcardv2.shumaidata.com";
        String path = "/new-idcard";
        String method = "GET";
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appCode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("idcard", idcard);
        querys.put("name", name);
        try {
            /**
             * 重要提示如下:
             * HttpUtils请从
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
             * 下载
             *
             * 相应的依赖请参照
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
             */
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            //System.out.println(response.toString());
            if(StringUtils.isBlank(response.toString())){
                return AjaxResult.error("认证失败");
            }
            //获取response的body
            //System.out.println(EntityUtils.toString(response.getEntity()));
            JSONObject jsonObject = JSONObject.parseObject(EntityUtils.toString(response.getEntity()));
            Integer code = (Integer)jsonObject.get("code");
            if(code==200){//认证成功
                JSONObject data = JSONObject.parseObject(jsonObject.get("data").toString());
                if(data!=null){
                    Integer result = (Integer)data.get("result");
                    if(result==0){
                        return AjaxResult.success("实名成功");
                    }else if(result==1){
                        return AjaxResult.error("实名信息不一致");
                    }else{
                        return AjaxResult.error("实名信息无记录");
                    }

                }else{
                    return AjaxResult.error("认证失败");
                }
            }else{
                return AjaxResult.error("认证失败");
            }

        } catch (Exception e) {
            return AjaxResult.error("高级认证失败");
        }
    }
}
