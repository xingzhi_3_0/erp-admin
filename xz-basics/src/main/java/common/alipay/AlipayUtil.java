package common.alipay;


import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayFundAuthOperationCancelModel;
import com.alipay.api.domain.AlipayFundAuthOperationDetailQueryModel;
import com.alipay.api.domain.AlipayFundAuthOrderFreezeModel;
import com.alipay.api.domain.AlipayFundAuthOrderUnfreezeModel;
import com.alipay.api.request.AlipayFundAuthOperationCancelRequest;
import com.alipay.api.request.AlipayFundAuthOperationDetailQueryRequest;
import com.alipay.api.request.AlipayFundAuthOrderAppFreezeRequest;
import com.alipay.api.request.AlipayFundAuthOrderUnfreezeRequest;
import com.alipay.api.response.AlipayFundAuthOperationCancelResponse;
import com.alipay.api.response.AlipayFundAuthOperationDetailQueryResponse;
import com.alipay.api.response.AlipayFundAuthOrderAppFreezeResponse;
import com.alipay.api.response.AlipayFundAuthOrderUnfreezeResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

/**
 * @class name:AlipayUtill
 * @description: TODO
 * @author: LiWangJun
 * @create Time: 2023/7/5 17:39
 **/
public class AlipayUtil {

    /**
     * 线上资金授权冻结接口
     * alipay.fund.auth.order.app.freeze
     * @author ZhangZhiJia
     * @date 2023/7/6 9:37
     */
    public static AlipayFundAuthOrderAppFreezeResponse freeze(AlipayFundAuthOrderFreezeModel alipayFundAuthOrderFreezeModel) throws AlipayApiException {
        AlipayClient alipayClient = getAlipayClient();
        AlipayFundAuthOrderAppFreezeRequest request = new AlipayFundAuthOrderAppFreezeRequest();
        request.setBizContent(convertToJson(alipayFundAuthOrderFreezeModel));
        return alipayClient.sdkExecute(request);
    }

    /**
     * 资金授权解冻接口
     * alipay.fund.auth.order.unfreeze
     * @author ZhangZhiJia
     * @date 2023/7/6 9:37
     */
    public static AlipayFundAuthOrderUnfreezeResponse unfreeze(AlipayFundAuthOrderUnfreezeModel alipayFundAuthOrderUnfreezeModel) throws AlipayApiException {
        AlipayClient alipayClient = getAlipayClient();
        AlipayFundAuthOrderUnfreezeRequest request = new AlipayFundAuthOrderUnfreezeRequest();
        request.setBizContent(convertToJson(alipayFundAuthOrderUnfreezeModel));
        return alipayClient.execute(request);
    }
    /**
     * 资金授权操作查询接口
     * alipay.fund.auth.operation.detail.query
     * @author ZhangZhiJia
     * @date 2023/7/6 9:37
     */
    public static AlipayFundAuthOperationDetailQueryResponse operationDetailQuery(AlipayFundAuthOperationDetailQueryModel alipayFundAuthOperationDetailQueryModel) throws AlipayApiException {
        AlipayClient alipayClient = getAlipayClient();
        AlipayFundAuthOperationDetailQueryRequest request = new AlipayFundAuthOperationDetailQueryRequest();
        request.setBizContent(convertToJson(alipayFundAuthOperationDetailQueryModel));
        return alipayClient.execute(request);
    }
    /**
     * 资金授权撤销接口
     * alipay.fund.auth.operation.cancel
     * @author ZhangZhiJia
     * @date 2023/7/6 9:37
     */
    public static AlipayFundAuthOperationCancelResponse operationCancel(AlipayFundAuthOperationCancelModel alipayFundAuthOperationCancelModel) throws AlipayApiException {
        AlipayClient alipayClient = getAlipayClient();
        AlipayFundAuthOperationCancelRequest request = new AlipayFundAuthOperationCancelRequest();
        request.setBizContent(convertToJson(alipayFundAuthOperationCancelModel));
        return alipayClient.execute(request);
    }




    private static AlipayClient getAlipayClient() {
        return  new DefaultAlipayClient("https://openapi.alipay.com/gateway.do","app_id","your private_key","json","GBK","alipay_public_key","RSA2");

    }
    private static String convertToJson(Object obj) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.setPropertyNamingStrategy(new PropertyNamingStrategy.SnakeCaseStrategy());
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
