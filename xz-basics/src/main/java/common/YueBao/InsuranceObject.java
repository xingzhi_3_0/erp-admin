package common.YueBao;

import lombok.Data;

/**
 * 险种
 **/
@Data
public class InsuranceObject {
    private	String	name	;//	险种名称
    private	String	basicAmnt	;//	基本保额
    private	String	prem	;//	保费
    private	String	riskPremSum	;//	保险费小计；包含不计免赔
    private	String	isDeductible	;//	是否投保不计免赔
    private	String	quantity	;//	乘客险投保份数
    private	String	singleAmount	;//	乘客险单位保额
    private	String	glassType	;//	玻璃类型
    private	String	deductibleRate	;//	不计免赔率
    private	String	deductibleAmount	;//	不计免赔额

}
