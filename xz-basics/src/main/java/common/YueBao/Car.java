package common.YueBao;

import lombok.Data;

/**
 * 车辆信息
 **/
@Data
public class Car {
    private	String	carLicense	;//	车牌
    private	String	carKind	;//	车辆种类
    private	String	carUseProperty	;//	车辆使用性质
    private	String	vinNo	;//	车架号
    private	String	engineNo	;//	发动机号
    private	String	modelName	;//	车型名称
    private	String	registerDate	;//	初登日期
    private	String	seatCount	;//	座位数
    private	String	exhaustCapacity	;//	排量
    private	String	power	;//	功率
    private	String	loads	;//	核定载重
    private	String	transferDate	;//	过户日期

}
