package common.YueBao;

import lombok.Data;

/**
 * 车主信息
 **/
@Data
public class CarOwner {

    private String name;//车主
}
