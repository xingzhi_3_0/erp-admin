package common.YueBao;

import lombok.Data;

/**
 * 投保人信息
 **/
@Data
public class Insurer {
    private	String	name	;//	姓名
    private	String	age	;//	年龄
    private	String	gender	;//	性别
    private	String	birth	;//	出生日期；yyyy-MM-dd
    private	String	certiType	;//	证件类型
    private	String	certiCode	;//	证件号码
    private	String	mobilePhone	;//	电话号码
    private	String	landlinePhone	;//	座机号码
    private	String	contactAddress	;//	联系地址
    private	String	email	;//	邮箱地址
    private	String	customerNo	;//	客户号

}
