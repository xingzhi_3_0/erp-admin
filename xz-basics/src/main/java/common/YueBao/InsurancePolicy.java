package common.YueBao;

import lombok.Data;

import java.util.List;

/**
 * @class name:InsurancePolicy
 * @description: TODO
 * @author: LiWangJun
 * @create Time: 2022/8/5 17:10
 **/
@Data
public class InsurancePolicy {
    private	String	policyNo	;//	保单号或者合同号
    private	String	companyName	;//	保险公司名称
    private	String	department	;//	机构名称
    private	String	policyConfirmDate	;//	投保确认时间；yyyy-MM-dd HH:mm:ss
    private	String	vehiclePolicyType	;//	车型保单类型；交强险/商业险
    private	String	policyName	;//	保单名称；机动车综合商业保险保险单
    private	String	premium	;//	总保费
    private	String	coverYears	;//	保险期间；自XXXX时起至XXXX止
    private	String	policyStartDate	;//	保险起期；yyyy-MM-dd HH:mm:ss
    private	String	policyEndDate	;//	保险止期；yyyy-MM-dd HH:mm:ss
    private	String	vciDiscount	;//	商业险折扣
    private	String	tciDiscount	;//	交强险折扣
    private	String	taxPremium	;//	车船税
    private	String	policyType	;//	业务类型
    private	String	insureNo	;//	投保单号
    private	String	arrearFee	;//	车船税往年应缴
    private	String	lateFee	;//	车船税滞纳金
    private	String	payableFee	;//	车船税当年应缴
    private	String	url	;//	保单路径
//    private	CarOwner carOwner	;//	车主信息
//    private	Insurer	insurer	;//	投保人信息
//    private	List<Insurer> insuredList	;//	被保人对象
//    private	Car	car	;//	车辆信息
//    private List<InsuranceObject> productList	;//	险种对象

}
