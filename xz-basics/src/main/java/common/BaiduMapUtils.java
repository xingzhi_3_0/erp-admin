package common;

import net.sf.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;


/**
 * @author lz
 * @date 2021年03月30日 15:05
 * 百度地图经纬度和地址相互转换的工具类
 */
public class BaiduMapUtils {
    private static final  String AK = "5xg2kqlDtrMlhuMjuhVA5tYZe4OQ68dY";
    /**
     * 百度地图通过经纬度来获取地址,传入参数纬度lat、经度lng
     * @param lat
     * @param lng
     * @return
     */
    public static String getDetailAddress(String lat, String lng) {
        JSONObject obj = getLocationInfo(lat, lng).getJSONObject("result");
        return obj.getString("formatted_address");
    }

    /**
     * 获取City
     * @param lat
     * @param lng
     * @return
     */
    public static String getCity(String lat, String lng) {
        JSONObject obj = getLocationInfo(lat, lng).getJSONObject("result").getJSONObject("addressComponent");
        return obj.getString("city");
    }

    public static JSONObject getLocationInfo(String lat, String lng) {
        String url = "http://api.map.baidu.com/geocoder/v2/?callback=renderReverse&location="+lat+","+lng+"&output=json&pois=0&ak="+AK;
        JSONObject obj = JSONObject.fromObject(loadJSON(url));
        //System.out.println(obj);
        return obj;
    }

    /**
     * 百度地图通过地址来获取经纬度，传入参数address
     * @param address
     * @return
     * Todo
     */
    public static Map<String,Double> getLngAndLat(String address){
        Map<String,Double> map=new HashMap<String, Double>();
        String url = "http://api.map.baidu.com/geocoder/v2/?address="+address+"&output=json&ak="+AK;
        String json = loadJSON(url);
        JSONObject obj = JSONObject.fromObject(json);
        if(obj.get("status").toString().equals("0")){
            double lng=obj.getJSONObject("result").getJSONObject("location").getDouble("lng");
            double lat=obj.getJSONObject("result").getJSONObject("location").getDouble("lat");
            map.put("lng", lng);
            map.put("lat", lat);
            System.out.println("经度：" + lng + "--- 纬度：" + lat);
        }else{
            System.out.println("未找到相匹配的经纬度！");
        }
        return map;
    }

    public static String loadJSON (String url) {
        StringBuilder json = new StringBuilder();
        try {
            URL oracle = new URL(url);
            URLConnection yc = oracle.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    yc.getInputStream(),"UTF-8"));
            String inputLine = null;
            while ( (inputLine = in.readLine()) != null) {
                json.append(inputLine);
            }
            in.close();
        } catch (Exception e) {
        }
        int index1 = json.indexOf("(");
        int index2 = json.lastIndexOf(")");

        return json.substring(index1+1,index2).toString();
    }

    public static void main(String[] args) {
        String detailAddress = getDetailAddress("34", "114");
        System.out.println(detailAddress);
    }
}
