package common.pay;

import com.xz.common.exception.ServiceException;
import common.RRException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class WXPay {


    private static final Logger logger= LoggerFactory.getLogger(WXPay.class);




    /**
     * 处理 HTTPS API返回数据，转换成Map对象。return_code为SUCCESS时，验证签名。
     * @param xmlStr API返回的XML格式数据
     * @return Map类型数据
     * @throws Exception
     */
    public static Map<String, String>  processResponseXml(String xmlStr,String key){
        String RETURN_CODE = "return_code";
        String RESULT_CODE = "result_code";
        String return_code;
        String result_code;
        Map<String, String> respData = null;
        try {
            respData = WXPayUtil.xmlToMap(xmlStr);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("processResponseXml处理返回的xml转为map异常"+e.getMessage());
            throw new ServiceException(e.getMessage());
        }
        if (respData.containsKey(RETURN_CODE)) {
            return_code = respData.get(RETURN_CODE);
            logger.error("return_code======"+return_code);
        }else {
            logger.error(String.format("No `return_code` in XML: %s", xmlStr));
            throw new ServiceException("微信参数返回异常,未返回"+RETURN_CODE);
        }
        if (return_code.equals(WXPayConstants.FAIL)) {
            logger.error(String.format("`return_code` is FAIL in XML: %s", xmlStr));
            throw new ServiceException(respData.get("return_msg"));
        }else if (return_code.equals(WXPayConstants.SUCCESS)) {
                if (isResponseSignatureValid(respData,key)) {
                    if (respData.containsKey(RESULT_CODE)) {
                        result_code = respData.get(RESULT_CODE);
                        logger.error("result_code======"+result_code);
                        if (result_code.equals(WXPayConstants.FAIL)) {
                            logger.error(String.format("`result_code` is FAIL in XML: %s", xmlStr));
                            throw new ServiceException(respData.get("err_code")+":"+respData.get("err_code_des"));
                        }else if (result_code.equals(WXPayConstants.SUCCESS)) {
                            logger.info(String.format("processResponseXml success"));
                            return respData;
                        }else{
                            logger.error("result_code状态不正确"+result_code);
                            throw new ServiceException("result_code状态不正确"+result_code);
                        }
                    }else{
                        logger.error(String.format("No `result_code` in XML: %s", xmlStr));
                        throw new ServiceException("微信参数返回异常,未返回"+RESULT_CODE);
                    }
                } else {
                    logger.error(String.format("Invalid sign value in XML: %s", xmlStr));
                    throw new ServiceException("解析微信签名返回参数不正确");
                }
        }else {
            logger.error("return_code状态不正确"+return_code);
            throw new ServiceException("return_code状态不正确"+return_code);
        }
    }
    /**
     * 判断xml数据的sign是否有效，必须包含sign字段，否则返回false。
     *
     * @param reqData 向wxpay post的请求数据
     * @return 签名是否有效
     * @throws Exception
     */
    public static boolean isResponseSignatureValid(Map<String, String> reqData,String key){
        // 返回数据的签名方式和请求中给定的签名方式是一致的
        return WXPayUtil.isSignatureValid(reqData, key, WXPayConstants.SignType.MD5);
    }
}
